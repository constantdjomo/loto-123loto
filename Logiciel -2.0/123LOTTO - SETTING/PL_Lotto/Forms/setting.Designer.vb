﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class setting
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.Panel34 = New System.Windows.Forms.Panel()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.tbIron_max = New System.Windows.Forms.TextBox()
        Me.Panel35 = New System.Windows.Forms.Panel()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.tbIron_min = New System.Windows.Forms.TextBox()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tSilver_max = New System.Windows.Forms.TextBox()
        Me.Panel33 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tSilver_min = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TrackBar1 = New System.Windows.Forms.TrackBar()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TrackBar3 = New System.Windows.Forms.TrackBar()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TrackBar2 = New System.Windows.Forms.TrackBar()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.pb_photo_loto = New System.Windows.Forms.PictureBox()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Panel26 = New System.Windows.Forms.Panel()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.dtg_lot = New System.Windows.Forms.DataGridView()
        Me.Panel24 = New System.Windows.Forms.Panel()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.videoLot_lb = New System.Windows.Forms.Label()
        Me.videoLot_tb = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Tb_id_lot = New System.Windows.Forms.TextBox()
        Me.tb_id_jack = New System.Windows.Forms.TextBox()
        Me.Tb_etat_lot = New System.Windows.Forms.TextBox()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.nom_pv_tb = New System.Windows.Forms.TextBox()
        Me.tb_gold_mat = New System.Windows.Forms.TextBox()
        Me.tbNomLot = New System.Windows.Forms.TextBox()
        Me.montantLot_Tb = New System.Windows.Forms.TextBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Panel28 = New System.Windows.Forms.Panel()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.DpFin = New System.Windows.Forms.DateTimePicker()
        Me.DpDebut = New System.Windows.Forms.DateTimePicker()
        Me.Dg_ListJack = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Panel29 = New System.Windows.Forms.Panel()
        Me.chk_jack_ca = New System.Windows.Forms.CheckBox()
        Me.TrackBar7 = New System.Windows.Forms.TrackBar()
        Me.TrackBar6 = New System.Windows.Forms.TrackBar()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Panel25 = New System.Windows.Forms.Panel()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Panel27 = New System.Windows.Forms.Panel()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TrackBar5 = New System.Windows.Forms.TrackBar()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TrackBar4 = New System.Windows.Forms.TrackBar()
        Me.TbNbShare = New System.Windows.Forms.TextBox()
        Me.TbTotalLotDisp = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BtValider = New System.Windows.Forms.Button()
        Me.TbBronze = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Tb_etat_silver = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.tbNomLotBronze = New System.Windows.Forms.TextBox()
        Me.tb_bronze_mat = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.dtg_lot_bronze = New System.Windows.Forms.DataGridView()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.Panel34.SuspendLayout()
        Me.Panel35.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel33.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.TrackBar3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.pb_photo_loto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel23.SuspendLayout()
        Me.Panel22.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.Panel26.SuspendLayout()
        Me.Panel20.SuspendLayout()
        CType(Me.dtg_lot, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel24.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.Panel18.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel28.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.Dg_ListJack, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.Panel29.SuspendLayout()
        CType(Me.TrackBar7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBar6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        Me.Panel25.SuspendLayout()
        Me.Panel21.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.Panel27.SuspendLayout()
        CType(Me.TrackBar5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TrackBar4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtg_lot_bronze, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox10)
        Me.GroupBox1.Controls.Add(Me.GroupBox9)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Controls.Add(Me.TrackBar1)
        Me.GroupBox1.Controls.Add(Me.Panel2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Panel3)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TrackBar3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TrackBar2)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox1.Location = New System.Drawing.Point(38, 17)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox1.Size = New System.Drawing.Size(1106, 665)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.Panel34)
        Me.GroupBox10.Controls.Add(Me.tbIron_max)
        Me.GroupBox10.Controls.Add(Me.Panel35)
        Me.GroupBox10.Controls.Add(Me.tbIron_min)
        Me.GroupBox10.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.GroupBox10.ForeColor = System.Drawing.Color.White
        Me.GroupBox10.Location = New System.Drawing.Point(12, 500)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(1088, 121)
        Me.GroupBox10.TabIndex = 19
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Mini Cagnotte"
        '
        'Panel34
        '
        Me.Panel34.BackColor = System.Drawing.Color.Transparent
        Me.Panel34.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel34.Controls.Add(Me.Label37)
        Me.Panel34.Location = New System.Drawing.Point(671, 52)
        Me.Panel34.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel34.Name = "Panel34"
        Me.Panel34.Size = New System.Drawing.Size(141, 50)
        Me.Panel34.TabIndex = 18
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(56, 4)
        Me.Label37.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(79, 37)
        Me.Label37.TabIndex = 3
        Me.Label37.Text = "Max"
        '
        'tbIron_max
        '
        Me.tbIron_max.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbIron_max.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIron_max.Location = New System.Drawing.Point(810, 54)
        Me.tbIron_max.Margin = New System.Windows.Forms.Padding(6)
        Me.tbIron_max.Name = "tbIron_max"
        Me.tbIron_max.Size = New System.Drawing.Size(243, 44)
        Me.tbIron_max.TabIndex = 17
        '
        'Panel35
        '
        Me.Panel35.BackColor = System.Drawing.Color.Transparent
        Me.Panel35.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel35.Controls.Add(Me.Label38)
        Me.Panel35.Location = New System.Drawing.Point(33, 56)
        Me.Panel35.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel35.Name = "Panel35"
        Me.Panel35.Size = New System.Drawing.Size(141, 50)
        Me.Panel35.TabIndex = 16
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.White
        Me.Label38.Location = New System.Drawing.Point(56, 4)
        Me.Label38.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(71, 37)
        Me.Label38.TabIndex = 3
        Me.Label38.Text = "Min"
        '
        'tbIron_min
        '
        Me.tbIron_min.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbIron_min.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIron_min.Location = New System.Drawing.Point(172, 58)
        Me.tbIron_min.Margin = New System.Windows.Forms.Padding(6)
        Me.tbIron_min.Name = "tbIron_min"
        Me.tbIron_min.Size = New System.Drawing.Size(243, 44)
        Me.tbIron_min.TabIndex = 15
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.Panel10)
        Me.GroupBox9.Controls.Add(Me.tSilver_max)
        Me.GroupBox9.Controls.Add(Me.Panel33)
        Me.GroupBox9.Controls.Add(Me.tSilver_min)
        Me.GroupBox9.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.GroupBox9.ForeColor = System.Drawing.Color.White
        Me.GroupBox9.Location = New System.Drawing.Point(12, 338)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(1088, 121)
        Me.GroupBox9.TabIndex = 18
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "L'argent c'est l'argent"
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Panel10.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel10.Controls.Add(Me.Label1)
        Me.Panel10.Location = New System.Drawing.Point(671, 52)
        Me.Panel10.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(141, 50)
        Me.Panel10.TabIndex = 18
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(56, 4)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 37)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Max"
        '
        'tSilver_max
        '
        Me.tSilver_max.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tSilver_max.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tSilver_max.Location = New System.Drawing.Point(810, 54)
        Me.tSilver_max.Margin = New System.Windows.Forms.Padding(6)
        Me.tSilver_max.Name = "tSilver_max"
        Me.tSilver_max.Size = New System.Drawing.Size(243, 44)
        Me.tSilver_max.TabIndex = 17
        '
        'Panel33
        '
        Me.Panel33.BackColor = System.Drawing.Color.Transparent
        Me.Panel33.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel33.Controls.Add(Me.Label11)
        Me.Panel33.Location = New System.Drawing.Point(33, 56)
        Me.Panel33.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel33.Name = "Panel33"
        Me.Panel33.Size = New System.Drawing.Size(141, 50)
        Me.Panel33.TabIndex = 16
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(56, 4)
        Me.Label11.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(71, 37)
        Me.Label11.TabIndex = 3
        Me.Label11.Text = "Min"
        '
        'tSilver_min
        '
        Me.tSilver_min.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tSilver_min.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tSilver_min.Location = New System.Drawing.Point(172, 58)
        Me.tSilver_min.Margin = New System.Windows.Forms.Padding(6)
        Me.tSilver_min.Name = "tSilver_min"
        Me.tSilver_min.Size = New System.Drawing.Size(243, 44)
        Me.tSilver_min.TabIndex = 15
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(12, 50)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(376, 52)
        Me.Panel1.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(70, 5)
        Me.Label2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(285, 32)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Prélèvement Marché"
        '
        'TrackBar1
        '
        Me.TrackBar1.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TrackBar1.Location = New System.Drawing.Point(379, 50)
        Me.TrackBar1.Margin = New System.Windows.Forms.Padding(6)
        Me.TrackBar1.Maximum = 100
        Me.TrackBar1.Name = "TrackBar1"
        Me.TrackBar1.Size = New System.Drawing.Size(556, 90)
        Me.TrackBar1.TabIndex = 4
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(12, 232)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(374, 50)
        Me.Panel2.TabIndex = 12
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(99, 4)
        Me.Label3.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(255, 32)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "L'argent c L'argent"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(950, 61)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(6)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(120, 41)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Location = New System.Drawing.Point(12, 144)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(374, 50)
        Me.Panel3.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(161, -3)
        Me.Label5.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(193, 32)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Gold Jackpot"
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.Location = New System.Drawing.Point(950, 147)
        Me.TextBox4.Margin = New System.Windows.Forms.Padding(6)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(120, 41)
        Me.TextBox4.TabIndex = 7
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TrackBar3
        '
        Me.TrackBar3.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TrackBar3.Location = New System.Drawing.Point(379, 138)
        Me.TrackBar3.Margin = New System.Windows.Forms.Padding(6)
        Me.TrackBar3.Maximum = 100
        Me.TrackBar3.Name = "TrackBar3"
        Me.TrackBar3.Size = New System.Drawing.Size(556, 90)
        Me.TrackBar3.TabIndex = 8
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.TextBox2.Location = New System.Drawing.Point(950, 239)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(6)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(120, 41)
        Me.TextBox2.TabIndex = 10
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TrackBar2
        '
        Me.TrackBar2.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TrackBar2.Location = New System.Drawing.Point(379, 232)
        Me.TrackBar2.Margin = New System.Windows.Forms.Padding(6)
        Me.TrackBar2.Maximum = 100
        Me.TrackBar2.Name = "TrackBar2"
        Me.TrackBar2.Size = New System.Drawing.Size(559, 90)
        Me.TrackBar2.TabIndex = 11
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Left
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(6)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(2708, 1410)
        Me.TabControl1.TabIndex = 3
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TabPage3.Controls.Add(Me.GroupBox6)
        Me.TabPage3.Controls.Add(Me.CheckBox2)
        Me.TabPage3.Controls.Add(Me.CheckBox1)
        Me.TabPage3.Controls.Add(Me.GroupBox4)
        Me.TabPage3.Controls.Add(Me.Panel23)
        Me.TabPage3.Controls.Add(Me.ProgressBar1)
        Me.TabPage3.Controls.Add(Me.Panel22)
        Me.TabPage3.Controls.Add(Me.dtg_lot)
        Me.TabPage3.Controls.Add(Me.Panel24)
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Controls.Add(Me.Button8)
        Me.TabPage3.Controls.Add(Me.Button9)
        Me.TabPage3.Controls.Add(Me.Button5)
        Me.TabPage3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage3.Location = New System.Drawing.Point(45, 4)
        Me.TabPage3.Margin = New System.Windows.Forms.Padding(6)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(6)
        Me.TabPage3.Size = New System.Drawing.Size(2659, 1402)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Jackpot"
        '
        'GroupBox6
        '
        Me.GroupBox6.ForeColor = System.Drawing.Color.Black
        Me.GroupBox6.Location = New System.Drawing.Point(52, 115)
        Me.GroupBox6.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox6.Size = New System.Drawing.Size(2574, 146)
        Me.GroupBox6.TabIndex = 11
        Me.GroupBox6.TabStop = False
        '
        'CheckBox2
        '
        Me.CheckBox2.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox2.BackColor = System.Drawing.Color.Crimson
        Me.CheckBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox2.Font = New System.Drawing.Font("Century Gothic", 13.875!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.ForeColor = System.Drawing.Color.White
        Me.CheckBox2.Location = New System.Drawing.Point(1328, 381)
        Me.CheckBox2.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(372, 90)
        Me.CheckBox2.TabIndex = 12
        Me.CheckBox2.Text = "Activé/Desactivé"
        Me.CheckBox2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox2.UseVisualStyleBackColor = False
        '
        'CheckBox1
        '
        Me.CheckBox1.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox1.BackColor = System.Drawing.Color.Crimson
        Me.CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox1.Font = New System.Drawing.Font("Century Gothic", 13.875!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.White
        Me.CheckBox1.Location = New System.Drawing.Point(1712, 381)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(454, 90)
        Me.CheckBox1.TabIndex = 12
        Me.CheckBox1.Text = "Afficher Etat"
        Me.CheckBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox1.UseVisualStyleBackColor = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.pb_photo_loto)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox4.ForeColor = System.Drawing.Color.White
        Me.GroupBox4.Location = New System.Drawing.Point(52, 396)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox4.Size = New System.Drawing.Size(432, 412)
        Me.GroupBox4.TabIndex = 15
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Photo"
        '
        'pb_photo_loto
        '
        Me.pb_photo_loto.Cursor = System.Windows.Forms.Cursors.Hand
        Me.pb_photo_loto.Image = Global.PL_Lotto.My.Resources.Resources.gift_logo
        Me.pb_photo_loto.Location = New System.Drawing.Point(12, 54)
        Me.pb_photo_loto.Margin = New System.Windows.Forms.Padding(6)
        Me.pb_photo_loto.Name = "pb_photo_loto"
        Me.pb_photo_loto.Size = New System.Drawing.Size(408, 333)
        Me.pb_photo_loto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pb_photo_loto.TabIndex = 14
        Me.pb_photo_loto.TabStop = False
        '
        'Panel23
        '
        Me.Panel23.BackColor = System.Drawing.Color.Transparent
        Me.Panel23.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel23.Controls.Add(Me.Label23)
        Me.Panel23.Location = New System.Drawing.Point(500, 390)
        Me.Panel23.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel23.Name = "Panel23"
        Me.Panel23.Size = New System.Drawing.Size(308, 79)
        Me.Panel23.TabIndex = 13
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Century Gothic", 13.875!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(60, 15)
        Me.Label23.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(223, 44)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "Progression"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(810, 390)
        Me.ProgressBar1.Margin = New System.Windows.Forms.Padding(6)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(506, 81)
        Me.ProgressBar1.TabIndex = 10
        '
        'Panel22
        '
        Me.Panel22.BackColor = System.Drawing.Color.DarkSlateGray
        Me.Panel22.Controls.Add(Me.Label24)
        Me.Panel22.Controls.Add(Me.Panel19)
        Me.Panel22.Controls.Add(Me.Panel26)
        Me.Panel22.Controls.Add(Me.Panel20)
        Me.Panel22.Location = New System.Drawing.Point(52, 296)
        Me.Panel22.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(398, 63)
        Me.Panel22.TabIndex = 12
        Me.Panel22.Visible = False
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(324, 10)
        Me.Label24.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(173, 44)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "Legende"
        '
        'Panel19
        '
        Me.Panel19.BackColor = System.Drawing.Color.Gold
        Me.Panel19.Controls.Add(Me.Label21)
        Me.Panel19.Location = New System.Drawing.Point(44, 81)
        Me.Panel19.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(224, 81)
        Me.Panel19.TabIndex = 9
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(20, 19)
        Me.Label21.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(169, 44)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Cagnote"
        '
        'Panel26
        '
        Me.Panel26.BackColor = System.Drawing.Color.Brown
        Me.Panel26.Controls.Add(Me.Label28)
        Me.Panel26.Location = New System.Drawing.Point(564, 81)
        Me.Panel26.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel26.Name = "Panel26"
        Me.Panel26.Size = New System.Drawing.Size(224, 81)
        Me.Panel26.TabIndex = 9
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.White
        Me.Label28.Location = New System.Drawing.Point(50, 19)
        Me.Label28.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(145, 44)
        Me.Label28.TabIndex = 0
        Me.Label28.Text = "Bronze"
        '
        'Panel20
        '
        Me.Panel20.BackColor = System.Drawing.Color.Silver
        Me.Panel20.Controls.Add(Me.Label22)
        Me.Panel20.Location = New System.Drawing.Point(304, 81)
        Me.Panel20.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(224, 81)
        Me.Panel20.TabIndex = 9
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(50, 19)
        Me.Label22.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(121, 44)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Silver"
        '
        'dtg_lot
        '
        Me.dtg_lot.AllowUserToAddRows = False
        Me.dtg_lot.AllowUserToDeleteRows = False
        Me.dtg_lot.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dtg_lot.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtg_lot.Location = New System.Drawing.Point(52, 838)
        Me.dtg_lot.Margin = New System.Windows.Forms.Padding(6)
        Me.dtg_lot.Name = "dtg_lot"
        Me.dtg_lot.ReadOnly = True
        Me.dtg_lot.RowHeadersWidth = 82
        Me.dtg_lot.Size = New System.Drawing.Size(1832, 544)
        Me.dtg_lot.TabIndex = 0
        '
        'Panel24
        '
        Me.Panel24.BackColor = System.Drawing.Color.Transparent
        Me.Panel24.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel24.Controls.Add(Me.Label20)
        Me.Panel24.Controls.Add(Me.Label26)
        Me.Panel24.Location = New System.Drawing.Point(52, 15)
        Me.Panel24.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel24.Name = "Panel24"
        Me.Panel24.Size = New System.Drawing.Size(1026, 87)
        Me.Panel24.TabIndex = 6
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(630, 33)
        Me.Label20.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(369, 37)
        Me.Label20.TabIndex = 3
        Me.Label20.Text = "(pour filtrer les Cadeaux)"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.White
        Me.Label26.Location = New System.Drawing.Point(22, 25)
        Me.Label26.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(549, 48)
        Me.Label26.TabIndex = 3
        Me.Label26.Text = "Choisir Un Point de Vente: "
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button3)
        Me.GroupBox3.Controls.Add(Me.Panel9)
        Me.GroupBox3.Controls.Add(Me.videoLot_tb)
        Me.GroupBox3.Controls.Add(Me.GroupBox5)
        Me.GroupBox3.Controls.Add(Me.Panel17)
        Me.GroupBox3.Controls.Add(Me.Tb_id_lot)
        Me.GroupBox3.Controls.Add(Me.tb_id_jack)
        Me.GroupBox3.Controls.Add(Me.Tb_etat_lot)
        Me.GroupBox3.Controls.Add(Me.Panel18)
        Me.GroupBox3.Controls.Add(Me.Panel7)
        Me.GroupBox3.Controls.Add(Me.Panel5)
        Me.GroupBox3.Controls.Add(Me.Panel6)
        Me.GroupBox3.Controls.Add(Me.nom_pv_tb)
        Me.GroupBox3.Controls.Add(Me.tb_gold_mat)
        Me.GroupBox3.Controls.Add(Me.tbNomLot)
        Me.GroupBox3.Controls.Add(Me.montantLot_Tb)
        Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox3.Location = New System.Drawing.Point(500, 483)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox3.Size = New System.Drawing.Size(2132, 325)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Button3.Location = New System.Drawing.Point(1484, 238)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 48)
        Me.Button3.TabIndex = 11
        Me.Button3.Text = "..."
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Panel9.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel9.Controls.Add(Me.videoLot_lb)
        Me.Panel9.Location = New System.Drawing.Point(784, 238)
        Me.Panel9.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(308, 50)
        Me.Panel9.TabIndex = 10
        '
        'videoLot_lb
        '
        Me.videoLot_lb.AutoSize = True
        Me.videoLot_lb.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.videoLot_lb.ForeColor = System.Drawing.Color.White
        Me.videoLot_lb.Location = New System.Drawing.Point(174, 2)
        Me.videoLot_lb.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.videoLot_lb.Name = "videoLot_lb"
        Me.videoLot_lb.Size = New System.Drawing.Size(91, 32)
        Me.videoLot_lb.TabIndex = 3
        Me.videoLot_lb.Text = "Video"
        '
        'videoLot_tb
        '
        Me.videoLot_tb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.videoLot_tb.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.videoLot_tb.Location = New System.Drawing.Point(1092, 240)
        Me.videoLot_tb.Margin = New System.Windows.Forms.Padding(6)
        Me.videoLot_tb.Name = "videoLot_tb"
        Me.videoLot_tb.ReadOnly = True
        Me.videoLot_tb.Size = New System.Drawing.Size(392, 44)
        Me.videoLot_tb.TabIndex = 9
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.RadioButton2)
        Me.GroupBox5.Controls.Add(Me.RadioButton1)
        Me.GroupBox5.Location = New System.Drawing.Point(368, 29)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox5.Size = New System.Drawing.Size(358, 113)
        Me.GroupBox5.TabIndex = 9
        Me.GroupBox5.TabStop = False
        '
        'RadioButton2
        '
        Me.RadioButton2.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton2.BackColor = System.Drawing.Color.Crimson
        Me.RadioButton2.ForeColor = System.Drawing.Color.White
        Me.RadioButton2.Location = New System.Drawing.Point(200, 38)
        Me.RadioButton2.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(116, 52)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "NON"
        Me.RadioButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'RadioButton1
        '
        Me.RadioButton1.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton1.BackColor = System.Drawing.Color.Crimson
        Me.RadioButton1.ForeColor = System.Drawing.Color.White
        Me.RadioButton1.Location = New System.Drawing.Point(42, 38)
        Me.RadioButton1.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(116, 52)
        Me.RadioButton1.TabIndex = 1
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "OUI"
        Me.RadioButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'Panel17
        '
        Me.Panel17.BackColor = System.Drawing.Color.Transparent
        Me.Panel17.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel17.Controls.Add(Me.Label19)
        Me.Panel17.Location = New System.Drawing.Point(784, 152)
        Me.Panel17.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(308, 50)
        Me.Panel17.TabIndex = 8
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(214, 6)
        Me.Label19.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(62, 32)
        Me.Label19.TabIndex = 3
        Me.Label19.Text = "Etat"
        '
        'Tb_id_lot
        '
        Me.Tb_id_lot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Tb_id_lot.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tb_id_lot.Location = New System.Drawing.Point(1506, 152)
        Me.Tb_id_lot.Margin = New System.Windows.Forms.Padding(6)
        Me.Tb_id_lot.Name = "Tb_id_lot"
        Me.Tb_id_lot.ReadOnly = True
        Me.Tb_id_lot.Size = New System.Drawing.Size(158, 44)
        Me.Tb_id_lot.TabIndex = 7
        Me.Tb_id_lot.Visible = False
        '
        'tb_id_jack
        '
        Me.tb_id_jack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tb_id_jack.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_id_jack.Location = New System.Drawing.Point(1506, 67)
        Me.tb_id_jack.Margin = New System.Windows.Forms.Padding(6)
        Me.tb_id_jack.Name = "tb_id_jack"
        Me.tb_id_jack.ReadOnly = True
        Me.tb_id_jack.Size = New System.Drawing.Size(158, 44)
        Me.tb_id_jack.TabIndex = 7
        Me.tb_id_jack.Visible = False
        '
        'Tb_etat_lot
        '
        Me.Tb_etat_lot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Tb_etat_lot.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tb_etat_lot.Location = New System.Drawing.Point(1092, 154)
        Me.Tb_etat_lot.Margin = New System.Windows.Forms.Padding(6)
        Me.Tb_etat_lot.Name = "Tb_etat_lot"
        Me.Tb_etat_lot.Size = New System.Drawing.Size(392, 44)
        Me.Tb_etat_lot.TabIndex = 7
        '
        'Panel18
        '
        Me.Panel18.BackColor = System.Drawing.Color.Transparent
        Me.Panel18.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel18.Controls.Add(Me.Label25)
        Me.Panel18.Location = New System.Drawing.Point(46, 238)
        Me.Panel18.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(308, 50)
        Me.Panel18.TabIndex = 6
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(89, 2)
        Me.Label25.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(205, 32)
        Me.Label25.TabIndex = 3
        Me.Label25.Text = "Point De Vente"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Panel7.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.Label8)
        Me.Panel7.Location = New System.Drawing.Point(46, 154)
        Me.Panel7.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(308, 50)
        Me.Panel7.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(126, 6)
        Me.Label8.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(164, 32)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Nom Du Lot"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Panel5.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Location = New System.Drawing.Point(786, 69)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(308, 50)
        Me.Panel5.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(142, 6)
        Me.Label4.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(112, 31)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Montant"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Transparent
        Me.Panel6.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.Label6)
        Me.Panel6.Location = New System.Drawing.Point(46, 69)
        Me.Panel6.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(308, 50)
        Me.Panel6.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(46, 6)
        Me.Label6.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(247, 32)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Materiel (oui/non)"
        '
        'nom_pv_tb
        '
        Me.nom_pv_tb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nom_pv_tb.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nom_pv_tb.Location = New System.Drawing.Point(354, 238)
        Me.nom_pv_tb.Margin = New System.Windows.Forms.Padding(6)
        Me.nom_pv_tb.Name = "nom_pv_tb"
        Me.nom_pv_tb.ReadOnly = True
        Me.nom_pv_tb.Size = New System.Drawing.Size(370, 44)
        Me.nom_pv_tb.TabIndex = 1
        '
        'tb_gold_mat
        '
        Me.tb_gold_mat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tb_gold_mat.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_gold_mat.Location = New System.Drawing.Point(1793, 239)
        Me.tb_gold_mat.Margin = New System.Windows.Forms.Padding(6)
        Me.tb_gold_mat.Name = "tb_gold_mat"
        Me.tb_gold_mat.ReadOnly = True
        Me.tb_gold_mat.Size = New System.Drawing.Size(35, 44)
        Me.tb_gold_mat.TabIndex = 1
        Me.tb_gold_mat.Visible = False
        '
        'tbNomLot
        '
        Me.tbNomLot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNomLot.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNomLot.Location = New System.Drawing.Point(354, 154)
        Me.tbNomLot.Margin = New System.Windows.Forms.Padding(6)
        Me.tbNomLot.Name = "tbNomLot"
        Me.tbNomLot.Size = New System.Drawing.Size(370, 44)
        Me.tbNomLot.TabIndex = 1
        '
        'montantLot_Tb
        '
        Me.montantLot_Tb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.montantLot_Tb.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.montantLot_Tb.Location = New System.Drawing.Point(1092, 69)
        Me.montantLot_Tb.Margin = New System.Windows.Forms.Padding(6)
        Me.montantLot_Tb.Name = "montantLot_Tb"
        Me.montantLot_Tb.Size = New System.Drawing.Size(392, 44)
        Me.montantLot_Tb.TabIndex = 1
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Crimson
        Me.Button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Century Gothic", 13.875!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.Location = New System.Drawing.Point(1920, 1131)
        Me.Button8.Margin = New System.Windows.Forms.Padding(6)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(272, 94)
        Me.Button8.TabIndex = 5
        Me.Button8.Text = "Supprimer"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.Crimson
        Me.Button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Century Gothic", 13.875!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.White
        Me.Button9.Location = New System.Drawing.Point(1920, 962)
        Me.Button9.Margin = New System.Windows.Forms.Padding(6)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(272, 94)
        Me.Button9.TabIndex = 5
        Me.Button9.Text = "Ajouter"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Crimson
        Me.Button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Century Gothic", 13.875!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(2182, 381)
        Me.Button5.Margin = New System.Windows.Forms.Padding(6)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(444, 90)
        Me.Button5.TabIndex = 8
        Me.Button5.Text = "Enregistrer"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TabPage1.Controls.Add(Me.Label31)
        Me.TabPage1.Controls.Add(Me.Panel28)
        Me.TabPage1.Controls.Add(Me.Panel4)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.DpFin)
        Me.TabPage1.Controls.Add(Me.DpDebut)
        Me.TabPage1.Controls.Add(Me.Dg_ListJack)
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.TbTotalLotDisp)
        Me.TabPage1.Controls.Add(Me.TextBox5)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.Button2)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.BtValider)
        Me.TabPage1.ForeColor = System.Drawing.Color.White
        Me.TabPage1.Location = New System.Drawing.Point(45, 4)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(6)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(6)
        Me.TabPage1.Size = New System.Drawing.Size(2659, 1402)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "% Marché"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(1662, 1110)
        Me.Label31.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(51, 36)
        Me.Label31.TabIndex = 16
        Me.Label31.Text = ">>"
        '
        'Panel28
        '
        Me.Panel28.BackColor = System.Drawing.Color.Transparent
        Me.Panel28.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel28.Controls.Add(Me.Label32)
        Me.Panel28.Location = New System.Drawing.Point(2112, 733)
        Me.Panel28.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel28.Name = "Panel28"
        Me.Panel28.Size = New System.Drawing.Size(212, 50)
        Me.Panel28.TabIndex = 6
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.White
        Me.Label32.Location = New System.Drawing.Point(44, 4)
        Me.Label32.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(143, 37)
        Me.Label32.TabIndex = 3
        Me.Label32.Text = "Total Lot"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Location = New System.Drawing.Point(38, 733)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(376, 50)
        Me.Panel4.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(256, 4)
        Me.Label7.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(98, 37)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Reste"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(1486, 739)
        Me.Label9.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(55, 37)
        Me.Label9.TabIndex = 3
        Me.Label9.Text = "To"
        '
        'DpFin
        '
        Me.DpFin.Location = New System.Drawing.Point(1552, 733)
        Me.DpFin.Margin = New System.Windows.Forms.Padding(6)
        Me.DpFin.Name = "DpFin"
        Me.DpFin.Size = New System.Drawing.Size(414, 41)
        Me.DpFin.TabIndex = 15
        '
        'DpDebut
        '
        Me.DpDebut.Location = New System.Drawing.Point(1056, 737)
        Me.DpDebut.Margin = New System.Windows.Forms.Padding(6)
        Me.DpDebut.Name = "DpDebut"
        Me.DpDebut.Size = New System.Drawing.Size(414, 41)
        Me.DpDebut.TabIndex = 15
        '
        'Dg_ListJack
        '
        Me.Dg_ListJack.AllowUserToAddRows = False
        Me.Dg_ListJack.AllowUserToDeleteRows = False
        Me.Dg_ListJack.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.Dg_ListJack.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dg_ListJack.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4})
        Me.Dg_ListJack.Location = New System.Drawing.Point(1730, 833)
        Me.Dg_ListJack.Margin = New System.Windows.Forms.Padding(6)
        Me.Dg_ListJack.Name = "Dg_ListJack"
        Me.Dg_ListJack.ReadOnly = True
        Me.Dg_ListJack.RowHeadersWidth = 82
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        Me.Dg_ListJack.RowsDefaultCellStyle = DataGridViewCellStyle5
        Me.Dg_ListJack.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Dg_ListJack.Size = New System.Drawing.Size(896, 544)
        Me.Dg_ListJack.TabIndex = 14
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Ticket"
        Me.DataGridViewTextBoxColumn1.MinimumWidth = 10
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Jackpot"
        Me.DataGridViewTextBoxColumn2.MinimumWidth = 10
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Montant"
        Me.DataGridViewTextBoxColumn3.MinimumWidth = 10
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Caissier(e)"
        Me.DataGridViewTextBoxColumn4.MinimumWidth = 10
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column5, Me.Column1, Me.Column2, Me.Column3, Me.Column4})
        Me.DataGridView1.Location = New System.Drawing.Point(38, 833)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(6)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersWidth = 82
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(1604, 544)
        Me.DataGridView1.TabIndex = 14
        '
        'Column5
        '
        Me.Column5.HeaderText = "id PV"
        Me.Column5.MinimumWidth = 10
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Visible = False
        '
        'Column1
        '
        Me.Column1.HeaderText = "Salle"
        Me.Column1.MinimumWidth = 10
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Column2
        '
        Me.Column2.HeaderText = "Entree"
        Me.Column2.MinimumWidth = 10
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "Sortie"
        Me.Column3.MinimumWidth = 10
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "Reste"
        Me.Column4.MinimumWidth = 10
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Panel29)
        Me.GroupBox2.Controls.Add(Me.TrackBar7)
        Me.GroupBox2.Controls.Add(Me.TrackBar6)
        Me.GroupBox2.Controls.Add(Me.Panel8)
        Me.GroupBox2.Controls.Add(Me.Panel25)
        Me.GroupBox2.Controls.Add(Me.Panel21)
        Me.GroupBox2.Controls.Add(Me.Panel15)
        Me.GroupBox2.Controls.Add(Me.TextBox9)
        Me.GroupBox2.Controls.Add(Me.Panel27)
        Me.GroupBox2.Controls.Add(Me.TextBox8)
        Me.GroupBox2.Controls.Add(Me.TextBox7)
        Me.GroupBox2.Controls.Add(Me.TrackBar5)
        Me.GroupBox2.Controls.Add(Me.TextBox3)
        Me.GroupBox2.Controls.Add(Me.TrackBar4)
        Me.GroupBox2.Controls.Add(Me.TbNbShare)
        Me.GroupBox2.Location = New System.Drawing.Point(1192, 17)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox2.Size = New System.Drawing.Size(1134, 665)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        '
        'Panel29
        '
        Me.Panel29.BackColor = System.Drawing.Color.Transparent
        Me.Panel29.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel29.Controls.Add(Me.chk_jack_ca)
        Me.Panel29.Location = New System.Drawing.Point(586, 565)
        Me.Panel29.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel29.Name = "Panel29"
        Me.Panel29.Size = New System.Drawing.Size(512, 56)
        Me.Panel29.TabIndex = 13
        '
        'chk_jack_ca
        '
        Me.chk_jack_ca.AutoSize = True
        Me.chk_jack_ca.ForeColor = System.Drawing.Color.White
        Me.chk_jack_ca.Location = New System.Drawing.Point(43, 6)
        Me.chk_jack_ca.Name = "chk_jack_ca"
        Me.chk_jack_ca.Size = New System.Drawing.Size(375, 40)
        Me.chk_jack_ca.TabIndex = 14
        Me.chk_jack_ca.Text = "Deduire Jackpot du CA"
        Me.chk_jack_ca.UseVisualStyleBackColor = True
        '
        'TrackBar7
        '
        Me.TrackBar7.Location = New System.Drawing.Point(396, 325)
        Me.TrackBar7.Margin = New System.Windows.Forms.Padding(6)
        Me.TrackBar7.Maximum = 100
        Me.TrackBar7.Name = "TrackBar7"
        Me.TrackBar7.Size = New System.Drawing.Size(566, 90)
        Me.TrackBar7.TabIndex = 13
        '
        'TrackBar6
        '
        Me.TrackBar6.Location = New System.Drawing.Point(396, 237)
        Me.TrackBar6.Margin = New System.Windows.Forms.Padding(6)
        Me.TrackBar6.Maximum = 100
        Me.TrackBar6.Name = "TrackBar6"
        Me.TrackBar6.Size = New System.Drawing.Size(566, 90)
        Me.TrackBar6.TabIndex = 13
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Transparent
        Me.Panel8.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.Label16)
        Me.Panel8.Location = New System.Drawing.Point(12, 409)
        Me.Panel8.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(374, 50)
        Me.Panel8.TabIndex = 12
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(154, 2)
        Me.Label16.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(171, 32)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "Nb. Partage"
        '
        'Panel25
        '
        Me.Panel25.BackColor = System.Drawing.Color.Transparent
        Me.Panel25.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel25.Controls.Add(Me.Label29)
        Me.Panel25.Location = New System.Drawing.Point(12, 327)
        Me.Panel25.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel25.Name = "Panel25"
        Me.Panel25.Size = New System.Drawing.Size(374, 50)
        Me.Panel25.TabIndex = 12
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label29.ForeColor = System.Drawing.Color.White
        Me.Label29.Location = New System.Drawing.Point(160, 8)
        Me.Label29.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(142, 32)
        Me.Label29.TabIndex = 3
        Me.Label29.Text = "Loto Moto"
        '
        'Panel21
        '
        Me.Panel21.BackColor = System.Drawing.Color.Transparent
        Me.Panel21.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel21.Controls.Add(Me.Label27)
        Me.Panel21.Location = New System.Drawing.Point(12, 238)
        Me.Panel21.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(374, 50)
        Me.Panel21.TabIndex = 12
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label27.ForeColor = System.Drawing.Color.White
        Me.Label27.Location = New System.Drawing.Point(160, 8)
        Me.Label27.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(162, 32)
        Me.Label27.TabIndex = 3
        Me.Label27.Text = "Lot Mystere"
        '
        'Panel15
        '
        Me.Panel15.BackColor = System.Drawing.Color.Transparent
        Me.Panel15.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel15.Controls.Add(Me.Label17)
        Me.Panel15.Location = New System.Drawing.Point(12, 138)
        Me.Panel15.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(374, 50)
        Me.Panel15.TabIndex = 12
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(176, 4)
        Me.Label17.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(155, 32)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "Lot a gogo"
        '
        'TextBox9
        '
        Me.TextBox9.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.TextBox9.Location = New System.Drawing.Point(978, 325)
        Me.TextBox9.Margin = New System.Windows.Forms.Padding(6)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(120, 41)
        Me.TextBox9.TabIndex = 10
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel27
        '
        Me.Panel27.BackColor = System.Drawing.Color.Transparent
        Me.Panel27.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel27.Controls.Add(Me.Label30)
        Me.Panel27.Location = New System.Drawing.Point(12, 50)
        Me.Panel27.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel27.Name = "Panel27"
        Me.Panel27.Size = New System.Drawing.Size(374, 50)
        Me.Panel27.TabIndex = 12
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold)
        Me.Label30.ForeColor = System.Drawing.Color.White
        Me.Label30.Location = New System.Drawing.Point(128, 4)
        Me.Label30.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(197, 32)
        Me.Label30.TabIndex = 3
        Me.Label30.Text = "Mini Cagnotte"
        '
        'TextBox8
        '
        Me.TextBox8.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.TextBox8.Location = New System.Drawing.Point(978, 237)
        Me.TextBox8.Margin = New System.Windows.Forms.Padding(6)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(120, 41)
        Me.TextBox8.TabIndex = 10
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox7
        '
        Me.TextBox7.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.TextBox7.Location = New System.Drawing.Point(978, 142)
        Me.TextBox7.Margin = New System.Windows.Forms.Padding(6)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(120, 41)
        Me.TextBox7.TabIndex = 10
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TrackBar5
        '
        Me.TrackBar5.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TrackBar5.Location = New System.Drawing.Point(396, 138)
        Me.TrackBar5.Margin = New System.Windows.Forms.Padding(6)
        Me.TrackBar5.Maximum = 100
        Me.TrackBar5.Name = "TrackBar5"
        Me.TrackBar5.Size = New System.Drawing.Size(566, 90)
        Me.TrackBar5.TabIndex = 11
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.TextBox3.Location = New System.Drawing.Point(978, 54)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(6)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(120, 41)
        Me.TextBox3.TabIndex = 10
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TrackBar4
        '
        Me.TrackBar4.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TrackBar4.Location = New System.Drawing.Point(396, 48)
        Me.TrackBar4.Margin = New System.Windows.Forms.Padding(6)
        Me.TrackBar4.Maximum = 100
        Me.TrackBar4.Name = "TrackBar4"
        Me.TrackBar4.Size = New System.Drawing.Size(566, 90)
        Me.TrackBar4.TabIndex = 11
        '
        'TbNbShare
        '
        Me.TbNbShare.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TbNbShare.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbNbShare.Location = New System.Drawing.Point(385, 410)
        Me.TbNbShare.Margin = New System.Windows.Forms.Padding(6)
        Me.TbNbShare.Name = "TbNbShare"
        Me.TbNbShare.Size = New System.Drawing.Size(552, 44)
        Me.TbNbShare.TabIndex = 1
        '
        'TbTotalLotDisp
        '
        Me.TbTotalLotDisp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TbTotalLotDisp.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbTotalLotDisp.Location = New System.Drawing.Point(2324, 732)
        Me.TbTotalLotDisp.Margin = New System.Windows.Forms.Padding(6)
        Me.TbTotalLotDisp.Name = "TbTotalLotDisp"
        Me.TbTotalLotDisp.ReadOnly = True
        Me.TbTotalLotDisp.Size = New System.Drawing.Size(304, 44)
        Me.TbTotalLotDisp.TabIndex = 1
        Me.TbTotalLotDisp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox5
        '
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(412, 733)
        Me.TextBox5.Margin = New System.Windows.Forms.Padding(6)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(562, 44)
        Me.TextBox5.TabIndex = 1
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Crimson
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(1982, 732)
        Me.Button2.Margin = New System.Windows.Forms.Padding(6)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(84, 52)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "OK"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Crimson
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Button1.Location = New System.Drawing.Point(2338, 256)
        Me.Button1.Margin = New System.Windows.Forms.Padding(6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(290, 90)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Annuler"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'BtValider
        '
        Me.BtValider.BackColor = System.Drawing.Color.Crimson
        Me.BtValider.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtValider.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtValider.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold)
        Me.BtValider.Location = New System.Drawing.Point(2338, 121)
        Me.BtValider.Margin = New System.Windows.Forms.Padding(6)
        Me.BtValider.Name = "BtValider"
        Me.BtValider.Size = New System.Drawing.Size(290, 90)
        Me.BtValider.TabIndex = 2
        Me.BtValider.Text = "Enregistrer"
        Me.BtValider.UseVisualStyleBackColor = False
        '
        'TbBronze
        '
        Me.TbBronze.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TbBronze.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbBronze.Location = New System.Drawing.Point(210, 183)
        Me.TbBronze.Name = "TbBronze"
        Me.TbBronze.Size = New System.Drawing.Size(339, 44)
        Me.TbBronze.TabIndex = 1
        '
        'TextBox6
        '
        Me.TextBox6.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.TextBox6.Location = New System.Drawing.Point(488, 142)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(61, 32)
        Me.TextBox6.TabIndex = 1
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Tb_etat_silver
        '
        Me.Tb_etat_silver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Tb_etat_silver.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tb_etat_silver.Location = New System.Drawing.Point(660, 184)
        Me.Tb_etat_silver.Name = "Tb_etat_silver"
        Me.Tb_etat_silver.ReadOnly = True
        Me.Tb_etat_silver.Size = New System.Drawing.Size(80, 44)
        Me.Tb_etat_silver.TabIndex = 1
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(36, 4)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(83, 19)
        Me.Label15.TabIndex = 3
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(46, 3)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(60, 19)
        Me.Label14.TabIndex = 3
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(30, 3)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(33, 19)
        Me.Label18.TabIndex = 3
        '
        'tbNomLotBronze
        '
        Me.tbNomLotBronze.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNomLotBronze.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNomLotBronze.Location = New System.Drawing.Point(213, 85)
        Me.tbNomLotBronze.Name = "tbNomLotBronze"
        Me.tbNomLotBronze.Size = New System.Drawing.Size(336, 44)
        Me.tbNomLotBronze.TabIndex = 8
        Me.tbNomLotBronze.Visible = False
        '
        'tb_bronze_mat
        '
        Me.tb_bronze_mat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tb_bronze_mat.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bronze_mat.Location = New System.Drawing.Point(213, 43)
        Me.tb_bronze_mat.Name = "tb_bronze_mat"
        Me.tb_bronze_mat.ReadOnly = True
        Me.tb_bronze_mat.Size = New System.Drawing.Size(120, 44)
        Me.tb_bronze_mat.TabIndex = 8
        Me.tb_bronze_mat.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(17, 3)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(118, 19)
        Me.Label13.TabIndex = 3
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(29, 3)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(94, 19)
        Me.Label12.TabIndex = 3
        '
        'dtg_lot_bronze
        '
        Me.dtg_lot_bronze.AllowUserToAddRows = False
        Me.dtg_lot_bronze.AllowUserToDeleteRows = False
        Me.dtg_lot_bronze.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtg_lot_bronze.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dtg_lot_bronze.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtg_lot_bronze.DefaultCellStyle = DataGridViewCellStyle7
        Me.dtg_lot_bronze.Location = New System.Drawing.Point(16, 57)
        Me.dtg_lot_bronze.Name = "dtg_lot_bronze"
        Me.dtg_lot_bronze.ReadOnly = True
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtg_lot_bronze.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dtg_lot_bronze.RowHeadersWidth = 82
        Me.dtg_lot_bronze.Size = New System.Drawing.Size(772, 215)
        Me.dtg_lot_bronze.TabIndex = 0
        '
        'Panel14
        '
        Me.Panel14.BackColor = System.Drawing.Color.Transparent
        Me.Panel14.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel14.Location = New System.Drawing.Point(59, 136)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(155, 27)
        Me.Panel14.TabIndex = 6
        '
        'Panel13
        '
        Me.Panel13.BackColor = System.Drawing.Color.Transparent
        Me.Panel13.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel13.Location = New System.Drawing.Point(59, 183)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(155, 27)
        Me.Panel13.TabIndex = 6
        '
        'Panel16
        '
        Me.Panel16.BackColor = System.Drawing.Color.Transparent
        Me.Panel16.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel16.Location = New System.Drawing.Point(555, 183)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(106, 27)
        Me.Panel16.TabIndex = 6
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.Transparent
        Me.Panel12.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel12.Location = New System.Drawing.Point(59, 42)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(155, 27)
        Me.Panel12.TabIndex = 10
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.Transparent
        Me.Panel11.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel11.Location = New System.Drawing.Point(59, 85)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(155, 27)
        Me.Panel11.TabIndex = 9
        '
        'Button11
        '
        Me.Button11.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bg_bt_save
        Me.Button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.White
        Me.Button11.Location = New System.Drawing.Point(833, 94)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(98, 49)
        Me.Button11.TabIndex = 5
        Me.Button11.Text = "Ajouter"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bg_bt_save
        Me.Button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.ForeColor = System.Drawing.Color.White
        Me.Button10.Location = New System.Drawing.Point(833, 175)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(98, 49)
        Me.Button10.TabIndex = 5
        Me.Button10.Text = "Supprimer"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'setting
        '
        Me.AcceptButton = Me.BtValider
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkSlateGray
        Me.ClientSize = New System.Drawing.Size(2708, 1410)
        Me.Controls.Add(Me.TabControl1)
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "setting"
        Me.Text = "Tableau de Bord"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.Panel34.ResumeLayout(False)
        Me.Panel34.PerformLayout()
        Me.Panel35.ResumeLayout(False)
        Me.Panel35.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel33.ResumeLayout(False)
        Me.Panel33.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.TrackBar3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.pb_photo_loto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel23.ResumeLayout(False)
        Me.Panel23.PerformLayout()
        Me.Panel22.ResumeLayout(False)
        Me.Panel22.PerformLayout()
        Me.Panel19.ResumeLayout(False)
        Me.Panel19.PerformLayout()
        Me.Panel26.ResumeLayout(False)
        Me.Panel26.PerformLayout()
        Me.Panel20.ResumeLayout(False)
        Me.Panel20.PerformLayout()
        CType(Me.dtg_lot, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel24.ResumeLayout(False)
        Me.Panel24.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.Panel28.ResumeLayout(False)
        Me.Panel28.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.Dg_ListJack, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel29.ResumeLayout(False)
        Me.Panel29.PerformLayout()
        CType(Me.TrackBar7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBar6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel25.ResumeLayout(False)
        Me.Panel25.PerformLayout()
        Me.Panel21.ResumeLayout(False)
        Me.Panel21.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        Me.Panel27.ResumeLayout(False)
        Me.Panel27.PerformLayout()
        CType(Me.TrackBar5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TrackBar4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtg_lot_bronze, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtValider As System.Windows.Forms.Button
    Friend WithEvents TrackBar1 As System.Windows.Forms.TrackBar
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents montantLot_Tb As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbNomLot As System.Windows.Forms.TextBox
    Friend WithEvents dtg_lot As DataGridView
    Friend WithEvents Button8 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents tb_gold_mat As TextBox
    Friend WithEvents Panel17 As Panel
    Friend WithEvents Label19 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents Panel20 As Panel
    Friend WithEvents Label22 As Label
    Friend WithEvents Panel19 As Panel
    Friend WithEvents Label21 As Label
    Friend WithEvents tb_id_jack As TextBox
    Friend WithEvents Tb_id_lot As TextBox
    Friend WithEvents TbBronze As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents Tb_etat_silver As TextBox
    Friend WithEvents Panel14 As Panel
    Friend WithEvents Label15 As Label
    Friend WithEvents Panel13 As Panel
    Friend WithEvents Label14 As Label
    Friend WithEvents Panel16 As Panel
    Friend WithEvents Label18 As Label
    Friend WithEvents tbNomLotBronze As TextBox
    Friend WithEvents tb_bronze_mat As TextBox
    Friend WithEvents Panel12 As Panel
    Friend WithEvents Label13 As Label
    Friend WithEvents Panel11 As Panel
    Friend WithEvents Label12 As Label
    Friend WithEvents Button11 As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents dtg_lot_bronze As DataGridView
    Friend WithEvents Tb_etat_lot As TextBox
    Friend WithEvents Panel22 As Panel
    Friend WithEvents Label24 As Label
    Friend WithEvents Panel23 As Panel
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents pb_photo_loto As PictureBox
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents Panel26 As Panel
    Friend WithEvents Label28 As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents Panel24 As Panel
    Friend WithEvents Label26 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TrackBar2 As TrackBar
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents TrackBar3 As TrackBar
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents Panel27 As Panel
    Friend WithEvents Label30 As Label
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TrackBar4 As TrackBar
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label7 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents DpFin As DateTimePicker
    Friend WithEvents DpDebut As DateTimePicker
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents Button2 As Button
    Friend WithEvents Panel8 As Panel
    Friend WithEvents Label16 As Label
    Friend WithEvents TbNbShare As TextBox
    Friend WithEvents Panel15 As Panel
    Friend WithEvents Label17 As Label
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents TrackBar5 As TrackBar
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Panel18 As Panel
    Friend WithEvents Label25 As Label
    Friend WithEvents nom_pv_tb As TextBox
    Friend WithEvents CheckBox2 As CheckBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents TrackBar6 As TrackBar
    Friend WithEvents Panel21 As Panel
    Friend WithEvents Label27 As Label
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents TrackBar7 As TrackBar
    Friend WithEvents Panel25 As Panel
    Friend WithEvents Label29 As Label
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents Dg_ListJack As DataGridView
    Friend WithEvents Label31 As Label
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Panel28 As Panel
    Friend WithEvents Label32 As Label
    Friend WithEvents TbTotalLotDisp As TextBox
    Friend WithEvents GroupBox10 As GroupBox
    Friend WithEvents Panel34 As Panel
    Friend WithEvents Label37 As Label
    Friend WithEvents tbIron_max As TextBox
    Friend WithEvents Panel35 As Panel
    Friend WithEvents Label38 As Label
    Friend WithEvents tbIron_min As TextBox
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents Panel10 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents tSilver_max As TextBox
    Friend WithEvents Panel33 As Panel
    Friend WithEvents Label11 As Label
    Friend WithEvents tSilver_min As TextBox
    Friend WithEvents Panel9 As Panel
    Friend WithEvents videoLot_lb As Label
    Friend WithEvents videoLot_tb As TextBox
    Friend WithEvents Button3 As Button
    Friend WithEvents Panel29 As Panel
    Friend WithEvents chk_jack_ca As CheckBox
End Class
