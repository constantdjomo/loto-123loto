﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports System.Globalization
Imports System.Windows.Forms.VisualStyles.VisualStyleElement
Imports System.Deployment.Application

Public Class Connexion
    Private oUser As BL_Lotto.BlUser
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

        Dim rep, id_pv_rep, user_type, bookmaker_id, spli() As String
        CONNECTED_USERID = ""
        CONNECTED_USERNAME = ""

        Try
            rep = Ocaisse.UserCOnnect(txtUsername.Text.Trim, txtPassword.Text.Trim, secret.Text.Trim.ToUpper)
            spli = rep.Split(CChar("/"))
            If spli.Count > 1 Then
                CONNECTED_USERID = spli(0)
                CONNECTED_USERNAME = spli(1)
                id_pv_rep = spli(2)
                user_type = spli(3) ' 3 for player 
                bookmaker_id = spli(4)
                If {"1", "0"}.Contains(user_type) Then
                    If CONNECTED_USERID = "0" Then
                        MessageBox.Show("Compte Admin Invalide!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        txtUsername.Focus()
                    Else
                        My.Settings.BOOKMAKER_CREDENT = secret.Text.Trim.ToUpper
                        My.Settings.ID_BOOKMAKER = bookmaker_id
                        My.Settings.ID_PV = id_pv_rep
                        My.Settings.Save()
                        setting.Show()
                        Me.Close()
                    End If
                Else
                    MessageBox.Show("Compte Admin Invalide!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    txtUsername.Focus()
                End If

            Else
                MessageBox.Show("Utilisateur Invalide!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtUsername.Focus()

            End If

        Catch ex As Exception
            MessageBox.Show("Impossible de vous connecter! verifiez Votre Connexion", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Dispose()

    End Sub

    Private Sub Connexion_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            gererConexionCaisse()
        Catch ex As Exception
            MessageBox.Show("Erreur lors de la tentative de connexion au serveur. Vérifiez que votre connexion Internet : Paramettre de connexion:. " & SERVEUR & " : " & PORT, "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        initialiseSetting()
        checkupdate()
    End Sub

    Private Sub gererConexionCaisse()

        'Dim ip As String = Hostname2IP(My.Settings.HOST)
        Dim ip As String = SERVEUR

        MonSocketCaisse = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp) 'Initialise le socket
        'MsgBox(ip)
        Try
            Dim MonEP As IPEndPoint = New IPEndPoint(IPAddress.Parse(ip), CInt(PORT)) 'Entre les informations de connexion
            MonSocketCaisse.Connect(MonEP) 'Tente de se connecter
            Ocaisse = New Client(MonSocketCaisse)

        Catch ex As Exception
            MessageBox.Show("Erreur lors de la tentative de connexion au serveur. Vérifiez l'ip et le port du serveur." & ex.ToString, "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub checkupdate()
        Dim info As UpdateCheckInfo = Nothing

        If (ApplicationDeployment.IsNetworkDeployed) Then
            Dim AD As ApplicationDeployment = ApplicationDeployment.CurrentDeployment

            Try
                info = AD.CheckForDetailedUpdate()
            Catch dde As DeploymentDownloadException
                MessageBox.Show("The new version of the application cannot be downloaded at this time. " + ControlChars.Lf & ControlChars.Lf & "Please check your network connection, or try again later. Error: " + dde.Message)
                Return
            Catch ioe As InvalidOperationException
                MessageBox.Show("This application cannot be updated. It is likely not a ClickOnce application. Error: " & ioe.Message)
                Return
            End Try

            If (info.UpdateAvailable) Then
                Dim doUpdate As Boolean = True

                If (Not info.IsUpdateRequired) Then
                    Dim dr As DialogResult = MessageBox.Show("An update is available. Would you like to update the application now?", "Update Available", MessageBoxButtons.OKCancel)
                    If (Not System.Windows.Forms.DialogResult.OK = dr) Then
                        doUpdate = False
                    End If
                Else
                    ' Display a message that the app MUST reboot. Display the minimum required version.
                    MessageBox.Show("This application has detected a mandatory update from your current " &
                    "version to version " & info.MinimumRequiredVersion.ToString() &
                    ". The application will now install the update and restart.",
                    "Update Available", MessageBoxButtons.OK,
                    MessageBoxIcon.Information)
                End If

                If (doUpdate) Then
                    Try
                        AD.Update()
                        MessageBox.Show("The application has been upgraded, and will now restart.")
                        Application.Restart()
                    Catch dde As DeploymentDownloadException
                        MessageBox.Show("Cannot install the latest version of the application. " & ControlChars.Lf & ControlChars.Lf & "Please check your network connection, or try again later.")
                        Return
                    End Try
                End If
            End If
        End If


    End Sub
End Class
