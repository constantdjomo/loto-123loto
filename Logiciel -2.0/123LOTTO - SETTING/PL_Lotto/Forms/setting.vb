﻿Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing
Imports System
Imports System.Globalization
Imports System.Deployment.Application

Public Class setting
#Region "bitmaps"

    Private oImage As Byte() = {
        &H42, &H4D, &HC6, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H76, &H0, &H0, &H0, &H28, &H0,
        &H0, &H0, &HB, &H0, &H0, &H0, &HA, &H0,
        &H0, &H0, &H1, &H0, &H4, &H0, &H0, &H0,
        &H0, &H0, &H50, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H0, &H0, &H10, &H0,
        &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H80, &H0, &H0, &H80,
        &H0, &H0, &H0, &H80, &H80, &H0, &H80, &H0,
        &H0, &H0, &H80, &H0, &H80, &H0, &H80, &H80,
        &H0, &H0, &HC0, &HC0, &HC0, &H0, &H80, &H80,
        &H80, &H0, &H0, &H0, &HFF, &H0, &H0, &HFF,
        &H0, &H0, &H0, &HFF, &HFF, &H0, &HFF, &H0,
        &H0, &H0, &HFF, &H0, &HFF, &H0, &HFF, &HFF,
        &H0, &H0, &HFF, &HFF, &HFF, &H0, &HFF, &HFF,
        &H0, &HF, &HFF, &HF0, &H0, &H0, &HFF, &H0,
        &HFF, &HF0, &HF, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HF, &HFF,
        &HFF, &HFF, &HFF, &H0, &H0, &H0, &HF, &HFF,
        &HFF, &HFF, &HFF, &H0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HFF, &H0,
        &HFF, &HF0, &HF, &HF0, &H0, &H0, &HFF, &HFF,
        &H0, &HF, &HFF, &HF0, &H0, &H0}

    Private xImage As Byte() = {
        &H42, &H4D, &HC6, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H76, &H0, &H0, &H0, &H28, &H0,
        &H0, &H0, &HB, &H0, &H0, &H0, &HA, &H0,
        &H0, &H0, &H1, &H0, &H4, &H0, &H0, &H0,
        &H0, &H0, &H50, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H0, &H0, &H10, &H0,
        &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H80, &H0, &H0, &H80,
        &H0, &H0, &H0, &H80, &H80, &H0, &H80, &H0,
        &H0, &H0, &H80, &H0, &H80, &H0, &H80, &H80,
        &H0, &H0, &HC0, &HC0, &HC0, &H0, &H80, &H80,
        &H80, &H0, &H0, &H0, &HFF, &H0, &H0, &HFF,
        &H0, &H0, &H0, &HFF, &HFF, &H0, &HFF, &H0,
        &H0, &H0, &HFF, &H0, &HFF, &H0, &HFF, &HFF,
        &H0, &H0, &HFF, &HFF, &HFF, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HFF, &HF,
        &HFF, &HFF, &HF, &HF0, &H0, &H0, &HFF, &HF0,
        &HFF, &HF0, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HF, &HF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HF, &HF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HF, &HF, &HFF, &HF0, &H0, &H0, &HFF, &HF0,
        &HFF, &HF0, &HFF, &HF0, &H0, &H0, &HFF, &HF,
        &HFF, &HFF, &HF, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0}

    Private blankImage As Byte() = {
        &H42, &H4D, &HC6, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H76, &H0, &H0, &H0, &H28, &H0,
        &H0, &H0, &HB, &H0, &H0, &H0, &HA, &H0,
        &H0, &H0, &H1, &H0, &H4, &H0, &H0, &H0,
        &H0, &H0, &H50, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H0, &H0, &H10, &H0,
        &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H80, &H0, &H0, &H80,
        &H0, &H0, &H0, &H80, &H80, &H0, &H80, &H0,
        &H0, &H0, &H80, &H0, &H80, &H0, &H80, &H80,
        &H0, &H0, &HC0, &HC0, &HC0, &H0, &H80, &H80,
        &H80, &H0, &H0, &H0, &HFF, &H0, &H0, &HFF,
        &H0, &H0, &H0, &HFF, &HFF, &H0, &HFF, &H0,
        &H0, &H0, &HFF, &H0, &HFF, &H0, &HFF, &HFF,
        &H0, &H0, &HFF, &HFF, &HFF, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0}
#End Region
    Private x As Bitmap = New Bitmap(New MemoryStream(xImage))
    Private o As Bitmap = New Bitmap(New MemoryStream(oImage))

    Private blank As Bitmap = New Bitmap(New MemoryStream(blankImage))
    Private bitmapPadding As Integer = 6

    Private ojacpot As BL_Lotto.BlJackot
    Dim Coleu As Drawing.Color
    Public is_material_gold As Integer = 1
    Public is_material_silver As Integer = 1
    Public schow_etat As Integer = 1
    Public status As Integer = 1
    Public chargemen_g, share_w As Integer
    Public list_radio_bouton As New List(Of RadioButton)
    Private oDataSet, oDataSet1 As Data.DataSet
    'Private oDataSet, oDataSet1, dstjack1, dstjack2, dstjack3 As Data.DataSet
    Private ojackpotDataSet As DataSet
    Private Selectsilver, Selectbronze As String
    Private SelectedLot, SelectPV As String
    Dim scr As Screen() = Screen.AllScreens
    Private selected_pv As String = "0"

    Private Sub BtValider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtValider.Click
        If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok AndAlso bonPourcent() Then

            DIFICULT_LEVEL = TrackBar1.Value

            SILVER_POT = TrackBar2.Value

            GOLD_POT = TrackBar3.Value

            IRON_POT = TrackBar4.Value

            CAGNOTE_POT = TrackBar5.Value

            MSTERIOUS_POT = TrackBar6.Value

            MEGA_POT = TrackBar7.Value
            ' Jacpot in CA
            If chk_jack_ca.Checked Then
                DEDUCT_JACK_TO_TURN_OVER = 1
            Else
                DEDUCT_JACK_TO_TURN_OVER = 0
            End If


            MIN_GAIIN_SILVER = tSilver_min.Text
            MAX_GAIIN_SILVER = tSilver_max.Text

            MAX_GAIN_IRON = tbIron_max.Text
            MIN_GAIN_IRON = tbIron_min.Text

            'DELAI_JEU = Tb_delaijeu.Text

            NB_SHARER = CInt(TbNbShare.Text)
            'MsgBox(CStr(getScreenNumber(Me.cmbDevices.Text)))

            PersistSetting()

        End If
    End Sub

    Private Function getScreenNumber(ByVal DeviceID As String) As Integer
        Dim i As Integer = 0
        For Each s As Screen In scr
            'MsgBox("device " & s.DeviceName)
            'MsgBox("selected " & "\\.\" & DeviceID)
            If (s.DeviceName = "\\.\" & DeviceID) Then Return i
            i += 1
        Next

        'if cannot find the device reset to the default 0
        Return 0
    End Function



    Private Sub setting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'initialiseSetting()

        If Not Ocaisse.isActivated() Then
            MessageBox.Show("Attention votre Credit est expiré vos fonctionalités seront desormais limités!", "Credit", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If


        ' MIN_LOTO_PLASMA, MIN_PARI_JACKPOT   ces variables sont pas encore gerer sur l'interface

        TrackBar1.Value = DIFICULT_LEVEL
        TextBox1.Text = DIFICULT_LEVEL & " %"



        'L#argent c'est l'argent
        TrackBar2.Value = SILVER_POT
        TextBox2.Text = SILVER_POT & " %"

        tSilver_min.Text = MIN_GAIIN_SILVER
        tSilver_max.Text = MAX_GAIIN_SILVER

        ' Gold Jackpot
        TrackBar3.Value = GOLD_POT
        TextBox4.Text = GOLD_POT & " %"

        ' Mini Jackpot
        TrackBar4.Value = IRON_POT
        TextBox3.Text = IRON_POT & " %"

        tbIron_max.Text = MAX_GAIN_IRON
        tbIron_min.Text = MIN_GAIN_IRON

        'Lot a gogo
        TrackBar5.Value = CAGNOTE_POT
        TextBox7.Text = CAGNOTE_POT & " %"

        ' Loto Mystere
        TrackBar6.Value = MSTERIOUS_POT
        TextBox8.Text = MSTERIOUS_POT & " %"

        'Loto Moto
        TrackBar7.Value = MEGA_POT
        TextBox9.Text = MEGA_POT & " %"

        ' Jacpot in CA
        If (CBool(DEDUCT_JACK_TO_TURN_OVER)) Then
            chk_jack_ca.Checked = True
        Else
            chk_jack_ca.Checked = False
        End If

        TbNbShare.Text = NB_SHARER

        chergergrid_Lot("0")


        Using oticket As New BL_Lotto.BlTicket(source)

            Dim pv As New DataSet
            pv = oticket.Getsalle(My.Settings.ID_BOOKMAKER)

            Dim Myradio As New RadioButton

            For i As Integer = 0 To pv.Tables("point_vente").Rows.Count - 1

                Myradio = New RadioButton
                With Myradio
                    .Text = pv.Tables("point_vente").Rows(i).Item("nom_pv") & "#" & pv.Tables("point_vente").Rows(i).Item("id_pv")
                    .Size = New Drawing.Size(155, 40)
                    .Location = New Point(10 + (i * 155), 30)
                    .FlatStyle = FlatStyle.Flat
                    .Appearance = Appearance.Button

                End With
                AddHandler Myradio.CheckedChanged, AddressOf Myradio_CheckedChanged

                GroupBox6.Controls.Add(Myradio)
                list_radio_bouton.Add(Myradio)

            Next


        End Using

        DpDebut.Value = Date.Now
        DpFin.Value = Date.Now


    End Sub

    Friend Sub Myradio_CheckedChanged(sender As Object, e As EventArgs)
        'Write code here.
        Dim d As New RadioButton
        Dim te As String()
        d = CType(sender, RadioButton)
        If d.Checked Then

            te = d.Text.Split(CChar("#"))
            selected_pv = te(1)
            chergergrid_Lot(selected_pv)
            'MsgBox(selected_pv)
        End If

    End Sub

    'Private Sub chergergrid_silver()
    '    Using ojacpot As New BL_Lotto.BlJackot(source)
    '        Try
    '            With dtg_lot_silver


    '                'TODO gerer l'interface de setting de la salle1 
    '                'oDataSet = ojacpot.GetListlot((ID_SILVER))

    '                .DataSource = Nothing
    '                .Columns.Clear()
    '                .Rows.Clear()

    '                .DataSource = oDataSet.Tables("jackpot")

    '                .SelectionMode = DataGridViewSelectionMode.FullRowSelect
    '                'Empêche les modifications de lignes, colonnes, l'ajout, la suppression
    '                .AllowUserToAddRows = False
    '                .AllowUserToDeleteRows = False
    '                .AllowUserToOrderColumns = False
    '                .AllowUserToResizeColumns = False
    '                .AllowUserToResizeRows = False

    '                'masquer la premiere colonne  `id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp`FROM `lot_jackpot`
    '                .Columns(0).HeaderText = "id"
    '                .Columns(0).Visible = False
    '                'empeche les colonne d'etre reordonner
    '                .Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable


    '                'Nomme les colonnes (en têtes)
    '                .Columns(1).HeaderText = "Materiel 0=non 1=oui"
    '                'empeche les colonne d'etre reordonner
    '                .Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable

    '                'Nomme les colonnes (en têtes)
    '                .Columns(2).HeaderText = "Nom Materiel"

    '                'Nomme les colonnes (en têtes)
    '                .Columns(3).HeaderText = "Montant/Valeur"


    '                Dim imageColumn As New DataGridViewImageColumn


    '                Dim unMarked As Bitmap = blank
    '                imageColumn = New DataGridViewImageColumn()

    '                ' Add twice the padding for the left and 
    '                ' right sides of the cell.
    '                imageColumn.Width = x.Width + 2 * bitmapPadding + 1

    '                imageColumn.Image = unMarked
    '                imageColumn.ImageLayout = DataGridViewImageCellLayout.NotSet
    '                imageColumn.Description = "default image layout"
    '                imageColumn.HeaderText = "default"

    '                '.Columns.Insert(.ColumnCount, imageColumn)
    '                .Columns.Insert(0, imageColumn)
    '                dstjack1 = ojacpot.GetJackpot(ID_SILVER)

    '                Dim Img As New DataGridViewImageCell
    '                Img.Value = My.Resources.star_128

    '                For j As Integer = 0 To .Rows.Count - 1

    '                    If (.Item(1, j).Value.ToString = dstjack1.Tables("jackpot").Rows(0).Item("id_lot_encour").ToString) Then
    '                        Dim cell As DataGridViewImageCell = CType(.Rows(j).Cells(0), DataGridViewImageCell)
    '                        cell.Value = x
    '                    End If
    '                Next

    '            End With

    '        Catch ExceptionErr As Exception
    '            MessageBox.Show(ExceptionErr.Message.ToString)
    '        End Try
    '    End Using
    '    If dtg_lot_silver.Rows.Count <= 0 Then Exit Sub
    '    Selectsilver = CStr(dtg_lot_silver.Rows(0).Cells(1).Value)
    'End Sub



    Private Sub chergergrid_Lot(ByVal id_sal As String)

        Using ojacpot As New BL_Lotto.BlJackot(source)
            Try

                With dtg_lot
                    If id_sal <> "0" Then
                        oDataSet = ojacpot.GetListlotBookmaker(My.Settings.ID_BOOKMAKER, id_sal)
                    Else
                        oDataSet = ojacpot.GetListlot(My.Settings.ID_BOOKMAKER)
                    End If
                    oDataSet1 = ojacpot.GetListPointVente(My.Settings.ID_BOOKMAKER)



                    .DataSource = Nothing
                    .Columns.Clear()
                    .Rows.Clear()
                    .DataSource = oDataSet.Tables("jackpot")
                    .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                    'Empêche les modifications de lignes, colonnes, l'ajout, la suppression
                    .AllowUserToAddRows = False
                    .AllowUserToDeleteRows = False
                    .AllowUserToOrderColumns = False
                    .AllowUserToResizeColumns = False
                    .AllowUserToResizeRows = False
                    .DefaultCellStyle.Font = New Font(.Font.FontFamily, 9.25, FontStyle.Bold)


                    ''Une ligne sur 2 en bleue
                    '.RowsDefaultCellStyle.BackColor = Color.White
                    '.AlternatingRowsDefaultCellStyle.BackColor = Color.Gold

                    'masquer la premiere colonne  `id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp`FROM `lot_jackpot`
                    .Columns(0).HeaderText = "Id"
                    .Columns(0).Visible = False
                    'empeche les colonne d'etre reordonner
                    .Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable

                    'Nomme les colonnes (en têtes)
                    .Columns(1).HeaderText = "Type"
                    'masquer la premiere colonne
                    .Columns(1).Visible = False

                    'Nomme les colonnes (en têtes)
                    .Columns(2).HeaderText = "point de vente"
                    'masquer la premiere colonne
                    .Columns(2).Visible = False

                    'Nomme les colonnes (en têtes)
                    .Columns(3).HeaderText = "Materiel 0=non 1=oui"
                    'empeche les colonne d'etre reordonner
                    .Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable

                    'Nomme les colonnes (en têtes)
                    .Columns(4).HeaderText = "Nom Materiel"

                    'Nomme les colonnes (en têtes)
                    .Columns(5).HeaderText = "Montant/Valeur"

                    '`id_lot` `id_jack` `id_pv` `is_matirial` `nom_materiel` `montant_jackp` `photo` `etat_jackp` `status` share

                    .Columns(6).Visible = False
                    .Columns(9).Visible = False

                    Dim imageColumn As New DataGridViewImageColumn
                    '.Columns.Insert(2, chk)
                    'chk.HeaderText = "Statut"
                    'chk.Name = "default"

                    Dim unMarked As Bitmap = blank
                    imageColumn = New DataGridViewImageColumn()

                    ' Add twice the padding for the left and 
                    ' right sides of the cell.
                    imageColumn.Width = x.Width + 2 * bitmapPadding + 1

                    imageColumn.Image = unMarked
                    imageColumn.ImageLayout = DataGridViewImageCellLayout.NotSet
                    imageColumn.Description = "default image layout"
                    imageColumn.HeaderText = "Status"

                    .Columns.Insert(.ColumnCount, imageColumn)

                    'dstjack1 = ojacpot.GetJackpot(ID_GOLD)
                    'dstjack2 = ojacpot.GetJackpot(ID_IRON)
                    'dstjack3 = ojacpot.GetJackpot(ID_SILVER)

                    For j As Integer = 0 To .Rows.Count - 1
                        'MsgBox(.Item(1, j).Value.ToString)
                        If (.Item(1, j).Value.ToString = ID_GOLD) Then
                            .Rows(j).DefaultCellStyle.BackColor = Color.Gold
                        ElseIf (.Item(1, j).Value.ToString = ID_IRON) Then
                            .Rows(j).DefaultCellStyle.BackColor = Color.Silver
                        ElseIf (.Item(1, j).Value.ToString = ID_SILVER) Then
                            .Rows(j).DefaultCellStyle.BackColor = Color.Brown
                        ElseIf (.Item(1, j).Value.ToString = ID_MSTERIOUS) Then
                            .Rows(j).DefaultCellStyle.BackColor = Color.Red
                        ElseIf (.Item(1, j).Value.ToString = ID_MEGA) Then
                            .Rows(j).DefaultCellStyle.BackColor = Color.Black
                            .Rows(j).DefaultCellStyle.ForeColor = Color.White
                        End If
                    Next

                    Dim Img As New DataGridViewImageCell
                    Img.Value = My.Resources.star_128

                    For j As Integer = 0 To .Rows.Count - 1

                        If (.Item(9, j).Value.ToString = "1") Then
                            Dim cell As DataGridViewImageCell = CType(.Rows(j).Cells(.ColumnCount - 1), DataGridViewImageCell)
                            cell.Value = x
                        End If
                    Next

                End With

            Catch ExceptionErr As Exception
                MessageBox.Show(ExceptionErr.Message.ToString)
            End Try
        End Using
    End Sub


    Private Sub TrackBar1_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBar1.Scroll
        TextBox1.Text = TrackBar1.Value & " %"
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    'Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If (Tbsilver.Text <> "" And Tb_etat_bonze.Text <> "" And IsNumeric(Tb_etat_bonze.Text.Trim) And IsNumeric(Tbsilver.Text.Trim) And IsNumeric(Tb_min_jack.Text.Trim) And IsNumeric(Tb_max_jack.Text.Trim) And bonPourcent()) Then
    '        If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
    '            Using ojacpot As New BL_Lotto.BlJackot(source)
    '                'MsgBox(CBool(is_material_silver))
    '                'ojacpot.Maj_mon_Jackpot(ID_SILVER, Tbsilver.Text.Trim, CBool(is_material_silver), tbNomLotSilver.Text.Trim)

    '                ojacpot.Maj_mon_Jackpot(ID_SILVER, Tb_id_lot_silver.Text.Trim, Tbsilver.Text.Trim, Tb_etat_bonze.Text, is_material_silver, tbNomLotSilver.Text.Trim)
    '                ojacpot.Maj_mon_Lot(Tb_id_lot_silver.Text, Tbsilver.Text.Trim, CBool(is_material_silver), tbNomLotSilver.Text.Trim)

    '                SILVER_POT = CInt(TrackBar2.Value)
    '                MIN_GAIIN_SILVER = CInt(Tb_min_jack.Text)
    '                MAX_GAIIN_SILVER = CInt(Tb_max_jack.Text)
    '                PersistSetting()

    '                MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                chergergrid_silver()
    '            End Using
    '        End If

    '    Else
    '        MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End If
    'End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        'If (tb_id_jack.Text = ID_GOLD) Then

        'MsgBox((share_w <> 1 Or is_material_gold <> 1))

        Dim fname As String = "default.jpg"

        fname = System.IO.Path.GetFileName(pb_photo_loto.ImageLocation)

        Dim fnameVideo As String = "default.avi"

        fnameVideo = System.IO.Path.GetFileName(videoLot_tb.Text)



        If (montantLot_Tb.Text <> "" And Tb_etat_lot.Text <> "" And IsNumeric(montantLot_Tb.Text.Trim) And bonPourcent() And (share_w <> 1 Or is_material_gold <> 1)) Then
            If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then

                If Ocaisse.Maj_mon_Lot_Set(Tb_id_lot.Text, montantLot_Tb.Text.Trim, Tb_etat_lot.Text, CBool(is_material_gold), tbNomLot.Text.Trim, nom_pv_tb.Text, schow_etat, status, fname, fnameVideo) Then
                    MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                'Dim cell As DataGridViewImageCell = CType(dtg_lot_gold.Rows(dtg_lot_gold.CurrentRow.Index).Cells(dtg_lot_gold.ColumnCount - 1), DataGridViewImageCell)
                'If (cell.Value IsNot blank) Then
                '    ojacpot.Maj_mon_Jackpot(ID_GOLD, Tb_id_lot.Text, TbGold.Text.Trim, Tb_etat_gold.Text.Trim, CBool(is_material_gold), tbNomLotGold.Text.Trim)
                '    GOLD_POT = CInt(TrackBar3.Value)
                '    PersistSetting()
                'End If


                chergergrid_Lot(selected_pv)

            End If

        Else
            MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        'ElseIf (tb_id_jack.Text = ID_IRON) Then

        'If (TbGold.Text <> "" And Tb_etat_gold.Text <> "" And IsNumeric(TbGold.Text.Trim) And bonPourcent()) Then
        '        If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
        '            Using ojacpot As New BL_Lotto.BlJackot(source)
        '                ojacpot.Maj_mon_Lot(Tb_id_lot.Text, TbGold.Text.Trim, CBool(is_material_gold), tbNomLotGold.Text.Trim)
        '                Dim cell As DataGridViewImageCell = CType(dtg_lot_gold.Rows(dtg_lot_gold.CurrentRow.Index).Cells(dtg_lot_gold.ColumnCount - 1), DataGridViewImageCell)
        '                If (cell.Value IsNot blank) Then
        '                    ojacpot.Maj_mon_Jackpot(ID_IRON.ToString, Tb_id_lot.Text, TbGold.Text.Trim, Tb_etat_gold.Text.Trim, CBool(is_material_gold), tbNomLotGold.Text.Trim)
        '                    IRON_POT = CInt(TrackBar3.Value)
        '                    PersistSetting()
        '                End If
        '                MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '                chergergrid_Lot()
        '            End Using
        '        End If

        '    Else
        '        MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    End If

        'End If

    End Sub



    Public Function bonPourcent() As Boolean

        Dim pourcent_prevel As Integer

        If chk_jack_ca.Checked Then
            pourcent_prevel = TrackBar1.Value + TrackBar2.Value + TrackBar3.Value + TrackBar4.Value + TrackBar5.Value + TrackBar6.Value + TrackBar7.Value
        Else
            pourcent_prevel = TrackBar1.Value
        End If
        If pourcent_prevel > My.Settings.MAX_PERCENT Then
            Return False
        Else
            Return True
        End If
    End Function

    'Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
    '    If RadioButton1.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton1.BackColor

    '        RadioButton1.BackColor = RadioButton1.ForeColor
    '        RadioButton1.ForeColor = Coleu

    '        is_material_gold = 1
    '        Panel7.Visible = True
    '        tbNomLotGold.Visible = True

    '    Else
    '        Coleu = RadioButton1.BackColor
    '        RadioButton1.BackColor = RadioButton1.ForeColor
    '        RadioButton1.ForeColor = Coleu

    '    End If
    'End Sub

    'Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
    '    If RadioButton2.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton2.BackColor

    '        RadioButton2.BackColor = RadioButton2.ForeColor
    '        RadioButton2.ForeColor = Coleu
    '        is_material_gold = 0
    '        Panel7.Visible = False
    '        tbNomLotGold.Visible = False
    '    Else
    '        Coleu = RadioButton2.BackColor
    '        RadioButton2.BackColor = RadioButton2.ForeColor
    '        RadioButton2.ForeColor = Coleu
    '    End If
    'End Sub

    'Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If RadioButton4.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton4.BackColor

    '        RadioButton4.BackColor = RadioButton4.ForeColor
    '        RadioButton4.ForeColor = Coleu

    '        is_material_silver = 1
    '        Panel8.Visible = True
    '        tbNomLotSilver.Visible = True

    '    Else
    '        Coleu = RadioButton4.BackColor
    '        RadioButton4.BackColor = RadioButton4.ForeColor
    '        RadioButton4.ForeColor = Coleu

    '    End If
    'End Sub

    'Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If RadioButton3.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton3.BackColor

    '        RadioButton3.BackColor = RadioButton3.ForeColor
    '        RadioButton3.ForeColor = Coleu
    '        is_material_silver = 0
    '        Panel8.Visible = False
    '        tbNomLotSilver.Visible = False
    '    Else
    '        Coleu = RadioButton3.BackColor
    '        RadioButton3.BackColor = RadioButton3.ForeColor
    '        RadioButton3.ForeColor = Coleu
    '    End If
    'End Sub

    'Private Sub Button7_Click(sender As Object, e As EventArgs)
    '    If dtg_lot_silver.Rows.Count <= 0 Then Exit Sub
    '    Using ojacpot As New BL_Lotto.BlJackot(source)
    '        Dim cell As DataGridViewImageCell = CType(dtg_lot_silver.Rows(dtg_lot_silver.CurrentRow.Index).Cells(0), DataGridViewImageCell)
    '        If (dtg_lot_silver.Rows(dtg_lot_silver.CurrentRow.Index).Cells(1).Value.ToString = 0) Then
    '            MsgBox("Ce Lot ne peut etre supprimé", MsgBoxStyle.Information, "Warning")
    '            Exit Sub
    '        End If
    '        If (cell.Value Is blank) Then
    '            If MsgBox("Etes-vous sûr de vouloir supprimer le(s) enregistrement(s)?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
    '                ojacpot.suprim_lot_Jackpot(Selectsilver)
    '                chergergrid_silver()
    '            End If
    '        Else
    '            If MsgBox("Vous etes sur le point de supprimer un Lot encours continuer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
    '                ojacpot.suprim_lot_Jackpot(Selectsilver)
    '                'TODO gerer l'interface de stting de la salle
    '                'select_another_lot(ID_SILVER)
    '                chergergrid_silver()
    '            End If
    '        End If

    '    End Using


    'End Sub

    'Private Sub Button6_Click(sender As Object, e As EventArgs)
    '    Dim f As New ajouter_lot
    '    'f.tbid_jck.Text = ID_GOLD
    '    f.cb_type_jack.Items.Clear()
    '    f.cb_type_jack.Items.Add(ID_SILVER & "# LOT")
    '    f.ShowDialog()
    '    chergergrid_silver()
    'End Sub

    'Private Sub TabPage2_Click(sender As Object, e As EventArgs) Handles TabPage2.Click

    'End Sub





    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Dim f As New ajouter_lot
        'f.tbid_jck.Text = ID_GOLD
        f.cb_type_jack.Items.Clear()
        f.cb_type_jack.Items.Add(ID_GOLD & "# Loto Plasma")
        f.cb_type_jack.Items.Add(ID_SILVER & "# Cagnotte")
        f.cb_type_jack.Items.Add(ID_IRON & "# Super Lot")
        f.cb_type_jack.Items.Add(ID_CAGNOTE & "# Lot a Gogo")
        f.cb_type_jack.Items.Add(ID_MSTERIOUS & "# Lot Mystere")

        Using ojacpot As New BL_Lotto.BlJackot(source)

            oDataSet1 = ojacpot.GetListPointVente(My.Settings.ID_BOOKMAKER)

            f.CbPointVente.Items.Clear()
            f.CbPointVente.DataSource = Nothing
            f.CbPointVente.DataSource = oDataSet1.Tables("point_vente")
            f.CbPointVente.DisplayMember = "nom_pv"
            f.CbPointVente.ValueMember = "id_pv"

            'f.CheckBox2.Checked = True

        End Using

        f.ShowDialog()
        chergergrid_Lot(selected_pv)
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Dim id_j, stat As String
        If dtg_lot.Rows.Count <= 0 Then Exit Sub
        Using ojacpot As New BL_Lotto.BlJackot(source)
            Dim cell As DataGridViewImageCell = CType(dtg_lot.Rows(dtg_lot.CurrentRow.Index).Cells(dtg_lot.ColumnCount - 1), DataGridViewImageCell)

            If MsgBox("Etes-vous sûr de vouloir supprimer le(s) enregistrement(s)?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                id_j = CStr(dtg_lot.Rows(dtg_lot.CurrentRow.Index).Cells(1).Value)
                stat = CStr(dtg_lot.Rows(dtg_lot.CurrentRow.Index).Cells(9).Value)

                ojacpot.suprim_lot_Jackpot(SelectedLot)
                MsgBox(stat)
                MsgBox(id_j)
                If stat = "1" Then
                    MsgBox("bien")
                    select_another_lot(id_j)
                End If

                chergergrid_Lot(selected_pv)
            End If

        End Using
    End Sub


    Private Sub dtg_lot_gold_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dtg_lot.CellMouseClick
        If dtg_lot.Rows.Count <= 0 Then Exit Sub
        SelectedLot = CStr(dtg_lot.Rows(dtg_lot.CurrentRow.Index).Cells(0).Value)
    End Sub

    'Private Sub Button13_Click(sender As Object, e As EventArgs)
    '    If (TbBronze.Text <> "" And IsNumeric(TbBronze.Text.Trim) And bonPourcent()) Then
    '        If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
    '            Using ojacpot As New BL_Lotto.BlJackot(source)
    '                'MsgBox(CBool(is_material_silver))
    '                ojacpot.Maj_mon_Jackpot(ID_IRON, TbBronze.Text.Trim, CBool(is_material_bronze), tbNomLotBronze.Text.Trim)
    '                IRON_POT = CInt(TrackBar4.Value)
    '                MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            End Using
    '        End If
    '    Else
    '        MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End If
    'End Sub

    Private Sub TrackBar4_Scroll(sender As Object, e As EventArgs) Handles TrackBar4.Scroll
        TextBox3.Text = TrackBar4.Value & " %"
    End Sub

    Private Sub TextBox3_TextChanged(sender As Object, e As EventArgs)

    End Sub




    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Dim f As New ajouter_lot
        f.tbid_jck.Text = ID_IRON
        f.ShowDialog()
        'chergergrid_bronze()
    End Sub



    Private Sub TrackBar5_Scroll(sender As Object, e As EventArgs) Handles TrackBar5.Scroll
        TextBox7.Text = TrackBar5.Value & " %"
    End Sub

    Private Sub TabPage3_Click(sender As Object, e As EventArgs) Handles TabPage3.Click

    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then ' Teste si le bouton est coché.
            Coleu = CheckBox1.BackColor
            CheckBox1.BackColor = CheckBox1.ForeColor
            CheckBox1.ForeColor = Coleu
            schow_etat = 1

        Else
            Coleu = CheckBox1.BackColor
            CheckBox1.BackColor = CheckBox1.ForeColor
            CheckBox1.ForeColor = Coleu
            schow_etat = 0

        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then ' Teste si le bouton est coché.
            Coleu = CheckBox2.BackColor
            CheckBox2.BackColor = CheckBox2.ForeColor
            CheckBox2.ForeColor = Coleu
            status = 1

        Else
            Coleu = CheckBox2.BackColor
            CheckBox2.BackColor = CheckBox2.ForeColor
            CheckBox2.ForeColor = Coleu
            status = 0

        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged

        If RadioButton1.Checked = True Then ' Teste si le bouton est coché.
            Coleu = RadioButton1.BackColor
            RadioButton1.BackColor = RadioButton1.ForeColor
            RadioButton1.ForeColor = Coleu
            is_material_gold = 1
            Panel7.Visible = True
            tbNomLot.Visible = True

        Else
            Coleu = RadioButton1.BackColor
            RadioButton1.BackColor = RadioButton1.ForeColor
            RadioButton1.ForeColor = Coleu


        End If




    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then ' Teste si le bouton est coché.
            Coleu = RadioButton2.BackColor
            RadioButton2.BackColor = RadioButton2.ForeColor
            RadioButton2.ForeColor = Coleu
            is_material_gold = 0
            Panel7.Visible = False
            tbNomLot.Visible = False

        Else
            Coleu = RadioButton2.BackColor
            RadioButton2.BackColor = RadioButton2.ForeColor
            RadioButton2.ForeColor = Coleu


        End If


    End Sub

    Private Sub pb_photo_loto_Click(sender As Object, e As EventArgs) Handles pb_photo_loto.Click
        'MsgBox(AppDomain.CurrentDomain.BaseDirectory + "photo")
        With OpenFileDialog1
            .Title = "Ouvrir"

            .InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "photo"
            .Filter = "Fichiers images|*.png;*.jpg;*.jpeg"
            .Multiselect = False
            .ValidateNames = True
            .CheckFileExists = True
            .AddExtension = True
        End With

        If MsgBox("Voulez vous modifier l'image du Lot?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then

            If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
                If (OpenFileDialog1.FileName = "") Then
                    MsgBox("veuillez choisir un fichier")
                    Exit Sub
                Else
                    pb_photo_loto.ImageLocation = OpenFileDialog1.FileName
                End If

            End If

        End If


    End Sub

    Private Sub TrackBar6_Scroll(sender As Object, e As EventArgs) Handles TrackBar6.Scroll
        TextBox8.Text = TrackBar6.Value & " %"
    End Sub
    Private Sub TrackBar7_Scroll(sender As Object, e As EventArgs) Handles TrackBar7.Scroll
        TextBox9.Text = TrackBar7.Value & " %"
    End Sub


    Private Sub dtg_lot_gold_Click(sender As Object, e As EventArgs) Handles dtg_lot.Click
        'If DgvNiveau.Rows.Count <= 0 Then Exit Sub
        'SelectMatiere = CStr(DgvNiveau.Rows(DgvNiveau.CurrentRow.Index).Cells(0).Value)
        'SelectcodMat = CStr(DgvNiveau.Rows(DgvNiveau.CurrentRow.Index).Cells(1).Value)

        If dtg_lot.Rows.Count <= 0 Then Exit Sub
        SelectedLot = CStr(dtg_lot.Rows(dtg_lot.CurrentRow.Index).Cells(0).Value)

        chargerLot(SelectedLot)
    End Sub

    Private Sub DataGridView1_Click(sender As Object, e As EventArgs) Handles DataGridView1.Click

        If DataGridView1.Rows.Count <= 0 Then Exit Sub
        SelectPV = CStr(DataGridView1.Rows(DataGridView1.CurrentRow.Index).Cells(0).Value)

        chargerListJack(SelectPV)

    End Sub

    Public Sub chargerListJack(ByVal SelectPv As String)

        Dim TotalmontantJack As Integer = 0
        Dim rest As Integer = 0
        Dim datedebut, datefin As String
        Dim Tabdatedebut, Tabdatefin As String()
        Dim totalLotDis As Integer = 0
        Dim dsTickJackList As DataSet
        Using oCompte As New BL_Lotto.BlCompte(source)

            If DateTime.Compare(DpDebut.Value, DpFin.Value) <= 0 Then

                Tabdatedebut = DpDebut.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
                datedebut = Tabdatedebut(2) & "#" & Tabdatedebut(1) & "#" & Tabdatedebut(0)
                'datedebut = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
                'MsgBox(datedebut)
                Tabdatefin = DpFin.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
                datefin = Tabdatefin(2) & "#" & Tabdatefin(1) & "#" & Tabdatefin(0)


                dsTickJackList = oCompte.getListJackParPv(SelectPv, Tabdatedebut(2) & "/" & Tabdatedebut(1) & "/" & Tabdatedebut(0) & " 00:00:00", Tabdatefin(2) & "/" & Tabdatefin(1) & "/" & Tabdatefin(0) & " 23:59:59")
                'repon = Ocaisse.GetLisJackParPv(SelectPv, datedebut, datefin)

                Dg_ListJack.Rows.Clear()
                'If repon <> "0" Then
                'Dim jackDujour() As String = repon.Split(CChar("/"))

                If (dsTickJackList.Tables("ticket").Rows.Count > 0) Then
                    For h As Integer = 0 To dsTickJackList.Tables("ticket").Rows.Count - 1
                        'Nom du lot , Prix
                        '`id_tick``id_lot``ID_USER_S``is_materiel``nom_materiel``montant` COD_BAR

                        'Code Bar, Nom du Materiel , Montant
                        'dsTickJackList.Tables("ticket").Rows(h).Item("id_tick") & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("COD_BAR") & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("ID_USER_S") & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("is_materiel") & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("nom_materiel") & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("montant")
                        Dim rowgrid As String() = {dsTickJackList.Tables("ticket").Rows(h).Item("COD_BAR"), dsTickJackList.Tables("ticket").Rows(h).Item("nom_materiel"), dsTickJackList.Tables("ticket").Rows(h).Item("montant"), dsTickJackList.Tables("ticket").Rows(h).Item("ID_USER_S")}
                        Dg_ListJack.Rows.Add(rowgrid)
                        totalLotDis += CInt(dsTickJackList.Tables("ticket").Rows(h).Item("montant"))
                    Next

                End If
                TbTotalLotDisp.Text = totalLotDis

                'For h As Integer = 0 To jackDujour.Count - 1

                '        Dim row0 As String() = jackDujour(h).Split(CChar("#"))

                '        'Plus necessaire
                '        'Dim nommat As String = ""
                '        'If CBool(row0(3)) Then
                '        '    nommat = row0(4)
                '        'Else
                '        '    nommat = row0(2)
                '        'End If

                '        'Code Bar, Nom du Materiel , Montant
                '        'dsTickJackList.Tables("ticket").Rows(h).Item("id_tick") & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("COD_BAR") & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("ID_USER_S") & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("is_materiel") & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("nom_materiel") & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("montant")
                '        Dim rowgrid As String() = {row0(1), row0(4), row0(5), row0(6)}
                '        Dg_ListJack.Rows.Add(rowgrid)
                '    Next
                'End If

            Else
                MessageBox.Show("Attention a la plage de Date.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        End Using

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        'MsgBox(AppDomain.CurrentDomain.BaseDirectory + "photo")
        With OpenFileDialog1
            .Title = "Ouvrir"
            .InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "video"
            .Filter = "Fichiers AVI|*.avi"
            .Multiselect = False
            .ValidateNames = True
            .CheckFileExists = True
            .AddExtension = True
        End With

        If MsgBox("Voulez vous modifier la video du Lot?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then

            If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
                If (OpenFileDialog1.FileName = "") Then
                    MsgBox("veuillez choisir un fichier")
                    Exit Sub
                Else
                    videoLot_tb.Text = OpenFileDialog1.FileName
                End If

            End If

        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Public Sub chargerLot(ByVal SelectLot As String)
        Dim Lot() As String

        Using ojacpot As New BL_Lotto.BlJackot(source)

            'id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp`

            Dim rep As String = Ocaisse.Getlot(SelectLot)
            Lot = rep.Split(CChar("/"))

            'montant_jackp & "/" & nom_materiel & "/" & id_jack & "/" & id_lot & "/" & etat_jackp & "/" & photo & "/" & id_pv & "/" & is_matirial & "/" & schow_etat & "/" & status & "/" & video

            montantLot_Tb.Text = Lot(0) 'oDataSet.Tables("lot_jackpot").Rows(0).Item("montant_jackp").ToString
            tbNomLot.Text = Lot(1) 'oDataSet.Tables("lot_jackpot").Rows(0).Item("nom_materiel").ToString
            tb_id_jack.Text = Lot(2) 'oDataSet.Tables("lot_jackpot").Rows(0).Item("id_jack").ToString
            Tb_id_lot.Text = Lot(3) 'oDataSet.Tables("lot_jackpot").Rows(0).Item("id_lot").ToString
            Tb_etat_lot.Text = Lot(4) 'oDataSet.Tables("lot_jackpot").Rows(0).Item("etat_jackp").ToString
            pb_photo_loto.ImageLocation = AppDomain.CurrentDomain.BaseDirectory & "photo\" & Lot(5) 'oDataSet.Tables("lot_jackpot").Rows(0).Item("photo").ToString
            videoLot_tb.Text = Lot(10) 'oDataSet.Tables("lot_jackpot").Rows(0).Item("photo").ToString
            'Tb_etat_gold.ReadOnly = False
            nom_pv_tb.Text = Lot(6)
            'For j As Integer = 0 To list_radio_bouton.Count - 1
            '    te = list_radio_bouton.Item(j).Text.Split(CChar("#"))
            '    If te(1) = Lot(6) Then
            '        list_radio_bouton.Item(j).Checked = True
            '    End If
            'Next


            If (Lot(2) = ID_GOLD) Then

                TrackBar3.Value = GOLD_POT
                TextBox4.Text = GOLD_POT & " %"
            ElseIf (Lot(2) = ID_IRON) Then

                TrackBar4.Value = IRON_POT
                TextBox3.Text = IRON_POT & " %"
            ElseIf (Lot(2) = ID_SILVER) Then

                TrackBar2.Value = SILVER_POT
                TextBox2.Text = SILVER_POT & " %"
            End If

            If TrackBar3.Visible = True Then
                If (CInt(Tb_etat_lot.Text) < CInt(montantLot_Tb.Text)) Then
                    ProgressBar1.Value = (CInt(Tb_etat_lot.Text) / CInt(montantLot_Tb.Text)) * 100
                Else
                    ProgressBar1.Value = 100
                End If
            Else
                'Tb_etat_gold.ReadOnly = True
                ProgressBar1.Value = 0
            End If

            If (CBool(Lot(7))) Then

                tb_gold_mat.Text = "OUI"
                RadioButton1.Checked = True

                is_material_gold = 1
                Panel7.Visible = True
                tbNomLot.Visible = True
            Else
                tb_gold_mat.Text = "NON"
                RadioButton2.Checked = True

                is_material_gold = 0
                Panel7.Visible = False
                tbNomLot.Visible = False
            End If

            If (CBool(Lot(8))) Then
                CheckBox1.Checked = True
            Else
                CheckBox1.Checked = False
            End If

            If (CBool(Lot(9))) Then
                CheckBox2.Checked = True
            Else
                CheckBox2.Checked = False
            End If

        End Using
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Dim TotalmontantJack As Integer = 0
        Dim rest As Integer = 0
        Dim datedebut, datefin As String
        Dim Tabdatedebut, Tabdatefin As String()

        If DateTime.Compare(DpDebut.Value, DpFin.Value) <= 0 Then
            Tabdatedebut = DpDebut.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
            datedebut = Tabdatedebut(2) & "#" & Tabdatedebut(1) & "#" & Tabdatedebut(0)
            'datedebut = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
            'MsgBox(datedebut)
            Tabdatefin = DpFin.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
            datefin = Tabdatefin(2) & "#" & Tabdatefin(1) & "#" & Tabdatefin(0)

            Dim repon As String = Ocaisse.generalComptbility(datedebut, datefin)

            Dim Cparsal() As String = repon.Split(CChar("/"))
            DataGridView1.Rows.Clear()
            For h As Integer = 0 To Cparsal.Count - 1
                Dim row0 As String() = Cparsal(h).Split(CChar("#"))
                DataGridView1.Rows.Add(row0)
                'dspv.Tables("point_vente").Rows(h).Item("nom_pv") & "#" & LbTotalMise & "#" & MontantSortie.ToString & "#" & benef.ToString
                Dim benef As Integer = CInt(row0(2)) - CInt(row0(3))
                rest += benef
            Next

            TextBox5.Text = rest.ToString


        Else
            MessageBox.Show("Attention a la plage de Date.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub



    Private Sub TrackBar3_Scroll(sender As Object, e As EventArgs) Handles TrackBar3.Scroll
        TextBox4.Text = TrackBar3.Value & " %"
    End Sub

    Private Sub TrackBar2_Scroll_1(sender As Object, e As EventArgs) Handles TrackBar2.Scroll
        TextBox2.Text = TrackBar2.Value & " %"
    End Sub


End Class