﻿Imports System.IO
Public Class ajouter_lot

    Public is_material As Integer = 0
    Public charge_glo As Integer = 0
    Public share_w As Integer = 0
    Dim Coleu As Drawing.Color
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim d As String()
        Dim pv As String
        Dim fname As String = "default.jpg"
        Dim videoFname As String = "default.avi"
        If (PictureBox1.ImageLocation = "") Then
            fname = "default.jpg"
        Else
            fname = System.IO.Path.GetFileName(PictureBox1.ImageLocation)
        End If

        If (video_tb.Text = "") Then
            videoFname = "default.avi"
        Else
            videoFname = System.IO.Path.GetFileName(video_tb.Text)
        End If
        d = cb_type_jack.Text.Split("#")
        'MsgBox(d(0))

        If (Tbmontant_jack.Text <> "" And IsNumeric(Tbmontant_jack.Text.Trim)) Then
            If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                Using ojacpot As New BL_Lotto.BlJackot(source)
                    'MsgBox(CBool(is_material_silver))

                    'If (CBool(charge_glo)) Then
                    '    pv = 0
                    'Else
                    pv = CbPointVente.SelectedValue.ToString()
                    'End If

                    If d.Length >= 2 Then
                        tbid_jck.Text = d(0)
                        If ojacpot.Ajouter_lot_Jackpot(My.Settings.ID_BOOKMAKER, tbid_jck.Text, pv, Tbmontant_jack.Text.Trim, CBool(is_material), tbNomLotSilver.Text.Trim, fname, share_w.ToString, videoFname) Then
                            MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)


                            If (Not Directory.Exists(AppDomain.CurrentDomain.BaseDirectory & "photo")) Then
                                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory & "photo")
                            End If

                            If (Not File.Exists(AppDomain.CurrentDomain.BaseDirectory & "photo\" & fname)) Then
                                FileCopy(PictureBox1.ImageLocation, AppDomain.CurrentDomain.BaseDirectory & "photo\" & fname)
                            End If

                            If (Not File.Exists(AppDomain.CurrentDomain.BaseDirectory & "\video\" & videoFname)) Then
                                FileCopy(video_tb.Text, AppDomain.CurrentDomain.BaseDirectory & "video\" & videoFname)
                            End If


                        Else
                            MessageBox.Show("Enregistrement echoué.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If


                    Else
                        MessageBox.Show("veillez selectionner un type.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If

                End Using
            End If

        Else
            MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then ' Teste si le bouton est coché.
            Coleu = CheckBox1.BackColor

            CheckBox1.BackColor = CheckBox1.ForeColor
            CheckBox1.ForeColor = Coleu
            CheckBox1.Text = "OUI"
            is_material = 1
            Panel8.Visible = True
            tbNomLotSilver.Visible = True

        Else
            Coleu = CheckBox1.BackColor
            CheckBox1.BackColor = CheckBox1.ForeColor
            CheckBox1.ForeColor = Coleu
            is_material = 0
            CheckBox1.Text = "NON"
            Panel8.Visible = False
            tbNomLotSilver.Visible = False
        End If
        'MsgBox(is_material)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        With OpenFileDialog1
            .Title = "Ouvrir"
            .InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "photo"
            .Filter = "Fichiers png|*.png;*.jpg"
            .Multiselect = False
            .ValidateNames = True
            .CheckFileExists = True
            .AddExtension = True
        End With

        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            If (OpenFileDialog1.FileName = "") Then
                MsgBox("veuillez choisir un fichier")
                Exit Sub
            Else
                PictureBox1.ImageLocation = OpenFileDialog1.FileName
            End If

        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then ' Teste si le bouton est coché.
            Coleu = CheckBox2.BackColor

            CheckBox2.BackColor = CheckBox2.ForeColor
            CheckBox2.ForeColor = Coleu
            CheckBox2.Text = "OUI"
            charge_glo = 1
            Label29.Visible = False
            CbPointVente.Visible = False

        Else
            Coleu = CheckBox2.BackColor
            CheckBox2.BackColor = CheckBox2.ForeColor
            CheckBox2.ForeColor = Coleu
            charge_glo = 0
            CheckBox2.Text = "NON"
            Label29.Visible = True
            CbPointVente.Visible = True
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then ' Teste si le bouton est coché.
            Coleu = CheckBox3.BackColor
            CheckBox3.BackColor = CheckBox3.ForeColor
            CheckBox3.ForeColor = Coleu
            CheckBox3.Text = "OUI"
            share_w = 1


        Else
            Coleu = CheckBox3.BackColor
            CheckBox3.BackColor = CheckBox3.ForeColor
            CheckBox3.ForeColor = Coleu
            share_w = 0
            CheckBox3.Text = "NON"

        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        With OpenFileDialog1
            .Title = "Ouvrir"
            .InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "video"
            .Filter = "Fichiers avi|*.avi"
            .Multiselect = False
            .ValidateNames = True
            .CheckFileExists = True
            .AddExtension = True
        End With

        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            If (OpenFileDialog1.FileName = "") Then
                MsgBox("veuillez choisir un fichier")
                Exit Sub
            Else
                video_tb.Text = OpenFileDialog1.FileName
            End If

        End If
    End Sub
End Class