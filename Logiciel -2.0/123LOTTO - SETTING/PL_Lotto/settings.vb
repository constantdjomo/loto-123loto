﻿Imports MySql
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports MySql.Data.Types

Imports System.Net.Sockets
Imports System.Net
Imports System.Threading

Module settings
    Public ListeClients As List(Of Client) 'Liste destinée à contenir les clients connectés

    Public TirageLancer As Integer = 0
    Public Resultat(4) As String
    Public timetosend As String = "0"
    Public animaDemarer As Boolean = False
    Public MonSocketClient, MonSocketCaisse As Socket
    Public Ocaisse As Client

    Dim Oactivation As BL_Lotto.BlActivation
    Public CONNECTED_USERID As String = ""
    Public CONNECTED_USERNAME As String = ""
    'Public SERVEUR As String = "192.168.1.110"
    Public SERVEUR As String = "10.8.0.1"
    'Public SERVEUR As String = "127.0.0.1"
    Public PORT As String = "9514"
    Public BD As String = "bd_loto"
    Public ID As String = "loto"
    'Public ID As String = "root"
    Public PWD As String = "jlprom2at@"
    Public source As String = "server=" & SERVEUR & ";user id=" & ID & ";Password=" & PWD & ";persist security info=True;database=" & BD
#Region "definition des settings"

    Public DEDUCT_JACK_TO_TURN_OVER, MONTH_BOOST_LIMIT, MAX_BOOST_LIMIT, DIFICULT_LEVEL, DELAI_AV_GAINS, GOLD_POT, SILVER_POT, IRON_POT, CAGNOTE_POT, MSTERIOUS_POT As Integer
    Public MEGA_POT, ID_CAGNOTE, ID_GOLD, ID_SILVER, ID_IRON, ID_MSTERIOUS, ID_MEGA, NB_SHARER, MAX_GAIIN_SILVER, MIN_GAIIN_SILVER As Integer
    Public MAX_GAIN_IRON, MIN_GAIN_IRON, MIN_LOTO_PLASMA, MIN_PARI_JACKPOT, DELAI_JEU As Integer
    Public SCREEN_SELECT As String



    Public ID_TICKET_GOLG_send, ID_TICKET_SILVER_send, ID_TICKET_IRON_send, ID_TICKET_CAGNOTE_send As String
    Public ID_TICKET_GOLG, ID_TICKET_SILVER, ID_TICKET_IRON, ID_TICKET_CAGNOTE, ID_TICKET_MSTERIOUS As String
    Public list_id_tick As New List(Of String)

    Public timesent As Integer = 0

    Public Sub initialiseSetting()


        Dim oDataSet, seting() As String

        oDataSet = Ocaisse.GetSetting
        seting = oDataSet.Split(CChar("/"))

        If seting.Count > 2 Then
            MONTH_BOOST_LIMIT = CInt(seting(0))
            MAX_BOOST_LIMIT = CInt(seting(1))
            DIFICULT_LEVEL = CInt(seting(2))
            DELAI_AV_GAINS = CInt(seting(3))
            PORT = seting(4)
            GOLD_POT = CInt(seting(5))
            SILVER_POT = CInt(seting(6))
            IRON_POT = CInt(seting(7))
            CAGNOTE_POT = CInt(seting(8))
            MSTERIOUS_POT = CInt(seting(9))
            MEGA_POT = CInt(seting(10))
            ID_CAGNOTE = CInt(seting(11))
            ID_GOLD = CInt(seting(12))
            ID_SILVER = CInt(seting(13))
            ID_IRON = CInt(seting(14))
            ID_MSTERIOUS = CInt(seting(15))
            ID_MEGA = CInt(seting(16))
            NB_SHARER = CInt(seting(18))
            MAX_GAIIN_SILVER = CInt(seting(19))
            MIN_GAIIN_SILVER = CInt(seting(20))
            MAX_GAIN_IRON = CInt(seting(21))
            MIN_GAIN_IRON = CInt(seting(22))
            MIN_LOTO_PLASMA = CInt(seting(23))
            MIN_PARI_JACKPOT = CInt(seting(24))
            DELAI_JEU = CInt(seting(25))
            DEDUCT_JACK_TO_TURN_OVER = CInt(seting(26))
            'SCREEN_SELECT
        End If

    End Sub



    Public Function PersistSetting() As Boolean
        Return Ocaisse.SaveSetting(MONTH_BOOST_LIMIT, MAX_BOOST_LIMIT, DIFICULT_LEVEL, DELAI_AV_GAINS, PORT, GOLD_POT, SILVER_POT, IRON_POT, CAGNOTE_POT, MSTERIOUS_POT,
                                   MEGA_POT, ID_CAGNOTE, ID_GOLD, ID_SILVER, ID_IRON, ID_MSTERIOUS, ID_MEGA, SCREEN_SELECT, NB_SHARER, MAX_GAIIN_SILVER, MIN_GAIIN_SILVER,
                                   MAX_GAIN_IRON, MIN_GAIN_IRON, MIN_LOTO_PLASMA, MIN_PARI_JACKPOT, DEDUCT_JACK_TO_TURN_OVER)
    End Function


    Public Sub select_another_lot(ByVal id_jack As String, Optional IsSilver As Boolean = False)

        Dim oDataSet As New DataSet
        Dim q() As Integer
        Dim aqt As Integer
        Dim cpt As Integer

        Using ojacpot As New BL_Lotto.BlJackot(source)
            Try
                'todo a revoir cette partie
                'MsgBox("tres bien")
                oDataSet = ojacpot.GetListlot(id_jack, False)
                cpt = oDataSet.Tables("lot_jackpot").Rows.Count
                MsgBox("Lot " & oDataSet.Tables("lot_jackpot").Rows.Count)
                aqt = 1
                q = GetRandomDistinct(aqt, cpt)
                MsgBox(q(0))
                If Not IsSilver Then

                    'MsgBox(" avant d'entrer" & ListWinTemp.Length)
                    If (oDataSet.Tables("lot_jackpot").Rows.Count > 0) Then

                        ojacpot.Maj_mon_Jackpot(id_jack, oDataSet.Tables("lot_jackpot").Rows(q(0)).Item("id_lot").ToString)

                    End If
                Else
                    'MsgBox(" avant d'entrer" & ListWinTemp.Length)
                    If (oDataSet.Tables("lot_jackpot").Rows.Count > 0) Then

                        'If (oDataSet.Tables("lot_jackpot").Rows(q(0)).Item("id_lot").ToString <> "0") Then
                        ojacpot.Maj_mon_Jackpot(id_jack, oDataSet.Tables("lot_jackpot").Rows(q(0)).Item("id_lot").ToString)

                        Dim rand As New Random()
                        Dim nv_jack_sil As Integer

                        nv_jack_sil = rand.Next(1000, MAX_GAIIN_SILVER)
                        'ojacpot.Maj_mon_Jackpot(id_jack, 0, nv_jack_sil, 0, "Cagnotte variable")
                        ojacpot.Maj_mon_Lot(oDataSet.Tables("lot_jackpot").Rows(q(0)).Item("id_lot").ToString, nv_jack_sil, 0, "")

                        'End If
                    End If
                End If


            Catch ex As Exception

            End Try
        End Using

    End Sub
#End Region



    Sub Combo(ByVal ComboI As ComboBox, ByVal Req As String)
        Dim con As MySqlConnection = New MySqlConnection(source)
        Dim cmd As New MySqlCommand(Req, con)
        Dim dr As MySqlDataReader
        con.Open()
        dr = cmd.ExecuteReader
        While dr.Read
            ComboI.Items.Add(dr(0))
        End While
        dr.Close()
        con.Close()
    End Sub
    Public Function GetRandomDistinct(ByVal tailleVoulue As Integer, ByVal cpt As Integer) As Integer()
        Dim nombres As New List(Of Integer)()
        While nombres.Count <> tailleVoulue
            While nombres.Count <> tailleVoulue
                Dim rand As New Random()

                nombres.Add(rand.Next(0, cpt))
            End While
            nombres = nombres.Distinct().ToList()
        End While
        Return nombres.ToArray()
    End Function



    Public Function Hostname2IP(ByVal hostname As String) As String
        Dim hostname2 As IPHostEntry = Dns.GetHostEntry(hostname)
        Dim ip As IPAddress() = hostname2.AddressList

        Return ip(1).ToString()
    End Function


    Sub main()

    End Sub


    Public Class Client
        Private _SocketClient As Socket 'Le socket du client
        Private _Pseudo As String 'Le pseudo du client

        'Constructeur
        Sub New(ByVal Sock As Socket)
            _SocketClient = Sock
        End Sub


        Public Function EnvoiRequete(ByVal Message As String) As String
            'envoi de la requete
            Me.EnvoiMessage(Message)

            'reception de la reponse
            Dim Bytes(255) As Byte
            Dim Recu As Integer
            Try
                Recu = _SocketClient.Receive(Bytes)
            Catch ex As Exception 'Erreur si fermeture du socket pendant la réception
                MessageBox.Show("Connexion perdue, arrêt de la réception des données ...", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Dim reponse As String
            reponse = System.Text.Encoding.UTF8.GetString(Bytes)
            reponse = reponse.Substring(0, Recu)

            Return reponse

        End Function


        Public Function UserCOnnect(ByVal login As String, ByVal pwd As String, ByVal credent As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "9" & "/" & login & "/" & pwd & "/" & credent

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function UserComptbility(ByVal userid As String, ByVal datdebut As String, ByVal datfin As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "15" & "/" & userid & "/" & datdebut & "/" & datfin

            Return Me.EnvoiRequete(Message)

        End Function
        ' DIFICULT_LEVEL, DELAI_AV_GAINS, GOLD_POT, SILVER_POT, IRON_POT, CAGNOTE_POT, MSTERIOUS_POT, MEGA_POT, ID_GOLD, ID_SILVER, ID_IRON, ID_MSTERIOUS, ID_MEGA, DELAI_JEU, MAX_GAIIN_SILVER, PORT, SCREEN_SELECT, NB_SHARER

        Public Function SaveSetting(ByVal MONTH_BOOST_LIMIT As String, ByVal MAX_BOOST_LIMIT As String, ByVal DIFICULT_LEVEL As String, ByVal DELAI_AV_GAINS As String, ByVal PORT As String, ByVal GOLD_POT As String, ByVal SILVER_POT As String, ByVal IRON_POT As String, ByVal CAGNOTE_POT As String, ByVal MSTERIOUS_POT As String, ByVal MEGA_POT As String, ByVal ID_CAGNOTE As String, ByVal ID_GOLD As String, ByVal ID_SILVER As String, ByVal ID_IRON As String, ByVal ID_MSTERIOUS As String, ByVal ID_MEGA As String, ByVal SCREEN_SELECT As String, ByVal NB_SHARER As String, ByVal MAX_GAIIN_SILVER As String, ByVal MIN_GAIIN_SILVER As String, ByVal MAX_GAIN_IRON As String, ByVal MIN_GAIN_IRON As String, ByVal MIN_LOTO_PLASMA As String, ByVal MIN_PARI_JACKPOT As String, ByVal DEDUCT_JACK_TO_TURN_OVER As String) As Boolean
            Dim Message, rep As String


            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "19" & "/" & My.Settings.ID_BOOKMAKER & "/" & MONTH_BOOST_LIMIT & "/" & MAX_BOOST_LIMIT & "/" & DIFICULT_LEVEL & "/" & DELAI_AV_GAINS & "/" & PORT & "/" & GOLD_POT & "/" & SILVER_POT & "/" & IRON_POT & "/" & CAGNOTE_POT & "/" & MSTERIOUS_POT & "/" & MEGA_POT & "/" & ID_CAGNOTE & "/" & ID_GOLD & "/" & ID_SILVER & "/" & ID_IRON & "/" & ID_MSTERIOUS & "/" & ID_MEGA & "/" & SCREEN_SELECT & "/" & NB_SHARER & "/" & MAX_GAIIN_SILVER & "/" & MIN_GAIIN_SILVER & "/" & MAX_GAIN_IRON & "/" & MIN_GAIN_IRON & "/" & MIN_LOTO_PLASMA & "/" & MIN_PARI_JACKPOT & "/" & DEDUCT_JACK_TO_TURN_OVER

            rep = Me.EnvoiRequete(Message)

            If (rep <> "0") Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function generalComptbility(ByVal datdebut As String, ByVal datfin As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "18" & "/" & datdebut & "/" & datfin & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)

        End Function


        Public Function GetLisJackParPv(ByVal idpv As String, ByVal datdebut As String, ByVal datfin As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "22" & "/" & idpv & "/" & datdebut & "/" & datfin

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function getallmode() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "10"

            Return Me.EnvoiRequete(Message)

        End Function
        Public Function getLastTirage() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "11"

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function isActivated() As Boolean


            Dim Message As String
            Dim reponse As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "12" & "/" & My.Settings.ID_BOOKMAKER

            reponse = Me.EnvoiRequete(Message)

            If (reponse <> "0") Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function GetSetting() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "14" & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)


        End Function


        Public Function can(ByVal user_id As String) As Boolean

            Dim Message, rep As String


            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "13" & "/" & user_id

            rep = Me.EnvoiRequete(Message)

            If (rep <> "0") Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function Getlot(ByVal id_lot As String) As String

            Dim Message, rep As String


            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "16" & "/" & id_lot

            rep = Me.EnvoiRequete(Message)
            Return rep


        End Function

        Public Function Maj_mon_Lot_Set(ByVal id_lot As String, ByVal TbGold As String, ByVal Tb_etat_gold As String, ByVal is_material_gold As String, ByVal tbNomLotGold As String, ByVal selected_pv As String, ByVal schow_etat As String, ByVal status As String, ByVal fname As String, ByVal videoFile As String) As Boolean

            Dim Message, rep As String


            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "17" & "/" & id_lot & "/" & TbGold & "/" & Tb_etat_gold & "/" & is_material_gold & "/" & tbNomLotGold & "/" & selected_pv & "/" & schow_etat & "/" & status & "/" & fname & "/" & videoFile

            rep = Me.EnvoiRequete(Message)

            If (rep <> "0") Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function checkTicket(ByVal codebar As String, ByVal caissier As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "2/" & codebar & "/" & caissier

            Return Me.EnvoiRequete(Message)


        End Function

        Public Function checkTicketjacpot(ByVal codebar As String, ByVal caissier As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "4/" & codebar & "/" & caissier

            Return Me.EnvoiRequete(Message)


        End Function

        Public Function GetTimerAndTirage() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "3"

            Return Me.EnvoiRequete(Message)


        End Function







        Public Sub EnvoiMessage(ByVal Message As String)
            Dim Mess As Byte() = System.Text.Encoding.UTF8.GetBytes(Message)
            Dim Envoi As Integer = _SocketClient.Send(Mess)
            'Console.WriteLine(Envoi & " bytes envoyés au client " & _Pseudo)
        End Sub
        Public Function ReceiveMessage() As String()
            Dim Bytes(255) As Byte
            Dim Recu As Integer
            Try
                Recu = _SocketClient.Receive(Bytes)
            Catch ex As Exception 'Erreur si fermeture du socket pendant la réception
                MessageBox.Show("Connexion perdue, arrêt de la réception des données ...", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Dim Message As String
            Message = System.Text.Encoding.UTF8.GetString(Bytes)
            Message = Message.Substring(0, Recu)

            Return Message.Split(CChar("/"))
        End Function



    End Class


    Sub Broadcast(ByVal Message As String)

        'Écrit le message dans la console et l'envoie à tous les clients connectés
        Console.WriteLine("BROADCAST : " & Message)
        For Each Cli In ListeClients
            Cli.EnvoiMessage(Message)
        Next

    End Sub


End Module
