﻿Public Class UCformUser

    Private oDataSet As Data.DataSet
    Private oUserDataSet As DataSet
    Private oUser As BL_Lotto.BlUser

    Function AjouterUser() As Boolean
        Using oUser As New BL_Lotto.BlUser(source)
            'Get a new Project DataSet
            oDataSet = oUser.NvDSUser()
            'Initialize a datarow object from the Project DataSet
            Dim oDataRow As Data.DataRow = _
                  oDataSet.Tables("users").NewRow
            'Set the values in the DataRow
            oDataRow.Item("ID_USER") = Trim(TbIduser.Text)
            oDataRow.Item("NOM") = Trim(TbNom.Text)
            oDataRow.Item("PRENOM") = Trim(TbPrenom.Text)
            oDataRow.Item("id_user_typ") = ComboBox1.SelectedValue
            oDataRow.Item("LOGIN") = Trim(TbCompte.Text)
            oDataRow.Item("PWD") = Trim(TbPwd.Text)

            'Add the DataRow to the DataSet
            oDataSet.Tables("users").Rows.Add(oDataRow)
            'Add the DataRow to the DataSet
            'Add the Project
            If Not oUser.AjouterUser(oDataSet) Then
                Throw New Exception("Echec d'enregistrement")
            Else
                AjouterUser = True
            End If
        End Using
    End Function

    Function ModifUser() As Boolean
        Using oUser As New BL_Lotto.BlUser(source)
            'Get a new Project DataSet
            oDataSet = oUser.NvDSUser()
            'Initialize a datarow object from the Project DataSet
            Dim oDataRow As Data.DataRow = _
                  oDataSet.Tables("users").NewRow
            'Set the values in the DataRow
            oDataRow.Item("ID_USER") = Trim(TbIduser.Text)
            oDataRow.Item("NOM") = Trim(TbNom.Text)
            oDataRow.Item("PRENOM") = Trim(TbPrenom.Text)
            oDataRow.Item("id_user_typ") = ComboBox1.SelectedValue
            oDataRow.Item("LOGIN") = Trim(TbCompte.Text)
            oDataRow.Item("PWD") = Trim(TbPwd.Text)

            'Add the DataRow to the DataSet
            oDataSet.Tables("users").Rows.Add(oDataRow)
            'Add the DataRow to the DataSet
            'Add the Project
            If Not oUser.MajUser(oDataSet) Then
                Throw New Exception("Echec Modification")
            Else
                ModifUser = True
            End If
        End Using
    End Function



    Private Sub btEnregistrer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btEnregistrer.Click
        If (TbConfirm.Text <> TbPwd.Text) Then
            MessageBox.Show("Mauvaise Confirmation du mot de passe", "Erreur Saisie")
        ElseIf (TbCompte.Text = "" Or TbConfirm.Text = "" Or TbPwd.Text = "") Then
            MessageBox.Show("Tous les champs sont obligatoires", "Erreur Saisie")
        Else
            Try

                Using oUser As New BL_Lotto.BlUser(source)
                    'MsgBox("le nom de compte est " & TbCompte.Text.Trim)
                    If (btEnregistrer.Text = "Enregistrer") Then
                        'MsgBox("le nom de compte est " & TbCompte.Text.Trim)
                        If Not oUser.DejaUser(TbIduser.Text.Trim) AndAlso TbNom.Text <> "" AndAlso TbPrenom.Text <> "" Then
                            If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then

                                If AjouterUser() = True Then
                                    MessageBox.Show("Enregistré avec succès!", "Enregistrement de l'utilisateur")
                                    'Me.Close()
                                    'Me.Dispose()
                                Else
                                    MessageBox.Show("Erreur d'enregistrement !", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                End If
                            End If
                        Else
                            MessageBox.Show("Compte deja existant", "Enregistrement impossible")
                        End If
                    Else

                        If MsgBox("Etes-vous sûr de vouloir modifier?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                            If ModifUser() = True Then
                                MessageBox.Show("Enregistré avec succès!", "Enregistrement de la modification")
                                Me.ParentForm.Close()
                                'Me.Dispose()
                            Else
                                MessageBox.Show("Erreur d'enregistrement !", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        End If
                    End If


                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message.ToString)
            End Try
        End If
    End Sub

    Private Sub BtAnnuler_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtAnnuler.Click
        'Me.Close()
        Me.ParentForm.Close()
    End Sub

   
    Private Sub UCformUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim dsuser As DataSet

        Using oUser As New BL_Lotto.BlUser(source)

            dsuser = oUser.GetUserTyp
            ComboBox1.DataSource = Nothing
            ComboBox1.DataSource = dsuser.Tables("users_type")
            ComboBox1.DisplayMember = "user_typ"
            ComboBox1.ValueMember = "id_user_typ"

        End Using
    End Sub
End Class
