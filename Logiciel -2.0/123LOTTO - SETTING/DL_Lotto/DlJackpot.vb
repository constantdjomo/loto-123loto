﻿Public Class DlJackpot
    Inherits DlBase

#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region

#Region " Public Role Functions "

    Public Function Maj_mon_Jackpot(ByVal id_jack As String, ByVal id_lot_encour As String) As Boolean
        Try


            MyBase.SQL = "UPDATE lot_jackpot SET status = 0 WHERE id_jack =" & id_jack '& " AND id_pv = 0"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            ExecuteStoredProcedure()
            MyBase.SQL = "UPDATE lot_jackpot SET status = 1 WHERE id_lot =" & id_lot_encour
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            Maj_mon_Jackpot = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function Maj_mon_Jackpot(ByVal id_pv As String, ByVal id_jack As String, ByVal id_lot_encour As String) As Boolean
        Try


            MyBase.SQL = "UPDATE lot_jackpot SET status = 0 WHERE id_jackp =" & id_jack & " AND id_pv =" & id_pv
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            ExecuteStoredProcedure()
            MyBase.SQL = "UPDATE lot_jackpot SET status = 1 WHERE id_lot =" & id_lot_encour
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            Maj_mon_Jackpot = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    'Public Function Maj_mon_Jackpot(ByVal id_jack As String, ByVal montant As String, ByVal is_material As Boolean, ByVal NomLot As String) As Boolean
    '    Try


    '        MyBase.SQL = "UPDATE jackpot SET id_lot_encour = 0 , montant_jackp = " & montant & " , is_matirial = " & is_material & " , nom_materiel = '" & NomLot & "' WHERE id_jackp =" & id_jack
    '        'Initialisation l'objet command
    '        MyBase.InitializeCommand()

    '        Maj_mon_Jackpot = ExecuteStoredProcedure()
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message,
    '        ExceptionErr.InnerException)
    '    End Try
    'End Function
    Public Function Maj_mon_Lot(ByVal id_lot As String, ByVal id_pv As String, ByVal montant As String, ByVal etat_jackp As String, ByVal is_material As Boolean, ByVal NomLot As String) As Boolean
        Try

            MyBase.SQL = "UPDATE lot_jackpot SET id_pv = " & id_pv & ", montant_jackp= " & montant & " , is_matirial = " & is_material & " , nom_materiel = '" & NomLot & "', etat_jackp = " & etat_jackp & "  WHERE id_lot =" & id_lot
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            Maj_mon_Lot = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function Maj_mon_Lot(ByVal id_lot As String, ByVal montant As String, ByVal is_material As Boolean, ByVal NomLot As String) As Boolean
        Try


            MyBase.SQL = "UPDATE lot_jackpot SET montant_jackp= " & montant & " , is_matirial = " & is_material & " , nom_materiel = '" & NomLot & "' WHERE id_lot =" & id_lot
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            Maj_mon_Lot = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function Ajouter_lot_Jackpot(ByVal id_bookmaker As String, ByVal id_jack As String, ByVal id_pv As String, ByVal montant As String, ByVal is_material As Boolean, ByVal NomLot As String, ByVal photo As String, ByVal share As String, ByVal video As String) As Boolean

        Try
            MyBase.SQL = "INSERT INTO lot_jackpot(`id_bookmaker`,`id_jack`,`id_pv`,`is_matirial`,`nom_materiel`,`montant_jackp`,`photo`,`share`,`video`) VALUE (" & id_bookmaker & ", " & id_jack & " , " & id_pv & " , " & is_material & " ,'" & NomLot & "', " & montant & ",'" & photo & "','" & share & "', '" & video & "')"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            Ajouter_lot_Jackpot = ExecuteStoredProcedure()

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function suprim_lot_Jackpot(ByVal id_lot As String) As Boolean
        Try
            MyBase.SQL = "DELETE FROM lot_jackpot WHERE id_lot=" & id_lot
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            suprim_lot_Jackpot = ExecuteStoredProcedure()

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function



    Public Function GetMontantJackpot(ByVal id_jackp As String) As String
        Try
            Dim dss As New DataSet

            MyBase.SQL = "Select montant_jackp from jackpot WHERE id_jackp=" & id_jackp
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "jackpot")
            GetMontantJackpot = dss.Tables("jackpot").Rows(0).Item("montant_jackp")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetEtatJackpot(ByVal id_jackp As String) As String
        Try
            Dim dss As New DataSet

            MyBase.SQL = "Select etat_jackp from lot_jackpot WHERE id_jackp=" & id_jackp
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "jackpot")
            GetEtatJackpot = dss.Tables("jackpot").Rows(0).Item("etat_jackp")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetJackpot(ByVal id_jackp As String) As DataSet ' utiliser uniquement pour les lot partager dans toute les salles
        Try

            GetJackpot = New DataSet

            'MyBase.SQL = "Select * from jackpot WHERE id_jackp=" & id_jackp
            MyBase.SQL = "SELECT l.`id_jack`, `id_lot` as `id_lot_encour`, l.`is_matirial`,nom_jackp, l.`nom_materiel`, l.`montant_jackp`, `etat_jackp`,`photo` FROM `lot_jackpot` l,`jackpot` j WHERE j.`id_jackp` = l.`id_jack` AND `status`= 1 AND `id_jack` =" & id_jackp
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(GetJackpot, "jackpot")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetJackpotForTick(ByVal COD_BAR As String, ByVal id_jack As String) As DataSet ' utiliser uniquement pour les lot partager dans toute les salles
        Try

            GetJackpotForTick = New DataSet

            'MyBase.SQL = "Select * from jackpot WHERE id_jackp=" & id_jackp
            MyBase.SQL = "SELECT g.`id_lot`, j.`nom_jackp`, g.`is_materiel`, g.`nom_materiel`, g.`montant`, l.etat_jackp,`photo` FROM `gagner_jackp` g , `lot_jackpot` l, `jackpot` j,`ticket` t WHERE t.ID_TICKET = g.id_tick AND g.`id_lot` = l.`id_lot` AND l.id_jack = j.id_jackp AND t.`COD_BAR`=" & COD_BAR & " AND l.id_jack = " & id_jack
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(GetJackpotForTick, "jackpot")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetLotParSalle(ByVal id_salle As String) As DataSet
        Try

            GetLotParSalle = New DataSet

            'MyBase.SQL = "Select * from jackpot WHERE id_jackp=" & id_jackp
            MyBase.SQL = "SELECT * FROM `lot_jackpot` l WHERE status = 1 AND `id_pv`=" & id_salle
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(GetLotParSalle, "lot_jackpot")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetListlot(ByVal id_bookmaker As String, ByVal id_jackp As String, Optional is_a_radom As Boolean = False) As DataSet
        Try
            Dim dss As New DataSet

            MyBase.SQL = "Select id_lot as id_lot_encour from lot_jackpot WHERE status = 1 AND id_bookmaker=" & id_bookmaker & " AND id_jack=" & id_jackp '& " AND id_pv = 0"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "lot_jackpot")

            If (dss.Tables("lot_jackpot").Rows.Count > 0) Then

                GetListlot = New DataSet
                If (is_a_radom) Then
                    MyBase.SQL = "SELECT `id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp` FROM `lot_jackpot` WHERE id_lot <>" & dss.Tables("lot_jackpot").Rows(0).Item("id_lot_encour") & "  AND id_bookmaker=" & id_bookmaker & " AND id_jack=" & id_jackp & " ORDER BY RAND()"
                Else
                    MyBase.SQL = "SELECT `id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp` FROM `lot_jackpot` WHERE id_bookmaker=" & id_bookmaker & " AND id_jack=" & id_jackp & " ORDER BY RAND()"
                End If

                MyBase.InitializeCommand()
                'remplir le dataset
                MyBase.FillDataSet(GetListlot, "lot_jackpot")
            Else

                GetListlot = New DataSet

                MyBase.SQL = "Select id_lot from lot_jackpot WHERE id_bookmaker=" & id_bookmaker & " AND  id_jack=" & id_jackp & " ORDER BY RAND()"
                MyBase.InitializeCommand()
                'remplir le dataset
                MyBase.FillDataSet(GetListlot, "lot_jackpot")
            End If


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function



    Public Function Getlot(ByVal id_lot As String) As DataSet
        Try
            Getlot = New DataSet

            MyBase.SQL = "SELECT * FROM `lot_jackpot` WHERE id_lot=" & id_lot
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(Getlot, "lot_jackpot")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetListPointVente(ByVal id_bookmaker As String) As DataSet
        Try
            GetListPointVente = New DataSet

            MyBase.SQL = "SELECT *  FROM `point_vente` WHERE id_pv <> 0 AND bookmaker_id=" & id_bookmaker
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(GetListPointVente, "point_vente")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetListlot(ByVal id_bookmaker As String) As DataSet
        Try
            GetListlot = New DataSet


            MyBase.SQL = "SELECT id_lot,id_jack,l.id_pv,is_matirial,nom_materiel,montant_jackp,photo,nom_pv,etat_jackp,status,share  FROM `lot_jackpot` l,`point_vente` p WHERE  l.id_bookmaker= " & id_bookmaker & " AND l.id_pv = p.id_pv ORDER BY id_jack"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(GetListlot, "jackpot")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetListlotBookmaker(ByVal id_bookmaker As String, ByVal id_pv As String) As DataSet
        Try
            GetListlotBookmaker = New DataSet


            MyBase.SQL = "SELECT id_lot,id_jack,l.id_pv,is_matirial,nom_materiel,montant_jackp,photo,nom_pv,etat_jackp,status,share  FROM `lot_jackpot` l,`point_vente` p WHERE l.id_pv = p.id_pv AND l.id_pv = " & id_pv & " AND l.id_bookmaker= " & id_bookmaker & "  ORDER BY id_jack"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(GetListlotBookmaker, "jackpot")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetNomLotJackpot(ByVal id_jackp As String) As String
        Try
            Dim dss As New DataSet

            MyBase.SQL = "Select nom_materiel from jackpot WHERE id_jackp=" & id_jackp
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "jackpot")
            GetNomLotJackpot = dss.Tables("jackpot").Rows(0).Item("nom_materiel")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetMontantPariEncour(ByVal tirage As String, ByVal ID_MODE As String) As String
        Try
            Dim dss As New DataSet

            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE STATUS=1 AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND t.NUM_TIRAGE=" & tirage & " AND p.ID_MODE = " & ID_MODE
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "ticket")

            If (Not dss.Tables("ticket").Rows(0).IsNull("TOTALMISE")) Then
                GetMontantPariEncour = CInt(dss.Tables("ticket").Rows(0).Item("TOTALMISE"))
            Else
                GetMontantPariEncour = 0
            End If


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function GetMontStatutJack(ByVal id_jackp As String) As String
        Try
            Dim dss As New DataSet

            MyBase.SQL = "Select etat_jackp from jackpot WHERE id_jackp=" & id_jackp
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "jackpot")
            GetMontStatutJack = dss.Tables("jackpot").Rows(0).Item("etat_jackp")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function setGagnant(ByVal id_lot As String, ByVal tirage As String, ByVal montant_jackp As String, ByVal is_materiel As Integer, ByVal nom_materiel As String) As String

        Dim dss As New DataSet

        MyBase.SQL = "SELECT * FROM ticket WHERE STATUS=1 AND NUM_TIRAGE = " & tirage & "  ORDER BY RAND()"
        MyBase.InitializeCommand()

        MyBase.FillDataSet(dss, "ticket")
        If dss.Tables("ticket").Rows.Count > 0 Then
            MyBase.SQL = "INSERT INTO gagner_jackp (`id_tick`, `id_lot` ,`montant` , `is_materiel`, `nom_materiel`) VALUES (" & dss.Tables("ticket").Rows(0).Item("ID_TICKET") & "," & id_lot & " ," & montant_jackp & "," & is_materiel & ", '" & nom_materiel & "' )"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            ExecuteStoredProcedure()
            Return dss.Tables("ticket").Rows(0).Item("COD_BAR").ToString '& "-" & dss.Tables("ticket").Rows(0).Item("ID_TICKET").ToString
        Else
            Return "0"
        End If


    End Function


    Public Function verifWinnerJackpot(ByVal id_jackp As String, ByVal tirage As String, ByRef idticket As String) As Boolean
        Try
            Dim dss As New DataSet
            Dim rest, montant_jackp, is_materiel As Integer
            Dim nom_materiel As String
            MyBase.SQL = "SELECT ( etat_jackp - montant_jackp) as reste , montant_jackp ,is_matirial, nom_materiel FROM `jackpot` WHERE id_jackp =" & id_jackp

            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "jackpot")
            rest = CInt(dss.Tables("jackpot").Rows(0).Item("reste"))
            montant_jackp = CInt(dss.Tables("jackpot").Rows(0).Item("montant_jackp"))
            nom_materiel = CStr(dss.Tables("jackpot").Rows(0).Item("nom_materiel"))
            is_materiel = CInt(dss.Tables("jackpot").Rows(0).Item("is_matirial"))

            If (rest >= 0) Then
                idticket = setGagnant(id_jackp, tirage, montant_jackp.ToString, is_materiel, nom_materiel)
                If (idticket <> "0") Then
                    Maj_Jackpot(id_jackp, rest.ToString)
                    verifWinnerJackpot = True
                Else
                    verifWinnerJackpot = False
                End If
            Else
                verifWinnerJackpot = False
            End If

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function verifWinnerParlot(ByVal id_lot As String, ByVal tirage As String, ByRef idticket As String) As Boolean
        Try
            Dim dss As New DataSet
            Dim rest, montant_jackp, is_materiel As Integer
            Dim nom_materiel As String
            MyBase.SQL = "SELECT (etat_jackp - montant_jackp) as reste ,id_lot, montant_jackp ,is_matirial, nom_materiel FROM `lot_jackpot` WHERE status = 1 AND id_lot = " & id_lot

            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "jackpot")
            rest = CInt(dss.Tables("jackpot").Rows(0).Item("reste"))
            montant_jackp = CInt(dss.Tables("jackpot").Rows(0).Item("montant_jackp"))
            nom_materiel = CStr(dss.Tables("jackpot").Rows(0).Item("nom_materiel"))
            is_materiel = CInt(dss.Tables("jackpot").Rows(0).Item("is_matirial"))

            If (rest >= 0) Then
                idticket = setGagnant(id_lot, tirage, montant_jackp.ToString, is_materiel, nom_materiel)
                If (idticket <> "0") Then
                    Maj_Jackpot(id_lot, rest.ToString)
                    verifWinnerParlot = True
                Else
                    verifWinnerParlot = False
                End If
            Else
                verifWinnerParlot = False
            End If

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function Maj_Jackpot(ByVal id_lot As String, ByVal montant As String) As Boolean
        Try


            MyBase.SQL = "UPDATE lot_jackpot SET etat_jackp = " & montant & " WHERE id_lot =" & id_lot
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            Maj_Jackpot = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function Maj_Satut_Jackpot(ByVal id_jack As String, ByVal montant As String) As Boolean
        Try


            MyBase.SQL = "UPDATE jackpot SET etat_jackp = (etat_jackp + " & montant & ") WHERE id_jackp =" & id_jack
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            Maj_Satut_Jackpot = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    'Public Function Maj_Satut_Jackpot(ByVal id_jack As String, ByVal montant As String) As Boolean
    '    Try


    '        MyBase.SQL = "UPDATE jackpot SET etat_jackp = (etat_jackp + " & montant & ") WHERE id_jackp =" & id_jack
    '        'Initialisation l'objet command
    '        MyBase.InitializeCommand()

    '        Maj_Satut_Jackpot = ExecuteStoredProcedure()
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message, _
    '        ExceptionErr.InnerException)
    '    End Try
    'End Function
#End Region
End Class
