﻿Public Class BlCompte
    Implements IDisposable
    Private oDlCompte As DL_Lotto.DlCompte
    Private disposedValue As Boolean = False        ' Détection des appels rédondants

    ' IDisposable
#Region "Constructeur et Destructeur"
    Public Sub New(ByVal Constring As String)
        oDlCompte = New DL_Lotto.DlCompte(Constring)
    End Sub
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: Libérer les ressources non gérées lors d'appel expplicite
            End If
            oDlCompte.Dispose()
            oDlCompte = Nothing
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region
#Region " Public Ticket Functions "
    Public Function getStatTirage(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatTirage = oDlCompte.getStatTirage(DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    'Public Function getGainTicket(ByVal id_ticket) As DataSet
    '    Try
    '        'Appel le composant de donnée pour obtenir tous les utilisateurs
    '        getGainTicket = oDlCompte.getGainTicket(id_ticket)
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message, _
    '            ExceptionErr.InnerException)
    '    End Try
    'End Function

    Public Function getListTicket(ByVal idpv As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getListTicket = oDlCompte.getListTicket(idpv, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getListTicketTirag(ByVal DatDebut As String, ByVal DatFin As String, ByVal tirage As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getListTicketTirag = oDlCompte.getListTicketTirag(DatDebut, DatFin, tirage)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getListJackParPv(ByVal idpv As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getListJackParPv = oDlCompte.getListJackParPv(idpv, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getListTicketParUser(ByVal IdUser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getListTicketParUser = oDlCompte.getListTicketParUser(IdUser, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getListTicketServi(ByVal IdUser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getListTicketServi = oDlCompte.getListTicketServi(IdUser, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getStatTicket(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatTicket = oDlCompte.getStatTicket(DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getDateInscription() As String
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getDateInscription = oDlCompte.getDateInscription()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTicketJack(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatTicketJack = oDlCompte.getStatTicketJack(DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTickJackMon(ByVal idpv As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatTickJackMon = oDlCompte.getStatTickJackMon(idpv, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTicketJackParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatTicketJackParUser = oDlCompte.getStatTicketJackParUser(iduser, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTickJackMonParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatTickJackMonParUser = oDlCompte.getStatTickJackMonParUser(iduser, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTicketParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatTicketParUser = oDlCompte.getStatTicketParUser(iduser, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getStatPari(ByVal idpv As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatPari = oDlCompte.getStatPari(idpv, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getStatPariSolo(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatPariSolo = oDlCompte.getStatPariSolo(DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatPariDoublet(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatPariDoublet = oDlCompte.getStatPariDoublet(DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatPariParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getStatPariParUser = oDlCompte.getStatPariParUser(iduser, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function



#End Region
    

End Class
