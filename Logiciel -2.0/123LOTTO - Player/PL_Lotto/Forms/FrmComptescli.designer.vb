﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComptescli
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BtOk = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DpDebut = New System.Windows.Forms.DateTimePicker()
        Me.DpFin = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.LbNbtTicketJack = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LbNbTicket = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.LbBenefice = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.LbTotalSortie = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.LbTotalMise = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.LbNbtTicketGagna = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LbNbTirage = New System.Windows.Forms.Label()
        Me.TbNomUser = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Lb_Nb_annule = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel9.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BtOk)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.DpDebut)
        Me.GroupBox1.Controls.Add(Me.DpFin)
        Me.GroupBox1.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(423, 20)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(515, 121)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Date"
        '
        'BtOk
        '
        Me.BtOk.BackColor = System.Drawing.Color.Crimson
        Me.BtOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtOk.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.BtOk.ForeColor = System.Drawing.Color.White
        Me.BtOk.Location = New System.Drawing.Point(206, 80)
        Me.BtOk.Name = "BtOk"
        Me.BtOk.Size = New System.Drawing.Size(101, 33)
        Me.BtOk.TabIndex = 3
        Me.BtOk.Text = "&Ok"
        Me.BtOk.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(384, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 25)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Fin"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(85, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 25)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Debut"
        '
        'DpDebut
        '
        Me.DpDebut.CalendarFont = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DpDebut.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DpDebut.Location = New System.Drawing.Point(6, 48)
        Me.DpDebut.MinDate = New Date(2014, 1, 1, 0, 0, 0, 0)
        Me.DpDebut.Name = "DpDebut"
        Me.DpDebut.Size = New System.Drawing.Size(200, 22)
        Me.DpDebut.TabIndex = 1
        '
        'DpFin
        '
        Me.DpFin.CalendarFont = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DpFin.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DpFin.Location = New System.Drawing.Point(309, 48)
        Me.DpFin.MinDate = New Date(2014, 1, 1, 0, 0, 0, 0)
        Me.DpFin.Name = "DpFin"
        Me.DpFin.Size = New System.Drawing.Size(200, 22)
        Me.DpFin.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Panel9)
        Me.GroupBox2.Controls.Add(Me.PictureBox2)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.Panel3)
        Me.GroupBox2.Controls.Add(Me.Panel2)
        Me.GroupBox2.Controls.Add(Me.Panel7)
        Me.GroupBox2.Controls.Add(Me.Panel6)
        Me.GroupBox2.Controls.Add(Me.Panel5)
        Me.GroupBox2.Controls.Add(Me.Panel4)
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(21, 159)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(917, 387)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Resume"
        '
        'PictureBox2
        '
        Me.PictureBox2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox2.Image = Global.PL_Lotto.My.Resources.Resources.logo_coupons
        Me.PictureBox2.Location = New System.Drawing.Point(661, 101)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(240, 268)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 7
        Me.PictureBox2.TabStop = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Crimson
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Location = New System.Drawing.Point(661, 28)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(240, 39)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "&Fermer"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.RoyalBlue
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.LbNbtTicketJack)
        Me.Panel3.Location = New System.Drawing.Point(33, 284)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(161, 96)
        Me.Panel3.TabIndex = 6
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(13, 10)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(136, 25)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "G. JACKPOT"
        '
        'LbNbtTicketJack
        '
        Me.LbNbtTicketJack.Font = New System.Drawing.Font("Courier New", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbNbtTicketJack.ForeColor = System.Drawing.Color.White
        Me.LbNbtTicketJack.Location = New System.Drawing.Point(3, 62)
        Me.LbNbtTicketJack.Name = "LbNbtTicketJack"
        Me.LbNbtTicketJack.Size = New System.Drawing.Size(155, 22)
        Me.LbNbtTicketJack.TabIndex = 5
        Me.LbNbtTicketJack.Text = "0"
        Me.LbNbtTicketJack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Crimson
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.LbNbTicket)
        Me.Panel2.Location = New System.Drawing.Point(235, 32)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(161, 96)
        Me.Panel2.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(20, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(121, 25)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "NB TICKETS"
        '
        'LbNbTicket
        '
        Me.LbNbTicket.Font = New System.Drawing.Font("Courier New", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbNbTicket.ForeColor = System.Drawing.Color.White
        Me.LbNbTicket.Location = New System.Drawing.Point(3, 60)
        Me.LbNbTicket.Name = "LbNbTicket"
        Me.LbNbTicket.Size = New System.Drawing.Size(155, 22)
        Me.LbNbTicket.TabIndex = 3
        Me.LbNbTicket.Text = "0"
        Me.LbNbTicket.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Crimson
        Me.Panel7.Controls.Add(Me.LbBenefice)
        Me.Panel7.Controls.Add(Me.Label8)
        Me.Panel7.Location = New System.Drawing.Point(235, 284)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(363, 96)
        Me.Panel7.TabIndex = 6
        '
        'LbBenefice
        '
        Me.LbBenefice.Font = New System.Drawing.Font("Century Gothic", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbBenefice.ForeColor = System.Drawing.Color.White
        Me.LbBenefice.Location = New System.Drawing.Point(22, 48)
        Me.LbBenefice.Name = "LbBenefice"
        Me.LbBenefice.Size = New System.Drawing.Size(315, 33)
        Me.LbBenefice.TabIndex = 3
        Me.LbBenefice.Text = "0"
        Me.LbBenefice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(141, 10)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(79, 25)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "GAINS"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.DarkViolet
        Me.Panel6.Controls.Add(Me.Label7)
        Me.Panel6.Controls.Add(Me.LbTotalSortie)
        Me.Panel6.Location = New System.Drawing.Point(437, 158)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(161, 96)
        Me.Panel6.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(41, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(79, 25)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "SORTIE"
        '
        'LbTotalSortie
        '
        Me.LbTotalSortie.Font = New System.Drawing.Font("Courier New", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbTotalSortie.ForeColor = System.Drawing.Color.White
        Me.LbTotalSortie.Location = New System.Drawing.Point(3, 59)
        Me.LbTotalSortie.Name = "LbTotalSortie"
        Me.LbTotalSortie.Size = New System.Drawing.Size(155, 22)
        Me.LbTotalSortie.TabIndex = 3
        Me.LbTotalSortie.Text = "0"
        Me.LbTotalSortie.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Goldenrod
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.LbTotalMise)
        Me.Panel5.Location = New System.Drawing.Point(235, 158)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(161, 96)
        Me.Panel5.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(39, 10)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 25)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "ENTREE"
        '
        'LbTotalMise
        '
        Me.LbTotalMise.Font = New System.Drawing.Font("Courier New", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbTotalMise.ForeColor = System.Drawing.Color.White
        Me.LbTotalMise.Location = New System.Drawing.Point(3, 59)
        Me.LbTotalMise.Name = "LbTotalMise"
        Me.LbTotalMise.Size = New System.Drawing.Size(155, 22)
        Me.LbTotalMise.TabIndex = 3
        Me.LbTotalMise.Text = "0"
        Me.LbTotalMise.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Goldenrod
        Me.Panel4.Controls.Add(Me.Label5)
        Me.Panel4.Controls.Add(Me.LbNbtTicketGagna)
        Me.Panel4.Location = New System.Drawing.Point(33, 158)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(161, 96)
        Me.Panel4.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(4, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(153, 25)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "T. GAGNANTS"
        '
        'LbNbtTicketGagna
        '
        Me.LbNbtTicketGagna.Font = New System.Drawing.Font("Courier New", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbNbtTicketGagna.ForeColor = System.Drawing.Color.White
        Me.LbNbtTicketGagna.Location = New System.Drawing.Point(3, 59)
        Me.LbNbtTicketGagna.Name = "LbNbtTicketGagna"
        Me.LbNbtTicketGagna.Size = New System.Drawing.Size(155, 22)
        Me.LbNbtTicketGagna.TabIndex = 3
        Me.LbNbtTicketGagna.Text = "0"
        Me.LbNbtTicketGagna.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.LbNbTirage)
        Me.Panel1.Location = New System.Drawing.Point(33, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(161, 96)
        Me.Panel1.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(20, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(118, 25)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "NB TIRAGE"
        '
        'LbNbTirage
        '
        Me.LbNbTirage.Font = New System.Drawing.Font("Courier New", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbNbTirage.ForeColor = System.Drawing.Color.White
        Me.LbNbTirage.Location = New System.Drawing.Point(3, 60)
        Me.LbNbTirage.Name = "LbNbTirage"
        Me.LbNbTirage.Size = New System.Drawing.Size(155, 22)
        Me.LbNbTirage.TabIndex = 3
        Me.LbNbTirage.Text = "0"
        Me.LbNbTirage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TbNomUser
        '
        Me.TbNomUser.AutoSize = True
        Me.TbNomUser.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbNomUser.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.TbNomUser.Location = New System.Drawing.Point(206, 40)
        Me.TbNomUser.Name = "TbNomUser"
        Me.TbNomUser.Size = New System.Drawing.Size(156, 23)
        Me.TbNomUser.TabIndex = 0
        Me.TbNomUser.Text = "NAMGNI Arnold"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.Label9.Location = New System.Drawing.Point(206, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(171, 21)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "mercredi 23 juin 2014"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.PL_Lotto.My.Resources.Resources.AucunePersonne
        Me.PictureBox1.Location = New System.Drawing.Point(21, 20)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(179, 113)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Crimson
        Me.Panel9.Controls.Add(Me.Label15)
        Me.Panel9.Controls.Add(Me.Lb_Nb_annule)
        Me.Panel9.Location = New System.Drawing.Point(437, 32)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(161, 96)
        Me.Panel9.TabIndex = 8
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(23, 10)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(115, 25)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "T. ANNULE"
        '
        'Lb_Nb_annule
        '
        Me.Lb_Nb_annule.Font = New System.Drawing.Font("Courier New", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Lb_Nb_annule.ForeColor = System.Drawing.Color.White
        Me.Lb_Nb_annule.Location = New System.Drawing.Point(0, 55)
        Me.Lb_Nb_annule.Name = "Lb_Nb_annule"
        Me.Lb_Nb_annule.Size = New System.Drawing.Size(161, 31)
        Me.Lb_Nb_annule.TabIndex = 3
        Me.Lb_Nb_annule.Text = "0"
        Me.Lb_Nb_annule.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FrmComptescli
        '
        Me.AcceptButton = Me.BtOk
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkSlateGray
        Me.ClientSize = New System.Drawing.Size(963, 556)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.TbNomUser)
        Me.Name = "FrmComptescli"
        Me.Text = "123Loto - Bilan Périodique"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DpFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents BtOk As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents LbBenefice As System.Windows.Forms.Label
    Friend WithEvents LbTotalSortie As System.Windows.Forms.Label
    Friend WithEvents LbTotalMise As System.Windows.Forms.Label
    Friend WithEvents LbNbtTicketGagna As System.Windows.Forms.Label
    Friend WithEvents LbNbTicket As System.Windows.Forms.Label
    Friend WithEvents LbNbTirage As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TbNomUser As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents LbNbtTicketJack As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents DpDebut As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel1 As Panel
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Panel9 As Panel
    Friend WithEvents Label15 As Label
    Friend WithEvents Lb_Nb_annule As Label
End Class
