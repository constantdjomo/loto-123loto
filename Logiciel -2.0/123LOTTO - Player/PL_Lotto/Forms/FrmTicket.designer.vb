﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTicket
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTicket))
        Me.bt_print = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Lb_Mise = New System.Windows.Forms.Label()
        Me.RadioButton12 = New System.Windows.Forms.RadioButton()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.RadioButton11 = New System.Windows.Forms.RadioButton()
        Me.RadioButton9 = New System.Windows.Forms.RadioButton()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.RadioButton10 = New System.Windows.Forms.RadioButton()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.LB_NumTirage = New System.Windows.Forms.Label()
        Me.BtCancel = New System.Windows.Forms.Button()
        Me.CheckBox90 = New System.Windows.Forms.CheckBox()
        Me.CheckBox80 = New System.Windows.Forms.CheckBox()
        Me.CheckBox70 = New System.Windows.Forms.CheckBox()
        Me.CheckBox60 = New System.Windows.Forms.CheckBox()
        Me.CheckBox50 = New System.Windows.Forms.CheckBox()
        Me.CheckBox40 = New System.Windows.Forms.CheckBox()
        Me.CheckBox20 = New System.Windows.Forms.CheckBox()
        Me.CheckBox8 = New System.Windows.Forms.CheckBox()
        Me.CheckBox89 = New System.Windows.Forms.CheckBox()
        Me.CheckBox79 = New System.Windows.Forms.CheckBox()
        Me.CheckBox69 = New System.Windows.Forms.CheckBox()
        Me.CheckBox59 = New System.Windows.Forms.CheckBox()
        Me.CheckBox49 = New System.Windows.Forms.CheckBox()
        Me.CheckBox39 = New System.Windows.Forms.CheckBox()
        Me.CheckBox19 = New System.Windows.Forms.CheckBox()
        Me.CheckBox9 = New System.Windows.Forms.CheckBox()
        Me.CheckBox88 = New System.Windows.Forms.CheckBox()
        Me.CheckBox78 = New System.Windows.Forms.CheckBox()
        Me.CheckBox68 = New System.Windows.Forms.CheckBox()
        Me.CheckBox58 = New System.Windows.Forms.CheckBox()
        Me.CheckBox48 = New System.Windows.Forms.CheckBox()
        Me.CheckBox38 = New System.Windows.Forms.CheckBox()
        Me.CheckBox18 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox87 = New System.Windows.Forms.CheckBox()
        Me.CheckBox77 = New System.Windows.Forms.CheckBox()
        Me.CheckBox67 = New System.Windows.Forms.CheckBox()
        Me.CheckBox57 = New System.Windows.Forms.CheckBox()
        Me.CheckBox47 = New System.Windows.Forms.CheckBox()
        Me.CheckBox37 = New System.Windows.Forms.CheckBox()
        Me.CheckBox17 = New System.Windows.Forms.CheckBox()
        Me.CheckBox86 = New System.Windows.Forms.CheckBox()
        Me.CheckBox76 = New System.Windows.Forms.CheckBox()
        Me.CheckBox66 = New System.Windows.Forms.CheckBox()
        Me.CheckBox56 = New System.Windows.Forms.CheckBox()
        Me.CheckBox46 = New System.Windows.Forms.CheckBox()
        Me.CheckBox36 = New System.Windows.Forms.CheckBox()
        Me.CheckBox16 = New System.Windows.Forms.CheckBox()
        Me.CheckBox10 = New System.Windows.Forms.CheckBox()
        Me.CheckBox85 = New System.Windows.Forms.CheckBox()
        Me.CheckBox75 = New System.Windows.Forms.CheckBox()
        Me.CheckBox65 = New System.Windows.Forms.CheckBox()
        Me.CheckBox55 = New System.Windows.Forms.CheckBox()
        Me.CheckBox45 = New System.Windows.Forms.CheckBox()
        Me.CheckBox35 = New System.Windows.Forms.CheckBox()
        Me.CheckBox15 = New System.Windows.Forms.CheckBox()
        Me.CheckBox7 = New System.Windows.Forms.CheckBox()
        Me.CheckBox84 = New System.Windows.Forms.CheckBox()
        Me.CheckBox74 = New System.Windows.Forms.CheckBox()
        Me.CheckBox64 = New System.Windows.Forms.CheckBox()
        Me.CheckBox54 = New System.Windows.Forms.CheckBox()
        Me.CheckBox44 = New System.Windows.Forms.CheckBox()
        Me.CheckBox34 = New System.Windows.Forms.CheckBox()
        Me.CheckBox14 = New System.Windows.Forms.CheckBox()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.CheckBox83 = New System.Windows.Forms.CheckBox()
        Me.CheckBox73 = New System.Windows.Forms.CheckBox()
        Me.CheckBox63 = New System.Windows.Forms.CheckBox()
        Me.CheckBox53 = New System.Windows.Forms.CheckBox()
        Me.CheckBox43 = New System.Windows.Forms.CheckBox()
        Me.CheckBox33 = New System.Windows.Forms.CheckBox()
        Me.CheckBox13 = New System.Windows.Forms.CheckBox()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox82 = New System.Windows.Forms.CheckBox()
        Me.CheckBox72 = New System.Windows.Forms.CheckBox()
        Me.CheckBox62 = New System.Windows.Forms.CheckBox()
        Me.CheckBox52 = New System.Windows.Forms.CheckBox()
        Me.CheckBox42 = New System.Windows.Forms.CheckBox()
        Me.CheckBox32 = New System.Windows.Forms.CheckBox()
        Me.CheckBox12 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CheckBox81 = New System.Windows.Forms.CheckBox()
        Me.CheckBox71 = New System.Windows.Forms.CheckBox()
        Me.CheckBox61 = New System.Windows.Forms.CheckBox()
        Me.CheckBox51 = New System.Windows.Forms.CheckBox()
        Me.CheckBox41 = New System.Windows.Forms.CheckBox()
        Me.CheckBox31 = New System.Windows.Forms.CheckBox()
        Me.CheckBox11 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Dgd_Tick = New System.Windows.Forms.DataGridView()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.LB_STAT_5 = New System.Windows.Forms.Label()
        Me.LB_STAT_4 = New System.Windows.Forms.Label()
        Me.LB_STAT_3 = New System.Windows.Forms.Label()
        Me.LB_STAT_2 = New System.Windows.Forms.Label()
        Me.LB_STAT_1 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.PbTickGagnant = New System.Windows.Forms.PictureBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TbIdticket = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.CheckBox21 = New System.Windows.Forms.CheckBox()
        Me.CheckBox30 = New System.Windows.Forms.CheckBox()
        Me.CheckBox29 = New System.Windows.Forms.CheckBox()
        Me.CheckBox22 = New System.Windows.Forms.CheckBox()
        Me.CheckBox28 = New System.Windows.Forms.CheckBox()
        Me.CheckBox23 = New System.Windows.Forms.CheckBox()
        Me.CheckBox24 = New System.Windows.Forms.CheckBox()
        Me.CheckBox25 = New System.Windows.Forms.CheckBox()
        Me.CheckBox26 = New System.Windows.Forms.CheckBox()
        Me.CheckBox27 = New System.Windows.Forms.CheckBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Lb_Min = New System.Windows.Forms.Label()
        Me.LB_second = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PbVoyant = New System.Windows.Forms.PictureBox()
        Me.playerLabl = New System.Windows.Forms.Label()
        Me.lbCredit = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.legendPanel = New System.Windows.Forms.Panel()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.CheckBox91 = New System.Windows.Forms.CheckBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Dgv_LastTick = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.jackpanel = New System.Windows.Forms.Panel()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Lb_iron_pot_name = New System.Windows.Forms.Label()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Lb_iron_pot = New System.Windows.Forms.Label()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.gpot_name_lbl = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.gpot_lbl = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.RadioButton16 = New System.Windows.Forms.RadioButton()
        Me.RadioButton15 = New System.Windows.Forms.RadioButton()
        Me.RadioButton14 = New System.Windows.Forms.RadioButton()
        Me.RadioButton13 = New System.Windows.Forms.RadioButton()
        Me.AxWindowsMediaPlayer1 = New AxWMPLib.AxWindowsMediaPlayer()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1.SuspendLayout()
        CType(Me.Dgd_Tick, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.PbTickGagnant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PbVoyant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.Panel23.SuspendLayout()
        Me.legendPanel.SuspendLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.Dgv_LastTick, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.jackpanel.SuspendLayout()
        Me.Panel18.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel14.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bt_print
        '
        Me.bt_print.BackColor = System.Drawing.Color.DarkGreen
        Me.bt_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bt_print.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt_print.ForeColor = System.Drawing.Color.White
        Me.bt_print.Location = New System.Drawing.Point(1416, 1333)
        Me.bt_print.Margin = New System.Windows.Forms.Padding(6)
        Me.bt_print.Name = "bt_print"
        Me.bt_print.Size = New System.Drawing.Size(32, 67)
        Me.bt_print.TabIndex = 9
        Me.bt_print.UseVisualStyleBackColor = False
        Me.bt_print.Visible = False
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Black
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(758, 200)
        Me.Label2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Padding = New System.Windows.Forms.Padding(10)
        Me.Label2.Size = New System.Drawing.Size(20, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tirage No:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.SetColumnSpan(Me.GroupBox1, 3)
        Me.GroupBox1.Controls.Add(Me.Button7)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Button6)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(Me.Lb_Mise)
        Me.GroupBox1.Controls.Add(Me.RadioButton12)
        Me.GroupBox1.Controls.Add(Me.RadioButton7)
        Me.GroupBox1.Controls.Add(Me.RadioButton11)
        Me.GroupBox1.Controls.Add(Me.RadioButton9)
        Me.GroupBox1.Controls.Add(Me.RadioButton6)
        Me.GroupBox1.Controls.Add(Me.RadioButton10)
        Me.GroupBox1.Controls.Add(Me.RadioButton8)
        Me.GroupBox1.Controls.Add(Me.RadioButton5)
        Me.GroupBox1.Controls.Add(Me.RadioButton4)
        Me.GroupBox1.Controls.Add(Me.LB_NumTirage)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(6, 599)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox1.Size = New System.Drawing.Size(874, 142)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.Crimson
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.White
        Me.Button7.Location = New System.Drawing.Point(776, 56)
        Me.Button7.Margin = New System.Windows.Forms.Padding(6)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(80, 60)
        Me.Button7.TabIndex = 7
        Me.Button7.Text = "C"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Black
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(8, 46)
        Me.Label5.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(300, 69)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Mise"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkGreen
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.Button6.Location = New System.Drawing.Point(706, 85)
        Me.Button6.Margin = New System.Windows.Forms.Padding(6)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(52, 44)
        Me.Button6.TabIndex = 5
        Me.Button6.Text = "-"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkGreen
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.Button5.Location = New System.Drawing.Point(706, 29)
        Me.Button5.Margin = New System.Windows.Forms.Padding(6)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(52, 44)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "+"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Lb_Mise
        '
        Me.Lb_Mise.BackColor = System.Drawing.Color.White
        Me.Lb_Mise.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lb_Mise.ForeColor = System.Drawing.Color.DarkGreen
        Me.Lb_Mise.Location = New System.Drawing.Point(318, 48)
        Me.Lb_Mise.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Lb_Mise.Name = "Lb_Mise"
        Me.Lb_Mise.Size = New System.Drawing.Size(366, 67)
        Me.Lb_Mise.TabIndex = 4
        Me.Lb_Mise.Text = "100"
        Me.Lb_Mise.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'RadioButton12
        '
        Me.RadioButton12.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton12.BackColor = System.Drawing.Color.Silver
        Me.RadioButton12.Enabled = False
        Me.RadioButton12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton12.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton12.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton12.Location = New System.Drawing.Point(790, 231)
        Me.RadioButton12.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton12.Name = "RadioButton12"
        Me.RadioButton12.Size = New System.Drawing.Size(20, 19)
        Me.RadioButton12.TabIndex = 3
        Me.RadioButton12.Text = "2000"
        Me.RadioButton12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton12.UseVisualStyleBackColor = False
        Me.RadioButton12.Visible = False
        '
        'RadioButton7
        '
        Me.RadioButton7.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton7.BackColor = System.Drawing.Color.Silver
        Me.RadioButton7.Enabled = False
        Me.RadioButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton7.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton7.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton7.Location = New System.Drawing.Point(790, 256)
        Me.RadioButton7.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(20, 19)
        Me.RadioButton7.TabIndex = 3
        Me.RadioButton7.Text = "500"
        Me.RadioButton7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton7.UseVisualStyleBackColor = False
        Me.RadioButton7.Visible = False
        '
        'RadioButton11
        '
        Me.RadioButton11.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton11.BackColor = System.Drawing.Color.Silver
        Me.RadioButton11.Enabled = False
        Me.RadioButton11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton11.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton11.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton11.Location = New System.Drawing.Point(758, 262)
        Me.RadioButton11.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton11.Name = "RadioButton11"
        Me.RadioButton11.Size = New System.Drawing.Size(20, 19)
        Me.RadioButton11.TabIndex = 3
        Me.RadioButton11.Text = "3000"
        Me.RadioButton11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton11.UseVisualStyleBackColor = False
        Me.RadioButton11.Visible = False
        '
        'RadioButton9
        '
        Me.RadioButton9.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton9.BackColor = System.Drawing.Color.Silver
        Me.RadioButton9.Enabled = False
        Me.RadioButton9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton9.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton9.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton9.Location = New System.Drawing.Point(758, 231)
        Me.RadioButton9.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton9.Name = "RadioButton9"
        Me.RadioButton9.Size = New System.Drawing.Size(20, 19)
        Me.RadioButton9.TabIndex = 3
        Me.RadioButton9.Text = "1500"
        Me.RadioButton9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton9.UseVisualStyleBackColor = False
        Me.RadioButton9.Visible = False
        '
        'RadioButton6
        '
        Me.RadioButton6.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton6.BackColor = System.Drawing.Color.Silver
        Me.RadioButton6.Enabled = False
        Me.RadioButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton6.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton6.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton6.Location = New System.Drawing.Point(758, 256)
        Me.RadioButton6.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(20, 19)
        Me.RadioButton6.TabIndex = 3
        Me.RadioButton6.Text = "300"
        Me.RadioButton6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton6.UseVisualStyleBackColor = False
        Me.RadioButton6.Visible = False
        '
        'RadioButton10
        '
        Me.RadioButton10.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton10.BackColor = System.Drawing.Color.Silver
        Me.RadioButton10.Enabled = False
        Me.RadioButton10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton10.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton10.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton10.Location = New System.Drawing.Point(726, 262)
        Me.RadioButton10.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(20, 19)
        Me.RadioButton10.TabIndex = 3
        Me.RadioButton10.Text = "2500"
        Me.RadioButton10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton10.UseVisualStyleBackColor = False
        Me.RadioButton10.Visible = False
        '
        'RadioButton8
        '
        Me.RadioButton8.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton8.BackColor = System.Drawing.Color.Silver
        Me.RadioButton8.Enabled = False
        Me.RadioButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton8.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton8.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton8.Location = New System.Drawing.Point(726, 231)
        Me.RadioButton8.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(20, 19)
        Me.RadioButton8.TabIndex = 3
        Me.RadioButton8.Text = "1000"
        Me.RadioButton8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton8.UseVisualStyleBackColor = False
        Me.RadioButton8.Visible = False
        '
        'RadioButton5
        '
        Me.RadioButton5.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton5.BackColor = System.Drawing.Color.Silver
        Me.RadioButton5.Enabled = False
        Me.RadioButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton5.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton5.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton5.Location = New System.Drawing.Point(726, 200)
        Me.RadioButton5.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(20, 19)
        Me.RadioButton5.TabIndex = 3
        Me.RadioButton5.Text = "200"
        Me.RadioButton5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton5.UseVisualStyleBackColor = False
        Me.RadioButton5.Visible = False
        '
        'RadioButton4
        '
        Me.RadioButton4.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton4.BackColor = System.Drawing.Color.Green
        Me.RadioButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton4.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton4.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton4.Location = New System.Drawing.Point(790, 262)
        Me.RadioButton4.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(20, 19)
        Me.RadioButton4.TabIndex = 3
        Me.RadioButton4.Text = "100"
        Me.RadioButton4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton4.UseVisualStyleBackColor = False
        Me.RadioButton4.Visible = False
        '
        'LB_NumTirage
        '
        Me.LB_NumTirage.BackColor = System.Drawing.Color.Black
        Me.LB_NumTirage.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_NumTirage.Location = New System.Drawing.Point(790, 200)
        Me.LB_NumTirage.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.LB_NumTirage.Name = "LB_NumTirage"
        Me.LB_NumTirage.Padding = New System.Windows.Forms.Padding(10)
        Me.LB_NumTirage.Size = New System.Drawing.Size(20, 19)
        Me.LB_NumTirage.TabIndex = 2
        Me.LB_NumTirage.Text = "15242123434"
        '
        'BtCancel
        '
        Me.BtCancel.BackColor = System.Drawing.Color.DarkGreen
        Me.BtCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtCancel.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtCancel.ForeColor = System.Drawing.Color.White
        Me.BtCancel.Location = New System.Drawing.Point(1370, 1333)
        Me.BtCancel.Margin = New System.Windows.Forms.Padding(6)
        Me.BtCancel.Name = "BtCancel"
        Me.BtCancel.Size = New System.Drawing.Size(32, 67)
        Me.BtCancel.TabIndex = 12
        Me.BtCancel.Text = "Cancel"
        Me.BtCancel.UseVisualStyleBackColor = False
        Me.BtCancel.Visible = False
        '
        'CheckBox90
        '
        Me.CheckBox90.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox90.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox90.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox90.ForeColor = System.Drawing.Color.White
        Me.CheckBox90.Location = New System.Drawing.Point(1014, 1102)
        Me.CheckBox90.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox90.Name = "CheckBox90"
        Me.CheckBox90.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox90.TabIndex = 4
        Me.CheckBox90.Text = "88"
        Me.CheckBox90.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox90.UseVisualStyleBackColor = True
        '
        'CheckBox80
        '
        Me.CheckBox80.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox80.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox80.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox80.ForeColor = System.Drawing.Color.White
        Me.CheckBox80.Location = New System.Drawing.Point(1014, 965)
        Me.CheckBox80.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox80.Name = "CheckBox80"
        Me.CheckBox80.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox80.TabIndex = 4
        Me.CheckBox80.Text = "78"
        Me.CheckBox80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox80.UseVisualStyleBackColor = True
        '
        'CheckBox70
        '
        Me.CheckBox70.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox70.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox70.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox70.ForeColor = System.Drawing.Color.White
        Me.CheckBox70.Location = New System.Drawing.Point(1014, 828)
        Me.CheckBox70.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox70.Name = "CheckBox70"
        Me.CheckBox70.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox70.TabIndex = 4
        Me.CheckBox70.Text = "68"
        Me.CheckBox70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox70.UseVisualStyleBackColor = True
        '
        'CheckBox60
        '
        Me.CheckBox60.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox60.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox60.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox60.ForeColor = System.Drawing.Color.White
        Me.CheckBox60.Location = New System.Drawing.Point(1014, 691)
        Me.CheckBox60.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox60.Name = "CheckBox60"
        Me.CheckBox60.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox60.TabIndex = 4
        Me.CheckBox60.Text = "58"
        Me.CheckBox60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox60.UseVisualStyleBackColor = True
        '
        'CheckBox50
        '
        Me.CheckBox50.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox50.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox50.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox50.ForeColor = System.Drawing.Color.White
        Me.CheckBox50.Location = New System.Drawing.Point(1014, 554)
        Me.CheckBox50.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox50.Name = "CheckBox50"
        Me.CheckBox50.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox50.TabIndex = 4
        Me.CheckBox50.Text = "48"
        Me.CheckBox50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox50.UseVisualStyleBackColor = True
        '
        'CheckBox40
        '
        Me.CheckBox40.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox40.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox40.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox40.ForeColor = System.Drawing.Color.White
        Me.CheckBox40.Location = New System.Drawing.Point(1014, 417)
        Me.CheckBox40.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox40.Name = "CheckBox40"
        Me.CheckBox40.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox40.TabIndex = 4
        Me.CheckBox40.Text = "38"
        Me.CheckBox40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox40.UseVisualStyleBackColor = True
        '
        'CheckBox20
        '
        Me.CheckBox20.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox20.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox20.ForeColor = System.Drawing.Color.White
        Me.CheckBox20.Location = New System.Drawing.Point(1014, 143)
        Me.CheckBox20.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox20.Name = "CheckBox20"
        Me.CheckBox20.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox20.TabIndex = 4
        Me.CheckBox20.Text = "18"
        Me.CheckBox20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox20.UseVisualStyleBackColor = True
        '
        'CheckBox8
        '
        Me.CheckBox8.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox8.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox8.ForeColor = System.Drawing.Color.White
        Me.CheckBox8.Location = New System.Drawing.Point(1014, 6)
        Me.CheckBox8.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox8.TabIndex = 4
        Me.CheckBox8.Text = "8"
        Me.CheckBox8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox8.UseVisualStyleBackColor = True
        '
        'CheckBox89
        '
        Me.CheckBox89.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox89.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox89.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox89.ForeColor = System.Drawing.Color.White
        Me.CheckBox89.Location = New System.Drawing.Point(1158, 1102)
        Me.CheckBox89.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox89.Name = "CheckBox89"
        Me.CheckBox89.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox89.TabIndex = 3
        Me.CheckBox89.Text = "89"
        Me.CheckBox89.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox89.UseVisualStyleBackColor = True
        '
        'CheckBox79
        '
        Me.CheckBox79.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox79.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox79.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox79.ForeColor = System.Drawing.Color.White
        Me.CheckBox79.Location = New System.Drawing.Point(1158, 965)
        Me.CheckBox79.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox79.Name = "CheckBox79"
        Me.CheckBox79.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox79.TabIndex = 3
        Me.CheckBox79.Text = "79"
        Me.CheckBox79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox79.UseVisualStyleBackColor = True
        '
        'CheckBox69
        '
        Me.CheckBox69.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox69.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox69.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox69.ForeColor = System.Drawing.Color.White
        Me.CheckBox69.Location = New System.Drawing.Point(1158, 828)
        Me.CheckBox69.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox69.Name = "CheckBox69"
        Me.CheckBox69.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox69.TabIndex = 3
        Me.CheckBox69.Text = "69"
        Me.CheckBox69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox69.UseVisualStyleBackColor = True
        '
        'CheckBox59
        '
        Me.CheckBox59.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox59.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox59.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox59.ForeColor = System.Drawing.Color.White
        Me.CheckBox59.Location = New System.Drawing.Point(1158, 691)
        Me.CheckBox59.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox59.Name = "CheckBox59"
        Me.CheckBox59.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox59.TabIndex = 3
        Me.CheckBox59.Text = "59"
        Me.CheckBox59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox59.UseVisualStyleBackColor = True
        '
        'CheckBox49
        '
        Me.CheckBox49.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox49.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox49.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox49.ForeColor = System.Drawing.Color.White
        Me.CheckBox49.Location = New System.Drawing.Point(1158, 554)
        Me.CheckBox49.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox49.Name = "CheckBox49"
        Me.CheckBox49.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox49.TabIndex = 3
        Me.CheckBox49.Text = "49"
        Me.CheckBox49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox49.UseVisualStyleBackColor = True
        '
        'CheckBox39
        '
        Me.CheckBox39.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox39.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox39.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox39.ForeColor = System.Drawing.Color.White
        Me.CheckBox39.Location = New System.Drawing.Point(1158, 417)
        Me.CheckBox39.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox39.Name = "CheckBox39"
        Me.CheckBox39.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox39.TabIndex = 3
        Me.CheckBox39.Text = "39"
        Me.CheckBox39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox39.UseVisualStyleBackColor = True
        '
        'CheckBox19
        '
        Me.CheckBox19.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox19.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox19.ForeColor = System.Drawing.Color.White
        Me.CheckBox19.Location = New System.Drawing.Point(1158, 143)
        Me.CheckBox19.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox19.Name = "CheckBox19"
        Me.CheckBox19.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox19.TabIndex = 3
        Me.CheckBox19.Text = "19"
        Me.CheckBox19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox19.UseVisualStyleBackColor = True
        '
        'CheckBox9
        '
        Me.CheckBox9.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox9.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox9.ForeColor = System.Drawing.Color.White
        Me.CheckBox9.Location = New System.Drawing.Point(1158, 6)
        Me.CheckBox9.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox9.Name = "CheckBox9"
        Me.CheckBox9.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox9.TabIndex = 3
        Me.CheckBox9.Text = "9"
        Me.CheckBox9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox9.UseVisualStyleBackColor = True
        '
        'CheckBox88
        '
        Me.CheckBox88.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox88.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox88.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox88.ForeColor = System.Drawing.Color.White
        Me.CheckBox88.Location = New System.Drawing.Point(294, 1102)
        Me.CheckBox88.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox88.Name = "CheckBox88"
        Me.CheckBox88.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox88.TabIndex = 2
        Me.CheckBox88.Text = "83"
        Me.CheckBox88.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox88.UseVisualStyleBackColor = True
        '
        'CheckBox78
        '
        Me.CheckBox78.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox78.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox78.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox78.ForeColor = System.Drawing.Color.White
        Me.CheckBox78.Location = New System.Drawing.Point(294, 965)
        Me.CheckBox78.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox78.Name = "CheckBox78"
        Me.CheckBox78.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox78.TabIndex = 2
        Me.CheckBox78.Text = "73"
        Me.CheckBox78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox78.UseVisualStyleBackColor = True
        '
        'CheckBox68
        '
        Me.CheckBox68.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox68.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox68.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox68.ForeColor = System.Drawing.Color.White
        Me.CheckBox68.Location = New System.Drawing.Point(294, 828)
        Me.CheckBox68.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox68.Name = "CheckBox68"
        Me.CheckBox68.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox68.TabIndex = 2
        Me.CheckBox68.Text = "63"
        Me.CheckBox68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox68.UseVisualStyleBackColor = True
        '
        'CheckBox58
        '
        Me.CheckBox58.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox58.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox58.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox58.ForeColor = System.Drawing.Color.White
        Me.CheckBox58.Location = New System.Drawing.Point(294, 691)
        Me.CheckBox58.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox58.Name = "CheckBox58"
        Me.CheckBox58.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox58.TabIndex = 2
        Me.CheckBox58.Text = "53"
        Me.CheckBox58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox58.UseVisualStyleBackColor = True
        '
        'CheckBox48
        '
        Me.CheckBox48.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox48.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox48.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox48.ForeColor = System.Drawing.Color.White
        Me.CheckBox48.Location = New System.Drawing.Point(294, 554)
        Me.CheckBox48.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox48.Name = "CheckBox48"
        Me.CheckBox48.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox48.TabIndex = 2
        Me.CheckBox48.Text = "43"
        Me.CheckBox48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox48.UseVisualStyleBackColor = True
        '
        'CheckBox38
        '
        Me.CheckBox38.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox38.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox38.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox38.ForeColor = System.Drawing.Color.White
        Me.CheckBox38.Location = New System.Drawing.Point(294, 417)
        Me.CheckBox38.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox38.Name = "CheckBox38"
        Me.CheckBox38.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox38.TabIndex = 2
        Me.CheckBox38.Text = "33"
        Me.CheckBox38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox38.UseVisualStyleBackColor = True
        '
        'CheckBox18
        '
        Me.CheckBox18.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox18.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox18.ForeColor = System.Drawing.Color.White
        Me.CheckBox18.Location = New System.Drawing.Point(294, 143)
        Me.CheckBox18.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox18.Name = "CheckBox18"
        Me.CheckBox18.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox18.TabIndex = 2
        Me.CheckBox18.Text = "13"
        Me.CheckBox18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox18.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox3.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.ForeColor = System.Drawing.Color.White
        Me.CheckBox3.Location = New System.Drawing.Point(294, 6)
        Me.CheckBox3.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox3.TabIndex = 2
        Me.CheckBox3.Text = "3"
        Me.CheckBox3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox87
        '
        Me.CheckBox87.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox87.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox87.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox87.ForeColor = System.Drawing.Color.White
        Me.CheckBox87.Location = New System.Drawing.Point(1302, 1102)
        Me.CheckBox87.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox87.Name = "CheckBox87"
        Me.CheckBox87.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox87.TabIndex = 1
        Me.CheckBox87.Text = "90"
        Me.CheckBox87.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox87.UseVisualStyleBackColor = True
        '
        'CheckBox77
        '
        Me.CheckBox77.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox77.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox77.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox77.ForeColor = System.Drawing.Color.White
        Me.CheckBox77.Location = New System.Drawing.Point(1302, 965)
        Me.CheckBox77.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox77.Name = "CheckBox77"
        Me.CheckBox77.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox77.TabIndex = 1
        Me.CheckBox77.Text = "80"
        Me.CheckBox77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox77.UseVisualStyleBackColor = True
        '
        'CheckBox67
        '
        Me.CheckBox67.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox67.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox67.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox67.ForeColor = System.Drawing.Color.White
        Me.CheckBox67.Location = New System.Drawing.Point(1302, 828)
        Me.CheckBox67.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox67.Name = "CheckBox67"
        Me.CheckBox67.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox67.TabIndex = 1
        Me.CheckBox67.Text = "70"
        Me.CheckBox67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox67.UseVisualStyleBackColor = True
        '
        'CheckBox57
        '
        Me.CheckBox57.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox57.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox57.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox57.ForeColor = System.Drawing.Color.White
        Me.CheckBox57.Location = New System.Drawing.Point(1302, 691)
        Me.CheckBox57.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox57.Name = "CheckBox57"
        Me.CheckBox57.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox57.TabIndex = 1
        Me.CheckBox57.Text = "60"
        Me.CheckBox57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox57.UseVisualStyleBackColor = True
        '
        'CheckBox47
        '
        Me.CheckBox47.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox47.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox47.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox47.ForeColor = System.Drawing.Color.White
        Me.CheckBox47.Location = New System.Drawing.Point(1302, 554)
        Me.CheckBox47.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox47.Name = "CheckBox47"
        Me.CheckBox47.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox47.TabIndex = 1
        Me.CheckBox47.Text = "50"
        Me.CheckBox47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox47.UseVisualStyleBackColor = True
        '
        'CheckBox37
        '
        Me.CheckBox37.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox37.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox37.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox37.ForeColor = System.Drawing.Color.White
        Me.CheckBox37.Location = New System.Drawing.Point(1302, 417)
        Me.CheckBox37.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox37.Name = "CheckBox37"
        Me.CheckBox37.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox37.TabIndex = 1
        Me.CheckBox37.Text = "40"
        Me.CheckBox37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox37.UseVisualStyleBackColor = True
        '
        'CheckBox17
        '
        Me.CheckBox17.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox17.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox17.ForeColor = System.Drawing.Color.White
        Me.CheckBox17.Location = New System.Drawing.Point(1302, 143)
        Me.CheckBox17.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox17.Name = "CheckBox17"
        Me.CheckBox17.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox17.TabIndex = 1
        Me.CheckBox17.Text = "20"
        Me.CheckBox17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox17.UseVisualStyleBackColor = True
        '
        'CheckBox86
        '
        Me.CheckBox86.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox86.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox86.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox86.ForeColor = System.Drawing.Color.White
        Me.CheckBox86.Location = New System.Drawing.Point(870, 1102)
        Me.CheckBox86.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox86.Name = "CheckBox86"
        Me.CheckBox86.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox86.TabIndex = 1
        Me.CheckBox86.Text = "87"
        Me.CheckBox86.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox86.UseVisualStyleBackColor = True
        '
        'CheckBox76
        '
        Me.CheckBox76.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox76.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox76.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox76.ForeColor = System.Drawing.Color.White
        Me.CheckBox76.Location = New System.Drawing.Point(870, 965)
        Me.CheckBox76.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox76.Name = "CheckBox76"
        Me.CheckBox76.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox76.TabIndex = 1
        Me.CheckBox76.Text = "77"
        Me.CheckBox76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox76.UseVisualStyleBackColor = True
        '
        'CheckBox66
        '
        Me.CheckBox66.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox66.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox66.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox66.ForeColor = System.Drawing.Color.White
        Me.CheckBox66.Location = New System.Drawing.Point(870, 828)
        Me.CheckBox66.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox66.Name = "CheckBox66"
        Me.CheckBox66.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox66.TabIndex = 1
        Me.CheckBox66.Text = "67"
        Me.CheckBox66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox66.UseVisualStyleBackColor = True
        '
        'CheckBox56
        '
        Me.CheckBox56.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox56.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox56.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox56.ForeColor = System.Drawing.Color.White
        Me.CheckBox56.Location = New System.Drawing.Point(870, 691)
        Me.CheckBox56.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox56.Name = "CheckBox56"
        Me.CheckBox56.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox56.TabIndex = 1
        Me.CheckBox56.Text = "57"
        Me.CheckBox56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox56.UseVisualStyleBackColor = True
        '
        'CheckBox46
        '
        Me.CheckBox46.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox46.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox46.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox46.ForeColor = System.Drawing.Color.White
        Me.CheckBox46.Location = New System.Drawing.Point(870, 554)
        Me.CheckBox46.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox46.Name = "CheckBox46"
        Me.CheckBox46.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox46.TabIndex = 1
        Me.CheckBox46.Text = "47"
        Me.CheckBox46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox46.UseVisualStyleBackColor = True
        '
        'CheckBox36
        '
        Me.CheckBox36.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox36.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox36.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox36.ForeColor = System.Drawing.Color.White
        Me.CheckBox36.Location = New System.Drawing.Point(870, 417)
        Me.CheckBox36.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox36.Name = "CheckBox36"
        Me.CheckBox36.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox36.TabIndex = 1
        Me.CheckBox36.Text = "37"
        Me.CheckBox36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox36.UseVisualStyleBackColor = True
        '
        'CheckBox16
        '
        Me.CheckBox16.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox16.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox16.ForeColor = System.Drawing.Color.White
        Me.CheckBox16.Location = New System.Drawing.Point(870, 143)
        Me.CheckBox16.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox16.Name = "CheckBox16"
        Me.CheckBox16.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox16.TabIndex = 1
        Me.CheckBox16.Text = "17"
        Me.CheckBox16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox16.UseVisualStyleBackColor = True
        '
        'CheckBox10
        '
        Me.CheckBox10.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox10.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox10.ForeColor = System.Drawing.Color.White
        Me.CheckBox10.Location = New System.Drawing.Point(1302, 6)
        Me.CheckBox10.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox10.Name = "CheckBox10"
        Me.CheckBox10.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox10.TabIndex = 1
        Me.CheckBox10.Text = "10"
        Me.CheckBox10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox10.UseVisualStyleBackColor = True
        '
        'CheckBox85
        '
        Me.CheckBox85.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox85.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox85.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox85.ForeColor = System.Drawing.Color.White
        Me.CheckBox85.Location = New System.Drawing.Point(726, 1102)
        Me.CheckBox85.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox85.Name = "CheckBox85"
        Me.CheckBox85.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox85.TabIndex = 1
        Me.CheckBox85.Text = "86"
        Me.CheckBox85.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox85.UseVisualStyleBackColor = True
        '
        'CheckBox75
        '
        Me.CheckBox75.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox75.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox75.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox75.ForeColor = System.Drawing.Color.White
        Me.CheckBox75.Location = New System.Drawing.Point(726, 965)
        Me.CheckBox75.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox75.Name = "CheckBox75"
        Me.CheckBox75.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox75.TabIndex = 1
        Me.CheckBox75.Text = "76"
        Me.CheckBox75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox75.UseVisualStyleBackColor = True
        '
        'CheckBox65
        '
        Me.CheckBox65.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox65.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox65.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox65.ForeColor = System.Drawing.Color.White
        Me.CheckBox65.Location = New System.Drawing.Point(726, 828)
        Me.CheckBox65.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox65.Name = "CheckBox65"
        Me.CheckBox65.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox65.TabIndex = 1
        Me.CheckBox65.Text = "66"
        Me.CheckBox65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox65.UseVisualStyleBackColor = True
        '
        'CheckBox55
        '
        Me.CheckBox55.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox55.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox55.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox55.ForeColor = System.Drawing.Color.White
        Me.CheckBox55.Location = New System.Drawing.Point(726, 691)
        Me.CheckBox55.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox55.Name = "CheckBox55"
        Me.CheckBox55.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox55.TabIndex = 1
        Me.CheckBox55.Text = "56"
        Me.CheckBox55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox55.UseVisualStyleBackColor = True
        '
        'CheckBox45
        '
        Me.CheckBox45.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox45.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox45.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox45.ForeColor = System.Drawing.Color.White
        Me.CheckBox45.Location = New System.Drawing.Point(726, 554)
        Me.CheckBox45.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox45.Name = "CheckBox45"
        Me.CheckBox45.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox45.TabIndex = 1
        Me.CheckBox45.Text = "46"
        Me.CheckBox45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox45.UseVisualStyleBackColor = True
        '
        'CheckBox35
        '
        Me.CheckBox35.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox35.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox35.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox35.ForeColor = System.Drawing.Color.White
        Me.CheckBox35.Location = New System.Drawing.Point(726, 417)
        Me.CheckBox35.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox35.Name = "CheckBox35"
        Me.CheckBox35.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox35.TabIndex = 1
        Me.CheckBox35.Text = "36"
        Me.CheckBox35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox35.UseVisualStyleBackColor = True
        '
        'CheckBox15
        '
        Me.CheckBox15.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox15.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox15.ForeColor = System.Drawing.Color.White
        Me.CheckBox15.Location = New System.Drawing.Point(726, 143)
        Me.CheckBox15.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox15.Name = "CheckBox15"
        Me.CheckBox15.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox15.TabIndex = 1
        Me.CheckBox15.Text = "16"
        Me.CheckBox15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox15.UseVisualStyleBackColor = True
        '
        'CheckBox7
        '
        Me.CheckBox7.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox7.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox7.ForeColor = System.Drawing.Color.White
        Me.CheckBox7.Location = New System.Drawing.Point(870, 6)
        Me.CheckBox7.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox7.TabIndex = 1
        Me.CheckBox7.Text = "7"
        Me.CheckBox7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox7.UseVisualStyleBackColor = True
        '
        'CheckBox84
        '
        Me.CheckBox84.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox84.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox84.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox84.ForeColor = System.Drawing.Color.White
        Me.CheckBox84.Location = New System.Drawing.Point(582, 1102)
        Me.CheckBox84.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox84.Name = "CheckBox84"
        Me.CheckBox84.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox84.TabIndex = 1
        Me.CheckBox84.Text = "85"
        Me.CheckBox84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox84.UseVisualStyleBackColor = True
        '
        'CheckBox74
        '
        Me.CheckBox74.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox74.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox74.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox74.ForeColor = System.Drawing.Color.White
        Me.CheckBox74.Location = New System.Drawing.Point(582, 965)
        Me.CheckBox74.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox74.Name = "CheckBox74"
        Me.CheckBox74.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox74.TabIndex = 1
        Me.CheckBox74.Text = "75"
        Me.CheckBox74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox74.UseVisualStyleBackColor = True
        '
        'CheckBox64
        '
        Me.CheckBox64.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox64.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox64.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox64.ForeColor = System.Drawing.Color.White
        Me.CheckBox64.Location = New System.Drawing.Point(582, 828)
        Me.CheckBox64.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox64.Name = "CheckBox64"
        Me.CheckBox64.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox64.TabIndex = 1
        Me.CheckBox64.Text = "65"
        Me.CheckBox64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox64.UseVisualStyleBackColor = True
        '
        'CheckBox54
        '
        Me.CheckBox54.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox54.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox54.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox54.ForeColor = System.Drawing.Color.White
        Me.CheckBox54.Location = New System.Drawing.Point(582, 691)
        Me.CheckBox54.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox54.Name = "CheckBox54"
        Me.CheckBox54.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox54.TabIndex = 1
        Me.CheckBox54.Text = "55"
        Me.CheckBox54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox54.UseVisualStyleBackColor = True
        '
        'CheckBox44
        '
        Me.CheckBox44.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox44.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox44.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox44.ForeColor = System.Drawing.Color.White
        Me.CheckBox44.Location = New System.Drawing.Point(582, 554)
        Me.CheckBox44.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox44.Name = "CheckBox44"
        Me.CheckBox44.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox44.TabIndex = 1
        Me.CheckBox44.Text = "45"
        Me.CheckBox44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox44.UseVisualStyleBackColor = True
        '
        'CheckBox34
        '
        Me.CheckBox34.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox34.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox34.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox34.ForeColor = System.Drawing.Color.White
        Me.CheckBox34.Location = New System.Drawing.Point(582, 417)
        Me.CheckBox34.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox34.Name = "CheckBox34"
        Me.CheckBox34.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox34.TabIndex = 1
        Me.CheckBox34.Text = "35"
        Me.CheckBox34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox34.UseVisualStyleBackColor = True
        '
        'CheckBox14
        '
        Me.CheckBox14.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox14.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox14.ForeColor = System.Drawing.Color.White
        Me.CheckBox14.Location = New System.Drawing.Point(582, 143)
        Me.CheckBox14.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox14.Name = "CheckBox14"
        Me.CheckBox14.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox14.TabIndex = 1
        Me.CheckBox14.Text = "15"
        Me.CheckBox14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox14.UseVisualStyleBackColor = True
        '
        'CheckBox6
        '
        Me.CheckBox6.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox6.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox6.ForeColor = System.Drawing.Color.White
        Me.CheckBox6.Location = New System.Drawing.Point(726, 6)
        Me.CheckBox6.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox6.TabIndex = 1
        Me.CheckBox6.Text = "6"
        Me.CheckBox6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'CheckBox83
        '
        Me.CheckBox83.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox83.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox83.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox83.ForeColor = System.Drawing.Color.White
        Me.CheckBox83.Location = New System.Drawing.Point(438, 1102)
        Me.CheckBox83.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox83.Name = "CheckBox83"
        Me.CheckBox83.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox83.TabIndex = 1
        Me.CheckBox83.Text = "84"
        Me.CheckBox83.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox83.UseVisualStyleBackColor = True
        '
        'CheckBox73
        '
        Me.CheckBox73.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox73.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox73.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox73.ForeColor = System.Drawing.Color.White
        Me.CheckBox73.Location = New System.Drawing.Point(438, 965)
        Me.CheckBox73.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox73.Name = "CheckBox73"
        Me.CheckBox73.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox73.TabIndex = 1
        Me.CheckBox73.Text = "74"
        Me.CheckBox73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox73.UseVisualStyleBackColor = True
        '
        'CheckBox63
        '
        Me.CheckBox63.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox63.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox63.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox63.ForeColor = System.Drawing.Color.White
        Me.CheckBox63.Location = New System.Drawing.Point(438, 828)
        Me.CheckBox63.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox63.Name = "CheckBox63"
        Me.CheckBox63.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox63.TabIndex = 1
        Me.CheckBox63.Text = "64"
        Me.CheckBox63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox63.UseVisualStyleBackColor = True
        '
        'CheckBox53
        '
        Me.CheckBox53.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox53.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox53.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox53.ForeColor = System.Drawing.Color.White
        Me.CheckBox53.Location = New System.Drawing.Point(438, 691)
        Me.CheckBox53.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox53.Name = "CheckBox53"
        Me.CheckBox53.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox53.TabIndex = 1
        Me.CheckBox53.Text = "54"
        Me.CheckBox53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox53.UseVisualStyleBackColor = True
        '
        'CheckBox43
        '
        Me.CheckBox43.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox43.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox43.ForeColor = System.Drawing.Color.White
        Me.CheckBox43.Location = New System.Drawing.Point(438, 554)
        Me.CheckBox43.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox43.Name = "CheckBox43"
        Me.CheckBox43.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox43.TabIndex = 1
        Me.CheckBox43.Text = "44"
        Me.CheckBox43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox43.UseVisualStyleBackColor = True
        '
        'CheckBox33
        '
        Me.CheckBox33.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox33.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox33.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox33.ForeColor = System.Drawing.Color.White
        Me.CheckBox33.Location = New System.Drawing.Point(438, 417)
        Me.CheckBox33.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox33.Name = "CheckBox33"
        Me.CheckBox33.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox33.TabIndex = 1
        Me.CheckBox33.Text = "34"
        Me.CheckBox33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox33.UseVisualStyleBackColor = True
        '
        'CheckBox13
        '
        Me.CheckBox13.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox13.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox13.ForeColor = System.Drawing.Color.White
        Me.CheckBox13.Location = New System.Drawing.Point(438, 143)
        Me.CheckBox13.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox13.Name = "CheckBox13"
        Me.CheckBox13.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox13.TabIndex = 1
        Me.CheckBox13.Text = "14"
        Me.CheckBox13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox13.UseVisualStyleBackColor = True
        '
        'CheckBox5
        '
        Me.CheckBox5.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox5.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox5.ForeColor = System.Drawing.Color.White
        Me.CheckBox5.Location = New System.Drawing.Point(582, 6)
        Me.CheckBox5.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox5.TabIndex = 1
        Me.CheckBox5.Text = "5"
        Me.CheckBox5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'CheckBox82
        '
        Me.CheckBox82.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox82.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox82.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox82.ForeColor = System.Drawing.Color.White
        Me.CheckBox82.Location = New System.Drawing.Point(150, 1102)
        Me.CheckBox82.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox82.Name = "CheckBox82"
        Me.CheckBox82.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox82.TabIndex = 0
        Me.CheckBox82.Text = "82"
        Me.CheckBox82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox82.UseVisualStyleBackColor = True
        '
        'CheckBox72
        '
        Me.CheckBox72.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox72.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox72.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox72.ForeColor = System.Drawing.Color.White
        Me.CheckBox72.Location = New System.Drawing.Point(150, 965)
        Me.CheckBox72.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox72.Name = "CheckBox72"
        Me.CheckBox72.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox72.TabIndex = 0
        Me.CheckBox72.Text = "72"
        Me.CheckBox72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox72.UseVisualStyleBackColor = True
        '
        'CheckBox62
        '
        Me.CheckBox62.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox62.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox62.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox62.ForeColor = System.Drawing.Color.White
        Me.CheckBox62.Location = New System.Drawing.Point(150, 828)
        Me.CheckBox62.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox62.Name = "CheckBox62"
        Me.CheckBox62.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox62.TabIndex = 0
        Me.CheckBox62.Text = "62"
        Me.CheckBox62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox62.UseVisualStyleBackColor = True
        '
        'CheckBox52
        '
        Me.CheckBox52.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox52.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox52.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox52.ForeColor = System.Drawing.Color.White
        Me.CheckBox52.Location = New System.Drawing.Point(150, 691)
        Me.CheckBox52.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox52.Name = "CheckBox52"
        Me.CheckBox52.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox52.TabIndex = 0
        Me.CheckBox52.Text = "52"
        Me.CheckBox52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox52.UseVisualStyleBackColor = True
        '
        'CheckBox42
        '
        Me.CheckBox42.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox42.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox42.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox42.ForeColor = System.Drawing.Color.White
        Me.CheckBox42.Location = New System.Drawing.Point(150, 554)
        Me.CheckBox42.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox42.Name = "CheckBox42"
        Me.CheckBox42.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox42.TabIndex = 0
        Me.CheckBox42.Text = "42"
        Me.CheckBox42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox42.UseVisualStyleBackColor = True
        '
        'CheckBox32
        '
        Me.CheckBox32.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox32.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox32.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox32.ForeColor = System.Drawing.Color.White
        Me.CheckBox32.Location = New System.Drawing.Point(150, 417)
        Me.CheckBox32.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox32.Name = "CheckBox32"
        Me.CheckBox32.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox32.TabIndex = 0
        Me.CheckBox32.Text = "32"
        Me.CheckBox32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox32.UseVisualStyleBackColor = True
        '
        'CheckBox12
        '
        Me.CheckBox12.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox12.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox12.ForeColor = System.Drawing.Color.White
        Me.CheckBox12.Location = New System.Drawing.Point(150, 143)
        Me.CheckBox12.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox12.Name = "CheckBox12"
        Me.CheckBox12.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox12.TabIndex = 0
        Me.CheckBox12.Text = "12"
        Me.CheckBox12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox12.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox4.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.ForeColor = System.Drawing.Color.White
        Me.CheckBox4.Location = New System.Drawing.Point(438, 6)
        Me.CheckBox4.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox4.TabIndex = 1
        Me.CheckBox4.Text = "4"
        Me.CheckBox4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'CheckBox81
        '
        Me.CheckBox81.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox81.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox81.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox81.ForeColor = System.Drawing.Color.White
        Me.CheckBox81.Location = New System.Drawing.Point(6, 1102)
        Me.CheckBox81.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox81.Name = "CheckBox81"
        Me.CheckBox81.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox81.TabIndex = 0
        Me.CheckBox81.Text = "81"
        Me.CheckBox81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox81.UseVisualStyleBackColor = True
        '
        'CheckBox71
        '
        Me.CheckBox71.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox71.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox71.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox71.ForeColor = System.Drawing.Color.White
        Me.CheckBox71.Location = New System.Drawing.Point(6, 965)
        Me.CheckBox71.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox71.Name = "CheckBox71"
        Me.CheckBox71.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox71.TabIndex = 0
        Me.CheckBox71.Text = "71"
        Me.CheckBox71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox71.UseVisualStyleBackColor = True
        '
        'CheckBox61
        '
        Me.CheckBox61.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox61.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox61.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox61.ForeColor = System.Drawing.Color.White
        Me.CheckBox61.Location = New System.Drawing.Point(6, 828)
        Me.CheckBox61.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox61.Name = "CheckBox61"
        Me.CheckBox61.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox61.TabIndex = 0
        Me.CheckBox61.Text = "61"
        Me.CheckBox61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox61.UseVisualStyleBackColor = True
        '
        'CheckBox51
        '
        Me.CheckBox51.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox51.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox51.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox51.ForeColor = System.Drawing.Color.White
        Me.CheckBox51.Location = New System.Drawing.Point(6, 691)
        Me.CheckBox51.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox51.Name = "CheckBox51"
        Me.CheckBox51.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox51.TabIndex = 0
        Me.CheckBox51.Text = "51"
        Me.CheckBox51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox51.UseVisualStyleBackColor = True
        '
        'CheckBox41
        '
        Me.CheckBox41.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox41.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox41.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox41.ForeColor = System.Drawing.Color.White
        Me.CheckBox41.Location = New System.Drawing.Point(6, 554)
        Me.CheckBox41.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox41.Name = "CheckBox41"
        Me.CheckBox41.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox41.TabIndex = 0
        Me.CheckBox41.Text = "41"
        Me.CheckBox41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox41.UseVisualStyleBackColor = True
        '
        'CheckBox31
        '
        Me.CheckBox31.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox31.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox31.ForeColor = System.Drawing.Color.White
        Me.CheckBox31.Location = New System.Drawing.Point(6, 417)
        Me.CheckBox31.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox31.Name = "CheckBox31"
        Me.CheckBox31.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox31.TabIndex = 0
        Me.CheckBox31.Text = "31"
        Me.CheckBox31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox31.UseVisualStyleBackColor = True
        '
        'CheckBox11
        '
        Me.CheckBox11.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox11.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox11.ForeColor = System.Drawing.Color.White
        Me.CheckBox11.Location = New System.Drawing.Point(6, 143)
        Me.CheckBox11.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox11.Name = "CheckBox11"
        Me.CheckBox11.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox11.TabIndex = 0
        Me.CheckBox11.Text = "11"
        Me.CheckBox11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox11.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox2.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.ForeColor = System.Drawing.Color.White
        Me.CheckBox2.Location = New System.Drawing.Point(150, 6)
        Me.CheckBox2.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox2.TabIndex = 0
        Me.CheckBox2.Text = "2"
        Me.CheckBox2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.White
        Me.CheckBox1.Location = New System.Drawing.Point(6, 6)
        Me.CheckBox1.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "1"
        Me.CheckBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton3.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton3.Enabled = False
        Me.RadioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton3.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.ForeColor = System.Drawing.Color.Black
        Me.RadioButton3.Location = New System.Drawing.Point(286, 50)
        Me.RadioButton3.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(108, 60)
        Me.RadioButton3.TabIndex = 3
        Me.RadioButton3.Text = "TRIPLET"
        Me.RadioButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton3.UseVisualStyleBackColor = False
        '
        'RadioButton2
        '
        Me.RadioButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton2.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton2.Enabled = False
        Me.RadioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton2.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.Color.Black
        Me.RadioButton2.Location = New System.Drawing.Point(152, 50)
        Me.RadioButton2.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(108, 60)
        Me.RadioButton2.TabIndex = 3
        Me.RadioButton2.Text = "DOUBLET"
        Me.RadioButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'RadioButton1
        '
        Me.RadioButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton1.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.Black
        Me.RadioButton1.Location = New System.Drawing.Point(9, 50)
        Me.RadioButton1.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(108, 60)
        Me.RadioButton1.TabIndex = 3
        Me.RadioButton1.Text = "SOLO"
        Me.RadioButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'Dgd_Tick
        '
        Me.Dgd_Tick.AllowUserToAddRows = False
        Me.Dgd_Tick.AllowUserToDeleteRows = False
        Me.Dgd_Tick.AllowUserToResizeColumns = False
        Me.Dgd_Tick.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Dgd_Tick.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.Dgd_Tick.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Dgd_Tick.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TableLayoutPanel1.SetColumnSpan(Me.Dgd_Tick, 3)
        Me.Dgd_Tick.Location = New System.Drawing.Point(6, 228)
        Me.Dgd_Tick.Margin = New System.Windows.Forms.Padding(6)
        Me.Dgd_Tick.MultiSelect = False
        Me.Dgd_Tick.Name = "Dgd_Tick"
        Me.Dgd_Tick.ReadOnly = True
        Me.Dgd_Tick.RowHeadersWidth = 20
        Me.Dgd_Tick.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Dgd_Tick.Size = New System.Drawing.Size(874, 359)
        Me.Dgd_Tick.TabIndex = 13
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BackColor = System.Drawing.Color.Black
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(6)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(54, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.SplitContainer1.Panel1.Controls.Add(Me.TableLayoutPanel2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.BtCancel)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bt_print)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(54, Byte), Integer), CType(CType(54, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.SplitContainer1.Panel2.Controls.Add(Me.TableLayoutPanel1)
        Me.SplitContainer1.Size = New System.Drawing.Size(2752, 1548)
        Me.SplitContainer1.SplitterDistance = 1858
        Me.SplitContainer1.SplitterWidth = 8
        Me.SplitContainer1.TabIndex = 15
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 10
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.311966!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.55983!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.52564!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.982906!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.807693!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.311966!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.264957!))
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox2, 0, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox5, 0, 8)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel2, 7, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.playerLabl, 6, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.lbCredit, 6, 8)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel23, 9, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox3, 7, 3)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(6)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 9
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.32469!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.912688!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.05048!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1858, 1548)
        Me.TableLayoutPanel2.TabIndex = 29
        '
        'GroupBox2
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.GroupBox2, 4)
        Me.GroupBox2.Controls.Add(Me.LB_STAT_5)
        Me.GroupBox2.Controls.Add(Me.LB_STAT_4)
        Me.GroupBox2.Controls.Add(Me.LB_STAT_3)
        Me.GroupBox2.Controls.Add(Me.LB_STAT_2)
        Me.GroupBox2.Controls.Add(Me.LB_STAT_1)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(6, 1259)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox2.Size = New System.Drawing.Size(607, 110)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Dernier tirage"
        '
        'LB_STAT_5
        '
        Me.LB_STAT_5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LB_STAT_5.BackColor = System.Drawing.Color.Transparent
        Me.LB_STAT_5.Font = New System.Drawing.Font("Showcard Gothic", 16.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_STAT_5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LB_STAT_5.Location = New System.Drawing.Point(460, 34)
        Me.LB_STAT_5.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.LB_STAT_5.Name = "LB_STAT_5"
        Me.LB_STAT_5.Size = New System.Drawing.Size(84, 62)
        Me.LB_STAT_5.TabIndex = 27
        Me.LB_STAT_5.Text = "99"
        Me.LB_STAT_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_STAT_4
        '
        Me.LB_STAT_4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LB_STAT_4.BackColor = System.Drawing.Color.Transparent
        Me.LB_STAT_4.Font = New System.Drawing.Font("Showcard Gothic", 16.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_STAT_4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LB_STAT_4.Location = New System.Drawing.Point(360, 34)
        Me.LB_STAT_4.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.LB_STAT_4.Name = "LB_STAT_4"
        Me.LB_STAT_4.Size = New System.Drawing.Size(84, 62)
        Me.LB_STAT_4.TabIndex = 26
        Me.LB_STAT_4.Text = "99"
        Me.LB_STAT_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_STAT_3
        '
        Me.LB_STAT_3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LB_STAT_3.BackColor = System.Drawing.Color.Transparent
        Me.LB_STAT_3.Font = New System.Drawing.Font("Showcard Gothic", 16.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_STAT_3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LB_STAT_3.Location = New System.Drawing.Point(260, 34)
        Me.LB_STAT_3.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.LB_STAT_3.Name = "LB_STAT_3"
        Me.LB_STAT_3.Size = New System.Drawing.Size(84, 62)
        Me.LB_STAT_3.TabIndex = 25
        Me.LB_STAT_3.Text = "99"
        Me.LB_STAT_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_STAT_2
        '
        Me.LB_STAT_2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LB_STAT_2.BackColor = System.Drawing.Color.Transparent
        Me.LB_STAT_2.Font = New System.Drawing.Font("Showcard Gothic", 16.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_STAT_2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LB_STAT_2.Location = New System.Drawing.Point(160, 34)
        Me.LB_STAT_2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.LB_STAT_2.Name = "LB_STAT_2"
        Me.LB_STAT_2.Size = New System.Drawing.Size(84, 62)
        Me.LB_STAT_2.TabIndex = 24
        Me.LB_STAT_2.Text = "99"
        Me.LB_STAT_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_STAT_1
        '
        Me.LB_STAT_1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.LB_STAT_1.BackColor = System.Drawing.Color.Transparent
        Me.LB_STAT_1.Font = New System.Drawing.Font("Showcard Gothic", 16.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_STAT_1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LB_STAT_1.Location = New System.Drawing.Point(60, 34)
        Me.LB_STAT_1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.LB_STAT_1.Name = "LB_STAT_1"
        Me.LB_STAT_1.Size = New System.Drawing.Size(84, 62)
        Me.LB_STAT_1.TabIndex = 23
        Me.LB_STAT_1.Text = "99"
        Me.LB_STAT_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox5
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.GroupBox5, 4)
        Me.GroupBox5.Controls.Add(Me.PbTickGagnant)
        Me.GroupBox5.Controls.Add(Me.Button3)
        Me.GroupBox5.Controls.Add(Me.TbIdticket)
        Me.GroupBox5.Font = New System.Drawing.Font("Century Gothic", 10.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.White
        Me.GroupBox5.Location = New System.Drawing.Point(6, 1381)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox5.Size = New System.Drawing.Size(607, 137)
        Me.GroupBox5.TabIndex = 13
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Ticket"
        '
        'PbTickGagnant
        '
        Me.PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_cancel1
        Me.PbTickGagnant.Location = New System.Drawing.Point(518, 46)
        Me.PbTickGagnant.Margin = New System.Windows.Forms.Padding(6)
        Me.PbTickGagnant.Name = "PbTickGagnant"
        Me.PbTickGagnant.Size = New System.Drawing.Size(64, 63)
        Me.PbTickGagnant.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PbTickGagnant.TabIndex = 4
        Me.PbTickGagnant.TabStop = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkGreen
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Image = Global.PL_Lotto.My.Resources.Resources.search
        Me.Button3.Location = New System.Drawing.Point(420, 46)
        Me.Button3.Margin = New System.Windows.Forms.Padding(6)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(86, 63)
        Me.Button3.TabIndex = 3
        Me.Button3.UseVisualStyleBackColor = False
        '
        'TbIdticket
        '
        Me.TbIdticket.ForeColor = System.Drawing.Color.Black
        Me.TbIdticket.Location = New System.Drawing.Point(18, 52)
        Me.TbIdticket.Margin = New System.Windows.Forms.Padding(6)
        Me.TbIdticket.Name = "TbIdticket"
        Me.TbIdticket.Size = New System.Drawing.Size(390, 41)
        Me.TbIdticket.TabIndex = 2
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TableLayoutPanel2.SetColumnSpan(Me.Panel3, 6)
        Me.Panel3.Controls.Add(Me.TableLayoutPanel3)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.ForeColor = System.Drawing.Color.White
        Me.Panel3.Location = New System.Drawing.Point(6, 6)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel3.Name = "Panel3"
        Me.TableLayoutPanel2.SetRowSpan(Me.Panel3, 7)
        Me.Panel3.Size = New System.Drawing.Size(1446, 1241)
        Me.Panel3.TabIndex = 32
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 10
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox1, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox87, 9, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox89, 8, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox90, 7, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox2, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox80, 7, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox88, 2, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox86, 6, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox83, 3, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox85, 5, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox84, 4, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox81, 0, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox82, 1, 8)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox79, 8, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox3, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox70, 7, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox71, 0, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox77, 9, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox4, 3, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox61, 0, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox78, 2, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox72, 1, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox73, 3, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox60, 7, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox69, 8, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox76, 6, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox5, 4, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox74, 4, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox51, 0, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox75, 5, 7)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox50, 7, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox62, 1, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox11, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox41, 0, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox59, 8, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox67, 9, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox63, 3, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox68, 2, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox40, 7, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox21, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox31, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox52, 1, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox64, 4, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox6, 5, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox66, 6, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox30, 7, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox49, 8, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox7, 6, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox65, 5, 6)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox8, 7, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox53, 3, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox58, 2, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox57, 9, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox9, 8, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox42, 1, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox10, 9, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox20, 7, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox39, 8, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox54, 4, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox12, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox18, 2, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox13, 3, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox32, 1, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox43, 3, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox56, 6, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox14, 4, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox55, 5, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox15, 5, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox16, 6, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox47, 9, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox29, 8, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox22, 1, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox19, 8, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox17, 9, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox28, 2, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox44, 4, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox23, 3, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox33, 3, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox38, 2, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox24, 4, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox25, 5, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox26, 6, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox27, 9, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox46, 6, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox37, 9, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox45, 5, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox34, 4, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox35, 5, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox36, 6, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBox48, 2, 4)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 9
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1444, 1239)
        Me.TableLayoutPanel3.TabIndex = 5
        '
        'CheckBox21
        '
        Me.CheckBox21.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox21.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox21.ForeColor = System.Drawing.Color.White
        Me.CheckBox21.Location = New System.Drawing.Point(6, 280)
        Me.CheckBox21.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox21.Name = "CheckBox21"
        Me.CheckBox21.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox21.TabIndex = 0
        Me.CheckBox21.Text = "21"
        Me.CheckBox21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox21.UseVisualStyleBackColor = True
        '
        'CheckBox30
        '
        Me.CheckBox30.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox30.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox30.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox30.ForeColor = System.Drawing.Color.White
        Me.CheckBox30.Location = New System.Drawing.Point(1014, 280)
        Me.CheckBox30.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox30.Name = "CheckBox30"
        Me.CheckBox30.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox30.TabIndex = 4
        Me.CheckBox30.Text = "28"
        Me.CheckBox30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox30.UseVisualStyleBackColor = True
        '
        'CheckBox29
        '
        Me.CheckBox29.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox29.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox29.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox29.ForeColor = System.Drawing.Color.White
        Me.CheckBox29.Location = New System.Drawing.Point(1158, 280)
        Me.CheckBox29.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox29.Name = "CheckBox29"
        Me.CheckBox29.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox29.TabIndex = 3
        Me.CheckBox29.Text = "29"
        Me.CheckBox29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox29.UseVisualStyleBackColor = True
        '
        'CheckBox22
        '
        Me.CheckBox22.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox22.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox22.ForeColor = System.Drawing.Color.White
        Me.CheckBox22.Location = New System.Drawing.Point(150, 280)
        Me.CheckBox22.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox22.Name = "CheckBox22"
        Me.CheckBox22.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox22.TabIndex = 0
        Me.CheckBox22.Text = "22"
        Me.CheckBox22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox22.UseVisualStyleBackColor = True
        '
        'CheckBox28
        '
        Me.CheckBox28.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox28.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox28.ForeColor = System.Drawing.Color.White
        Me.CheckBox28.Location = New System.Drawing.Point(294, 280)
        Me.CheckBox28.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox28.Name = "CheckBox28"
        Me.CheckBox28.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox28.TabIndex = 2
        Me.CheckBox28.Text = "23"
        Me.CheckBox28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox28.UseVisualStyleBackColor = True
        '
        'CheckBox23
        '
        Me.CheckBox23.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox23.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox23.ForeColor = System.Drawing.Color.White
        Me.CheckBox23.Location = New System.Drawing.Point(438, 280)
        Me.CheckBox23.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox23.Name = "CheckBox23"
        Me.CheckBox23.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox23.TabIndex = 1
        Me.CheckBox23.Text = "24"
        Me.CheckBox23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox23.UseVisualStyleBackColor = True
        '
        'CheckBox24
        '
        Me.CheckBox24.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox24.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox24.ForeColor = System.Drawing.Color.White
        Me.CheckBox24.Location = New System.Drawing.Point(582, 280)
        Me.CheckBox24.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox24.Name = "CheckBox24"
        Me.CheckBox24.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox24.TabIndex = 1
        Me.CheckBox24.Text = "25"
        Me.CheckBox24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox24.UseVisualStyleBackColor = True
        '
        'CheckBox25
        '
        Me.CheckBox25.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox25.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox25.ForeColor = System.Drawing.Color.White
        Me.CheckBox25.Location = New System.Drawing.Point(726, 280)
        Me.CheckBox25.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox25.Name = "CheckBox25"
        Me.CheckBox25.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox25.TabIndex = 1
        Me.CheckBox25.Text = "26"
        Me.CheckBox25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox25.UseVisualStyleBackColor = True
        '
        'CheckBox26
        '
        Me.CheckBox26.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox26.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox26.ForeColor = System.Drawing.Color.White
        Me.CheckBox26.Location = New System.Drawing.Point(870, 280)
        Me.CheckBox26.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox26.Name = "CheckBox26"
        Me.CheckBox26.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox26.TabIndex = 1
        Me.CheckBox26.Text = "27"
        Me.CheckBox26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox26.UseVisualStyleBackColor = True
        '
        'CheckBox27
        '
        Me.CheckBox27.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox27.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox27.ForeColor = System.Drawing.Color.White
        Me.CheckBox27.Location = New System.Drawing.Point(1302, 280)
        Me.CheckBox27.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox27.Name = "CheckBox27"
        Me.CheckBox27.Size = New System.Drawing.Size(118, 106)
        Me.CheckBox27.TabIndex = 1
        Me.CheckBox27.Text = "30"
        Me.CheckBox27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox27.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TableLayoutPanel2.SetColumnSpan(Me.Panel2, 3)
        Me.Panel2.Controls.Add(Me.Lb_Min)
        Me.Panel2.Controls.Add(Me.LB_second)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.PbVoyant)
        Me.Panel2.Location = New System.Drawing.Point(1575, 41)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(277, 90)
        Me.Panel2.TabIndex = 30
        '
        'Lb_Min
        '
        Me.Lb_Min.AutoSize = True
        Me.Lb_Min.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lb_Min.ForeColor = System.Drawing.Color.White
        Me.Lb_Min.Location = New System.Drawing.Point(116, 19)
        Me.Lb_Min.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Lb_Min.Name = "Lb_Min"
        Me.Lb_Min.Size = New System.Drawing.Size(51, 56)
        Me.Lb_Min.TabIndex = 27
        Me.Lb_Min.Text = "0"
        Me.Lb_Min.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LB_second
        '
        Me.LB_second.AutoSize = True
        Me.LB_second.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_second.ForeColor = System.Drawing.Color.White
        Me.LB_second.Location = New System.Drawing.Point(210, 19)
        Me.LB_second.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.LB_second.Name = "LB_second"
        Me.LB_second.Size = New System.Drawing.Size(78, 56)
        Me.LB_second.TabIndex = 26
        Me.LB_second.Text = "35"
        Me.LB_second.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(170, 15)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 56)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = ":"
        '
        'PbVoyant
        '
        Me.PbVoyant.Image = Global.PL_Lotto.My.Resources.Resources.Vvert
        Me.PbVoyant.Location = New System.Drawing.Point(12, 10)
        Me.PbVoyant.Margin = New System.Windows.Forms.Padding(6)
        Me.PbVoyant.Name = "PbVoyant"
        Me.PbVoyant.Size = New System.Drawing.Size(90, 73)
        Me.PbVoyant.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PbVoyant.TabIndex = 15
        Me.PbVoyant.TabStop = False
        '
        'playerLabl
        '
        Me.playerLabl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.playerLabl.BackColor = System.Drawing.Color.Black
        Me.TableLayoutPanel2.SetColumnSpan(Me.playerLabl, 4)
        Me.playerLabl.Font = New System.Drawing.Font("Century Gothic", 16.125!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.playerLabl.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.playerLabl.Location = New System.Drawing.Point(1464, 1304)
        Me.playerLabl.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.playerLabl.Name = "playerLabl"
        Me.playerLabl.Size = New System.Drawing.Size(388, 71)
        Me.playerLabl.TabIndex = 18
        Me.playerLabl.Text = "#LOGPOM1"
        Me.playerLabl.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbCredit
        '
        Me.lbCredit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbCredit.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel2.SetColumnSpan(Me.lbCredit, 4)
        Me.lbCredit.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbCredit.ForeColor = System.Drawing.Color.Green
        Me.lbCredit.Location = New System.Drawing.Point(1464, 1375)
        Me.lbCredit.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lbCredit.Name = "lbCredit"
        Me.lbCredit.Size = New System.Drawing.Size(388, 94)
        Me.lbCredit.TabIndex = 33
        Me.lbCredit.Text = "0"
        Me.lbCredit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox3
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.GroupBox3, 3)
        Me.GroupBox3.Controls.Add(Me.RadioButton1)
        Me.GroupBox3.Controls.Add(Me.RadioButton3)
        Me.GroupBox3.Controls.Add(Me.RadioButton2)
        Me.GroupBox3.Location = New System.Drawing.Point(1572, 519)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(241, 148)
        Me.GroupBox3.TabIndex = 34
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "GroupBox3"
        Me.GroupBox3.Visible = False
        '
        'Panel23
        '
        Me.Panel23.BackColor = System.Drawing.Color.Black
        Me.Panel23.Controls.Add(Me.legendPanel)
        Me.Panel23.Controls.Add(Me.Label6)
        Me.Panel23.Location = New System.Drawing.Point(1725, 866)
        Me.Panel23.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel23.Name = "Panel23"
        Me.TableLayoutPanel2.SetRowSpan(Me.Panel23, 2)
        Me.Panel23.Size = New System.Drawing.Size(127, 252)
        Me.Panel23.TabIndex = 31
        Me.Panel23.Visible = False
        '
        'legendPanel
        '
        Me.legendPanel.BackColor = System.Drawing.Color.White
        Me.legendPanel.Controls.Add(Me.Label26)
        Me.legendPanel.Controls.Add(Me.Label24)
        Me.legendPanel.Controls.Add(Me.Label22)
        Me.legendPanel.Controls.Add(Me.Label20)
        Me.legendPanel.Controls.Add(Me.Label18)
        Me.legendPanel.Controls.Add(Me.Label16)
        Me.legendPanel.Controls.Add(Me.Label14)
        Me.legendPanel.Controls.Add(Me.Label25)
        Me.legendPanel.Controls.Add(Me.Label23)
        Me.legendPanel.Controls.Add(Me.Label12)
        Me.legendPanel.Controls.Add(Me.Label21)
        Me.legendPanel.Controls.Add(Me.Label10)
        Me.legendPanel.Controls.Add(Me.Label19)
        Me.legendPanel.Controls.Add(Me.Label8)
        Me.legendPanel.Controls.Add(Me.Label17)
        Me.legendPanel.Controls.Add(Me.Label15)
        Me.legendPanel.Controls.Add(Me.PictureBox15)
        Me.legendPanel.Controls.Add(Me.PictureBox14)
        Me.legendPanel.Controls.Add(Me.Label13)
        Me.legendPanel.Controls.Add(Me.PictureBox13)
        Me.legendPanel.Controls.Add(Me.Label11)
        Me.legendPanel.Controls.Add(Me.PictureBox12)
        Me.legendPanel.Controls.Add(Me.Label9)
        Me.legendPanel.Controls.Add(Me.PictureBox11)
        Me.legendPanel.Controls.Add(Me.PictureBox10)
        Me.legendPanel.Controls.Add(Me.Label4)
        Me.legendPanel.Controls.Add(Me.PictureBox9)
        Me.legendPanel.Controls.Add(Me.PictureBox8)
        Me.legendPanel.Controls.Add(Me.PictureBox7)
        Me.legendPanel.Controls.Add(Me.PictureBox4)
        Me.legendPanel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.legendPanel.Location = New System.Drawing.Point(4, 94)
        Me.legendPanel.Margin = New System.Windows.Forms.Padding(6)
        Me.legendPanel.Name = "legendPanel"
        Me.legendPanel.Size = New System.Drawing.Size(5318, 165)
        Me.legendPanel.TabIndex = 0
        '
        'Label26
        '
        Me.Label26.Font = New System.Drawing.Font("Impact", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Red
        Me.Label26.Location = New System.Drawing.Point(5014, 90)
        Me.Label26.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(282, 62)
        Me.Label26.TabIndex = 2
        Me.Label26.Text = "10 000 FCFA"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label24
        '
        Me.Label24.Font = New System.Drawing.Font("Impact", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Orange
        Me.Label24.Location = New System.Drawing.Point(4480, 92)
        Me.Label24.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(282, 62)
        Me.Label24.TabIndex = 2
        Me.Label24.Text = "20 000 FCFA"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label22
        '
        Me.Label22.Font = New System.Drawing.Font("Impact", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Green
        Me.Label22.Location = New System.Drawing.Point(3948, 90)
        Me.Label22.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(282, 62)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "30 000 FCFA"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Impact", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Orange
        Me.Label20.Location = New System.Drawing.Point(3406, 90)
        Me.Label20.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(282, 62)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "100 000 FCFA"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label18
        '
        Me.Label18.Font = New System.Drawing.Font("Impact", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Red
        Me.Label18.Location = New System.Drawing.Point(2864, 90)
        Me.Label18.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(282, 62)
        Me.Label18.TabIndex = 2
        Me.Label18.Text = "100 000 FCFA"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Impact", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Green
        Me.Label16.Location = New System.Drawing.Point(2314, 90)
        Me.Label16.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(282, 62)
        Me.Label16.TabIndex = 2
        Me.Label16.Text = "200 000 FCFA"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Impact", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Red
        Me.Label14.Location = New System.Drawing.Point(1784, 90)
        Me.Label14.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(282, 62)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "300 000 FCFA"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label25
        '
        Me.Label25.BackColor = System.Drawing.Color.Black
        Me.Label25.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(5016, 17)
        Me.Label25.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(278, 62)
        Me.Label25.TabIndex = 2
        Me.Label25.Text = "Gareth Bale"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.Color.Black
        Me.Label23.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(4482, 19)
        Me.Label23.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(278, 62)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "Griezman"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Impact", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Green
        Me.Label12.Location = New System.Drawing.Point(1256, 90)
        Me.Label12.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(282, 62)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "400 000 FCFA"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.Color.Black
        Me.Label21.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(3952, 17)
        Me.Label21.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(278, 62)
        Me.Label21.TabIndex = 2
        Me.Label21.Text = "Neymar"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Impact", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Orange
        Me.Label10.Location = New System.Drawing.Point(728, 90)
        Me.Label10.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(282, 62)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "500 000 FCFA"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.Color.Black
        Me.Label19.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(3410, 17)
        Me.Label19.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(278, 62)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "CR7"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Impact", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Red
        Me.Label8.Location = New System.Drawing.Point(218, 90)
        Me.Label8.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(282, 62)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "1 000 000 FCFA"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.Black
        Me.Label17.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(2868, 17)
        Me.Label17.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(278, 62)
        Me.Label17.TabIndex = 2
        Me.Label17.Text = "Lionel Messi"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Black
        Me.Label15.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(2318, 17)
        Me.Label15.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(278, 62)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Roger Milla"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox15
        '
        Me.PictureBox15.Image = Global.PL_Lotto.My.Resources.Resources.bale
        Me.PictureBox15.Location = New System.Drawing.Point(4796, 4)
        Me.PictureBox15.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(176, 152)
        Me.PictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox15.TabIndex = 2
        Me.PictureBox15.TabStop = False
        '
        'PictureBox14
        '
        Me.PictureBox14.Image = Global.PL_Lotto.My.Resources.Resources.griezman
        Me.PictureBox14.Location = New System.Drawing.Point(4262, 6)
        Me.PictureBox14.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(176, 152)
        Me.PictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox14.TabIndex = 2
        Me.PictureBox14.TabStop = False
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Black
        Me.Label13.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(1788, 17)
        Me.Label13.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(278, 62)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "Ronaldinho"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox13
        '
        Me.PictureBox13.Image = Global.PL_Lotto.My.Resources.Resources.neymar
        Me.PictureBox13.Location = New System.Drawing.Point(3732, 4)
        Me.PictureBox13.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(176, 152)
        Me.PictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox13.TabIndex = 2
        Me.PictureBox13.TabStop = False
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Black
        Me.Label11.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(1260, 17)
        Me.Label11.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(278, 62)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "Zidane"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox12
        '
        Me.PictureBox12.Image = Global.PL_Lotto.My.Resources.Resources.ronaldo
        Me.PictureBox12.Location = New System.Drawing.Point(3190, 4)
        Me.PictureBox12.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(176, 152)
        Me.PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox12.TabIndex = 2
        Me.PictureBox12.TabStop = False
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Black
        Me.Label9.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(732, 17)
        Me.Label9.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(278, 62)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Maradona"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox11
        '
        Me.PictureBox11.Image = Global.PL_Lotto.My.Resources.Resources.messi
        Me.PictureBox11.Location = New System.Drawing.Point(2648, 4)
        Me.PictureBox11.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(176, 152)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox11.TabIndex = 2
        Me.PictureBox11.TabStop = False
        '
        'PictureBox10
        '
        Me.PictureBox10.Image = Global.PL_Lotto.My.Resources.Resources.roger_milla
        Me.PictureBox10.Location = New System.Drawing.Point(2098, 4)
        Me.PictureBox10.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(176, 152)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox10.TabIndex = 2
        Me.PictureBox10.TabStop = False
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Black
        Me.Label4.Font = New System.Drawing.Font("Impact", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(222, 17)
        Me.Label4.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(278, 62)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Le Roi Pele"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox9
        '
        Me.PictureBox9.Image = Global.PL_Lotto.My.Resources.Resources.ronaldinho
        Me.PictureBox9.Location = New System.Drawing.Point(1568, 4)
        Me.PictureBox9.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(176, 152)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox9.TabIndex = 2
        Me.PictureBox9.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = Global.PL_Lotto.My.Resources.Resources.zinedine_zidane_17986420
        Me.PictureBox8.Location = New System.Drawing.Point(1040, 4)
        Me.PictureBox8.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(176, 152)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox8.TabIndex = 2
        Me.PictureBox8.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = Global.PL_Lotto.My.Resources.Resources.maradona
        Me.PictureBox7.Location = New System.Drawing.Point(512, 4)
        Me.PictureBox7.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(176, 152)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox7.TabIndex = 2
        Me.PictureBox7.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.PL_Lotto.My.Resources.Resources.Pele_Wallpaper_1
        Me.PictureBox4.Location = New System.Drawing.Point(26, 4)
        Me.PictureBox4.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(176, 152)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox4.TabIndex = 2
        Me.PictureBox4.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Impact", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(160, 6)
        Me.Label6.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(682, 85)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Jackpot des Legendes"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.CheckBox91)
        Me.GroupBox7.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.ForeColor = System.Drawing.Color.White
        Me.GroupBox7.Location = New System.Drawing.Point(1200, 1190)
        Me.GroupBox7.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox7.Size = New System.Drawing.Size(106, 210)
        Me.GroupBox7.TabIndex = 14
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Loto Plasma: Activer/Désactiver"
        Me.GroupBox7.Visible = False
        '
        'CheckBox91
        '
        Me.CheckBox91.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox91.BackColor = System.Drawing.Color.Green
        Me.CheckBox91.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox91.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.CheckBox91.Location = New System.Drawing.Point(12, 42)
        Me.CheckBox91.Margin = New System.Windows.Forms.Padding(6)
        Me.CheckBox91.Name = "CheckBox91"
        Me.CheckBox91.Size = New System.Drawing.Size(346, 96)
        Me.CheckBox91.TabIndex = 5
        Me.CheckBox91.Text = "Desactivé"
        Me.CheckBox91.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox91.UseVisualStyleBackColor = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Dgv_LastTick)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox6.ForeColor = System.Drawing.Color.White
        Me.GroupBox6.Location = New System.Drawing.Point(1318, 1202)
        Me.GroupBox6.Margin = New System.Windows.Forms.Padding(6)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Padding = New System.Windows.Forms.Padding(6)
        Me.GroupBox6.Size = New System.Drawing.Size(168, 65)
        Me.GroupBox6.TabIndex = 16
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Dernier Ticket Imprimmer"
        Me.GroupBox6.Visible = False
        '
        'Dgv_LastTick
        '
        Me.Dgv_LastTick.AllowUserToAddRows = False
        Me.Dgv_LastTick.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        Me.Dgv_LastTick.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.Dgv_LastTick.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgv_LastTick.Location = New System.Drawing.Point(28, 63)
        Me.Dgv_LastTick.Margin = New System.Windows.Forms.Padding(6)
        Me.Dgv_LastTick.Name = "Dgv_LastTick"
        Me.Dgv_LastTick.ReadOnly = True
        Me.Dgv_LastTick.RowHeadersWidth = 82
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        Me.Dgv_LastTick.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.Dgv_LastTick.Size = New System.Drawing.Size(610, 198)
        Me.Dgv_LastTick.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Red
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(1364, 1279)
        Me.Button1.Margin = New System.Windows.Forms.Padding(6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(22, 42)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Imp. Dernier Tick."
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334!))
        Me.TableLayoutPanel1.Controls.Add(Me.Button4, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Dgd_Tick, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox4, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.AxWindowsMediaPlayer1, 0, 5)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(6)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 113.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.68518!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77.31481!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 154.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 104.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 410.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 267.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(886, 1548)
        Me.TableLayoutPanel1.TabIndex = 30
        '
        'Button4
        '
        Me.Button4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button4.BackColor = System.Drawing.Color.DarkGreen
        Me.TableLayoutPanel1.SetColumnSpan(Me.Button4, 3)
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(6, 753)
        Me.Button4.Margin = New System.Windows.Forms.Padding(6)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(874, 90)
        Me.Button4.TabIndex = 12
        Me.Button4.Text = "Placez un pari"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Black
        Me.TableLayoutPanel1.SetColumnSpan(Me.Label3, 3)
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(6, 9)
        Me.Label3.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(874, 94)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Coupon de paris"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel1, 3)
        Me.Panel1.Controls.Add(Me.jackpanel)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(6, 1267)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(874, 255)
        Me.Panel1.TabIndex = 16
        '
        'jackpanel
        '
        Me.jackpanel.Controls.Add(Me.Panel18)
        Me.jackpanel.Controls.Add(Me.Panel14)
        Me.jackpanel.Location = New System.Drawing.Point(27, 0)
        Me.jackpanel.Margin = New System.Windows.Forms.Padding(6)
        Me.jackpanel.Name = "jackpanel"
        Me.jackpanel.Size = New System.Drawing.Size(816, 538)
        Me.jackpanel.TabIndex = 32
        '
        'Panel18
        '
        Me.Panel18.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel18.BackColor = System.Drawing.Color.White
        Me.Panel18.Controls.Add(Me.Lb_iron_pot_name)
        Me.Panel18.Controls.Add(Me.PictureBox5)
        Me.Panel18.Controls.Add(Me.Lb_iron_pot)
        Me.Panel18.Location = New System.Drawing.Point(26, 298)
        Me.Panel18.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(762, 227)
        Me.Panel18.TabIndex = 32
        '
        'Lb_iron_pot_name
        '
        Me.Lb_iron_pot_name.BackColor = System.Drawing.Color.Black
        Me.Lb_iron_pot_name.Font = New System.Drawing.Font("Impact", 14.25!)
        Me.Lb_iron_pot_name.ForeColor = System.Drawing.Color.White
        Me.Lb_iron_pot_name.Location = New System.Drawing.Point(276, 29)
        Me.Lb_iron_pot_name.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Lb_iron_pot_name.Name = "Lb_iron_pot_name"
        Me.Lb_iron_pot_name.Size = New System.Drawing.Size(448, 73)
        Me.Lb_iron_pot_name.TabIndex = 28
        Me.Lb_iron_pot_name.Text = "Cagnotte"
        Me.Lb_iron_pot_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = Global.PL_Lotto.My.Resources.Resources.logo_coupons
        Me.PictureBox5.Location = New System.Drawing.Point(10, 15)
        Me.PictureBox5.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(238, 194)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox5.TabIndex = 27
        Me.PictureBox5.TabStop = False
        '
        'Lb_iron_pot
        '
        Me.Lb_iron_pot.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Lb_iron_pot.AutoSize = True
        Me.Lb_iron_pot.BackColor = System.Drawing.Color.Transparent
        Me.Lb_iron_pot.Font = New System.Drawing.Font("Impact", 15.75!)
        Me.Lb_iron_pot.ForeColor = System.Drawing.Color.Black
        Me.Lb_iron_pot.Location = New System.Drawing.Point(376, 131)
        Me.Lb_iron_pot.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Lb_iron_pot.Name = "Lb_iron_pot"
        Me.Lb_iron_pot.Size = New System.Drawing.Size(240, 52)
        Me.Lb_iron_pot.TabIndex = 24
        Me.Lb_iron_pot.Text = "Smart Phone"
        Me.Lb_iron_pot.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel14
        '
        Me.Panel14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel14.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel14.Controls.Add(Me.gpot_name_lbl)
        Me.Panel14.Controls.Add(Me.PictureBox2)
        Me.Panel14.Controls.Add(Me.gpot_lbl)
        Me.Panel14.Location = New System.Drawing.Point(26, 15)
        Me.Panel14.Margin = New System.Windows.Forms.Padding(6)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(760, 225)
        Me.Panel14.TabIndex = 31
        '
        'gpot_name_lbl
        '
        Me.gpot_name_lbl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpot_name_lbl.BackColor = System.Drawing.Color.Black
        Me.gpot_name_lbl.Font = New System.Drawing.Font("Impact", 14.25!)
        Me.gpot_name_lbl.ForeColor = System.Drawing.Color.White
        Me.gpot_name_lbl.Location = New System.Drawing.Point(276, 29)
        Me.gpot_name_lbl.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.gpot_name_lbl.Name = "gpot_name_lbl"
        Me.gpot_name_lbl.Size = New System.Drawing.Size(464, 73)
        Me.gpot_name_lbl.TabIndex = 28
        Me.gpot_name_lbl.Text = "Super Cagnotte"
        Me.gpot_name_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.PL_Lotto.My.Resources.Resources.logo_coupons
        Me.PictureBox2.Location = New System.Drawing.Point(14, 19)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(238, 194)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 27
        Me.PictureBox2.TabStop = False
        '
        'gpot_lbl
        '
        Me.gpot_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.gpot_lbl.AutoSize = True
        Me.gpot_lbl.Font = New System.Drawing.Font("Impact", 15.75!)
        Me.gpot_lbl.ForeColor = System.Drawing.Color.White
        Me.gpot_lbl.Location = New System.Drawing.Point(376, 131)
        Me.gpot_lbl.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.gpot_lbl.Name = "gpot_lbl"
        Me.gpot_lbl.Size = New System.Drawing.Size(251, 52)
        Me.gpot_lbl.TabIndex = 24
        Me.gpot_lbl.Text = "Ecran Plasma"
        Me.gpot_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox4
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.GroupBox4, 3)
        Me.GroupBox4.Controls.Add(Me.RadioButton16)
        Me.GroupBox4.Controls.Add(Me.RadioButton15)
        Me.GroupBox4.Controls.Add(Me.RadioButton14)
        Me.GroupBox4.Controls.Add(Me.RadioButton13)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox4.ForeColor = System.Drawing.Color.White
        Me.GroupBox4.Location = New System.Drawing.Point(3, 116)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(880, 103)
        Me.GroupBox4.TabIndex = 18
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.UseWaitCursor = True
        '
        'RadioButton16
        '
        Me.RadioButton16.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton16.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton16.Enabled = False
        Me.RadioButton16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton16.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton16.ForeColor = System.Drawing.Color.Black
        Me.RadioButton16.Location = New System.Drawing.Point(657, 30)
        Me.RadioButton16.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton16.Name = "RadioButton16"
        Me.RadioButton16.Size = New System.Drawing.Size(190, 60)
        Me.RadioButton16.TabIndex = 7
        Me.RadioButton16.Text = "4X"
        Me.RadioButton16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton16.UseVisualStyleBackColor = False
        Me.RadioButton16.UseWaitCursor = True
        '
        'RadioButton15
        '
        Me.RadioButton15.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton15.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton15.Enabled = False
        Me.RadioButton15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton15.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton15.ForeColor = System.Drawing.Color.Black
        Me.RadioButton15.Location = New System.Drawing.Point(449, 30)
        Me.RadioButton15.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton15.Name = "RadioButton15"
        Me.RadioButton15.Size = New System.Drawing.Size(190, 60)
        Me.RadioButton15.TabIndex = 6
        Me.RadioButton15.Text = "3X"
        Me.RadioButton15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton15.UseVisualStyleBackColor = False
        Me.RadioButton15.UseWaitCursor = True
        '
        'RadioButton14
        '
        Me.RadioButton14.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton14.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton14.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton14.ForeColor = System.Drawing.Color.Black
        Me.RadioButton14.Location = New System.Drawing.Point(241, 30)
        Me.RadioButton14.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton14.Name = "RadioButton14"
        Me.RadioButton14.Size = New System.Drawing.Size(190, 60)
        Me.RadioButton14.TabIndex = 5
        Me.RadioButton14.Text = "2X"
        Me.RadioButton14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton14.UseVisualStyleBackColor = False
        Me.RadioButton14.UseWaitCursor = True
        '
        'RadioButton13
        '
        Me.RadioButton13.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton13.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.RadioButton13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton13.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton13.ForeColor = System.Drawing.Color.Black
        Me.RadioButton13.Location = New System.Drawing.Point(33, 30)
        Me.RadioButton13.Margin = New System.Windows.Forms.Padding(6)
        Me.RadioButton13.Name = "RadioButton13"
        Me.RadioButton13.Size = New System.Drawing.Size(190, 60)
        Me.RadioButton13.TabIndex = 4
        Me.RadioButton13.Text = "1X"
        Me.RadioButton13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton13.UseVisualStyleBackColor = False
        Me.RadioButton13.UseWaitCursor = True
        '
        'AxWindowsMediaPlayer1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.AxWindowsMediaPlayer1, 3)
        Me.AxWindowsMediaPlayer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AxWindowsMediaPlayer1.Enabled = True
        Me.AxWindowsMediaPlayer1.Location = New System.Drawing.Point(3, 854)
        Me.AxWindowsMediaPlayer1.Name = "AxWindowsMediaPlayer1"
        Me.AxWindowsMediaPlayer1.OcxState = CType(resources.GetObject("AxWindowsMediaPlayer1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxWindowsMediaPlayer1.Size = New System.Drawing.Size(880, 404)
        Me.AxWindowsMediaPlayer1.TabIndex = 19
        '
        'Timer1
        '
        '
        'Timer2
        '
        '
        'FrmTicket
        '
        Me.AcceptButton = Me.Button3
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(2752, 1548)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "FrmTicket"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edition de Ticket"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.Dgd_Tick, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.PbTickGagnant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PbVoyant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel23.ResumeLayout(False)
        Me.Panel23.PerformLayout()
        Me.legendPanel.ResumeLayout(False)
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.Dgv_LastTick, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.jackpanel.ResumeLayout(False)
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bt_print As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtCancel As System.Windows.Forms.Button
    Friend WithEvents CheckBox90 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox80 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox70 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox60 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox50 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox40 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox20 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox8 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox89 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox79 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox69 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox59 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox49 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox39 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox19 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox9 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox88 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox78 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox68 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox58 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox48 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox38 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox18 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox87 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox77 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox67 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox57 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox47 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox37 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox17 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox86 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox76 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox66 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox56 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox46 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox36 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox16 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox10 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox85 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox75 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox65 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox55 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox45 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox35 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox15 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox7 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox84 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox74 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox64 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox54 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox44 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox34 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox14 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox83 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox73 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox63 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox53 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox43 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox33 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox13 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox82 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox72 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox62 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox52 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox42 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox32 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox12 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox81 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox71 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox61 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox51 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox41 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox31 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox11 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Dgd_Tick As System.Windows.Forms.DataGridView
    Friend WithEvents RadioButton12 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton7 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton11 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton9 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton10 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton8 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton5 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TbIdticket As System.Windows.Forms.TextBox
    Friend WithEvents PbTickGagnant As System.Windows.Forms.PictureBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents PbVoyant As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Dgv_LastTick As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents Lb_Min As Label
    Friend WithEvents LB_second As Label
    Friend WithEvents LB_NumTirage As Label
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents Button1 As Button
    Friend WithEvents CheckBox91 As CheckBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Button6 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Lb_Mise As Label
    Friend WithEvents Button7 As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel14 As Panel
    Friend WithEvents gpot_name_lbl As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents gpot_lbl As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel23 As Panel
    Friend WithEvents legendPanel As Panel
    Friend WithEvents Label26 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents PictureBox15 As PictureBox
    Friend WithEvents PictureBox14 As PictureBox
    Friend WithEvents Label13 As Label
    Friend WithEvents PictureBox13 As PictureBox
    Friend WithEvents Label11 As Label
    Friend WithEvents PictureBox12 As PictureBox
    Friend WithEvents Label9 As Label
    Friend WithEvents PictureBox11 As PictureBox
    Friend WithEvents PictureBox10 As PictureBox
    Friend WithEvents Label4 As Label
    Friend WithEvents PictureBox9 As PictureBox
    Friend WithEvents PictureBox8 As PictureBox
    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Timer2 As Timer
    Friend WithEvents Panel3 As Panel
    Friend WithEvents jackpanel As Panel
    Friend WithEvents Panel18 As Panel
    Friend WithEvents Lb_iron_pot_name As Label
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents Lb_iron_pot As Label
    Friend WithEvents lbCredit As Label
    Friend WithEvents playerLabl As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents LB_STAT_5 As Label
    Friend WithEvents LB_STAT_4 As Label
    Friend WithEvents LB_STAT_3 As Label
    Friend WithEvents LB_STAT_2 As Label
    Friend WithEvents LB_STAT_1 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents RadioButton16 As RadioButton
    Friend WithEvents RadioButton15 As RadioButton
    Friend WithEvents RadioButton14 As RadioButton
    Friend WithEvents RadioButton13 As RadioButton
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents AxWindowsMediaPlayer1 As AxWMPLib.AxWindowsMediaPlayer
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents CheckBox21 As CheckBox
    Friend WithEvents CheckBox30 As CheckBox
    Friend WithEvents CheckBox29 As CheckBox
    Friend WithEvents CheckBox22 As CheckBox
    Friend WithEvents CheckBox28 As CheckBox
    Friend WithEvents CheckBox23 As CheckBox
    Friend WithEvents CheckBox24 As CheckBox
    Friend WithEvents CheckBox25 As CheckBox
    Friend WithEvents CheckBox26 As CheckBox
    Friend WithEvents CheckBox27 As CheckBox
End Class
