﻿Imports MySql
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports MySql.Data.Types
Imports System.Drawing.Imaging
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Windows.Forms

Module lotosettings
    Public TirageLancer As Integer = 0
    Public timesent As Integer = 0
    Public ProchainTir As Integer = 0
    Public creditplayer As Integer = 0
    Public num_tirTab() As String = {"99", "99", "99", "99", "99"}
    Public Ocaisse As Client

    Dim Oactivation As BL_Lotto.BlActivation
    'Public CONNECTED_USERID As String = ""
    Public CONNECTED_USERID As String
    Public CONNECTED_USERNAME As String

    'Public SERVEUR As String = "serveur_loto"
    Public SERVEUR As String

    'Public BD As String = "bd_lot"
    Public BD As String

    'Public ID As String = My.Settings.USER_BD
    Public ID As String

    'Public PWD As String = My.Settings.PWD_BD
    Public PWD As String

    'Public source As String = "Server=" & My.Settings.HOST & ";Uid=" & ID & ";Pwd=" & PWD & ";persist security info=True;database=" & BD
    Public source As String

    Public MonSocketClient, MonSocketCaisse As Socket

    Public NB_MODE As Integer
    'Public MODELOTO(3, 3) As String
    Public MODELOTO As New Dictionary(Of String, String)


    Public MONTH_BOOST_LIMIT, MAX_BOOST_LIMIT, DIFICULT_LEVEL, DELAI_AV_GAINS, GOLD_POT, SILVER_POT, IRON_POT, CAGNOTE_POT, MSTERIOUS_POT, MEGA_POT, ID_CAGNOTE, ID_GOLD, ID_SILVER, ID_IRON, ID_MSTERIOUS, ID_MEGA, NB_SHARER, MAX_GAIIN_SILVER, MIN_GAIIN_SILVER, MAX_GAIN_IRON, MIN_GAIN_IRON, MIN_LOTO_PLASMA, MIN_PARI_JACKPOT, DELAI_JEU As Integer
    Public PORT, SCREEN_SELECT As String
    Public Sub initialiseSetting()
        Dim oDataSet, seting() As String

        oDataSet = Ocaisse.GetSetting
        seting = oDataSet.Split(CChar("/"))

        If seting.Count > 2 Then
            MONTH_BOOST_LIMIT = CInt(seting(0))
            MAX_BOOST_LIMIT = CInt(seting(1))
            DIFICULT_LEVEL = CInt(seting(2))
            DELAI_AV_GAINS = CInt(seting(3))
            PORT = seting(4)
            GOLD_POT = CInt(seting(5))
            SILVER_POT = CInt(seting(6))
            IRON_POT = CInt(seting(7))
            CAGNOTE_POT = CInt(seting(8))
            MSTERIOUS_POT = CInt(seting(9))
            MEGA_POT = CInt(seting(10))
            ID_CAGNOTE = CInt(seting(11))
            ID_GOLD = CInt(seting(12))
            ID_SILVER = CInt(seting(13))
            ID_IRON = CInt(seting(14))
            ID_MSTERIOUS = CInt(seting(15))
            ID_MEGA = CInt(seting(16))
            NB_SHARER = CInt(seting(18))
            MAX_GAIIN_SILVER = CInt(seting(19))
            MIN_GAIIN_SILVER = CInt(seting(20))
            MAX_GAIN_IRON = CInt(seting(21))
            MIN_GAIN_IRON = CInt(seting(22))
            MIN_LOTO_PLASMA = CInt(seting(23))
            MIN_PARI_JACKPOT = CInt(seting(24))
            DELAI_JEU = CInt(seting(25))
            'SCREEN_SELECT
        End If

    End Sub





    Sub Combo(ByVal ComboI As ComboBox, ByVal Req As String)
        Dim con As MySqlConnection = New MySqlConnection(source)
        Dim cmd As New MySqlCommand(Req, con)
        Dim dr As MySqlDataReader
        con.Open()
        dr = cmd.ExecuteReader
        While dr.Read
            ComboI.Items.Add(dr(0))
        End While
        dr.Close()
        con.Close()
    End Sub


    Sub initialisation()
        Dim omode, ttmode() As String

        Try
            omode = Ocaisse.getallmode()
            ttmode = omode.Split(CChar("/"))

            NB_MODE = ttmode.Count
            If (ttmode.Count > 0) Then
                Dim modegese() As String
                For h As Integer = 0 To ttmode.Count - 1
                    modegese = ttmode(h).Split(CChar("#"))
                    MODELOTO.Add(modegese(0), modegese(1))
                Next
            End If

        Catch ex As Exception
            MessageBox.Show("Probleme rencontrer lors du demarage du jeu", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub
    Public Function Hostname2IP(ByVal hostname As String) As String
        Dim hostname2 As IPHostEntry = Dns.GetHostEntry(hostname)
        Dim ip As IPAddress() = hostname2.AddressList

        Return ip(1).ToString()
    End Function

    Public Class Client
        Private _SocketClient As Socket 'Le socket du client
        Private _Pseudo As String 'Le pseudo du client

        'Constructeur
        Sub New(ByVal Sock As Socket)
            _SocketClient = Sock
        End Sub


        Public Function EnvoiRequete(ByVal Message As String) As String
            'envoi de la requete
            Me.EnvoiMessage(Message)

            'reception de la reponse
            Dim Bytes(255) As Byte
            Dim Recu As Integer
            Try
                Recu = _SocketClient.Receive(Bytes)
            Catch ex As Exception 'Erreur si fermeture du socket pendant la réception
                MessageBox.Show("Connexion perdue, arrêt de la réception des données ...", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Dim reponse As String
            reponse = System.Text.Encoding.UTF8.GetString(Bytes)
            reponse = reponse.Substring(0, Recu)

            Return reponse

        End Function
        Public Function AjouterTicket(ByVal tab As String(,), ByVal nbparie As Integer, ByVal chance As Integer) As String
            Dim Message As String
            Dim pari As String = ""
            For g As Integer = 0 To nbparie - 1

                If g = 0 Then
                    pari = tab(g, 0) & "#" & tab(g, 1) & "#" & tab(g, 2)
                Else
                    pari = pari & "*" & tab(g, 0) & "#" & tab(g, 1) & "#" & tab(g, 2)
                End If
            Next


            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "1/" & My.Settings.CODE_SALE & "/" & CONNECTED_USERID & "/" & nbparie & "/" & pari & "/" & chance

            Return Me.EnvoiRequete(Message)
        End Function

        Public Function UserCOnnect(ByVal login As String, ByVal pwd As String, ByVal credent As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "9" & "/" & login & "/" & pwd & "/" & credent

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function UserComptbility(ByVal userid As String, ByVal datdebut As String, ByVal datfin As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "15" & "/" & userid & "/" & datdebut & "/" & datfin

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function getallmode() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "10"

            Return Me.EnvoiRequete(Message)

        End Function
        Public Function getLastTirageID() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "11"

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function GetObAPPActived() As Boolean

            Dim Message, rep As String


            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "12"

            rep = Me.EnvoiRequete(Message)

            If (rep <> "0") Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function can(ByVal user_id As String) As Boolean

            Dim Message, rep As String


            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "13" & "/" & user_id

            rep = Me.EnvoiRequete(Message)

            If (rep <> "0") Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function GetSetting() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "14" & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function GetJackpot(ByVal id_j As String, ByVal id_pv As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "6" & "/" & id_j & "/" & id_pv & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function checkTicket(ByVal codebar As String, ByVal caissier As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "2/" & codebar & "/" & caissier

            Return Me.EnvoiRequete(Message)


        End Function

        Public Function checkTicketjacpot(ByVal codebar As String, ByVal caissier As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "4/" & codebar & "/" & caissier

            Return Me.EnvoiRequete(Message)


        End Function

        Public Function GetTimerAndTirage() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "3" & "/" & CONNECTED_USERID & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)


        End Function

        Public Function creditDebitPlayer(ByVal id_player As String, ByVal caissier As String, ByVal montant As String) As Boolean

            Dim Message, rep As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "23/" & id_player & "/" & caissier & "/" & montant

            rep = Me.EnvoiRequete(Message)

            If (rep <> "0") Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function getCreditPlayer() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "33" & "/" & CONNECTED_USERID

            Return Me.EnvoiRequete(Message)


        End Function

        Public Function getTcketParID(ByVal id_ticket As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "34/" & id_ticket

            Return Me.EnvoiRequete(Message)
        End Function

        Public Function getLastTirageNumber() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "26/" & "0" & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)
        End Function






        Public Sub EnvoiMessage(ByVal Message As String)
            Dim Mess As Byte() = System.Text.Encoding.UTF8.GetBytes(Message)
            Dim Envoi As Integer = _SocketClient.Send(Mess)
            'Console.WriteLine(Envoi & " bytes envoyés au client " & _Pseudo)
        End Sub
        Public Function ReceiveMessage() As String()
            Dim Bytes(255) As Byte
            Dim Recu As Integer
            Try
                Recu = _SocketClient.Receive(Bytes)
            Catch ex As Exception 'Erreur si fermeture du socket pendant la réception
                MessageBox.Show("Connexion perdue, arrêt de la réception des données ...", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Dim Message As String
            Message = System.Text.Encoding.UTF8.GetString(Bytes)
            Message = Message.Substring(0, Recu)

            Return Message.Split(CChar("/"))
        End Function



    End Class

    Sub main()

    End Sub


End Module
