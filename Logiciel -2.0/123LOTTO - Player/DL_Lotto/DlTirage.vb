﻿Public Class DlTirage
    Inherits DlBase

#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region

#Region " Public Role Functions "
    Public Function AjouterTirage(ByVal tab() As Integer) As Boolean
        Try
            Dim tirge As New DataSet
            Dim lasttirage As New Integer
            MyBase.SQL = "Select NUM_TIRAGE from tirage where NUM_TIRAGE >= ALL(Select NUM_TIRAGE from tirage)"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(tirge, "tirage")
            If (tirge.Tables("tirage").Rows.Count > 0) Then
                lasttirage = CInt(tirge.Tables("tirage").Rows(0).Item("NUM_TIRAGE")) + 1
            Else
                lasttirage = 1
            End If

            For i As Integer = 0 To tab.Length - 1
                MyBase.SQL = "INSERT INTO contenir (NUM_TIRAGE, ID_NUM_TIRER) values (" & lasttirage & ", " & tab(i) & " )"
                'Initialisation l'objet command
                MyBase.InitializeCommand()
                'Ajouter les paramètres 
                'MyBase.AddParameter("@IdUser", _
                '     Data.OleDb.OleDbType.VarChar, 15, _
                '    User.Tables("Users").Rows(0).Item("IdUser"))
                'MyBase.AddParameter("@Pwd", _
                '     Data.OleDb.OleDbType.VarChar, 20, _
                '    User.Tables("Users").Rows(0).Item("Pwd"))
                ExecuteStoredProcedure()
            Next

            MyBase.SQL = "INSERT INTO tirage (NUM_TIRAGE) values (" & lasttirage & " )"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            'Ajouter les paramètres 
            'MyBase.AddParameter("@IdUser", _
            '     Data.OleDb.OleDbType.VarChar, 15, _
            '    User.Tables("Users").Rows(0).Item("IdUser"))
            'MyBase.AddParameter("@Pwd", _
            '     Data.OleDb.OleDbType.VarChar, 20, _
            '    User.Tables("Users").Rows(0).Item("Pwd"))
            AjouterTirage = ExecuteStoredProcedure()




        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    'Public Function MajUser(ByVal User As DataSet) As Boolean
    '    Try
    '        MyBase.SQL = "Update  Users Set Pwd=@Pwd where  IdUser=@IdUser"
    '        MyBase.InitializeCommand()

    '        MyBase.AddParameter("@Pwd", _
    '             Data.OleDb.OleDbType.VarChar, 20, _
    '            User.Tables("Users").Rows(0).Item("Pwd"))

    '        MyBase.AddParameter("@IdUser", _
    '            Data.OleDb.OleDbType.VarChar, 15, _
    '            User.Tables("Users").Rows(0).Item("IdUser"))
    '        'Exécution de la 
    '        MajUser = ExecuteStoredProcedure()
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message, _
    '        ExceptionErr.InnerException)
    '    End Try
    'End Function

    'Public Function ListUser() As DataSet
    '    Try
    '        ListUser = New DataSet
    '        MyBase.SQL = "Select IdUser as [Compte] from Users Order by IdUser"
    '        MyBase.InitializeCommand()
    '        'remplir le dataset
    '        MyBase.FillDataSet(ListUser, "Users")
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message, _
    '        ExceptionErr.InnerException)
    '    End Try
    'End Function


    'Public Function ListUserNom(ByVal UserName As String) As DataSet
    '    Try
    '        ListUserNom = New DataSet
    '        MyBase.SQL = "Select IdUser as [Compte] from Users where IdUser=@Username Order by IdUser"
    '        MyBase.InitializeCommand()
    '        MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, _
    '            15, UserName)
    '        'remplir le dataset
    '        MyBase.FillDataSet(ListUserNom, "Users")
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message, _
    '        ExceptionErr.InnerException)
    '    End Try
    'End Function


    'Public Function ConPossible(ByVal UserName As String, ByVal UserPassword As String) As Boolean
    '    Try
    '        Dim LoginDataset As DataSet
    '        LoginDataset = New DataSet

    '        MyBase.SQL = "SELECT * FROM users WHERE LOGIN='" + UserName + "' AND PWD='" + UserPassword + "';"
    '        MyBase.InitializeCommand()

    '        'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

    '        'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

    '        MyBase.FillDataSet(LoginDataset, "users")

    '        If LoginDataset.Tables("users").Rows.Count > 0 Then
    '            ConPossible = True
    '        Else
    '            ConPossible = False
    '        End If
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message, _
    '        ExceptionErr.InnerException)
    '    End Try
    'End Function

    'Public Function UserParNom(ByVal UserName As String) As DataSet
    '    Try
    '        UserParNom = New DataSet
    '        MyBase.SQL = "Select * from Users where IdUser=@IdUser"

    '        MyBase.InitializeCommand()

    '        MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, _
    '            15, UserName)

    '        MyBase.FillDataSet(UserParNom, "Users")
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message, _
    '        ExceptionErr.InnerException)
    '    End Try
    'End Function

    'Public Function SupUser(ByVal UserName As String) As Boolean
    '    Try
    '        MyBase.SQL = "Delete * from Users where IdUser=@IdUser"

    '        MyBase.InitializeCommand()

    '        MyBase.AddParameter("@IdUser", _
    '             Data.OleDb.OleDbType.VarChar, 15, UserName)
    '        'exécuter la procédure stockée
    '        SupUser = ExecuteStoredProcedure()
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message, _
    '            ExceptionErr.InnerException)
    '    End Try
    'End Function
#End Region
End Class
