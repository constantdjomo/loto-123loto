﻿Public Class DlUser
    Inherits DlBase

#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region

#Region " Public Role Functions "
    Public Function AjouterUser(ByVal User As DataSet) As Boolean
        Try
            MyBase.SQL = "INSERT INTO users (ID_USER,NOM,PRENOM,LOGIN,PWD,id_user_typ ) values (" & User.Tables("users").Rows(0).Item("ID_USER") & ", '" & User.Tables("users").Rows(0).Item("NOM") & "','" _
                & User.Tables("users").Rows(0).Item("PRENOM") & "','" _
                & User.Tables("users").Rows(0).Item("LOGIN") & "','" _
                & User.Tables("users").Rows(0).Item("PWD") & "' ,'" _
                & User.Tables("users").Rows(0).Item("id_user_typ") & "')"

            'Initialisation l'objet command
            MyBase.InitializeCommand()
            'Ajouter les paramètres 
            'MyBase.AddParameter("@IdUser", _
            '     Data.OleDb.OleDbType.VarChar, 15, _
            '    User.Tables("Users").Rows(0).Item("IdUser"))
            'MyBase.AddParameter("@Pwd", _
            '     Data.OleDb.OleDbType.VarChar, 20, _
            '    User.Tables("Users").Rows(0).Item("Pwd"))
            AjouterUser = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function MajUser(ByVal User As DataSet) As Boolean
        Try
            MyBase.SQL = "Update  Users Set NOM ='" & User.Tables("users").Rows(0).Item("NOM") & "', PRENOM = '" _
                & User.Tables("users").Rows(0).Item("PRENOM") & "',LOGIN ='" _
                & User.Tables("users").Rows(0).Item("LOGIN") & "', PWD ='" _
                & User.Tables("users").Rows(0).Item("PWD") & "' , id_user_typ ='" _
                & User.Tables("users").Rows(0).Item("id_user_typ") & "' WHERE  ID_USER =" & User.Tables("users").Rows(0).Item("ID_USER")
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Pwd", _
            '     Data.OleDb.OleDbType.VarChar, 20, _
            '    User.Tables("Users").Rows(0).Item("Pwd"))

            'MyBase.AddParameter("@IdUser", _
            '    Data.OleDb.OleDbType.VarChar, 15, _
            '    User.Tables("Users").Rows(0).Item("IdUser"))
            'Exécution de la 
            MajUser = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ListUser() As DataSet
        Try
            ListUser = New DataSet
            MyBase.SQL = "SELECT ID_USER,NOM,PRENOM,LOGIN,PWD as PASSWORD,user_typ ,ut.id_user_typ  from users u,users_type ut WHERE u.id_user_typ=ut.id_user_typ"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(ListUser, "users")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getallmode() As DataSet
        Try
            getallmode = New DataSet
            MyBase.SQL = "SELECT * FROM `mode_jeux`"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getallmode, "mode_jeux")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function ListUserNom(ByVal UserName As String) As DataSet
        Try
            ListUserNom = New DataSet
            MyBase.SQL = "Select IdUser as [Compte] from Users where IdUser=@Username Order by IdUser"
            MyBase.InitializeCommand()
            MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, _
                15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(ListUserNom, "Users")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function ConPossible(ByVal UserName As String, ByVal UserPassword As String) As Boolean
        Try
            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            MyBase.SQL = "SELECT * FROM users WHERE LOGIN='" + UserName + "' AND PWD='" + UserPassword + "';"
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

            'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

            MyBase.FillDataSet(LoginDataset, "users")

            If LoginDataset.Tables("users").Rows.Count > 0 Then
                ConPossible = True
            Else
                ConPossible = False
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function can(ByVal use As String) As Boolean
        Try
            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            MyBase.SQL = "SELECT * FROM users WHERE ID_USER=" & use
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

            'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

            MyBase.FillDataSet(LoginDataset, "users")

            If LoginDataset.Tables("users").Rows.Count > 0 AndAlso LoginDataset.Tables("users").Rows(0).Item("id_user_typ") = My.Settings.AdminID Then
                can = True
            Else
                can = False
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetUserID(ByVal UserName As String, ByVal UserPassword As String) As Integer
        Try
            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            MyBase.SQL = "SELECT * FROM users WHERE LOGIN='" + UserName + "' AND PWD='" + UserPassword + "';"
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

            'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

            MyBase.FillDataSet(LoginDataset, "users")

            If LoginDataset.Tables("users").Rows.Count > 0 Then
                GetUserID = LoginDataset.Tables("users").Rows(0).Item("ID_USER")
            Else
                GetUserID = 0
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function




    Public Function UserParID(ByVal UserName As String) As DataSet
        Try
            UserParID = New DataSet
            MyBase.SQL = "Select * from users where ID_USER =" & UserName

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, UserName)

            MyBase.FillDataSet(UserParID, "users")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetUserParNom(ByVal UserName As String) As DataSet
        Try
            GetUserParNom = New DataSet
            MyBase.SQL = "Select * from users where LOGIN ='" & UserName & "'"

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, UserName)

            MyBase.FillDataSet(GetUserParNom, "users")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function GetUserTyp() As DataSet
        Try
            GetUserTyp = New DataSet
            MyBase.SQL = "Select * from users_type "

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, UserName)

            MyBase.FillDataSet(GetUserTyp, "users_type")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function SupUser(ByVal UserName As String) As Boolean
        Try
            MyBase.SQL = "Delete * from users where ID_USER=" & UserName

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, UserName)
            'exécuter la procédure stockée
            SupUser = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function
#End Region

End Class
