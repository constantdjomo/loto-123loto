-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 22 Novembre 2016 à 22:32
-- Version du serveur :  5.6.20
-- Version de PHP :  5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bd_loto`
--

-- --------------------------------------------------------

--
-- Structure de la table `avoir`
--

CREATE TABLE IF NOT EXISTS `avoir` (
  `ID_PARI` int(255) NOT NULL,
  `NUMEROS` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `cle_activation`
--

CREATE TABLE IF NOT EXISTS `cle_activation` (
`id_cle` int(11) NOT NULL,
  `cle` varchar(255) NOT NULL,
  `montant` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `etat` tinyint(1) NOT NULL DEFAULT '0',
  `used` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Contenu de la table `cle_activation`
--

INSERT INTO `cle_activation` (`id_cle`, `cle`, `montant`, `date_enreg`, `etat`, `used`) VALUES
(0, '92QIGV6Q', 100000, '2016-10-19 14:04:52', 1, 1),
(2, '6MSYVI33', 100000, '2016-06-05 02:36:07', 0, 0),
(3, '9KW5EQM8', 100000, '2016-06-05 02:36:07', 0, 0),
(4, 'R5BH23GV', 100000, '2016-06-05 02:36:07', 0, 0),
(5, 'PS2RRV22', 100000, '2016-06-05 02:36:07', 0, 0),
(6, 'MD8W44SV', 100000, '2016-06-05 02:36:07', 0, 0),
(7, 'T6SS8AM5', 100000, '2016-06-05 02:36:07', 0, 0),
(8, 'RW5NNE96', 100000, '2016-06-05 02:36:07', 0, 0),
(9, 'XYE2PS42', 100000, '2016-06-05 02:36:07', 0, 0),
(10, 'FU66F7TU', 100000, '2016-06-05 02:36:07', 0, 0),
(11, 'YF2CRA89', 300000, '2016-06-05 02:36:07', 0, 0),
(12, 'TLGWM748', 300000, '2016-06-05 02:36:07', 0, 0),
(13, '8MK8Q9HZ', 300000, '2016-06-05 02:36:07', 0, 0),
(14, 'M5ZU2LP7', 300000, '2016-06-05 02:36:07', 0, 0),
(15, '3XDXR9Y9', 300000, '2016-06-05 02:36:07', 0, 0),
(16, 'F88H4WTK', 300000, '2016-06-05 02:36:07', 0, 0),
(17, 'U9EWDU86', 300000, '2016-06-05 02:36:07', 0, 0),
(18, 'RS3N3RQ7', 300000, '2016-06-05 02:36:07', 0, 0),
(19, 'AW3Y9PP5', 300000, '2016-06-05 02:36:07', 0, 0),
(20, 'WJG6I8R3', 300000, '2016-06-05 02:36:07', 0, 0),
(21, '77Z3ZXGQ', 300000, '2016-06-05 02:36:07', 0, 0),
(22, '6TW2P7YJ', 300000, '2016-06-05 02:36:07', 0, 0),
(23, '5FP5JQ8Y', 300000, '2016-06-05 02:36:07', 0, 0),
(24, 'FY6QSQ86', 300000, '2016-06-05 02:36:07', 0, 0),
(25, 'CIB89QH5', 300000, '2016-06-05 02:36:07', 0, 0),
(26, 'TA6Q89YA', 300000, '2016-06-05 02:36:07', 0, 0),
(27, 'SUY8W6D4', 300000, '2016-06-05 02:36:07', 0, 0),
(28, 'U6QS2W8C', 300000, '2016-06-05 02:36:07', 0, 0),
(29, 'TGEDW845', 300000, '2016-06-05 02:36:07', 0, 0),
(30, 'KVV4LH55', 300000, '2016-06-05 02:36:07', 0, 0),
(31, '5JBA7X2H', 500000, '2016-06-05 02:36:07', 0, 0),
(32, 'SD3FUM78', 500000, '2016-06-05 02:36:07', 0, 0),
(33, '7KC9VR5J', 500000, '2016-06-05 02:36:07', 0, 0),
(34, 'Q32EREE2', 500000, '2016-06-05 02:36:07', 0, 0),
(35, 'K6UNDU82\r\n', 500000, '2016-06-05 02:36:07', 0, 0),
(36, 'V8C6UHJ7', 500000, '2016-06-05 02:36:07', 0, 0),
(37, '92D4IDXF', 500000, '2016-06-05 02:36:07', 0, 0),
(38, 'USI3Z4B9', 500000, '2016-06-05 02:36:07', 0, 0),
(39, '4K8B8SAM', 500000, '2016-06-05 02:36:07', 0, 0),
(40, '3JR5FD3E', 500000, '2016-06-05 02:36:07', 0, 0),
(41, 'YTGA87V5', 50000, '2016-06-05 02:36:07', 0, 0),
(42, '9AE5BJ7M', 50000, '2016-06-05 02:36:07', 0, 0),
(43, 'EG6BC83B', 50000, '2016-06-05 02:36:07', 0, 0),
(44, 'YPHV677H', 50000, '2016-06-05 02:36:07', 0, 0),
(45, '7VIZ8AM6', 50000, '2016-06-05 02:36:07', 0, 0),
(46, 'EK7VD58U', 50000, '2016-06-05 02:36:07', 0, 0),
(47, 'D8AN8S3U', 50000, '2016-06-05 02:36:07', 0, 0),
(48, '6WA8FN8D', 50000, '2016-06-05 02:36:07', 0, 0),
(49, 'Z776RMWS\r\n', 50000, '2016-06-05 02:36:07', 0, 0),
(50, 'LZR6T8G8', 50000, '2016-06-05 02:36:07', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `concerner`
--

CREATE TABLE IF NOT EXISTS `concerner` (
  `ID_TICKET` int(255) NOT NULL,
  `ID_PARI` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `contenir`
--

CREATE TABLE IF NOT EXISTS `contenir` (
  `NUM_TIRAGE` int(255) NOT NULL,
  `ID_NUM_TIRER` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `contenir`
--

INSERT INTO `contenir` (`NUM_TIRAGE`, `ID_NUM_TIRER`) VALUES
(15, 1),
(24, 1),
(8, 2),
(10, 2),
(43, 2),
(5, 4),
(22, 4),
(6, 5),
(35, 5),
(46, 5),
(47, 5),
(49, 5),
(51, 5),
(45, 6),
(3, 7),
(13, 7),
(28, 7),
(40, 7),
(41, 7),
(14, 8),
(18, 8),
(37, 9),
(11, 10),
(26, 10),
(34, 10),
(38, 10),
(9, 11),
(12, 11),
(16, 11),
(21, 11),
(36, 11),
(39, 11),
(1, 12),
(17, 12),
(31, 12),
(46, 12),
(48, 12),
(50, 12),
(4, 13),
(6, 13),
(26, 13),
(32, 13),
(19, 14),
(27, 14),
(24, 16),
(32, 16),
(41, 16),
(42, 16),
(43, 17),
(2, 18),
(15, 18),
(38, 18),
(7, 19),
(10, 19),
(16, 19),
(30, 19),
(51, 19),
(8, 20),
(31, 20),
(47, 20),
(3, 22),
(14, 22),
(23, 23),
(13, 24),
(34, 24),
(11, 25),
(20, 25),
(44, 25),
(12, 26),
(18, 26),
(21, 26),
(29, 26),
(1, 27),
(8, 27),
(25, 27),
(34, 27),
(37, 27),
(48, 27),
(26, 28),
(27, 29),
(32, 30),
(41, 30),
(46, 30),
(48, 30),
(33, 31),
(19, 32),
(15, 33),
(20, 33),
(38, 33),
(44, 33),
(10, 34),
(24, 34),
(42, 34),
(43, 34),
(2, 35),
(25, 35),
(37, 35),
(41, 35),
(7, 36),
(30, 36),
(22, 37),
(31, 37),
(47, 37),
(49, 37),
(51, 37),
(23, 38),
(35, 38),
(13, 39),
(28, 39),
(45, 39),
(3, 40),
(14, 40),
(29, 40),
(41, 40),
(23, 41),
(11, 42),
(34, 42),
(43, 42),
(39, 43),
(1, 44),
(21, 44),
(50, 44),
(5, 45),
(6, 45),
(17, 45),
(22, 45),
(26, 45),
(35, 45),
(46, 45),
(48, 45),
(51, 45),
(27, 46),
(45, 46),
(28, 47),
(40, 48),
(33, 49),
(2, 50),
(19, 50),
(38, 50),
(7, 51),
(9, 51),
(10, 51),
(12, 51),
(15, 51),
(20, 51),
(30, 51),
(36, 51),
(39, 51),
(40, 51),
(44, 51),
(16, 52),
(25, 52),
(31, 52),
(40, 52),
(50, 52),
(17, 53),
(4, 54),
(7, 54),
(23, 55),
(47, 55),
(49, 55),
(13, 57),
(14, 57),
(15, 58),
(29, 58),
(5, 59),
(10, 59),
(8, 60),
(11, 60),
(21, 61),
(36, 61),
(40, 61),
(5, 62),
(22, 62),
(46, 62),
(48, 62),
(6, 63),
(35, 63),
(19, 64),
(27, 64),
(33, 64),
(40, 64),
(45, 64),
(20, 65),
(28, 65),
(44, 65),
(12, 66),
(18, 66),
(33, 66),
(42, 66),
(25, 67),
(37, 67),
(9, 68),
(7, 69),
(16, 69),
(30, 69),
(36, 69),
(39, 69),
(40, 69),
(49, 69),
(17, 70),
(31, 70),
(47, 70),
(50, 70),
(4, 71),
(32, 71),
(42, 71),
(3, 72),
(14, 72),
(41, 72),
(23, 73),
(24, 74),
(43, 74),
(2, 75),
(11, 75),
(29, 75),
(1, 76),
(21, 76),
(30, 76),
(5, 77),
(8, 77),
(22, 77),
(51, 77),
(6, 78),
(35, 78),
(27, 79),
(45, 79),
(3, 80),
(28, 80),
(40, 80),
(18, 81),
(33, 81),
(34, 82),
(9, 83),
(12, 83),
(20, 83),
(39, 83),
(40, 83),
(44, 83),
(1, 84),
(16, 84),
(18, 84),
(25, 84),
(36, 84),
(37, 84),
(40, 84),
(17, 85),
(26, 85),
(38, 85),
(50, 85),
(4, 86),
(9, 86),
(40, 86),
(41, 86),
(49, 87),
(32, 88),
(4, 89),
(13, 89),
(24, 89),
(42, 89),
(2, 90),
(19, 90),
(29, 90);

-- --------------------------------------------------------

--
-- Structure de la table `gagner_jackp`
--

CREATE TABLE IF NOT EXISTS `gagner_jackp` (
  `id_tick` int(255) NOT NULL,
  `id_lot` int(255) NOT NULL,
  `ID_USER_S` int(11) NOT NULL DEFAULT '0',
  `is_materiel` tinyint(1) NOT NULL,
  `nom_materiel` varchar(255) NOT NULL,
  `montant` int(255) NOT NULL,
  `date_gagner` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `gagner_jackp`
--

INSERT INTO `gagner_jackp` (`id_tick`, `id_lot`, `ID_USER_S`, `is_materiel`, `nom_materiel`, `montant`, `date_gagner`) VALUES
(17986, 1, 0, 1, 'Moto', 370000, '2016-11-04 23:15:56');

-- --------------------------------------------------------

--
-- Structure de la table `jackpot`
--

CREATE TABLE IF NOT EXISTS `jackpot` (
`id_jackp` int(11) NOT NULL,
  `nom_jackp` varchar(25) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `jackpot`
--

INSERT INTO `jackpot` (`id_jackp`, `nom_jackp`) VALUES
(1, 'Loto Plasma'),
(2, 'Cagnotte'),
(3, 'Super Lot');

-- --------------------------------------------------------

--
-- Structure de la table `lot_jackpot`
--

CREATE TABLE IF NOT EXISTS `lot_jackpot` (
`id_lot` int(11) NOT NULL,
  `id_jack` int(11) NOT NULL,
  `id_pv` int(11) NOT NULL,
  `is_matirial` int(11) NOT NULL,
  `nom_materiel` varchar(255) NOT NULL,
  `montant_jackp` int(255) NOT NULL,
  `photo` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `etat_jackp` int(255) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `share` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `lot_jackpot`
--

INSERT INTO `lot_jackpot` (`id_lot`, `id_jack`, `id_pv`, `is_matirial`, `nom_materiel`, `montant_jackp`, `photo`, `etat_jackp`, `status`, `share`) VALUES
(1, 1, 0, 1, 'Hisense: 32"', 370000, 'tv.png', 0, 1, 0),
(4, 2, 0, 0, '', 7717, 'money-logo.jpg', 21, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `mode_jeux`
--

CREATE TABLE IF NOT EXISTS `mode_jeux` (
`ID_MODE` int(11) NOT NULL,
  `MODE` varchar(30) NOT NULL,
  `NB_NUM_JOUER` int(11) NOT NULL,
  `QUOTA` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `mode_jeux`
--

INSERT INTO `mode_jeux` (`ID_MODE`, `MODE`, `NB_NUM_JOUER`, `QUOTA`) VALUES
(1, 'solo', 1, 15),
(2, 'doublet', 2, 300),
(3, 'triplet', 3, 1000);

-- --------------------------------------------------------

--
-- Structure de la table `num_jouer`
--

CREATE TABLE IF NOT EXISTS `num_jouer` (
  `NUMEROS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `num_jouer`
--

INSERT INTO `num_jouer` (`NUMEROS`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57),
(58),
(59),
(60),
(61),
(62),
(63),
(64),
(65),
(66),
(67),
(68),
(69),
(70),
(71),
(72),
(73),
(74),
(75),
(76),
(77),
(78),
(79),
(80),
(81),
(82),
(83),
(84),
(85),
(86),
(87),
(88),
(89),
(90);

-- --------------------------------------------------------

--
-- Structure de la table `num_tirer`
--

CREATE TABLE IF NOT EXISTS `num_tirer` (
  `ID_NUM_TIRER` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `num_tirer`
--

INSERT INTO `num_tirer` (`ID_NUM_TIRER`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57),
(58),
(59),
(60),
(61),
(62),
(63),
(64),
(65),
(66),
(67),
(68),
(69),
(70),
(71),
(72),
(73),
(74),
(75),
(76),
(77),
(78),
(79),
(80),
(81),
(82),
(83),
(84),
(85),
(86),
(87),
(88),
(89),
(90);

-- --------------------------------------------------------

--
-- Structure de la table `pari`
--

CREATE TABLE IF NOT EXISTS `pari` (
`ID_PARI` int(11) NOT NULL,
  `ID_MODE` int(11) NOT NULL,
  `MONTANT_MISE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `point_vente`
--

CREATE TABLE IF NOT EXISTS `point_vente` (
`id_pv` int(11) NOT NULL,
  `nom_pv` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `point_vente`
--

INSERT INTO `point_vente` (`id_pv`, `nom_pv`) VALUES
(0, 'Overall'),
(1, 'Agence Logpom'),
(2, 'Agence Logbessou'),
(3, 'Bonangang');

-- --------------------------------------------------------

--
-- Structure de la table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id_setting` int(11) NOT NULL,
  `DIFICULT_LEVEL` int(11) NOT NULL,
  `DELAI_AV_GAINS` int(11) NOT NULL,
  `PORT` varchar(255) NOT NULL,
  `GOLD_POT` int(11) NOT NULL,
  `SILVER_POT` int(11) NOT NULL,
  `IRON_POT` int(11) NOT NULL,
  `ID_GOLD` int(11) NOT NULL,
  `ID_SILVER` int(11) NOT NULL,
  `ID_IRON` int(11) NOT NULL,
  `SCREEN_SELECT` varchar(255) NOT NULL,
  `NB_SHARER` int(11) NOT NULL DEFAULT '1',
  `DELAI_JEU` int(11) NOT NULL,
  `MAX_GAIIN_SILVER` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `setting`
--

INSERT INTO `setting` (`id_setting`, `DIFICULT_LEVEL`, `DELAI_AV_GAINS`, `PORT`, `GOLD_POT`, `SILVER_POT`, `IRON_POT`, `ID_GOLD`, `ID_SILVER`, `ID_IRON`, `SCREEN_SELECT`, `NB_SHARER`, `DELAI_JEU`, `MAX_GAIIN_SILVER`) VALUES
(0, 10, 0, '9514', 2, 3, 0, 1, 2, 3, '0', 2, 30, 10000);

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
`ID_TICKET` int(255) NOT NULL,
  `id_pv` int(11) NOT NULL,
  `COD_BAR` varchar(255) NOT NULL,
  `NUM_TIRAGE` int(255) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `ID_USER_S` int(11) NOT NULL DEFAULT '0',
  `DATE_ACHAT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `STATUS` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `tirage`
--

CREATE TABLE IF NOT EXISTS `tirage` (
`NUM_TIRAGE` int(255) NOT NULL,
  `DATE_TIRAGE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `STATE` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Contenu de la table `tirage`
--

INSERT INTO `tirage` (`NUM_TIRAGE`, `DATE_TIRAGE`, `STATE`) VALUES
(1, '2016-11-20 08:56:11', 0),
(2, '2016-11-20 08:59:36', 0),
(3, '2016-11-20 09:02:30', 0),
(4, '2016-11-20 09:04:05', 0),
(5, '2016-11-20 09:05:06', 0),
(6, '2016-11-20 09:06:08', 0),
(7, '2016-11-20 09:07:10', 0),
(8, '2016-11-20 09:08:12', 0),
(9, '2016-11-20 09:09:14', 0),
(10, '2016-11-20 09:10:16', 0),
(11, '2016-11-20 09:11:18', 0),
(12, '2016-11-20 09:12:20', 0),
(13, '2016-11-20 09:18:33', 0),
(14, '2016-11-20 09:23:38', 0),
(15, '2016-11-20 09:28:43', 0),
(16, '2016-11-20 09:33:48', 0),
(17, '2016-11-20 09:38:54', 0),
(18, '2016-11-20 09:50:18', 0),
(19, '2016-11-20 09:55:23', 0),
(20, '2016-11-20 10:00:28', 0),
(21, '2016-11-20 10:05:34', 0),
(22, '2016-11-20 10:10:39', 0),
(23, '2016-11-20 10:15:44', 0),
(24, '2016-11-20 10:20:49', 0),
(25, '2016-11-20 10:25:55', 0),
(26, '2016-11-20 10:31:00', 0),
(27, '2016-11-20 10:36:05', 0),
(28, '2016-11-20 10:41:10', 0),
(29, '2016-11-20 10:46:15', 0),
(30, '2016-11-20 10:51:20', 0),
(31, '2016-11-20 10:56:26', 0),
(32, '2016-11-20 11:01:31', 0),
(33, '2016-11-20 11:06:36', 0),
(34, '2016-11-20 11:11:41', 0),
(35, '2016-11-20 12:06:08', 0),
(36, '2016-11-20 12:11:13', 0),
(37, '2016-11-20 15:10:33', 0),
(38, '2016-11-20 15:15:39', 0),
(39, '2016-11-20 15:20:44', 0),
(40, '2016-11-21 09:26:48', 0),
(41, '2016-11-21 09:26:53', 0),
(42, '2016-11-21 09:26:54', 0),
(43, '2016-11-22 20:19:37', 0),
(44, '2016-11-22 20:27:26', 0),
(45, '2016-11-22 20:41:20', 0),
(46, '2016-11-22 20:53:16', 0),
(47, '2016-11-22 20:53:47', 0),
(48, '2016-11-22 20:54:19', 0),
(49, '2016-11-22 20:54:50', 0),
(50, '2016-11-22 20:55:21', 0),
(51, '2016-11-22 20:55:53', 0);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`ID_USER` int(10) NOT NULL,
  `NOM` varchar(255) NOT NULL,
  `PRENOM` varchar(255) NOT NULL,
  `LOGIN` varchar(30) NOT NULL,
  `PWD` varchar(30) NOT NULL,
  `id_user_typ` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`ID_USER`, `NOM`, `PRENOM`, `LOGIN`, `PWD`, `id_user_typ`) VALUES
(0, 'Everybody ', 'Account', '', '', 1),
(1, 'ADMIN', 'ADMIN', '123loto', 'voiture', 1),
(3, 'arnold', 'djomo', 'ascorp', 'ascorp', 2);

-- --------------------------------------------------------

--
-- Structure de la table `users_type`
--

CREATE TABLE IF NOT EXISTS `users_type` (
`id_user_typ` int(11) NOT NULL,
  `user_typ` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `users_type`
--

INSERT INTO `users_type` (`id_user_typ`, `user_typ`) VALUES
(1, 'administration'),
(2, 'caisse');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `avoir`
--
ALTER TABLE `avoir`
 ADD PRIMARY KEY (`ID_PARI`,`NUMEROS`), ADD KEY `NUMEROS` (`NUMEROS`);

--
-- Index pour la table `cle_activation`
--
ALTER TABLE `cle_activation`
 ADD PRIMARY KEY (`id_cle`), ADD UNIQUE KEY `cle` (`cle`);

--
-- Index pour la table `concerner`
--
ALTER TABLE `concerner`
 ADD PRIMARY KEY (`ID_TICKET`,`ID_PARI`), ADD KEY `ID_PARI` (`ID_PARI`);

--
-- Index pour la table `contenir`
--
ALTER TABLE `contenir`
 ADD PRIMARY KEY (`NUM_TIRAGE`,`ID_NUM_TIRER`), ADD KEY `ID_NUM_TIRER` (`ID_NUM_TIRER`);

--
-- Index pour la table `gagner_jackp`
--
ALTER TABLE `gagner_jackp`
 ADD PRIMARY KEY (`id_tick`,`id_lot`), ADD KEY `id_jacp` (`id_lot`);

--
-- Index pour la table `jackpot`
--
ALTER TABLE `jackpot`
 ADD PRIMARY KEY (`id_jackp`);

--
-- Index pour la table `lot_jackpot`
--
ALTER TABLE `lot_jackpot`
 ADD PRIMARY KEY (`id_lot`);

--
-- Index pour la table `mode_jeux`
--
ALTER TABLE `mode_jeux`
 ADD PRIMARY KEY (`ID_MODE`);

--
-- Index pour la table `num_jouer`
--
ALTER TABLE `num_jouer`
 ADD PRIMARY KEY (`NUMEROS`);

--
-- Index pour la table `num_tirer`
--
ALTER TABLE `num_tirer`
 ADD PRIMARY KEY (`ID_NUM_TIRER`);

--
-- Index pour la table `pari`
--
ALTER TABLE `pari`
 ADD PRIMARY KEY (`ID_PARI`);

--
-- Index pour la table `point_vente`
--
ALTER TABLE `point_vente`
 ADD PRIMARY KEY (`id_pv`);

--
-- Index pour la table `setting`
--
ALTER TABLE `setting`
 ADD PRIMARY KEY (`id_setting`);

--
-- Index pour la table `ticket`
--
ALTER TABLE `ticket`
 ADD PRIMARY KEY (`ID_TICKET`);

--
-- Index pour la table `tirage`
--
ALTER TABLE `tirage`
 ADD PRIMARY KEY (`NUM_TIRAGE`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`ID_USER`);

--
-- Index pour la table `users_type`
--
ALTER TABLE `users_type`
 ADD PRIMARY KEY (`id_user_typ`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `cle_activation`
--
ALTER TABLE `cle_activation`
MODIFY `id_cle` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `jackpot`
--
ALTER TABLE `jackpot`
MODIFY `id_jackp` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `lot_jackpot`
--
ALTER TABLE `lot_jackpot`
MODIFY `id_lot` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `mode_jeux`
--
ALTER TABLE `mode_jeux`
MODIFY `ID_MODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `pari`
--
ALTER TABLE `pari`
MODIFY `ID_PARI` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `point_vente`
--
ALTER TABLE `point_vente`
MODIFY `id_pv` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `ticket`
--
ALTER TABLE `ticket`
MODIFY `ID_TICKET` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tirage`
--
ALTER TABLE `tirage`
MODIFY `NUM_TIRAGE` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
MODIFY `ID_USER` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `users_type`
--
ALTER TABLE `users_type`
MODIFY `id_user_typ` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
