﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports System.Timers
Imports System.ComponentModel

Public Class Connexion
    Private oUser As BL_Lotto.BlUser
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim rep, id_pv_rep, id_user_Typ, bookmaker_id, spli() As String
        Try
            rep = Ocaisse.UserCOnnect(txtUsername.Text.Trim, txtPassword.Text.Trim, secret.Text.Trim.ToUpper)
            spli = rep.Split(CChar("/"))
            If spli.Count > 1 Then
                'ID_USER, PRENOM, ID_PV, Id_user_Typ
                CONNECTED_USERID = spli(0)
                CONNECTED_USERNAME = spli(1)
                id_pv_rep = spli(2)
                id_user_Typ = spli(3)
                bookmaker_id = spli(4)
                If CONNECTED_USERID = "0" Then
                    MessageBox.Show("Utilisateur Invalide!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    txtUsername.Focus()
                Else
                    My.Settings.CREDENTIAL = secret.Text.Trim.ToUpper
                    My.Settings.ID_PV = id_pv_rep
                    My.Settings.ID_BOOKMAKER = bookmaker_id
                    My.Settings.Save()
                    FrmPrincipal.Show()
                    Me.Close()
                End If
            Else
                MessageBox.Show("Utilisateur Invalide!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtUsername.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show("Impossible de vous connecter! verifiez Votre Connexion", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Dispose()

    End Sub

    Private Sub Connexion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            gererConexion()
        Catch ex As Exception
            MessageBox.Show("Erreur lors de la tentative de connexion au serveur. Vérifiez que votre connexion Internet : Paramettre de connexion:. " & SERVEUR & " : " & PORT, "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        initialiseSetting()
        'checkupdate()
    End Sub

    Private Sub gererConexion()

        'Dim ip As String = Hostname2IP(SERVEUR)
        Dim ip As String = SERVEUR
        'MsgBox(ip)
        'ip = "192.164.1.5"

        MonSocketClient = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp) 'Initialise le socket
        'MsgBox(ip)
        Try
            Dim MonEP As IPEndPoint = New IPEndPoint(IPAddress.Parse(ip), CInt(PORT_COM)) 'Entre les informations de connexion
            MonSocketClient.Connect(MonEP) 'Tente de se connecter
            Ocaisse = New Client(MonSocketClient)
        Catch ex As Exception
            MessageBox.Show("Erreur lors de la tentative de connexion au serveur. Vérifiez l'ip et le port du serveur.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub checkupdate()
        Dim Updat As New WebClient
        Dim CurrentVersion As String = ProductVersion
        'Dim LastVersion As String = Updat.DownloadString("http://68.168.125.59/jeu/version.txt")
        'If (LastVersion > CurrentVersion) Then
        '    If MsgBox("Une nouvelle version du Logiciel est disponible. Souhaitez vous Telecharger?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
        '        Process.Start("http://68.168.125.59/jeu/setup/Setup1.msi")
        '    End If

        'End If


    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs)

    End Sub
End Class
