﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class jackpotgold
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(jackpotgold))
        Me.lbtitrejack = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbelNumtick = New System.Windows.Forms.Label()
        Me.lbeljackpot = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.pb_photo_lot = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.AxWindowsMediaPlayer1 = New AxWMPLib.AxWindowsMediaPlayer()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pb_photo_lot, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbtitrejack
        '
        Me.lbtitrejack.AutoSize = True
        Me.lbtitrejack.BackColor = System.Drawing.Color.Black
        Me.TableLayoutPanel1.SetColumnSpan(Me.lbtitrejack, 2)
        Me.lbtitrejack.Font = New System.Drawing.Font("Century Gothic", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbtitrejack.ForeColor = System.Drawing.Color.White
        Me.lbtitrejack.Location = New System.Drawing.Point(1552, 161)
        Me.lbtitrejack.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lbtitrejack.Name = "lbtitrejack"
        Me.lbtitrejack.Padding = New System.Windows.Forms.Padding(10)
        Me.lbtitrejack.Size = New System.Drawing.Size(874, 128)
        Me.lbtitrejack.TabIndex = 1
        Me.lbtitrejack.Text = "SUPER CAGNOTTE"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel1.SetColumnSpan(Me.Label3, 2)
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(170, 33)
        Me.Label3.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Padding = New System.Windows.Forms.Padding(10)
        Me.Label3.Size = New System.Drawing.Size(666, 97)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Ticket(s) gagnant(s)"
        '
        'lbelNumtick
        '
        Me.lbelNumtick.AutoSize = True
        Me.lbelNumtick.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel1.SetColumnSpan(Me.lbelNumtick, 3)
        Me.lbelNumtick.Font = New System.Drawing.Font("Century Gothic", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbelNumtick.ForeColor = System.Drawing.Color.Black
        Me.lbelNumtick.Location = New System.Drawing.Point(848, 33)
        Me.lbelNumtick.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lbelNumtick.Name = "lbelNumtick"
        Me.lbelNumtick.Padding = New System.Windows.Forms.Padding(10)
        Me.lbelNumtick.Size = New System.Drawing.Size(1970, 97)
        Me.lbelNumtick.TabIndex = 1
        Me.lbelNumtick.Text = "000179840003 - 000179840003 - 000179840003 - 000179840003" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'lbeljackpot
        '
        Me.lbeljackpot.AutoSize = True
        Me.lbeljackpot.BackColor = System.Drawing.Color.Black
        Me.TableLayoutPanel1.SetColumnSpan(Me.lbeljackpot, 3)
        Me.lbeljackpot.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbeljackpot.ForeColor = System.Drawing.Color.White
        Me.lbeljackpot.Location = New System.Drawing.Point(1552, 417)
        Me.lbeljackpot.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lbeljackpot.Name = "lbeljackpot"
        Me.lbeljackpot.Padding = New System.Windows.Forms.Padding(10)
        Me.lbeljackpot.Size = New System.Drawing.Size(699, 106)
        Me.lbeljackpot.TabIndex = 1
        Me.lbeljackpot.Text = "COAF-2311-26254"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Image = Global.PL_Lotto.My.Resources.Resources.Loto
        Me.PictureBox1.Location = New System.Drawing.Point(2960, 39)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(173, 116)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'pb_photo_lot
        '
        Me.pb_photo_lot.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.SetColumnSpan(Me.pb_photo_lot, 3)
        Me.pb_photo_lot.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pb_photo_lot.Image = Global.PL_Lotto.My.Resources.Resources.congratanim
        Me.pb_photo_lot.Location = New System.Drawing.Point(1552, 551)
        Me.pb_photo_lot.Margin = New System.Windows.Forms.Padding(6)
        Me.pb_photo_lot.Name = "pb_photo_lot"
        Me.TableLayoutPanel1.SetRowSpan(Me.pb_photo_lot, 7)
        Me.pb_photo_lot.Size = New System.Drawing.Size(1581, 884)
        Me.pb_photo_lot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pb_photo_lot.TabIndex = 5
        Me.pb_photo_lot.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel1.SetColumnSpan(Me.Label1, 3)
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(1552, 289)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Padding = New System.Windows.Forms.Padding(10)
        Me.Label1.Size = New System.Drawing.Size(1172, 106)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Felicitation à tous les Gagnants"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.backloto_1_flou
        Me.TableLayoutPanel1.ColumnCount = 8
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.397218!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.53364!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.53364!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.10787!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.10787!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.10787!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.814661!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.397218!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lbelNumtick, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox1, 6, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lbtitrejack, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 4, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.lbeljackpot, 4, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.pb_photo_lot, 4, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.AxWindowsMediaPlayer1, 1, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 13
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.256945!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.680556!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.256945!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(3186, 1477)
        Me.TableLayoutPanel1.TabIndex = 7
        '
        'AxWindowsMediaPlayer1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.AxWindowsMediaPlayer1, 3)
        Me.AxWindowsMediaPlayer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AxWindowsMediaPlayer1.Enabled = True
        Me.AxWindowsMediaPlayer1.Location = New System.Drawing.Point(47, 164)
        Me.AxWindowsMediaPlayer1.Name = "AxWindowsMediaPlayer1"
        Me.AxWindowsMediaPlayer1.OcxState = CType(resources.GetObject("AxWindowsMediaPlayer1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.TableLayoutPanel1.SetRowSpan(Me.AxWindowsMediaPlayer1, 10)
        Me.AxWindowsMediaPlayer1.Size = New System.Drawing.Size(1496, 1274)
        Me.AxWindowsMediaPlayer1.TabIndex = 6
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'jackpotgold
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.backloto_1_flou1
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(3186, 1477)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "jackpotgold"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Felicitation!!!"
        Me.TopMost = True
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pb_photo_lot, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lbtitrejack As System.Windows.Forms.Label
    Friend WithEvents lbeljackpot As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lbelNumtick As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents pb_photo_lot As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents AxWindowsMediaPlayer1 As AxWMPLib.AxWindowsMediaPlayer
    Friend WithEvents Timer1 As Timer
End Class
