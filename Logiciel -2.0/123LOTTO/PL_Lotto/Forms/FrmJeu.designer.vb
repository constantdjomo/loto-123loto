﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmJeu))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.LB_NumTir = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.num1 = New System.Windows.Forms.Label()
        Me.num2 = New System.Windows.Forms.Label()
        Me.num3 = New System.Windows.Forms.Label()
        Me.num4 = New System.Windows.Forms.Label()
        Me.num5 = New System.Windows.Forms.Label()
        Me.anim_boule_lb = New System.Windows.Forms.Label()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Tag = "time"
        '
        'LB_NumTir
        '
        Me.LB_NumTir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LB_NumTir.AutoSize = True
        Me.LB_NumTir.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.SetColumnSpan(Me.LB_NumTir, 2)
        Me.LB_NumTir.Font = New System.Drawing.Font("Showcard Gothic", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_NumTir.ForeColor = System.Drawing.Color.White
        Me.LB_NumTir.Location = New System.Drawing.Point(1629, 63)
        Me.LB_NumTir.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.LB_NumTir.Name = "LB_NumTir"
        Me.LB_NumTir.Size = New System.Drawing.Size(245, 119)
        Me.LB_NumTir.TabIndex = 18
        Me.LB_NumTir.Text = "1024"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Black
        Me.TableLayoutPanel2.BackgroundImage = CType(resources.GetObject("TableLayoutPanel2.BackgroundImage"), System.Drawing.Image)
        Me.TableLayoutPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TableLayoutPanel2.ColumnCount = 7
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.120429!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.55183!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.55183!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.55183!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.55183!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.55183!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.120429!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label1, 2, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.LB_NumTir, 4, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.num1, 1, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.num2, 2, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.num3, 3, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.num4, 4, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.num5, 5, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.anim_boule_lb, 2, 3)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Font = New System.Drawing.Font("Showcard Gothic", 72.0!)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(6)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 9
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.188298!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.16269!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.188298!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.27103!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.27103!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.27103!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.188298!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.27103!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.188298!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(2716, 1476)
        Me.TableLayoutPanel2.TabIndex = 41
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.SetColumnSpan(Me.Label1, 2)
        Me.Label1.Font = New System.Drawing.Font("Showcard Gothic", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(1041, 63)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(576, 119)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Tirage No:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'num1
        '
        Me.num1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.num1.BackColor = System.Drawing.Color.Transparent
        Me.num1.Font = New System.Drawing.Font("Showcard Gothic", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num1.ForeColor = System.Drawing.Color.White
        Me.num1.Image = CType(resources.GetObject("num1.Image"), System.Drawing.Image)
        Me.num1.Location = New System.Drawing.Point(36, 1143)
        Me.num1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.num1.Name = "num1"
        Me.num1.Size = New System.Drawing.Size(519, 299)
        Me.num1.TabIndex = 21
        Me.num1.Text = "??"
        Me.num1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num2
        '
        Me.num2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.num2.BackColor = System.Drawing.Color.Transparent
        Me.num2.Font = New System.Drawing.Font("Showcard Gothic", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num2.ForeColor = System.Drawing.Color.White
        Me.num2.Image = CType(resources.GetObject("num2.Image"), System.Drawing.Image)
        Me.num2.Location = New System.Drawing.Point(567, 1143)
        Me.num2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.num2.Name = "num2"
        Me.num2.Size = New System.Drawing.Size(519, 299)
        Me.num2.TabIndex = 19
        Me.num2.Text = "??"
        Me.num2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num3
        '
        Me.num3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.num3.BackColor = System.Drawing.Color.Transparent
        Me.num3.Font = New System.Drawing.Font("Showcard Gothic", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num3.ForeColor = System.Drawing.Color.White
        Me.num3.Image = CType(resources.GetObject("num3.Image"), System.Drawing.Image)
        Me.num3.Location = New System.Drawing.Point(1098, 1143)
        Me.num3.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.num3.Name = "num3"
        Me.num3.Size = New System.Drawing.Size(519, 299)
        Me.num3.TabIndex = 16
        Me.num3.Text = "??"
        Me.num3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num4
        '
        Me.num4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.num4.BackColor = System.Drawing.Color.Transparent
        Me.num4.Font = New System.Drawing.Font("Showcard Gothic", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num4.ForeColor = System.Drawing.Color.White
        Me.num4.Image = CType(resources.GetObject("num4.Image"), System.Drawing.Image)
        Me.num4.Location = New System.Drawing.Point(1629, 1143)
        Me.num4.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.num4.Name = "num4"
        Me.num4.Size = New System.Drawing.Size(519, 299)
        Me.num4.TabIndex = 17
        Me.num4.Text = "??"
        Me.num4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num5
        '
        Me.num5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.num5.BackColor = System.Drawing.Color.Transparent
        Me.num5.Font = New System.Drawing.Font("Showcard Gothic", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num5.ForeColor = System.Drawing.Color.White
        Me.num5.Image = CType(resources.GetObject("num5.Image"), System.Drawing.Image)
        Me.num5.Location = New System.Drawing.Point(2160, 1143)
        Me.num5.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.num5.Name = "num5"
        Me.num5.Size = New System.Drawing.Size(519, 299)
        Me.num5.TabIndex = 20
        Me.num5.Text = "??"
        Me.num5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'anim_boule_lb
        '
        Me.anim_boule_lb.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.anim_boule_lb.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.SetColumnSpan(Me.anim_boule_lb, 3)
        Me.anim_boule_lb.Font = New System.Drawing.Font("Showcard Gothic", 199.875!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.anim_boule_lb.ForeColor = System.Drawing.Color.White
        Me.anim_boule_lb.Location = New System.Drawing.Point(567, 214)
        Me.anim_boule_lb.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.anim_boule_lb.Name = "anim_boule_lb"
        Me.TableLayoutPanel2.SetRowSpan(Me.anim_boule_lb, 3)
        Me.anim_boule_lb.Size = New System.Drawing.Size(1581, 897)
        Me.anim_boule_lb.TabIndex = 17
        Me.anim_boule_lb.Text = "??"
        Me.anim_boule_lb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Timer2
        '
        '
        'FrmJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(192.0!, 192.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.Black
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(2716, 1476)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "FrmJeu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "123Lotto-Jeu"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents num4 As System.Windows.Forms.Label
    Friend WithEvents num3 As System.Windows.Forms.Label
    Friend WithEvents num2 As System.Windows.Forms.Label
    Friend WithEvents num1 As System.Windows.Forms.Label
    Friend WithEvents LB_NumTir As System.Windows.Forms.Label
    Friend WithEvents num5 As Label
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Timer2 As Timer
    Friend WithEvents Label1 As Label
    Friend WithEvents anim_boule_lb As Label
End Class
