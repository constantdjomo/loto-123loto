﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComptes
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BtOk = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DpFin = New System.Windows.Forms.DateTimePicker()
        Me.DpDebut = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.LbBenefice = New System.Windows.Forms.Label()
        Me.LbTotalSortie = New System.Windows.Forms.Label()
        Me.LbTotalMise = New System.Windows.Forms.Label()
        Me.LbNbtTicketGagna = New System.Windows.Forms.Label()
        Me.LbNbTicket = New System.Windows.Forms.Label()
        Me.LbNbTirage = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TbNomUser = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Cbuser = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.LbNbtTicketJack = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BtOk)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.DpFin)
        Me.GroupBox1.Controls.Add(Me.DpDebut)
        Me.GroupBox1.Location = New System.Drawing.Point(444, 20)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(494, 115)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Date"
        '
        'BtOk
        '
        Me.BtOk.Location = New System.Drawing.Point(199, 74)
        Me.BtOk.Name = "BtOk"
        Me.BtOk.Size = New System.Drawing.Size(101, 33)
        Me.BtOk.TabIndex = 3
        Me.BtOk.Text = "&Ok"
        Me.BtOk.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(363, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(21, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Fin"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(86, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Debut"
        '
        'DpFin
        '
        Me.DpFin.Location = New System.Drawing.Point(276, 44)
        Me.DpFin.MinDate = New Date(2014, 1, 1, 0, 0, 0, 0)
        Me.DpFin.Name = "DpFin"
        Me.DpFin.Size = New System.Drawing.Size(200, 20)
        Me.DpFin.TabIndex = 1
        '
        'DpDebut
        '
        Me.DpDebut.CustomFormat = "yyyy/mm/dd"
        Me.DpDebut.Location = New System.Drawing.Point(20, 44)
        Me.DpDebut.MinDate = New Date(2014, 1, 1, 0, 0, 0, 0)
        Me.DpDebut.Name = "DpDebut"
        Me.DpDebut.Size = New System.Drawing.Size(200, 20)
        Me.DpDebut.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.LbBenefice)
        Me.GroupBox2.Controls.Add(Me.LbTotalSortie)
        Me.GroupBox2.Controls.Add(Me.LbTotalMise)
        Me.GroupBox2.Controls.Add(Me.LbNbtTicketJack)
        Me.GroupBox2.Controls.Add(Me.LbNbtTicketGagna)
        Me.GroupBox2.Controls.Add(Me.LbNbTicket)
        Me.GroupBox2.Controls.Add(Me.LbNbTirage)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Location = New System.Drawing.Point(21, 159)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(917, 447)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'LbBenefice
        '
        Me.LbBenefice.AutoSize = True
        Me.LbBenefice.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbBenefice.Location = New System.Drawing.Point(275, 376)
        Me.LbBenefice.Name = "LbBenefice"
        Me.LbBenefice.Size = New System.Drawing.Size(19, 20)
        Me.LbBenefice.TabIndex = 3
        Me.LbBenefice.Text = "0"
        '
        'LbTotalSortie
        '
        Me.LbTotalSortie.AutoSize = True
        Me.LbTotalSortie.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbTotalSortie.Location = New System.Drawing.Point(275, 321)
        Me.LbTotalSortie.Name = "LbTotalSortie"
        Me.LbTotalSortie.Size = New System.Drawing.Size(0, 20)
        Me.LbTotalSortie.TabIndex = 3
        '
        'LbTotalMise
        '
        Me.LbTotalMise.AutoSize = True
        Me.LbTotalMise.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbTotalMise.Location = New System.Drawing.Point(275, 266)
        Me.LbTotalMise.Name = "LbTotalMise"
        Me.LbTotalMise.Size = New System.Drawing.Size(0, 20)
        Me.LbTotalMise.TabIndex = 3
        '
        'LbNbtTicketGagna
        '
        Me.LbNbtTicketGagna.AutoSize = True
        Me.LbNbtTicketGagna.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbNbtTicketGagna.Location = New System.Drawing.Point(275, 156)
        Me.LbNbtTicketGagna.Name = "LbNbtTicketGagna"
        Me.LbNbtTicketGagna.Size = New System.Drawing.Size(0, 20)
        Me.LbNbtTicketGagna.TabIndex = 3
        '
        'LbNbTicket
        '
        Me.LbNbTicket.AutoSize = True
        Me.LbNbTicket.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbNbTicket.Location = New System.Drawing.Point(275, 101)
        Me.LbNbTicket.Name = "LbNbTicket"
        Me.LbNbTicket.Size = New System.Drawing.Size(0, 20)
        Me.LbNbTicket.TabIndex = 3
        '
        'LbNbTirage
        '
        Me.LbNbTirage.AutoSize = True
        Me.LbNbTirage.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbNbTirage.Location = New System.Drawing.Point(275, 46)
        Me.LbNbTirage.Name = "LbNbTirage"
        Me.LbNbTirage.Size = New System.Drawing.Size(0, 20)
        Me.LbNbTirage.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(69, 376)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(150, 20)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Gain Total (fcfa) :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(80, 321)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(139, 20)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Sortie Total (fcfa) :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(66, 266)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(153, 20)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Entrées Total (fcfa) :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(38, 156)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(181, 20)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Total Tickets Gagnants :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(54, 101)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(165, 20)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Total Tickets Vendus :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(21, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(198, 20)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Nombres Total de Tirages :"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(410, 612)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(110, 39)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "&Fermer"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.PL_Lotto.My.Resources.Resources.AucunePersonne
        Me.PictureBox1.Location = New System.Drawing.Point(21, 20)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(179, 113)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'TbNomUser
        '
        Me.TbNomUser.AutoSize = True
        Me.TbNomUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbNomUser.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.TbNomUser.Location = New System.Drawing.Point(206, 40)
        Me.TbNomUser.Name = "TbNomUser"
        Me.TbNomUser.Size = New System.Drawing.Size(103, 16)
        Me.TbNomUser.TabIndex = 0
        Me.TbNomUser.Text = "NAMGNI Arnold"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.Label9.Location = New System.Drawing.Point(206, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(132, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "mercredi 23 juin 2014"
        '
        'Cbuser
        '
        Me.Cbuser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cbuser.FormattingEnabled = True
        Me.Cbuser.Location = New System.Drawing.Point(269, 114)
        Me.Cbuser.Name = "Cbuser"
        Me.Cbuser.Size = New System.Drawing.Size(155, 21)
        Me.Cbuser.TabIndex = 4
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.Label10.Location = New System.Drawing.Point(206, 115)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(57, 16)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Resultat"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(17, 211)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(202, 20)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Tickets Gagnants Jackpot :"
        '
        'LbNbtTicketJack
        '
        Me.LbNbtTicketJack.AutoSize = True
        Me.LbNbtTicketJack.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbNbtTicketJack.Location = New System.Drawing.Point(275, 211)
        Me.LbNbtTicketJack.Name = "LbNbtTicketJack"
        Me.LbNbtTicketJack.Size = New System.Drawing.Size(0, 20)
        Me.LbNbtTicketJack.TabIndex = 3
        '
        'FrmComptes
        '
        Me.AcceptButton = Me.BtOk
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkSlateGray
        Me.ClientSize = New System.Drawing.Size(963, 677)
        Me.Controls.Add(Me.Cbuser)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.TbNomUser)
        Me.Name = "FrmComptes"
        Me.Text = "123Loto - Bilan Périodique"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DpFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents DpDebut As System.Windows.Forms.DateTimePicker
    Friend WithEvents BtOk As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents LbBenefice As System.Windows.Forms.Label
    Friend WithEvents LbTotalSortie As System.Windows.Forms.Label
    Friend WithEvents LbTotalMise As System.Windows.Forms.Label
    Friend WithEvents LbNbtTicketGagna As System.Windows.Forms.Label
    Friend WithEvents LbNbTicket As System.Windows.Forms.Label
    Friend WithEvents LbNbTirage As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TbNomUser As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Cbuser As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents LbNbtTicketJack As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
