﻿Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing
Imports System

Public Class setting
#Region "bitmaps"

    Private oImage As Byte() = {
        &H42, &H4D, &HC6, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H76, &H0, &H0, &H0, &H28, &H0,
        &H0, &H0, &HB, &H0, &H0, &H0, &HA, &H0,
        &H0, &H0, &H1, &H0, &H4, &H0, &H0, &H0,
        &H0, &H0, &H50, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H0, &H0, &H10, &H0,
        &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H80, &H0, &H0, &H80,
        &H0, &H0, &H0, &H80, &H80, &H0, &H80, &H0,
        &H0, &H0, &H80, &H0, &H80, &H0, &H80, &H80,
        &H0, &H0, &HC0, &HC0, &HC0, &H0, &H80, &H80,
        &H80, &H0, &H0, &H0, &HFF, &H0, &H0, &HFF,
        &H0, &H0, &H0, &HFF, &HFF, &H0, &HFF, &H0,
        &H0, &H0, &HFF, &H0, &HFF, &H0, &HFF, &HFF,
        &H0, &H0, &HFF, &HFF, &HFF, &H0, &HFF, &HFF,
        &H0, &HF, &HFF, &HF0, &H0, &H0, &HFF, &H0,
        &HFF, &HF0, &HF, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HF, &HFF,
        &HFF, &HFF, &HFF, &H0, &H0, &H0, &HF, &HFF,
        &HFF, &HFF, &HFF, &H0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HFF, &H0,
        &HFF, &HF0, &HF, &HF0, &H0, &H0, &HFF, &HFF,
        &H0, &HF, &HFF, &HF0, &H0, &H0}

    Private xImage As Byte() = {
        &H42, &H4D, &HC6, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H76, &H0, &H0, &H0, &H28, &H0,
        &H0, &H0, &HB, &H0, &H0, &H0, &HA, &H0,
        &H0, &H0, &H1, &H0, &H4, &H0, &H0, &H0,
        &H0, &H0, &H50, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H0, &H0, &H10, &H0,
        &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H80, &H0, &H0, &H80,
        &H0, &H0, &H0, &H80, &H80, &H0, &H80, &H0,
        &H0, &H0, &H80, &H0, &H80, &H0, &H80, &H80,
        &H0, &H0, &HC0, &HC0, &HC0, &H0, &H80, &H80,
        &H80, &H0, &H0, &H0, &HFF, &H0, &H0, &HFF,
        &H0, &H0, &H0, &HFF, &HFF, &H0, &HFF, &H0,
        &H0, &H0, &HFF, &H0, &HFF, &H0, &HFF, &HFF,
        &H0, &H0, &HFF, &HFF, &HFF, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HFF, &HF,
        &HFF, &HFF, &HF, &HF0, &H0, &H0, &HFF, &HF0,
        &HFF, &HF0, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HF, &HF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HF, &HF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HF, &HF, &HFF, &HF0, &H0, &H0, &HFF, &HF0,
        &HFF, &HF0, &HFF, &HF0, &H0, &H0, &HFF, &HF,
        &HFF, &HFF, &HF, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0}

    Private blankImage As Byte() = {
        &H42, &H4D, &HC6, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H76, &H0, &H0, &H0, &H28, &H0,
        &H0, &H0, &HB, &H0, &H0, &H0, &HA, &H0,
        &H0, &H0, &H1, &H0, &H4, &H0, &H0, &H0,
        &H0, &H0, &H50, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H0, &H0, &H10, &H0,
        &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H80, &H0, &H0, &H80,
        &H0, &H0, &H0, &H80, &H80, &H0, &H80, &H0,
        &H0, &H0, &H80, &H0, &H80, &H0, &H80, &H80,
        &H0, &H0, &HC0, &HC0, &HC0, &H0, &H80, &H80,
        &H80, &H0, &H0, &H0, &HFF, &H0, &H0, &HFF,
        &H0, &H0, &H0, &HFF, &HFF, &H0, &HFF, &H0,
        &H0, &H0, &HFF, &H0, &HFF, &H0, &HFF, &HFF,
        &H0, &H0, &HFF, &HFF, &HFF, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0}
#End Region
    Private x As Bitmap = New Bitmap(New MemoryStream(xImage))
    Private o As Bitmap = New Bitmap(New MemoryStream(oImage))

    Private blank As Bitmap = New Bitmap(New MemoryStream(blankImage))
    Private bitmapPadding As Integer = 6

    Private ojacpot As BL_Lotto.BlJackot
    Dim Coleu As Drawing.Color
    Public is_material_gold As Integer = 1
    Public is_material_silver As Integer = 1
    Public is_material_bronze As Integer = 1
    Private oDataSet, dstjack1, dstjack2 As Data.DataSet
    Private ojackpotDataSet As DataSet
    Private Selectsilver, Selectbronze As String
    Private Selectjackpot As String
    Dim scr As Screen() = Screen.AllScreens

    Private Sub BtValider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtValider.Click
        My.Settings.PRINTER_NAME = TbNameImp.Text
        If (IsNumeric(Tb_refsale.Text.Trim)) Then
            If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then

                My.Settings.SCREEN_SELECT = CStr(getScreenNumber(Me.cmbDevices.Text.ToString()))
                My.Settings.ID_BOOKMAKER = bookmaker_id.Text
                My.Settings.ID_PV = Tb_refsale.Text

                My.Settings.Save()
                MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        Else
            MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If


    End Sub

    Private Function getScreenNumber(ByVal DeviceID As String) As Integer
        Dim i As Integer = 0
        For Each s As Screen In scr
            'MsgBox("device " & s.DeviceName)
            'MsgBox("selected " & "\\.\" & DeviceID)
            If (s.DeviceName = "\\.\" & DeviceID) Then Return i
            i += 1
        Next

        'if cannot find the device reset to the default 0
        Return 0
    End Function



    Private Sub setting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TbNameImp.Text = My.Settings.PRINTER_NAME

        Tb_refsale.Text = My.Settings.ID_PV

        bookmaker_id.Text = My.Settings.ID_BOOKMAKER



        Using ojacpot As New BL_Lotto.BlJackot(source)

            chergergrid_screen()
        End Using

    End Sub
    Private Sub chergergrid_screen()

        Dim strDeviceName As String

        Me.cmbDevices.Items.Clear()
        For Each s As System.Windows.Forms.Screen In scr
            strDeviceName = s.DeviceName.Replace("\\.\", "")
            'strDeviceName = s.DeviceName
            'Me.cmbDevices.Items.Add(strDeviceName)
            ' you can check the device Is primary Or Not this way
            'If (s.Primary) Then
            'Me.cmbDevices.Items.Add(">" + strDeviceName)
            'Else
            Me.cmbDevices.Items.Add(strDeviceName)
            'End If
        Next
        For i As Integer = 0 To Me.cmbDevices.Items.Count - 1
            If (My.Settings.SCREEN_SELECT = CStr(getScreenNumber(Me.cmbDevices.Items(i).ToString))) Then
                Me.cmbDevices.SelectedIndex = i
            End If
        Next

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If (IsNumeric(Tb_refsale.Text.Trim)) Then
            If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                Using ojacpot As New BL_Lotto.BlJackot(source)
                    'MsgBox(CBool(is_material_silver))
                    'ojacpot.Maj_mon_Jackpot(ID_SILVER, Tbsilver.Text.Trim, CBool(is_material_silver), tbNomLotSilver.Text.Trim)
                    My.Settings.SCREEN_SELECT = CStr(getScreenNumber(Me.cmbDevices.Text.ToString()))
                    'PersistSetting()

                    MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)

                End Using
            End If

        Else
            MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub


    'Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
    '    If RadioButton1.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton1.BackColor

    '        RadioButton1.BackColor = RadioButton1.ForeColor
    '        RadioButton1.ForeColor = Coleu

    '        is_material_gold = 1
    '        Panel7.Visible = True
    '        tbNomLotGold.Visible = True

    '    Else
    '        Coleu = RadioButton1.BackColor
    '        RadioButton1.BackColor = RadioButton1.ForeColor
    '        RadioButton1.ForeColor = Coleu

    '    End If
    'End Sub

    'Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
    '    If RadioButton2.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton2.BackColor

    '        RadioButton2.BackColor = RadioButton2.ForeColor
    '        RadioButton2.ForeColor = Coleu
    '        is_material_gold = 0
    '        Panel7.Visible = False
    '        tbNomLotGold.Visible = False
    '    Else
    '        Coleu = RadioButton2.BackColor
    '        RadioButton2.BackColor = RadioButton2.ForeColor
    '        RadioButton2.ForeColor = Coleu
    '    End If
    'End Sub

    'Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If RadioButton4.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton4.BackColor

    '        RadioButton4.BackColor = RadioButton4.ForeColor
    '        RadioButton4.ForeColor = Coleu

    '        is_material_silver = 1
    '        Panel8.Visible = True
    '        tbNomLotSilver.Visible = True

    '    Else
    '        Coleu = RadioButton4.BackColor
    '        RadioButton4.BackColor = RadioButton4.ForeColor
    '        RadioButton4.ForeColor = Coleu

    '    End If
    'End Sub

    'Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If RadioButton3.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton3.BackColor

    '        RadioButton3.BackColor = RadioButton3.ForeColor
    '        RadioButton3.ForeColor = Coleu
    '        is_material_silver = 0
    '        Panel8.Visible = False
    '        tbNomLotSilver.Visible = False
    '    Else
    '        Coleu = RadioButton3.BackColor
    '        RadioButton3.BackColor = RadioButton3.ForeColor
    '        RadioButton3.ForeColor = Coleu
    '    End If
    'End Sub


    Private Sub TabPage2_Click(sender As Object, e As EventArgs)

    End Sub







    'Private Sub Button13_Click(sender As Object, e As EventArgs)
    '    If (TbBronze.Text <> "" And IsNumeric(TbBronze.Text.Trim) And bonPourcent()) Then
    '        If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
    '            Using ojacpot As New BL_Lotto.BlJackot(source)
    '                'MsgBox(CBool(is_material_silver))
    '                ojacpot.Maj_mon_Jackpot(ID_IRON, TbBronze.Text.Trim, CBool(is_material_bronze), tbNomLotBronze.Text.Trim)
    '                IRON_POT = CInt(TrackBar4.Value)
    '                MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            End Using
    '        End If
    '    Else
    '        MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End If
    'End Sub


    Private Sub TextBox3_TextChanged(sender As Object, e As EventArgs)

    End Sub

End Class