﻿Imports System.IO
Public Class ajouter_lot

    Public is_material As Integer = 0
    Dim Coleu As Drawing.Color
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim d As String()
        Dim fname As String = "default.jpg"
        If (OpenFileDialog1.FileName = "") Then
            fname = "default.jpg"
        Else
            fname = System.IO.Path.GetFileName(PictureBox1.ImageLocation)
        End If
        d = cb_type_jack.Text.Split("#")
        'MsgBox(d(0))

        If (Tbmontant_jack.Text <> "" And IsNumeric(Tbmontant_jack.Text.Trim)) Then
            If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                Using ojacpot As New BL_Lotto.BlJackot(source)
                    'MsgBox(CBool(is_material_silver))

                    If d.Length >= 2 Then
                        tbid_jck.Text = d(0)
                        ojacpot.Ajouter_lot_Jackpot(tbid_jck.Text, Tbmontant_jack.Text.Trim, CBool(is_material), tbNomLotSilver.Text.Trim, fname)
                        MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        If (OpenFileDialog1.FileName <> "") Then
                            If (Not Directory.Exists(Application.StartupPath & "\photo")) Then
                                Directory.CreateDirectory(Application.StartupPath & "\photo")
                            End If
                            FileCopy(PictureBox1.ImageLocation, Application.StartupPath & "\photo\" & fname)
                        End If

                    Else
                        MessageBox.Show("veillez selectionner un type.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If

                End Using
            End If

        Else
            MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then ' Teste si le bouton est coché.
            Coleu = CheckBox1.BackColor

            CheckBox1.BackColor = CheckBox1.ForeColor
            CheckBox1.ForeColor = Coleu
            CheckBox1.Text = "OUI"
            is_material = 1
            Panel8.Visible = True
            tbNomLotSilver.Visible = True

        Else
            Coleu = CheckBox1.BackColor
            CheckBox1.BackColor = CheckBox1.ForeColor
            CheckBox1.ForeColor = Coleu
            is_material = 0
            CheckBox1.Text = "NON"
            Panel8.Visible = False
            tbNomLotSilver.Visible = False
        End If
        'MsgBox(is_material)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        With OpenFileDialog1
            .Title = "Ouvrir"
            .InitialDirectory = "c:\"
            .Filter = "Fichiers png|*.png;*.jpg"
            .Multiselect = False
            .ValidateNames = True
            .CheckFileExists = True
            .AddExtension = True
        End With

        If (OpenFileDialog1.ShowDialog() = DialogResult.OK) Then
            If (OpenFileDialog1.FileName = "") Then
                MsgBox("veuillez choisir un fichier")
                Exit Sub
            Else
                PictureBox1.ImageLocation = OpenFileDialog1.FileName
            End If

        End If
    End Sub
End Class