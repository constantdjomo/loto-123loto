﻿Public Class ParamConnexion
    Private oUser As BL_Lotto.BlUser
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim rep, spli() As String
        Try
            rep = Ocaisse.UserCOnnect(txtUsername.Text.Trim, txtPassword.Text.Trim, My.Settings.CREDENTIAL)
            spli = rep.Split(CChar("/"))
            If spli.Count > 1 Then
                CONNECTED_USERID = spli(0)
                CONNECTED_USERNAME = spli(1)
                If CONNECTED_USERID = "0" Then
                    MessageBox.Show("Utilisateur Invalide!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    txtUsername.Focus()
                Else
                    If (oUser.can(CType(oUser.GetUserID(txtUsername.Text.Trim, txtPassword.Text.Trim), String))) Then
                        setting.Show()
                        Me.Close()
                    Else
                        MessageBox.Show("Acces Refuser", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                    End If
                End If
            Else
                MessageBox.Show("Utilisateur Invalide!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtUsername.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show("Impossible de vous connecter! verifiez Votre Connexion", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Dispose()
    End Sub

    Private Sub ParamConnexion_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
