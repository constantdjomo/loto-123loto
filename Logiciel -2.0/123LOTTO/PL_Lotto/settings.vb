﻿Imports MySql
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports MySql.Data.Types

Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary

Module settings
    Public ListeClients As List(Of Client) 'Liste destinée à contenir les clients connectés

    Public TirageLancer As Integer = 0
    Public Resultat() As String = {"12", "21", "23", "14"}
    Public timetosend As String = "0"
    Public animaDemarer As Boolean = False
    Public animaDejaDemarer As Boolean = False
    Public jackpotDejaDemarer As Boolean = False
    Public notificationTexte As String = " *** Loto Smart Phone *** "

    Public notificationPhoto As String = "tv.png"
    Public notificationLot As String = " Loto Smart Phone "
    Public notificationTicket As String = " 012323432112 "


    Public Ocaisse As Client

    Public MonSocketServeur, MonSocketClient As Socket
    Public MonThread As Thread

    Dim Oactivation As BL_Lotto.BlActivation
    Public CONNECTED_USERID As String = ""
    Public CONNECTED_USERNAME As String = ""
    'Public SERVEUR As String = "10.8.0.1"
    'Public SERVEUR As String = "127.0.0.1"
    Public SERVEUR As String = "10.8.0.1"
    Public PORT_COM As String = "9514"
    Public BD As String = "bd_loto"
    'Public ID As String = "loto"
    Public ID As String = "loto"
    Public PWD As String = "jlprom2at@"
    Public source As String = "server=" & SERVEUR & ";user id=" & ID & ";Password=" & PWD & ";persist security info=True;database=" & BD
#Region "definition des settings"

    Public MONTH_BOOST_LIMIT, MAX_BOOST_LIMIT, DIFICULT_LEVEL, DELAI_AV_GAINS, GOLD_POT, SILVER_POT, IRON_POT, CAGNOTE_POT, MSTERIOUS_POT, MEGA_POT, ID_CAGNOTE, ID_GOLD, ID_SILVER, ID_IRON, ID_MSTERIOUS, ID_MEGA, NB_SHARER, MAX_GAIIN_SILVER, MIN_GAIIN_SILVER, MAX_GAIN_IRON, MIN_GAIN_IRON, MIN_LOTO_PLASMA, MIN_PARI_JACKPOT, DELAI_JEU As Integer
    Public PORT, SCREEN_SELECT As String

    Public ID_TICKET_GOLG_send, ID_TICKET_SILVER_send, ID_TICKET_IRON_send, ID_TICKET_CAGNOTE_send, ID_TICKET_MSTERIOUS_send, ID_TICKET_MEGA_send As String()
    Public ID_TICKET_GOLG, ID_TICKET_SILVER, ID_TICKET_IRON, ID_TICKET_CAGNOTE, ID_TICKET_MSTERIOUS, ID_TICKET_MEGA As String()
    Public list_id_tick As New List(Of String)

    Public timesent As Integer = 0

    Public Sub initialiseSetting()
        Dim oDataSet, seting() As String

        oDataSet = Ocaisse.GetSetting
        seting = oDataSet.Split(CChar("/"))

        If seting.Count > 2 Then

            MONTH_BOOST_LIMIT = seting(0)
            MAX_BOOST_LIMIT = seting(1)
            DIFICULT_LEVEL = seting(2)
            DELAI_AV_GAINS = seting(3)
            PORT = seting(4)
            GOLD_POT = seting(5)
            SILVER_POT = seting(6)
            IRON_POT = seting(7)
            CAGNOTE_POT = seting(8)
            MSTERIOUS_POT = seting(9)
            MEGA_POT = seting(10)
            ID_CAGNOTE = seting(11)
            ID_GOLD = seting(12)
            ID_SILVER = seting(13)
            ID_IRON = seting(14)
            ID_MSTERIOUS = seting(15)
            ID_MEGA = seting(16)
            NB_SHARER = seting(18)
            MAX_GAIIN_SILVER = seting(19)
            MIN_GAIIN_SILVER = seting(20)
            MAX_GAIN_IRON = seting(21)
            MIN_GAIN_IRON = seting(22)
            MIN_LOTO_PLASMA = seting(23)
            MIN_PARI_JACKPOT = seting(24)
            DELAI_JEU = seting(25)

        End If


        SCREEN_SELECT = My.Settings.SCREEN_SELECT

    End Sub
#End Region



    Sub Combo(ByVal ComboI As ComboBox, ByVal Req As String)
        Dim con As MySqlConnection = New MySqlConnection(source)
        Dim cmd As New MySqlCommand(Req, con)
        Dim dr As MySqlDataReader
        con.Open()
        dr = cmd.ExecuteReader
        While dr.Read
            ComboI.Items.Add(dr(0))
        End While
        dr.Close()
        con.Close()
    End Sub
    Public Function GetRandomDistinct(ByVal tailleVoulue As Integer, ByVal cpt As Integer) As Integer()
        Dim nombres As New List(Of Integer)()
        While nombres.Count <> tailleVoulue
            While nombres.Count <> tailleVoulue
                Dim rand As New Random()

                nombres.Add(rand.Next(0, cpt))
            End While
            nombres = nombres.Distinct().ToList()
        End While
        Return nombres.ToArray()
    End Function

    Public Sub select_another_lot(ByVal id_pv As String, ByVal id_jack As String, Optional IsSilver As Boolean = False)

        Dim oDataSet As DataSet
        Dim q() As Integer
        Dim aqt As Integer
        Dim cpt As Integer

        Using ojacpot As New BL_Lotto.BlJackot(source)
            Try
                'todo a revoir cette partie
                oDataSet = ojacpot.GetListlot(My.Settings.ID_PV, id_jack, True)
                cpt = oDataSet.Tables("jackpot").Rows.Count
                aqt = 1
                q = GetRandomDistinct(aqt, cpt)

                'If Not IsSilver Then

                'MsgBox(" avant d'entrer" & ListWinTemp.Length)
                If (oDataSet.Tables("jackpot").Rows.Count > 0) Then
                    ojacpot.Maj_mon_Jackpot(id_pv, id_jack, oDataSet.Tables("jackpot").Rows(q(0)).Item("id_lot").ToString)
                End If
                'Else

                '    oDataSet = ojacpot.GetListlot(id_pv, id_jack, False)
                '    'MsgBox(" avant d'entrer" & ListWinTemp.Length)
                '    If (oDataSet.Tables("jackpot").Rows.Count > 0) Then

                '        'ojacpot.Maj_mon_Jackpot(id_pv, id_jack, oDataSet.Tables("jackpot").Rows(q(0)).Item("id_lot").ToString)

                '        Dim rand As New Random()
                '            Dim nv_jack_sil As Integer

                '        nv_jack_sil = rand.Next(DELAI_JEU, MAX_GAIIN_SILVER)
                '        'ojacpot.Maj_mon_Jackpot(id_jack, 0, nv_jack_sil, 0, "Cagnotte variable")
                '        ojacpot.Maj_mon_Lot(oDataSet.Tables("jackpot").Rows(q(0)).Item("id_lot").ToString, nv_jack_sil, 0, "Cagnotte variable")

                '    End If
                'End If


            Catch ex As Exception

            End Try
        End Using

    End Sub

    Public Function Hostname2IP(ByVal hostname As String) As String
        Dim hostname2 As IPHostEntry = Dns.GetHostEntry(hostname)
        Dim ip As IPAddress() = hostname2.AddressList

        Return ip(1).ToString()
    End Function


    Sub main()

    End Sub


    Public Class Client
        Private _SocketClient As Socket 'Le socket du client
        Private _Pseudo As String 'Le pseudo du client

        'Constructeur
        Sub New(ByVal Sock As Socket)
            _SocketClient = Sock
        End Sub


        Public Function EnvoiRequete(ByVal Message As String) As String
            'envoi de la requete
            Me.EnvoiMessage(Message)

            'reception de la reponse
            Dim Bytes(2024) As Byte
            Dim Recu As Integer
            Try
                Recu = _SocketClient.Receive(Bytes)
            Catch ex As Exception 'Erreur si fermeture du socket pendant la réception
                MessageBox.Show("Connexion perdue, arrêt de la réception des données ...", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Dim reponse As String
            reponse = System.Text.Encoding.UTF8.GetString(Bytes)
            reponse = reponse.Substring(0, Recu)

            Return reponse

        End Function


        Public Function GetTimerAndTirageServer() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "5" & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)


        End Function

        Public Function GetJackpot(ByVal id_j As String, ByVal id_pv As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "6" & "/" & id_j & "/" & id_pv & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function GetJackpotForTick(ByVal cobar As String, ByVal id_pv As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "21" & "/" & cobar & "/" & id_pv

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function GetTirages() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "8" & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function UserCOnnect(ByVal login As String, ByVal pwd As String, ByVal cedential As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "9" & "/" & login & "/" & pwd & "/" & cedential

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function GetSetting() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "14" & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function IsActivated() As Boolean

            Dim Message As String
            Dim reponse As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "12" & "/" & My.Settings.ID_BOOKMAKER

            reponse = Me.EnvoiRequete(Message)

            If reponse = "1" Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function GetLastJack() As String
            'avoir le dernier Jackpot

            Dim Message As String

            Message = "20" & "/" & My.Settings.ID_BOOKMAKER


            Return Me.EnvoiRequete(Message)


        End Function
        Public Function cleExist(ByVal cle As String) As Boolean

            Dim Message, reponse As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "24" & "/" & My.Settings.ID_BOOKMAKER & "/" & cle

            reponse = Me.EnvoiRequete(Message)

            If reponse <> "0" Then
                Return True
            Else
                Return False
            End If

        End Function
        Public Function Getmessage() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "7" & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)

        End Function

        Public Sub EnvoiMessage(ByVal Message As String)
            Dim Mess As Byte() = System.Text.Encoding.UTF8.GetBytes(Message)
            Dim Envoi As Integer = _SocketClient.Send(Mess)
            'Console.WriteLine(Envoi & " bytes envoyés au client " & _Pseudo)
        End Sub
        Public Function ReceiveMessage() As String()
            Dim Bytes(255) As Byte
            Dim Recu As Integer
            Try
                Recu = _SocketClient.Receive(Bytes)
            Catch ex As Exception 'Erreur si fermeture du socket pendant la réception
                MessageBox.Show("Connexion perdue, arrêt de la réception des données ...", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Dim Message As String
            Message = System.Text.Encoding.UTF8.GetString(Bytes)
            Message = Message.Substring(0, Recu)

            Return Message.Split(CChar("/"))
        End Function



    End Class


    Sub Broadcast(ByVal Message As String)

        'Écrit le message dans la console et l'envoie à tous les clients connectés
        Console.WriteLine("BROADCAST : " & Message)
        For Each Cli In ListeClients
            Cli.EnvoiMessage(Message)
        Next

    End Sub


End Module
