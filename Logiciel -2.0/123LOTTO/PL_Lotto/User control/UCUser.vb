﻿Public Class UCUser
    Private oDataSet As Data.DataSet
    Private oUserDataSet As DataSet
    Private oUser As BL_Lotto.BlUser
    Private SelectUser As String

    Private Sub chergerdategrid()
        Using oUser As New BL_Lotto.BlUser(source)
            Try
                oDataSet = oUser.ListUser()

                DgvUser.DataSource = Nothing
                DgvUser.DataSource = oDataSet.Tables("users")

            Catch ExceptionErr As Exception
                MessageBox.Show(ExceptionErr.Message.ToString)
            End Try
        End Using
        If DgvUser.Rows.Count <= 0 Then Exit Sub
        SelectUser = CStr(DgvUser.Rows(DgvUser.CurrentRow.Index).Cells(0).Value)
    End Sub
    
    Private Sub UCUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        chergerdategrid()
    End Sub

    Private Sub AsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AsToolStripMenuItem.Click
        Dim ucuser As New UCformUser
        ucuser.TbCompte.Text = ""
        ucuser.TbConfirm.Text = ""
        ucuser.TbPwd.Text = ""
        Dim f As New FormUser
        f.Controls.Clear()
        f.Controls.Add(ucuser)
        f.ShowDialog()
    End Sub

    Private Sub QfqdfToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QfqdfToolStripMenuItem.Click
        Using oUser As New BL_Lotto.BlUser(source)
            'MsgBox(SelectUser)
            If MsgBox("Etes-vous sûr de vouloir supprimer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                oUser.SupUser(SelectUser)
                chergerdategrid()
            End If
        End Using
    End Sub

    

    Private Sub DgvUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DgvUser.Click
        If DgvUser.Rows.Count > 0 Then

            SelectUser = CStr(DgvUser.Rows(DgvUser.CurrentRow.Index).Cells(0).Value)
            'MsgBox(SelectUser)
        End If

    End Sub

   
    Private Sub DqfqToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DqfqToolStripMenuItem.Click
        Dim f As New FormUser
        Using OcUser As New BL_Lotto.BlUser(source)
            Try
                oDataSet = OcUser.UserParNom(SelectUser.Trim)
                Dim ucuser As New UCformUser

                ucuser.TbIduser.Text = oDataSet.Tables("users").Rows(0).Item("ID_USER").ToString
                ucuser.TbIduser.ReadOnly = True
                ucuser.TbNom.Text = oDataSet.Tables("users").Rows(0).Item("NOM").ToString
                ucuser.TbPrenom.Text = oDataSet.Tables("users").Rows(0).Item("PRENOM").ToString
                ucuser.TbCompte.Text = oDataSet.Tables("users").Rows(0).Item("LOGIN").ToString

                ucuser.TbConfirm.Text = oDataSet.Tables("users").Rows(0).Item("PWD").ToString
                ucuser.TbPwd.Text = oDataSet.Tables("users").Rows(0).Item("PWD").ToString
                ucuser.btEnregistrer.Text = "Modifier"

                f.Controls.Clear()
                f.Controls.Add(ucuser)

            Catch ExceptionErr As Exception
                MessageBox.Show(ExceptionErr.Message.ToString)
            End Try
        End Using
        f.ShowDialog()
        chergerdategrid()
    End Sub

    Private Sub AnnulerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub
End Class
