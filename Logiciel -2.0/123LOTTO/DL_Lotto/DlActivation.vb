﻿Public Class DlActivation
    Inherits DlBase

#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region

#Region " Public Role Functions "
    
    Public Function CleExist(ByVal cle As String) As Boolean
        Try
            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            MyBase.SQL = "SELECT * FROM cle_activation WHERE cle='" + cle + "' "
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

            'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

            MyBase.FillDataSet(LoginDataset, "cle_activation")

            If LoginDataset.Tables("cle_activation").Rows.Count > 0 Then
                Try
                    MyBase.SQL = "UPDATE cle_activation SET etat=0 WHERE id_cle=" & LoginDataset.Tables("cle_activation").Rows(0).Item("id_cle")
                    MyBase.InitializeCommand()
                    CleExist = ExecuteStoredProcedure()
                Catch ExceptionErr As Exception
                    Throw New System.Exception(ExceptionErr.Message, _
                    ExceptionErr.InnerException)
                End Try
            Else
                CleExist = False
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function IsActivated(ByVal id_bookmaker As String) As Boolean
        Try
            Dim getStatPari As DataSet
            Dim Tabdate(), datedebut, datedujour As String
            Dim montant_sousci, montant_actuel As Integer

            getStatPari = New DataSet

            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            MyBase.SQL = "SELECT * FROM cle_activation WHERE etat=1 AND used = 1 AND id_bookmaker=" & id_bookmaker
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

            'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

            MyBase.FillDataSet(LoginDataset, "cle_activation")

            If LoginDataset.Tables("cle_activation").Rows.Count > 0 Then
                'on vas calculer le montant actuel recu
                datedebut = LoginDataset.Tables("cle_activation").Rows(0).Item("date_enreg")

                Tabdate = Date.Now.ToString("dd/MM/yyyy").Split(CChar("/"))
                datedujour = Date.Now.ToString("dd/MM/yyyy") & " 23:59:59"

                MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c,point_vente pv WHERE pv.id_pv=t.id_pv STATUS=1 AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & datedebut & "' AND '" & datedujour & "' AND pv.                                 id_bookmaker=" & id_bookmaker
                MyBase.InitializeCommand()
                'remplir le dataset
                MyBase.FillDataSet(getStatPari, "pari")

                ' on teste si le total de mise n'est pas null
                If (Not getStatPari.Tables("pari").Rows(0).IsNull("TOTALMISE")) Then
                    montant_actuel = CInt(getStatPari.Tables("pari").Rows(0).Item("TOTALMISE"))
                Else
                    montant_actuel = 0
                End If

                montant_sousci = CInt(LoginDataset.Tables("cle_activation").Rows(0).Item("montant"))

                'si le montant actuel vendu est inferieur au montant souscrit
                If (montant_actuel < montant_sousci) Then
                    IsActivated = True
                Else
                    IsActivated = False
                End If
            Else
                IsActivated = False
            End If

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


#End Region

End Class
