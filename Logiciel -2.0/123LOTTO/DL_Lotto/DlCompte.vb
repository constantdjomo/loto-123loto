﻿Public Class DlCompte
    Inherits DlBase

#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region
#Region " Public Role Functions "
    Public Function getStatTirage(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTirage = New DataSet
            MyBase.SQL = "SELECT COUNT(NUM_TIRAGE) AS NB_TIRAGE FROM tirage WHERE DATE_TIRAGE BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTirage, "tirage")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTicket(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTicket = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket WHERE STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTicket, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getDateInscription() As String
        Try
            Dim Tabdatedebut() As String
            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            MyBase.SQL = "SELECT DATE_FORMAT(`date_enreg`,'%Y/%m/%d %H:%i:%s') as date_enreg,`etat`,`used` FROM `cle_activation` WHERE etat=1 AND used = 1"
            MyBase.InitializeCommand()

            MyBase.FillDataSet(LoginDataset, "cle_activation")

            If LoginDataset.Tables("cle_activation").Rows.Count > 0 Then
                'on vas calculer le montant actuel recu
                getDateInscription = LoginDataset.Tables("cle_activation").Rows(0).Item("date_enreg")
            Else
                Tabdatedebut = Date.Now.ToString("dd/MM/yyyy").Split(CChar("/"))
                getDateInscription = Tabdatedebut(2) & "/" & Tabdatedebut(1) & "/" & Tabdatedebut(0) & " 00:00:00"
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function



    Public Function getStatTicketParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTicketParUser = New DataSet
            MyBase.SQL = "Select COUNT(ID_TICKET) As NB_TICKET FROM ticket WHERE STATUS=1 And ID_USER=" & iduser & " And DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTicketParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTicketJack(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTicketJack = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket, gagner_jackp WHERE id_tick = ID_TICKET AND STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTicketJack, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTicketJackParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTicketJackParUser = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket, gagner_jackp g WHERE id_tick = ID_TICKET AND STATUS=1 AND g.ID_USER_S=" & iduser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTicketJackParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTickJackMon(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTickJackMon = New DataSet
            MyBase.SQL = "SELECT SUM(montant) AS TOTAL_MONTANT FROM ticket t,gagner_jackp g WHERE id_tick = ID_TICKET AND STATUS=1 AND g.ID_USER_S <> 0 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTickJackMon, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTickJackMonParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTickJackMonParUser = New DataSet
            MyBase.SQL = "SELECT SUM(montant) AS TOTAL_MONTANT FROM ticket,gagner_jackp WHERE id_tick = ID_TICKET AND STATUS=1 AND ID_USER_S=" & iduser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTickJackMonParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getStatPariSolo(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPariSolo = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE p.`ID_MODE`=1 AND STATUS=1 AND  t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPariSolo, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getStatPariDoublet(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPariDoublet = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE p.`ID_MODE`=2 AND STATUS=1 AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPariDoublet, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getStatPari(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPari = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE STATUS=1 AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPari, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getStatPariParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPariParUser = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE STATUS=1 AND t.ID_USER=" & iduser & "  AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPariParUser, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function



    Public Function getListTicketTirag(ByVal DatDebut As String, ByVal DatFin As String, ByVal tirage As String) As DataSet
        Try
            getListTicketTirag = New DataSet
            MyBase.SQL = "SELECT * FROM ticket WHERE STATUS=1 AND NUM_TIRAGE = " & tirage & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "' ORDER BY RAND()"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListTicketTirag, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getListTicket(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getListTicket = New DataSet
            MyBase.SQL = "SELECT * FROM ticket WHERE STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListTicket, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getListTicketServi(ByVal IdUser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getListTicketServi = New DataSet
            MyBase.SQL = "SELECT * FROM ticket WHERE ID_USER_S = " & IdUser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListTicketServi, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getListTicketParUser(ByVal IdUser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getListTicketParUser = New DataSet
            MyBase.SQL = "SELECT * FROM ticket WHERE STATUS=1 AND  ID_USER = " & IdUser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListTicketParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getGainTicket(ByVal id_ticket) As DataSet
        Try
            getGainTicket = New DataSet
            MyBase.SQL = "SELECT QUOTA*MONTANT_MISE AS GAIN FROM ticket t,mode_jeux m WHERE t.ID_MODE = m.ID_MODE And ID_TICKET = " & id_ticket
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getGainTicket, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


#End Region
    

End Class
