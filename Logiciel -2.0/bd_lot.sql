-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 30 Juillet 2015 à 20:06
-- Version du serveur :  5.6.20
-- Version de PHP :  5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bd_lot`
--

-- --------------------------------------------------------

--
-- Structure de la table `avoir`
--

CREATE TABLE IF NOT EXISTS `avoir` (
  `ID_PARI` int(255) NOT NULL,
  `NUMEROS` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `cle_activation`
--

CREATE TABLE IF NOT EXISTS `cle_activation` (
`id_cle` int(11) NOT NULL,
  `cle` varchar(255) NOT NULL,
  `montant` int(11) NOT NULL,
  `date_enreg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `etat` tinyint(1) NOT NULL DEFAULT '0',
  `used` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `cle_activation`
--

INSERT INTO `cle_activation` (`id_cle`, `cle`, `montant`, `date_enreg`, `etat`, `used`) VALUES
(1, '92qiGV6q', 100000, '2015-07-29 22:43:16', 0, 0),
(2, '6MsYvi33', 100000, '0000-00-00 00:00:00', 0, 0),
(3, '9kW5Eqm8', 100000, '0000-00-00 00:00:00', 0, 0),
(4, 'R5bh23gV', 100000, '0000-00-00 00:00:00', 0, 0),
(5, 'Ps2rrV22', 100000, '0000-00-00 00:00:00', 0, 0),
(6, 'MD8w44sv', 100000, '2015-07-29 22:34:23', 0, 0),
(7, 't6sS8Am5', 300000, '2015-07-29 22:34:23', 0, 0),
(8, 'RW5nne96', 200000, '2015-07-29 22:34:23', 0, 0),
(9, 'xYE2ps42', 200000, '2015-07-29 22:34:23', 0, 0),
(10, 'FU66f7tu', 200000, '2015-07-29 22:34:23', 0, 0),
(11, 'yf2cRA89', 200000, '2015-07-29 22:34:23', 0, 0),
(12, 'TLgwm748', 200000, '2015-07-29 22:34:23', 0, 0),
(13, '8mk8q9HZ', 200000, '2015-07-29 23:25:16', 1, 1),
(14, 'm5zU2Lp7', 200000, '2015-07-29 23:36:49', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `concerner`
--

CREATE TABLE IF NOT EXISTS `concerner` (
  `ID_TICKET` int(255) NOT NULL,
  `ID_PARI` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `contenir`
--

CREATE TABLE IF NOT EXISTS `contenir` (
  `NUM_TIRAGE` int(255) NOT NULL,
  `ID_NUM_TIRER` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `gagner_jackp`
--

CREATE TABLE IF NOT EXISTS `gagner_jackp` (
  `id_tick` int(255) NOT NULL,
  `id_jacp` int(255) NOT NULL,
  `ID_USER_S` int(11) NOT NULL DEFAULT '0',
  `montant` int(255) NOT NULL,
  `date_gagner` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `jackpot`
--

CREATE TABLE IF NOT EXISTS `jackpot` (
`id_jackp` int(11) NOT NULL,
  `nom_jackp` varchar(25) NOT NULL,
  `is_matirial` int(11) NOT NULL,
  `nom_materiel` varchar(255) NOT NULL,
  `montant_jackp` int(255) NOT NULL,
  `etat_jackp` int(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `jackpot`
--

INSERT INTO `jackpot` (`id_jackp`, `nom_jackp`, `is_matirial`, `nom_materiel`, `montant_jackp`, `etat_jackp`) VALUES
(1, 'gold', 0, 'MOTO', 25000, 1176),
(2, 'silver', 0, 'TELEPHONE', 1000, 310);

-- --------------------------------------------------------

--
-- Structure de la table `mode_jeux`
--

CREATE TABLE IF NOT EXISTS `mode_jeux` (
`ID_MODE` int(11) NOT NULL,
  `MODE` varchar(30) NOT NULL,
  `NB_NUM_JOUER` int(11) NOT NULL,
  `QUOTA` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `mode_jeux`
--

INSERT INTO `mode_jeux` (`ID_MODE`, `MODE`, `NB_NUM_JOUER`, `QUOTA`) VALUES
(1, 'solo', 1, 15),
(2, 'doublet', 2, 300),
(3, 'triplet', 3, 1000);

-- --------------------------------------------------------

--
-- Structure de la table `num_jouer`
--

CREATE TABLE IF NOT EXISTS `num_jouer` (
  `NUMEROS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `num_jouer`
--

INSERT INTO `num_jouer` (`NUMEROS`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57),
(58),
(59),
(60),
(61),
(62),
(63),
(64),
(65),
(66),
(67),
(68),
(69),
(70),
(71),
(72),
(73),
(74),
(75),
(76),
(77),
(78),
(79),
(80),
(81),
(82),
(83),
(84),
(85),
(86),
(87),
(88),
(89),
(90);

-- --------------------------------------------------------

--
-- Structure de la table `num_tirer`
--

CREATE TABLE IF NOT EXISTS `num_tirer` (
  `ID_NUM_TIRER` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `num_tirer`
--

INSERT INTO `num_tirer` (`ID_NUM_TIRER`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57),
(58),
(59),
(60),
(61),
(62),
(63),
(64),
(65),
(66),
(67),
(68),
(69),
(70),
(71),
(72),
(73),
(74),
(75),
(76),
(77),
(78),
(79),
(80),
(81),
(82),
(83),
(84),
(85),
(86),
(87),
(88),
(89),
(90);

-- --------------------------------------------------------

--
-- Structure de la table `pari`
--

CREATE TABLE IF NOT EXISTS `pari` (
`ID_PARI` int(11) NOT NULL,
  `ID_MODE` int(11) NOT NULL,
  `MONTANT_MISE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
`ID_TICKET` int(255) NOT NULL,
  `COD_BAR` varchar(255) NOT NULL,
  `NUM_TIRAGE` int(255) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `ID_USER_S` int(11) NOT NULL DEFAULT '0',
  `DATE_ACHAT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `STATUS` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `tirage`
--

CREATE TABLE IF NOT EXISTS `tirage` (
`NUM_TIRAGE` int(255) NOT NULL,
  `DATE_TIRAGE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `STATE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`ID_USER` int(10) NOT NULL,
  `NOM` varchar(255) NOT NULL,
  `PRENOM` varchar(255) NOT NULL,
  `LOGIN` varchar(30) NOT NULL,
  `PWD` varchar(30) NOT NULL,
  `id_user_typ` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`ID_USER`, `NOM`, `PRENOM`, `LOGIN`, `PWD`, `id_user_typ`) VALUES
(0, 'Everybody ', 'Account', '', '', 1),
(1, 'ADMIN', 'ADMIN', '123loto', 'voiture', 1),
(3, 'arnold', 'djomo', 'ascorp', 'ascorp', 2);

-- --------------------------------------------------------

--
-- Structure de la table `users_type`
--

CREATE TABLE IF NOT EXISTS `users_type` (
`id_user_typ` int(11) NOT NULL,
  `user_typ` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `users_type`
--

INSERT INTO `users_type` (`id_user_typ`, `user_typ`) VALUES
(1, 'administration'),
(2, 'caisse');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `avoir`
--
ALTER TABLE `avoir`
 ADD PRIMARY KEY (`ID_PARI`,`NUMEROS`), ADD KEY `NUMEROS` (`NUMEROS`);

--
-- Index pour la table `cle_activation`
--
ALTER TABLE `cle_activation`
 ADD PRIMARY KEY (`id_cle`), ADD UNIQUE KEY `cle` (`cle`);

--
-- Index pour la table `concerner`
--
ALTER TABLE `concerner`
 ADD PRIMARY KEY (`ID_TICKET`,`ID_PARI`), ADD KEY `ID_PARI` (`ID_PARI`);

--
-- Index pour la table `contenir`
--
ALTER TABLE `contenir`
 ADD PRIMARY KEY (`NUM_TIRAGE`,`ID_NUM_TIRER`), ADD KEY `ID_NUM_TIRER` (`ID_NUM_TIRER`);

--
-- Index pour la table `gagner_jackp`
--
ALTER TABLE `gagner_jackp`
 ADD PRIMARY KEY (`id_tick`,`id_jacp`), ADD KEY `id_jacp` (`id_jacp`);

--
-- Index pour la table `jackpot`
--
ALTER TABLE `jackpot`
 ADD PRIMARY KEY (`id_jackp`);

--
-- Index pour la table `mode_jeux`
--
ALTER TABLE `mode_jeux`
 ADD PRIMARY KEY (`ID_MODE`);

--
-- Index pour la table `num_jouer`
--
ALTER TABLE `num_jouer`
 ADD PRIMARY KEY (`NUMEROS`);

--
-- Index pour la table `num_tirer`
--
ALTER TABLE `num_tirer`
 ADD PRIMARY KEY (`ID_NUM_TIRER`);

--
-- Index pour la table `pari`
--
ALTER TABLE `pari`
 ADD PRIMARY KEY (`ID_PARI`);

--
-- Index pour la table `ticket`
--
ALTER TABLE `ticket`
 ADD PRIMARY KEY (`ID_TICKET`);

--
-- Index pour la table `tirage`
--
ALTER TABLE `tirage`
 ADD PRIMARY KEY (`NUM_TIRAGE`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`ID_USER`);

--
-- Index pour la table `users_type`
--
ALTER TABLE `users_type`
 ADD PRIMARY KEY (`id_user_typ`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `cle_activation`
--
ALTER TABLE `cle_activation`
MODIFY `id_cle` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `jackpot`
--
ALTER TABLE `jackpot`
MODIFY `id_jackp` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `mode_jeux`
--
ALTER TABLE `mode_jeux`
MODIFY `ID_MODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `pari`
--
ALTER TABLE `pari`
MODIFY `ID_PARI` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `ticket`
--
ALTER TABLE `ticket`
MODIFY `ID_TICKET` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tirage`
--
ALTER TABLE `tirage`
MODIFY `NUM_TIRAGE` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
MODIFY `ID_USER` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `users_type`
--
ALTER TABLE `users_type`
MODIFY `id_user_typ` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
