﻿Public Class BlTicket
    Implements IDisposable
    Private oDlTicket As DL_Lotto.DlTickect
    Private disposedValue As Boolean = False        ' Détection des appels rédondants

    ' IDisposable
#Region "Constructeur et Destructeur"
    Public Sub New(ByVal Constring As String)
        oDlTicket = New DL_Lotto.DlTickect(Constring)
    End Sub
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: Libérer les ressources non gérées lors d'appel expplicite
            End If
            oDlTicket.Dispose()
            oDlTicket = Nothing
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region
#Region " Public Ticket Functions "
    Public Function ListMode() As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            ListMode = oDlTicket.ListMode
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getListJackParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getListJackParUser = oDlTicket.getListJackParUser(iduser, DatDebut, DatFin)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function List_Ticket() As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            List_Ticket = oDLTicket.List_Ticket
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ajouterNumJouer(ByVal num As String, ByVal tick As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlTicket.ajouterNumJouer(num, tick)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ValideTicket(ByVal idticket As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlTicket.ValideTicket(idticket)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ServirTicket(ByVal idticket As String, ByVal user As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlTicket.ServirTicket(idticket, user)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ServirTicketJack(ByVal idticket As String, ByVal user As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlTicket.ServirTicketJack(idticket, user)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function AjouterTicket(ByVal Ticket As DataSet, ByVal tab As String(,), ByVal nbparie As Integer, ByRef idticket As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlTicket.AjouterTicket(Ticket, tab, nbparie, idticket)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function AjouterParie(ByVal ID_TICKET_P As String, ByVal ID_MODE As String, ByVal MONTANT_MISE As String, ByVal Tab_Num() As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlTicket.AjouterParie(ID_TICKET_P, ID_MODE, MONTANT_MISE, Tab_Num)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function



    Public Function GetLastTirageNum() As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetLastTirageNum = oDlTicket.GetLastTirageNum
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetQuota(ByVal id_mode As String) As Integer
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetQuota = oDlTicket.GetQuota(id_mode)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetLastTickectNum() As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetLastTickectNum = oDlTicket.GetLastTickectNum
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetLastPariNum() As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetLastPariNum = oDlTicket.GetLastPariNum
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function TicketParId(ByVal id_Ticket As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            TicketParId = oDlTicket.TicketParId(id_Ticket)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function TicketParCodebar(ByVal codebar As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            TicketParCodebar = oDlTicket.TicketParCodebar(codebar)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function TicketNonPerimParId(ByVal id_Ticket As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            TicketNonPerimParId = oDlTicket.TicketNonPerimParId(id_Ticket)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetTypeMode(ByVal idMode As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetTypeMode = oDlTicket.GetTypeMode(idMode)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function


    Public Function GetMode(ByVal idMode As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetMode = oDlTicket.GetMode(idMode)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getTirage(ByVal num_tirage As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getTirage = oDlTicket.getTirage(num_tirage)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetPari(ByVal id_ticket As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetPari = oDlTicket.GetPari(id_ticket)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getNumJouer(ByVal ID_PARI As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getNumJouer = oDlTicket.getNumJouer(ID_PARI)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function verifWinner(ByVal id_Ticket As String, ByVal tirage As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlTicket.verifWinner(id_Ticket, tirage)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function NvDSTickect() As DataSet
        Try
            'instancie un nouveau objet DataSet 
            NvDSTickect = New DataSet

            'crée un objet Datatable 
            Dim objDataTable As DataTable = NvDSTickect.Tables.Add("ticket")

            'crée un objet DataColumn 
            Dim objDataColumn As DataColumn

            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("COD_BAR", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = False
            objDataColumn.MaxLength = 255

            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)

            objDataColumn = New DataColumn("id_pv",
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = False
            objDataColumn.MaxLength = 255

            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)


            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("NUM_TIRAGE", _
                Type.GetType("System.String"))
            objDataColumn.MaxLength = 255
            'Add the column to the table
            objDataTable.Columns.Add(objDataColumn)


            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("ID_USER", _
                Type.GetType("System.String"))
            objDataColumn.MaxLength = 255
            'Add the column to the table
            objDataTable.Columns.Add(objDataColumn)




        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function


#End Region
End Class
