﻿Imports MySql
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports MySql.Data.Types
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Windows.Forms

Module lotosettings
    Public TirageLancer As Integer = 0
    Public timesent As Integer = 0
    Public ProchainTir As Integer = 0
    Public montant_en_caisse As Integer = 0
    Public Ocaisse As Client

    Dim Oactivation As BL_Lotto.BlActivation
    'Public CONNECTED_USERID As String = ""
    Public CONNECTED_USERID As String
    Public CONNECTED_USERNAME As String

    'Public SERVEUR As String = "serveur_loto"
    Public SERVEUR As String

    'Public BD As String = "bd_lot"
    Public BD As String

    'Public ID As String = My.Settings.USER_BD
    Public ID As String

    'Public PWD As String = My.Settings.PWD_BD
    Public PWD As String

    'Public source As String = "Server=" & My.Settings.HOST & ";Uid=" & ID & ";Pwd=" & PWD & ";persist security info=True;database=" & BD
    Public source As String

    Public MonSocketClient, MonSocketCaisse As Socket

    Public NB_MODE As Integer
    'Public MODELOTO(3, 3) As String
    Public MODELOTO As New Dictionary(Of String, String)
    Sub Combo(ByVal ComboI As ComboBox, ByVal Req As String)
        Dim con As MySqlConnection = New MySqlConnection(source)
        Dim cmd As New MySqlCommand(Req, con)
        Dim dr As MySqlDataReader
        con.Open()
        dr = cmd.ExecuteReader
        While dr.Read
            ComboI.Items.Add(dr(0))
        End While
        dr.Close()
        con.Close()
    End Sub
    Sub initialisation()
        Dim omode, ttmode() As String

        Try
            omode = Ocaisse.getallmode()
            ttmode = omode.Split(CChar("/"))

            NB_MODE = ttmode.Count
            If (ttmode.Count > 0) Then
                Dim modegese() As String
                For h As Integer = 0 To ttmode.Count - 1
                    modegese = ttmode(h).Split(CChar("#"))
                    MODELOTO.Add(modegese(0), modegese(1))
                Next
            End If

        Catch ex As Exception
            MessageBox.Show("Probleme rencontrer lors du demarage du jeu", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub
    Public Function Hostname2IP(ByVal hostname As String) As String
        Dim hostname2 As IPHostEntry = Dns.GetHostEntry(hostname)
        Dim ip As IPAddress() = hostname2.AddressList

        Return ip(1).ToString()
    End Function

    Public Class Client
        Private _SocketClient As Socket 'Le socket du client
        Private _Pseudo As String 'Le pseudo du client

        'Constructeur
        Sub New(ByVal Sock As Socket)
            _SocketClient = Sock
        End Sub


        Public Function EnvoiRequete(ByVal Message As String) As String
            'envoi de la requete
            Me.EnvoiMessage(Message)

            'reception de la reponse
            Dim Bytes(255) As Byte
            Dim Recu As Integer
            Try
                Recu = _SocketClient.Receive(Bytes)
            Catch ex As Exception 'Erreur si fermeture du socket pendant la réception
                MessageBox.Show("Connexion perdue, arrêt de la réception des données ...", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Dim reponse As String
            reponse = System.Text.Encoding.UTF8.GetString(Bytes)
            reponse = reponse.Substring(0, Recu)

            Return reponse

        End Function
        Public Function AjouterTicket(ByVal tab As String(,), ByVal nbparie As Integer, ByVal chance As Integer) As String
            Dim Message As String
            Dim pari As String = ""
            For g As Integer = 0 To nbparie - 1

                If g = 0 Then
                    pari = tab(g, 0) & "#" & tab(g, 1) & "#" & tab(g, 2)
                Else
                    pari = pari & "*" & tab(g, 0) & "#" & tab(g, 1) & "#" & tab(g, 2)
                End If
            Next


            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "1/" & My.Settings.CODE_SALE & "/" & CONNECTED_USERID & "/" & nbparie & "/" & pari & "/" & chance

            Return Me.EnvoiRequete(Message)
        End Function

        Public Function UserCOnnect(ByVal login As String, ByVal pwd As String, ByVal credential As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "9" & "/" & login & "/" & pwd & "/" & credential

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function UserComptbility(ByVal userid As String, ByVal datdebut As String, ByVal datfin As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "15" & "/" & userid & "/" & datdebut & "/" & datfin & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function getallmode() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "10"

            Return Me.EnvoiRequete(Message)

        End Function
        Public Function getLastTirage() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "11"

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function isActivated() As Boolean

            Dim Message As String
            Dim reponse As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "12" & "/" & My.Settings.ID_BOOKMAKER

            reponse = Me.EnvoiRequete(Message)

            If (reponse <> "0") Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function can(ByVal user_id As String) As Boolean

            Dim Message, rep As String


            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "13" & "/" & user_id

            rep = Me.EnvoiRequete(Message)

            If (rep <> "0") Then
                Return True
            Else
                Return False
            End If

        End Function

        Public Function checkTicket(ByVal codebar As String, ByVal caissier As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "2/" & codebar & "/" & caissier

            Return Me.EnvoiRequete(Message)


        End Function

        Public Function checkTicketjacpot(ByVal codebar As String, ByVal caissier As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "4/" & codebar & "/" & caissier

            Return Me.EnvoiRequete(Message)


        End Function

        Public Function creditDebitPlayer(ByVal id_player As String, ByVal caissier As String, ByVal montant As String) As Boolean

            Dim Message, rep As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "23/" & id_player & "/" & caissier & "/" & montant

            rep = Me.EnvoiRequete(Message)

            If (rep <> "0") Then
                Return True
            Else
                Return False
            End If
        End Function



        Public Function cleExist(ByVal cle As String) As Boolean

            Dim Message, reponse As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "24" & "/" & My.Settings.ID_BOOKMAKER & "/" & cle

            reponse = Me.EnvoiRequete(Message)

            If reponse <> "0" Then
                Return True
            Else
                Return False
            End If

        End Function



        Public Function GetTimerAndTirage() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "3" & "/" & CONNECTED_USERID & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)


        End Function

        Public Function getTicketParCodeBarre(ByVal code_bare As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "25/" & code_bare

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function getTirage(ByVal num_tirage As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "26/" & num_tirage & "/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function getPari(ByVal id_ticket As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "27/" & id_ticket

            Return Me.EnvoiRequete(Message)

        End Function

        Public Function verifWinner(ByVal id_pari As String, ByVal id_tirage As String) As Boolean

            Dim Message As String
            Dim reponse As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "28/" & My.Settings.ID_BOOKMAKER & "/" & id_pari & "/" & id_tirage

            reponse = Me.EnvoiRequete(Message)

            If reponse = "1" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function getListJackParUser(ByVal id_user As String, ByVal datedeb As String, ByVal datef As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "29/" & id_user & "/" & datedeb & "/" & datef

            Return Me.EnvoiRequete(Message)
        End Function

        Public Function listUsers() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "30/" & My.Settings.ID_BOOKMAKER

            Return Me.EnvoiRequete(Message)
        End Function

        Public Function getcreditresiduel() As Integer

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "31/" & My.Settings.ID_BOOKMAKER

            Return CInt(Me.EnvoiRequete(Message))
        End Function

        Public Function listPlayers() As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "32/" & My.Settings.CODE_SALE

            Return Me.EnvoiRequete(Message)
        End Function


        Public Function getTcketParID(ByVal id_ticket As String) As String

            Dim Message As String

            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
            Message = "34/" & id_ticket

            Return Me.EnvoiRequete(Message)
        End Function











        Public Sub EnvoiMessage(ByVal Message As String)
            Dim Mess As Byte() = System.Text.Encoding.UTF8.GetBytes(Message)
            Dim Envoi As Integer = _SocketClient.Send(Mess)
            'Console.WriteLine(Envoi & " bytes envoyés au client " & _Pseudo)
        End Sub
        Public Function ReceiveMessage() As String()
            Dim Bytes(255) As Byte
            Dim Recu As Integer
            Try
                Recu = _SocketClient.Receive(Bytes)
            Catch ex As Exception 'Erreur si fermeture du socket pendant la réception
                MessageBox.Show("Connexion perdue, arrêt de la réception des données ...", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Dim Message As String
            Message = System.Text.Encoding.UTF8.GetString(Bytes)
            Message = Message.Substring(0, Recu)

            Return Message.Split(CChar("/"))
        End Function



    End Class

    Sub main()

    End Sub


End Module
