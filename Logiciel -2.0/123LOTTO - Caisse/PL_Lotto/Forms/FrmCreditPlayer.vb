﻿Imports System.Reflection.Emit

Public Class FrmCreditPlayer

    Public MiseTicketMin As Integer = 100
    Dim dsPlayers As DataSet

    Private Sub Button4_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim temp As Integer
        temp = CInt(Tb_credit.Text)

        temp = temp + MiseTicketMin

        Tb_credit.Text = temp.ToString
    End Sub

    Private Sub FrmCreditPlayer_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim usersStr, usersTab() As String
        usersStr = Ocaisse.listPlayers()
        usersTab = usersStr.Split(CChar("/"))

        Dim comboSource As New Dictionary(Of String, String)()
        comboSource.Add("0", "-Choisir le compte-")
        For i As Integer = 0 To usersTab.Count - 1
            Dim infoUsers As String() = usersTab(i).Split(CChar("#"))
            comboSource.Add(infoUsers(0), infoUsers(1))
        Next
        cbPlayer.DataSource = New BindingSource(comboSource, Nothing)
        cbPlayer.DisplayMember = "Value"
        cbPlayer.ValueMember = "Key"

    End Sub

    Private Sub Button4_Click_1(sender As Object, e As EventArgs) Handles Button4.Click
        Dim temp As Integer
        temp = CInt(Tb_credit.Text)

        temp = temp - MiseTicketMin

        Tb_credit.Text = temp.ToString
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim temp As Integer
        temp = CInt(Tb_credit.Text)

        temp = temp + CInt(Button5.Text)

        Tb_credit.Text = temp.ToString
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim temp As Integer
        temp = CInt(Tb_credit.Text)

        temp = temp - CInt(Button5.Text)

        Tb_credit.Text = temp.ToString
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If cbPlayer.SelectedValue.ToString <> "0" And cbPlayer.SelectedValue.ToString <> "" And IsNumeric(Tb_credit.Text) And CInt(Tb_credit.Text) <> 0 Then

            Dim text_operation As String = If(CInt(Tb_credit.Text) > 0, "credite", "debite")

            If Ocaisse.creditDebitPlayer(cbPlayer.SelectedValue.ToString, CONNECTED_USERID, Tb_credit.Text) Then

                MessageBox.Show("Player: " & cbPlayer.SelectedText & " a ete " & text_operation & " avec succes de " & Tb_credit.Text & " Fcfa.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Operation echoue !!", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            Me.Close()

        End If



    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class