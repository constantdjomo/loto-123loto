﻿Imports System.ComponentModel
Imports System.Text
Imports System.Drawing.Drawing2D
Imports Spire.Barcode

Imports System.Drawing.Printing
'Imports com.epson.pos.driver
Imports System.Management
Imports System.IO
Imports System.Reflection.Emit

Public Class FrmVerifTicket

    Private oTicket As BL_Lotto.BlTicket
    Dim tbOtick() As String
    Private ID_TICKET, COD_SAL As String

    ' Constant variable holding the Printer name.
    Private PRINTER_NAME As String = My.Settings.PRINTER_NAME
    Private ticket As Integer = 0
    ' Variables/Objects.
    'Dim m_objAPI As New StatusAPI
    Private WithEvents pdPrint As PrintDocument
    Dim isFinish As Boolean
    Dim cancelErr As Boolean
    Dim isTimeout As Boolean
    'Dim printStatus As ASB

    Public settings As BarcodeSettings
    Private DSticket As New DataSet


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim oDsNumJou As DataSet
        Dim rep, pari_num, NUM_TIRAGE, DATE_ACHAT, ID_USER_S, bookmaker_id As String
        Dim ticketTab, tirageTab, dateTab, OsPariTab As String()
        Dim Gains As Integer = 0
        Dim Gagner As Boolean = False
        'tbOtick = TbIdticket.Text.Split(CChar("-"))
        'tbOtick = TbIdticket.Text.Split(CChar("/"))
        Gains = 0
        COD_SAL = TbIdticket.Text.Substring(8)
        'ID_TICKET = tbOtick(2)
        'ODsTicket = oTicket.TicketParCodebar(TbIdticket.Text)


        'COD_SAL = tbOtick(0)

        If COD_SAL <> CInt(My.Settings.CODE_SALE).ToString("0000") Then
            MessageBox.Show(" Ce Ticket Ne Provient Pas De Cette Salle ", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        rep = Ocaisse.getTicketParCodeBarre(TbIdticket.Text)

        ticketTab = rep.Split(CChar("/"))

        If (ticketTab.Count > 2) Then
            'ODsTicket1 = oTicket.TicketNonPerimParId(tbOtick(1))
            ID_TICKET = ticketTab(0)
            NUM_TIRAGE = ticketTab(1)
            dateTab = ticketTab(2).Split(CChar("#"))
            DATE_ACHAT = dateTab(0) & "/" & dateTab(1) & "/" & dateTab(2)
            ID_USER_S = ticketTab(3)
            bookmaker_id = ticketTab(4)

            tirageTab = Ocaisse.getTirage(NUM_TIRAGE).Split(CChar("/"))

            If (tirageTab.Count > 0) Then
                Gb_tirage.Text = "Tirage No : " & NUM_TIRAGE

                num_tirage1.Text = tirageTab(0).ToString

                num_tirage2.Text = tirageTab(1).ToString

                num_tirage3.Text = tirageTab(2).ToString

                num_tirage4.Text = tirageTab(3).ToString

                num_tirage5.Text = tirageTab(4).ToString


                TbNumTir.Text = NUM_TIRAGE

                Tb_date_achat.Text = DATE_ACHAT


                OsPariTab = Ocaisse.getPari(ID_TICKET).Split(CChar("/"))

                If (OsPariTab.Count > 0) Then
                    With Dgrid_TicketVerif
                        'mode,numero parier, montant mise,Quota,Gains probable
                        'On met 5 colonnes
                        .Columns.Clear()
                        .ColumnCount = 5

                        .AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders
                        .ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single
                        .CellBorderStyle = DataGridViewCellBorderStyle.Single
                        .RowHeadersVisible = False

                        'On donne le nom des colonnes
                        .Columns(0).Name = "Mode"
                        .Columns(1).Name = "Numero Joue"
                        .Columns(2).Name = "Montant de Mise"
                        .Columns(3).Name = "Quota"
                        .Columns(4).Name = "Gains probable"
                        .Columns(4).DefaultCellStyle.Font = New Font(Me.Dgrid_TicketVerif.DefaultCellStyle.Font, FontStyle.Italic)
                        .SelectionMode = DataGridViewSelectionMode.FullRowSelect

                        .Columns(1).DefaultCellStyle.Font = New Font(Me.Dgrid_TicketVerif.DefaultCellStyle.Font, FontStyle.Italic)
                        .SelectionMode = DataGridViewSelectionMode.FullRowSelect

                        .RowsDefaultCellStyle.BackColor = Drawing.Color.White
                        .AlternatingRowsDefaultCellStyle.BackColor = Drawing.Color.AliceBlue

                        .MultiSelect = False



                        For i As Integer = 0 To OsPariTab.Count - 1
                            Dim pariTab As String()
                            ' MODE, pari_numbers, MONTANT_MISE, QUOTA, gains
                            Dim mode, pari_numbers, id_pari As String
                            Dim montant_mise, quota As Integer

                            pariTab = OsPariTab(i).Split(CChar("#"))
                            mode = pariTab(0)
                            pari_numbers = pariTab(1)
                            montant_mise = CInt(pariTab(2))
                            quota = CInt(pariTab(3))
                            id_pari = pariTab(4)
                            Dim row As String() = {mode, pari_numbers, montant_mise.ToString, quota.ToString, (montant_mise * quota).ToString}
                            .Rows.Add(row)

                            Dim winPari As Boolean = Ocaisse.verifWinner(id_pari, NUM_TIRAGE)
                            If winPari Then
                                Gains += montant_mise * quota
                            End If


                            Gagner = Gagner Or winPari

                        Next


                    End With

                    TbCagnotte.Text = "Cagnotte : " & Gains & " FCFA"
                    If (Gagner) Then
                        PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_ok1 : PbTickGagnant.Refresh()
                        'PbTickGagnant.ImageLocation = "button_ok.png"
                        MessageBox.Show("felicitation ce ticket est gagnant", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)


                        If (0 <> CInt(ID_USER_S)) Then
                            LbelCaisse.Visible = True
                            LbelCaisse.Text = "Servi Par la Caisse :" & ID_USER_S.ToString
                            BtCheck.Enabled = False
                        Else
                            LbelCaisse.Visible = False
                            BtCheck.Enabled = True
                        End If
                    Else
                        MessageBox.Show("le ticket n'est pas gagnant Desole", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        'PbTickGagnant.ImageLocation = "button_cancel.png"
                        PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_cancel1 : PbTickGagnant.Refresh()
                        LbelCaisse.Visible = False
                        BtCheck.Enabled = False
                    End If


                End If
            Else
                MessageBox.Show(" le tirage du ticket n'a pas encore eu lieu .", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
            TbIdticket.Text = ""


        Else
            MessageBox.Show(" Ticket introuvable.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
    End Sub


    Private Sub BtCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtCheck.Click

        'Using oTicket As New BL_Lotto.BlTicket(source)

        '    ' Associate the created callback function to the event handler of StatusAPI.
        '    AddHandler m_objAPI.StatusCallback, AddressOf StatusMonitoring


        '    If Not m_objAPI.IsValid Then
        '        ' Open a printe-r status monitor for the selected printer.
        '        If m_objAPI.OpenMonPrinter(OpenType.TYPE_PRINTER, PRINTER_NAME) <> ErrorCode.SUCCESS Then
        '            MessageBox.Show("Failed to open printer status monitor.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            Exit Sub
        '        End If
        '    End If

        '    isFinish = False
        '    cancelErr = False
        '    isTimeout = False

        '    If (oTicket.ServirTicket(ID_TICKET, CONNECTED_USERID)) Then
        '        'impression du ticket

        '        Try
        '            ' Open a printer status monitor for the selected printer.
        '            If m_objAPI.OpenMonPrinter(OpenType.TYPE_PRINTER, PRINTER_NAME) = ErrorCode.SUCCESS Then

        '                isFinish = False
        '                cancelErr = False

        '                ' Set the callback function that will monitor printer status.
        '                If m_objAPI.SetStatusBack() = ErrorCode.SUCCESS Then

        '                    pdPrint = New PrintDocument

        '                    ' Change the printer to the indicated printer.
        '                    pdPrint.PrinterSettings.PrinterName = PRINTER_NAME

        '                    If pdPrint.PrinterSettings.IsValid Then

        '                        pdPrint.DocumentName = "Ticket " & ticket
        '                        ' Start printing
        '                        pdPrint.Print()

        '                        ' Wait until callback function will say that the task is done.
        '                        ' When done, end the monitoring of printer status.
        '                        Dim iStartTime = DateTime.Now.Ticks / 100000
        '                        ' Wait until callback function will say that the task is done.
        '                        ' When done, end the monitoring of printer status.
        '                        Do
        '                            If isFinish Then
        '                                ' End the monitoring of printer status.
        '                                m_objAPI.CancelStatusBack()
        '                            End If
        '                            If iStartTime + 150000 < DateTime.Now.Ticks / 100000 Then
        '                                isFinish = True
        '                                isTimeout = True
        '                            End If
        '                        Loop While Not isFinish

        '                        If isTimeout Then
        '                            ForceResetPrinter()
        '                        Else
        '                            ' Display the status/error message.
        '                            DisplayStatusMessage()

        '                            ' If roll paper end is detected, cancel the print job
        '                            If (printStatus And ASB.ASB_PAPER_END) = ASB.ASB_PAPER_END Then
        '                                CancelPrintJob()
        '                            End If

        '                            ' If an error occurred, restore the recoverable error.
        '                            If cancelErr Then
        '                                m_objAPI.CancelError()
        '                            End If

        '                        End If

        '                    Else
        '                        MessageBox.Show("Printer is not available.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '                    End If

        '                Else
        '                    MessageBox.Show("Failed to set callback function.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '                End If

        '                ' Always close the Status Monitor after using the Status API.
        '                If Not m_objAPI.CloseMonPrinter = ErrorCode.SUCCESS Then
        '                    MessageBox.Show("Failed to close printer status monitor.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '                End If

        '            Else
        '                MessageBox.Show("Failed to open printer status monitor.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            End If

        '        Catch ex As Exception
        '            MessageBox.Show("Failed to open StatusAPI.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '        End Try

        '        ticket += 1




        '    Else

        '    End If
        'End Using
    End Sub














    ' The event handler function when pdPrint.Print is called.
    ' This is where the actual printing of sample data to the printer is made.
    Private Sub pdPrint_Print(ByVal sender As System.Object, ByVal e As PrintPageEventArgs) Handles pdPrint.PrintPage
        Dim codbar As String = ""
        Dim x, y, somme, lineOffset As Integer
        somme = 0
        'Dim mystrem As FileStream

        ' Instantiate font objects used in printing.
        Dim printFont As New Font("Microsoft Sans Serif", 18, FontStyle.Regular, GraphicsUnit.Point) 'Substituted to FontA Font



        'setting pour le code bare

        settings = New BarcodeSettings()
        Dim data As String = "20140983-90"
        Dim type As String = "Code128"
        Dim user As String = ""

        'settings.Data2D = data
        'settings.Data = data


        settings.Type = CType(System.Enum.Parse(GetType(BarCodeType), type), BarCodeType)
        settings.X = 0.51

        'Dim fontSize As Short = 8
        'Dim font As String = "SimSun"

        'settings.TextFont = New Font(font, fontSize, FontStyle.Bold)

        Dim barHeight As Short = 15
        settings.BarHeight = barHeight

        settings.ShowTextOnBottom = True

        ' fin setting code bare




        e.Graphics.PageUnit = GraphicsUnit.Point

        ' Draw the bitmap
        x = 30
        y = 0


        e.Graphics.DrawString(" Loto MOTO WS ", printFont, Brushes.Red, x, y)

        'e.Graphics.DrawImage(pbImage.Image, x, y, pbImage.Image.Width - 187, 50)

        ' Print the receipt text
        printFont = New Font("Microsoft Sans Serif", 12, FontStyle.Regular, GraphicsUnit.Point)
        lineOffset = CInt(printFont.GetHeight(e.Graphics))
        x = 10
        y = 10 + lineOffset


        Try
            Using ODsTicket As New BL_Lotto.BlTicket(source)
                DSticket = ODsTicket.TicketParId(ID_TICKET)
                codbar = CStr(DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("COD_BAR")) & "-" & CStr(DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("ID_TICKET"))
                user = DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("ID_USER_S").ToString
            End Using
            e.Graphics.DrawString("Product ID: " & codbar, printFont, Brushes.Black, x, y)

            printFont = New Font("Times New Roman", 10, FontStyle.Regular, GraphicsUnit.Point)
            y += lineOffset


            'e.Graphics.DrawString("Douala,Logpom", printFont, Brushes.Black, x, y)
            'y += lineOffset
            'e.Graphics.DrawString("TEL   99-99-99-99", printFont, Brushes.Black, x, y)
            'y += lineOffset
            e.Graphics.DrawString("Caisse: " & user, printFont, Brushes.Black, x, y)
            lineOffset = CInt(printFont.GetHeight(e.Graphics))
            y = CInt(y + (lineOffset * 1.7))


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


        e.Graphics.DrawString("Numero du Tirage :       " & TbNumTir.Text, printFont, Brushes.Black, x, y)
        y += lineOffset
        e.Graphics.DrawString(Date.Now.ToLongDateString & " " & Date.Now.ToLongTimeString, printFont, Brushes.Black, x, y)
        y = CInt(y + (lineOffset * 1.7))
        e.Graphics.DrawString("$$$ Felicitation $$$", printFont, Brushes.Black, x, y)
        y = CInt(y + (lineOffset * 1.7))



        e.Graphics.DrawString("___________________________________", printFont, Brushes.Black, x, y)
        y = CInt(y + (lineOffset * 1.2))
        y = CInt(y + (lineOffset * 1.7))
        e.Graphics.DrawString("Ticket Valide", printFont, Brushes.Black, x, y)

        somme = CInt(TbCagnotte.Text)

        printFont = New Font("Times New Roman", 13, FontStyle.Bold, GraphicsUnit.Point)
        lineOffset = CInt(printFont.GetHeight(e.Graphics))
        y = CInt(y + (lineOffset * 1.7))
        e.Graphics.DrawString(":   " & somme & " F.CFA", printFont, Brushes.Black, x - 1, y)

        printFont = New Font("Comic Sans MS", 12, FontStyle.Bold, GraphicsUnit.Point)
        lineOffset = CInt(printFont.GetHeight(e.Graphics) - 4)
        y = CInt(y + (lineOffset * 1.8))
        'e.Graphics.DrawString("oui!! c'est vrai!", printFont, Brushes.Red, 61, y)
        'y += lineOffset
        settings.Data2D = codbar
        settings.Data = codbar

        Dim generator As New BarCodeGenerator(settings)
        Dim barcode As Image = generator.GenerateImage()

        'save the barcode as an image
        barcode.Save("barcode.png")

        e.Graphics.DrawImage(Image.FromFile("barcode.png"), x, y, 150, 75)
        'Todo put the image in the ticket

        'e.Graphics.DrawString(codbar, barcodeFont, Brushes.Black, x, y + 5)

        ' Indicate that no more data to print, and the Print Document can now send the print data to the spooler.
        e.HasMorePages = False
    End Sub


    '' The callback function that will monitor printer/printing status.
    'Public Sub StatusMonitoring(ByVal dwStatus As ASB)

    '    If (dwStatus And ASB.ASB_PRINT_SUCCESS) = ASB.ASB_PRINT_SUCCESS Then
    '        printStatus = dwStatus
    '        isFinish = True

    '    ElseIf (dwStatus And ASB.ASB_NO_RESPONSE) = ASB.ASB_NO_RESPONSE Or _
    '            (dwStatus And ASB.ASB_COVER_OPEN) = ASB.ASB_COVER_OPEN Or _
    '            (dwStatus And ASB.ASB_AUTOCUTTER_ERR) = ASB.ASB_AUTOCUTTER_ERR Or _
    '            (dwStatus And ASB.ASB_PAPER_END) = ASB.ASB_PAPER_END Then
    '        printStatus = dwStatus
    '        isFinish = True
    '        cancelErr = True

    '    End If

    'End Sub

    '' Open the cash drawer using the Status API.
    'Private Sub OpenDrawer()

    '    Try
    '        ' Execute drawer operation.
    '        If Not m_objAPI.OpenDrawer(Drawer.EPS_BI_DRAWER_1, Pulse.EPS_BI_PULSE_100) = ErrorCode.SUCCESS Then
    '            MessageBox.Show("Failed to open drawer.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Failed to open drawer.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '    End Try

    'End Sub

    'Private Sub ForceResetPrinter()
    '    ' If both Printing complete and error are not take then use BiForceResetPrinter.
    '    If MessageBox.Show("Anomalous occurrence." & vbLf & "Execute BiForceResetPrinter.", "123... LOTO", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
    '        If m_objAPI.ForceResetPrinter() = ErrorCode.SUCCESS Then
    '            MessageBox.Show("Force reset printer is success.", "123... LOTO")
    '        Else
    '            MessageBox.Show("Failed to force reset printer.", "123... LOTO")
    '        End If
    '    End If
    'End Sub

    ' Executes the canceling of print job.
    Private Sub CancelPrintJob()
        Dim searchPrintJobs As ManagementObjectSearcher
        Dim printJobCollection As ManagementObjectCollection
        Dim printJob As ManagementObject
        Dim isDeleted As Boolean = False

        searchPrintJobs = New ManagementObjectSearcher("SELECT * FROM Win32_PrintJob")

        printJobCollection = searchPrintJobs.Get

        For Each printJob In printJobCollection
            If CBool(System.String.Compare(printJob.Properties("Name").Value.ToString(), PRINTER_NAME)) Then
                printJob.Delete()
                isDeleted = True
                Exit For
            End If
        Next

        If isDeleted Then
            MessageBox.Show("Print job cancelled.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("Failed to cancel print job.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    'Private Sub DisplayStatusMessage()

    '    If (printStatus And ASB.ASB_PRINT_SUCCESS) = ASB.ASB_PRINT_SUCCESS Then
    '        MessageBox.Show("Printing complete.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)

    '    ElseIf (printStatus And ASB.ASB_NO_RESPONSE) = ASB.ASB_NO_RESPONSE Then
    '        MessageBox.Show("No response.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    '    ElseIf (printStatus And ASB.ASB_COVER_OPEN) = ASB.ASB_COVER_OPEN Then
    '        MessageBox.Show("Cover is open.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    '    ElseIf (printStatus And ASB.ASB_AUTOCUTTER_ERR) = ASB.ASB_AUTOCUTTER_ERR Then
    '        MessageBox.Show("Autocutter error occurred.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    '    ElseIf (printStatus And ASB.ASB_PAPER_END) = ASB.ASB_PAPER_END Then
    '        MessageBox.Show("Roll paper end sensor: paper not present.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    '    End If
    'End Sub

    Private Sub FrmVerifTicket_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub Dgrid_TicketVerif_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Dgrid_TicketVerif.CellContentClick

    End Sub
End Class