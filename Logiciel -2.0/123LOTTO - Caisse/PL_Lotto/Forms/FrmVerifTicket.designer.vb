﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmVerifTicket
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.PbTickGagnant = New System.Windows.Forms.PictureBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TbNumTir = New System.Windows.Forms.TextBox()
        Me.Dgrid_TicketVerif = New System.Windows.Forms.DataGridView()
        Me.Tb_date_achat = New System.Windows.Forms.TextBox()
        Me.TbCagnotte = New System.Windows.Forms.Label()
        Me.LbelCaisse = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Gb_tirage = New System.Windows.Forms.GroupBox()
        Me.num_tirage5 = New System.Windows.Forms.Label()
        Me.num_tirage4 = New System.Windows.Forms.Label()
        Me.num_tirage3 = New System.Windows.Forms.Label()
        Me.num_tirage2 = New System.Windows.Forms.Label()
        Me.num_tirage1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.BtCheck = New System.Windows.Forms.Button()
        Me.TbIdticket = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PbTickGagnant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.Dgrid_TicketVerif, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Gb_tirage.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.PbTickGagnant)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 77)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(328, 176)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ticket Gagnant"
        '
        'PbTickGagnant
        '
        Me.PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_cancel1
        Me.PbTickGagnant.Location = New System.Drawing.Point(101, 21)
        Me.PbTickGagnant.Name = "PbTickGagnant"
        Me.PbTickGagnant.Size = New System.Drawing.Size(128, 128)
        Me.PbTickGagnant.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PbTickGagnant.TabIndex = 0
        Me.PbTickGagnant.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TbNumTir)
        Me.GroupBox2.Controls.Add(Me.Dgrid_TicketVerif)
        Me.GroupBox2.Controls.Add(Me.Tb_date_achat)
        Me.GroupBox2.Controls.Add(Me.TbCagnotte)
        Me.GroupBox2.Controls.Add(Me.LbelCaisse)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(346, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(779, 423)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Infos Ticket"
        '
        'TbNumTir
        '
        Me.TbNumTir.Location = New System.Drawing.Point(140, 33)
        Me.TbNumTir.Name = "TbNumTir"
        Me.TbNumTir.ReadOnly = True
        Me.TbNumTir.Size = New System.Drawing.Size(161, 22)
        Me.TbNumTir.TabIndex = 3
        '
        'Dgrid_TicketVerif
        '
        Me.Dgrid_TicketVerif.AllowUserToAddRows = False
        Me.Dgrid_TicketVerif.AllowUserToDeleteRows = False
        Me.Dgrid_TicketVerif.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgrid_TicketVerif.Location = New System.Drawing.Point(39, 146)
        Me.Dgrid_TicketVerif.Name = "Dgrid_TicketVerif"
        Me.Dgrid_TicketVerif.ReadOnly = True
        Me.Dgrid_TicketVerif.Size = New System.Drawing.Size(708, 190)
        Me.Dgrid_TicketVerif.TabIndex = 16
        '
        'Tb_date_achat
        '
        Me.Tb_date_achat.Location = New System.Drawing.Point(585, 35)
        Me.Tb_date_achat.Name = "Tb_date_achat"
        Me.Tb_date_achat.ReadOnly = True
        Me.Tb_date_achat.Size = New System.Drawing.Size(162, 22)
        Me.Tb_date_achat.TabIndex = 3
        '
        'TbCagnotte
        '
        Me.TbCagnotte.AutoSize = True
        Me.TbCagnotte.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbCagnotte.Location = New System.Drawing.Point(271, 368)
        Me.TbCagnotte.Name = "TbCagnotte"
        Me.TbCagnotte.Size = New System.Drawing.Size(209, 29)
        Me.TbCagnotte.TabIndex = 1
        Me.TbCagnotte.Text = "Cagnotte : 0 FCFA"
        '
        'LbelCaisse
        '
        Me.LbelCaisse.AutoSize = True
        Me.LbelCaisse.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbelCaisse.Location = New System.Drawing.Point(404, 96)
        Me.LbelCaisse.Name = "LbelCaisse"
        Me.LbelCaisse.Size = New System.Drawing.Size(148, 20)
        Me.LbelCaisse.TabIndex = 2
        Me.LbelCaisse.Text = "Servi Par la Caisse :"
        Me.LbelCaisse.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(448, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 20)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Date de Jeu :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(35, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tirage No :"
        '
        'Gb_tirage
        '
        Me.Gb_tirage.Controls.Add(Me.num_tirage5)
        Me.Gb_tirage.Controls.Add(Me.num_tirage4)
        Me.Gb_tirage.Controls.Add(Me.num_tirage3)
        Me.Gb_tirage.Controls.Add(Me.num_tirage2)
        Me.Gb_tirage.Controls.Add(Me.num_tirage1)
        Me.Gb_tirage.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Gb_tirage.Location = New System.Drawing.Point(12, 259)
        Me.Gb_tirage.Name = "Gb_tirage"
        Me.Gb_tirage.Size = New System.Drawing.Size(328, 176)
        Me.Gb_tirage.TabIndex = 0
        Me.Gb_tirage.TabStop = False
        Me.Gb_tirage.Text = "Tirage No :"
        '
        'num_tirage5
        '
        Me.num_tirage5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num_tirage5.ForeColor = System.Drawing.Color.White
        Me.num_tirage5.Location = New System.Drawing.Point(250, 86)
        Me.num_tirage5.Name = "num_tirage5"
        Me.num_tirage5.Size = New System.Drawing.Size(44, 38)
        Me.num_tirage5.TabIndex = 0
        Me.num_tirage5.Text = "--"
        Me.num_tirage5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num_tirage4
        '
        Me.num_tirage4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num_tirage4.ForeColor = System.Drawing.Color.White
        Me.num_tirage4.Location = New System.Drawing.Point(190, 86)
        Me.num_tirage4.Name = "num_tirage4"
        Me.num_tirage4.Size = New System.Drawing.Size(44, 38)
        Me.num_tirage4.TabIndex = 0
        Me.num_tirage4.Text = "--"
        Me.num_tirage4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num_tirage3
        '
        Me.num_tirage3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num_tirage3.ForeColor = System.Drawing.Color.White
        Me.num_tirage3.Location = New System.Drawing.Point(130, 86)
        Me.num_tirage3.Name = "num_tirage3"
        Me.num_tirage3.Size = New System.Drawing.Size(44, 38)
        Me.num_tirage3.TabIndex = 0
        Me.num_tirage3.Text = "--"
        Me.num_tirage3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num_tirage2
        '
        Me.num_tirage2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num_tirage2.ForeColor = System.Drawing.Color.White
        Me.num_tirage2.Location = New System.Drawing.Point(70, 86)
        Me.num_tirage2.Name = "num_tirage2"
        Me.num_tirage2.Size = New System.Drawing.Size(44, 38)
        Me.num_tirage2.TabIndex = 0
        Me.num_tirage2.Text = "--"
        Me.num_tirage2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num_tirage1
        '
        Me.num_tirage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num_tirage1.ForeColor = System.Drawing.Color.White
        Me.num_tirage1.Location = New System.Drawing.Point(19, 88)
        Me.num_tirage1.Name = "num_tirage1"
        Me.num_tirage1.Size = New System.Drawing.Size(35, 34)
        Me.num_tirage1.TabIndex = 0
        Me.num_tirage1.Text = "--"
        Me.num_tirage1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(113, 476)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(137, 43)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "OK"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(8, 20)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(80, 20)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "ID Ticket :"
        '
        'BtCheck
        '
        Me.BtCheck.Enabled = False
        Me.BtCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtCheck.Location = New System.Drawing.Point(294, 477)
        Me.BtCheck.Name = "BtCheck"
        Me.BtCheck.Size = New System.Drawing.Size(137, 43)
        Me.BtCheck.TabIndex = 2
        Me.BtCheck.Text = "Valider"
        Me.BtCheck.UseVisualStyleBackColor = True
        '
        'TbIdticket
        '
        Me.TbIdticket.Location = New System.Drawing.Point(138, 20)
        Me.TbIdticket.Name = "TbIdticket"
        Me.TbIdticket.Size = New System.Drawing.Size(161, 20)
        Me.TbIdticket.TabIndex = 1
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Controls.Add(Me.TbIdticket)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(328, 58)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        '
        'FrmVerifTicket
        '
        Me.AcceptButton = Me.Button1
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkSlateGray
        Me.ClientSize = New System.Drawing.Size(1137, 550)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.BtCheck)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Gb_tirage)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmVerifTicket"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Verification de Ticket"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PbTickGagnant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.Dgrid_TicketVerif, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Gb_tirage.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Gb_tirage As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PbTickGagnant As System.Windows.Forms.PictureBox
    Friend WithEvents Tb_date_achat As System.Windows.Forms.TextBox
    Friend WithEvents num_tirage5 As System.Windows.Forms.Label
    Friend WithEvents num_tirage4 As System.Windows.Forms.Label
    Friend WithEvents num_tirage3 As System.Windows.Forms.Label
    Friend WithEvents num_tirage2 As System.Windows.Forms.Label
    Friend WithEvents num_tirage1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Dgrid_TicketVerif As System.Windows.Forms.DataGridView
    Friend WithEvents TbCagnotte As System.Windows.Forms.Label
    Friend WithEvents BtCheck As System.Windows.Forms.Button
    Friend WithEvents LbelCaisse As System.Windows.Forms.Label
    Friend WithEvents TbNumTir As System.Windows.Forms.TextBox
    Friend WithEvents TbIdticket As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
End Class
