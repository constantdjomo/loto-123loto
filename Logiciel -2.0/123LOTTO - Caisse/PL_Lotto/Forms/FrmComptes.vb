﻿Imports System.Globalization
Public Class FrmComptes
    Private oCompte As BL_Lotto.BlCompte
    Private oTicket As BL_Lotto.BlTicket
    Private oUser As BL_Lotto.BlUser

    Dim Tabdatedebut(), Tabdatefin() As String
    Dim datedebut, datefin As String
    Dim delim As Char = CChar("/")
    Dim dsTicket, dsTicketJack, dsTicketAnnul, dsTickJackMon, dsTouticket, dsPari, ODsPari, dsTirage, DsGain As DataSet
    Dim usersStr As String
    Dim usersTab As String()



    Dim id_ticket As String

    Private Sub FrmComptes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Using oUser As New BL_Lotto.BlUser(source)
            'CONNECTED_USERID = "1"

            TbNomUser.Text = CONNECTED_USERNAME
            Label9.Text = Date.Now.ToLongDateString
            ' ID_USER,NOM,PRENOM,LOGIN,PWD as PASSWORD,user_typ ,ut.id_user_typ
            usersStr = Ocaisse.listUsers()
            usersTab = usersStr.Split(CChar("/"))
            Dim comboSource As New Dictionary(Of String, String)()
            For i As Integer = 0 To usersTab.Count - 1
                Dim infoUsers As String() = usersTab(i).Split(CChar("#"))
                comboSource.Add(infoUsers(0), infoUsers(1))
            Next


            Cbuser.DataSource = New BindingSource(comboSource, Nothing)
            Cbuser.DisplayMember = "Value"
            Cbuser.ValueMember = "Key"

        End Using

    End Sub

    Private Sub BtOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtOk.Click

        Dim Gagner As Boolean = False
        Dim Gains As Integer = 0
        Dim TotalmontantJack As Integer = 0
        Dim totaltickannuller As Integer = 0
        Dim creditResiduel As Integer = 0
        Dim rep, compte() As String
        Using oCompte As New BL_Lotto.BlCompte(source)

            If DateTime.Compare(DpDebut.Value, DpFin.Value) <= 0 Then
                Tabdatedebut = DpDebut.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
                datedebut = Tabdatedebut(2) & "#" & Tabdatedebut(1) & "#" & Tabdatedebut(0)
                'datedebut = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
                Tabdatefin = DpFin.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
                datefin = Tabdatefin(2) & "#" & Tabdatefin(1) & "#" & Tabdatefin(0)


                'NbtTicketJack & "/" & NbTicket & "/" & TotalMise & "/" & NbTirage & "/" & nbticketGagnant & "/" & MontantSortie & "/" & totaltickannuller & "/" & TotalCreditDebit

                rep = Ocaisse.UserComptbility(Cbuser.SelectedValue.ToString(), datedebut, datefin)

                compte = rep.Split(CChar("/"))

                If compte.Count > 2 Then
                    LbNbtTicketJack.Text = compte(0)

                    LbNbTicket.Text = compte(1)
                    LbTotalMise.Text = compte(2)
                    LbNbTirage.Text = compte(3)
                    LbNbtTicketGagna.Text = compte(4)
                    LbTotalSortie.Text = compte(5)
                    Lb_Nb_annule.Text = compte(6)
                    LbCreditPlayer.Text = compte(7)
                    'Dim Listjack As String = compte(8)
                    ' ajouter le montant du credit distribuer soit en sortie soit en entre
                    LbTotalMise.Text = (CInt(LbTotalMise.Text) + If(CInt(LbCreditPlayer.Text) > 0, CInt(LbCreditPlayer.Text), 0)).ToString
                    LbTotalSortie.Text = (CInt(LbTotalSortie.Text) + If(CInt(LbCreditPlayer.Text) < 0, CInt(LbCreditPlayer.Text), 0)).ToString

                End If

                creditResiduel = Ocaisse.getcreditresiduel()
                'on gere le credit residuel
                If (creditResiduel = -1) Then
                    Lbcreditesiduel.Text = "Illimité"
                Else
                    Lbcreditesiduel.Text = creditResiduel.ToString
                End If


                If LbTotalMise.Text = "" Then
                    LbTotalMise.Text = 0.ToString
                End If
                If LbTotalSortie.Text = "" Then
                    LbTotalSortie.Text = 0.ToString
                End If

                Dim benef As Integer = CInt(LbTotalMise.Text) - CInt(LbTotalSortie.Text)
                LbBenefice.Text = benef.ToString

            Else
                MessageBox.Show("Attention a la plage de Date.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Using
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

    End Sub

    Private Sub FrmComptes_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        FrmPrincipal.Show()
    End Sub
End Class