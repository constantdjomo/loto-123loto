﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class setting
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TbNameImp = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TbCodeSal = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TbPwd = New System.Windows.Forms.TextBox()
        Me.TBUser = New System.Windows.Forms.TextBox()
        Me.TbHost = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BtValider = New System.Windows.Forms.Button()
        Me.TB_bookmaker_id = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(54, 195)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(177, 36)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Imprimante :"
        '
        'TbNameImp
        '
        Me.TbNameImp.Location = New System.Drawing.Point(332, 193)
        Me.TbNameImp.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.TbNameImp.Name = "TbNameImp"
        Me.TbNameImp.Size = New System.Drawing.Size(440, 31)
        Me.TbNameImp.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TB_bookmaker_id)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.TbCodeSal)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.TbPwd)
        Me.GroupBox1.Controls.Add(Me.TBUser)
        Me.GroupBox1.Controls.Add(Me.TbHost)
        Me.GroupBox1.Controls.Add(Me.TbNameImp)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.BtValider)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 23)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.GroupBox1.Size = New System.Drawing.Size(852, 646)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Parametre"
        '
        'TbCodeSal
        '
        Me.TbCodeSal.Location = New System.Drawing.Point(332, 71)
        Me.TbCodeSal.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.TbCodeSal.MaxLength = 4
        Me.TbCodeSal.Name = "TbCodeSal"
        Me.TbCodeSal.Size = New System.Drawing.Size(216, 31)
        Me.TbCodeSal.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(54, 73)
        Me.Label5.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(154, 36)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Code Salle"
        '
        'TbPwd
        '
        Me.TbPwd.Location = New System.Drawing.Point(332, 456)
        Me.TbPwd.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.TbPwd.Name = "TbPwd"
        Me.TbPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TbPwd.Size = New System.Drawing.Size(440, 31)
        Me.TbPwd.TabIndex = 1
        Me.TbPwd.Visible = False
        '
        'TBUser
        '
        Me.TBUser.Location = New System.Drawing.Point(332, 360)
        Me.TBUser.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.TBUser.Name = "TBUser"
        Me.TBUser.Size = New System.Drawing.Size(440, 31)
        Me.TBUser.TabIndex = 1
        Me.TBUser.Visible = False
        '
        'TbHost
        '
        Me.TbHost.Location = New System.Drawing.Point(332, 263)
        Me.TbHost.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.TbHost.Name = "TbHost"
        Me.TbHost.Size = New System.Drawing.Size(440, 31)
        Me.TbHost.TabIndex = 1
        Me.TbHost.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(54, 458)
        Me.Label4.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(258, 36)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Mot De Passe BD :"
        Me.Label4.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(54, 362)
        Me.Label3.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(216, 36)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Utilisateur BD :"
        Me.Label3.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(54, 265)
        Me.Label2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(246, 36)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Hote Du Serveur :"
        Me.Label2.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(486, 544)
        Me.Button1.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(162, 60)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Fermer"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'BtValider
        '
        Me.BtValider.Location = New System.Drawing.Point(252, 544)
        Me.BtValider.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.BtValider.Name = "BtValider"
        Me.BtValider.Size = New System.Drawing.Size(162, 60)
        Me.BtValider.TabIndex = 2
        Me.BtValider.Text = "Sauver"
        Me.BtValider.UseVisualStyleBackColor = True
        '
        'TB_bookmaker_id
        '
        Me.TB_bookmaker_id.Location = New System.Drawing.Point(332, 127)
        Me.TB_bookmaker_id.Margin = New System.Windows.Forms.Padding(6)
        Me.TB_bookmaker_id.MaxLength = 4
        Me.TB_bookmaker_id.Name = "TB_bookmaker_id"
        Me.TB_bookmaker_id.Size = New System.Drawing.Size(216, 31)
        Me.TB_bookmaker_id.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(54, 129)
        Me.Label6.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(204, 36)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Bookmaker ID"
        '
        'setting
        '
        Me.AcceptButton = Me.BtValider
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkSlateGray
        Me.ClientSize = New System.Drawing.Size(902, 704)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.Name = "setting"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TbNameImp As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtValider As System.Windows.Forms.Button
    Friend WithEvents TbHost As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TbPwd As System.Windows.Forms.TextBox
    Friend WithEvents TBUser As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TbCodeSal As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TB_bookmaker_id As TextBox
    Friend WithEvents Label6 As Label
End Class
