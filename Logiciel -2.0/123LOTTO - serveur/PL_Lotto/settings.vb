﻿Imports System.ComponentModel
Imports System.Text
Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports System.Globalization
Imports System.Data
Imports System.Management
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.InteropServices.WindowsRuntime

'Imports MySql
'Imports MySql.Data
'Imports MySql.Data.MySqlClient
'Imports MySql.Data.Types




Module settings
    Public ListeClients, ListeCaisses As List(Of Client) 'Liste destinée à contenir les clients connectés

    Public TirageLancer As Integer = 0

    Public Tir_No As Integer = 0
    Public lastTir_No As Integer = 0
    Public num_Tiragetosend As Integer = 0

    Public timetosend As String = "0"
    Public animaDemarer As Boolean = False

    Dim Oactivation As BL_Lotto.BlActivation
    Public CONNECTED_USERID As String = ""
    Public SERVEUR As String = "localhost"
    Public BD As String = "bd_loto"
    Public ID As String = "root"
    Public PWD As String = "jlprom2at@"
    Public source As String = "server=" & SERVEUR & ";user id=" & ID & ";Password=" & PWD & ";persist security info=True;database=" & BD
#Region "definition des settings"

    Public list_bookmaker As DataSet

    Public bookmaker_data As Hashtable = New Hashtable()

    'Public DIFICULT_LEVEL, DELAI_AV_GAINS, GOLD_POT, SILVER_POT, CAGNOTE_POT, IRON_POT, MSTERIOUS_POT, MEGA_POT, NB_SHARER, ID_GOLD, ID_SILVER, ID_IRON, ID_CAGNOTE, ID_MSTERIOUS, ID_MEGA, DELAI_JEU, MAX_GAIIN_SILVER, MAX_GAIN_IRON, MIN_LOTO_PLASMA, MIN_PARI_JACKPOT As Integer
    Public SCREEN_SELECT As String


    'id des ticket gagnant du jacpot
    Public bookmaker_id_jack As Hashtable = New Hashtable()
    'Public ID_TICKET_GOLG, ID_TICKET_SILVER, ID_TICKET_IRON, ID_TICKET_CAGNOTE, ID_TICKET_MSTERIOUS, ID_TICKET_MEGA As String()

    Public Sub initialiseSetting()
        Dim oDataSet, dsti As DataSet

        Using oTicketTi As New BL_Lotto.BlTicket(source)
            dsti = oTicketTi.GetLastTirageNum()
            If (dsti.Tables("tirage").Rows.Count > 0) Then
                lastTir_No = CInt(dsti.Tables("tirage").Rows(0).Item("NUM_TIRAGE"))
            Else
                lastTir_No = 0
            End If
        End Using

        Using oUser As New BL_Lotto.BlUser(source)
            list_bookmaker = oUser.getBookMakerlist()
            If (list_bookmaker.Tables("bookmaker").Rows.Count > 0) Then

                For i As Integer = 0 To list_bookmaker.Tables("bookmaker").Rows.Count - 1

                    Dim bookmaker_set As Hashtable = New Hashtable()

                    oDataSet = oUser.GetSetting(list_bookmaker.Tables("bookmaker").Rows(i).Item("bookmaker_id").ToString)
                    If oDataSet.Tables("setting").Rows.Count > 0 Then

                        bookmaker_set.Add("EMERGENCY_STOP", oDataSet.Tables("setting").Rows(0).Item("EMERGENCY_STOP").ToString)

                        bookmaker_set.Add("DEDUCT_JACK_TO_TURN_OVER", oDataSet.Tables("setting").Rows(0).Item("DEDUCT_JACK_TO_TURN_OVER").ToString)

                        bookmaker_set.Add("MONTH_BOOST_LIMIT", oDataSet.Tables("setting").Rows(0).Item("MONTH_BOOST_LIMIT").ToString)

                        bookmaker_set.Add("MAX_BOOST_LIMIT", oDataSet.Tables("setting").Rows(0).Item("MAX_BOOST_LIMIT").ToString)

                        bookmaker_set.Add("DIFICULT_LEVEL", oDataSet.Tables("setting").Rows(0).Item("DIFICULT_LEVEL").ToString)
                        'DIFICULT_LEVEL = oDataSet.Tables("setting").Rows(0).Item("DIFICULT_LEVEL").ToString

                        bookmaker_set.Add("DELAI_AV_GAINS", oDataSet.Tables("setting").Rows(0).Item("DELAI_AV_GAINS").ToString)
                        'DELAI_AV_GAINS = oDataSet.Tables("setting").Rows(0).Item("DELAI_AV_GAINS").ToString

                        bookmaker_set.Add("GOLD_POT", oDataSet.Tables("setting").Rows(0).Item("GOLD_POT").ToString)
                        'GOLD_POT = oDataSet.Tables("setting").Rows(0).Item("GOLD_POT").ToString

                        bookmaker_set.Add("SILVER_POT", oDataSet.Tables("setting").Rows(0).Item("SILVER_POT").ToString)
                        'SILVER_POT = oDataSet.Tables("setting").Rows(0).Item("SILVER_POT").ToString

                        bookmaker_set.Add("CAGNOTE_POT", oDataSet.Tables("setting").Rows(0).Item("CAGNOTE_POT").ToString)
                        'CAGNOTE_POT = oDataSet.Tables("setting").Rows(0).Item("CAGNOTE_POT").ToString

                        bookmaker_set.Add("ID_CAGNOTE", oDataSet.Tables("setting").Rows(0).Item("ID_CAGNOTE").ToString)
                        'ID_CAGNOTE = oDataSet.Tables("setting").Rows(0).Item("ID_CAGNOTE").ToString

                        bookmaker_set.Add("ID_MSTERIOUS", oDataSet.Tables("setting").Rows(0).Item("ID_MSTERIOUS").ToString)
                        'ID_MSTERIOUS = oDataSet.Tables("setting").Rows(0).Item("ID_MSTERIOUS").ToString

                        bookmaker_set.Add("ID_MEGA", oDataSet.Tables("setting").Rows(0).Item("ID_MEGA").ToString)
                        'ID_MEGA = oDataSet.Tables("setting").Rows(0).Item("ID_MEGA").ToString

                        bookmaker_set.Add("NB_SHARER", oDataSet.Tables("setting").Rows(0).Item("NB_SHARER").ToString)
                        'NB_SHARER = oDataSet.Tables("setting").Rows(0).Item("NB_SHARER").ToString

                        bookmaker_set.Add("IRON_POT", oDataSet.Tables("setting").Rows(0).Item("IRON_POT").ToString)
                        'IRON_POT = oDataSet.Tables("setting").Rows(0).Item("IRON_POT").ToString

                        bookmaker_set.Add("MSTERIOUS_POT", oDataSet.Tables("setting").Rows(0).Item("MSTERIOUS_POT").ToString)
                        'MSTERIOUS_POT = oDataSet.Tables("setting").Rows(0).Item("MSTERIOUS_POT").ToString

                        bookmaker_set.Add("MEGA_POT", oDataSet.Tables("setting").Rows(0).Item("MEGA_POT").ToString)
                        'MEGA_POT = oDataSet.Tables("setting").Rows(0).Item("MEGA_POT").ToString

                        bookmaker_set.Add("ID_GOLD", oDataSet.Tables("setting").Rows(0).Item("ID_GOLD").ToString)
                        'ID_GOLD = oDataSet.Tables("setting").Rows(0).Item("ID_GOLD").ToString

                        bookmaker_set.Add("ID_SILVER", oDataSet.Tables("setting").Rows(0).Item("ID_SILVER").ToString)
                        'ID_SILVER = oDataSet.Tables("setting").Rows(0).Item("ID_SILVER").ToString

                        bookmaker_set.Add("ID_IRON", oDataSet.Tables("setting").Rows(0).Item("ID_IRON").ToString)
                        'ID_IRON = oDataSet.Tables("setting").Rows(0).Item("ID_IRON").ToString

                        'DELAI_JEU = oDataSet.Tables("setting").Rows(0).Item("DELAI_JEU").ToString

                        bookmaker_set.Add("MIN_GAIIN_SILVER", oDataSet.Tables("setting").Rows(0).Item("MIN_GAIIN_SILVER").ToString)

                        bookmaker_set.Add("MAX_GAIIN_SILVER", oDataSet.Tables("setting").Rows(0).Item("MAX_GAIIN_SILVER").ToString)
                        'MAX_GAIIN_SILVER = oDataSet.Tables("setting").Rows(0).Item("MAX_GAIIN_SILVER").ToString

                        bookmaker_set.Add("MIN_GAIN_IRON", oDataSet.Tables("setting").Rows(0).Item("MIN_GAIN_IRON").ToString)

                        bookmaker_set.Add("MAX_GAIN_IRON", oDataSet.Tables("setting").Rows(0).Item("MAX_GAIN_IRON").ToString)
                        'MAX_GAIN_IRON = oDataSet.Tables("setting").Rows(0).Item("MAX_GAIN_IRON").ToString

                        bookmaker_set.Add("MIN_LOTO_PLASMA", oDataSet.Tables("setting").Rows(0).Item("MIN_LOTO_PLASMA").ToString)
                        'MIN_LOTO_PLASMA = oDataSet.Tables("setting").Rows(0).Item("MIN_LOTO_PLASMA").ToString

                        bookmaker_set.Add("MIN_PARI_JACKPOT", oDataSet.Tables("setting").Rows(0).Item("MIN_PARI_JACKPOT").ToString)
                        'MIN_PARI_JACKPOT = oDataSet.Tables("setting").Rows(0).Item("MIN_PARI_JACKPOT").ToString

                        bookmaker_set.Add("PORT", oDataSet.Tables("setting").Rows(0).Item("PORT").ToString)
                        'PORT = oDataSet.Tables("setting").Rows(0).Item("PORT").ToString

                        bookmaker_set.Add("SCREEN_SELECT", oDataSet.Tables("setting").Rows(0).Item("SCREEN_SELECT").ToString)

                        'SCREEN_SELECT = oDataSet.Tables("setting").Rows(0).Item("SCREEN_SELECT").ToString

                        'tirage 
                        bookmaker_set.Add("TIRAGE", "0*0*0*0*0")

                        'Ticket gagnat du Jackpot
                        bookmaker_set.Add("ID_TICKET_GOLG", {"0"})
                        bookmaker_set.Add("ID_TICKET_SILVER", {"0"})
                        bookmaker_set.Add("ID_TICKET_IRON", {"0"})
                        bookmaker_set.Add("ID_TICKET_CAGNOTE", {"0"})
                        bookmaker_set.Add("ID_TICKET_MSTERIOUS", {"0"})
                        bookmaker_set.Add("ID_TICKET_MEGA", {"0"})
                        bookmaker_set.Add("animaDemarer", False)
                        bookmaker_set.Add("num_Tiragetosend", lastTir_No.ToString)


                        bookmaker_data(list_bookmaker.Tables("bookmaker").Rows(i).Item("bookmaker_id").ToString) = bookmaker_set

                    End If



                Next

            End If


        End Using
    End Sub

    Public Function PersistSetting(ByVal id_bookmaker As String) As Boolean

        Dim BOOKMAKER As Hashtable = CType(bookmaker_data(id_bookmaker), Hashtable)

        Using oUser As New BL_Lotto.BlUser(source)
            Return oUser.SaveSetting(id_bookmaker, BOOKMAKER("MONTH_BOOST_LIMIT").ToString, BOOKMAKER("MAX_BOOST_LIMIT").ToString, BOOKMAKER("DIFICULT_LEVEL").ToString, BOOKMAKER("DELAI_AV_GAINS").ToString, BOOKMAKER("PORT").ToString, BOOKMAKER("GOLD_POT").ToString, BOOKMAKER("SILVER_POT").ToString, BOOKMAKER("IRON_POT").ToString, BOOKMAKER("CAGNOTE_POT").ToString, BOOKMAKER("MSTERIOUS_POT").ToString, BOOKMAKER("MEGA_POT").ToString, BOOKMAKER("ID_CAGNOTE").ToString, BOOKMAKER("ID_GOLD").ToString, BOOKMAKER("ID_SILVER").ToString, BOOKMAKER("ID_IRON").ToString, BOOKMAKER("ID_MSTERIOUS").ToString, BOOKMAKER("ID_MEGA").ToString, BOOKMAKER("SCREEN_SELECT").ToString, BOOKMAKER("NB_SHARER").ToString, BOOKMAKER("MAX_GAIIN_SILVER").ToString, BOOKMAKER("MIN_GAIIN_SILVER").ToString, BOOKMAKER("MAX_GAIN_IRON").ToString, BOOKMAKER("MIN_GAIN_IRON").ToString, BOOKMAKER("MIN_LOTO_PLASMA").ToString, BOOKMAKER("MIN_PARI_JACKPOT").ToString, BOOKMAKER("DEDUCT_JACK_TO_TURN_OVER").ToString)
        End Using

    End Function
#End Region



    'Sub Combo(ByVal ComboI As ComboBox, ByVal Req As String)
    '    Dim con As MySqlConnection = New MySqlConnection(source)
    '    Dim cmd As New MySqlCommand(Req, con)
    '    Dim dr As MySqlDataReader
    '    con.Open()
    '    dr = cmd.ExecuteReader
    '    While dr.Read
    '        ComboI.Items.Add(dr(0))
    '    End While
    '    dr.Close()
    '    con.Close()
    'End Sub
    Public Function GetRandomDistinct(ByVal tailleVoulue As Integer, ByVal cpt As Integer) As Integer()
        Dim nombres As New List(Of Integer)()
        While nombres.Count <> tailleVoulue
            While nombres.Count <> tailleVoulue
                Dim rand As New Random()

                nombres.Add(rand.Next(0, cpt))
            End While
            nombres = nombres.Distinct().ToList()
        End While
        Return nombres.ToArray()
    End Function
    Public Sub select_another_lot(ByVal id_bookmaker As String, ByVal id_jack As String, ByVal id_pv As String, Optional IsRandAmount As Boolean = False)

        Dim oDataSet As DataSet
        Dim q As Integer
        Dim aqt As Integer
        Dim cpt As Integer
        Dim BOOKMAKER As Hashtable = CType(bookmaker_data(id_bookmaker), Hashtable)

        Using ojacpot As New BL_Lotto.BlJackot(source)
            Try
                'todo a revoir cette partie
                Dim rand As New Random()


                oDataSet = ojacpot.GetListlot(id_jack, id_bookmaker, id_pv, False)
                cpt = oDataSet.Tables("lot_jackpot").Rows.Count
                aqt = 1
                q = rand.Next(0, cpt)
                'q = GetRandomDistinct(aqt, cpt)

                If Not IsRandAmount Then

                    'MsgBox(" avant d'entrer" & ListWinTemp.Length)
                    If (oDataSet.Tables("lot_jackpot").Rows.Count > 0) Then

                        ojacpot.Maj_mon_Jackpot(id_jack, id_bookmaker, id_pv, oDataSet.Tables("lot_jackpot").Rows(q).Item("id_lot").ToString)

                    End If
                Else

                    'If (oDataSet.Tables("lot_jackpot").Rows(q(0)).Item("id_lot").ToString <> "0") Then
                    'ojacpot.Maj_mon_Jackpot(id_jack, oDataSet.Tables("lot_jackpot").Rows(q(0)).Item("id_lot").ToString)
                    Dim dlot As New DataSet
                    dlot = ojacpot.GetlotEnCours(id_bookmaker, id_jack, id_pv)


                    If (dlot.Tables("lot_jackpot").Rows.Count > 0) Then
                        rand = New Random()
                        Dim nv_jack_sil As Integer = 0
                        Select Case id_jack
                            Case BOOKMAKER("ID_SILVER").ToString
                                nv_jack_sil = rand.Next(CInt(BOOKMAKER("MIN_GAIIN_SILVER")), CInt(BOOKMAKER("MAX_GAIIN_SILVER")))
                            Case BOOKMAKER("ID_IRON").ToString
                                nv_jack_sil = rand.Next(CInt(BOOKMAKER("MIN_GAIN_IRON")), CInt(BOOKMAKER("MAX_GAIN_IRON")))

                        End Select


                        'ojacpot.Maj_mon_Jackpot(id_jack, 0, nv_jack_sil, 0, "Cagnotte variable")
                        ojacpot.Maj_mon_Lot(dlot.Tables("lot_jackpot").Rows(0).Item("id_lot").ToString, nv_jack_sil.ToString, False, "")

                    End If


                    'End If
                End If


            Catch ex As Exception

            End Try
        End Using

    End Sub

    Sub main()

    End Sub


    Public Class Client
        Private _SocketClient As Socket 'Le socket du client
        Private _Pseudo As String 'Le pseudo du client

        'Constructeur
        Sub New(ByVal Sock As Socket)
            _SocketClient = Sock
        End Sub

        Public Sub TraitementCaisse()

            'Console.WriteLine("Thread client lancé. ")

            'Le client vient de se connecter

            'Broadcast(_Pseudo & " identifié sur le chat") 'Diffuse le message à tout le monde 
            While (_SocketClient.Connected)
                Try
                    'on reste en attente de la requete du client
                    Dim req As String() = Me.ReceiveMessage

                    Dim reponse As String = ""
                    'on effectue l'operation desire
                    Try
                        reponse = Operation(req)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try


                    'la reponse est ensuite envoiye au client

                    Dim Message As String = reponse
                    'Dim Message As String = TirageLancer.ToString & "/" & timetosend & "/" & anim & "/" & tirage & "/" & String.Join("*", ID_TICKET_GOLG) & "/" & String.Join("*", ID_TICKET_SILVER) & "/" & String.Join("*", ID_TICKET_IRON)
                    'Recu = _SocketClient.Receive(Bytes)
                    Me.EnvoiMessage(Message)

                    'juste pour les test
                    'If (ID_TICKET_GOLG <> "0") Or (ID_TICKET_SILVER <> "0") Or (ID_TICKET_IRON <> "0") Then
                    '    MsgBox(Message)
                    'End If

                    'Broadcast(_Pseudo & " dit : " & Message) 'Diffuse le message à tout le monde 
                Catch ex As Exception 'Le client est déconnecté
                    ListeCaisses.Remove(Me) 'Le supprime de la liste des clients connectés
                    _SocketClient.Close() 'Ferme son socket
                    'Broadcast(_Pseudo & " déconnecté.") 'Diffuse le message à tout le monde 
                    Return 'Fin de la fonction
                End Try
            End While

        End Sub

        Public Sub EnvoiMessage(ByVal Message As String)
            Dim Mess As Byte() = System.Text.Encoding.UTF8.GetBytes(Message)
            Dim Envoi As Integer = _SocketClient.Send(Mess)
            'Console.WriteLine(Envoi & " bytes envoyés au client " & _Pseudo)
        End Sub

        Public Sub Envoibyte(ByVal Message As Byte())

            Dim Envoi As Integer = _SocketClient.Send(Message)
            'Console.WriteLine(Envoi & " bytes envoyés au client " & _Pseudo)
        End Sub

        Public Function ReceiveMessage() As String()
            Dim Bytes(255) As Byte
            Dim Recu As Integer
            Try
                Recu = _SocketClient.Receive(Bytes)
            Catch ex As Exception 'Erreur si fermeture du socket pendant la réception
                MessageBox.Show("Connexion perdue, arrêt de la réception des données ...", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Dim Message As String
            Message = System.Text.Encoding.UTF8.GetString(Bytes)
            Message = Message.Substring(0, Recu)

            Return Message.Split(CChar("/"))
        End Function

        Public Function Operation(ByVal paramOperation As String()) As String
            Dim numoperatio As String
            Dim reponse As String = "0"

            If paramOperation.Length > 0 Then

                numoperatio = paramOperation(0)

                Select Case numoperatio
                    Case "1" 'enregistrement d'un nouveau ticket

                        If paramOperation.Length < 5 Then
                            reponse = "0"
                        Else
                            'numero requete,code_sale,numcaissier,nbparie,tabnumerojouer
                            Dim code_sale As String = paramOperation(1)
                            Dim numcaissier As String = paramOperation(2)
                            Dim nbparie As String = paramOperation(3)
                            Dim tabnumerojouer As String = paramOperation(4)
                            Dim pari As String() = paramOperation(4).Split(CChar("*"))
                            Dim nb_chance As Integer = CInt(paramOperation(5))

                            Dim paris(100, 3) As String

                            For i As Integer = 0 To pari.Length - 1
                                Dim tem As String() = pari(i).Split(CChar("#"))

                                'mode de jeu
                                paris(i, 0) = tem(0)
                                'montant de la mise
                                paris(i, 1) = tem(1)
                                'numero jouer
                                paris(i, 2) = tem(2)
                            Next i

                            reponse = Me.AjouterTicket(code_sale, numcaissier, paris, CInt(nbparie), nb_chance).ToString
                        End If

                    Case "2" 'check ticket
                        Dim code_bar As String = paramOperation(1)
                        Dim numcaissier As String = paramOperation(2)
                        reponse = Me.check_tick(code_bar, numcaissier)

                    Case "3" 'retourne le timer et l'id du prochain tirage actuel
                        Dim user_id As String = paramOperation(1)
                        Dim id_bookmaker As String = paramOperation(2)

                        Dim datedebut As String = Date.Now.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture)
                        Dim datefin As String = Date.Now.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture)
                        Dim benef As Integer = 0
                        Dim mise As Integer = 0
                        Dim sortie As Integer = 0

                        Dim rep As String = Me.UserComptbility(user_id, id_bookmaker, datedebut, datefin)

                        Dim compte() As String = rep.Split(CChar("/"))

                        'NbtTicketJack & "/" & NbTicket & "/" & TotalMise & "/" & NbTirage & "/" & nbticketGagnant & "/" & MontantSortie & "/" & totaltickannuller & "/" & TotalCreditDebit 
                        If compte.Count > 2 Then
                            mise = (CInt(compte(2)) + If(CInt(compte(7)) > 0, CInt(compte(7)), 0))
                            sortie = (CInt(compte(5)) + If(CInt(compte(7)) < 0, CInt(compte(7)), 0))
                            benef = mise - sortie
                        End If

                        reponse = TirageLancer.ToString & "/" & timetosend & "/" & Tir_No & "/" & benef.ToString

                    'Recu = _SocketClient.Receive(Bytes)

                    Case "4" 'check ticket
                        Dim code_bar As String = paramOperation(1)
                        Dim numcaissier As String = paramOperation(2)
                        reponse = Me.check_jackpot_tick(code_bar, numcaissier)
                    Case "5" 'information pour le jeu
                        Dim id_bookmaker As String = paramOperation(1)
                        If bookmaker_data.ContainsKey(id_bookmaker) Then
                            Dim BOOKMAKER As Hashtable = CType(bookmaker_data(id_bookmaker), Hashtable)
                            reponse = timetosend & "/" & CInt(BOOKMAKER("animaDemarer")) & "/" & Tir_No & "/" & BOOKMAKER("num_Tiragetosend").ToString & "/" & BOOKMAKER("TIRAGE").ToString & "/" & Join(BOOKMAKER("ID_TICKET_GOLG"), "#") & "/" & Join(BOOKMAKER("ID_TICKET_SILVER"), "#") & "/" & Join(BOOKMAKER("ID_TICKET_IRON"), "#") & "/" & Join(BOOKMAKER("ID_TICKET_CAGNOTE"), "#") & "/" & Join(BOOKMAKER("ID_TICKET_MSTERIOUS"), "#") & "/" & Join(BOOKMAKER("ID_TICKET_MEGA"), "#")
                        Else
                            reponse = timetosend & "/0/0/0/0/0/0/0/0/0/0"
                        End If

                    Case "6" 'get lot

                        Dim id_jack As String = paramOperation(1)
                        Dim id_pv As String = paramOperation(2)
                        Dim id_bookmaker As String = paramOperation(3)

                        Dim dstsilver As New DataSet


                        'Sérialisation et écriture
                        Using ojacpot As New BL_Lotto.BlJackot(source)
                            dstsilver = ojacpot.GetJackpot(id_jack, id_pv, id_bookmaker)
                        End Using
                        If (dstsilver.Tables("jackpot").Rows.Count > 0) Then
                            reponse = dstsilver.Tables("jackpot").Rows(0).Item("nom_jackp").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("photo").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("is_matirial").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("nom_materiel").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("montant_jackp").ToString & " FCFA" & "/" & dstsilver.Tables("jackpot").Rows(0).Item("etat_jackp").ToString & " FCFA" & "/" & dstsilver.Tables("jackpot").Rows(0).Item("schow_etat").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("video").ToString
                        Else
                            reponse = "0"
                        End If
                    Case "7" 'get message jackpot

                        'Dim id_jack As String = paramOperation(1)
                        Dim id_bookmaker As String = paramOperation(1)
                        Dim dstmessage As New DataSet


                        'Sérialisation et écriture
                        Using ojacpot As New BL_Lotto.BlJackot(source)
                            dstmessage = ojacpot.GetMessage(id_bookmaker)
                        End Using
                        If (dstmessage.Tables("jackpot").Rows.Count > 0) Then
                            Dim tem As String = ""
                            Dim jac As String = ""
                            For i As Integer = 0 To dstmessage.Tables("jackpot").Rows.Count - 1
                                If CType(dstmessage.Tables("jackpot").Rows(i).Item("is_materiel"), Boolean) Then
                                    jac = dstmessage.Tables("jackpot").Rows(i).Item("nom_materiel").ToString
                                Else
                                    jac = dstmessage.Tables("jackpot").Rows(i).Item("montant").ToString
                                End If

                                If i = 0 Then
                                    tem = " Jackpot : " & jac & " TICKET GAGNANT  : " & dstmessage.Tables("jackpot").Rows(i).Item("COD_BAR").ToString
                                Else
                                    tem = tem & " | Jackpot : " & jac & " TICKET GAGNANT  : " & dstmessage.Tables("jackpot").Rows(i).Item("COD_BAR").ToString
                                End If
                            Next i
                            reponse = tem
                        Else
                            reponse = "0"
                        End If
                    Case "8" 'get Later Tirage jackpot

                        Dim id_bookmaker As String = paramOperation(1)
                        Dim dsttirage As New DataSet

                        'Sérialisation et écriture
                        Using otirage As New BL_Lotto.BlTirage(source)
                            dsttirage = otirage.getTirages(id_bookmaker)
                        End Using
                        If (dsttirage.Tables("contenir").Rows.Count > 0) Then
                            Dim tem As String = ""
                            For i As Integer = 0 To dsttirage.Tables("contenir").Rows.Count - 1
                                If i = 0 Then
                                    tem = dsttirage.Tables("contenir").Rows(i).Item("NUM_TIRAGE").ToString & "#" & dsttirage.Tables("contenir").Rows(i).Item("ID_NUM_TIRER").ToString
                                Else
                                    tem = tem & "/" & dsttirage.Tables("contenir").Rows(i).Item("NUM_TIRAGE").ToString & "#" & dsttirage.Tables("contenir").Rows(i).Item("ID_NUM_TIRER").ToString
                                End If
                            Next i
                            reponse = tem
                        Else
                            reponse = "0"
                        End If
                    Case "9" 'Connect user

                        Dim login As String = paramOperation(1)
                        Dim pwd As String = paramOperation(2)
                        Dim bookmakerCredential As String = paramOperation(3).ToUpper()
                        Dim USERID As String = "0"

                        reponse = "0/0/0/0"  'ID_USER, PRENOM, ID_PV, Id_user_Typ
                        'Sérialisation et écriture
                        Using oUser As New BL_Lotto.BlUser(source)
                            If oUser.ConPossible(login, pwd, bookmakerCredential) = True Then
                                reponse = oUser.GetUserinfo(login, pwd, bookmakerCredential)
                            End If
                        End Using


                    Case "10" 'get allmode

                        Dim dsuser As New DataSet

                        Using oUser As New BL_Lotto.BlUser(source)
                            dsuser = oUser.getallmode

                            If (dsuser.Tables("mode_jeux").Rows.Count > 0) Then
                                Dim tem As String = ""
                                For h As Integer = 0 To dsuser.Tables("mode_jeux").Rows.Count - 1
                                    If h = 0 Then
                                        tem = dsuser.Tables("mode_jeux").Rows(h).Item("ID_MODE").ToString & "#" & dsuser.Tables("mode_jeux").Rows(h).Item("QUOTA").ToString
                                    Else
                                        tem = tem & "/" & dsuser.Tables("mode_jeux").Rows(h).Item("ID_MODE").ToString & "#" & dsuser.Tables("mode_jeux").Rows(h).Item("QUOTA").ToString
                                    End If
                                Next
                                reponse = tem
                            Else
                                reponse = "0"

                            End If
                        End Using

                    Case "11" 'get last 

                        Dim dsuser As New DataSet
                        Using oTicket As New BL_Lotto.BlTicket(source)

                            Dim dsTirage As New DataSet
                            dsTirage = oTicket.GetLastTirageNum()

                            If dsTirage.Tables("tirage").Rows.Count > 0 Then
                                reponse = CStr(CInt(dsTirage.Tables("tirage").Rows(0).Item("NUM_TIRAGE")) + 1)
                            Else
                                reponse = "1"
                            End If

                        End Using
                    Case "12" 'Verifier la l'activation du compte
                        Dim id_bookmaker As String = paramOperation(1)
                        Using Oactivation As New BL_Lotto.BlActivation(source)
                            'Dim Tabdate(), datedujour As String

                            If (Oactivation.IsActivated(id_bookmaker)) Then
                                reponse = "1"
                            Else
                                reponse = "0"
                            End If

                        End Using

                    Case "13" 'Ob User Kann

                        Dim user_id As String = paramOperation(1)

                        Using Ouser As New BL_Lotto.BlUser(source)



                            If (Ouser.can(user_id)) Then
                                reponse = "1"
                            Else
                                reponse = "0"
                            End If


                        End Using
                    Case "14" 'GetSetting

                        Dim oDataSet As DataSet
                        Dim id_bookmaker As String = paramOperation(1)
                        Dim MONTH_BOOST_LIMIT, MAX_BOOST_LIMIT, DIFICULT_LEVEL, DELAI_AV_GAINS, PORT, GOLD_POT, SILVER_POT, IRON_POT, CAGNOTE_POT, MSTERIOUS_POT, MEGA_POT, ID_CAGNOTE, ID_GOLD, ID_SILVER, ID_IRON, ID_MSTERIOUS, ID_MEGA, SCREEN_SELECT, NB_SHARER, MAX_GAIIN_SILVER, MIN_GAIIN_SILVER, MAX_GAIN_IRON, MIN_GAIN_IRON, MIN_LOTO_PLASMA, MIN_PARI_JACKPOT, DELAI_JEU, DEDUCT_JACK_TO_TURN_OVER As String

                        Using oUser As New BL_Lotto.BlUser(source)
                            oDataSet = oUser.GetSetting(id_bookmaker)
                            If oDataSet.Tables("setting").Rows.Count > 0 Then

                                MONTH_BOOST_LIMIT = oDataSet.Tables("setting").Rows(0).Item("MONTH_BOOST_LIMIT").ToString
                                MAX_BOOST_LIMIT = oDataSet.Tables("setting").Rows(0).Item("MAX_BOOST_LIMIT").ToString
                                DIFICULT_LEVEL = oDataSet.Tables("setting").Rows(0).Item("DIFICULT_LEVEL").ToString
                                DELAI_AV_GAINS = oDataSet.Tables("setting").Rows(0).Item("DELAI_AV_GAINS").ToString
                                PORT = oDataSet.Tables("setting").Rows(0).Item("PORT").ToString
                                GOLD_POT = oDataSet.Tables("setting").Rows(0).Item("GOLD_POT").ToString
                                SILVER_POT = oDataSet.Tables("setting").Rows(0).Item("SILVER_POT").ToString
                                IRON_POT = oDataSet.Tables("setting").Rows(0).Item("IRON_POT").ToString
                                CAGNOTE_POT = oDataSet.Tables("setting").Rows(0).Item("CAGNOTE_POT").ToString
                                MSTERIOUS_POT = oDataSet.Tables("setting").Rows(0).Item("MSTERIOUS_POT").ToString
                                MEGA_POT = oDataSet.Tables("setting").Rows(0).Item("MEGA_POT").ToString
                                ID_CAGNOTE = oDataSet.Tables("setting").Rows(0).Item("ID_CAGNOTE").ToString
                                ID_GOLD = oDataSet.Tables("setting").Rows(0).Item("ID_GOLD").ToString
                                ID_SILVER = oDataSet.Tables("setting").Rows(0).Item("ID_SILVER").ToString
                                ID_IRON = oDataSet.Tables("setting").Rows(0).Item("ID_IRON").ToString
                                ID_MSTERIOUS = oDataSet.Tables("setting").Rows(0).Item("ID_MSTERIOUS").ToString
                                ID_MEGA = oDataSet.Tables("setting").Rows(0).Item("ID_MEGA").ToString
                                SCREEN_SELECT = oDataSet.Tables("setting").Rows(0).Item("SCREEN_SELECT").ToString
                                NB_SHARER = oDataSet.Tables("setting").Rows(0).Item("NB_SHARER").ToString
                                MAX_GAIIN_SILVER = oDataSet.Tables("setting").Rows(0).Item("MAX_GAIIN_SILVER").ToString
                                MIN_GAIIN_SILVER = oDataSet.Tables("setting").Rows(0).Item("MIN_GAIIN_SILVER").ToString
                                MAX_GAIN_IRON = oDataSet.Tables("setting").Rows(0).Item("MAX_GAIN_IRON").ToString
                                MIN_GAIN_IRON = oDataSet.Tables("setting").Rows(0).Item("MIN_GAIN_IRON").ToString
                                MIN_LOTO_PLASMA = oDataSet.Tables("setting").Rows(0).Item("MIN_LOTO_PLASMA").ToString
                                MIN_PARI_JACKPOT = oDataSet.Tables("setting").Rows(0).Item("MIN_PARI_JACKPOT").ToString
                                DELAI_JEU = My.Settings.DELAI_JEU.ToString
                                DEDUCT_JACK_TO_TURN_OVER = oDataSet.Tables("setting").Rows(0).Item("DEDUCT_JACK_TO_TURN_OVER").ToString


                                reponse = MONTH_BOOST_LIMIT & "/" & MAX_BOOST_LIMIT & "/" & DIFICULT_LEVEL & "/" & DELAI_AV_GAINS & "/" & PORT & "/" & GOLD_POT & "/" & SILVER_POT & "/" & IRON_POT & "/" & CAGNOTE_POT & "/" & MSTERIOUS_POT & "/" & MEGA_POT & "/" & ID_CAGNOTE & "/" & ID_GOLD & "/" & ID_SILVER & "/" & ID_IRON & "/" & ID_MSTERIOUS & "/" & ID_MEGA & "/" & SCREEN_SELECT & "/" & NB_SHARER & "/" & MAX_GAIIN_SILVER & "/" & MIN_GAIIN_SILVER & "/" & MAX_GAIN_IRON & "/" & MIN_GAIN_IRON & "/" & MIN_LOTO_PLASMA & "/" & MIN_PARI_JACKPOT & "/" & DELAI_JEU & "/" & DEDUCT_JACK_TO_TURN_OVER
                            Else
                                reponse = "0"
                            End If


                        End Using
                    Case "15" 'compte Caisse

                        Dim user_id As String = paramOperation(1)

                        Dim datedeb1(), datef1() As String

                        Dim datedeb As String = paramOperation(2)
                        Dim datef As String = paramOperation(3)
                        Dim id_bookmaker As String = paramOperation(4)

                        datedeb1 = datedeb.Split(CChar("#"))
                        datef1 = datef.Split(CChar("#"))

                        Dim datedebut As String = datedeb1(0) & "/" & datedeb1(1) & "/" & datedeb1(2)
                        Dim datefin As String = datef1(0) & "/" & datef1(1) & "/" & datef1(2)

                        reponse = Me.UserComptbility(user_id, id_bookmaker, datedebut, datefin)

                    Case "16" 'Getlot

                        Dim Idlot As String = paramOperation(1)
                        Dim montant_jackp, nom_materiel, id_jack, id_lot, etat_jackp, photo, id_pv, is_matirial, schow_etat, status, video As String

                        Dim oDataSet As DataSet

                        Using ojacpot As New BL_Lotto.BlJackot(source)
                            oDataSet = ojacpot.Getlot(Idlot)

                            montant_jackp = oDataSet.Tables("lot_jackpot").Rows(0).Item("montant_jackp").ToString
                            nom_materiel = oDataSet.Tables("lot_jackpot").Rows(0).Item("nom_materiel").ToString
                            id_jack = oDataSet.Tables("lot_jackpot").Rows(0).Item("id_jack").ToString
                            id_lot = oDataSet.Tables("lot_jackpot").Rows(0).Item("id_lot").ToString
                            etat_jackp = oDataSet.Tables("lot_jackpot").Rows(0).Item("etat_jackp").ToString
                            photo = oDataSet.Tables("lot_jackpot").Rows(0).Item("photo").ToString
                            id_pv = oDataSet.Tables("lot_jackpot").Rows(0).Item("id_pv").ToString
                            is_matirial = oDataSet.Tables("lot_jackpot").Rows(0).Item("is_matirial").ToString
                            schow_etat = oDataSet.Tables("lot_jackpot").Rows(0).Item("schow_etat").ToString
                            status = oDataSet.Tables("lot_jackpot").Rows(0).Item("status").ToString
                            video = oDataSet.Tables("lot_jackpot").Rows(0).Item("video").ToString

                            'Tb_etat_gold.ReadOnly = False

                            reponse = montant_jackp & "/" & nom_materiel & "/" & id_jack & "/" & id_lot & "/" & etat_jackp & "/" & photo & "/" & id_pv & "/" & is_matirial & "/" & schow_etat & "/" & status & "/" & video
                        End Using

                    Case "17" 'Maj_mon_Lot_Set

                        Dim Idlot As String = paramOperation(1)
                        Dim TbGold As String = paramOperation(2)
                        Dim Tb_etat_gold As String = paramOperation(3)
                        Dim is_material_gold As String = paramOperation(4)
                        Dim tbNomLotGold As String = paramOperation(5)
                        Dim selected_pv As String = paramOperation(6)
                        Dim schow_etat As String = paramOperation(7)
                        Dim status As String = paramOperation(8)
                        Dim fname As String = paramOperation(9)
                        Dim videoFileName As String = paramOperation(10)


                        Using ojacpot As New BL_Lotto.BlJackot(source)
                            If ojacpot.Maj_mon_Lot_Set(Idlot, selected_pv, TbGold.Trim, Tb_etat_gold, CBool(is_material_gold), tbNomLotGold.Trim, schow_etat, status, fname, videoFileName) Then
                                reponse = "1"
                            Else
                                reponse = "0"
                            End If
                        End Using
                    Case "18" ' comptabilite generale

                        Dim datedeb1(), datef1() As String

                        Dim datedeb As String = paramOperation(1)
                        Dim datef As String = paramOperation(2)
                        Dim id_bookmaker As String = paramOperation(3)

                        datedeb1 = datedeb.Split(CChar("#"))
                        datef1 = datef.Split(CChar("#"))

                        'on initialise l'interval des date

                        Dim datedebut As String = datedeb1(0) & "/" & datedeb1(1) & "/" & datedeb1(2)
                        Dim datefin As String = datef1(0) & "/" & datef1(1) & "/" & datef1(2)


                        Dim row0 As String = ""
                        Dim Gagner As Boolean = False
                        Dim Gains As Integer = 0
                        Dim TotalmontantJack As Integer = 0
                        Dim totaltickannuller As Integer = 0
                        Dim rest As Integer = 0
                        Dim LbTotalMise, id_ticket, idpv As String
                        Dim dsTickJackMon, dsPari, ODsPari, dsTouticket, dspv As New DataSet

                        'recuperation de la liste des points de vente
                        Using ojacpot As New BL_Lotto.BlJackot(source)
                            dspv = ojacpot.GetListPointVente(id_bookmaker)
                        End Using
                        Using oCompte As New BL_Lotto.BlCompte(source)

                            'et on boucle dessus pour effectuer le compte sur chacune d'elle
                            For h As Integer = 0 To dspv.Tables("point_vente").Rows.Count - 1
                                idpv = dspv.Tables("point_vente").Rows(h).Item("id_pv").ToString

                                ' montant de l'enssemble des Jackpot checker par salle
                                dsTickJackMon = oCompte.getStatTickJackMon(idpv, datedebut & " 00:00:00", datefin & " 23:59:59")

                                dsPari = oCompte.getStatPari(idpv, datedebut & " 00:00:00", datefin & " 23:59:59")
                                dsTouticket = oCompte.getListTicket(idpv, datedebut & " 00:00:00", datefin & " 23:59:59")

                                If (dsTickJackMon.Tables("ticket").Rows.Count > 0) Then
                                    If (Not dsTickJackMon.Tables("ticket").Rows(0).IsNull("TOTAL_MONTANT")) Then
                                        TotalmontantJack = CInt(dsTickJackMon.Tables("ticket").Rows(0).Item("TOTAL_MONTANT"))
                                    Else
                                        TotalmontantJack = 0
                                    End If
                                End If
                                LbTotalMise = "0"
                                If (dsPari.Tables("pari").Rows.Count > 0) Then
                                    If (Not dsPari.Tables("pari").Rows(0).IsNull("TOTALMISE")) Then
                                        LbTotalMise = dsPari.Tables("pari").Rows(0).Item("TOTALMISE").ToString
                                    End If
                                End If

                                'calcul du nombre de ticket gagnant

                                Dim MontantSortie, nbticketGagnant As Integer
                                nbticketGagnant = 0
                                MontantSortie = 0
                                Using oTicket As New BL_Lotto.BlTicket(source)
                                    For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
                                        id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                                        ODsPari = oTicket.GetPari(id_ticket)
                                        Gagner = False
                                        Gains = 0
                                        'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                                        If (ODsPari.Tables("pari").Rows.Count > 0) Then

                                            For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                                                'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)
                                                Dim verif As Boolean = oTicket.verifWinner(id_bookmaker, ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                                                    dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)
                                                If (verif) Then
                                                    Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                                                End If

                                                Gagner = Gagner Or verif

                                            Next

                                        End If

                                        If (Gagner) Then
                                            nbticketGagnant += 1
                                            MontantSortie += CInt(Gains)
                                        End If

                                    Next
                                End Using

                                MontantSortie += TotalmontantJack

                                Dim benef As Integer = CInt(LbTotalMise) - CInt(MontantSortie)
                                rest += benef
                                If h = 0 Then
                                    row0 = dspv.Tables("point_vente").Rows(h).Item("id_pv").ToString & "#" & dspv.Tables("point_vente").Rows(h).Item("nom_pv").ToString & "#" & LbTotalMise & "#" & MontantSortie.ToString & "#" & benef.ToString
                                Else
                                    row0 = row0 & "/" & dspv.Tables("point_vente").Rows(h).Item("id_pv").ToString & "#" & dspv.Tables("point_vente").Rows(h).Item("nom_pv").ToString & "#" & LbTotalMise & "#" & MontantSortie.ToString & "#" & benef.ToString
                                End If

                            Next
                            reponse = row0

                        End Using
                    Case "19" 'save setting
                        Dim id_bookmaker As String = paramOperation(1)
                        Dim MTH_BOOST_LIMIT As String = paramOperation(2)
                        Dim MX_BOOST_LIMIT As String = paramOperation(3)
                        Dim DIFICULT_LEVEL As String = paramOperation(4)
                        Dim DELAI_AV_GAINS As String = paramOperation(5)
                        Dim PRT As String = paramOperation(6)
                        Dim GLD_POT As String = paramOperation(7)
                        Dim SLVR_POT As String = paramOperation(8)
                        Dim IRN_POT As String = paramOperation(9)


                        Dim CAGNOTE_POT As String = paramOperation(10)
                        Dim MYSTRUS_POT As String = paramOperation(11)
                        Dim MGA_POT As String = paramOperation(12)
                        Dim ID_CAGNOTE As String = paramOperation(13)
                        Dim ID_GOLD As String = paramOperation(14)


                        Dim ID_SILVER As String = paramOperation(15)
                        Dim ID_IRON As String = paramOperation(16)
                        Dim ID_MSTERIOUS As String = paramOperation(17)
                        Dim ID_MEGA As String = paramOperation(18)
                        Dim SCREEN_SELECT As String = paramOperation(19)
                        Dim NB_SHARER As String = paramOperation(20)
                        Dim MAX_GAIIN_SILVER As String = paramOperation(21)
                        Dim MIN_GAIIN_SILVER As String = paramOperation(22)
                        Dim MAX_GAIN_IRON As String = paramOperation(23)
                        Dim MIN_GAIN_IRON As String = paramOperation(24)
                        Dim MIN_LOTO_PLASMA As String = paramOperation(25)
                        Dim MIN_PARI_JACKPOT As String = paramOperation(26)
                        Dim DEDUCT_JACK_TO_TURN_OVER As String = paramOperation(27)



                        Using oUser As New BL_Lotto.BlUser(source)

                            If oUser.SaveSetting(id_bookmaker, MTH_BOOST_LIMIT, MX_BOOST_LIMIT, DIFICULT_LEVEL, DELAI_AV_GAINS, PRT, GLD_POT, SLVR_POT, IRN_POT,
                                                 CAGNOTE_POT, MYSTRUS_POT, MGA_POT, ID_CAGNOTE, ID_GOLD, ID_SILVER, ID_IRON, ID_MSTERIOUS, ID_MEGA, SCREEN_SELECT,
                                                 NB_SHARER, MAX_GAIIN_SILVER, MIN_GAIIN_SILVER, MAX_GAIN_IRON, MIN_GAIN_IRON, MIN_LOTO_PLASMA, MIN_PARI_JACKPOT, DEDUCT_JACK_TO_TURN_OVER) Then
                                reponse = "1"
                            Else
                                reponse = "0"
                            End If
                        End Using

                    Case "20" 'get message jackpot
                        Dim bookmaker_id As String = paramOperation(1)
                        'Dim id_jack As String = paramOperation(1)
                        Dim dstlastJack As New DataSet
                        'Sérialisation et écriture
                        Using ojacpot As New BL_Lotto.BlJackot(source)
                            dstlastJack = ojacpot.GetLastJackpot(bookmaker_id)
                        End Using
                        If (dstlastJack.Tables("jackpot").Rows.Count > 0) Then
                            Dim tem As String = ""
                            Dim jac As String = ""
                            If CType(dstlastJack.Tables("jackpot").Rows(0).Item("is_materiel"), Boolean) Then
                                jac = dstlastJack.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
                            Else
                                jac = dstlastJack.Tables("jackpot").Rows(0).Item("montant").ToString
                            End If

                            tem = jac & "/" & dstlastJack.Tables("jackpot").Rows(0).Item("photo").ToString & "/" & dstlastJack.Tables("jackpot").Rows(0).Item("COD_BAR").ToString

                            reponse = tem
                        Else
                            reponse = "0"
                        End If
                    Case "21" 'get GetJackpotForTick

                        Dim code_bar_tick As String = paramOperation(1)
                        Dim id_jack As String = paramOperation(2)
                        Dim dstsilver As New DataSet


                        'Sérialisation et écriture
                        Using ojacpot As New BL_Lotto.BlJackot(source)
                            'dstsilver = ojacpot.GetJackpot(id_jack, id_pv)
                            dstsilver = ojacpot.GetJackpotForTick(code_bar_tick, id_jack)
                        End Using
                        If (dstsilver.Tables("jackpot").Rows.Count > 0) Then
                            'g.`id_lot`, j.`nom_jackp`, g.`is_materiel`, g.`nom_materiel`, g.`montant`, l.etat_jackp,`photo`, `video`
                            reponse = dstsilver.Tables("jackpot").Rows(0).Item("nom_jackp").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("is_materiel").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("nom_materiel").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("montant").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("etat_jackp").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("photo").ToString & "/" & dstsilver.Tables("jackpot").Rows(0).Item("video").ToString
                        Else
                            reponse = "0"
                        End If

                    Case "22" 'get GetJackpotList par PV

                        Dim dsTickJackList As New DataSet

                        'TODO: Change the client contrepart 


                        Dim idpv As String = paramOperation(1)

                        Dim datedeb1(), datef1() As String

                        Dim datedeb As String = paramOperation(2)
                        Dim datef As String = paramOperation(3)
                        Dim row0 As String = "0"

                        datedeb1 = datedeb.Split(CChar("#"))
                        datef1 = datef.Split(CChar("#"))

                        Dim datedebut As String = datedeb1(0) & "/" & datedeb1(1) & "/" & datedeb1(2)
                        Dim datefin As String = datef1(0) & "/" & datef1(1) & "/" & datef1(2)

                        Using oCompte As New BL_Lotto.BlCompte(source)
                            dsTickJackList = oCompte.getListJackParPv(idpv, datedebut & " 00:00:00", datefin & " 23:59:59")

                            If (dsTickJackList.Tables("ticket").Rows.Count > 0) Then
                                For h As Integer = 0 To dsTickJackList.Tables("ticket").Rows.Count - 1
                                    'Nom du lot , Prix
                                    '`id_tick``id_lot``ID_USER_S``is_materiel``nom_materiel``montant` COD_BAR
                                    If h = 0 Then
                                        row0 = dsTickJackList.Tables("ticket").Rows(h).Item("id_tick").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("COD_BAR").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("ID_USER_S").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("is_materiel").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("nom_materiel").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("montant").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("ID_USER_S").ToString
                                    Else
                                        row0 = row0 & "/" & dsTickJackList.Tables("ticket").Rows(h).Item("id_tick").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("COD_BAR").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("ID_USER_S").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("is_materiel").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("nom_materiel").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("montant").ToString & "#" & dsTickJackList.Tables("ticket").Rows(h).Item("ID_USER_S").ToString
                                    End If
                                Next
                            Else
                                row0 = "0"
                            End If
                        End Using
                        reponse = row0

                    Case "23" 'get GetJackpotList

                        Dim id_player As String = paramOperation(1)
                        Dim caisse As String = paramOperation(2)
                        Dim montant As String = paramOperation(3)
                        Dim dsCreditPlayer As DataSet
                        Dim TotalCreditDebit As Integer



                        Using oCaisse As New BL_Lotto.BlUser(source)

                            Using oCompte As New BL_Lotto.BlCompte(source)
                                dsCreditPlayer = oCompte.getCreditPlayer(id_player)
                                If (dsCreditPlayer.Tables("credit").Rows.Count > 0) Then
                                    If (Not dsCreditPlayer.Tables("credit").Rows(0).IsNull("TOTALCREDIT")) Then
                                        TotalCreditDebit = CInt(dsCreditPlayer.Tables("credit").Rows(0).Item("TOTALCREDIT"))
                                    Else
                                        TotalCreditDebit = 0
                                    End If
                                End If
                            End Using


                            If (CInt(montant) > 0) Or (Math.Abs(CInt(montant)) <= TotalCreditDebit) Then
                                If oCaisse.creditDebitPlayer(id_player, caisse, CInt(montant)) Then
                                    reponse = "1"
                                Else
                                    reponse = "0"
                                End If
                            Else
                                reponse = "0"
                            End If

                        End Using
                    Case "24" 'set new key
                        Dim bookmaker_id As String = paramOperation(1)
                        Dim cle As String = paramOperation(2)

                        Dim creditactivated As New Boolean
                        'Sérialisation et écriture
                        Using oActiv As New BL_Lotto.BlActivation(source)
                            creditactivated = oActiv.cleExist(bookmaker_id, cle)
                        End Using
                        If (creditactivated) Then
                            reponse = "1"
                        Else
                            reponse = "0"
                        End If
                    Case "25" ' get ticket details
                        Dim code_bar As String = paramOperation(1)
                        Dim ODsTicket As DataSet
                        Dim date_achat As String()
                        Dim date_achat_ticket As String
                        reponse = "0"

                        Using oTicket As New BL_Lotto.BlTicket(source)

                            ODsTicket = oTicket.TicketParCodebar(code_bar)

                            date_achat = ODsTicket.Tables("ticket").Rows(0).Item("DATE_ACHAT").ToString.Split(CChar("/"))
                            date_achat_ticket = date_achat(0) & "#" & date_achat(1) & "#" & date_achat(2)

                            ' ID_TICKET, NUM_TIRAGE, DATE_ACHAT, ID_USER_S, bookmaker_id
                            If (ODsTicket.Tables("ticket").Rows.Count > 0) Then
                                reponse = ODsTicket.Tables("ticket").Rows(0).Item("ID_TICKET").ToString & "/" & ODsTicket.Tables("ticket").Rows(0).Item("NUM_TIRAGE").ToString & "/" & date_achat_ticket & "/" & ODsTicket.Tables("ticket").Rows(0).Item("ID_USER_S").ToString & "/" & ODsTicket.Tables("ticket").Rows(0).Item("bookmaker_id").ToString
                            End If
                        End Using
                    Case "26" ' get tirage from num tirage and id_bookmaker
                        Dim num_tirage As String = If(paramOperation(1) = "0", lastTir_No.ToString, paramOperation(1))
                        Dim id_bookmaker As String = paramOperation(2)
                        Dim ODsTicket As DataSet
                        reponse = "0/0/0/0/0"

                        Using oTicket As New BL_Lotto.BlTicket(source)

                            ODsTicket = oTicket.getTirage(id_bookmaker, num_tirage)
                            ' ID_TICKET, NUM_TIRAGE, DATE_ACHAT, ID_USER_S, bookmaker_id
                            If (ODsTicket.Tables("contenir").Rows.Count > 0) Then
                                reponse = ODsTicket.Tables("contenir").Rows(0).Item("ID_NUM_TIRER").ToString & "/" & ODsTicket.Tables("contenir").Rows(1).Item("ID_NUM_TIRER").ToString & "/" & ODsTicket.Tables("contenir").Rows(2).Item("ID_NUM_TIRER").ToString & "/" & ODsTicket.Tables("contenir").Rows(3).Item("ID_NUM_TIRER").ToString & "/" & ODsTicket.Tables("contenir").Rows(4).Item("ID_NUM_TIRER").ToString
                            End If
                        End Using

                    Case "27" ' get Pari
                        Dim id_ticket As String = paramOperation(1)
                        Dim ODsPari, oDsNumJou As DataSet
                        Dim row, pari_num As String

                        reponse = "0"

                        Using oTicket As New BL_Lotto.BlTicket(source)

                            ODsPari = oTicket.GetPari(id_ticket)

                            For i As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1

                                oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(i).Item("ID_PARI").ToString)
                                pari_num = ""
                                If (CInt(ODsPari.Tables("pari").Rows(i).Item("NB_NUM_JOUER")) = 1) Then

                                    pari_num = oDsNumJou.Tables("avoir").Rows(0).Item("NUMEROS").ToString

                                ElseIf (CInt(ODsPari.Tables("pari").Rows(i).Item("NB_NUM_JOUER")) = 2) Then

                                    pari_num = oDsNumJou.Tables("avoir").Rows(0).Item("NUMEROS").ToString & "-" _
                                                & oDsNumJou.Tables("avoir").Rows(1).Item("NUMEROS").ToString

                                ElseIf (CInt(ODsPari.Tables("pari").Rows(i).Item("NB_NUM_JOUER")) = 3) Then
                                    pari_num = oDsNumJou.Tables("avoir").Rows(0).Item("NUMEROS").ToString & "-" _
                                                & oDsNumJou.Tables("avoir").Rows(1).Item("NUMEROS").ToString & "-" _
                                                & oDsNumJou.Tables("avoir").Rows(2).Item("NUMEROS").ToString
                                End If
                                ' MODE, pari_numbers, MONTANT_MISE, QUOTA, gains
                                row = ODsPari.Tables("pari").Rows(i).Item("MODE").ToString & "#" & pari_num & "#" & ODsPari.Tables("pari").Rows(i).Item("MONTANT_MISE").ToString & "#" & ODsPari.Tables("pari").Rows(i).Item("QUOTA").ToString & "#" & ODsPari.Tables("pari").Rows(i).Item("ID_PARI").ToString


                                reponse = If(i = 0, row, reponse & "/" & row).ToString
                            Next
                        End Using

                    Case "28" ' Verif Pari
                        Dim id_bookmaker As String = paramOperation(1)
                        Dim id_pari As String = paramOperation(2)
                        Dim tirage_num As String = paramOperation(3)

                        reponse = "0"

                        Using oTicket As New BL_Lotto.BlTicket(source)

                            If (oTicket.verifWinner(id_bookmaker, id_pari, tirage_num)) Then
                                reponse = "1"
                            Else
                                reponse = "0"
                            End If

                        End Using
                    Case "29" ' get ticket de jackpot servie par un caissier
                        Dim id_user As String = paramOperation(1)
                        Dim datedeb As String = paramOperation(2)
                        Dim datef As String = paramOperation(3)
                        Dim row As String

                        Dim dsTickJackList As New DataSet

                        Dim datedeb1(), datef1() As String


                        Dim row0 As String = "0"

                        datedeb1 = datedeb.Split(CChar("#"))
                        datef1 = datef.Split(CChar("#"))

                        Dim datedebut As String = datedeb1(0) & "/" & datedeb1(1) & "/" & datedeb1(2)
                        Dim datefin As String = datef1(0) & "/" & datef1(1) & "/" & datef1(2)

                        reponse = "0"

                        Using oTicket As New BL_Lotto.BlTicket(source)

                            dsTickJackList = oTicket.getListJackParUser(id_user, datedebut & " 00:00:00", datefin & " 23:59:59")

                            For i As Integer = 0 To dsTickJackList.Tables("ticket").Rows.Count - 1
                                ' COD_BAR, nom_materiel montant
                                row = dsTickJackList.Tables("ticket").Rows(i).Item("COD_BAR").ToString & "#" & dsTickJackList.Tables("ticket").Rows(i).Item("nom_materiel").ToString & "#" & dsTickJackList.Tables("ticket").Rows(i).Item("montant").ToString

                                If i = 0 Then
                                    reponse = row
                                Else
                                    reponse = reponse & "/" & row
                                End If

                            Next
                        End Using
                    Case "30" ' get user per bookmaker
                        Dim id_bookmaker As String = paramOperation(1)

                        Dim row As String

                        Dim dslistUser As New DataSet

                        reponse = "0"
                        Using oUser As New BL_Lotto.BlUser(source)

                            dslistUser = oUser.ListUser(id_bookmaker)

                            For i As Integer = 0 To dslistUser.Tables("users").Rows.Count - 1
                                ' ID_USER,NOM,PRENOM,LOGIN,PWD as PASSWORD,user_typ ,ut.id_user_typ
                                row = dslistUser.Tables("users").Rows(i).Item("ID_USER").ToString & "#" & dslistUser.Tables("users").Rows(i).Item("NOM").ToString & "#" & dslistUser.Tables("users").Rows(i).Item("PRENOM").ToString & "#" & dslistUser.Tables("users").Rows(i).Item("LOGIN").ToString & "#" & dslistUser.Tables("users").Rows(i).Item("user_typ").ToString & "#" & dslistUser.Tables("users").Rows(i).Item("id_user_typ").ToString
                                reponse = If(i = 0, row, reponse & "/" & row).ToString
                            Next
                        End Using
                    Case "31" ' get credit residuel per bookmaker
                        Dim id_bookmaker As String = paramOperation(1)
                        Dim creditRes As Integer

                        Using oCompte As New BL_Lotto.BlCompte(source)

                            creditRes = oCompte.getCreditResiduel(id_bookmaker)

                            reponse = creditRes.ToString

                        End Using

                    Case "32" ' get list player per bookmaker
                        Dim id_pv As String = paramOperation(1)

                        Dim row As String

                        Dim dslistUser As New DataSet

                        reponse = "0"
                        Using oUser As New BL_Lotto.BlUser(source)

                            dslistUser = oUser.ListPlayers(id_pv)

                            For i As Integer = 0 To dslistUser.Tables("users").Rows.Count - 1
                                'ID_USER,NOM,PRENOM,LOGIN,user_typ,ut.id_user_typ
                                row = dslistUser.Tables("users").Rows(i).Item("ID_USER").ToString & "#" & dslistUser.Tables("users").Rows(i).Item("NOM").ToString & "#" & dslistUser.Tables("users").Rows(i).Item("PRENOM").ToString & "#" & dslistUser.Tables("users").Rows(i).Item("LOGIN").ToString & "#" & dslistUser.Tables("users").Rows(i).Item("user_typ").ToString & "#" & dslistUser.Tables("users").Rows(i).Item("id_user_typ").ToString
                                reponse = If(i = 0, row, reponse & "/" & row).ToString
                            Next
                        End Using
                    Case "33" 'get getCreditPlayer

                        Dim id_player As String = paramOperation(1)

                        Dim dsCreditPlayer As DataSet
                        Dim TotalCreditDebit As Integer

                        reponse = "0"
                        Using oCompte As New BL_Lotto.BlCompte(source)
                            dsCreditPlayer = oCompte.getCreditPlayer(id_player)
                            If (dsCreditPlayer.Tables("credit").Rows.Count > 0) Then
                                If (Not dsCreditPlayer.Tables("credit").Rows(0).IsNull("TOTALCREDIT")) Then
                                    TotalCreditDebit = CInt(dsCreditPlayer.Tables("credit").Rows(0).Item("TOTALCREDIT"))
                                Else
                                    TotalCreditDebit = 0
                                End If
                            End If
                        End Using
                        reponse = TotalCreditDebit.ToString

                    Case "34" ' get ticket details with id
                        Dim id_ticket As String = paramOperation(1)
                        Dim ODsTicket As DataSet
                        Dim date_achat As String()
                        Dim date_achat_ticket As String
                        reponse = "0"

                        Using oTicket As New BL_Lotto.BlTicket(source)

                            ODsTicket = oTicket.TicketParId(id_ticket)

                            date_achat = ODsTicket.Tables("ticket").Rows(0).Item("DATE_ACHAT").ToString.Split(CChar("/"))
                            date_achat_ticket = date_achat(0) & "#" & date_achat(1) & "#" & date_achat(2)

                            ' ID_TICKET, NUM_TIRAGE, DATE_ACHAT, ID_USER, bookmaker_id, COde_bar
                            If (ODsTicket.Tables("ticket").Rows.Count > 0) Then
                                reponse = ODsTicket.Tables("ticket").Rows(0).Item("ID_TICKET").ToString & "/" & ODsTicket.Tables("ticket").Rows(0).Item("NUM_TIRAGE").ToString & "/" & date_achat_ticket & "/" & ODsTicket.Tables("ticket").Rows(0).Item("ID_USER").ToString & "/" & ODsTicket.Tables("ticket").Rows(0).Item("bookmaker_id").ToString & "/" & ODsTicket.Tables("ticket").Rows(0).Item("COD_BAR").ToString
                            End If
                        End Using

                    Case Else
                        reponse = "0"
                End Select

                Return reponse

            Else
                Return "0"

            End If


        End Function

        Function AjouterTicket(ByVal CODE_SALE As String, ByVal CONNECTED_USERID As String, ByVal tab As String(,), ByVal nbparie As Integer, ByVal selected_chance As Integer) As Integer
            Dim code_bar As String = ""
            Dim Tabdate(), ID_TICKET_P As String


            'Dim retour As Boolean

            Tabdate = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
            code_bar = CInt(CODE_SALE).ToString("0000")

            Using OBlTicket As New BL_Lotto.BlTicket(source)
                'Get a new Project DataSet

                Dim dsTirage As New DataSet
                dsTirage = OBlTicket.GetLastTirageNum
                Dim num_tirage As Integer

                If dsTirage.Tables("tirage").Rows.Count > 0 Then
                    num_tirage = CInt(dsTirage.Tables("tirage").Rows(0).Item("NUM_TIRAGE").ToString) + 1
                Else
                    num_tirage = 1
                End If

                Dim oDataSet As DataSet
                oDataSet = OBlTicket.NvDSTickect()
                'Initialize a datarow object from the Project DataSet
                Dim oDataRow As Data.DataRow = oDataSet.Tables("ticket").NewRow
                'Set the values in the DataRow
                oDataRow.Item("COD_BAR") = code_bar
                oDataRow.Item("id_pv") = CODE_SALE
                oDataRow.Item("parent_ticket") = "0"

                oDataRow.Item("NUM_TIRAGE") = num_tirage
                oDataRow.Item("ID_USER") = CONNECTED_USERID

                oDataSet.Tables("ticket").Rows.Add(oDataRow)
                'Add the DataRow to the DataSet
                'Add the Project
                ID_TICKET_P = "0"
                If Not OBlTicket.AjouterTicket(oDataSet, tab, nbparie, ID_TICKET_P, selected_chance) Then
                    Return 0
                Else
                    Dim ID_TICKET_CHANCE As String
                    Dim chance_op As Boolean = True

                    For i As Integer = 1 To selected_chance - 1
                        num_tirage += 1

                        oDataSet = OBlTicket.NvDSTickect()
                        'Initialize a datarow object from the Project DataSet
                        oDataRow = oDataSet.Tables("ticket").NewRow
                        'Set the values in the DataRow
                        oDataRow.Item("COD_BAR") = code_bar
                        oDataRow.Item("id_pv") = CODE_SALE
                        oDataRow.Item("parent_ticket") = ID_TICKET_P

                        oDataRow.Item("NUM_TIRAGE") = num_tirage
                        oDataRow.Item("ID_USER") = CONNECTED_USERID

                        oDataSet.Tables("ticket").Rows.Add(oDataRow)
                        'Add the DataRow to the DataSet
                        'Add the Project
                        ID_TICKET_CHANCE = "0"
                        chance_op = chance_op And OBlTicket.AjouterTicket(oDataSet, tab, nbparie, ID_TICKET_CHANCE, selected_chance)
                    Next
                    If chance_op Then
                        AjouterTicket = CInt(ID_TICKET_P)
                    Else
                        Return 0
                    End If
                End If
            End Using
        End Function

        Function check_tick(ByVal codebar As String, ByVal caissier As String) As String
            Dim ODsTicket, ODsPari As DataSet
            'Dim oDsNumJou As DataSet
            Dim pari_num, ID_TICKET, id_bookmaker As String

            Dim pariGagner As Boolean
            Dim proch_tirage As Integer
            Dim Gagner As Boolean = False
            Dim Gains As Integer = 0

            Dim need_to_print As Integer = 0
            Dim message As String = ""
            Dim deja_servi As Boolean = True
            Dim deja_annuler As Boolean = False
            Dim need_cancel As Boolean = False
            Dim ticket_to_cancel As String = ""
            Dim ticket_gagnant As Boolean = False
            Dim need_to_print_gagn As Boolean = False


            Using oTicket As New BL_Lotto.BlTicket(source)

                Gains = 0

                ODsTicket = oTicket.TicketParCodebar(codebar)

                If (ODsTicket.Tables("ticket").Rows.Count > 0) Then

                    For h As Integer = 0 To ODsTicket.Tables("ticket").Rows.Count - 1
                        ID_TICKET = ODsTicket.Tables("ticket").Rows(h).Item("ID_TICKET").ToString
                        id_bookmaker = ODsTicket.Tables("ticket").Rows(h).Item("bookmaker_id").ToString
                        proch_tirage = Tir_No


                        'ticket deja servie
                        deja_servi = deja_servi And (0 <> CInt(ODsTicket.Tables("ticket").Rows(h).Item("ID_USER_S")))
                        If (0 <> CInt(ODsTicket.Tables("ticket").Rows(h).Item("ID_USER_S"))) Then
                            ' pour controller si l'enssemble des ticket a deja servi
                            Continue For
                        End If

                        'on verifie si le ticket avait deja été annuler
                        If (0 = CInt(ODsTicket.Tables("ticket").Rows(h).Item("STATUS"))) Then
                            deja_annuler = True
                            Exit For
                        End If
                        ' on peu annuller les ticket qu'au premier tirage du ticket
                        If (0 = CInt(ODsTicket.Tables("ticket").Rows(h).Item("parent_ticket")) And proch_tirage = CInt(ODsTicket.Tables("ticket").Rows(h).Item("NUM_TIRAGE"))) Then
                            need_cancel = True
                            ticket_to_cancel = ID_TICKET
                            Exit For
                        End If

                        ' on verifie si le ticket est gagant
                        ODsPari = oTicket.GetPari(ID_TICKET)

                        Gagner = False
                        For i As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(i).Item("ID_PARI").ToString)
                            pari_num = ""
                            pariGagner = oTicket.verifWinner(id_bookmaker, ODsPari.Tables("pari").Rows(i).Item("ID_PARI").ToString,
                                                                            ODsTicket.Tables("ticket").Rows(h).Item("NUM_TIRAGE").ToString)
                            If (pariGagner) Then
                                Gains += Math.Round((CDbl(ODsPari.Tables("pari").Rows(i).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(i).Item("QUOTA"))))
                            End If
                            Gagner = Gagner Or pariGagner
                        Next
                        ticket_gagnant = ticket_gagnant Or Gagner
                        If (Gagner) Then
                            need_to_print_gagn = need_to_print_gagn Or oTicket.ServirTicket(ID_TICKET, caissier)
                        End If

                    Next



                    If (deja_servi) Then
                        need_to_print = 0
                        message = "Ticket gagnant deja servit Par la caisse"
                        'MessageBox.Show(" Ticket gagnant deja servit Par la caisse N:" & ODsTicket.Tables("ticket").Rows(0).Item("ID_USER_S").ToString, "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return need_to_print & "/" & message
                    End If

                    'on verifie si le ticket avait deja été annuler
                    If (deja_annuler) Then
                        need_to_print = 0
                        message = "Ticket deja annuler"
                        'MessageBox.Show(" Ticket gagnant deja servit Par la caisse N:" & ODsTicket.Tables("ticket").Rows(0).Item("ID_USER_S").ToString, "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return need_to_print & "/" & message
                    End If

                    If (need_cancel) Then
                        ' il faut annuler le ticket
                        need_to_print = If(oTicket.annulerTicket(ticket_to_cancel), 1, 0)
                        message = "Ticket annuler"
                        Return need_to_print & "/" & message
                    Else

                        If (ticket_gagnant) Then

                            need_to_print = If(need_to_print_gagn, 1, 0)

                            message = Gains.ToString

                            Return need_to_print & "/" & message

                        Else
                            need_to_print = 0
                            message = "Ticket non Gagnant"
                            Return need_to_print & "/" & message

                        End If

                    End If

                Else
                    need_to_print = 0
                    message = " Ticket invalide."
                    Return need_to_print & "/" & message
                End If


            End Using

        End Function


        Function check_jackpot_tick(ByVal codebar As String, ByVal caissier As String) As String
            Dim ODsTicket As DataSet
            'Dim oDsNumJou As DataSet
            Dim ID_TICKET As String
            Dim id_bookmaker As String
            Dim Gains As String = "0"
            Dim deja_gagnant As Boolean = False
            Dim ticket_gagnant As Boolean = False
            Dim need_to_print_gagnan As Boolean = True
            Dim need_to_print As Integer = 0
            Dim message As String = ""

            Using oTicket As New BL_Lotto.BlTicket(source)
                Gains = "0"
                ODsTicket = oTicket.TicketParCodebar(codebar)

                If (ODsTicket.Tables("ticket").Rows.Count > 0) Then


                    For h As Integer = 0 To ODsTicket.Tables("ticket").Rows.Count - 1

                        ID_TICKET = ODsTicket.Tables("ticket").Rows(h).Item("ID_TICKET").ToString
                        id_bookmaker = ODsTicket.Tables("ticket").Rows(h).Item("bookmaker_id").ToString

                        'validation Jackpot
                        Dim dsjackpot As DataSet

                        Using ojacpot As New BL_Lotto.BlJackot(source)
                            dsjackpot = ojacpot.GetGagnantTicketJackpot(ID_TICKET)

                            If (dsjackpot.Tables("jackpot").Rows.Count > 0) Then
                                For p As Integer = 0 To dsjackpot.Tables("jackpot").Rows.Count - 1

                                    If (0 <> CInt(dsjackpot.Tables("jackpot").Rows(p).Item("ID_USER_S"))) Then
                                        deja_gagnant = True
                                        Continue For
                                    Else

                                        If CType(dsjackpot.Tables("jackpot").Rows(p).Item("is_materiel"), Boolean) Then
                                            Gains = dsjackpot.Tables("jackpot").Rows(p).Item("nom_materiel").ToString '& "|" & dsjackpot.Tables("jackpot").Rows(p).Item("montant").ToString
                                        Else
                                            Gains = " Jackpot: " & dsjackpot.Tables("jackpot").Rows(p).Item("montant").ToString

                                        End If
                                        ticket_gagnant = True
                                        need_to_print_gagnan = need_to_print_gagnan And oTicket.ServirTicketJack(ID_TICKET, caissier)

                                        message = If(message = "", Gains, message & "#" & Gains)

                                    End If

                                Next

                            Else
                                ticket_gagnant = ticket_gagnant Or False
                            End If
                        End Using

                    Next

                    If (ticket_gagnant) Then
                        need_to_print = 1
                        If Not need_to_print_gagnan Then
                            message = "Une erreur c'est produite veillez contacter la direction"
                        End If
                    Else
                        need_to_print = 0
                        If (deja_gagnant) Then
                            message = "Ticket gagnant Du Jackpot deja servit "
                        Else
                            message = "Aucun Jackpot."
                        End If

                    End If

                Else
                    need_to_print = 0
                    message = " Ticket invalide."
                End If

                Return need_to_print & "/" & message

            End Using

        End Function
        Function UserComptbility(ByVal user_id As String, ByVal id_bookmaker As String, ByVal datedebut As String, ByVal datefin As String) As String

            Dim dsTicket, dsTicketJack, dsTicketAnnul, dsTickJackMon, dsTouticket, dsPari, ODsPari, dsTirage, dsCreditdebit As DataSet
            Dim MontantSortie, nbticketGagnant As Integer
            nbticketGagnant = 0
            MontantSortie = 0
            Dim Gagner As Boolean = False
            Dim Gains As Integer = 0
            Dim NbTicket, NbTirage, TotalMise, NbtTicketJack As String
            Dim TotalmontantJack As Integer = 0
            Dim totaltickannuller As Integer = 0
            Dim TotalCreditDebit As Integer = 0

            Dim id_ticket As String
            Dim row0 As String = ""
            NbTicket = "0"
            TotalMise = NbTicket
            NbTirage = NbTicket
            NbtTicketJack = NbTicket

            Using oCompte As New BL_Lotto.BlCompte(source)
                If user_id <> "0" Then

                    dsTicketJack = oCompte.getStatTicketJackParUser(user_id, datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTickJackMon = oCompte.getStatTickJackMonParUser(user_id, datedebut & " 00:00:00", datefin & " 23:59:59")

                    dsTicket = oCompte.getStatTicketParUser(user_id, datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTicketAnnul = oCompte.getStatTickAnnulParUser(user_id, datedebut & " 00:00:00", datefin & " 23:59:59")

                    dsPari = oCompte.getStatPariParUser(user_id, datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsCreditdebit = oCompte.getStatCreditParUser(user_id, datedebut & " 00:00:00", datefin & " 23:59:59")
                    'calcul du nombre de ticket gagnant
                    dsTouticket = oCompte.getListTicketServi(user_id, datedebut & " 00:00:00", datefin & " 23:59:59")

                Else
                    dsTicketJack = oCompte.getStatTicketJack(datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTickJackMon = oCompte.getStatTickJackMon(datedebut & " 00:00:00", datefin & " 23:59:59")


                    dsTicket = oCompte.getStatTicket(id_bookmaker, datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTicketAnnul = oCompte.getStatTickAnnulBookmaker(id_bookmaker, datedebut & " 00:00:00", datefin & " 23:59:59")

                    dsPari = oCompte.getStatPariBook(id_bookmaker, datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsCreditdebit = oCompte.getStatCreditBookmaker(id_bookmaker, datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTouticket = oCompte.getListTicketBookMaker(id_bookmaker, datedebut & " 00:00:00", datefin & " 23:59:59")
                End If

                dsTirage = oCompte.getStatTirage(datedebut & " 00:00:00", datefin & " 23:59:59")

                If (dsTickJackMon.Tables("ticket").Rows.Count > 0) Then
                    If (Not dsTickJackMon.Tables("ticket").Rows(0).IsNull("TOTAL_MONTANT")) Then
                        TotalmontantJack = CInt(dsTickJackMon.Tables("ticket").Rows(0).Item("TOTAL_MONTANT"))
                    Else
                        TotalmontantJack = 0
                    End If
                End If

                If (dsTicketAnnul.Tables("ticket").Rows.Count > 0) Then
                    If (Not dsTicketAnnul.Tables("ticket").Rows(0).IsNull("NB_TICKET")) Then
                        totaltickannuller = CInt(dsTicketAnnul.Tables("ticket").Rows(0).Item("NB_TICKET"))
                    Else
                        totaltickannuller = 0
                    End If
                End If

                If (dsCreditdebit.Tables("credit").Rows.Count > 0) Then
                    If (Not dsCreditdebit.Tables("credit").Rows(0).IsNull("TOTALCREDIT")) Then
                        TotalCreditDebit = CInt(dsCreditdebit.Tables("credit").Rows(0).Item("TOTALCREDIT"))
                    Else
                        TotalCreditDebit = 0
                    End If
                End If

                If (dsTirage.Tables("tirage").Rows.Count > 0) Then
                    NbTirage = dsTirage.Tables("tirage").Rows(0).Item("NB_TIRAGE").ToString
                Else
                    NbTirage = "0"
                End If

                If (dsTicketJack.Tables("ticket").Rows.Count > 0) Then
                    NbtTicketJack = dsTicketJack.Tables("ticket").Rows(0).Item("NB_TICKET").ToString
                End If

                If (CInt(dsTicket.Tables("ticket").Rows(0).Item("NB_TICKET")) > 0) Then

                    NbTicket = dsTicket.Tables("ticket").Rows(0).Item("NB_TICKET").ToString
                    TotalMise = dsPari.Tables("pari").Rows(0).Item("TOTALMISE").ToString


                    Using oTicket As New BL_Lotto.BlTicket(source)
                        For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
                            id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                            Dim oDsNumJou As DataSet

                            ODsPari = oTicket.GetPari(id_ticket)
                            Gagner = False
                            Gains = 0
                            'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                            If (ODsPari.Tables("pari").Rows.Count > 0) Then

                                For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                                    'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                                    If (oTicket.verifWinner(id_bookmaker, ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                                    dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)) Then
                                        Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                                    End If

                                    Gagner = Gagner Or oTicket.verifWinner(id_bookmaker, ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                                dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                                Next

                            End If

                            oDsNumJou = oTicket.getNumJouer(id_ticket)
                            If (Gagner) Then
                                nbticketGagnant += 1
                                MontantSortie += CInt(Gains)
                            End If

                        Next
                    End Using

                    MontantSortie += TotalmontantJack
                End If


                Return NbtTicketJack & "/" & NbTicket & "/" & TotalMise & "/" & NbTirage & "/" & nbticketGagnant & "/" & MontantSortie & "/" & totaltickannuller & "/" & TotalCreditDebit '& "/" & row0
            End Using
        End Function

    End Class




    Sub Broadcast(ByVal Message As String)

        'Écrit le message dans la console et l'envoie à tous les clients connectés
        Console.WriteLine("BROADCAST : " & Message)
        For Each Cli In ListeClients
            Cli.EnvoiMessage(Message)
        Next

    End Sub






End Module
