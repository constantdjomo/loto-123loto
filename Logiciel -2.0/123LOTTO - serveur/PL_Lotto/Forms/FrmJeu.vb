﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports System.Timers
Imports System.ComponentModel
Imports System.Globalization
Imports PL_Lotto.My
Imports BL_Lotto
Imports DirectShowLib

Public Class FrmJeu


    Private oTirage As BL_Lotto.BlTirage
    Private oTicketTi As BL_Lotto.BlTicket
    Private oBltriche As BL_Lotto.BlTriche
    'Private Const Delai_JEu = 60 '300
    Private Const Delai_demarage_tir = 10 '10
    Private Const DureeAnim = 27
    Private Const MaxBoul = 90
    Private Const miseMinimum = 100
    Private Const NbBoulTirag = 5
    Private Const Delai_anim_jackpot = 60000 ' 3min (1s = 1000 miliseconde )
    Private p As Point = New Point(136, 39)

    Private TabDelaiGagnant As Integer() = {My.Settings.DELAI_JEU, My.Settings.DELAI_JEU} ' 5 min; 10 min; 15 h;

    Private Tir_cour_jack As Integer

    Public ThreadClient, ThreadAnim, ThreadCaisse As Thread
    Public bookMakAnimThread As New List(Of Thread)
    Public MonSocketServeur, MonSockPourCaisse As Socket



    Dim dsti As DataSet
    Dim d As DateTime = DateTime.Now



    Dim time As Integer
    Dim DElaiGains As Integer = 0
    Dim tab1(5), tab2(5), tab3(5), tab4(5), tab5(5) As Integer
    Dim min, sec As Double


    Private Sub FrmJeu_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Dispose()
    End Sub

    'Public Sub initialiseEcartGagnant()
    '    Dim ind As Integer()
    '    ind = CType(GetRandomDistinct(1, TabDelaiGagnant.Length), Integer())
    '    DELAI_AV_GAINS = TabDelaiGagnant(ind(0))
    '    DElaiGains = DELAI_AV_GAINS
    'End Sub

    Public Sub GererCaisse()
        'Crée le socket et l'IP EP
        MonSockPourCaisse = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        Dim MonEP As IPEndPoint = New IPEndPoint(IPAddress.Any, CInt(My.Settings.PORT))

        ListeCaisses = New List(Of Client) 'Initialise la liste

        MonSockPourCaisse.Bind(MonEP) 'Lie le socket à cette IP
        MonSockPourCaisse.Listen(1) 'Se met en écoute

        'Console.WriteLine("Socket serveur initialisé sur le port " & port)

        While True 'Boucle à l'infini
            'Console.WriteLine("En attente d'un client.")
            'Se met en attente de connexion et appelle TraitementConnexion() lors d'une connexion.
            Dim SocketEnvoi As Socket = MonSockPourCaisse.Accept() 'Bloquant tant que pas de connexion
            TraitementConnexionCaisse(SocketEnvoi) 'Traite la connexion du client
        End While
    End Sub

    Sub TraitementConnexionCaisse(ByVal SocketEnvoi As Socket)
        'Console.WriteLine("Socket client connecté, création d'un thread.")
        Dim NouveauClient As New Client(SocketEnvoi) 'Crée une instance de « client »
        ListeCaisses.Add(NouveauClient) 'Ajoute le client à la liste
        'Crée un thread pour traiter ce client et le démarre
        Dim ThreadCaisse As New Thread(AddressOf NouveauClient.TraitementCaisse)
        ThreadCaisse.IsBackground = True
        ThreadCaisse.Start()
    End Sub



    'Public Sub majforJack()
    '    Using ojacpot As New BL_Lotto.BlJackot(source)
    '        Dim dstsilver, dstgold, dstiron As New DataSet

    '        dstsilver = ojacpot.GetJackpot(ID_SILVER)
    '        lbelSilver_name.Text = dstsilver.Tables("jackpot").Rows(0).Item("nom_jackp").ToString
    '        PictureBox6.ImageLocation = Application.StartupPath & "\photo\" & dstsilver.Tables("jackpot").Rows(0).Item("photo").ToString
    '        PictureBox6.Refresh()
    '        If (CBool(dstsilver.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
    '            lbelSilver.Text = dstsilver.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
    '            'lbelSilver.Text = ojacpot.GetMontStatutJack(ID_SILVER) & " FCFA"
    '        Else
    '            'lbelSilver.Text = ojacpot.GetMontStatutJack(ID_SILVER) & " FCFA"
    '            lbelSilver.Text = ojacpot.GetMontantJackpot(ID_SILVER) & " FCFA"
    '        End If

    '        dstgold = ojacpot.GetJackpot(ID_GOLD)
    '        gpot_name_lbl.Text = dstgold.Tables("jackpot").Rows(0).Item("nom_jackp").ToString
    '        PictureBox2.ImageLocation = Application.StartupPath & "\photo\" & dstgold.Tables("jackpot").Rows(0).Item("photo").ToString
    '        PictureBox2.Refresh()
    '        If (CBool(dstgold.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
    '            gpot_lbl.Text = dstgold.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
    '            'lbelSatGold.Text = ojacpot.GetMontStatutJack(ID_GOLD) & " FCFA"
    '        Else
    '            gpot_lbl.Text = ojacpot.GetMontantJackpot(ID_GOLD) & " FCFA"
    '            'lbelSatGold.Text = "--- --- ----"
    '        End If

    '        ' iRON 
    '        dstiron = ojacpot.GetJackpot(ID_IRON)
    '        Lb_iron_pot_name.Text = dstiron.Tables("jackpot").Rows(0).Item("nom_jackp").ToString
    '        PictureBox5.ImageLocation = Application.StartupPath & "\photo\" & dstiron.Tables("jackpot").Rows(0).Item("photo").ToString
    '        PictureBox5.Refresh()
    '        If (CBool(dstiron.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
    '            Lb_iron_pot.Text = dstiron.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
    '            'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"
    '            'lbelSatGold.Text = "--- --- ----"
    '        Else
    '            Lb_iron_pot.Text = ojacpot.GetMontantJackpot(ID_IRON) & " FCFA"
    '            'Lb_iron_pot.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"
    '            'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"
    '            'lbelSatGold.Text = "--- --- ----"
    '        End If
    '    End Using
    'End Sub
    'Delegate Sub dmajforJack()



    'Public Sub majSatutforJack(ByRef tes As Integer)
    '    Using ojacpot As New BL_Lotto.BlJackot(source)
    '        Dim dstsilver, dstgold, dstiron As New DataSet

    '        dstsilver = ojacpot.GetJackpot(ID_SILVER)

    '        If ((ID_TICKET_SILVER(0) = "0") OrElse (tes = 1)) Then

    '            'lbelSatSilver.Text = ojacpot.GetMontStatutJack(ID_SILVER) & " FCFA"

    '            If (CBool(dstsilver.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
    '                lbelSilver.Text = dstsilver.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
    '                'lbelSilver.Text = ojacpot.GetMontStatutJack(ID_SILVER) & " FCFA"
    '            Else
    '                lbelSilver.Text = ojacpot.GetMontantJackpot(ID_SILVER) & " FCFA"
    '                'lbelSilver.Text = ojacpot.GetMontStatutJack(ID_SILVER) & " FCFA"
    '            End If

    '        Else
    '            'silpot_lbl.Text = ID_TICKET_SILVER
    '            lbelSilver.Text = ID_TICKET_SILVER(0)
    '        End If

    '        If ((ID_TICKET_IRON(0) = "0") OrElse (tes = 1)) Then

    '            dstiron = ojacpot.GetJackpot(ID_IRON)

    '            If (CBool(dstiron.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
    '                Lb_iron_pot.Text = dstiron.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
    '                'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"

    '            Else
    '                Lb_iron_pot.Text = ojacpot.GetMontantJackpot(ID_IRON) & " FCFA"
    '                'Lb_iron_pot.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"
    '                'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"

    '            End If

    '        Else

    '            'Lb_iron_pot.Text = ID_TICKET_IRON
    '        End If

    '        If ((ID_TICKET_GOLG(0) = "0") OrElse (tes = 1)) Then

    '            dstgold = ojacpot.GetJackpot(ID_GOLD)

    '            If (CBool(dstgold.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
    '                gpot_lbl.Text = dstgold.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
    '                'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"
    '            Else
    '                gpot_lbl.Text = ojacpot.GetMontantJackpot(ID_GOLD) & " FCFA"
    '                'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"

    '            End If

    '        Else

    '            gpot_lbl.Text = ID_TICKET_GOLG(0)
    '        End If

    '    End Using
    'End Sub


    Private Sub FrmJeu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' element retirer du formulaire principale apres la connexion
        initialiseSetting()

        'Me.SetStyle(ControlStyles.AllPaintingInWmPaint Or ControlStyles.DoubleBuffer Or ControlStyles.ResizeRedraw Or ControlStyles.UserPaint, True)
        time = My.Settings.DELAI_JEU
        'majforJack()
        'Crée un thread pour traiter ce client et le démarre
        'ThreadClient = New Thread(AddressOf GererClient)
        'ThreadClient.Start()

        'demarrage du thread pour gerer les caisses
        ThreadCaisse = New Thread(AddressOf GererCaisse)
        ThreadCaisse.IsBackground = True
        ThreadCaisse.Start()


        Tir_No = lastTir_No + 1

        Timer1.Interval = 1000 'Timer1_Tick sera déclenché toutes les secondes.
        Timer1.Start()  'On démarre le Timer

        'initialiseEcartGagnant() 'initialisation au meme moment du delai avant prochain gains
        TirageLancer = 0
        LB_NumTir.Text = Tir_No.ToString : LB_NumTir.Refresh()

        Lb_Min.Text = 0.ToString
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        'If (Not animaDemarer) Then
        '    min = Math.Truncate(time / 60)
        '    sec = ((time / 60) - (Math.Truncate(time / 60))) * 60
        '    LB_second.Text = CInt(sec).ToString : LB_second.Refresh()
        '    Lb_Min.Text = min.ToString : Lb_Min.Refresh()
        '    time -= 1
        '    DElaiGains -= 1

        '    If (time Mod 10) = 0 Then
        '        majSatutforJack(1)
        '    Else
        '        majSatutforJack(0)
        '    End If
        'End If
    End Sub

    Private Sub Timer1_Tick(ByVal ByValsender As Object, ByVal ByVale As System.EventArgs) Handles Timer1.Tick

        'chronometre
        If (Not animaDemarer) Then
            time -= 1
            DElaiGains -= 1
            min = Math.Truncate(time / 60)
            sec = ((time / 60) - (Math.Truncate(time / 60))) * 60
            LB_second.Text = CInt(sec).ToString : LB_second.Refresh()
            Lb_Min.Text = min.ToString : Lb_Min.Refresh()

            timetosend = time.ToString
        End If

        If (min <= 0 And sec <= 10) And (Not animaDemarer) Then
            TirageLancer = 1
        End If

        'la condition est verifié apres 5min et on entre dans la boucle
        If (min <= 0 And sec <= 0) And (Not animaDemarer) Then
            bookMakAnimThread.ForEach(Sub(ele) If ele.IsAlive() Then ele.Abort())
            bookMakAnimThread.Clear()
            'on initialise la variable de com
            initialiseSetting()
            TirageLancer = 1
            animaDemarer = True
            Using oUser As New BL_Lotto.BlUser(source)
                list_bookmaker = oUser.getBookMakerlist()
                If (list_bookmaker.Tables("bookmaker").Rows.Count > 0) Then
                    Using oTirage As New BL_Lotto.BlTirage(source)
                        oTirage.AjouterTirage(Tir_No.ToString)
                    End Using

                    For i As Integer = 0 To list_bookmaker.Tables("bookmaker").Rows.Count - 1

                        Dim bookthread As New Thread(New ParameterizedThreadStart(AddressOf GererAnimation)) With {.IsBackground = True}
                        bookthread.Start(New Object() {list_bookmaker.Tables("bookmaker").Rows(i).Item("bookmaker_id"), Tir_No})
                        bookMakAnimThread.Add(bookthread)
                    Next
                End If
            End Using
            ' TODO: Probablement ajouter un sleep ici 

            'pour controler qu'on demare bien a 0
            If (CInt(LB_second.Text) > 0) Then
                Thread.Sleep(CInt(LB_second.Text) * 1000)
            End If
            Thread.Sleep(10000)

            time = My.Settings.DELAI_JEU
            Tir_No += 1
            LB_NumTir.Text = Tir_No.ToString : LB_NumTir.Refresh()
            TirageLancer = 0
            animaDemarer = False

            'ThreadAnim.SetApartmentState(Threading.ApartmentState.STA)
            'ThreadAnim.Start()

        End If

    End Sub

    Public Sub enegistrerTirage(ByVal id_bookmaker As String, ByVal tirage_id As String, ByVal table() As Integer)

        Using oTirage As New BL_Lotto.BlTirage(source)
            oTirage.SaveBookMakerTirage(id_bookmaker, tirage_id, table)
        End Using

    End Sub
    Private Sub GererAnimation(ByVal param_obj As Object)

        Dim indice(4) As Integer
        Dim random As New Random
        Dim d1 As DateTime = DateTime.Now
        Dim PreTirageList As Array
        Dim numeroToWin As Array
        Dim ListTirage(MaxBoul) As Integer
        Dim ListWinTemp(MaxBoul) As Integer
        Dim ListWin(MaxBoul) As Integer
        Dim param As Array = CType(param_obj, Array)
        Dim id_bookmaker As String = param(0).ToString
        Dim book_num_tir As Integer = CInt(param(1))

        Dim BOOKMAKER As Hashtable = CType(bookmaker_data(id_bookmaker), Hashtable)
        Dim Resultat(4) As Integer

        Dim TotalEntrer, TotalEntrerSolo, TotalEntrerDoublet As ULong
        Dim TotalBenef, TotalBenefSolo, TotalBenefDoublet As ULong
        Dim prelSilverpot, prelSilverpotSolo, prelSilverpotDoub,
        prelevGoldpot, prelevGoldpotSolo, prelevGoldpotDoubl,
        prelevIrondpot, prelevIronpotSolo, prelevIronpotDoubl,
        prelevCagnotedpot, prelevCagnotepotSolo, prelevCagnotepotDoubl,
        prelevMsteriousPot, prelevMsteriousPotSolo, prelevMsteriousPotDoubl,
        prelevMegaPot, prelevMegaPotSolo, prelevMegaPotDoubl As ULong

        Dim TotalmontantJack, GainsSolo, GainsDoublet As ULong

        'NumTir = CInt(LB_NumTir.Text)
        Dim cpt As Integer
        Dim dsPariSolo, dsPariDoublet, dsTickJackMon As DataSet
        Dim Tabdatedebut() As String
        Dim datedebut, datefin As String
        Dim tirage As String = "0*0*0*0*0"

        'If (DElaiGains <= 0) Then

        ' il faut un gagnant

        ' on calcul les entrers 

        ' Intervalle de calcul fixer pour le mois

        Tabdatedebut = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
        datefin = Tabdatedebut(2) & "/" & Tabdatedebut(1) & "/" & Date.DaysInMonth(Date.Now.Year, Date.Now.Month).ToString
        'datefin = Tabdatedebut(2) & "/" & Tabdatedebut(1) & "/" & Tabdatedebut(0)

        datedebut = Tabdatedebut(2) & "/" & Tabdatedebut(1) & "/" & "01" & " " & "00:00:00"
        'datedebut = Tabdatedebut(2) & "/" & Tabdatedebut(1) & "/" & Tabdatedebut(0) & " " & "00:00:00"
        datefin = datefin & " " & "23:59:59"

        Using oCompte As New BL_Lotto.BlCompte(source)
            'check if there were no ticket in the 
            dsPariSolo = oCompte.getStatPariSolo(id_bookmaker, datedebut, datefin)
            dsPariDoublet = oCompte.getStatPariDoublet(id_bookmaker, datedebut, datefin)


            If (Not dsPariSolo.Tables("pari").Rows(0).IsNull("TOTALMISE")) Then
                TotalEntrerSolo = CType(dsPariSolo.Tables("pari").Rows(0).Item("TOTALMISE"), ULong)
            Else
                TotalEntrerSolo = 0
            End If


            If (Not dsPariDoublet.Tables("pari").Rows(0).IsNull("TOTALMISE")) Then
                TotalEntrerDoublet = CType(dsPariDoublet.Tables("pari").Rows(0).Item("TOTALMISE"), ULong)
            Else
                TotalEntrerDoublet = 0
            End If
            TotalEntrer = TotalEntrerSolo + TotalEntrerDoublet


            'on preleve le pourcentage prevue
            If (TotalEntrer = 0) Then
                PreTirageList = triche(id_bookmaker, book_num_tir)
                ListTirage = CType(PreTirageList, Integer())
                cpt = PreTirageList.Length
            Else

                'on preleve les jackpot
                Dim monPariensolo, monParienDouble As ULong
                Using ojacpot As New BL_Lotto.BlJackot(source)
                    monPariensolo = CULng(ojacpot.GetMntPariEncourBookmaker(id_bookmaker, book_num_tir.ToString, "1"))
                    monParienDouble = CULng(ojacpot.GetMntPariEncourBookmaker(id_bookmaker, book_num_tir.ToString, "2"))
                End Using

                ' For every Jackpot we comute the amount the have to grow

                'silver jackpot
                prelSilverpotSolo = CType((CInt(BOOKMAKER("SILVER_POT")) / 100) * monPariensolo, ULong)
                prelSilverpotDoub = CType((CInt(BOOKMAKER("SILVER_POT")) / 100) * monParienDouble, ULong)
                prelSilverpot = prelSilverpotSolo + prelSilverpotDoub

                'Gold jacpot
                prelevGoldpotSolo = CType((CInt(BOOKMAKER("GOLD_POT")) / 100) * monPariensolo, ULong)
                prelevGoldpotDoubl = CType((CInt(BOOKMAKER("GOLD_POT")) / 100) * monParienDouble, ULong)
                prelevGoldpot = prelevGoldpotSolo + prelevGoldpotDoubl

                'iron jacpot
                prelevIronpotSolo = CType((CInt(BOOKMAKER("IRON_POT")) / 100) * monPariensolo, ULong)
                prelevIronpotDoubl = CType((CInt(BOOKMAKER("IRON_POT")) / 100) * monParienDouble, ULong)
                prelevIrondpot = prelevIronpotSolo + prelevIronpotDoubl

                'cagnote
                prelevCagnotepotSolo = CType((CInt(BOOKMAKER("CAGNOTE_POT")) / 100) * monPariensolo, ULong)
                prelevCagnotepotDoubl = CType((CInt(BOOKMAKER("CAGNOTE_POT")) / 100) * monParienDouble, ULong)
                prelevCagnotedpot = prelevCagnotepotSolo + prelevCagnotepotDoubl

                'MSTERIOUS
                prelevMsteriousPotSolo = CType((CInt(BOOKMAKER("MSTERIOUS_POT")) / 100) * monPariensolo, ULong)
                prelevMsteriousPotDoubl = CType((CInt(BOOKMAKER("MSTERIOUS_POT")) / 100) * monParienDouble, ULong)
                prelevMsteriousPot = prelevMsteriousPotSolo + prelevMsteriousPotDoubl

                'Mega Jackpot
                prelevMegaPotSolo = CType((CInt(BOOKMAKER("MEGA_POT")) / 100) * monPariensolo, ULong)
                prelevMegaPotDoubl = CType((CInt(BOOKMAKER("MEGA_POT")) / 100) * monParienDouble, ULong)
                prelevMegaPot = prelevMegaPotSolo + prelevMegaPotDoubl


                'ici on enregistre les modification fait sur les jackpot
                ' c'est uniquement les lot commun qui sont affecter ici
                Using ojacpot As New BL_Lotto.BlJackot(source)
                    'Dim listsalle As New DataSet
                    'listsalle = ojacpot.Getsalle(id_bookmaker)
                    'Dim nbsalle As Integer
                    'If (listsalle.Tables("point_vente").Rows.Count > 2) Then
                    '    nbsalle = CInt(listsalle.Tables("point_vente").Rows.Count) - 2
                    'Else
                    '    nbsalle = 1
                    'End If

                    ' on repartie les part dans chaque salle
                    'ojacpot.Maj_Satut_Jackpot(ID_SILVER, CType(prelSilverpot / nbsalle, ULong).ToString)
                    'ojacpot.Maj_Satut_Jackpot(ID_GOLD, CType(prelevGoldpot / nbsalle, ULong).ToString)
                    ojacpot.Maj_Satut_Jackpot(id_bookmaker, BOOKMAKER("ID_SILVER").ToString, CType(prelSilverpot, ULong).ToString)
                    ojacpot.Maj_Satut_Jackpot(id_bookmaker, BOOKMAKER("ID_GOLD").ToString, CType(prelevGoldpot, ULong).ToString)

                    ' on le fait ici parceque Boro veut un lot unique pour toute les salle (partager)
                    ojacpot.Maj_Satut_Jackpot(id_bookmaker, BOOKMAKER("ID_IRON").ToString, CType(prelevIrondpot, ULong).ToString)

                    ojacpot.Maj_Satut_Jackpot(id_bookmaker, BOOKMAKER("ID_CAGNOTE").ToString, CType(prelevCagnotedpot, ULong).ToString)

                    ojacpot.Maj_Satut_Jackpot(id_bookmaker, BOOKMAKER("ID_MSTERIOUS").ToString, CType(prelevMsteriousPot, ULong).ToString)

                    ojacpot.Maj_Satut_Jackpot(id_bookmaker, BOOKMAKER("ID_MEGA").ToString, CType(prelevMegaPot, ULong).ToString)
                End Using


                'il faut a present gerer les autres lots individuels

                'Dim monPariensoloPv, monParienDoublePv, monPariPv As ULong
                'Dim dspv As New DataSet
                'Dim refpv As String
                'Using ojacpot As New BL_Lotto.BlJackot(source)
                '    dspv = ojacpot.GetListPv()
                '    For i As Integer = 0 To dspv.Tables("point_vente").Rows.Count - 1
                '        refpv = dspv.Tables("point_vente").Rows(0).Item("id_pv")
                '        monPariensoloPv = CULng(ojacpot.GetMontantPariEncour(tir_no.ToString, "1", refpv))
                '        monParienDoublePv = CULng(ojacpot.GetMontantPariEncour(tir_no.ToString, "2", refpv))
                '        monPariPv = monPariensoloPv + monParienDoublePv

                '        'silver jackpot
                '        prelSilverpotSoloPv = CType((SILVER_POT / 100) * monPariensoloPv, ULong)
                '        prelSilverpotDoubPv = CType((SILVER_POT / 100) * monParienDoublePv, ULong)
                '        prelSilverpotPv = prelSilverpotSoloPv + prelSilverpotDoubPv

                '        'Gold jacpot
                '        prelevGoldpotSoloPv = CType((GOLD_POT / 100) * monPariensoloPv, ULong)
                '        prelevGoldpotDoublPv = CType((GOLD_POT / 100) * monParienDoublePv, ULong)
                '        prelevGoldpotPv = prelevGoldpotSoloPv + prelevGoldpotDoublPv

                '        'iron jacpot
                '        prelevIronpotSolo = CType((IRON_POT / 100) * monPariensolo, ULong)
                '        prelevIronpotDoubl = CType((IRON_POT / 100) * monParienDouble, ULong)
                '        prelevIrondpotPv = prelevIronpotSolo + prelevGoldpotDoubl


                '        ojacpot.Maj_Satut_Jackpot(ID_SILVER, prelSilverpotPv.ToString, refpv)
                '        ojacpot.Maj_Satut_Jackpot(ID_GOLD, prelevGoldpotPv.ToString, refpv)
                '        ojacpot.Maj_Satut_Jackpot(ID_IRON, prelevIrondpotPv.ToString, refpv)
                '    Next

                'End Using

                Dim POURVENTAGE_PRELEVER As Integer

                If (CBool(BOOKMAKER("DEDUCT_JACK_TO_TURN_OVER"))) Then
                    POURVENTAGE_PRELEVER = CInt(BOOKMAKER("DIFICULT_LEVEL")) + CInt(BOOKMAKER("SILVER_POT")) + CInt(BOOKMAKER("GOLD_POT")) + CInt(BOOKMAKER("IRON_POT")) + CInt(BOOKMAKER("CAGNOTE_POT")) + CInt(BOOKMAKER("MSTERIOUS_POT")) + CInt(BOOKMAKER("MEGA_POT"))
                Else
                    POURVENTAGE_PRELEVER = CInt(BOOKMAKER("DIFICULT_LEVEL"))
                End If



                TotalBenef = CType((POURVENTAGE_PRELEVER / 100) * TotalEntrer, ULong)
                TotalBenefSolo = CType((POURVENTAGE_PRELEVER / 100) * TotalEntrerSolo, ULong)
                TotalBenefDoublet = CType((POURVENTAGE_PRELEVER / 100) * TotalEntrerDoublet, ULong)

                Dim resteAgagner As Long = CLng(TotalEntrer - TotalBenef)
                Dim resteAgagnerSolo As Long = CLng(TotalEntrerSolo - TotalBenefSolo)
                Dim resteAgagnerDoublet As Long = CLng(TotalEntrerDoublet - TotalBenefDoublet)


                ' le reste on retire le montant des tickets deja gagnants ensuite
                'GainsActuelClient = MontantGagnerActuel(id_bookmaker, datedebut, datefin)
                GainsSolo = GainsActuelSolo(id_bookmaker, book_num_tir, datedebut, datefin)
                GainsDoublet = GainsActuelDoublet(id_bookmaker, book_num_tir, datedebut, datefin)

                ' le reste on retire le montant des tickets deja gagnants du jackpot
                dsTickJackMon = oCompte.getMontTickJack_Bookmaker(id_bookmaker, datedebut, datefin)
                If (dsTickJackMon.Tables("ticket").Rows.Count > 0) Then
                    If (Not dsTickJackMon.Tables("ticket").Rows(0).IsNull("TOTAL_MONTANT")) Then
                        TotalmontantJack = CULng(dsTickJackMon.Tables("ticket").Rows(0).Item("TOTAL_MONTANT"))
                    Else
                        TotalmontantJack = 0
                    End If
                End If

                'resteAgagner = resteAgagner - CLng(GainsActuelClient) - CLng(TotalmontantJack)
                'resteAgagner = resteAgagner - CLng(GainsActuelClient)
                If (CBool(BOOKMAKER("DEDUCT_JACK_TO_TURN_OVER"))) Then
                    resteAgagnerSolo = resteAgagnerSolo - CLng(GainsSolo) - CLng(TotalmontantJack / 2)

                    resteAgagnerDoublet = resteAgagnerDoublet - CLng(GainsDoublet) - CLng(TotalmontantJack / 2)
                Else
                    resteAgagnerSolo = resteAgagnerSolo - CLng(GainsSolo)

                    resteAgagnerDoublet = resteAgagnerDoublet - CLng(GainsDoublet)
                End If


                'Boost le jeu 



                ' on preleve les tickets succeptibles de gagner
                Dim tabperdant, tabgagnant, totalnum, totalnumper As New List(Of Integer)
                Dim tabperdantSolo, tabgagnantSolo As New List(Of Integer)
                Dim tabperdantDoublet, tabgagnantDoublet As New List(Of Integer)


                'etape necessaire pour augmenter l'argent en caisse pour un moment
                resteAgagnerSolo = If(monPariensolo > 0, lotoBoost(id_bookmaker, "1", resteAgagnerSolo, datedebut, datefin), resteAgagnerSolo)
                resteAgagnerDoublet = If(monParienDouble > 0, lotoBoost(id_bookmaker, "2", resteAgagnerDoublet, datedebut, datefin), resteAgagnerDoublet)

                ' To take smoothly the money of player we assume that we always repay at leat 50% of the input of the actual round
                If monPariensolo * (My.Settings.ROUND_MIN_REDISTRIBUTION / 100) > resteAgagnerSolo Then
                    resteAgagnerSolo = CLng(monPariensolo * (My.Settings.ROUND_MIN_REDISTRIBUTION / 100))
                End If
                If monParienDouble * (My.Settings.ROUND_MIN_REDISTRIBUTION / 100) > resteAgagnerDoublet Then
                    resteAgagnerDoublet = CLng(monParienDouble * (My.Settings.ROUND_MIN_REDISTRIBUTION / 100))
                End If

                ' when ever a emergency stop is needed EMERGENCY_STOP
                If (BOOKMAKER("EMERGENCY_STOP") = "1") Then
                    resteAgagnerSolo = -99999999
                    resteAgagnerDoublet = -99999999
                End If


                'tabgagnant = ScinderGagnant(resteAgagner, datedebut, datefin, tabperdant)
                tabgagnantSolo = ScinderGagnantPerdant(id_bookmaker, book_num_tir, "1", resteAgagnerSolo, datedebut, datefin, tabperdantSolo) ' 1 => solo
                tabgagnantDoublet = ScinderGagnantPerdant(id_bookmaker, book_num_tir, "2", resteAgagnerDoublet, datedebut, datefin, tabperdantDoublet) ' 2 => doublet
                'MsgBox("gagnant solo" & tabgagnantSolo.Count & " perdant " & tabperdantSolo.Count)

                'fusionner les tableau gagnant
                tabgagnant = tabgagnantSolo
                tabgagnant.AddRange(tabgagnantDoublet)

                'fusionner les tableau de perdant
                tabperdant = tabperdantSolo
                tabperdant.AddRange(tabperdantDoublet)


                PreTirageList = removeNumeroPerdant(tabperdant)

                numeroToWin = getNumeroGagnant(tabgagnant)



                'MsgBox(" nb gagnant perdant " & table1.Length)

                ListWin = CType(numeroToWin, Integer())
                ListWinTemp = CType(PreTirageList, Integer())
                totalnum.Clear()
                totalnumper.Clear()

                totalnumper.AddRange(ListWinTemp)
                totalnumper = totalnumper.Distinct.ToList

                totalnum.AddRange(ListWin)

                totalnum = totalnum.Distinct.ToList

                'on doit verifier qu'il ya pas un doublet entre perdant et gagnant
                ' on retire les numero gagnant qui ne peuvent pas gagner car pas present sur la pre liste
                totalnum.RemoveAll(Function(i) Not totalnumper.Contains(i))



                If totalnum.Count < NbBoulTirag Then
                    'MsgBox(" avant d'entrer" & ListWinTemp.Length)

                    Dim ra As New Random()
                    ListWinTemp = ListWinTemp.OrderBy(Function(e) ra.Next()).ToArray()
                    Dim j As Integer = 0
                    'MsgBox("taille " & q.Length)
                    Do

                        If Not totalnum.Contains(ListWinTemp(j)) Then
                            totalnum.Add(ListWinTemp(j))
                        End If
                        j += 1

                    Loop Until totalnum.Count = 5 Or j >= ListWinTemp.Length

                End If
                'MsgBox("dans le for " & totalnum.Count)
                PreTirageList = totalnum.ToArray
            End If
        End Using

        ' on prend 1 ou plusieurs tickets qu'on fait gagner

        ListTirage = CType(PreTirageList, Integer())
        cpt = PreTirageList.Length


        'tirage 50% de chance
        'MsgBox(indice.Length)

        Resultat = ListTirage.OrderBy(Function(e) random.Next()).ToList().GetRange(0, NbBoulTirag).ToArray()

        'enregistrement des tirage dans la base de donnee
        enegistrerTirage(id_bookmaker, book_num_tir.ToString, Resultat)
        tirage = ""

        If Resultat.Length > 0 Then
            tirage = Resultat(0).ToString
        End If

        For k As Integer = 1 To Resultat.Length - 1
            tirage &= "*" & Resultat(k).ToString
        Next



        '    
        Me.Invoke(New dAnimation(AddressOf Animantion), id_bookmaker, Resultat, book_num_tir.ToString)

            '    'verification des jackpot

            Tir_cour_jack = book_num_tir
        'ID_TICKET_GOLG = "0"
        'ID_TICKET_SILVER = "0"
        'ID_TICKET_IRON = "0"

        Using ojacpot As New BL_Lotto.BlJackot(source)
            Dim dlistlot As New DataSet
            Dim st As String
            Dim ID_TICKET_GOLG_T, ID_TICKET_SILVER_T, ID_TICKET_IRON_T, ID_TICKET_CAGNOTE_T, ID_TICKET_MSTERIOUS_T, ID_TICKET_MEGA_T As String()
            Dim TICKET_GOLG, TICKET_SILVER, TICKET_IRON, TICKET_CAGNOTE, TICKET_MSTERIOUS, TICKET_MEGA As String()
            ID_TICKET_GOLG_T = {"0"}
            ID_TICKET_SILVER_T = {"0"}
            ID_TICKET_IRON_T = {"0"}
            ID_TICKET_CAGNOTE_T = {"0"}
            ID_TICKET_MSTERIOUS_T = {"0"}
            ID_TICKET_MEGA_T = {"0"}

            BOOKMAKER("ID_TICKET_GOLG") = {"0"}
            TICKET_GOLG = BOOKMAKER("ID_TICKET_GOLG")
            BOOKMAKER("ID_TICKET_SILVER") = {"0"}
            TICKET_SILVER = BOOKMAKER("ID_TICKET_SILVER")
            BOOKMAKER("ID_TICKET_IRON") = {"0"}
            TICKET_IRON = BOOKMAKER("ID_TICKET_IRON")
            BOOKMAKER("ID_TICKET_CAGNOTE") = {"0"}
            TICKET_CAGNOTE = BOOKMAKER("ID_TICKET_CAGNOTE")
            BOOKMAKER("ID_TICKET_MSTERIOUS") = {"0"}
            TICKET_MSTERIOUS = BOOKMAKER("ID_TICKET_MSTERIOUS")
            BOOKMAKER("ID_TICKET_MEGA") = {"0"}
            TICKET_MEGA = BOOKMAKER("ID_TICKET_MEGA")
            'mini Jackpot
            dlistlot = ojacpot.GetListlot(id_bookmaker, BOOKMAKER("ID_SILVER").ToString)
            For i As Integer = 0 To dlistlot.Tables("jackpot").Rows.Count - 1
                If (ojacpot.verifWinnerJackpot_lot(dlistlot.Tables("jackpot").Rows(i).Item("id_lot").ToString, Tir_cour_jack.ToString, ID_TICKET_SILVER_T, CInt(BOOKMAKER("NB_SHARER")), False, CInt(BOOKMAKER("MIN_LOTO_PLASMA")), CInt(BOOKMAKER("MIN_PARI_JACKPOT")))) Then
                    If (ID_TICKET_SILVER_T(0) <> "0") Then
                        st = String.Join("*", ID_TICKET_SILVER_T)
                        st = dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString & "*" & st
                        TICKET_SILVER(TICKET_SILVER.Length - 1) = st
                        select_another_lot(id_bookmaker, BOOKMAKER("ID_SILVER").ToString, dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString, True)

                        'Me.BeginInvoke(New Action(Sub() FomCongrat.Close()))
                    End If
                End If
            Next
            BOOKMAKER("ID_TICKET_SILVER") = TICKET_SILVER

            dlistlot = ojacpot.GetListlot(id_bookmaker, BOOKMAKER("ID_IRON").ToString)
            For i As Integer = 0 To dlistlot.Tables("jackpot").Rows.Count - 1
                If (ojacpot.verifWinnerJackpot_lot(dlistlot.Tables("jackpot").Rows(i).Item("id_lot").ToString, Tir_cour_jack.ToString, ID_TICKET_IRON_T, CInt(BOOKMAKER("NB_SHARER")), True, CInt(BOOKMAKER("MIN_LOTO_PLASMA")), CInt(BOOKMAKER("MIN_PARI_JACKPOT")))) Then
                    If (ID_TICKET_IRON_T(0) <> "0") Then
                        st = String.Join("*", ID_TICKET_IRON_T)
                        st = dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString & "*" & st
                        TICKET_IRON(TICKET_IRON.Length - 1) = st
                        select_another_lot(id_bookmaker, BOOKMAKER("ID_IRON").ToString, dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString, True)
                        'Me.BeginInvoke(New Action(Sub() FomCongrat.Close()))
                    End If
                End If
            Next
            BOOKMAKER("ID_TICKET_IRON") = TICKET_IRON

            'Tele
            dlistlot = ojacpot.GetListlot(id_bookmaker, BOOKMAKER("ID_GOLD").ToString)
            For i As Integer = 0 To dlistlot.Tables("jackpot").Rows.Count - 1
                If (ojacpot.verifWinnerJackpot_lot(dlistlot.Tables("jackpot").Rows(i).Item("id_lot").ToString, Tir_cour_jack.ToString, ID_TICKET_GOLG_T, CInt(BOOKMAKER("NB_SHARER")), True, CInt(BOOKMAKER("MIN_LOTO_PLASMA")), CInt(BOOKMAKER("MIN_PARI_JACKPOT")))) Then
                    If (ID_TICKET_GOLG_T(0) <> "0") Then
                        st = String.Join("*", ID_TICKET_GOLG_T)
                        st = dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString & "*" & st
                        TICKET_GOLG(TICKET_GOLG.Length - 1) = st
                        select_another_lot(id_bookmaker, BOOKMAKER("ID_GOLD").ToString, dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString)
                        'Me.BeginInvoke(New Action(Sub() FomCongrat.Close()))
                    End If
                End If
            Next
            BOOKMAKER("ID_TICKET_GOLG") = TICKET_GOLG

            'If (ojacpot.verifWinnerJackpot(ID_GOLD, Tir_cour_jack.ToString, ID_TICKET_GOLG, NB_SHARER, True, MIN_LOTO_PLASMA)) Then
            '    If (ID_TICKET_GOLG(0) <> "0") Then
            '        select_another_lot(ID_GOLD)
            '    End If

            'End If

            ' jus canette et autres (4)
            dlistlot = ojacpot.GetListlot(id_bookmaker, BOOKMAKER("ID_CAGNOTE").ToString)
            For i As Integer = 0 To dlistlot.Tables("jackpot").Rows.Count - 1
                If (ojacpot.verifWinnerJackpot_lot(dlistlot.Tables("jackpot").Rows(i).Item("id_lot").ToString, Tir_cour_jack.ToString, ID_TICKET_CAGNOTE_T, CInt(BOOKMAKER("NB_SHARER")), False, CInt(BOOKMAKER("MIN_LOTO_PLASMA")), CInt(BOOKMAKER("MIN_PARI_JACKPOT")))) Then
                    If (ID_TICKET_CAGNOTE_T(0) <> "0") Then
                        st = String.Join("*", ID_TICKET_CAGNOTE_T)
                        st = dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString & "*" & st

                        TICKET_CAGNOTE(TICKET_CAGNOTE.Length - 1) = st

                        select_another_lot(id_bookmaker, BOOKMAKER("ID_CAGNOTE").ToString, dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString)
                        'Me.BeginInvoke(New Action(Sub() FomCongrat.Close()))
                    End If
                End If
            Next
            BOOKMAKER("ID_TICKET_CAGNOTE") = TICKET_CAGNOTE

            dlistlot = ojacpot.GetListlot(id_bookmaker, BOOKMAKER("ID_MSTERIOUS").ToString)
            For i As Integer = 0 To dlistlot.Tables("jackpot").Rows.Count - 1
                If (ojacpot.verifWinnerJackpot_lot(dlistlot.Tables("jackpot").Rows(i).Item("id_lot").ToString, Tir_cour_jack.ToString, ID_TICKET_MSTERIOUS_T, CInt(BOOKMAKER("NB_SHARER")), True, CInt(BOOKMAKER("MIN_LOTO_PLASMA")), CInt(BOOKMAKER("MIN_PARI_JACKPOT")))) Then

                    If (ID_TICKET_MSTERIOUS_T(0) <> "0") Then
                        st = String.Join("*", ID_TICKET_MSTERIOUS_T)
                        st = dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString & "*" & st

                        TICKET_MSTERIOUS(TICKET_MSTERIOUS.Length - 1) = st

                        select_another_lot(id_bookmaker, BOOKMAKER("ID_MSTERIOUS").ToString, dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString)
                        'Me.BeginInvoke(New Action(Sub() FomCongrat.Close()))
                    End If
                End If
            Next
            BOOKMAKER("ID_TICKET_MSTERIOUS") = TICKET_MSTERIOUS

            dlistlot = ojacpot.GetListlot(id_bookmaker, BOOKMAKER("ID_MEGA").ToString)
            For i As Integer = 0 To dlistlot.Tables("jackpot").Rows.Count - 1
                If (ojacpot.verifWinnerJackpot_lot(dlistlot.Tables("jackpot").Rows(i).Item("id_lot").ToString, Tir_cour_jack.ToString, ID_TICKET_MEGA_T, CInt(BOOKMAKER("NB_SHARER")), True, CInt(BOOKMAKER("MIN_LOTO_PLASMA")), CInt(BOOKMAKER("MIN_PARI_JACKPOT")))) Then
                    If (ID_TICKET_MEGA_T(0) <> "0") Then
                        st = String.Join("*", ID_TICKET_MEGA_T)
                        st = dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString & "*" & st

                        TICKET_MEGA(TICKET_MEGA.Length - 1) = st
                        select_another_lot(id_bookmaker, BOOKMAKER("ID_MEGA").ToString, dlistlot.Tables("jackpot").Rows(i).Item("id_pv").ToString)
                        'Me.BeginInvoke(New Action(Sub() FomCongrat.Close()))
                    End If
                End If
            Next
            BOOKMAKER("ID_TICKET_MEGA") = TICKET_MEGA
        End Using
        BOOKMAKER("TIRAGE") = tirage

        BOOKMAKER("num_Tiragetosend") = book_num_tir


        BOOKMAKER("animaDemarer") = True
        bookmaker_data(id_bookmaker) = BOOKMAKER ' actualiser les mise a a jour effectué 
        Thread.Sleep(30000)
        BOOKMAKER("animaDemarer") = False
        bookmaker_data(id_bookmaker) = BOOKMAKER ' actualiser les mise a a jour effectué 




        ' c'est ici qu'on devrai modifier le picture box

    End Sub
    Delegate Sub dAnimation(ByVal id_Bookmaker As String, ByVal Resultat() As Integer, ByVal tirnum As String)


    Public Sub Animantion(ByVal id_Bookmaker As String, ByVal Resultat() As Integer, ByVal tirnum As String)




        LB_DT_51.Text = LB_DT_41.Text
        LB_DT_52.Text = LB_DT_42.Text
        LB_DT_53.Text = LB_DT_43.Text
        LB_DT_54.Text = LB_DT_44.Text
        LB_DT_55.Text = LB_DT_45.Text
        LB_DT_5.Text = LB_DT_4.Text

        LB_DT_41.Text = LB_DT_31.Text
        LB_DT_42.Text = LB_DT_32.Text
        LB_DT_43.Text = LB_DT_33.Text
        LB_DT_44.Text = LB_DT_34.Text
        LB_DT_45.Text = LB_DT_35.Text
        LB_DT_4.Text = LB_DT_3.Text

        LB_DT_31.Text = LB_DT_21.Text
        LB_DT_32.Text = LB_DT_22.Text
        LB_DT_33.Text = LB_DT_23.Text
        LB_DT_34.Text = LB_DT_24.Text
        LB_DT_35.Text = LB_DT_25.Text
        LB_DT_3.Text = LB_DT_2.Text

        LB_DT_21.Text = LB_DT_11.Text
        LB_DT_22.Text = LB_DT_12.Text
        LB_DT_23.Text = LB_DT_13.Text
        LB_DT_24.Text = LB_DT_14.Text
        LB_DT_25.Text = LB_DT_15.Text
        LB_DT_2.Text = LB_DT_1.Text

        LB_DT_1.Text = "Bookmaker: " & id_Bookmaker & " / " & Label1.Text & " " & tirnum
        LB_DT_11.Text = Resultat(0).ToString
        LB_DT_12.Text = Resultat(1).ToString
        LB_DT_13.Text = Resultat(2).ToString
        LB_DT_14.Text = Resultat(3).ToString
        LB_DT_15.Text = Resultat(4).ToString



    End Sub

    'Fonction et procedure



    Private Function GetRandomDistinctImpair(ByVal tailleVoulue As Integer, ByVal cpt As Integer) As Integer()
        Dim nombres As New List(Of Integer)()

        Do
            Do
                'Dim rand As New Random()
                'nombres.Add(rand.Next(1, cpt))
                Randomize()
                nombres.Add(CInt(Int(cpt * Rnd())))
            Loop While nombres.Count <> tailleVoulue
            nombres = nombres.Distinct().ToList()
        Loop While nombres.Count <> tailleVoulue
        Return nombres.ToArray()
    End Function

    Private Function triche(ByVal id_bookmaker As String, ByVal NumTir As Integer) As Array
        Using oBltriche As New BL_Lotto.BlTriche(source)
            Dim NbTriche As New List(Of Integer)()
            Dim Dt As DataSet
            Dim N As Integer = MaxBoul - 1
            Dim ListTirage As New List(Of Integer)()
            Dim nbmax As Integer = 0

            'Dim porcentage As Double

            Dt = oBltriche.RecupNumJouer(id_bookmaker, NumTir)

            For i = 0 To MaxBoul - 1
                ListTirage.Add(i + 1)
            Next

            'apres avoir recuperer les numero jouer dans base de donnée, on conserve que la moitié de ses numeros
            If (Dt.Tables("numero").Rows.Count > 0) Then

                'Tricherie 50% de chance

                nbmax = CInt(Dt.Tables("numero").Rows.Count)

                If nbmax > 0 Then

                    If (nbmax > (MaxBoul - NbBoulTirag)) Then
                        ' si il arrive que 90 personne on parier sur 90 nombre differents on laisse passeer 90 %
                        'Dim porcentage As Double = 93 / 100
                        nbmax = MaxBoul - NbBoulTirag
                    End If

                    For i = 0 To nbmax - 1
                        NbTriche.Add(CInt(Dt.Tables("numero").Rows(i).Item("NUMEROS")))
                    Next

                End If

                'ici on retire la moitié des nombres jouer de la liste de tirage (1-90)
                For i = 0 To NbTriche.Count - 1
                    ListTirage.Remove(NbTriche(i))
                Next
            End If
            Return ListTirage.ToArray()
        End Using

    End Function

    Public Function MontantGagnerActuel(ByVal id_bookmaker As String, ByVal datedebut As String, ByVal datefin As String) As ULong
        Dim dsTouticket, ODsPari As DataSet
        'Dim oDsNumJou As DataSet
        Dim MontantSortie, nbticketGagnant, Gains, Tir_Noa As Integer
        Dim id_ticket As String
        Dim Gagner As Boolean
        nbticketGagnant = 0
        MontantSortie = 0


        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTouticket = oCompte.getListTicketBookMaker(id_bookmaker, datedebut, datefin)

            Using oTicket As New BL_Lotto.BlTicket(source)
                For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1

                    ' on teste si le ticket fait partie des ticket en cour
                    If (Tir_Noa = CInt(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE"))) Then
                        Continue For
                    End If

                    id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                    ODsPari = oTicket.GetPari(id_ticket)
                    Gagner = False
                    Gains = 0
                    'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then

                        For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                            Dim pari_gagnant As Boolean = oTicket.verifWinner(id_bookmaker, ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                            dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                            If (pari_gagnant) Then
                                Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))
                            End If

                            Gagner = Gagner Or pari_gagnant
                        Next

                    End If

                    'oDsNumJou = oTicket.getNumJouer(id_ticket)
                    If (Gagner) Then
                        nbticketGagnant += 1
                        MontantSortie += CInt(Gains)
                    End If
                Next
            End Using
        End Using
        Return CType(MontantSortie, ULong)
    End Function

    Public Function GainsActuelSolo(ByVal id_bookmaker As String, ByVal actualTirage As Integer, ByVal datedebut As String, ByVal datefin As String) As ULong
        Dim dsTouticket, ODsPari As DataSet
        'Dim oDsNumJou As DataSet
        Dim MontantSortie, nbticketGagnant, Gains As Integer
        Dim id_ticket As String
        Dim Gagner As Boolean
        nbticketGagnant = 0
        MontantSortie = 0


        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTouticket = oCompte.getListTicketBookMaker(id_bookmaker, datedebut, datefin)

            Using oTicket As New BL_Lotto.BlTicket(source)
                For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1

                    ' pour une raison ou une autre ce code en commentaire ne fonctionne pas

                    'If (actualTirage = CInt(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE"))) Then
                    '    Continue For
                    'End If

                    id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                    ODsPari = oTicket.GetPariParMode(id_ticket, "1")
                    Gagner = False
                    Gains = 0
                    'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then

                        For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)
                            Dim pari_gagnant As Boolean = oTicket.verifWinner(id_bookmaker, ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                            dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)
                            If pari_gagnant Then
                                Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                            End If
                            Gagner = Gagner Or pari_gagnant

                        Next

                    End If

                    'oDsNumJou = oTicket.getNumJouer(id_ticket)
                    If (Gagner) Then
                        nbticketGagnant += 1
                        MontantSortie += CInt(Gains)
                    End If
                Next
            End Using
        End Using
        Return CType(MontantSortie, ULong)
    End Function


    Public Function GainsActuelDoublet(ByVal id_bookmaker As String, ByVal actualTirage As Integer, ByVal datedebut As String, ByVal datefin As String) As ULong
        Dim dsTouticket, ODsPari As DataSet
        'Dim oDsNumJou As DataSet
        Dim MontantSortie, nbticketGagnant, Gains As Integer
        Dim id_ticket As String
        Dim Gagner As Boolean
        nbticketGagnant = 0
        MontantSortie = 0

        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTouticket = oCompte.getListTicketBookMaker(id_bookmaker, datedebut, datefin)

            Using oTicket As New BL_Lotto.BlTicket(source)
                For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1


                    If (actualTirage = CInt(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE"))) Then
                        Continue For
                    End If

                    id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                    ODsPari = oTicket.GetPariParMode(id_ticket, "2")
                    Gagner = False
                    Gains = 0
                    'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then

                        For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)
                            Dim pari_gagnant As Boolean = oTicket.verifWinner(id_bookmaker, ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                            dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)
                            If (pari_gagnant) Then
                                Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))
                            End If

                            Gagner = Gagner Or pari_gagnant

                        Next

                    End If

                    'oDsNumJou = oTicket.getNumJouer(id_ticket)
                    If (Gagner) Then
                        nbticketGagnant += 1
                        MontantSortie += CInt(Gains)
                    End If
                Next
            End Using
        End Using
        Return CType(MontantSortie, ULong)
    End Function


    Public Function ScinderGagnantPerdant(ByVal bookmaker_id As String, ByVal actualTirage As Integer, ByVal mode As String, ByVal montantdispo As Long, ByVal datedebut As String, ByVal datefin As String, ByRef TabPerdant As List(Of Integer)) As List(Of Integer)
        Dim perdant, gagnat, parilist As New List(Of Integer)
        Dim dsTouticket, ODsPari As DataSet
        Dim MontantSortie, nbticketGagnant, Gains As Integer
        Dim id_ticket As String
        Dim MONTANTDISPON As Long = montantdispo
        nbticketGagnant = 0
        MontantSortie = 0

        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTouticket = oCompte.getListTicketTirag_parBook(datedebut, datefin, actualTirage.ToString, bookmaker_id)

            Using oTicket As New BL_Lotto.BlTicket(source)
                For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
                    id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString
                    ODsPari = oTicket.GetPariParMode(id_ticket, mode)
                    Gains = 0

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then
                        parilist.AddRange(ODsPari.Tables("pari").AsEnumerable.Select(Function(r) r.Field(Of Int32)("ID_PARI")).ToList())
                    End If
                Next
                If (parilist.Count > 0) Then
                    Dim tempon As List(Of Integer) = parilist
                    Dim ODsetPari As DataSet
                    Dim nb As Integer
                    While parilist.Count > 0
                        Dim rand As New Random()
                        Randomize()
                        nb = rand.Next(0, parilist.Count)

                        ODsetPari = oTicket.GetUnPari(parilist.Item(nb).ToString)

                        If (ODsetPari.Tables("pari").Rows.Count > 0) Then

                            Gains = (CInt(ODsetPari.Tables("pari").Rows(0).Item("MONTANT_MISE")) * CInt(ODsetPari.Tables("pari").Rows(0).Item("QUOTA")))

                            If (Gains < MONTANTDISPON) Then
                                MONTANTDISPON = MONTANTDISPON - CLng(Gains)
                                'gagnat.Add({CInt(id_ticket), CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI"))})
                                gagnat.Add(CInt(ODsetPari.Tables("pari").Rows(0).Item("ID_PARI")))
                            Else
                                perdant.Add(CInt(ODsetPari.Tables("pari").Rows(0).Item("ID_PARI")))
                            End If
                        End If
                        parilist.RemoveAt(nb)

                    End While
                End If


            End Using
            TabPerdant = perdant
            Return gagnat
        End Using
    End Function

    Public Function lotoBoost(ByVal id_bookmaker As String, ByVal mode As String, ByVal montantdispo As Long, ByVal datedebut As String, ByVal datefin As String) As Long
        ' cette algorith fonctione selon le principe decrit sur l'image dans le Drive


        Dim boosterAmount As Long = 0
        Dim factor As Integer = 0

        Using oTicket As New BL_Lotto.BlTicket(source)
            Dim cota As Integer = oTicket.GetQuota(mode)
            'check if the actual amount can let a player win

            If montantdispo < miseMinimum * cota Then
                'if not let see if we can help
                Dim rand As New Random()
                Randomize()
                Dim decision As Integer = rand.Next(1, 11)
                Dim monthly_boost As Integer = oTicket.GetStatBoost(id_bookmaker, datedebut, datefin)
                Dim BOOKMAKER As Hashtable = CType(bookmaker_data(id_bookmaker), Hashtable)
                If decision Mod 3 = 0 And monthly_boost < CInt(BOOKMAKER("MONTH_BOOST_LIMIT")) Then
                    'If monthly_boost < CInt(BOOKMAKER("MONTH_BOOST_LIMIT")) Then
                    'if we do not pass the limit and the random value accept we boost the game
                    Randomize()
                    Do
                        factor = rand.Next(1, 6)
                        boosterAmount = factor * miseMinimum * cota
                    Loop Until boosterAmount < CInt(BOOKMAKER("MAX_BOOST_LIMIT"))

                    oTicket.saveBoost(id_bookmaker, boosterAmount)

                End If
            End If

        End Using
        Return If(boosterAmount = 0, montantdispo, boosterAmount)
    End Function

    'Public Function ScinderGagnantDoublet(ByVal montantdispo As Long, ByVal datedebut As String, ByVal datefin As String, ByRef TabPerdant As List(Of Integer)) As List(Of Integer)
    '    Dim perdant, gagnat, parilist As New List(Of Integer)
    '    Dim dsTouticket, ODsPari As DataSet
    '    Dim MontantSortie, nbticketGagnant, Gains As Integer
    '    Dim id_ticket As String
    '    Dim Gagner As Boolean
    '    Dim Tir_Noa As Integer
    '    Dim MONTANTDISPON As Long = montantdispo
    '    nbticketGagnant = 0
    '    MontantSortie = 0

    '    Using oTicketTi As New BL_Lotto.BlTicket(source)
    '        dsti = oTicketTi.GetLastTirageNum()
    '        If (dsti.Tables("tirage").Rows.Count > 0) Then
    '            Tir_Noa = CInt(dsti.Tables("tirage").Rows(0).Item("NUM_TIRAGE")) + 1
    '        Else
    '            Tir_Noa = 1
    '        End If
    '    End Using


    '    Using oCompte As New BL_Lotto.BlCompte(source)

    '        dsTouticket = oCompte.getListTicketTirag(datedebut, datefin, Tir_Noa.ToString)

    '        Using oTicket As New BL_Lotto.BlTicket(source)
    '            For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
    '                id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

    '                ODsPari = oTicket.GetPariParMode(id_ticket, "2")
    '                Gagner = False
    '                Gains = 0
    '                'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

    '                If (ODsPari.Tables("pari").Rows.Count > 0) Then

    '                    For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1


    '                        parilist.Add(CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI")))
    '                    Next

    '                End If
    '            Next

    '            If (parilist.Count > 0) Then
    '                Dim tempon As List(Of Integer) = parilist
    '                Dim ODsetPari As DataSet
    '                Dim nb As Integer
    '                While parilist.Count > 0
    '                    Dim rand As New Random()

    '                    nb = rand.Next(0, parilist.Count)

    '                    ODsetPari = oTicket.GetUnPari(parilist.Item(nb).ToString)

    '                    If (ODsetPari.Tables("pari").Rows.Count > 0) Then

    '                        Gains = (CInt(ODsetPari.Tables("pari").Rows(0).Item("MONTANT_MISE")) * CInt(ODsetPari.Tables("pari").Rows(0).Item("QUOTA")))

    '                        If (Gains < MONTANTDISPON) Then
    '                            MONTANTDISPON = MONTANTDISPON - CLng(Gains)

    '                            'gagnat.Add({CInt(id_ticket), CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI"))})
    '                            gagnat.Add(CInt(ODsetPari.Tables("pari").Rows(0).Item("ID_PARI")))
    '                        Else
    '                            perdant.Add(CInt(ODsetPari.Tables("pari").Rows(0).Item("ID_PARI")))
    '                        End If
    '                    End If
    '                    parilist.RemoveAt(nb)

    '                End While
    '            End If


    '        End Using
    '        TabPerdant = perdant
    '        Return gagnat
    '    End Using
    'End Function


    Public Function removeNumeroPerdant(ByVal perdant As List(Of Integer)) As Integer()
        Using oBltriche As New BL_Lotto.BlTriche(source)
            Dim NbTriche As New List(Of Integer)()
            Dim NumPerdant As New List(Of Integer)
            Dim oDsNumJou As DataSet
            'Dim N As Integer = MaxBoul - 1
            Dim ListTirage As New List(Of Integer)()
            Dim temp As Integer
            Dim nbmax As Integer = 0
            Dim nb As Integer = 0
            Dim trouve As Boolean = False

            For i = 0 To MaxBoul - 1
                ListTirage.Add(i + 1)
            Next

            'apres avoir recuperer les numero jouer dans base de donnée, on conserve que la moitié de ses numeros
            Using oTicket As New BL_Lotto.BlTicket(source)
                For j As Integer = 0 To perdant.Count - 1
                    temp = perdant.Item(j)
                    oDsNumJou = oTicket.getNumJouer(temp.ToString)

                    If (oDsNumJou.Tables("avoir").Rows.Count > 0) Then

                        ' si le pari est un doublet il suffit de retirer un des numeros
                        If Not NumPerdant.Contains(CInt(oDsNumJou.Tables("avoir").Rows(0).Item("NUMEROS"))) Then
                            NumPerdant.Add(CInt(oDsNumJou.Tables("avoir").Rows(0).Item("NUMEROS")))
                        End If

                    End If

                Next
            End Using

            nbmax = NumPerdant.Count


            If (nbmax > (MaxBoul - NbBoulTirag)) Then
                ' si il arrive que 90 personne on parier sur 90 nombre differents on laisse passeer 90 %
                'Dim porcentage As Double = 93 / 100
                nbmax = MaxBoul - NbBoulTirag
            End If

            NbTriche.AddRange(NumPerdant.GetRange(0, nbmax))

            'ici on retire la moitié des nombres jouer de la liste de tirage (1-90)
            ListTirage = ListTirage.Except(NbTriche).ToList()


            Return ListTirage.ToArray()
        End Using
    End Function


    Public Function getNumeroGagnant(ByVal gagnant As List(Of Integer)) As Integer()
        Using oBltriche As New BL_Lotto.BlTriche(source)
            Dim NbTriche As New List(Of Integer)()
            Dim NbPerdant As New List(Of Integer)
            Dim oDsNumJou As DataSet
            Dim temp As Integer

            'apres avoir recuperer les numero jouer dans base de donnée, on conserve que la moitié de ses numeros
            Using oTicket As New BL_Lotto.BlTicket(source)
                For j As Integer = 0 To gagnant.Count - 1
                    temp = gagnant.Item(j)
                    oDsNumJou = oTicket.getNumJouer(temp.ToString)

                    If (oDsNumJou.Tables("avoir").Rows.Count > 0) Then

                        For k As Integer = 0 To oDsNumJou.Tables("avoir").Rows.Count - 1

                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)
                            If Not NbPerdant.Contains(CInt(oDsNumJou.Tables("avoir").Rows(k).Item("NUMEROS"))) Then
                                NbPerdant.Add(CInt(oDsNumJou.Tables("avoir").Rows(k).Item("NUMEROS")))
                            End If

                        Next

                    End If

                Next
            End Using

            NbTriche.AddRange(NbPerdant)

            Return NbTriche.ToArray()
        End Using
    End Function


    Private Sub FrmJeu_Closing(sender As Object, e As CancelEventArgs) Handles Me.FormClosing
        If Not MonSocketServeur Is Nothing Then 'Si le socket a été créé
            MonSocketServeur.Close() 'On le ferme
        End If
        If Not MonSockPourCaisse Is Nothing Then 'Si le socket a été créé
            MonSockPourCaisse.Close() 'On le ferme
        End If

        If Not ThreadClient Is Nothing Then 'Si le thread a été créé
            If ThreadClient.IsAlive Then 'S'il tourne
                ThreadClient.Abort() 'On le stoppe
            End If
        End If
        If Not ThreadAnim Is Nothing Then 'Si le thread a été créé
            If ThreadAnim.IsAlive Then 'S'il tourne
                ThreadAnim.Abort() 'On le stoppe
            End If
        End If
        If Not ThreadCaisse Is Nothing Then 'Si le thread a été créé
            If ThreadCaisse.IsAlive Then 'S'il tourne
                ThreadCaisse.Abort() 'On le stoppe
            End If
        End If
    End Sub
End Class