﻿Imports System.Globalization
Public Class DlCompte
    Inherits DlBase
    Private oTicket As DlTickect
#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
        oTicket = New DlTickect(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region
#Region " Public Role Functions "
    Public Function getStatTirage(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTirage = New DataSet
            MyBase.SQL = "SELECT COUNT(NUM_TIRAGE) AS NB_TIRAGE FROM tirage WHERE DATE_TIRAGE BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTirage, "tirage")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getCreditResiduel(ByVal id_bookmaker As String) As Integer
        Try
            Dim getStatPari As DataSet
            Dim Tabdate(), datedebut, datedujour As String
            Dim montant_sousci, montant_actuel As Integer

            getStatPari = New DataSet

            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            MyBase.SQL = "SELECT `id_cle`,`cle`,`montant`,DATE_FORMAT(`date_enreg`,'%Y/%m/%d %H:%i:%s') as date_enreg,`etat`,`used` FROM `cle_activation` WHERE id_bookmaker = " & id_bookmaker & " AND etat=1 AND used = 1"
            MyBase.InitializeCommand()

            MyBase.FillDataSet(LoginDataset, "cle_activation")

            If LoginDataset.Tables("cle_activation").Rows.Count > 0 Then
                If (LoginDataset.Tables("cle_activation").Rows(0).Item("montant").ToString = "0") Then
                    Return -1
                End If

                'on vas calculer le montant actuel recu
                datedebut = LoginDataset.Tables("cle_activation").Rows(0).Item("date_enreg")

                Tabdate = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))

                datedujour = Tabdate(2) & "/" & Tabdate(1) & "/" & Tabdate(0) & " 23:59:59"

                ' on teste si le total de mise n'est pas null
                montant_actuel = Me.GetBenefice(id_bookmaker, datedebut, datedujour)
                'MsgBox(montant_actuel)
                montant_sousci = CInt(LoginDataset.Tables("cle_activation").Rows(0).Item("montant"))
                'MsgBox(montant_sousci)
                'si le montant actuel vendu est inferieur au montant souscrit

                getCreditResiduel = montant_sousci - montant_actuel

            Else
                getCreditResiduel = 0
            End If

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTicket(ByVal id_bookmaker As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTicket = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket t, point_vente p WHERE t.id_pv = p.id_pv AND STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "' AND p.bookmaker_id=" & id_bookmaker
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTicket, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTicket(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTicket = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket WHERE STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTicket, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTickAnnulParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTickAnnulParUser = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket WHERE STATUS=0 AND ID_USER=" & iduser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTickAnnulParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTickAnnulBookmaker(ByVal id_bookmaker As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTickAnnulBookmaker = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket t, point_vente pv WHERE t.id_pv=pv.id_pv AND pv.bookmaker_id=" & id_bookmaker & " AND STATUS=0 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTickAnnulBookmaker, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function



    Public Function getStatTickAnnul(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTickAnnul = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket WHERE STATUS=0 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTickAnnul, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTicketParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTicketParUser = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket WHERE STATUS=1 AND ID_USER=" & iduser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTicketParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getStatTicketJack(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTicketJack = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket, gagner_jackp WHERE id_tick = ID_TICKET AND STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTicketJack, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTicketJackParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTicketJackParUser = New DataSet
            MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET FROM ticket, gagner_jackp g WHERE id_tick = ID_TICKET AND STATUS=1 AND g.ID_USER_S=" & iduser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTicketJackParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getListJackParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getListJackParUser = New DataSet
            MyBase.SQL = "SELECT g.id_tick,`id_lot`,g.ID_USER_S,g.is_materiel,`nom_materiel`,`montant`,COD_BAR FROM ticket t, gagner_jackp g WHERE id_tick = ID_TICKET AND STATUS=1 AND g.ID_USER_S=" & iduser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListJackParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getListJackParPv(ByVal idpv As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getListJackParPv = New DataSet
            MyBase.SQL = "SELECT g.id_tick,`id_lot`,g.ID_USER_S,g.is_materiel,`nom_materiel`,`montant`,COD_BAR FROM ticket t, gagner_jackp g WHERE id_tick = ID_TICKET AND STATUS=1 AND t.id_pv = " & idpv & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"

            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListJackParPv, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getMontTickJack_Bookmaker(ByVal id_bookmaker As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getMontTickJack_Bookmaker = New DataSet
            'MyBase.SQL = "SELECT SUM(montant) AS TOTAL_MONTANT FROM ticket,gagner_jackp WHERE id_tick = ID_TICKET  AND is_materiel = 0 AND  STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.SQL = "SELECT SUM(montant) AS TOTAL_MONTANT FROM ticket t,gagner_jackp g , point_vente pv WHERE pv.id_pv=t.id_pv AND pv.bookmaker_id=" & id_bookmaker & " AND id_tick = ID_TICKET AND g.ID_USER_S <> 0 AND STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getMontTickJack_Bookmaker, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getStatTickJackMon(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTickJackMon = New DataSet
            'MyBase.SQL = "SELECT SUM(montant) AS TOTAL_MONTANT FROM ticket,gagner_jackp WHERE id_tick = ID_TICKET  AND is_materiel = 0 AND  STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.SQL = "SELECT SUM(montant) AS TOTAL_MONTANT FROM ticket t,gagner_jackp g WHERE id_tick = ID_TICKET AND g.ID_USER_S <> 0 AND STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTickJackMon, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTickJackMon(ByVal idpv As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTickJackMon = New DataSet
            MyBase.SQL = "SELECT SUM(montant) AS TOTAL_MONTANT FROM ticket t,gagner_jackp g WHERE id_tick = ID_TICKET AND STATUS=1 AND g.ID_USER_S <> 0 AND id_pv = " & idpv & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTickJackMon, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatTickJackMonParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatTickJackMonParUser = New DataSet

            MyBase.SQL = "SELECT SUM(montant) AS TOTAL_MONTANT FROM ticket,gagner_jackp g WHERE id_tick = ID_TICKET AND STATUS=1 AND is_materiel = 0 AND g.ID_USER_S=" & iduser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT SUM(montant) AS TOTAL_MONTANT FROM ticket,gagner_jackp g WHERE id_tick = ID_TICKET AND STATUS=1 AND g.ID_USER_S=" & iduser & " AND g.is_materiel = 0 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatTickJackMonParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatPariSolo(ByVal id_bookmaker As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPariSolo = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c, point_vente pv WHERE pv.id_pv=t.id_pv AND pv.bookmaker_id= " & id_bookmaker & " AND p.`ID_MODE`=1 AND STATUS=1 AND  t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPariSolo, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatPariSolo(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPariSolo = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE p.`ID_MODE`=1 AND STATUS=1 AND  t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPariSolo, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatPariDoublet(ByVal id_bookmaker As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPariDoublet = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c, point_vente pv WHERE t.id_pv=pv.id_pv AND pv.bookmaker_id=" & id_bookmaker & " AND p.`ID_MODE`=2 AND STATUS=1 AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPariDoublet, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getStatPariDoublet(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPariDoublet = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE p.`ID_MODE`=2 AND STATUS=1 AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPariDoublet, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getStatPari(ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPari = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE STATUS=1 AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPari, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatPari(ByVal idpv As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPari = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE STATUS=1 AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND id_pv = " & idpv & " AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPari, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatPariBook(ByVal id_bookmaker As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPariBook = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c, point_vente pv WHERE t.id_pv=pv.id_pv AND pv.bookmaker_id=" & id_bookmaker & " AND STATUS=1 AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPariBook, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getStatPariParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatPariParUser = New DataSet
            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE STATUS=1 AND t.ID_USER=" & iduser & "  AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND  DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatPariParUser, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatCreditParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatCreditParUser = New DataSet
            MyBase.SQL = "SELECT SUM(montant) AS TOTALCREDIT FROM credit_player c WHERE id_caisse=" & iduser & " AND  date_operation BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatCreditParUser, "credit")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getStatCreditbookmaker(ByVal id_bookmaker As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getStatCreditbookmaker = New DataSet
            MyBase.SQL = "SELECT SUM(montant) AS TOTALCREDIT FROM user u, credit_player c, point_vente pv WHERE u.ID_USER=id_caisse AND u.id_pv=pv.id_pv AND pv.bookmaker_id=" & id_bookmaker & " AND  date_operation BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getStatCreditbookmaker, "credit")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getCreditPlayer(ByVal id_player As String) As DataSet
        Try
            getCreditPlayer = New DataSet
            MyBase.SQL = "SELECT SUM(montant) AS TOTALCREDIT FROM credit_player c WHERE id_player=" & id_player
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getCreditPlayer, "credit")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function





    Public Function getListTicketTirag_parBook(ByVal DatDebut As String, ByVal DatFin As String, ByVal tirage As String, ByVal id_bookmaker As String) As DataSet
        Try
            getListTicketTirag_parBook = New DataSet
            MyBase.SQL = "SELECT * FROM ticket t, point_vente pv WHERE t.id_pv = pv.id_pv AND STATUS=1 AND pv.bookmaker_id=" & id_bookmaker & " AND NUM_TIRAGE = " & tirage & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "' ORDER BY RAND()"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListTicketTirag_parBook, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getListTicketBookMaker(ByVal id_bookmaker As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getListTicketBookMaker = New DataSet
            MyBase.SQL = "SELECT * FROM ticket t, point_vente pv WHERE t.id_pv = pv.id_pv AND pv.bookmaker_id=" & id_bookmaker & " AND STATUS=1 AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListTicketBookMaker, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getListTicket(ByVal idpv As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getListTicket = New DataSet
            MyBase.SQL = "SELECT * FROM ticket WHERE STATUS=1 AND id_pv = " & idpv & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListTicket, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getListTicketServi(ByVal IdUser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getListTicketServi = New DataSet
            MyBase.SQL = "SELECT * FROM ticket WHERE ID_USER_S = " & IdUser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListTicketServi, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function getListTicketParUser(ByVal IdUser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getListTicketParUser = New DataSet
            MyBase.SQL = "SELECT * FROM ticket WHERE STATUS=1 AND  ID_USER = " & IdUser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListTicketParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getGainTicket(ByVal id_ticket) As DataSet
        Try
            getGainTicket = New DataSet
            MyBase.SQL = "SELECT QUOTA*MONTANT_MISE AS GAIN FROM ticket t,mode_jeux m WHERE t.ID_MODE = m.ID_MODE And ID_TICKET = " & id_ticket
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getGainTicket, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetBenefice(ByVal id_bookmaker As String, ByVal datedebut As String, ByVal datedujour As String) As Integer
        Dim Gagner As Boolean = False
        Dim Gains As Integer = 0
        Dim dsTicket, dsTickJackMon, dsTouticket, dsPari, ODsPari As DataSet
        Dim TotalmontantJack As Integer = 0
        Dim ID_TICKET_CAIS, TotalMise As String


        dsTickJackMon = Me.getMontTickJack_Bookmaker(id_bookmaker, datedebut, datedujour)
        dsTicket = Me.getStatTicket(id_bookmaker, datedebut, datedujour)
        dsPari = Me.getStatPariBook(id_bookmaker, datedebut, datedujour)

        If (dsTickJackMon.Tables("ticket").Rows.Count > 0) Then
            If (Not dsTickJackMon.Tables("ticket").Rows(0).IsNull("TOTAL_MONTANT")) Then
                TotalmontantJack = CInt(dsTickJackMon.Tables("ticket").Rows(0).Item("TOTAL_MONTANT"))
            Else
                TotalmontantJack = 0
            End If
        End If

        If (dsTicket.Tables("ticket").Rows.Count > 0) Then

            TotalMise = dsPari.Tables("pari").Rows(0).Item("TOTALMISE").ToString
            'calcul du nombre de ticket gagnant
            dsTouticket = Me.getListTicketBookMaker(id_bookmaker, datedebut, datedujour)
            Dim MontantSortie As Integer
            MontantSortie = 0

            For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
                ID_TICKET_CAIS = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                'Dim oDsNumJou As DataSet

                ODsPari = oTicket.GetPari(ID_TICKET_CAIS)
                Gagner = False
                Gains = 0
                'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                If (ODsPari.Tables("pari").Rows.Count > 0) Then

                    For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1

                        If (oTicket.verifWinner(id_bookmaker, ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                                dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)) Then
                            Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))
                            Gagner = True
                        End If

                    Next

                End If

                'cumul des sortie
                If (Gagner) Then
                    MontantSortie += CInt(Gains)
                End If

            Next

            MontantSortie += TotalmontantJack

            If TotalMise = "" Then
                TotalMise = 0.ToString
            End If
            Dim benef As Integer = CInt(TotalMise) - MontantSortie

            GetBenefice = benef
        Else
            GetBenefice = 0
        End If

    End Function


#End Region


End Class
