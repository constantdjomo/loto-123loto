﻿Public Class DlActivation
    Inherits DlBase
    Private oCompte As DlCompte
    Private oTicket As DlTickect
#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
        oCompte = New DlCompte(ConString)
        oTicket = New DlTickect(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region

#Region " Public Role Functions "

    Public Function CleExist(ByVal id_bookmaker As String, ByVal cle As String) As Boolean
        Try
            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            ' etat = 1 signifie que la cle est actif
            ' used = 1 signifie que la cle n'est plus  utilisable
            MyBase.SQL = "SELECT * FROM cle_activation WHERE etat=0 AND used = 0 AND cle='" + cle + "' "
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

            'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

            MyBase.FillDataSet(LoginDataset, "cle_activation")

            If LoginDataset.Tables("cle_activation").Rows.Count > 0 Then
                Try
                    MyBase.SQL = "UPDATE cle_activation SET etat=0 WHERE id_bookmaker=" & id_bookmaker
                    MyBase.InitializeCommand()
                    ExecuteStoredProcedure()

                    ' la cle n'est plus utilisable etat = 0 et used=1
                    MyBase.SQL = "UPDATE cle_activation SET etat=1 AND used=1 AND id_bookmaker=" & id_bookmaker & " WHERE id_cle=" & LoginDataset.Tables("cle_activation").Rows(0).Item("id_cle")
                    MyBase.InitializeCommand()
                    CleExist = ExecuteStoredProcedure()
                Catch ExceptionErr As Exception
                    Throw New System.Exception(ExceptionErr.Message,
                    ExceptionErr.InnerException)
                End Try
            Else
                CleExist = False
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function IsActivated(ByVal id_bookmaker As String) As Boolean
        Try
            Dim montant_residuel As Integer
            montant_residuel = oCompte.getCreditResiduel(id_bookmaker)
            'MsgBox(montant_residuel)
            'credit illimite
            If (montant_residuel = -1) Then
                Return True
            End If


            If (montant_residuel > 0) Then
                IsActivated = True
            Else
                IsActivated = False
            End If

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


#End Region

End Class
