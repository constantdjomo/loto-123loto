﻿Public Class DlTickect
    Inherits DlBase

#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region

#Region " Public Role Functions "

    Public Function annulerTicket(ByVal idticket As String) As Boolean
        Try


            MyBase.SQL = "UPDATE ticket SET STATUS = 0 WHERE ID_TICKET='" & idticket & "' OR parent_ticket='" & idticket & "'"
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            annulerTicket = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ServirTicket(ByVal idticket As String, ByVal user As String) As Boolean
        Try


            MyBase.SQL = "UPDATE ticket SET ID_USER_S=" & user & " WHERE ID_TICKET=" & idticket
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            ServirTicket = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function ServirTicketJack(ByVal idticket As String, ByVal user As String) As Boolean
        Try


            MyBase.SQL = "UPDATE gagner_jackp SET ID_USER_S=" & user & " WHERE id_tick=" & idticket
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            ServirTicketJack = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function AjouterTicket(ByVal Ticket As DataSet, ByVal tab As String(,), ByVal nbparie As Integer, ByRef idticket As String, ByVal selected_chance As Integer) As Boolean
        Try
            Dim ok As Boolean = True
            Dim p(), reqar As String

            MyBase.SQL = "INSERT INTO ticket (parent_ticket, COD_BAR, id_pv, ID_USER, NUM_TIRAGE) values ( '" + Ticket.Tables("ticket").Rows(0).Item("parent_ticket") + "', '" + Ticket.Tables("ticket").Rows(0).Item("COD_BAR") + "'," + Ticket.Tables("ticket").Rows(0).Item("id_pv") + "," + Ticket.Tables("ticket").Rows(0).Item("ID_USER") + "," + Ticket.Tables("ticket").Rows(0).Item("NUM_TIRAGE") + " ) ; SELECT LAST_INSERT_ID()"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            idticket = CStr(ExecuteStoredScalar())
            'MsgBox(tes)
            'AjouterTicket = CBool(idticket)
            If (CBool(idticket)) Then

                Dim tick As Integer = CInt(idticket)
                'MsgBox(tick.ToString("00000000"))
                MyBase.SQL = "UPDATE ticket SET COD_BAR = CONCAT('" & tick.ToString("00000000") & "', COD_BAR)  WHERE ID_TICKET=" & idticket & ";"
                'Initialisation l'objet command
                'MyBase.InitializeCommand()

                'ok = ExecuteStoredProcedure()


                'insertion des paris

                'MyBase.SQL = ""
                For g As Integer = 0 To nbparie - 1

                    'numero jouer
                    p = tab(g, 2).ToString.Split(CChar("-"))

                    Dim mise_reviser As Integer = Math.Round(CInt(tab(g, 1)) / selected_chance, 2)

                    MyBase.SQL = MyBase.SQL & " INSERT INTO pari (ID_PARI,ID_MODE,MONTANT_MISE) values (NULL," & tab(g, 0) & "," & mise_reviser &
                    "); INSERT INTO concerner (ID_TICKET,ID_PARI) values (" & idticket & ",LAST_INSERT_ID()); "

                    reqar = " INSERT INTO avoir (NUMEROS,ID_PARI) values "
                    For i As Integer = 0 To p.Length - 1
                        If i = 0 Then
                            reqar = reqar & " ( " + p(i) + ",LAST_INSERT_ID() ) "
                        Else
                            reqar = reqar & " ,( " + p(i) + ",LAST_INSERT_ID() ) "
                        End If

                    Next

                    reqar = reqar & ";"

                    MyBase.SQL = MyBase.SQL & reqar

                Next

                MyBase.InitializeCommand()
                ExecuteStoredScalar()


                'Initialisation l'objet command
                'MyBase.InitializeCommand()
                'ok = ok AndAlso ExecuteStoredProcedure()




                AjouterTicket = ok

            Else
                AjouterTicket = False
            End If



        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function AjouterParie(ByVal ID_TICKET_P As String, ByVal ID_MODE As String, ByVal MONTANT_MISE As String, ByVal Tab_Num() As String) As Boolean
        Try

            Dim ok As Boolean = True
            Dim ID_PARI As String
            MyBase.SQL = "INSERT INTO pari (ID_PARI,ID_MODE,MONTANT_MISE) values (NULL," & ID_MODE & "," & MONTANT_MISE & "); SELECT LAST_INSERT_ID()"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            ID_PARI = CStr(ExecuteStoredScalar())
            ok = CBool(ID_PARI)

            MyBase.SQL = "INSERT INTO concerner (ID_TICKET,ID_PARI) values (" & ID_TICKET_P & "," & ID_PARI & ")"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            ok = ok AndAlso ExecuteStoredProcedure()


            For i As Integer = 0 To Tab_Num.Length - 1
                ok = ok AndAlso Me.ajouterNumJouer(Tab_Num(i), ID_PARI)
            Next

            AjouterParie = ok

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function saveBoost(ByVal id_bookmaker As String, ByVal montant As Long) As Boolean
        Try
            MyBase.SQL = "INSERT INTO `tirage_boost`(`id_bookmaker`, `montant`) VALUES ( " + id_bookmaker + "," & montant & " )"
            'Initialisation l'objet command 
            MyBase.InitializeCommand()

            saveBoost = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ajouterNumJouer(ByVal num As String, ByVal tick As String) As Boolean
        Try
            MyBase.SQL = "INSERT INTO avoir (NUMEROS,ID_PARI) values ( " + num + "," & tick & " )"
            'Initialisation l'objet command 
            MyBase.InitializeCommand()

            ajouterNumJouer = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    'Public Function MajUser(ByVal User As DataSet) As Boolean
    '    Try
    '        MyBase.SQL = "Update  Users Set Pwd=@Pwd where  IdUser=@IdUser"
    '        MyBase.InitializeCommand()

    '        MyBase.AddParameter("@Pwd", _
    '             Data.OleDb.OleDbType.VarChar, 20, _
    '            User.Tables("Users").Rows(0).Item("Pwd"))

    '        MyBase.AddParameter("@IdUser", _
    '            Data.OleDb.OleDbType.VarChar, 15, _
    '            User.Tables("Users").Rows(0).Item("IdUser"))
    '        'Exécution de la 
    '        MajUser = ExecuteStoredProcedure()
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message, _
    '        ExceptionErr.InnerException)
    '    End Try
    'End Function

    Public Function ListMode() As DataSet
        Try
            ListMode = New DataSet
            MyBase.SQL = "Select ID_MODE,MODE from mode_jeux Order by ID_MODE"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(ListMode, "mode_jeux")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetStatBoost(ByVal id_bookmaker As String, ByVal datedebut As String, ByVal datefin As String) As Integer
        Try
            Dim dss As New DataSet
            MyBase.SQL = "SELECT SUM(montant) AS TOTALBOOST FROM tirage_boost WHERE id_bookmaker=" & id_bookmaker & " AND date_operation BETWEEN '" & datedebut & "' AND '" & datefin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "tirage_boost")
            If (dss.Tables("tirage_boost").Rows(0).IsNull("TOTALBOOST")) Then
                GetStatBoost = 0
            Else
                GetStatBoost = CInt(dss.Tables("tirage_boost").Rows(0).Item("TOTALBOOST"))
            End If

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetQuota(ByVal id_mode As String) As Integer
        Try
            Dim dss As New DataSet

            MyBase.SQL = "Select QUOTA from mode_jeux WHERE ID_MODE=" & id_mode
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "mode_jeux")
            GetQuota = CInt(dss.Tables("mode_jeux").Rows(0).Item("QUOTA"))
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function GetLastTickectNum() As DataSet
        Try
            GetLastTickectNum = New DataSet
            MyBase.SQL = "Select ID_TICKET from ticket where ID_TICKET >= ALL(Select ID_TICKET from ticket)"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(GetLastTickectNum, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetPariParMode(ByVal id_ticket As String, ByVal mode As String) As DataSet
        Try
            GetPariParMode = New DataSet
            MyBase.SQL = "Select p.ID_PARI,p.ID_MODE,MODE,NB_NUM_JOUER,QUOTA,MONTANT_MISE from ticket t, pari p ,concerner c,mode_jeux m where p.ID_MODE=" & mode & " AND m.ID_MODE=p.ID_MODE AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND t.ID_TICKET=" & id_ticket & " ORDER BY RAND()"

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(GetPariParMode, "pari")


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetUnPari(ByVal pari As String) As DataSet
        Try
            GetUnPari = New DataSet
            MyBase.SQL = "Select p.ID_PARI,p.ID_MODE,MODE,NB_NUM_JOUER,QUOTA,MONTANT_MISE from  pari p ,concerner c,mode_jeux m where m.ID_MODE=p.ID_MODE AND p.ID_PARI=c.ID_PARI AND p.ID_PARI=" & pari & " ORDER BY RAND()"

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(GetUnPari, "pari")


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetLastPariNum() As DataSet
        Try
            GetLastPariNum = New DataSet
            MyBase.SQL = "Select ID_PARI from pari where ID_PARI >= ALL(Select ID_PARI from pari)"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(GetLastPariNum, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetLastTirageNum() As DataSet
        Try
            GetLastTirageNum = New DataSet
            MyBase.SQL = "SELECT NUM_TIRAGE FROM tirage WHERE NUM_TIRAGE >= ALL(SELECT NUM_TIRAGE FROM tirage)"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(GetLastTirageNum, "tirage")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetTypeMode(ByVal Mode As String) As DataSet
        Try
            GetTypeMode = New DataSet
            MyBase.SQL = "Select NB_NUM_JOUER from mode_jeux where MODE ='" & Mode & "'"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(GetTypeMode, "mode_jeux")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetMode(ByVal Mode As String) As DataSet
        Try
            GetMode = New DataSet
            MyBase.SQL = "Select * from mode_jeux where ID_MODE ='" & Mode & "'"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(GetMode, "mode_jeux")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function List_Ticket()
        Try
            List_Ticket = New DataSet
            MyBase.SQL = "Select * from ticket ORDER BY ID_TICKET"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(List_Ticket, "ticket")
        Catch ex As Exception
            Throw New System.Exception(ex.Message,
            ex.InnerException)
        End Try
    End Function


    Public Function TicketParId(ByVal id_Ticket As String) As DataSet
        Try
            TicketParId = New DataSet
            MyBase.SQL = "Select * from ticket t, point_vente pv where pv.id_pv=t.id_pv AND  t.ID_TICKET=" & id_Ticket

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(TicketParId, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function TicketParCodebar(ByVal id_Ticket As String) As DataSet
        Try
            TicketParCodebar = New DataSet
            MyBase.SQL = "SELECT * FROM ticket t INNER JOIN point_vente pv ON pv.id_pv = t.id_pv WHERE t.COD_BAR = '" & id_Ticket & "' OR t.parent_ticket IN ( SELECT ID_TICKET FROM ticket t2 INNER JOIN point_vente pv2 ON pv2.id_pv = t2.id_pv WHERE t2.COD_BAR = '" & id_Ticket & "' )"

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(TicketParCodebar, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function TicketNonPerimParId(ByVal id_Ticket As String) As DataSet
        Try
            TicketNonPerimParId = New DataSet
            MyBase.SQL = "Select * from ticket t where STATUS=1 AND t.ID_TICKET=" & id_Ticket

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(TicketNonPerimParId, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function



    Public Function verifWinner(ByVal id_bookmaker As String, ByVal id_pari As String, ByVal tirage As String) As Boolean
        Try

            Dim dsnumero, dsTirage As New DataSet
            Dim verif As Boolean
            verif = True
            dsnumero = Me.getNumJouer(id_pari)

            dsTirage = Me.getTirage(id_bookmaker, tirage)


            Dim tirageList As List(Of Int32) = dsTirage.Tables("contenir").AsEnumerable.Select(Function(r) r.Field(Of Int32)("ID_NUM_TIRER")).ToList()

            For i As Integer = 0 To dsnumero.Tables("avoir").Rows.Count - 1

                verif = verif And tirageList.Contains(CInt(dsnumero.Tables("avoir").Rows(i).Item("NUMEROS")))

            Next
            verifWinner = verif


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getNumJouer(ByVal ID_PARI As String) As DataSet
        Try
            getNumJouer = New DataSet
            MyBase.SQL = "Select NUMEROS from avoir where ID_PARI=" & ID_PARI

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(getNumJouer, "avoir")


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getTirage(ByVal id_bookmaker As String, ByVal num_tirage As String) As DataSet
        Try
            getTirage = New DataSet
            MyBase.SQL = "Select ID_NUM_TIRER from contenir WHERE id_bookmaker = " & id_bookmaker & " AND NUM_TIRAGE=" & num_tirage

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(getTirage, "contenir")


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetPari(ByVal num_ticket As String) As DataSet
        Try
            GetPari = New DataSet
            MyBase.SQL = "Select p.ID_PARI,p.ID_MODE,MODE,NB_NUM_JOUER,QUOTA,MONTANT_MISE from ticket t, pari p ,concerner c,mode_jeux m where m.ID_MODE=p.ID_MODE AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND t.ID_TICKET=" & num_ticket

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(GetPari, "pari")


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getListJackParUser(ByVal iduser As String, ByVal DatDebut As String, ByVal DatFin As String) As DataSet
        Try
            getListJackParUser = New DataSet
            MyBase.SQL = "SELECT g.id_tick,`id_lot`,g.ID_USER_S,g.is_materiel,`nom_materiel`,`montant`,COD_BAR FROM ticket t, gagner_jackp g WHERE id_tick = ID_TICKET AND STATUS=1 AND g.ID_USER_S=" & iduser & " AND DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            'MyBase.SQL = "SELECT COUNT(ID_TICKET) AS NB_TICKET,SUM(MONTANT_MISE) AS TOTALMISE FROM ticket WHERE DATE_ACHAT BETWEEN '" & DatDebut & "' AND '" & DatFin & "'"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getListJackParUser, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


#End Region
End Class
