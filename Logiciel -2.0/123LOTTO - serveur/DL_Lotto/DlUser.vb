﻿Public Class DlUser
    Inherits DlBase

#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region

#Region " Public Role Functions "
    Public Function AjouterUser(ByVal User As DataSet) As Boolean
        Try
            MyBase.SQL = "INSERT INTO users (ID_USER,NOM,PRENOM,LOGIN,PWD,id_user_typ ) values (" & User.Tables("users").Rows(0).Item("ID_USER") & ", '" & User.Tables("users").Rows(0).Item("NOM") & "','" _
                & User.Tables("users").Rows(0).Item("PRENOM") & "','" _
                & User.Tables("users").Rows(0).Item("LOGIN") & "','" _
                & User.Tables("users").Rows(0).Item("PWD") & "' ,'" _
                & User.Tables("users").Rows(0).Item("id_user_typ") & "')"

            'Initialisation l'objet command
            MyBase.InitializeCommand()
            'Ajouter les paramètres 
            'MyBase.AddParameter("@IdUser", _
            '     Data.OleDb.OleDbType.VarChar, 15, _
            '    User.Tables("Users").Rows(0).Item("IdUser"))
            'MyBase.AddParameter("@Pwd", _
            '     Data.OleDb.OleDbType.VarChar, 20, _
            '    User.Tables("Users").Rows(0).Item("Pwd"))
            AjouterUser = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function MajUser(ByVal User As DataSet) As Boolean
        Try
            MyBase.SQL = "Update  Users Set NOM ='" & User.Tables("users").Rows(0).Item("NOM") & "', PRENOM = '" _
                & User.Tables("users").Rows(0).Item("PRENOM") & "',LOGIN ='" _
                & User.Tables("users").Rows(0).Item("LOGIN") & "', PWD ='" _
                & User.Tables("users").Rows(0).Item("PWD") & "' , id_user_typ ='" _
                & User.Tables("users").Rows(0).Item("id_user_typ") & "' WHERE  ID_USER =" & User.Tables("users").Rows(0).Item("ID_USER")
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Pwd", _
            '     Data.OleDb.OleDbType.VarChar, 20, _
            '    User.Tables("Users").Rows(0).Item("Pwd"))

            'MyBase.AddParameter("@IdUser", _
            '    Data.OleDb.OleDbType.VarChar, 15, _
            '    User.Tables("Users").Rows(0).Item("IdUser"))
            'Exécution de la 
            MajUser = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    'Public Function SaveSetting(ByVal DIFICULT_LEVEL As Integer, ByVal DELAI_AV_GAINS As Integer, ByVal GOLD_POT As Integer, ByVal SILVER_POT As Integer, ByVal IRON_POT As Integer, ByVal CAGNOTE_POT As Integer, ByVal MSTERIOUS_POT As Integer, ByVal ID_GOLD As Integer, ByVal ID_SILVER As Integer, ByVal ID_IRON As Integer, ByVal ID_MSTERIOUS As Integer, ByVal MIN_GAIIN_SILVER As Integer, ByVal MAX_GAIIN_SILVER As Integer, ByVal PORT As String, ByVal SCREEN_SELECT As String) As Boolean
    '    Try
    '        MyBase.SQL = "UPDATE `setting` SET `DIFICULT_LEVEL`=" & DIFICULT_LEVEL & ",`DELAI_AV_GAINS`=" & DELAI_AV_GAINS & ",`PORT`='" & PORT & "',`GOLD_POT`=" & GOLD_POT & ",`SILVER_POT`=" & SILVER_POT & ",`IRON_POT`=" & IRON_POT & ", `MSTERIOUS_POT` = " & MSTERIOUS_POT & " ,`ID_GOLD`=" & ID_GOLD & ",`ID_SILVER`=" & ID_SILVER & ",`ID_IRON`=" & ID_IRON & ",`SCREEN_SELECT`='" & SCREEN_SELECT & "',`MIN_GAIIN_SILVER`=" & MIN_GAIIN_SILVER & ",`MAX_GAIIN_SILVER`=" & MAX_GAIIN_SILVER & " WHERE `id_setting`=0"
    '        MyBase.InitializeCommand()

    '        SaveSetting = ExecuteStoredProcedure()
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message,
    '        ExceptionErr.InnerException)
    '    End Try
    'End Function
    Public Function SaveSetting(ByVal id_bookmaker As String, ByVal MONTH_BOOST_LIMIT As String, ByVal MAX_BOOST_LIMIT As String, ByVal DIFICULT_LEVEL As String, ByVal DELAI_AV_GAINS As String, ByVal PORT As String, ByVal GOLD_POT As String, ByVal SILVER_POT As String, ByVal IRON_POT As String, ByVal CAGNOTE_POT As String, ByVal MSTERIOUS_POT As String, ByVal MEGA_POT As String, ByVal ID_CAGNOTE As String, ByVal ID_GOLD As String, ByVal ID_SILVER As String, ByVal ID_IRON As String, ByVal ID_MSTERIOUS As String, ByVal ID_MEGA As String, ByVal SCREEN_SELECT As String, ByVal NB_SHARER As String, ByVal MAX_GAIIN_SILVER As String, ByVal MIN_GAIIN_SILVER As String, ByVal MAX_GAIN_IRON As String, ByVal MIN_GAIN_IRON As String, ByVal MIN_LOTO_PLASMA As String, ByVal MIN_PARI_JACKPOT As String, ByVal DEDUCT_JACK_TO_TURN_OVER As String) As Boolean
        Try
            MyBase.SQL = "UPDATE `setting` SET `MONTH_BOOST_LIMIT`=" & MONTH_BOOST_LIMIT & ",`MAX_BOOST_LIMIT`=" & MAX_BOOST_LIMIT & ",`DIFICULT_LEVEL`=" & DIFICULT_LEVEL & ",`DELAI_AV_GAINS`=" & DELAI_AV_GAINS & ",`PORT`='" & PORT & "',`GOLD_POT`=" & GOLD_POT & ",`SILVER_POT`=" & SILVER_POT & ",`IRON_POT`=" & IRON_POT & ",`CAGNOTE_POT`=" & CAGNOTE_POT & ",`MSTERIOUS_POT`=" & MSTERIOUS_POT & ",`MEGA_POT`=" & MEGA_POT & ",`ID_CAGNOTE`=" & ID_CAGNOTE & ",`ID_GOLD`=" & ID_GOLD & ",`ID_SILVER`=" & ID_SILVER & ",`ID_IRON`=" & ID_IRON & ",`ID_MSTERIOUS`=" & ID_MSTERIOUS & ",`ID_MEGA`=" & ID_MEGA & ",`SCREEN_SELECT`='" & SCREEN_SELECT & "',`NB_SHARER`='" & NB_SHARER & "',`MAX_GAIIN_SILVER`=" & MAX_GAIIN_SILVER & ",`MIN_GAIIN_SILVER`=" & MIN_GAIIN_SILVER & ",`MAX_GAIN_IRON`=" & MAX_GAIN_IRON & ",`MIN_GAIN_IRON`=" & MIN_GAIN_IRON & ",`MIN_LOTO_PLASMA`=" & MIN_LOTO_PLASMA & ",`MIN_PARI_JACKPOT`=" & MIN_PARI_JACKPOT & ", DEDUCT_JACK_TO_TURN_OVER=" & DEDUCT_JACK_TO_TURN_OVER & " WHERE `id_bookmaker`=" & id_bookmaker
            MyBase.InitializeCommand()

            SaveSetting = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function creditDebitPlayer(ByVal idplayer As String, ByVal caisse As String, ByVal montant As Integer) As Boolean
        Try

            MyBase.SQL = "INSERT INTO credit_player (id_player,id_caisse,montant) values ( " & idplayer & "," & caisse & "," & montant.ToString & " )"
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            creditDebitPlayer = ExecuteStoredProcedure()

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function ListUser(ByVal id_bookmaker As String) As DataSet
        Try
            ListUser = New DataSet
            MyBase.SQL = "SELECT ID_USER,NOM,PRENOM,LOGIN,PWD as PASSWORD,user_typ ,ut.id_user_typ  from users u, point_vente pv, users_type ut WHERE u.id_pv=pv.id_pv AND u.id_user_typ=ut.id_user_typ AND pv.bookmaker_id=" & id_bookmaker
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(ListUser, "users")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function ListUserNom(ByVal UserName As String) As DataSet
        Try
            ListUserNom = New DataSet
            MyBase.SQL = "Select IdUser as [Compte] from Users where IdUser=@Username Order by IdUser"
            MyBase.InitializeCommand()
            MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, _
                15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(ListUserNom, "Users")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function ConPossible(ByVal UserName As String, ByVal UserPassword As String, ByVal Bookmaker_credential As String) As Boolean
        Try
            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            MyBase.SQL = "SELECT `NOM`, `PRENOM`, `LOGIN`, `PWD`, `id_user_typ`,p.id_pv, b.certificat as certificat FROM users u, point_vente p, bookmaker b WHERE b.bookmaker_id = p.bookmaker_id AND p.id_pv = u.id_pv AND LOGIN='" & UserName & "' AND PWD='" & UserPassword & "' AND b.certificat='" & Bookmaker_credential & "' " &
                " UNION SELECT `NOM`, `PRENOM`, `LOGIN`, `PWD`, `id_user_typ`,p.id_pv, 0 as certificat FROM users u, point_vente p WHERE p.id_pv = u.id_pv AND LOGIN='" & UserName & "' AND PWD='" & UserPassword & "' AND id_user_typ=0"
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

            'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

            MyBase.FillDataSet(LoginDataset, "users")

            If LoginDataset.Tables("users").Rows.Count > 0 Then
                ConPossible = True
            Else
                ConPossible = False
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function can(ByVal use As String) As Boolean
        Try
            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            MyBase.SQL = "SELECT * FROM users WHERE ID_USER=" & use
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

            'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

            MyBase.FillDataSet(LoginDataset, "users")

            If LoginDataset.Tables("users").Rows.Count > 0 AndAlso {My.Settings.AdminID, My.Settings.SuperAdminID}.Contains(LoginDataset.Tables("users").Rows(0).Item("id_user_typ")) Then
                can = True
            Else
                can = False
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetUserID(ByVal UserName As String, ByVal UserPassword As String) As Integer
        Try
            Dim LoginDataset As DataSet
            LoginDataset = New DataSet

            MyBase.SQL = "SELECT * FROM users WHERE LOGIN='" & UserName & "' AND PWD='" & UserPassword & "';"
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

            'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

            MyBase.FillDataSet(LoginDataset, "users")

            If LoginDataset.Tables("users").Rows.Count > 0 Then
                GetUserID = LoginDataset.Tables("users").Rows(0).Item("ID_USER")
            Else
                GetUserID = 0
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetUserinfo(ByVal UserName As String, ByVal UserPassword As String, ByVal Bookmaker_credential As String) As String
        Try
            Dim LoginDataset As DataSet
            LoginDataset = New DataSet
            MyBase.SQL = "SELECT ID_USER, `NOM`, `PRENOM`, `LOGIN`, `PWD`, `id_user_typ`,p.id_pv as ID_PV, b.certificat as certificat, b.bookmaker_id FROM users u, point_vente p, bookmaker b WHERE b.bookmaker_id = p.bookmaker_id AND p.id_pv = u.id_pv AND LOGIN='" & UserName & "' AND PWD='" & UserPassword & "' AND b.certificat='" & Bookmaker_credential & "'"
            MyBase.InitializeCommand()

            'MyBase.AddParameter("@Username", Data.OleDb.OleDbType.VarChar, 15, UserName)

            'MyBase.AddParameter("@UserPassword", Data.OleDb.OleDbType.VarChar,15, UserPassword)

            MyBase.FillDataSet(LoginDataset, "users")

            If LoginDataset.Tables("users").Rows.Count > 0 Then
                GetUserinfo = LoginDataset.Tables("users").Rows(0).Item("ID_USER") & "/" & LoginDataset.Tables("users").Rows(0).Item("PRENOM") & "/" & LoginDataset.Tables("users").Rows(0).Item("ID_PV") & "/" & LoginDataset.Tables("users").Rows(0).Item("id_user_typ") & "/" & LoginDataset.Tables("users").Rows(0).Item("bookmaker_id")
            Else
                GetUserinfo = "0/0/0/0"
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function UserParID(ByVal UserName As String) As DataSet
        Try
            UserParID = New DataSet
            MyBase.SQL = "Select * from users where ID_USER =" & UserName

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, UserName)

            MyBase.FillDataSet(UserParID, "users")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getallmode() As DataSet
        Try
            getallmode = New DataSet
            MyBase.SQL = "SELECT * FROM `mode_jeux`"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(getallmode, "mode_jeux")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetUserParNom(ByVal UserName As String) As DataSet
        Try
            GetUserParNom = New DataSet
            MyBase.SQL = "Select * from users where LOGIN ='" & UserName & "'"

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, UserName)

            MyBase.FillDataSet(GetUserParNom, "users")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function GetUserTyp() As DataSet
        Try
            GetUserTyp = New DataSet
            MyBase.SQL = "Select * from users_type "

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, UserName)

            MyBase.FillDataSet(GetUserTyp, "users_type")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getBookMakerlist() As DataSet
        Try
            getBookMakerlist = New DataSet
            MyBase.SQL = "SELECT * FROM `bookmaker` where bookmaker_id <> 0"

            MyBase.InitializeCommand()


            MyBase.FillDataSet(getBookMakerlist, "bookmaker")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetSetting(ByVal id_bookmaker As String) As DataSet
        Try
            GetSetting = New DataSet
            MyBase.SQL = "SELECT * FROM `setting` WHERE `id_bookmaker` =" & id_bookmaker

            MyBase.InitializeCommand()


            MyBase.FillDataSet(GetSetting, "setting")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function



    Public Function SupUser(ByVal UserName As String) As Boolean
        Try
            MyBase.SQL = "Delete from users where ID_USER =" & UserName

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, UserName)
            'exécuter la procédure stockée
            SupUser = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ListPlayers(ByVal id_Pv As String) As DataSet
        Try
            ListPlayers = New DataSet
            MyBase.SQL = "SELECT ID_USER,NOM,PRENOM,LOGIN,user_typ,ut.id_user_typ from users u,users_type ut WHERE u.id_user_typ=ut.id_user_typ AND u.id_user_typ=3 AND u.id_pv='" & id_Pv & "' "
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(ListPlayers, "users")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
#End Region

End Class
