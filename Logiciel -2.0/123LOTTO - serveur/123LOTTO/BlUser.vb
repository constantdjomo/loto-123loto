﻿Public Class BlUser
    Implements IDisposable
    Private oDLUser As DL_Lotto.DlUser
    Private disposedValue As Boolean = False        ' Détection des appels rédondants

    ' IDisposable
#Region "Constructeur et Destructeur"
    Public Sub New(ByVal Constring As String)
        oDLUser = New DL_Lotto.DlUser(Constring)
    End Sub
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: Libérer les ressources non gérées lors d'appel expplicite
            End If
            oDLUser.Dispose()
            oDLUser = Nothing
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region
#Region " Public User Functions "
    Function DejaUser(ByVal UserName As String) As Boolean
        Try
            Dim UserDataSet As DataSet
            UserDataSet = New DataSet
            UserDataSet = oDLUser.UserParID(UserName)
            If UserDataSet.Tables("users").Rows.Count > 0 Then
                DejaUser = True
            Else
                DejaUser = False
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function getallmode() As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getallmode = oDLUser.getallmode
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function NvDSUser() As DataSet
        Try
            'instancie un nouveau objet DataSet 
            NvDSUser = New DataSet

            'crée un objet Datatable 
            Dim objDataTable As DataTable = NvDSUser.Tables.Add("users")

            'crée un objet DataColumn 
            Dim objDataColumn As DataColumn


            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("ID_USER", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = True
            objDataColumn.MaxLength = 20
            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)

            
            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("NOM", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = False
            objDataColumn.MaxLength = 20
            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)

            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("PRENOM", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = False
            objDataColumn.MaxLength = 20
            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)

            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("LOGIN", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = False
            objDataColumn.MaxLength = 20
            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)

            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("id_user_typ", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = False
            objDataColumn.MaxLength = 20
            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)


            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("PWD", _
                Type.GetType("System.String"))
            objDataColumn.MaxLength = 20
            'Add the column to the table
            objDataTable.Columns.Add(objDataColumn)


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ListUser(ByVal id_bookmaker As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            ListUser = oDLUser.ListUser(id_bookmaker)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ListPlayers(ByVal id_pv As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            ListPlayers = oDLUser.ListPlayers(id_pv)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function




    Public Function GetUserTyp() As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetUserTyp = oDLUser.GetUserTyp
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetSetting(ByVal id_bookmaker As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetSetting = oDLUser.GetSetting(id_bookmaker)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getBookMakerlist() As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            getBookMakerlist = oDLUser.getBookMakerlist()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function UserParNom(ByVal UserName As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir un utilisateur spécifique
            UserParNom = oDLUser.UserParID(UserName)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ConPossible(ByVal UserName As String, ByVal UserPassword As String, ByVal Bookmaker_credential As String) As Boolean
        Try

            ConPossible = oDLUser.ConPossible(UserName, UserPassword, Bookmaker_credential)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function can(ByVal use As String) As Boolean
        Try

            can = oDLUser.can(use)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function



    Public Function GetUserID(ByVal UserName As String, ByVal UserPassword As String) As Integer
        Try

            GetUserID = oDLUser.GetUserID(UserName, UserPassword)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetUserinfo(ByVal UserName As String, ByVal UserPassword As String, ByVal bookmaker_credential As String) As String
        Try

            GetUserinfo = oDLUser.GetUserinfo(UserName, UserPassword, bookmaker_credential)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function AjouterUser(ByVal User As DataSet) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDLUser.AjouterUser(User)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function MajUser(ByVal User As DataSet) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour mettre à jour l'utilisateur
            Return oDLUser.MajUser(User)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    'Public Function SaveSetting(ByVal DIFICULT_LEVEL As Integer, ByVal DELAI_AV_GAINS As Integer, ByVal GOLD_POT As Integer, ByVal SILVER_POT As Integer, ByVal IRON_POT As Integer, ByVal CAGNOTE_POT As Integer, ByVal MSTERIOUS_POT As Integer, ByVal ID_GOLD As Integer, ByVal ID_SILVER As Integer, ByVal ID_IRON As Integer, ByVal ID_MSTERIOUS As Integer, ByVal MIN_GAIIN_SILVER As Integer, ByVal MAX_GAIIN_SILVER As Integer, ByVal PORT As String, ByVal SCREEN_SELECT As String) As Boolean
    '    Try
    '        'Valide les données de l'utilisateur

    '        'Appel le composant de données pour mettre à jour l'utilisateur
    '        Return oDLUser.SaveSetting(DIFICULT_LEVEL, DELAI_AV_GAINS, GOLD_POT, SILVER_POT, IRON_POT, CAGNOTE_POT, MSTERIOUS_POT, ID_GOLD, ID_SILVER, ID_IRON, ID_MSTERIOUS, MIN_GAIIN_SILVER, MAX_GAIIN_SILVER, PORT, SCREEN_SELECT)
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message,
    '            ExceptionErr.InnerException)
    '    End Try
    'End Function

    Public Function SaveSetting(ByVal id_bookmaker As String, ByVal MONTH_BOOST_LIMIT As String, ByVal MAX_BOOST_LIMIT As String, ByVal DIFICULT_LEVEL As String, ByVal DELAI_AV_GAINS As String, ByVal PORT As String, ByVal GOLD_POT As String, ByVal SILVER_POT As String, ByVal IRON_POT As String, ByVal CAGNOTE_POT As String, ByVal MSTERIOUS_POT As String, ByVal MEGA_POT As String, ByVal ID_CAGNOTE As String, ByVal ID_GOLD As String, ByVal ID_SILVER As String, ByVal ID_IRON As String, ByVal ID_MSTERIOUS As String, ByVal ID_MEGA As String, ByVal SCREEN_SELECT As String, ByVal NB_SHARER As String, ByVal MAX_GAIIN_SILVER As String, ByVal MIN_GAIIN_SILVER As String, ByVal MAX_GAIN_IRON As String, ByVal MIN_GAIN_IRON As String, ByVal MIN_LOTO_PLASMA As String, ByVal MIN_PARI_JACKPOT As String, ByVal DEDUCT_JACK_TO_TURN_OVER As String) As Boolean
        Try
            'Valide les données de l'utilisateur
            'Appel le composant de données pour mettre à jour l'utilisateur
            Return oDLUser.SaveSetting(id_bookmaker, MONTH_BOOST_LIMIT, MAX_BOOST_LIMIT, DIFICULT_LEVEL, DELAI_AV_GAINS, PORT, GOLD_POT, SILVER_POT, IRON_POT, CAGNOTE_POT, MSTERIOUS_POT, MEGA_POT, ID_CAGNOTE, ID_GOLD, ID_SILVER, ID_IRON, ID_MSTERIOUS, ID_MEGA, SCREEN_SELECT, NB_SHARER, MAX_GAIIN_SILVER, MIN_GAIIN_SILVER, MAX_GAIN_IRON, MIN_GAIN_IRON, MIN_LOTO_PLASMA, MIN_PARI_JACKPOT, DEDUCT_JACK_TO_TURN_OVER)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function creditDebitPlayer(ByVal idplayer As String, ByVal caisse As String, ByVal montant As Integer) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour mettre à jour l'utilisateur
            Return oDLUser.creditDebitPlayer(idplayer, caisse, montant)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function SupUser(ByVal UserName As String) As Boolean
        Try
            'Appel le composant de données pour supprimer l'utilisateur
            Return oDLUser.SupUser(UserName)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function
#End Region

End Class
