﻿Public Class BlJackot
    Implements IDisposable
    Private oDlJackpot As DL_Lotto.DlJackpot
    Private disposedValue As Boolean = False        ' Détection des appels rédondants

    ' IDisposable
#Region "Constructeur et Destructeur"
    Public Sub New(ByVal Constring As String)
        oDlJackpot = New DL_Lotto.DlJackpot(Constring)
    End Sub
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: Libérer les ressources non gérées lors d'appel expplicite
            End If
            oDlJackpot.Dispose()
            oDlJackpot = Nothing
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region
#Region " Public Ticket Functions "

    'Public Function Maj_mon_Jackpot(ByVal id_jack As String, ByVal id_lot_encour As String, ByVal montant As String, ByVal etat_jack As String, ByVal is_material As Boolean, ByVal NomLot As String) As Boolean
    '    Try
    '        'Valide les données de l'utilisateur

    '        'Appel le composant de données pour ajouter le nouvel utilisateur
    '        Return oDlJackpot.Maj_mon_Jackpot(id_jack, id_lot_encour, montant, etat_jack, is_material, NomLot)
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message,
    '            ExceptionErr.InnerException)
    '    End Try
    'End Function
    Public Function Maj_mon_Jackpot(ByVal id_jack As String, ByVal id_bookmaker As String, ByVal id_lot_encour As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlJackpot.Maj_mon_Jackpot(id_jack, id_bookmaker, id_lot_encour)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function Maj_mon_Jackpot(ByVal id_jack As String, ByVal id_bookmaker As String, ByVal id_pv As String, ByVal id_lot_encour As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlJackpot.Maj_mon_Jackpot(id_jack, id_bookmaker, id_pv, id_lot_encour)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetListPointVente(ByVal id_bookmaker As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetListPointVente = oDlJackpot.GetListPointVente(id_bookmaker)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetGagnantTicketJackpot(ByVal id_ticket As String) As DataSet
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlJackpot.GetGagnantTicketJackpot(id_ticket)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function


    'Public Function Maj_mon_Lot(ByVal id_lot As String, ByVal id_pv As String, ByVal montant As String, ByVal etat_jackp As String, ByVal is_material As Boolean, ByVal NomLot As String, ByVal share As String) As Boolean
    '    Try
    '        'Valide les données de l'utilisateur

    '        'Appel le composant de données pour ajouter le nouvel utilisateur
    '        Return oDlJackpot.Maj_mon_Lot(id_lot, id_pv, montant, etat_jackp, is_material, NomLot, share)
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message,
    '            ExceptionErr.InnerException)
    '    End Try
    'End Function


    Public Function Maj_mon_Lot(ByVal id_lot As String, ByVal montant As String, ByVal is_material As Boolean, ByVal NomLot As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlJackpot.Maj_mon_Lot(id_lot, montant, is_material, NomLot)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function Maj_mon_Lot_Set(ByVal id_lot As String, ByVal id_pv As String, ByVal montant As String, ByVal etat_jackp As String, ByVal is_material As Boolean, ByVal NomLot As String, ByVal schow_etat As String, ByVal status As String, ByVal fname As String, ByVal videoFileName As String) As Boolean

        Try
            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlJackpot.Maj_mon_Lot_Set(id_lot, id_pv, montant, etat_jackp, is_material, NomLot, schow_etat, status, fname, videoFileName)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try

    End Function

    Public Function Ajouter_lot_Jackpot(ByVal id_jack As String, ByVal montant As String, ByVal is_material As Boolean, ByVal NomLot As String, ByVal photo As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlJackpot.Ajouter_lot_Jackpot(id_jack, montant, is_material, NomLot, photo)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function suprim_lot_Jackpot(ByVal id_lot As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlJackpot.suprim_lot_Jackpot(id_lot)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function


    Public Function verifWinnerJackpot(ByVal id_jackp As String, ByVal tirage As String, ByRef idticket As String(), ByVal NB_SHARER As Integer, ByVal is_loto_plasma As Boolean, ByVal MIN_LOTO_PLASMA As Integer, ByVal MIN_LOTO_JACKPOT As Integer) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlJackpot.verifWinnerJackpot(id_jackp, tirage, idticket, NB_SHARER, is_loto_plasma, MIN_LOTO_PLASMA, MIN_LOTO_JACKPOT)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    ' le paramettre "is_loto_plasma" permet de restrainde les ticket qui gagnerons le jackpot a un montant minimum par Ticket

    Public Function verifWinnerJackpot_lot(ByVal id_lo As String, ByVal tirage As String, ByRef idticket As String(), ByVal NB_SHARER As Integer, ByVal is_loto_plasma As Boolean, ByVal MIN_LOTO_PLASMA As Integer, ByVal MIN_LOTO_JACKPOT As Integer) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlJackpot.verifWinnerJackpot_lot(id_lo, tirage, idticket, NB_SHARER, is_loto_plasma, MIN_LOTO_PLASMA, MIN_LOTO_JACKPOT)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function Maj_Satut_Jackpot(ByVal id_bookmaker As String, ByVal id_jack As String, ByVal montant As String) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDlJackpot.Maj_Satut_Jackpot(id_bookmaker, id_jack, montant)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetMontantJackpot(ByVal id_jackp As String) As String
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetMontantJackpot = oDlJackpot.GetMontantJackpot(id_jackp)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetEtatJackpot(ByVal id_jackp As String) As String
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetEtatJackpot = oDlJackpot.GetEtatJackpot(id_jackp)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetNomLotJackpot(ByVal id_jackp As String) As String
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetNomLotJackpot = oDlJackpot.GetNomLotJackpot(id_jackp)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function



    Public Function GetJackpot(ByVal id_jackp As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetJackpot = oDlJackpot.GetJackpot(id_jackp)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function Getsalle(ByVal id_bookmaker As String) As DataSet
        Try
            'retourner la liste des salles
            Getsalle = oDlJackpot.Getsalle(id_bookmaker)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetJackpot(ByVal id_jackp As String, ByVal id_pv As String, ByVal id_bookmaker As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetJackpot = oDlJackpot.GetJackpot(id_jackp, id_pv, id_bookmaker)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetJackpotForTick(ByVal COD_BAR As String, ByVal id_jack As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetJackpotForTick = oDlJackpot.GetJackpotForTick(COD_BAR, id_jack)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetLastJackpot(ByVal bookmaker_id As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetLastJackpot = oDlJackpot.GetLastJackpot(bookmaker_id)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetMessage(ByVal id_bookmaker As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetMessage = oDlJackpot.GetMessage(id_bookmaker)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function Getlot(ByVal id_lot As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            Getlot = oDlJackpot.Getlot(id_lot)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetListlot(ByVal id_jackp As String, ByVal id_bookmaker As String, ByVal id_pv As String, Optional is_a_radom As Boolean = False) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetListlot = oDlJackpot.GetListlot(id_jackp, id_bookmaker, id_pv, is_a_radom)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetlotEnCours(ByVal id_bookmaker As String, ByVal id_jackp As String, ByVal id_pv As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetlotEnCours = oDlJackpot.GetlotEnCours(id_bookmaker, id_jackp, id_pv)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetListlot(ByVal id_bookmaker As String, ByVal id_jackp As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetListlot = oDlJackpot.GetListlot(id_bookmaker, id_jackp)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetListPv(Optional id_pv As String = "") As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetListPv = oDlJackpot.GetListPv(id_pv)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetMntPariEncourBookmaker(ByVal id_bookmaker As String, ByVal tirage As String, ByVal ID_MODE As String) As String
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetMntPariEncourBookmaker = oDlJackpot.GetMntPariEncourBookmaker(id_bookmaker, tirage, ID_MODE)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function
    Public Function GetMontantPariEncour(ByVal tirage As String, ByVal ID_MODE As String, ByVal id_pv As String) As Integer
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetMontantPariEncour = oDlJackpot.GetMontantPariEncour(tirage, ID_MODE, id_pv)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetMontStatutJack(ByVal id_jackp As String) As String
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetMontStatutJack = oDlJackpot.GetMontStatutJack(id_jackp)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function
#End Region
End Class
