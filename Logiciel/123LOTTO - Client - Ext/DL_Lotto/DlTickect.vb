﻿Public Class DlTickect
    Inherits DlBase

#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region

#Region " Public Role Functions "

    Public Function ValideTicket(ByVal idticket As String) As Boolean
        Try


            MyBase.SQL = "UPDATE ticket SET STATUS = 0 WHERE ID_TICKET=" & idticket
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            ValideTicket = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ServirTicket(ByVal idticket As String, ByVal user As String) As Boolean
        Try


            MyBase.SQL = "UPDATE ticket SET ID_USER_S=" & user & " WHERE ID_TICKET=" & idticket
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            ServirTicket = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function ServirTicketJack(ByVal idticket As String, ByVal user As String) As Boolean
        Try


            MyBase.SQL = "UPDATE gagner_jackp SET ID_USER_S=" & user & " WHERE id_tick=" & idticket
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            ServirTicketJack = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function AjouterTicket(ByVal Ticket As DataSet, ByRef idticket As String) As Boolean
        Try

            MyBase.SQL = "INSERT INTO ticket (COD_BAR,ID_USER,NUM_TIRAGE) values ( '" + Ticket.Tables("ticket").Rows(0).Item("COD_BAR") + "'," + Ticket.Tables("ticket").Rows(0).Item("ID_USER") + "," + Ticket.Tables("ticket").Rows(0).Item("NUM_TIRAGE") + " ) ; SELECT LAST_INSERT_ID()"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            idticket = CStr(ExecuteStoredScalar())
            'MsgBox(tes)
            'AjouterTicket = CBool(idticket)
            If (CBool(idticket)) Then

                Dim tick As Integer = CInt(idticket)
                'MsgBox(tick.ToString("00000000"))
                MyBase.SQL = "UPDATE ticket SET COD_BAR = CONCAT('" & tick.ToString("00000000") & "', COD_BAR)  WHERE ID_TICKET=" & idticket
                'Initialisation l'objet command
                MyBase.InitializeCommand()

                AjouterTicket = ExecuteStoredProcedure()
            Else
                AjouterTicket = False
            End If



        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function AjouterParie(ByVal ID_TICKET_P As String, ByVal ID_MODE As String, ByVal MONTANT_MISE As String, ByVal Tab_Num() As String) As Boolean
        Try

            Dim ok As Boolean = True
            Dim ID_PARI As String
            MyBase.SQL = "INSERT INTO pari (ID_PARI,ID_MODE,MONTANT_MISE) values (NULL," & ID_MODE & "," & MONTANT_MISE & "); SELECT LAST_INSERT_ID()"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            ID_PARI = CStr(ExecuteStoredScalar())
            ok = CBool(ID_PARI)


            MyBase.SQL = "INSERT INTO concerner (ID_TICKET,ID_PARI) values (" & ID_TICKET_P & "," & ID_PARI & ")"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            ok = ok AndAlso ExecuteStoredProcedure()


            For i As Integer = 0 To Tab_Num.Length - 1
                ok = ok AndAlso Me.ajouterNumJouer(Tab_Num(i), ID_PARI)
            Next

            AjouterParie = ok

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function



    Public Function ajouterNumJouer(ByVal num As String, ByVal tick As String) As Boolean
        Try
            MyBase.SQL = "INSERT INTO avoir (NUMEROS,ID_PARI) values ( " + num + "," & tick & " )"
            'Initialisation l'objet command 
            MyBase.InitializeCommand()

            ajouterNumJouer = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    'Public Function MajUser(ByVal User As DataSet) As Boolean
    '    Try
    '        MyBase.SQL = "Update  Users Set Pwd=@Pwd where  IdUser=@IdUser"
    '        MyBase.InitializeCommand()

    '        MyBase.AddParameter("@Pwd", _
    '             Data.OleDb.OleDbType.VarChar, 20, _
    '            User.Tables("Users").Rows(0).Item("Pwd"))

    '        MyBase.AddParameter("@IdUser", _
    '            Data.OleDb.OleDbType.VarChar, 15, _
    '            User.Tables("Users").Rows(0).Item("IdUser"))
    '        'Exécution de la 
    '        MajUser = ExecuteStoredProcedure()
    '    Catch ExceptionErr As Exception
    '        Throw New System.Exception(ExceptionErr.Message, _
    '        ExceptionErr.InnerException)
    '    End Try
    'End Function

    Public Function ListMode() As DataSet
        Try
            ListMode = New DataSet
            MyBase.SQL = "Select ID_MODE,MODE from mode_jeux Order by ID_MODE"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(ListMode, "mode_jeux")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetQuota(ByVal id_mode As String) As Integer
        Try
            Dim dss As New DataSet

            MyBase.SQL = "Select QUOTA from mode_jeux WHERE ID_MODE=" & id_mode
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "mode_jeux")
            GetQuota = CInt(dss.Tables("mode_jeux").Rows(0).Item("QUOTA"))
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function GetLastTickectNum() As DataSet
        Try
            GetLastTickectNum = New DataSet
            MyBase.SQL = "Select ID_TICKET from ticket where ID_TICKET >= ALL(Select ID_TICKET from ticket)"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(GetLastTickectNum, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetLastPariNum() As DataSet
        Try
            GetLastPariNum = New DataSet
            MyBase.SQL = "Select ID_PARI from pari where ID_PARI >= ALL(Select ID_PARI from pari)"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(GetLastPariNum, "pari")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetLastTirageNum() As DataSet
        Try
            GetLastTirageNum = New DataSet
            MyBase.SQL = "Select NUM_TIRAGE from tirage where NUM_TIRAGE >= ALL(Select NUM_TIRAGE from tirage)"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(GetLastTirageNum, "tirage")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetTypeMode(ByVal Mode As String) As DataSet
        Try
            GetTypeMode = New DataSet
            MyBase.SQL = "Select NB_NUM_JOUER from mode_jeux where MODE ='" & Mode & "'"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(GetTypeMode, "mode_jeux")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetMode(ByVal Mode As String) As DataSet
        Try
            GetMode = New DataSet
            MyBase.SQL = "Select * from mode_jeux where ID_MODE ='" & Mode & "'"
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(GetMode, "mode_jeux")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function List_Ticket()
        Try
            List_Ticket = New DataSet
            MyBase.SQL = "Select * from ticket ORDER BY ID_TICKET"
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(List_Ticket, "ticket")
        Catch ex As Exception
            Throw New System.Exception(ex.Message, _
            ex.InnerException)
        End Try
    End Function


    Public Function TicketParId(ByVal id_Ticket As String) As DataSet
        Try
            TicketParId = New DataSet
            MyBase.SQL = "Select * from ticket t where  t.ID_TICKET=" & id_Ticket

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(TicketParId, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function TicketParCodebar(ByVal id_Ticket As String) As DataSet
        Try
            TicketParCodebar = New DataSet
            MyBase.SQL = "Select * from ticket t where  t.COD_BAR=" & id_Ticket

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(TicketParCodebar, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function TicketNonPerimParId(ByVal id_Ticket As String) As DataSet
        Try
            TicketNonPerimParId = New DataSet
            MyBase.SQL = "Select * from ticket t where STATUS=1 AND t.ID_TICKET=" & id_Ticket

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(TicketNonPerimParId, "ticket")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function



    Public Function verifWinner(ByVal id_Ticket As String, ByVal tirage As String) As Boolean
        Try
            Dim dsnumero, dsTirage As New DataSet
            Dim ok, verif As Boolean
            ok = False
            verif = True
            dsnumero = Me.getNumJouer(id_Ticket)

            dsTirage = Me.getTirage(tirage)
            Dim j As Integer

            For i As Integer = 0 To dsnumero.Tables("avoir").Rows.Count - 1
                j = 0
                While (j < dsTirage.Tables("contenir").Rows.Count) And (Not ok)
                    ok = (dsTirage.Tables("contenir").Rows(j).Item("ID_NUM_TIRER") = dsnumero.Tables("avoir").Rows(i).Item("NUMEROS"))
                    j += 1
                End While
                verif = verif And ok
                ok = False
            Next
            verifWinner = verif


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getNumJouer(ByVal ID_PARI As String) As DataSet
        Try
            getNumJouer = New DataSet
            MyBase.SQL = "Select NUMEROS from avoir where ID_PARI=" & ID_PARI

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(getNumJouer, "avoir")


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function getTirage(ByVal num_tirage As String) As DataSet
        Try
            getTirage = New DataSet
            MyBase.SQL = "Select ID_NUM_TIRER from contenir where NUM_TIRAGE=" & num_tirage

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(getTirage, "contenir")


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetPari(ByVal num_ticket As String) As DataSet
        Try
            GetPari = New DataSet
            MyBase.SQL = "Select p.ID_PARI,p.ID_MODE,MODE,NB_NUM_JOUER,QUOTA,MONTANT_MISE from ticket t, pari p ,concerner c,mode_jeux m where m.ID_MODE=p.ID_MODE AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND t.ID_TICKET=" & num_ticket

            MyBase.InitializeCommand()

            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar, 15, id_Ticket)

            MyBase.FillDataSet(GetPari, "pari")


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function


#End Region
End Class
