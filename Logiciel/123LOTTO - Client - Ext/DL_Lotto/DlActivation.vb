﻿Public Class DlActivation
    Inherits DlBase
    Private oCompte As DlCompte
    Private oTicket As DlTickect
#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
        oCompte = New DlCompte(ConString)
        oTicket = New DlTickect(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region

#Region " Public Role Functions "
    
    Public Function CleExist(ByVal cle As String) As Boolean
        Try
            Dim newKeyDatset As DataSet
            Dim montant_residuel As Integer



            newKeyDatset = New DataSet

            MyBase.SQL = "SELECT `id_cle`,`cle`,`etat`,`used` FROM cle_activation WHERE cle='" & cle & "' AND used = 0 "
            MyBase.InitializeCommand()

            MyBase.FillDataSet(newKeyDatset, "cle_activation")

            If newKeyDatset.Tables("cle_activation").Rows.Count > 0 Then
                Try
                    montant_residuel = oCompte.getCreditResiduel()

                    MyBase.SQL = "UPDATE cle_activation SET etat=0 WHERE 1"
                    MyBase.InitializeCommand()
                    ExecuteStoredProcedure()

                    MyBase.SQL = "UPDATE cle_activation SET etat=1,used=1,montant = (montant + " & montant_residuel & ") WHERE id_cle=" & newKeyDatset.Tables("cle_activation").Rows(0).Item("id_cle")
                    MyBase.InitializeCommand()
                    CleExist = ExecuteStoredProcedure()
                Catch ExceptionErr As Exception
                    Throw New System.Exception(ExceptionErr.Message,
                    ExceptionErr.InnerException)
                End Try
            Else
                CleExist = False
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function IsActivated() As Boolean
        Try
            Dim montant_residuel As Integer
            montant_residuel = oCompte.getCreditResiduel()
            'MsgBox(montant_residuel)
            'credit illimite
            If (montant_residuel = -1) Then
                Return True
            End If


            If (montant_residuel > 0) Then
                IsActivated = True
            Else
                IsActivated = False
            End If

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
            ExceptionErr.InnerException)
        End Try
    End Function




#End Region

End Class
