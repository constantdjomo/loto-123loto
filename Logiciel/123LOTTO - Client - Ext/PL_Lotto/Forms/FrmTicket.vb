﻿Imports System.ComponentModel
Imports System.Text
Imports System.Drawing.Drawing2D
Imports Spire.Barcode
Imports Zen.Barcode

Imports System.Drawing.Printing
Imports System.Management
Imports System.IO

Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports System.Globalization


Public Class FrmTicket
    Inherits Form
    Public settings As BarcodeSettings
    Public Mode_selected As Integer = 0
    Public Tab_Numero(92) As Integer
    Public MiseTicket As String = "0"
    Private ODsTicket As BL_Lotto.BlTicket
    Private DSticket, dsmode As New DataSet

    ' Constant variable holding the Printer name.
    Private PRINTER_NAME As String = My.Settings.PRINTER_NAME
    Private ticket As Integer = 0
    ' Variables/Objects.
    Private WithEvents pdPrint, AnTickPrint, TickPrint, JackTickPrint As PrintDocument
    Dim printdialog As PrintDialog
    Private WithEvents prdocument As PrintDocument
    Dim isFinish As Boolean
    Dim cancelErr As Boolean
    Dim isTimeout As Boolean
    Dim QUOTA As Integer
    Dim ID_TICKET_P As String
    Dim ID_TICKET, COD_SAL As String
    Dim Gains As Integer = 0
    Dim GainsJackpot As Integer = 0
    Dim Coleu As Drawing.Color

    Dim MonSocketClient As Socket
    Dim MonThread As Thread

    'Delegate pour écrire un message
    Delegate Sub dToggleVoyant()
    Private Sub ToggleVoyant()
        Dim min, sec As Double

        If (TirageLancer = 0) Then
            PbVoyant.Image = Global.PL_Lotto.My.Resources.Resources.Vvert : PbVoyant.Refresh()
        Else
            PbVoyant.Image = Global.PL_Lotto.My.Resources.Resources.Vr : PbVoyant.Refresh()
        End If

        min = Math.Truncate(timesent / 60)
        sec = ((timesent / 60) - (Math.Truncate(timesent / 60))) * 60
        LB_second.Text = CInt(sec).ToString : LB_second.Refresh()
        Lb_Min.Text = min.ToString : Lb_Min.Refresh()

    End Sub

    Private Sub gererConexion()

        Dim ip As String = My.Settings.HOST
        'MsgBox(ip)
        'ip = "192.164.1.5"

        MonSocketClient = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp) 'Initialise le socket
        'MsgBox(ip)
        Try
            Dim MonEP As IPEndPoint = New IPEndPoint(IPAddress.Parse(ip), CInt(My.Settings.PORT)) 'Entre les informations de connexion
            MonSocketClient.Connect(MonEP) 'Tente de se connecter
            TraitementConnexion()
        Catch ex As Exception
            MessageBox.Show("Erreur lors de la tentative de connexion au serveur. Vérifiez l'ip et le port du serveur." & ex.ToString, "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub TraitementConnexion()
        'EcritureMessage("Connexion au serveur réussie !", 1)

        MonThread = New Thread(AddressOf ThreadLecture)
        MonThread.Start()
    End Sub

    Sub ThreadLecture()
        While (MonSocketClient.Connected) 'Tant qu'on est connecté au serveur
            Dim Bytes(255) As Byte
            Dim Recu As Integer
            Dim recep As String()
            Try
                Recu = MonSocketClient.Receive(Bytes)
            Catch ex As Exception 'Erreur si fermeture du socket pendant la réception
                MessageBox.Show("Connexion perdue, arrêt de la réception des données ...", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Dim Message As String
            Message = System.Text.Encoding.UTF8.GetString(Bytes)
            Message = Message.Substring(0, Recu)
            recep = Message.Split(CChar("/"))
            timesent = CInt(recep(1))

            TirageLancer = CInt(recep(0))

            ' c'est ici qu'on devrai modifier le picture box
            Me.Invoke(New dToggleVoyant(AddressOf ToggleVoyant))
            System.Threading.Thread.Sleep(1000)
        End While
    End Sub

    Private Sub FrmTicket_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not MonSocketClient Is Nothing Then 'Si le socket a été créé
            MonSocketClient.Close() 'On le ferme
        End If
        If Not MonThread Is Nothing Then 'Si le thread a été créé
            If MonThread.IsAlive Then 'S'il tourne
                MonThread.Abort() 'On le stoppe
            End If
        End If
    End Sub

    Private Sub FrmTicket_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'initialisation de l'imprimante par defaut 
        PRINTER_NAME = DefaultPrinterName()
        My.Settings.PRINTER_NAME = GetDefaultPrinterName()
        'Crée un thread pour traiter ce client et le démarre
        Dim ThreadClient As New Thread(AddressOf gererConexion)
        ThreadClient.Start()

        Using oTicket As New BL_Lotto.BlTicket(source)
            'Dim dsMode As New DataSet
            'dsMode = oTicket.ListMode
            'CB_ModJeu.SelectedIndex = -1
            'CB_ModJeu.DataSource = dsMode.Tables("mode_jeux")
            'CB_ModJeu.DisplayMember = "MODE"
            'CB_ModJeu.ValueMember = "ID_MODE"

            Dim dsTirage As New DataSet
            dsTirage = oTicket.GetLastTirageNum
            Dim num_tirage As Integer
            If dsTirage.Tables("tirage").Rows.Count > 0 Then
                num_tirage = CInt(dsTirage.Tables("tirage").Rows(0).Item("NUM_TIRAGE").ToString) + 1
                LB_NumTirage.Text = num_tirage.ToString
            Else
                LB_NumTirage.Text = "1"
            End If

            RadioButton1.PerformClick()
            RadioButton4.PerformClick()
        End Using
        'chargerTxtBx()
        TbIdticket.Focus()
        RaffraichirCaisse()
    End Sub
    Function AjouterTicket() As Boolean
        Dim code_bar As String = ""
        Dim Tabdate() As String
        'Dim retour As Boolean

        Tabdate = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
        code_bar = My.Settings.CODE_SALE

        Using OBlTicket As New BL_Lotto.BlTicket(source)
            'Get a new Project DataSet

            Dim oDataSet As DataSet
            oDataSet = OBlTicket.NvDSTickect()
            'Initialize a datarow object from the Project DataSet
            Dim oDataRow As Data.DataRow = oDataSet.Tables("ticket").NewRow
            'Set the values in the DataRow
            oDataRow.Item("COD_BAR") = code_bar

            Dim dsTirage As New DataSet
            dsTirage = OBlTicket.GetLastTirageNum
            Dim num_tirage As Integer
            If dsTirage.Tables("tirage").Rows.Count > 0 Then
                num_tirage = CInt(dsTirage.Tables("tirage").Rows(0).Item("NUM_TIRAGE").ToString) + 1
                LB_NumTirage.Text = num_tirage.ToString
            Else
                LB_NumTirage.Text = "1"
            End If
            LB_NumTirage.Refresh()

            oDataRow.Item("NUM_TIRAGE") = Trim(LB_NumTirage.Text)
            oDataRow.Item("ID_USER") = CONNECTED_USERID
            'QUOTA = OBlTicket.GetQuota(CStr(Mode_selected))
            'Add the DataRow to the DataSet
            oDataSet.Tables("ticket").Rows.Add(oDataRow)
            'Add the DataRow to the DataSet
            'Add the Project

            If Not OBlTicket.AjouterTicket(oDataSet, ID_TICKET_P) Then
                Throw New Exception("Echec d'enregistrement")
            Else
                AjouterTicket = True
            End If

        End Using
    End Function

    Public Function ajouterNumJouer(ByVal ID_PARI As String, ByVal Tab_Num() As String) As Boolean
        Using OBlTicket As New BL_Lotto.BlTicket(source)
            Dim dsMode, dsTick As New DataSet
            Dim ok As Boolean

            ok = True
            For i As Integer = 0 To Tab_Num.Length - 1

                ok = ok AndAlso OBlTicket.ajouterNumJouer(Tab_Num(i), ID_PARI)

            Next

            'If (CInt(dsMode.Tables("mode_jeux").Rows(0).Item("NB_NUM_JOUER").ToString) = 1) Then
            '    ok = OBlTicket.ajouterNumJouer(TB_NumJouer1.Text, dsTick.Tables("ticket").Rows(0).Item("ID_TICKET").ToString)
            'End If
            'If (CInt(dsMode.Tables("mode_jeux").Rows(0).Item("NB_NUM_JOUER").ToString) = 2) Then
            '    ok = OBlTicket.ajouterNumJouer(TB_NumJouer1.Text, dsTick.Tables("ticket").Rows(0).Item("ID_TICKET").ToString)
            '    ok = ok And OBlTicket.ajouterNumJouer(TB_NumJouer2.Text, dsTick.Tables("ticket").Rows(0).Item("ID_TICKET").ToString)

            'End If
            'If (CInt(dsMode.Tables("mode_jeux").Rows(0).Item("NB_NUM_JOUER").ToString) = 3) Then
            '    ok = OBlTicket.ajouterNumJouer(TB_NumJouer1.Text, dsTick.Tables("ticket").Rows(0).Item("ID_TICKET").ToString)
            '    ok = ok And OBlTicket.ajouterNumJouer(TB_NumJouer2.Text, dsTick.Tables("ticket").Rows(0).Item("ID_TICKET").ToString)
            '    ok = ok And OBlTicket.ajouterNumJouer(TB_NumJouer3.Text, dsTick.Tables("ticket").Rows(0).Item("ID_TICKET").ToString)
            'End If

            ajouterNumJouer = ok
        End Using

    End Function



    'Private Sub CB_ModJeu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CB_ModJeu.SelectedIndexChanged
    '    Using oTicket As New BL_Lotto.BlTicket(source)
    '        Dim dsMode As New DataSet

    '        dsMode = oTicket.GetTypeMode(CB_ModJeu.Text)
    '        If (dsMode.Tables("mode_jeux").Rows.Count > 0) Then
    '            For i As Integer = 0 To CInt(dsMode.Tables("mode_jeux").Rows(0).Item("NB_NUM_JOUER").ToString) - 1
    '                If (i = 0) Then
    '                    TB_NumJouer1.ReadOnly = False
    '                    TB_NumJouer2.ReadOnly = True
    '                    TB_NumJouer2.Text = ""
    '                    TB_NumJouer3.ReadOnly = True
    '                    TB_NumJouer3.Text = ""

    '                End If
    '                If (i = 1) Then
    '                    TB_NumJouer1.ReadOnly = False
    '                    TB_NumJouer2.ReadOnly = False
    '                    TB_NumJouer3.ReadOnly = True
    '                    TB_NumJouer3.Text = ""
    '                End If
    '                If (i = 2) Then
    '                    TB_NumJouer1.ReadOnly = False
    '                    TB_NumJouer2.ReadOnly = False
    '                    TB_NumJouer3.ReadOnly = False
    '                End If
    '            Next



    '        End If

    '    End Using
    'End Sub

    Public Function countTab_Numero(ByRef result As String) As Integer
        Dim elem As Integer = 0
        result = ""
        For i As Integer = 0 To Tab_Numero.Length - 1
            If Tab_Numero(i) = 1 Then
                elem = elem + 1
                If (elem = 1) Then
                    result = result & i
                Else
                    result = result & "-" & i
                End If

            End If
        Next
        Return elem
    End Function
    Public Sub viderdatagrid()
        With Dgd_Tick
            .Rows.Clear()
            .Columns.Clear()
        End With
        Dim code_bar As String = ""
        Dim tick As Integer
        tick = CInt(ID_TICKET_P)

        code_bar = tick.ToString("00000000") & My.Settings.CODE_SALE

        If (Dgv_LastTick.RowCount = 3) Then
            Dgv_LastTick.Rows.RemoveAt(2)
        End If

        With Dgv_LastTick
            '.Columns.Clear()
            .ColumnCount = 2
            '.ColumnHeadersVisible = False
            '{CStr(num_tirage), CStr(Mode_selected), resultat, MiseTicket}
            Dim row0 As String() = {code_bar, Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)}
            .Rows.Insert(0, row0)
        End With



    End Sub
    Public Sub remiseZero()
        For i As Integer = 0 To Tab_Numero.Length - 1
            Tab_Numero(i) = 0
        Next
        CheckBox1.Checked = False
        CheckBox2.Checked = False
        CheckBox4.Checked = False
        CheckBox3.Checked = False
        CheckBox5.Checked = False
        CheckBox6.Checked = False
        CheckBox7.Checked = False
        CheckBox8.Checked = False
        CheckBox9.Checked = False
        CheckBox10.Checked = False

        CheckBox11.Checked = False
        CheckBox12.Checked = False
        CheckBox13.Checked = False
        CheckBox14.Checked = False
        CheckBox15.Checked = False
        CheckBox16.Checked = False
        CheckBox17.Checked = False
        CheckBox18.Checked = False
        CheckBox19.Checked = False
        CheckBox20.Checked = False

        CheckBox21.Checked = False
        CheckBox22.Checked = False
        CheckBox23.Checked = False
        CheckBox24.Checked = False
        CheckBox25.Checked = False
        CheckBox26.Checked = False
        CheckBox27.Checked = False
        CheckBox28.Checked = False
        CheckBox29.Checked = False
        CheckBox30.Checked = False

        CheckBox31.Checked = False
        CheckBox32.Checked = False
        CheckBox33.Checked = False
        CheckBox34.Checked = False
        CheckBox35.Checked = False
        CheckBox36.Checked = False
        CheckBox37.Checked = False
        CheckBox38.Checked = False
        CheckBox39.Checked = False
        CheckBox40.Checked = False

        CheckBox41.Checked = False
        CheckBox42.Checked = False
        CheckBox43.Checked = False
        CheckBox44.Checked = False
        CheckBox45.Checked = False
        CheckBox46.Checked = False
        CheckBox47.Checked = False
        CheckBox48.Checked = False
        CheckBox49.Checked = False
        CheckBox50.Checked = False

        CheckBox51.Checked = False
        CheckBox52.Checked = False
        CheckBox53.Checked = False
        CheckBox54.Checked = False
        CheckBox55.Checked = False
        CheckBox56.Checked = False
        CheckBox57.Checked = False
        CheckBox58.Checked = False
        CheckBox59.Checked = False
        CheckBox60.Checked = False

        CheckBox61.Checked = False
        CheckBox62.Checked = False
        CheckBox63.Checked = False
        CheckBox64.Checked = False
        CheckBox65.Checked = False
        CheckBox66.Checked = False
        CheckBox67.Checked = False
        CheckBox68.Checked = False
        CheckBox69.Checked = False
        CheckBox70.Checked = False

        CheckBox71.Checked = False
        CheckBox72.Checked = False
        CheckBox73.Checked = False
        CheckBox74.Checked = False
        CheckBox75.Checked = False
        CheckBox76.Checked = False
        CheckBox77.Checked = False
        CheckBox78.Checked = False
        CheckBox79.Checked = False
        CheckBox80.Checked = False

        CheckBox81.Checked = False
        CheckBox82.Checked = False
        CheckBox83.Checked = False
        CheckBox84.Checked = False
        CheckBox85.Checked = False
        CheckBox86.Checked = False
        CheckBox87.Checked = False
        CheckBox88.Checked = False
        CheckBox89.Checked = False
        CheckBox90.Checked = False


    End Sub
    Public Function VerifTab_Numero() As Boolean
        Dim stg As String = ""
        If CInt(MiseTicket) = 0 Then

            Return False
        End If


        Select Case Mode_selected
            Case 1
                If (countTab_Numero(stg) <> 1) Then

                    Return False
                Else
                    Return True
                End If
            Case 2
                If (countTab_Numero(stg) <> 2) Then

                    Return False
                Else
                    Return True
                End If
            Case 3
                If (countTab_Numero(stg) <> 3) Then

                    Return False
                Else
                    Return True
                End If
            Case Else
                Return False
        End Select

    End Function

    Public Function VerifFacilitiesTab_Numero() As Boolean
        Dim stg As String = ""
        If CInt(MiseTicket) = 0 Then

            Return False
        End If


        Select Case Mode_selected
            Case 1
                If (countTab_Numero(stg) < 1) Then

                    Return False
                Else
                    Return True
                End If
            Case 2
                If (countTab_Numero(stg) <> 2) Then

                    Return False
                Else
                    Return True
                End If
            Case 3
                If (countTab_Numero(stg) <> 3) Then

                    Return False
                Else
                    Return True
                End If
            Case Else
                Return False
        End Select

    End Function



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_print.Click
        If TirageLancer = 1 Then
            MessageBox.Show("svp Veuillez Patientez jusqu'a la fin du tirage.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            viderdatagrid()
            Exit Sub
        End If
        If (Dgd_Tick.RowCount > 5) Then
            MessageBox.Show("Taille Limite (5) Sur un Ticket atteinte!!", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            viderdatagrid()
            Exit Sub
        End If
        'Using Oactivation As New BL_Lotto.BlActivation(source)

        '    If (Not Oactivation.IsActivated) Then
        '        Activation.ShowDialog()
        '    End If

        'End Using
        Dim retour As Boolean
        Dim numpar() As String
        If (Dgd_Tick.RowCount > 0) Then
            'If (TbMise.Text <> "") And ((IsNumeric(TbMise.Text)) And num_jouer_Ok()) Then

            ' Open a printer status monitor for the selected printer.
            If AjouterTicket() = True Then
                Using ODsTicket As New BL_Lotto.BlTicket(source)
                    '{CStr(num_tirage), CStr(Mode_selected), resultat, MiseTicket}
                    For i As Integer = 0 To Dgd_Tick.Rows.Count - 1
                        Dim ID_PA As String = ""
                        numpar = Dgd_Tick.Rows(i).Cells(2).Value.ToString.Split(CChar("-"))
                        retour = ODsTicket.AjouterParie(ID_TICKET_P, Dgd_Tick.Rows(i).Cells(1).Value.ToString, Dgd_Tick.Rows(i).Cells(3).Value.ToString, numpar)
                        'ajouterNumJouer(ID_PA, numpar)
                    Next
                End Using

                Try
                    PrintingReceipt()

                    RadioButton1.PerformClick()
                    RadioButton4.PerformClick()
                    viderdatagrid()
                    RaffraichirCaisse()

                Catch ex As Exception
                    MessageBox.Show("Failed to print. cause: " + ex.Message, "ELITE...LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try

                ticket += 1
            Else
                MessageBox.Show("Erreur d'enregistrement !", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("Failed to open printer status monitor.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub PrintingReceipt()
        printdialog = New PrintDialog
        prdocument = New PrintDocument
        printdialog.Document = prdocument


        'Dim result As New DialogResult
        'result = printdialog.ShowDialog

        'If (result = DialogResult.OK) Then
        If PRINTER_NAME <> vbNullChar Then
            'MsgBox(PRINTER_NAME)
            prdocument.PrinterSettings.PrinterName = PRINTER_NAME
        End If

        prdocument.DocumentName = "Elite Ticket"

        prdocument.Print()

        'End If

    End Sub
    Public Shared Function DefaultPrinterName() As String
        Dim oPS As New System.Drawing.Printing.PrinterSettings

        Try
            DefaultPrinterName = oPS.PrinterName
        Catch ex As System.Exception
            DefaultPrinterName = ""
        Finally
            oPS = Nothing
        End Try
    End Function

    Function GetDefaultPrinterName() As String
        'Dim searcher As ManagementObjectSearcher
        'Dim query As ObjectQuery
        'query = New ObjectQuery("SELECT * FROM Win32_Printer")
        'searcher = New ManagementObjectSearcher(query)

        'For Each mo As ManagementObject In searcher.Get()
        '    Return mo.Properties("Name").ToString
        'Next

        Return vbNullChar
    End Function


    Private Sub prdocument_Print(ByVal sender As System.Object, ByVal e As PrintPageEventArgs) Handles prdocument.PrintPage

        Dim graphic As Graphics

        graphic = e.Graphics

        Dim printFont As New Font("Courier New", 18, FontStyle.Regular, GraphicsUnit.Point) 'Substituted to FontA Font

        Dim fontheight As Double
        Dim x, y, offset As Integer

        fontheight = printFont.GetHeight

        offset = 40


        Dim codbar As String = ""
        Dim user As String = ""
        Dim somme As Integer
        somme = 0


        'setting pour le code bare

        settings = New BarcodeSettings()


        Dim data As String = "20140983-90"
        Dim type As String = "Code128"


        settings.Type = CType(System.Enum.Parse(GetType(BarCodeType), type), BarCodeType)
        settings.X = 0.51


        Dim barHeight As Short = 15
        settings.BarHeight = barHeight

        settings.ShowTextOnBottom = True

        ' fin setting code bare

        graphic.PageUnit = GraphicsUnit.Point

        ' Draw the bitmap
        x = 0
        y = 0

        ' 
        'graphic.DrawString("ELITE LOTO", printFont, New SolidBrush(Color.Red), x, y)
        graphic.DrawImage(Global.PL_Lotto.My.Resources.Resources.loto, x, y, 150, 50)

        'e.Graphics.DrawImage(pbImage.Image, x, y, pbImage.Image.Width - 187, 50)

        ' Print the receipt text
        printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
        offset = CInt(fontheight)
        x = 10
        y = 25 + offset

        Try
            Using ODsTicket As New BL_Lotto.BlTicket(source)

                DSticket = ODsTicket.TicketParId(ID_TICKET_P)
                codbar = CStr(DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("COD_BAR")) '& "-" & CStr(DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("ID_TICKET"))
                'tirage = DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("num_tick")
                user = DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("ID_USER").ToString
            End Using
            graphic.DrawString("Product ID:".PadRight(10) & codbar, printFont, Brushes.Black, x, y)

            offset = CInt(printFont.GetHeight(e.Graphics))
            y += offset
            graphic.DrawString("Caisse:".PadRight(5) & user & "  Tirage :".PadRight(5) & DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("NUM_TIRAGE").ToString, printFont, Brushes.Black, x, y)
            'offset = CInt(printFont.GetHeight(e.Graphics))
            'y += offset

            'on affiche le tirage
            'graphic.DrawString("Numero du Tirage :".PadRight(20) & DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("NUM_TIRAGE").ToString, printFont, Brushes.Black, x, y)
            y += offset
            graphic.DrawString(Date.Now.ToShortDateString & " " & Date.Now.ToLongTimeString, printFont, Brushes.Black, x, y)
            'y = CInt(y + (offset * 1.7))
            y += offset
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


        Dim listePariSolo, listePariDoublet, listeParitriplet As New Collection
        Dim ListePariParMode As New Dictionary(Of String, Collection)
        Dim LmodeKp As KeyValuePair(Of String, Collection)

        Dim list100, list200, list300, list500, list1000, list1500, list2000, list2500, list3000 As String
        list100 = ""
        list1000 = ""
        list1500 = ""
        list200 = ""
        list2000 = ""
        list2500 = ""
        list300 = ""
        list3000 = ""
        list500 = ""

        For i As Integer = 0 To Dgd_Tick.Rows.Count - 1
            Using OBlTicket As New BL_Lotto.BlTicket(source)
                QUOTA = OBlTicket.GetQuota(Dgd_Tick.Rows(i).Cells(1).Value.ToString)
            End Using

            Using ODsTicket As New BL_Lotto.BlTicket(source)
                dsmode = ODsTicket.GetMode(Dgd_Tick.Rows(i).Cells(1).Value.ToString)
            End Using

            Select Case CType(Dgd_Tick.Rows(i).Cells(1).Value, Integer)
                Case 1
                    listePariSolo.Add(Dgd_Tick.Rows(i))
                Case 2
                    listePariDoublet.Add(Dgd_Tick.Rows(i))
                Case 3
                    listeParitriplet.Add(Dgd_Tick.Rows(i))
                Case Else
            End Select
            somme = somme + QUOTA * CInt(Dgd_Tick.Rows(i).Cells(3).Value.ToString)
        Next

        ListePariParMode.Add("1", listePariSolo)
        ListePariParMode.Add("2", listePariDoublet)
        ListePariParMode.Add("3", listeParitriplet)



        For Each LmodeKp In ListePariParMode
            Using OBlTicket As New BL_Lotto.BlTicket(source)
                QUOTA = OBlTicket.GetQuota(LmodeKp.Key)
            End Using

            Using ODsTicket As New BL_Lotto.BlTicket(source)
                dsmode = ODsTicket.GetMode(LmodeKp.Key)
            End Using
            If LmodeKp.Value.Count > 0 Then
                graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                y = CInt(y + (offset * 1.2))

                graphic.DrawString("Mode Du Jeu:".PadRight(10) & dsmode.Tables("mode_jeux").Rows(0).Item("MODE").ToString & " COTE:".PadRight(5) & QUOTA, printFont, Brushes.Black, x, y)
                'y = CInt(y + offset)
                'graphic.DrawString("COTE:".PadRight(20) & QUOTA, printFont, Brushes.Black, x, y)
                y = CInt(y + (offset * 1.2))
            End If


            Dim pariduMod As DataGridViewRow
            ' il faut coller les chiffres si ces le solo
            If LmodeKp.Key = "1" Then
                For Each pariduMod In LmodeKp.Value
                    Select Case CType(pariduMod.Cells(3).Value.ToString, Integer)
                        Case 100
                            list100 = list100 & " - " & pariduMod.Cells(2).Value.ToString
                        Case 200
                            list200 = list200 & " - " & pariduMod.Cells(2).Value.ToString
                        Case 300
                            list300 = list300 & " - " & pariduMod.Cells(2).Value.ToString
                        Case 500
                            list500 = list500 & " - " & pariduMod.Cells(2).Value.ToString
                        Case 1000
                            list1000 = list1000 & " - " & pariduMod.Cells(2).Value.ToString
                        Case 1500
                            list1500 = list1500 & " - " & pariduMod.Cells(2).Value.ToString
                        Case 2000
                            list2000 = list2000 & " - " & pariduMod.Cells(2).Value.ToString
                        Case 2500
                            list2500 = list2500 & " - " & pariduMod.Cells(2).Value.ToString
                        Case 3000
                            list3000 = list3000 & " - " & pariduMod.Cells(2).Value.ToString
                        Case Else
                    End Select

                Next pariduMod



                If list100 <> "" Then
                    Dim mi As Integer = 100
                    printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
                    graphic.DrawString("Loto:" & list100, printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                    printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
                    graphic.DrawString("MISE:".PadRight(20) + mi.ToString(), printFont, Brushes.Black, x, y)
                    y += offset
                    graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                End If

                If list200 <> "" Then
                    Dim mi As Integer = 200
                    printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
                    graphic.DrawString("Loto:" & list200, printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                    printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
                    graphic.DrawString("MISE :".PadRight(20) + mi.ToString(), printFont, Brushes.Black, x, y)
                    y += offset
                    graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))

                End If

                If list300 <> "" Then
                    Dim mi As Integer = 300
                    printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
                    graphic.DrawString("Loto:" & list300, printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                    printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
                    graphic.DrawString("MISE :".PadRight(20) + mi.ToString(), printFont, Brushes.Black, x, y)
                    y += offset
                    graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                End If

                If list500 <> "" Then
                    Dim mi As Integer = 500
                    printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
                    graphic.DrawString("Loto:" & list500, printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                    printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
                    graphic.DrawString("MISE :".PadRight(20) + mi.ToString(), printFont, Brushes.Black, x, y)
                    y += offset
                    graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                End If

                If list1000 <> "" Then
                    Dim mi As Integer = 1000
                    printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
                    e.Graphics.DrawString("Loto:" & list1000, printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                    printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
                    graphic.DrawString("MISE :".PadRight(20) + mi.ToString(), printFont, Brushes.Black, x, y)
                    y += offset
                    graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                End If

                If list1500 <> "" Then
                    Dim mi As Integer = 1500
                    printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
                    graphic.DrawString("Loto:" & list1500, printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                    printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
                    graphic.DrawString("MISE :".PadRight(20) + mi.ToString(), printFont, Brushes.Black, x, y)
                    y += offset
                    graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                End If

                If list2000 <> "" Then
                    Dim mi As Integer = 2000
                    printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
                    graphic.DrawString("Loto:" & list2000, printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                    printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
                    graphic.DrawString("MISE :".PadRight(20) + mi.ToString(), printFont, Brushes.Black, x, y)
                    y += offset
                    graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                End If

                If list2500 <> "" Then
                    Dim mi As Integer = 2500
                    printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
                    graphic.DrawString("Loto:" & list2500, printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                    printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
                    graphic.DrawString("MISE :".PadRight(20) + mi.ToString(), printFont, Brushes.Black, x, y)
                    y += offset
                    graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                End If

                If list3000 <> "" Then
                    Dim mi As Integer = 3000
                    printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
                    graphic.DrawString("Loto:" & list3000, printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                    printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
                    graphic.DrawString("MISE :".PadRight(20) + mi.ToString(), printFont, Brushes.Black, x, y)
                    y += offset
                    graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                End If
            Else

                For Each pariduMod In LmodeKp.Value

                    printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
                    graphic.DrawString("Loto:" & pariduMod.Cells(2).Value.ToString, printFont, Brushes.Black, x, y)
                    y = CInt(y + (offset * 1.2))
                    printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
                    Dim mot As Integer = CInt(pariduMod.Cells(3).Value)
                    graphic.DrawString("MISE :".PadRight(20) & mot.ToString("C", CultureInfo.GetCultureInfo("fr-FR")), printFont, Brushes.Black, x, y)
                    y += offset
                    graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
                    'y = CInt(y + (offset * 1.2))

                Next pariduMod

            End If

        Next (LmodeKp)

        'y += offset
        printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
        'offset = CInt(printFont.GetHeight(graphic))
        'y = CInt(y + (offset))
        graphic.DrawString("Gain Probable:".PadRight(20) & somme.ToString, printFont, Brushes.Black, x - 1, y)

        printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
        offset = CInt(printFont.GetHeight(graphic) - 4)
        y = CInt(y + (offset * 1.8))
        'graphic.DrawString("oui!! c'est vrai!", printFont, Brushes.Red, 61, y)
        'y += lineOffset
        settings.Data2D = codbar
        settings.Data = codbar

        Dim bdf As Code128BarcodeDraw = BarcodeDrawFactory.Code128WithChecksum

        Dim generator As New BarCodeGenerator(settings)
        Dim barcode As Image = generator.GenerateImage()
        'Dim barcode As Image = bdf.Draw(codbar, 20)

        'save the barcode as an image
        'barcode.Save(codbar + ".png")

        'graphic.DrawImage(Image.FromFile(codbar + ".png"), x, y, 150, 75)
        graphic.DrawImage(barcode, 30, y)
        'graphic.DrawImage(barcode, x, y, 150, 75)
        'Todo put the image in the ticket
        'graphic.DrawString(codbar, barcodeFont, Brushes.Black, x, y + 5)

        ' Indicate that no more data to print, and the Print Document can now send the print data to the spooler.
        e.HasMorePages = False

    End Sub



    Private Sub TickPrint_Print(ByVal sender As System.Object, ByVal e As PrintPageEventArgs) Handles TickPrint.PrintPage
        Dim graphic As Graphics

        graphic = e.Graphics

        Dim printFont As New Font("Courier New", 18, FontStyle.Regular, GraphicsUnit.Point) 'Substituted to FontA Font

        Dim fontheight As Double
        Dim x, y, offset As Integer

        fontheight = printFont.GetHeight

        offset = 40


        Dim codbar As String = ""
        Dim user As String = ""
        Dim somme As Integer
        somme = 0


        'setting pour le code bare

        settings = New BarcodeSettings()
        Dim data As String = "20140983-90"
        Dim type As String = "Code128"


        settings.Type = CType(System.Enum.Parse(GetType(BarCodeType), type), BarCodeType)
        settings.X = 0.51


        Dim barHeight As Short = 15
        'settings.BarHeight = barHeight

        settings.ShowTextOnBottom = True

        ' fin setting code bare

        graphic.PageUnit = GraphicsUnit.Point

        ' Draw the bitmap
        x = 30
        y = 0

        ' 
        'graphic.DrawString("ELITE LOTO", printFont, New SolidBrush(Color.Red), x, y)
        graphic.DrawImage(Global.PL_Lotto.My.Resources.Resources.loto, x, y, 100, 33)
        'e.Graphics.DrawImage(pbImage.Image, x, y, pbImage.Image.Width - 187, 50)

        ' Print the receipt text
        printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
        offset = CInt(fontheight)
        x = 10
        y = 10 + offset

        Try
            Using ODsTicket As New BL_Lotto.BlTicket(source)

                DSticket = ODsTicket.TicketParId(ID_TICKET_P)
                codbar = CStr(DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("COD_BAR")) '& "-" & CStr(DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("ID_TICKET"))
                'tirage = DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("num_tick")
                user = DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("ID_USER").ToString
            End Using
            graphic.DrawString("Product ID:".PadRight(10) & codbar, printFont, Brushes.Black, x, y)

            printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
            y += offset

            graphic.DrawString("Caisse:".PadRight(20) & user, printFont, Brushes.Black, x, y)
            offset = CInt(printFont.GetHeight(e.Graphics))
            ' y = CInt(y + (offset * 1.7))
            y += offset
            'on affiche le tirage
            graphic.DrawString("Numero du Tirage :".PadRight(20) & DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("NUM_TIRAGE").ToString, printFont, Brushes.Black, x, y)
            y += offset
            graphic.DrawString(Date.Now.ToLongDateString & " " & Date.Now.ToLongTimeString, printFont, Brushes.Black, x, y)
            y = CInt(y + (offset * 1.2))

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        graphic.DrawString("$$$ Felicitation $$$", printFont, Brushes.Black, x, y)
        y = CInt(y + (offset * 1))



        e.Graphics.DrawString("___________________________________", printFont, Brushes.Black, x, y)

        somme = CInt(Gains)

        printFont = New Font("Courier New", 10, FontStyle.Bold, GraphicsUnit.Point)
        offset = CInt(printFont.GetHeight(e.Graphics))
        y = CInt(y + (offset * 1.2))
        graphic.DrawString("MONTANT PAYE ".PadLeft(15) & somme.ToString, printFont, Brushes.Black, x, y)

        printFont = New Font("Comic Sans MS", 10, FontStyle.Bold, GraphicsUnit.Point)
        offset = CInt(printFont.GetHeight(e.Graphics) - 4)
        y = CInt(y + (offset * 1.8))
        'e.Graphics.DrawString("oui!! c'est vrai!", printFont, Brushes.Red, 61, y)
        'y += lineOffset
        'settings.Data2D = codbar
        'settings.Data = codbar

        'Dim generator As New BarCodeGenerator(settings)
        'Dim barcode As Image = generator.GenerateImage()

        'save the barcode as an image
        'barcode.Save(codbar + ".png")

        'e.Graphics.DrawImage(Image.FromFile(codbar + ".png"), x, y, 150, 75)
        'Todo put the image in the ticket

        'e.Graphics.DrawString(codbar, barcodeFont, Brushes.Black, x, y + 5)

        ' Indicate that no more data to print, and the Print Document can now send the print data to the spooler.
        e.HasMorePages = False
    End Sub

    Private Sub JackTickPrint_Print(ByVal sender As System.Object, ByVal e As PrintPageEventArgs) Handles JackTickPrint.PrintPage
        Dim graphic As Graphics

        graphic = e.Graphics

        Dim printFont As New Font("Courier New", 18, FontStyle.Regular, GraphicsUnit.Point) 'Substituted to FontA Font

        Dim fontheight As Double
        Dim x, y, offset As Integer

        fontheight = printFont.GetHeight

        offset = 40


        Dim codbar As String = ""
        Dim user As String = ""
        Dim somme As Integer
        somme = 0


        'setting pour le code bare

        settings = New BarcodeSettings()
        Dim data As String = "20140983-90"
        Dim type As String = "Code128"


        settings.Type = CType(System.Enum.Parse(GetType(BarCodeType), type), BarCodeType)
        settings.X = 0.51


        Dim barHeight As Short = 15
        settings.BarHeight = barHeight

        settings.ShowTextOnBottom = True

        ' fin setting code bare

        graphic.PageUnit = GraphicsUnit.Point

        ' Draw the bitmap
        x = 30
        y = 0

        ' 
        'graphic.DrawString("ELITE LOTO", printFont, New SolidBrush(Color.Red), x, y)
        graphic.DrawImage(Global.PL_Lotto.My.Resources.Resources.loto, x, y, 100, 33)
        'e.Graphics.DrawImage(pbImage.Image, x, y, pbImage.Image.Width - 187, 50)

        ' Print the receipt text
        printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
        offset = CInt(fontheight)
        x = 10
        y = 10 + offset

        Try
            Using ODsTicket As New BL_Lotto.BlTicket(source)

                DSticket = ODsTicket.TicketParId(ID_TICKET_P)
                codbar = CStr(DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("COD_BAR")) '& "-" & CStr(DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("ID_TICKET"))
                'tirage = DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("num_tick")
                user = DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("ID_USER").ToString
            End Using
            graphic.DrawString("Product ID:".PadRight(10) & codbar, printFont, Brushes.Black, x, y)

            printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
            y += offset

            graphic.DrawString("Caisse:".PadRight(20) & user, printFont, Brushes.Black, x, y)
            offset = CInt(printFont.GetHeight(e.Graphics))
            y = CInt(y + (offset * 1.7))

            'on affiche le tirage
            graphic.DrawString("Numero du Tirage :".PadRight(20) & DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("NUM_TIRAGE").ToString, printFont, Brushes.Black, x, y)
            y += offset
            graphic.DrawString(Date.Now.ToLongDateString & " " & Date.Now.ToLongTimeString, printFont, Brushes.Black, x, y)
            y = CInt(y + (offset * 1.7))

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        graphic.DrawString(" *** JACKPOT *** ", printFont, Brushes.Black, x + 10, y)
        y = CInt(y + (offset * 1.7))
        graphic.DrawString("$$$ Felicitation $$$", printFont, Brushes.Black, x, y)
        y = CInt(y + (offset * 1.2))



        graphic.DrawString("___________________________________", printFont, Brushes.Black, x, y)
        y = CInt(y + (offset * 1.2))
        'y = CInt(y + (lineOffset * 1.7))
        graphic.DrawString("Cagnotte : ", printFont, Brushes.Black, x, y)

        somme = CInt(GainsJackpot)

        printFont = New Font("Times New Roman", 16, FontStyle.Bold, GraphicsUnit.Point)
        offset = CInt(printFont.GetHeight(e.Graphics))
        y = CInt(y + (offset * 1.7))
        graphic.DrawString(" " & somme & " F.CFA", printFont, Brushes.Black, x - 1, y)

        printFont = New Font("Comic Sans MS", 12, FontStyle.Bold, GraphicsUnit.Point)
        offset = CInt(printFont.GetHeight(e.Graphics) - 4)
        y = CInt(y + (offset * 1.8))
        'e.Graphics.DrawString("oui!! c'est vrai!", printFont, Brushes.Red, 61, y)
        'y += lineOffset
        settings.Data2D = codbar
        settings.Data = codbar

        Dim generator As New BarCodeGenerator(settings)
        Dim barcode As Image = generator.GenerateImage()


        e.HasMorePages = False
    End Sub

    Private Sub AnTickPrint_Print(ByVal sender As System.Object, ByVal e As PrintPageEventArgs) Handles AnTickPrint.PrintPage


        Dim graphic As Graphics

        graphic = e.Graphics

        Dim printFont As New Font("Courier New", 18, FontStyle.Regular, GraphicsUnit.Point) 'Substituted to FontA Font

        Dim fontheight As Double
        Dim x, y, offset As Integer

        fontheight = printFont.GetHeight

        offset = 40


        Dim codbar As String = ""
        Dim user As String = ""
        Dim somme As Integer
        somme = 0


        'setting pour le code bare

        settings = New BarcodeSettings()
        Dim data As String = "20140983-90"
        Dim type As String = "Code128"


        settings.Type = CType(System.Enum.Parse(GetType(BarCodeType), type), BarCodeType)
        settings.X = 0.51


        Dim barHeight As Short = 15
        settings.BarHeight = barHeight

        settings.ShowTextOnBottom = True

        ' fin setting code bare

        graphic.PageUnit = GraphicsUnit.Point

        ' Draw the bitmap
        x = 30
        y = 0

        ' 
        'graphic.DrawString("ELITE LOTO", printFont, New SolidBrush(Color.Red), x, y)
        graphic.DrawImage(Global.PL_Lotto.My.Resources.Resources.loto, x, y, 100, 33)
        'e.Graphics.DrawImage(pbImage.Image, x, y, pbImage.Image.Width - 187, 50)

        ' Print the receipt text
        printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
        offset = CInt(fontheight)
        x = 10
        y = 10 + offset

        Try
            Using ODsTicket As New BL_Lotto.BlTicket(source)

                DSticket = ODsTicket.TicketParId(ID_TICKET_P)
                codbar = CStr(DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("COD_BAR")) '& "-" & CStr(DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("ID_TICKET"))
                'tirage = DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("num_tick")
                user = DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("ID_USER").ToString
            End Using
            graphic.DrawString("Product ID:".PadRight(10) & codbar, printFont, Brushes.Black, x, y)

            printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
            y += offset

            graphic.DrawString("Caisse:".PadRight(20) & user, printFont, Brushes.Black, x, y)
            offset = CInt(printFont.GetHeight(e.Graphics))
            y = CInt(y + (offset * 1))

            'on affiche le tirage
            graphic.DrawString("Numero du Tirage :".PadRight(20) & DSticket.Tables("ticket").Rows(DSticket.Tables("ticket").Rows.Count - 1).Item("NUM_TIRAGE").ToString, printFont, Brushes.Black, x, y)
            y += offset
            graphic.DrawString(Date.Now.ToLongDateString & " " & Date.Now.ToLongTimeString, printFont, Brushes.Black, x, y)
            y = CInt(y + (offset * 1))

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        e.Graphics.DrawString("Ticket Annuler  ", printFont, Brushes.Black, x, y)
        y = CInt(y + (offset * 1.2))



        'e.Graphics.DrawString("___________________________________", printFont, Brushes.Black, x, y)


        printFont = New Font("Comic Sans MS", 10, FontStyle.Bold, GraphicsUnit.Point)
        offset = CInt(printFont.GetHeight(graphic) - 4)
        y = CInt(y + (offset * 1.8))
        'e.Graphics.DrawString("oui!! c'est vrai!", printFont, Brushes.Red, 61, y)
        'y += lineOffset
        settings.Data2D = codbar
        settings.Data = codbar

        'Dim generator As New BarCodeGenerator(settings)
        'Dim barcode As Image = generator.GenerateImage()

        'save the barcode as an image
        'barcode.Save("codbar.png")

        'e.Graphics.DrawImage(Image.FromFile("codbar.png"), x, y, 150, 75)
        'Todo put the image in the ticket

        'e.Graphics.DrawString(codbar, barcodeFont, Brushes.Black, x, y + 5)

        ' Indicate that no more data to print, and the Print Document can now send the print data to the spooler.
        e.HasMorePages = False
    End Sub




    ' Executes the canceling of print job.
    Private Sub CancelPrintJob()
        Dim searchPrintJobs As ManagementObjectSearcher
        Dim printJobCollection As ManagementObjectCollection
        Dim printJob As ManagementObject
        Dim isDeleted As Boolean = False

        searchPrintJobs = New ManagementObjectSearcher("SELECT * FROM Win32_PrintJob")

        printJobCollection = searchPrintJobs.Get

        For Each printJob In printJobCollection
            If CBool(System.String.Compare(printJob.Properties("Name").Value.ToString(), PRINTER_NAME)) Then
                printJob.Delete()
                isDeleted = True
                Exit For
            End If
        Next

        If isDeleted Then
            MessageBox.Show("Print job cancelled.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("Failed to cancel print job.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub



    'Private Sub ForceResetPrinter()
    '    ' If both Printing complete and error are not take then use BiForceResetPrinter.
    '    If MessageBox.Show("Anomalous occurrence." & vbLf & "Execute BiForceResetPrinter.", "123... LOTO", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
    '        If m_objAPI.ForceResetPrinter() = ErrorCode.SUCCESS Then
    '            MessageBox.Show("Force reset printer is success.", "123... LOTO")
    '        Else
    '            MessageBox.Show("Failed to force reset printer.", "123... LOTO")
    '        End If
    '    End If
    'End Sub
    ' The executed function when the Close button is clicked.
    Private Sub BtCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtCancel.Click

        If (Dgd_Tick.Rows.Count > 0) Then
            Dgd_Tick.Rows.RemoveAt(Dgd_Tick.Rows.Count - 1)
        End If
        'If m_objAPI.IsValid Then
        '    ' close the Status Monitor after using the Status API.
        '    m_objAPI.CloseMonPrinter()
        'End If
        'Close()
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim resultat As String = ""
        'Dim retour As Boolean

        Using OBlTicket As New BL_Lotto.BlTicket(source)
            'Get a new Project DataSet

            Dim dsTirage As New DataSet
            dsTirage = OBlTicket.GetLastTirageNum
            Dim num_tirage As Integer
            If dsTirage.Tables("tirage").Rows.Count > 0 Then
                num_tirage = CInt(dsTirage.Tables("tirage").Rows(0).Item("NUM_TIRAGE").ToString) + 1
            Else
                num_tirage = 1
            End If

            With Dgd_Tick


                If VerifTab_Numero() Then
                    countTab_Numero(resultat)
                    '.Columns.Clear()
                    .ColumnCount = 4
                    '.ColumnHeadersVisible = False
                    '{CStr(num_tirage), CStr(Mode_selected), resultat, MiseTicket}
                    Dim row0 As String() = {CStr(num_tirage), CStr(Mode_selected), resultat, MiseTicket}
                    .Rows.Add(row0)

                    'on remet les compteur a zero
                    remiseZero()
                Else
                    MessageBox.Show("le Mode et les entrées ne coincide pas.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    remiseZero()
                End If


            End With
            RadioButton1.PerformClick()
            RadioButton4.PerformClick()
        End Using

    End Sub

#Region "gestion des cochages"
    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged

        If RadioButton1.Checked = True Then ' Teste si le bouton est coché.
            Coleu = RadioButton1.BackColor
            RadioButton1.BackColor = RadioButton1.ForeColor
            RadioButton1.ForeColor = Coleu

            Mode_selected = 1

        Else
            Coleu = RadioButton1.BackColor
            RadioButton1.BackColor = RadioButton1.ForeColor
            RadioButton1.ForeColor = Coleu

        End If

    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged

        If RadioButton2.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton2.BackColor
            RadioButton2.BackColor = RadioButton2.ForeColor
            RadioButton2.ForeColor = Coleu
            Mode_selected = 2

        Else
            Coleu = RadioButton2.BackColor
            RadioButton2.BackColor = RadioButton2.ForeColor
            RadioButton2.ForeColor = Coleu
        End If

    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged

        If RadioButton3.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton3.BackColor
            RadioButton3.BackColor = RadioButton3.ForeColor
            RadioButton3.ForeColor = Coleu

            Mode_selected = 3

        Else
            Coleu = RadioButton3.BackColor
            RadioButton3.BackColor = RadioButton3.ForeColor
            RadioButton3.ForeColor = Coleu
        End If

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox1.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox1.Text)) = 0
        End If

    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox2.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox2.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If CheckBox4.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox4.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox4.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox3.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox3.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If CheckBox5.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox5.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox5.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox6.CheckedChanged
        If CheckBox6.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox6.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox6.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox7.CheckedChanged
        If CheckBox7.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox7.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox7.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox8.CheckedChanged
        If CheckBox8.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox8.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox8.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox9_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox9.CheckedChanged
        If CheckBox9.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox9.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox9.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox10_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox10.CheckedChanged
        If CheckBox10.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox10.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox10.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox11_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox11.CheckedChanged
        If CheckBox11.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox11.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox11.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox12_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox12.CheckedChanged
        If CheckBox12.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox12.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox12.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox13_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox13.CheckedChanged
        If CheckBox13.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox13.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox13.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox14_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox14.CheckedChanged
        If CheckBox14.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox14.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox14.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox15_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox15.CheckedChanged
        If CheckBox15.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox15.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox15.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox16_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox16.CheckedChanged
        If CheckBox16.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox16.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox16.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox18_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox18.CheckedChanged
        If CheckBox18.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox18.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox18.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox19_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox19.CheckedChanged
        If CheckBox19.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox19.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox19.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox20_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox20.CheckedChanged
        If CheckBox20.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox20.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox20.Text)) = 0
        End If
    End Sub


    Private Sub CheckBox21_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox21.CheckedChanged
        If CheckBox21.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox21.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox21.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox22_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox22.CheckedChanged
        If CheckBox22.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox22.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox22.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox23_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox23.CheckedChanged
        If CheckBox23.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox23.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox23.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox24_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox24.CheckedChanged
        If CheckBox24.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox24.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox24.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox25_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox25.CheckedChanged
        If CheckBox25.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox25.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox25.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox26_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox26.CheckedChanged
        If CheckBox26.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox26.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox26.Text)) = 0
        End If
    End Sub
    Private Sub CheckBox27_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox27.CheckedChanged
        If CheckBox27.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox27.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox27.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox28_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox28.CheckedChanged
        If CheckBox28.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox28.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox28.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox29_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox29.CheckedChanged
        If CheckBox29.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox29.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox29.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox30_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox30.CheckedChanged
        If CheckBox30.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox30.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox30.Text)) = 0
        End If
    End Sub


    Private Sub CheckBox31_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox31.CheckedChanged
        If CheckBox31.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox31.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox31.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox32_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox32.CheckedChanged
        If CheckBox32.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox32.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox32.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox33_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox33.CheckedChanged
        If CheckBox33.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox33.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox33.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox34_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox34.CheckedChanged
        If CheckBox34.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox34.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox34.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox35_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox35.CheckedChanged
        If CheckBox35.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox35.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox35.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox36_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox36.CheckedChanged
        If CheckBox36.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox36.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox36.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox37_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox37.CheckedChanged
        If CheckBox37.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox37.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox37.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox38_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox38.CheckedChanged
        If CheckBox38.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox38.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox38.Text)) = 0
        End If
    End Sub



    Private Sub CheckBox39_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox39.CheckedChanged
        If CheckBox39.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox39.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox39.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox40_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox40.CheckedChanged
        If CheckBox40.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox40.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox40.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox41_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox41.CheckedChanged
        If CheckBox41.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox41.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox41.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox42_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox42.CheckedChanged
        If CheckBox42.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox42.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox42.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox43_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox43.CheckedChanged
        If CheckBox43.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox43.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox43.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox44_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox44.CheckedChanged
        If CheckBox44.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox44.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox44.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox45_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox45.CheckedChanged
        If CheckBox45.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox45.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox45.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox46_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox46.CheckedChanged
        If CheckBox46.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox46.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox46.Text)) = 0
        End If
    End Sub



    Private Sub CheckBox47_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox47.CheckedChanged
        If CheckBox47.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox47.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox47.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox48_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox48.CheckedChanged
        If CheckBox48.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox48.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox48.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox49_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox49.CheckedChanged
        If CheckBox49.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox49.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox49.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox50_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox50.CheckedChanged
        If CheckBox50.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox50.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox50.Text)) = 0
        End If
    End Sub



    Private Sub CheckBox51_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox51.CheckedChanged
        If CheckBox51.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox51.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox51.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox52_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox52.CheckedChanged
        If CheckBox52.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox52.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox52.Text)) = 0
        End If
    End Sub
    Private Sub CheckBox53_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox53.CheckedChanged
        If CheckBox53.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox53.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox53.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox54_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox54.CheckedChanged
        If CheckBox54.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox54.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox54.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox55_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox55.CheckedChanged
        If CheckBox55.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox55.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox55.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox56_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox56.CheckedChanged
        If CheckBox56.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox56.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox56.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox57_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox57.CheckedChanged
        If CheckBox57.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox57.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox57.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox58_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox58.CheckedChanged
        If CheckBox58.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox58.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox58.Text)) = 0
        End If
    End Sub



    Private Sub CheckBox60_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox60.CheckedChanged
        If CheckBox60.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox60.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox60.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox59_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox59.CheckedChanged
        If CheckBox59.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox59.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox59.Text)) = 0
        End If
    End Sub



    Private Sub CheckBox61_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox61.CheckedChanged
        If CheckBox61.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox61.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox61.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox62_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox62.CheckedChanged
        If CheckBox62.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox62.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox62.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox63_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox63.CheckedChanged
        If CheckBox63.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox63.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox63.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox64_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox64.CheckedChanged
        If CheckBox64.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox64.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox64.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox65_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox65.CheckedChanged
        If CheckBox65.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox65.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox65.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox66_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox66.CheckedChanged
        If CheckBox66.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox66.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox66.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox67_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox67.CheckedChanged
        If CheckBox67.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox67.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox67.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox68_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox68.CheckedChanged
        If CheckBox68.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox68.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox68.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox69_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox69.CheckedChanged
        If CheckBox69.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox69.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox69.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox70_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox70.CheckedChanged
        If CheckBox70.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox70.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox70.Text)) = 0
        End If
    End Sub




    Private Sub CheckBox71_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox71.CheckedChanged
        If CheckBox71.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox71.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox71.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox72_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox72.CheckedChanged
        If CheckBox72.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox72.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox72.Text)) = 0
        End If
    End Sub
    Private Sub CheckBox73_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox73.CheckedChanged
        If CheckBox73.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox73.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox73.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox74_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox74.CheckedChanged
        If CheckBox74.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox74.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox74.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox75_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox75.CheckedChanged
        If CheckBox75.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox75.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox75.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox76_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox76.CheckedChanged
        If CheckBox76.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox76.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox76.Text)) = 0
        End If
    End Sub
    Private Sub CheckBox78_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox78.CheckedChanged
        If CheckBox78.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox78.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox78.Text)) = 0
        End If
    End Sub



    Private Sub CheckBox80_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox80.CheckedChanged
        If CheckBox80.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox80.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox80.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox79_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox79.CheckedChanged
        If CheckBox79.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox79.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox79.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox77_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox77.CheckedChanged
        If CheckBox77.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox77.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox77.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox81_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox81.CheckedChanged
        If CheckBox81.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox81.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox81.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox82_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox82.CheckedChanged
        If CheckBox82.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox82.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox82.Text)) = 0
        End If
    End Sub
    Private Sub CheckBox83_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox83.CheckedChanged
        If CheckBox83.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox83.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox83.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox84_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox84.CheckedChanged
        If CheckBox84.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox84.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox84.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox85_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox85.CheckedChanged
        If CheckBox85.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox85.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox85.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox86_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox86.CheckedChanged
        If CheckBox86.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox86.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox86.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox87_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox87.CheckedChanged
        If CheckBox87.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox87.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox87.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox88_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox88.CheckedChanged
        If CheckBox88.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox88.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox88.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox89_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox89.CheckedChanged
        If CheckBox89.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox89.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox89.Text)) = 0
        End If
    End Sub

    Private Sub CheckBox90_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox90.CheckedChanged
        If CheckBox90.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox90.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox90.Text)) = 0
        End If
    End Sub

#End Region

    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
        If RadioButton4.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton4.BackColor
            RadioButton4.BackColor = RadioButton4.ForeColor
            RadioButton4.ForeColor = Coleu
            MiseTicket = RadioButton4.Text

        Else
            Coleu = RadioButton4.BackColor
            RadioButton4.BackColor = RadioButton4.ForeColor
            RadioButton4.ForeColor = Coleu
        End If

    End Sub
    Private Sub RadioButton9_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton9.CheckedChanged
        If RadioButton9.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton9.BackColor
            RadioButton9.BackColor = RadioButton9.ForeColor
            RadioButton9.ForeColor = Coleu
            MiseTicket = RadioButton9.Text

        Else
            Coleu = RadioButton9.BackColor
            RadioButton9.BackColor = RadioButton9.ForeColor
            RadioButton9.ForeColor = Coleu
        End If
    End Sub
    Private Sub RadioButton12_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton12.CheckedChanged
        If RadioButton12.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton12.BackColor
            RadioButton12.BackColor = RadioButton12.ForeColor
            RadioButton12.ForeColor = Coleu
            MiseTicket = RadioButton12.Text

        Else
            Coleu = RadioButton12.BackColor
            RadioButton12.BackColor = RadioButton12.ForeColor
            RadioButton12.ForeColor = Coleu
        End If
    End Sub
    Private Sub RadioButton8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton8.CheckedChanged
        If RadioButton8.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton8.BackColor
            RadioButton8.BackColor = RadioButton8.ForeColor
            RadioButton8.ForeColor = Coleu
            MiseTicket = RadioButton8.Text

        Else
            Coleu = RadioButton8.BackColor
            RadioButton8.BackColor = RadioButton8.ForeColor
            RadioButton8.ForeColor = Coleu
        End If
    End Sub
    Private Sub RadioButton10_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton10.CheckedChanged
        If RadioButton10.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton10.BackColor
            RadioButton10.BackColor = RadioButton10.ForeColor
            RadioButton10.ForeColor = Coleu
            MiseTicket = RadioButton10.Text

        Else
            Coleu = RadioButton10.BackColor
            RadioButton10.BackColor = RadioButton10.ForeColor
            RadioButton10.ForeColor = Coleu
        End If
    End Sub
    Private Sub RadioButton7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton7.CheckedChanged
        If RadioButton7.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton7.BackColor
            RadioButton7.BackColor = RadioButton7.ForeColor
            RadioButton7.ForeColor = Coleu
            MiseTicket = RadioButton7.Text

        Else
            Coleu = RadioButton7.BackColor
            RadioButton7.BackColor = RadioButton7.ForeColor
            RadioButton7.ForeColor = Coleu
        End If
    End Sub
    Private Sub RadioButton6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton6.CheckedChanged
        If RadioButton6.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton6.BackColor
            RadioButton6.BackColor = RadioButton6.ForeColor
            RadioButton6.ForeColor = Coleu
            MiseTicket = RadioButton6.Text

        Else
            Coleu = RadioButton6.BackColor
            RadioButton6.BackColor = RadioButton6.ForeColor
            RadioButton6.ForeColor = Coleu
        End If
    End Sub
    Private Sub RadioButton5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton5.CheckedChanged
        If RadioButton5.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton5.BackColor
            RadioButton5.BackColor = RadioButton5.ForeColor
            RadioButton5.ForeColor = Coleu
            MiseTicket = RadioButton5.Text

        Else
            Coleu = RadioButton5.BackColor
            RadioButton5.BackColor = RadioButton5.ForeColor
            RadioButton5.ForeColor = Coleu
        End If
    End Sub
    Private Sub RadioButton11_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton11.CheckedChanged

        If RadioButton11.Checked = True Then ' Teste si le bouton est coché.

            Coleu = RadioButton11.BackColor
            RadioButton11.BackColor = RadioButton11.ForeColor
            RadioButton11.ForeColor = Coleu
            MiseTicket = RadioButton11.Text

        Else
            Coleu = RadioButton11.BackColor
            RadioButton11.BackColor = RadioButton11.ForeColor
            RadioButton11.ForeColor = Coleu
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        PbTickGagnant.Visible = False : PbTickGagnant.Refresh()
        Using oTicket As New BL_Lotto.BlTicket(source)
            Dim ODsTicket, ODsTicket1, ODsPari, oDsTirage As DataSet
            'Dim oDsNumJou As DataSet
            Dim pari_num As String

            'Dim tbOtick() As String
            Dim pariGagner As Boolean
            Dim proch_tirage As Integer
            Dim Gagner As Boolean = False
            'tbOtick = TbIdticket.Text.Substring(8)
            'tbOtick = TbIdticket.Text.Split(CChar("/"))

            Gains = 0
            COD_SAL = TbIdticket.Text.Substring(8)

            'ID_TICKET = tbOtick(2)
            ODsTicket = oTicket.TicketParCodebar(TbIdticket.Text)

            If COD_SAL <> My.Settings.CODE_SALE Then
                    MessageBox.Show(" Ce Ticket Ne Provient Pas De Cette Salle ", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If

            If (ODsTicket.Tables("ticket").Rows.Count > 0) Then
                ID_TICKET = ODsTicket.Tables("ticket").Rows(0).Item("ID_TICKET").ToString

                ODsTicket1 = oTicket.TicketNonPerimParId(ID_TICKET)

                oDsTirage = oTicket.GetLastTirageNum

                If oDsTirage.Tables("tirage").Rows.Count > 0 Then
                    proch_tirage = CInt(oDsTirage.Tables("tirage").Rows(0).Item("NUM_TIRAGE")) + 1
                Else
                    proch_tirage = 1
                End If


                'TbCagnotte.Text = "Cagnotte : " & Gains & " FCFA"

                If (0 <> CInt(ODsTicket.Tables("ticket").Rows(0).Item("ID_USER_S"))) Then
                    MessageBox.Show(" Ticket gagnant deja servit Par la caisse N:" & ODsTicket.Tables("ticket").Rows(0).Item("ID_USER_S").ToString, "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    TbIdticket.Text = ""
                    Exit Sub
                End If
                'on verifie si le ticket avait deja été annuler
                If (0 = CInt(ODsTicket.Tables("ticket").Rows(0).Item("STATUS"))) Then
                    MessageBox.Show(" Ticket deja annuler", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    TbIdticket.Text = ""
                    Exit Sub
                End If

                If (proch_tirage = CInt(ODsTicket.Tables("ticket").Rows(0).Item("NUM_TIRAGE"))) Then
                    ' il faut annuler le ticket
                    Annuler_Ticket(ID_TICKET)
                Else
                    ' on verifie si le ticket est gagant

                    ODsPari = oTicket.GetPari(ID_TICKET)

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then

                        For i As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(i).Item("ID_PARI").ToString)
                            pari_num = ""
                            pariGagner = oTicket.verifWinner(ODsPari.Tables("pari").Rows(i).Item("ID_PARI").ToString,
                                                                            ODsTicket.Tables("ticket").Rows(0).Item("NUM_TIRAGE").ToString)
                            If (pariGagner) Then
                                Gains += (CInt(ODsPari.Tables("pari").Rows(i).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(i).Item("QUOTA")))
                            End If
                            Gagner = Gagner Or pariGagner

                        Next


                        If (Gagner) Then
                            ' annimation
                            For j As Integer = 0 To 50
                                If (j Mod 2 = 1) Then
                                    'PbTickGagnant.ImageLocation = "button_ok1.png"
                                    PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_ok1 : PbTickGagnant.Refresh()
                                Else
                                    'PbTickGagnant.ImageLocation = "button_cancel1.png"
                                    PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_cancel1 : PbTickGagnant.Refresh()
                                End If
                                'System.Threading.Thread.Sleep(1000)
                            Next

                            'PbTickGagnant.ImageLocation = "button_ok1.png"
                            PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_ok1
                            valider_Ticket(ID_TICKET)

                        Else
                            'oTicket.ValideTicket(ID_TICKET)

                            ' annimation


                            For j As Integer = 0 To 50
                                If (j Mod 2 = 1) Then
                                    '.ImageLocation = "button_ok1.png"
                                    PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_ok1 : PbTickGagnant.Refresh()
                                Else
                                    'PbTickGagnant.ImageLocation = "button_cancel1.png"
                                    PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_cancel1 : PbTickGagnant.Refresh()
                                End If
                            Next
                            PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_cancel1
                            'PbTickGagnant.ImageLocation = "button_cancel1.png"
                        End If

                    End If

                    'validation Jackpot
                    Dim dsjackpot As DataSet

                    Using ojacpot As New BL_Lotto.BlJackot(source)
                        dsjackpot = ojacpot.GetGagnantTicketJackpot(ID_TICKET)

                        If (dsjackpot.Tables("jackpot").Rows.Count > 0) Then
                            For p As Integer = 0 To dsjackpot.Tables("jackpot").Rows.Count - 1

                                If (0 <> CInt(dsjackpot.Tables("jackpot").Rows(p).Item("ID_USER_S"))) Then
                                    MessageBox.Show(" Ticket gagnant Du Jackpot deja servit Par la caisse N:" & dsjackpot.Tables("jackpot").Rows(p).Item("ID_USER_S").ToString, "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    TbIdticket.Text = ""
                                Else
                                    GainsJackpot = CInt(dsjackpot.Tables("jackpot").Rows(p).Item("montant"))
                                    valider_Ticket_Jacpot(ID_TICKET)
                                End If

                            Next

                        End If
                    End Using

                End If
                RaffraichirCaisse()

            Else
                MessageBox.Show(" Ticket invalide.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If


        End Using
        PbTickGagnant.Visible = True : PbTickGagnant.Refresh()
        TbIdticket.Text = ""
    End Sub
    Public Sub valider_Ticket_Jacpot(ByVal ID_TICKET As String)
        Using oTicket As New BL_Lotto.BlTicket(source)



            isFinish = False
            cancelErr = False
            isTimeout = False

            If (oTicket.ServirTicketJack(ID_TICKET, CONNECTED_USERID)) Then
                'impression du ticket


                Try

                    JackTickPrint = New PrintDocument

                    'Dim result As New DialogResult
                    'result = printdialog.ShowDialog

                    'If (result = DialogResult.OK) Then
                    If PRINTER_NAME <> vbNullChar Then
                        'MsgBox(PRINTER_NAME)
                        JackTickPrint.PrinterSettings.PrinterName = PRINTER_NAME
                    End If


                    If JackTickPrint.PrinterSettings.IsValid Then

                        JackTickPrint.DocumentName = "Ticket " & ticket
                        ' Start printing
                        JackTickPrint.Print()
                        ' Wait until callback function will say that the task is done.
                        ' When done, end the monitoring of printer status.


                    Else
                        MessageBox.Show("Printer is not available.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                    'End If
                Catch ex As Exception
                    MessageBox.Show("Failed to open StatusAPI.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try

                ticket += 1




            Else
                MessageBox.Show("Failed Save Jackpot.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Using
    End Sub


    Public Sub valider_Ticket(ByVal ID_TICKET As String)
        Using oTicket As New BL_Lotto.BlTicket(source)


            If (oTicket.ServirTicket(ID_TICKET, CONNECTED_USERID)) Then
                'impression du ticket


                Try
                    ' Set the callback function that will monitor printer status.


                    TickPrint = New PrintDocument


                    If PRINTER_NAME <> vbNullChar Then
                        'MsgBox(PRINTER_NAME)
                        TickPrint.PrinterSettings.PrinterName = PRINTER_NAME
                    End If
                    If TickPrint.PrinterSettings.IsValid Then

                        TickPrint.DocumentName = "Ticket " & ticket
                        ' Start printing
                        TickPrint.Print()
                        ' Wait until callback function will say that the task is done.
                        ' When done, end the monitoring of printer status.

                        ' Display the status/error message.
                        'DisplayStatusMessage()

                        ' If roll paper end is detected, cancel the print job


                    Else
                        MessageBox.Show("Printer is not available.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If


                Catch ex As Exception
                    MessageBox.Show("Failed to open StatusAPI.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try

                ticket += 1




            Else

            End If
        End Using
    End Sub




    Public Sub Annuler_Ticket(ByVal ID_TICKET As String)



        Using oTicket As New BL_Lotto.BlTicket(source)

            If (oTicket.ValideTicket(ID_TICKET)) Then
                'impression du ticket
                ' Set the callback function that will monitor printer status.


                printdialog = New PrintDialog
                AnTickPrint = New PrintDocument
                printdialog.Document = AnTickPrint


                'Dim result As New DialogResult
                'result = printdialog.ShowDialog

                'If (result = DialogResult.OK) Then
                If PRINTER_NAME <> vbNullChar Then
                    'MsgBox(PRINTER_NAME)
                    AnTickPrint.PrinterSettings.PrinterName = PRINTER_NAME
                End If


                'AnTickPrint.Print()


                If AnTickPrint.PrinterSettings.IsValid Then
                    AnTickPrint.DocumentName = "Elite Ticket"
                    ' Start printing
                    AnTickPrint.Print()
                    ' Wait until callback function will say that the task is done.
                    ' When done, end the monitoring of printer status.
                Else
                    MessageBox.Show("Printer is not available.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
                'End If
            Else
                MessageBox.Show("Failed to set callback function.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            ticket += 1

        End Using
    End Sub


    Private Sub TbIdticket_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TbIdticket.LostFocus
        Me.Focus()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim resultat As String = ""
        'Dim retour As Boolean

        Using OBlTicket As New BL_Lotto.BlTicket(source)
            'Get a new Project DataSet

            Dim dsTirage As New DataSet
            dsTirage = OBlTicket.GetLastTirageNum
            Dim num_tirage As Integer
            If dsTirage.Tables("tirage").Rows.Count > 0 Then
                num_tirage = CInt(dsTirage.Tables("tirage").Rows(0).Item("NUM_TIRAGE").ToString) + 1
            Else
                num_tirage = 1
            End If

            With Dgd_Tick

                If VerifFacilitiesTab_Numero() Then
                    countTab_Numero(resultat)
                    '.Columns.Clear()
                    .ColumnCount = 4
                    '.ColumnHeadersVisible = False
                    '{CStr(num_tirage), CStr(Mode_selected), resultat, MiseTicket}

                    'Dim row0 As String() = {CStr(num_tirage), CStr(Mode_selected), resultat, MiseTicket}

                    Select Case Mode_selected
                        Case 1
                            For i As Integer = 0 To Tab_Numero.Length - 1
                                If Tab_Numero(i) = 1 Then
                                    Dim row0 As String() = {CStr(num_tirage), CStr(Mode_selected), i.ToString, MiseTicket}
                                    .Rows.Add(row0)
                                End If
                            Next
                        Case 2
                            Dim row0 As String() = {CStr(num_tirage), CStr(Mode_selected), resultat, MiseTicket}
                            .Rows.Add(row0)
                        Case 3
                            Dim row0 As String() = {CStr(num_tirage), CStr(Mode_selected), resultat, MiseTicket}
                            .Rows.Add(row0)
                        Case Else
                    End Select




                    'on remet les compteur a zero
                    remiseZero()
                Else
                    MessageBox.Show("le Mode et les entrées ne coincide pas.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    remiseZero()
                End If

            End With

        End Using

        bt_print.PerformClick()



    End Sub

    Public Sub RaffraichirCaisse()
        Dim Gagner As Boolean = False
        Dim Gains As Integer = 0
        Dim Tabdatedebut(), Tabdatefin() As String
        Dim datedebut, datefin As String
        Dim dsTicket, dsTickJackMon, dsTouticket, dsPari, ODsPari As DataSet
        Dim TotalmontantJack As Integer = 0
        Dim ID_TICKET_CAIS, TotalMise, TotalSortie As String


        Tabdatedebut = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
        datedebut = Tabdatedebut(2) & "/" & Tabdatedebut(1) & "/" & Tabdatedebut(0)


        Tabdatefin = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
        datefin = Tabdatefin(2) & "/" & Tabdatefin(1) & "/" & Tabdatefin(0)
        'datefin = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTickJackMon = oCompte.getStatTickJackMonParUser(CONNECTED_USERID, datedebut & " 00:00:00", datefin & " 23:59:59")

            dsTicket = oCompte.getStatTicketParUser(CONNECTED_USERID, datedebut & " 00:00:00", datefin & " 23:59:59")
            dsPari = oCompte.getStatPariParUser(CONNECTED_USERID, datedebut & " 00:00:00", datefin & " 23:59:59")

            'If (dsTicketJack.Tables("ticket").Rows.Count > 0) Then
            '    LbNbtTicketJack.Text = dsTicketJack.Tables("ticket").Rows(0).Item("NB_TICKET").ToString
            'End If
            If (dsTickJackMon.Tables("ticket").Rows.Count > 0) Then
                If (Not dsTickJackMon.Tables("ticket").Rows(0).IsNull("TOTAL_MONTANT")) Then
                    TotalmontantJack = CInt(dsTickJackMon.Tables("ticket").Rows(0).Item("TOTAL_MONTANT"))
                Else
                    TotalmontantJack = 0
                End If

            End If

            If (dsTicket.Tables("ticket").Rows.Count > 0) Then
                'LbNbTicket.Text = dsTicket.Tables("ticket").Rows(0).Item("NB_TICKET").ToString
                TotalMise = dsPari.Tables("pari").Rows(0).Item("TOTALMISE").ToString

                'calcul du nombre de ticket gagnant
                dsTouticket = oCompte.getListTicketServi(CONNECTED_USERID, datedebut & " 00:00:00", datefin & " 23:59:59")
                Dim MontantSortie, nbticketGagnant As Integer
                nbticketGagnant = 0
                MontantSortie = 0
                Using oTicket As New BL_Lotto.BlTicket(source)
                    For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
                        ID_TICKET_CAIS = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                        'Dim oDsNumJou As DataSet

                        ODsPari = oTicket.GetPari(ID_TICKET_CAIS)
                        Gagner = False
                        Gains = 0
                        'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                        If (ODsPari.Tables("pari").Rows.Count > 0) Then

                            For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                                'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                                If (oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                                dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)) Then
                                    Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))
                                    Gagner = True
                                End If

                            Next

                        End If

                        'oDsNumJou = oTicket.getNumJouer(ID_TICKET_CAIS)
                        If (Gagner) Then
                            nbticketGagnant += 1
                            MontantSortie += CInt(Gains)
                        End If

                    Next
                End Using
                MontantSortie += TotalmontantJack
                TotalSortie = MontantSortie.ToString

                If TotalMise = "" Then
                    TotalMise = 0.ToString
                End If

                Dim benef As Integer = CInt(TotalMise) - CInt(TotalSortie)

                LbCaisseEntrance.Text = benef.ToString & " FCFA"
            Else
                Dim benef As Integer = 0
                LbCaisseEntrance.Text = benef.ToString & " FCFA"
            End If
        End Using

    End Sub

    Private Sub GroupBox2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub CheckBox17_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox17.CheckedChanged
        If CheckBox17.Checked = True Then ' Teste si le bouton est coché.
            Tab_Numero(CInt(CheckBox17.Text)) = 1
        Else
            Tab_Numero(CInt(CheckBox17.Text)) = 0
        End If
    End Sub

    Private Sub FrmTicket_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        'FrmPrincipal.Show()
    End Sub

End Class