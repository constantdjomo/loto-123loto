﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTicket
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bt_print = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RadioButton12 = New System.Windows.Forms.RadioButton()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.RadioButton11 = New System.Windows.Forms.RadioButton()
        Me.RadioButton9 = New System.Windows.Forms.RadioButton()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.RadioButton10 = New System.Windows.Forms.RadioButton()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.LB_NumTirage = New System.Windows.Forms.Label()
        Me.BtCancel = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CheckBox90 = New System.Windows.Forms.CheckBox()
        Me.CheckBox80 = New System.Windows.Forms.CheckBox()
        Me.CheckBox70 = New System.Windows.Forms.CheckBox()
        Me.CheckBox60 = New System.Windows.Forms.CheckBox()
        Me.CheckBox50 = New System.Windows.Forms.CheckBox()
        Me.CheckBox40 = New System.Windows.Forms.CheckBox()
        Me.CheckBox30 = New System.Windows.Forms.CheckBox()
        Me.CheckBox20 = New System.Windows.Forms.CheckBox()
        Me.CheckBox8 = New System.Windows.Forms.CheckBox()
        Me.CheckBox89 = New System.Windows.Forms.CheckBox()
        Me.CheckBox79 = New System.Windows.Forms.CheckBox()
        Me.CheckBox69 = New System.Windows.Forms.CheckBox()
        Me.CheckBox59 = New System.Windows.Forms.CheckBox()
        Me.CheckBox49 = New System.Windows.Forms.CheckBox()
        Me.CheckBox39 = New System.Windows.Forms.CheckBox()
        Me.CheckBox29 = New System.Windows.Forms.CheckBox()
        Me.CheckBox19 = New System.Windows.Forms.CheckBox()
        Me.CheckBox9 = New System.Windows.Forms.CheckBox()
        Me.CheckBox88 = New System.Windows.Forms.CheckBox()
        Me.CheckBox78 = New System.Windows.Forms.CheckBox()
        Me.CheckBox68 = New System.Windows.Forms.CheckBox()
        Me.CheckBox58 = New System.Windows.Forms.CheckBox()
        Me.CheckBox48 = New System.Windows.Forms.CheckBox()
        Me.CheckBox38 = New System.Windows.Forms.CheckBox()
        Me.CheckBox28 = New System.Windows.Forms.CheckBox()
        Me.CheckBox18 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox87 = New System.Windows.Forms.CheckBox()
        Me.CheckBox77 = New System.Windows.Forms.CheckBox()
        Me.CheckBox67 = New System.Windows.Forms.CheckBox()
        Me.CheckBox57 = New System.Windows.Forms.CheckBox()
        Me.CheckBox47 = New System.Windows.Forms.CheckBox()
        Me.CheckBox37 = New System.Windows.Forms.CheckBox()
        Me.CheckBox27 = New System.Windows.Forms.CheckBox()
        Me.CheckBox17 = New System.Windows.Forms.CheckBox()
        Me.CheckBox86 = New System.Windows.Forms.CheckBox()
        Me.CheckBox76 = New System.Windows.Forms.CheckBox()
        Me.CheckBox66 = New System.Windows.Forms.CheckBox()
        Me.CheckBox56 = New System.Windows.Forms.CheckBox()
        Me.CheckBox46 = New System.Windows.Forms.CheckBox()
        Me.CheckBox36 = New System.Windows.Forms.CheckBox()
        Me.CheckBox26 = New System.Windows.Forms.CheckBox()
        Me.CheckBox16 = New System.Windows.Forms.CheckBox()
        Me.CheckBox10 = New System.Windows.Forms.CheckBox()
        Me.CheckBox85 = New System.Windows.Forms.CheckBox()
        Me.CheckBox75 = New System.Windows.Forms.CheckBox()
        Me.CheckBox65 = New System.Windows.Forms.CheckBox()
        Me.CheckBox55 = New System.Windows.Forms.CheckBox()
        Me.CheckBox45 = New System.Windows.Forms.CheckBox()
        Me.CheckBox35 = New System.Windows.Forms.CheckBox()
        Me.CheckBox25 = New System.Windows.Forms.CheckBox()
        Me.CheckBox15 = New System.Windows.Forms.CheckBox()
        Me.CheckBox7 = New System.Windows.Forms.CheckBox()
        Me.CheckBox84 = New System.Windows.Forms.CheckBox()
        Me.CheckBox74 = New System.Windows.Forms.CheckBox()
        Me.CheckBox64 = New System.Windows.Forms.CheckBox()
        Me.CheckBox54 = New System.Windows.Forms.CheckBox()
        Me.CheckBox44 = New System.Windows.Forms.CheckBox()
        Me.CheckBox34 = New System.Windows.Forms.CheckBox()
        Me.CheckBox24 = New System.Windows.Forms.CheckBox()
        Me.CheckBox14 = New System.Windows.Forms.CheckBox()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.CheckBox83 = New System.Windows.Forms.CheckBox()
        Me.CheckBox73 = New System.Windows.Forms.CheckBox()
        Me.CheckBox63 = New System.Windows.Forms.CheckBox()
        Me.CheckBox53 = New System.Windows.Forms.CheckBox()
        Me.CheckBox43 = New System.Windows.Forms.CheckBox()
        Me.CheckBox33 = New System.Windows.Forms.CheckBox()
        Me.CheckBox23 = New System.Windows.Forms.CheckBox()
        Me.CheckBox13 = New System.Windows.Forms.CheckBox()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.CheckBox82 = New System.Windows.Forms.CheckBox()
        Me.CheckBox72 = New System.Windows.Forms.CheckBox()
        Me.CheckBox62 = New System.Windows.Forms.CheckBox()
        Me.CheckBox52 = New System.Windows.Forms.CheckBox()
        Me.CheckBox42 = New System.Windows.Forms.CheckBox()
        Me.CheckBox32 = New System.Windows.Forms.CheckBox()
        Me.CheckBox22 = New System.Windows.Forms.CheckBox()
        Me.CheckBox12 = New System.Windows.Forms.CheckBox()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CheckBox81 = New System.Windows.Forms.CheckBox()
        Me.CheckBox71 = New System.Windows.Forms.CheckBox()
        Me.CheckBox61 = New System.Windows.Forms.CheckBox()
        Me.CheckBox51 = New System.Windows.Forms.CheckBox()
        Me.CheckBox41 = New System.Windows.Forms.CheckBox()
        Me.CheckBox31 = New System.Windows.Forms.CheckBox()
        Me.CheckBox21 = New System.Windows.Forms.CheckBox()
        Me.CheckBox11 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Dgd_Tick = New System.Windows.Forms.DataGridView()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LbCaisseEntrance = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.PbTickGagnant = New System.Windows.Forms.PictureBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TbIdticket = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Lb_Min = New System.Windows.Forms.Label()
        Me.LB_second = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Dgv_LastTick = New System.Windows.Forms.DataGridView()
        Me.PbVoyant = New System.Windows.Forms.PictureBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.Dgd_Tick, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.PbTickGagnant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.Dgv_LastTick, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbVoyant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bt_print
        '
        Me.bt_print.BackColor = System.Drawing.Color.DarkGreen
        Me.bt_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bt_print.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt_print.ForeColor = System.Drawing.Color.White
        Me.bt_print.Location = New System.Drawing.Point(464, 643)
        Me.bt_print.Name = "bt_print"
        Me.bt_print.Size = New System.Drawing.Size(16, 35)
        Me.bt_print.TabIndex = 9
        Me.bt_print.Text = "Print"
        Me.bt_print.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Black
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(37, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Padding = New System.Windows.Forms.Padding(5)
        Me.Label2.Size = New System.Drawing.Size(113, 33)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tirage No:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadioButton12)
        Me.GroupBox1.Controls.Add(Me.RadioButton7)
        Me.GroupBox1.Controls.Add(Me.RadioButton11)
        Me.GroupBox1.Controls.Add(Me.RadioButton9)
        Me.GroupBox1.Controls.Add(Me.RadioButton6)
        Me.GroupBox1.Controls.Add(Me.RadioButton10)
        Me.GroupBox1.Controls.Add(Me.RadioButton8)
        Me.GroupBox1.Controls.Add(Me.RadioButton5)
        Me.GroupBox1.Controls.Add(Me.RadioButton4)
        Me.GroupBox1.Controls.Add(Me.LB_NumTirage)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(119, 162)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(361, 245)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Infos Ticket"
        '
        'RadioButton12
        '
        Me.RadioButton12.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton12.BackColor = System.Drawing.Color.Gray
        Me.RadioButton12.Enabled = False
        Me.RadioButton12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton12.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton12.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton12.Location = New System.Drawing.Point(41, 178)
        Me.RadioButton12.Name = "RadioButton12"
        Me.RadioButton12.Size = New System.Drawing.Size(87, 47)
        Me.RadioButton12.TabIndex = 3
        Me.RadioButton12.Text = "2000"
        Me.RadioButton12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton12.UseVisualStyleBackColor = False
        '
        'RadioButton7
        '
        Me.RadioButton7.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton7.BackColor = System.Drawing.Color.Gray
        Me.RadioButton7.Enabled = False
        Me.RadioButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton7.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton7.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton7.Location = New System.Drawing.Point(41, 125)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(87, 47)
        Me.RadioButton7.TabIndex = 3
        Me.RadioButton7.Text = "500"
        Me.RadioButton7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton7.UseVisualStyleBackColor = False
        '
        'RadioButton11
        '
        Me.RadioButton11.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton11.BackColor = System.Drawing.Color.Gray
        Me.RadioButton11.Enabled = False
        Me.RadioButton11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton11.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton11.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton11.Location = New System.Drawing.Point(240, 178)
        Me.RadioButton11.Name = "RadioButton11"
        Me.RadioButton11.Size = New System.Drawing.Size(87, 47)
        Me.RadioButton11.TabIndex = 3
        Me.RadioButton11.Text = "3000"
        Me.RadioButton11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton11.UseVisualStyleBackColor = False
        '
        'RadioButton9
        '
        Me.RadioButton9.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton9.BackColor = System.Drawing.Color.Gray
        Me.RadioButton9.Enabled = False
        Me.RadioButton9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton9.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton9.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton9.Location = New System.Drawing.Point(240, 125)
        Me.RadioButton9.Name = "RadioButton9"
        Me.RadioButton9.Size = New System.Drawing.Size(87, 47)
        Me.RadioButton9.TabIndex = 3
        Me.RadioButton9.Text = "1500"
        Me.RadioButton9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton9.UseVisualStyleBackColor = False
        '
        'RadioButton6
        '
        Me.RadioButton6.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton6.BackColor = System.Drawing.Color.Gray
        Me.RadioButton6.Enabled = False
        Me.RadioButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton6.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton6.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton6.Location = New System.Drawing.Point(240, 72)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(87, 47)
        Me.RadioButton6.TabIndex = 3
        Me.RadioButton6.Text = "300"
        Me.RadioButton6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton6.UseVisualStyleBackColor = False
        '
        'RadioButton10
        '
        Me.RadioButton10.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton10.BackColor = System.Drawing.Color.Gray
        Me.RadioButton10.Enabled = False
        Me.RadioButton10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton10.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton10.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton10.Location = New System.Drawing.Point(139, 178)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(87, 47)
        Me.RadioButton10.TabIndex = 3
        Me.RadioButton10.Text = "2500"
        Me.RadioButton10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton10.UseVisualStyleBackColor = False
        '
        'RadioButton8
        '
        Me.RadioButton8.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton8.BackColor = System.Drawing.Color.Gray
        Me.RadioButton8.Enabled = False
        Me.RadioButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton8.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton8.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton8.Location = New System.Drawing.Point(139, 125)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(87, 47)
        Me.RadioButton8.TabIndex = 3
        Me.RadioButton8.Text = "1000"
        Me.RadioButton8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton8.UseVisualStyleBackColor = False
        '
        'RadioButton5
        '
        Me.RadioButton5.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton5.BackColor = System.Drawing.Color.Gray
        Me.RadioButton5.Enabled = False
        Me.RadioButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton5.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton5.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton5.Location = New System.Drawing.Point(139, 72)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(87, 47)
        Me.RadioButton5.TabIndex = 3
        Me.RadioButton5.Text = "200"
        Me.RadioButton5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton5.UseVisualStyleBackColor = False
        '
        'RadioButton4
        '
        Me.RadioButton4.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton4.BackColor = System.Drawing.Color.Green
        Me.RadioButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton4.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton4.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton4.Location = New System.Drawing.Point(41, 72)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(87, 47)
        Me.RadioButton4.TabIndex = 3
        Me.RadioButton4.Text = "100"
        Me.RadioButton4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton4.UseVisualStyleBackColor = False
        '
        'LB_NumTirage
        '
        Me.LB_NumTirage.AutoSize = True
        Me.LB_NumTirage.BackColor = System.Drawing.Color.Black
        Me.LB_NumTirage.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_NumTirage.Location = New System.Drawing.Point(150, 25)
        Me.LB_NumTirage.Name = "LB_NumTirage"
        Me.LB_NumTirage.Padding = New System.Windows.Forms.Padding(5)
        Me.LB_NumTirage.Size = New System.Drawing.Size(64, 33)
        Me.LB_NumTirage.TabIndex = 2
        Me.LB_NumTirage.Text = "1524"
        '
        'BtCancel
        '
        Me.BtCancel.BackColor = System.Drawing.Color.DarkGreen
        Me.BtCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtCancel.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtCancel.ForeColor = System.Drawing.Color.White
        Me.BtCancel.Location = New System.Drawing.Point(149, 643)
        Me.BtCancel.Name = "BtCancel"
        Me.BtCancel.Size = New System.Drawing.Size(121, 35)
        Me.BtCancel.TabIndex = 12
        Me.BtCancel.Text = "Cancel"
        Me.BtCancel.UseVisualStyleBackColor = False
        Me.BtCancel.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.AutoSize = True
        Me.GroupBox2.Controls.Add(Me.CheckBox90)
        Me.GroupBox2.Controls.Add(Me.CheckBox80)
        Me.GroupBox2.Controls.Add(Me.CheckBox70)
        Me.GroupBox2.Controls.Add(Me.CheckBox60)
        Me.GroupBox2.Controls.Add(Me.CheckBox50)
        Me.GroupBox2.Controls.Add(Me.CheckBox40)
        Me.GroupBox2.Controls.Add(Me.CheckBox30)
        Me.GroupBox2.Controls.Add(Me.CheckBox20)
        Me.GroupBox2.Controls.Add(Me.CheckBox8)
        Me.GroupBox2.Controls.Add(Me.CheckBox89)
        Me.GroupBox2.Controls.Add(Me.CheckBox79)
        Me.GroupBox2.Controls.Add(Me.CheckBox69)
        Me.GroupBox2.Controls.Add(Me.CheckBox59)
        Me.GroupBox2.Controls.Add(Me.CheckBox49)
        Me.GroupBox2.Controls.Add(Me.CheckBox39)
        Me.GroupBox2.Controls.Add(Me.CheckBox29)
        Me.GroupBox2.Controls.Add(Me.CheckBox19)
        Me.GroupBox2.Controls.Add(Me.CheckBox9)
        Me.GroupBox2.Controls.Add(Me.CheckBox88)
        Me.GroupBox2.Controls.Add(Me.CheckBox78)
        Me.GroupBox2.Controls.Add(Me.CheckBox68)
        Me.GroupBox2.Controls.Add(Me.CheckBox58)
        Me.GroupBox2.Controls.Add(Me.CheckBox48)
        Me.GroupBox2.Controls.Add(Me.CheckBox38)
        Me.GroupBox2.Controls.Add(Me.CheckBox28)
        Me.GroupBox2.Controls.Add(Me.CheckBox18)
        Me.GroupBox2.Controls.Add(Me.CheckBox3)
        Me.GroupBox2.Controls.Add(Me.CheckBox87)
        Me.GroupBox2.Controls.Add(Me.CheckBox77)
        Me.GroupBox2.Controls.Add(Me.CheckBox67)
        Me.GroupBox2.Controls.Add(Me.CheckBox57)
        Me.GroupBox2.Controls.Add(Me.CheckBox47)
        Me.GroupBox2.Controls.Add(Me.CheckBox37)
        Me.GroupBox2.Controls.Add(Me.CheckBox27)
        Me.GroupBox2.Controls.Add(Me.CheckBox17)
        Me.GroupBox2.Controls.Add(Me.CheckBox86)
        Me.GroupBox2.Controls.Add(Me.CheckBox76)
        Me.GroupBox2.Controls.Add(Me.CheckBox66)
        Me.GroupBox2.Controls.Add(Me.CheckBox56)
        Me.GroupBox2.Controls.Add(Me.CheckBox46)
        Me.GroupBox2.Controls.Add(Me.CheckBox36)
        Me.GroupBox2.Controls.Add(Me.CheckBox26)
        Me.GroupBox2.Controls.Add(Me.CheckBox16)
        Me.GroupBox2.Controls.Add(Me.CheckBox10)
        Me.GroupBox2.Controls.Add(Me.CheckBox85)
        Me.GroupBox2.Controls.Add(Me.CheckBox75)
        Me.GroupBox2.Controls.Add(Me.CheckBox65)
        Me.GroupBox2.Controls.Add(Me.CheckBox55)
        Me.GroupBox2.Controls.Add(Me.CheckBox45)
        Me.GroupBox2.Controls.Add(Me.CheckBox35)
        Me.GroupBox2.Controls.Add(Me.CheckBox25)
        Me.GroupBox2.Controls.Add(Me.CheckBox15)
        Me.GroupBox2.Controls.Add(Me.CheckBox7)
        Me.GroupBox2.Controls.Add(Me.CheckBox84)
        Me.GroupBox2.Controls.Add(Me.CheckBox74)
        Me.GroupBox2.Controls.Add(Me.CheckBox64)
        Me.GroupBox2.Controls.Add(Me.CheckBox54)
        Me.GroupBox2.Controls.Add(Me.CheckBox44)
        Me.GroupBox2.Controls.Add(Me.CheckBox34)
        Me.GroupBox2.Controls.Add(Me.CheckBox24)
        Me.GroupBox2.Controls.Add(Me.CheckBox14)
        Me.GroupBox2.Controls.Add(Me.CheckBox6)
        Me.GroupBox2.Controls.Add(Me.CheckBox83)
        Me.GroupBox2.Controls.Add(Me.CheckBox73)
        Me.GroupBox2.Controls.Add(Me.CheckBox63)
        Me.GroupBox2.Controls.Add(Me.CheckBox53)
        Me.GroupBox2.Controls.Add(Me.CheckBox43)
        Me.GroupBox2.Controls.Add(Me.CheckBox33)
        Me.GroupBox2.Controls.Add(Me.CheckBox23)
        Me.GroupBox2.Controls.Add(Me.CheckBox13)
        Me.GroupBox2.Controls.Add(Me.CheckBox5)
        Me.GroupBox2.Controls.Add(Me.CheckBox82)
        Me.GroupBox2.Controls.Add(Me.CheckBox72)
        Me.GroupBox2.Controls.Add(Me.CheckBox62)
        Me.GroupBox2.Controls.Add(Me.CheckBox52)
        Me.GroupBox2.Controls.Add(Me.CheckBox42)
        Me.GroupBox2.Controls.Add(Me.CheckBox32)
        Me.GroupBox2.Controls.Add(Me.CheckBox22)
        Me.GroupBox2.Controls.Add(Me.CheckBox12)
        Me.GroupBox2.Controls.Add(Me.CheckBox4)
        Me.GroupBox2.Controls.Add(Me.CheckBox81)
        Me.GroupBox2.Controls.Add(Me.CheckBox71)
        Me.GroupBox2.Controls.Add(Me.CheckBox61)
        Me.GroupBox2.Controls.Add(Me.CheckBox51)
        Me.GroupBox2.Controls.Add(Me.CheckBox41)
        Me.GroupBox2.Controls.Add(Me.CheckBox31)
        Me.GroupBox2.Controls.Add(Me.CheckBox21)
        Me.GroupBox2.Controls.Add(Me.CheckBox11)
        Me.GroupBox2.Controls.Add(Me.CheckBox2)
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(735, 601)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Numero"
        '
        'CheckBox90
        '
        Me.CheckBox90.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox90.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox90.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox90.Location = New System.Drawing.Point(510, 516)
        Me.CheckBox90.Name = "CheckBox90"
        Me.CheckBox90.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox90.TabIndex = 4
        Me.CheckBox90.Text = "88"
        Me.CheckBox90.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox90.UseVisualStyleBackColor = True
        '
        'CheckBox80
        '
        Me.CheckBox80.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox80.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox80.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox80.Location = New System.Drawing.Point(510, 455)
        Me.CheckBox80.Name = "CheckBox80"
        Me.CheckBox80.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox80.TabIndex = 4
        Me.CheckBox80.Text = "78"
        Me.CheckBox80.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox80.UseVisualStyleBackColor = True
        '
        'CheckBox70
        '
        Me.CheckBox70.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox70.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox70.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox70.Location = New System.Drawing.Point(510, 394)
        Me.CheckBox70.Name = "CheckBox70"
        Me.CheckBox70.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox70.TabIndex = 4
        Me.CheckBox70.Text = "68"
        Me.CheckBox70.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox70.UseVisualStyleBackColor = True
        '
        'CheckBox60
        '
        Me.CheckBox60.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox60.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox60.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox60.Location = New System.Drawing.Point(510, 333)
        Me.CheckBox60.Name = "CheckBox60"
        Me.CheckBox60.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox60.TabIndex = 4
        Me.CheckBox60.Text = "58"
        Me.CheckBox60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox60.UseVisualStyleBackColor = True
        '
        'CheckBox50
        '
        Me.CheckBox50.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox50.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox50.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox50.Location = New System.Drawing.Point(510, 272)
        Me.CheckBox50.Name = "CheckBox50"
        Me.CheckBox50.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox50.TabIndex = 4
        Me.CheckBox50.Text = "48"
        Me.CheckBox50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox50.UseVisualStyleBackColor = True
        '
        'CheckBox40
        '
        Me.CheckBox40.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox40.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox40.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox40.Location = New System.Drawing.Point(510, 211)
        Me.CheckBox40.Name = "CheckBox40"
        Me.CheckBox40.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox40.TabIndex = 4
        Me.CheckBox40.Text = "38"
        Me.CheckBox40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox40.UseVisualStyleBackColor = True
        '
        'CheckBox30
        '
        Me.CheckBox30.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox30.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox30.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox30.Location = New System.Drawing.Point(510, 150)
        Me.CheckBox30.Name = "CheckBox30"
        Me.CheckBox30.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox30.TabIndex = 4
        Me.CheckBox30.Text = "28"
        Me.CheckBox30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox30.UseVisualStyleBackColor = True
        '
        'CheckBox20
        '
        Me.CheckBox20.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox20.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox20.Location = New System.Drawing.Point(510, 89)
        Me.CheckBox20.Name = "CheckBox20"
        Me.CheckBox20.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox20.TabIndex = 4
        Me.CheckBox20.Text = "18"
        Me.CheckBox20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox20.UseVisualStyleBackColor = True
        '
        'CheckBox8
        '
        Me.CheckBox8.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox8.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox8.Location = New System.Drawing.Point(510, 28)
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox8.TabIndex = 4
        Me.CheckBox8.Text = "8"
        Me.CheckBox8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox8.UseVisualStyleBackColor = True
        '
        'CheckBox89
        '
        Me.CheckBox89.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox89.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox89.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox89.Location = New System.Drawing.Point(582, 516)
        Me.CheckBox89.Name = "CheckBox89"
        Me.CheckBox89.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox89.TabIndex = 3
        Me.CheckBox89.Text = "89"
        Me.CheckBox89.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox89.UseVisualStyleBackColor = True
        '
        'CheckBox79
        '
        Me.CheckBox79.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox79.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox79.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox79.Location = New System.Drawing.Point(582, 455)
        Me.CheckBox79.Name = "CheckBox79"
        Me.CheckBox79.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox79.TabIndex = 3
        Me.CheckBox79.Text = "79"
        Me.CheckBox79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox79.UseVisualStyleBackColor = True
        '
        'CheckBox69
        '
        Me.CheckBox69.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox69.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox69.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox69.Location = New System.Drawing.Point(582, 394)
        Me.CheckBox69.Name = "CheckBox69"
        Me.CheckBox69.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox69.TabIndex = 3
        Me.CheckBox69.Text = "69"
        Me.CheckBox69.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox69.UseVisualStyleBackColor = True
        '
        'CheckBox59
        '
        Me.CheckBox59.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox59.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox59.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox59.Location = New System.Drawing.Point(582, 333)
        Me.CheckBox59.Name = "CheckBox59"
        Me.CheckBox59.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox59.TabIndex = 3
        Me.CheckBox59.Text = "59"
        Me.CheckBox59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox59.UseVisualStyleBackColor = True
        '
        'CheckBox49
        '
        Me.CheckBox49.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox49.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox49.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox49.Location = New System.Drawing.Point(582, 272)
        Me.CheckBox49.Name = "CheckBox49"
        Me.CheckBox49.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox49.TabIndex = 3
        Me.CheckBox49.Text = "49"
        Me.CheckBox49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox49.UseVisualStyleBackColor = True
        '
        'CheckBox39
        '
        Me.CheckBox39.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox39.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox39.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox39.Location = New System.Drawing.Point(582, 211)
        Me.CheckBox39.Name = "CheckBox39"
        Me.CheckBox39.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox39.TabIndex = 3
        Me.CheckBox39.Text = "39"
        Me.CheckBox39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox39.UseVisualStyleBackColor = True
        '
        'CheckBox29
        '
        Me.CheckBox29.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox29.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox29.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox29.Location = New System.Drawing.Point(582, 150)
        Me.CheckBox29.Name = "CheckBox29"
        Me.CheckBox29.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox29.TabIndex = 3
        Me.CheckBox29.Text = "29"
        Me.CheckBox29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox29.UseVisualStyleBackColor = True
        '
        'CheckBox19
        '
        Me.CheckBox19.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox19.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox19.Location = New System.Drawing.Point(582, 89)
        Me.CheckBox19.Name = "CheckBox19"
        Me.CheckBox19.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox19.TabIndex = 3
        Me.CheckBox19.Text = "19"
        Me.CheckBox19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox19.UseVisualStyleBackColor = True
        '
        'CheckBox9
        '
        Me.CheckBox9.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox9.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox9.Location = New System.Drawing.Point(582, 28)
        Me.CheckBox9.Name = "CheckBox9"
        Me.CheckBox9.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox9.TabIndex = 3
        Me.CheckBox9.Text = "9"
        Me.CheckBox9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox9.UseVisualStyleBackColor = True
        '
        'CheckBox88
        '
        Me.CheckBox88.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox88.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox88.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox88.Location = New System.Drawing.Point(150, 516)
        Me.CheckBox88.Name = "CheckBox88"
        Me.CheckBox88.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox88.TabIndex = 2
        Me.CheckBox88.Text = "83"
        Me.CheckBox88.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox88.UseVisualStyleBackColor = True
        '
        'CheckBox78
        '
        Me.CheckBox78.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox78.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox78.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox78.Location = New System.Drawing.Point(150, 455)
        Me.CheckBox78.Name = "CheckBox78"
        Me.CheckBox78.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox78.TabIndex = 2
        Me.CheckBox78.Text = "73"
        Me.CheckBox78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox78.UseVisualStyleBackColor = True
        '
        'CheckBox68
        '
        Me.CheckBox68.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox68.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox68.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox68.Location = New System.Drawing.Point(150, 394)
        Me.CheckBox68.Name = "CheckBox68"
        Me.CheckBox68.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox68.TabIndex = 2
        Me.CheckBox68.Text = "63"
        Me.CheckBox68.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox68.UseVisualStyleBackColor = True
        '
        'CheckBox58
        '
        Me.CheckBox58.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox58.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox58.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox58.Location = New System.Drawing.Point(150, 333)
        Me.CheckBox58.Name = "CheckBox58"
        Me.CheckBox58.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox58.TabIndex = 2
        Me.CheckBox58.Text = "53"
        Me.CheckBox58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox58.UseVisualStyleBackColor = True
        '
        'CheckBox48
        '
        Me.CheckBox48.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox48.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox48.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox48.Location = New System.Drawing.Point(150, 272)
        Me.CheckBox48.Name = "CheckBox48"
        Me.CheckBox48.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox48.TabIndex = 2
        Me.CheckBox48.Text = "43"
        Me.CheckBox48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox48.UseVisualStyleBackColor = True
        '
        'CheckBox38
        '
        Me.CheckBox38.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox38.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox38.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox38.Location = New System.Drawing.Point(150, 211)
        Me.CheckBox38.Name = "CheckBox38"
        Me.CheckBox38.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox38.TabIndex = 2
        Me.CheckBox38.Text = "33"
        Me.CheckBox38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox38.UseVisualStyleBackColor = True
        '
        'CheckBox28
        '
        Me.CheckBox28.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox28.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox28.Location = New System.Drawing.Point(150, 150)
        Me.CheckBox28.Name = "CheckBox28"
        Me.CheckBox28.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox28.TabIndex = 2
        Me.CheckBox28.Text = "23"
        Me.CheckBox28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox28.UseVisualStyleBackColor = True
        '
        'CheckBox18
        '
        Me.CheckBox18.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox18.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox18.Location = New System.Drawing.Point(150, 89)
        Me.CheckBox18.Name = "CheckBox18"
        Me.CheckBox18.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox18.TabIndex = 2
        Me.CheckBox18.Text = "13"
        Me.CheckBox18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox18.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox3.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox3.Location = New System.Drawing.Point(150, 28)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox3.TabIndex = 2
        Me.CheckBox3.Text = "3"
        Me.CheckBox3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox87
        '
        Me.CheckBox87.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox87.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox87.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox87.Location = New System.Drawing.Point(654, 516)
        Me.CheckBox87.Name = "CheckBox87"
        Me.CheckBox87.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox87.TabIndex = 1
        Me.CheckBox87.Text = "90"
        Me.CheckBox87.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox87.UseVisualStyleBackColor = True
        '
        'CheckBox77
        '
        Me.CheckBox77.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox77.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox77.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox77.Location = New System.Drawing.Point(654, 455)
        Me.CheckBox77.Name = "CheckBox77"
        Me.CheckBox77.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox77.TabIndex = 1
        Me.CheckBox77.Text = "80"
        Me.CheckBox77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox77.UseVisualStyleBackColor = True
        '
        'CheckBox67
        '
        Me.CheckBox67.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox67.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox67.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox67.Location = New System.Drawing.Point(654, 394)
        Me.CheckBox67.Name = "CheckBox67"
        Me.CheckBox67.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox67.TabIndex = 1
        Me.CheckBox67.Text = "70"
        Me.CheckBox67.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox67.UseVisualStyleBackColor = True
        '
        'CheckBox57
        '
        Me.CheckBox57.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox57.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox57.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox57.Location = New System.Drawing.Point(654, 333)
        Me.CheckBox57.Name = "CheckBox57"
        Me.CheckBox57.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox57.TabIndex = 1
        Me.CheckBox57.Text = "60"
        Me.CheckBox57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox57.UseVisualStyleBackColor = True
        '
        'CheckBox47
        '
        Me.CheckBox47.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox47.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox47.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox47.Location = New System.Drawing.Point(654, 272)
        Me.CheckBox47.Name = "CheckBox47"
        Me.CheckBox47.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox47.TabIndex = 1
        Me.CheckBox47.Text = "50"
        Me.CheckBox47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox47.UseVisualStyleBackColor = True
        '
        'CheckBox37
        '
        Me.CheckBox37.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox37.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox37.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox37.Location = New System.Drawing.Point(654, 211)
        Me.CheckBox37.Name = "CheckBox37"
        Me.CheckBox37.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox37.TabIndex = 1
        Me.CheckBox37.Text = "40"
        Me.CheckBox37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox37.UseVisualStyleBackColor = True
        '
        'CheckBox27
        '
        Me.CheckBox27.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox27.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox27.Location = New System.Drawing.Point(654, 150)
        Me.CheckBox27.Name = "CheckBox27"
        Me.CheckBox27.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox27.TabIndex = 1
        Me.CheckBox27.Text = "30"
        Me.CheckBox27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox27.UseVisualStyleBackColor = True
        '
        'CheckBox17
        '
        Me.CheckBox17.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox17.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox17.Location = New System.Drawing.Point(654, 89)
        Me.CheckBox17.Name = "CheckBox17"
        Me.CheckBox17.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox17.TabIndex = 1
        Me.CheckBox17.Text = "20"
        Me.CheckBox17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox17.UseVisualStyleBackColor = True
        '
        'CheckBox86
        '
        Me.CheckBox86.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox86.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox86.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox86.Location = New System.Drawing.Point(438, 516)
        Me.CheckBox86.Name = "CheckBox86"
        Me.CheckBox86.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox86.TabIndex = 1
        Me.CheckBox86.Text = "87"
        Me.CheckBox86.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox86.UseVisualStyleBackColor = True
        '
        'CheckBox76
        '
        Me.CheckBox76.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox76.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox76.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox76.Location = New System.Drawing.Point(438, 455)
        Me.CheckBox76.Name = "CheckBox76"
        Me.CheckBox76.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox76.TabIndex = 1
        Me.CheckBox76.Text = "77"
        Me.CheckBox76.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox76.UseVisualStyleBackColor = True
        '
        'CheckBox66
        '
        Me.CheckBox66.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox66.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox66.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox66.Location = New System.Drawing.Point(438, 394)
        Me.CheckBox66.Name = "CheckBox66"
        Me.CheckBox66.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox66.TabIndex = 1
        Me.CheckBox66.Text = "67"
        Me.CheckBox66.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox66.UseVisualStyleBackColor = True
        '
        'CheckBox56
        '
        Me.CheckBox56.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox56.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox56.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox56.Location = New System.Drawing.Point(438, 333)
        Me.CheckBox56.Name = "CheckBox56"
        Me.CheckBox56.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox56.TabIndex = 1
        Me.CheckBox56.Text = "57"
        Me.CheckBox56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox56.UseVisualStyleBackColor = True
        '
        'CheckBox46
        '
        Me.CheckBox46.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox46.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox46.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox46.Location = New System.Drawing.Point(438, 272)
        Me.CheckBox46.Name = "CheckBox46"
        Me.CheckBox46.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox46.TabIndex = 1
        Me.CheckBox46.Text = "47"
        Me.CheckBox46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox46.UseVisualStyleBackColor = True
        '
        'CheckBox36
        '
        Me.CheckBox36.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox36.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox36.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox36.Location = New System.Drawing.Point(438, 211)
        Me.CheckBox36.Name = "CheckBox36"
        Me.CheckBox36.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox36.TabIndex = 1
        Me.CheckBox36.Text = "37"
        Me.CheckBox36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox36.UseVisualStyleBackColor = True
        '
        'CheckBox26
        '
        Me.CheckBox26.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox26.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox26.Location = New System.Drawing.Point(438, 150)
        Me.CheckBox26.Name = "CheckBox26"
        Me.CheckBox26.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox26.TabIndex = 1
        Me.CheckBox26.Text = "27"
        Me.CheckBox26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox26.UseVisualStyleBackColor = True
        '
        'CheckBox16
        '
        Me.CheckBox16.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox16.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox16.Location = New System.Drawing.Point(438, 89)
        Me.CheckBox16.Name = "CheckBox16"
        Me.CheckBox16.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox16.TabIndex = 1
        Me.CheckBox16.Text = "17"
        Me.CheckBox16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox16.UseVisualStyleBackColor = True
        '
        'CheckBox10
        '
        Me.CheckBox10.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox10.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox10.Location = New System.Drawing.Point(654, 28)
        Me.CheckBox10.Name = "CheckBox10"
        Me.CheckBox10.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox10.TabIndex = 1
        Me.CheckBox10.Text = "10"
        Me.CheckBox10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox10.UseVisualStyleBackColor = True
        '
        'CheckBox85
        '
        Me.CheckBox85.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox85.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox85.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox85.Location = New System.Drawing.Point(366, 516)
        Me.CheckBox85.Name = "CheckBox85"
        Me.CheckBox85.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox85.TabIndex = 1
        Me.CheckBox85.Text = "86"
        Me.CheckBox85.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox85.UseVisualStyleBackColor = True
        '
        'CheckBox75
        '
        Me.CheckBox75.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox75.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox75.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox75.Location = New System.Drawing.Point(366, 455)
        Me.CheckBox75.Name = "CheckBox75"
        Me.CheckBox75.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox75.TabIndex = 1
        Me.CheckBox75.Text = "76"
        Me.CheckBox75.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox75.UseVisualStyleBackColor = True
        '
        'CheckBox65
        '
        Me.CheckBox65.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox65.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox65.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox65.Location = New System.Drawing.Point(366, 394)
        Me.CheckBox65.Name = "CheckBox65"
        Me.CheckBox65.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox65.TabIndex = 1
        Me.CheckBox65.Text = "66"
        Me.CheckBox65.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox65.UseVisualStyleBackColor = True
        '
        'CheckBox55
        '
        Me.CheckBox55.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox55.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox55.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox55.Location = New System.Drawing.Point(366, 333)
        Me.CheckBox55.Name = "CheckBox55"
        Me.CheckBox55.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox55.TabIndex = 1
        Me.CheckBox55.Text = "56"
        Me.CheckBox55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox55.UseVisualStyleBackColor = True
        '
        'CheckBox45
        '
        Me.CheckBox45.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox45.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox45.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox45.Location = New System.Drawing.Point(366, 272)
        Me.CheckBox45.Name = "CheckBox45"
        Me.CheckBox45.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox45.TabIndex = 1
        Me.CheckBox45.Text = "46"
        Me.CheckBox45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox45.UseVisualStyleBackColor = True
        '
        'CheckBox35
        '
        Me.CheckBox35.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox35.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox35.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox35.Location = New System.Drawing.Point(366, 211)
        Me.CheckBox35.Name = "CheckBox35"
        Me.CheckBox35.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox35.TabIndex = 1
        Me.CheckBox35.Text = "36"
        Me.CheckBox35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox35.UseVisualStyleBackColor = True
        '
        'CheckBox25
        '
        Me.CheckBox25.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox25.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox25.Location = New System.Drawing.Point(366, 150)
        Me.CheckBox25.Name = "CheckBox25"
        Me.CheckBox25.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox25.TabIndex = 1
        Me.CheckBox25.Text = "26"
        Me.CheckBox25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox25.UseVisualStyleBackColor = True
        '
        'CheckBox15
        '
        Me.CheckBox15.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox15.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox15.Location = New System.Drawing.Point(366, 89)
        Me.CheckBox15.Name = "CheckBox15"
        Me.CheckBox15.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox15.TabIndex = 1
        Me.CheckBox15.Text = "16"
        Me.CheckBox15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox15.UseVisualStyleBackColor = True
        '
        'CheckBox7
        '
        Me.CheckBox7.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox7.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox7.Location = New System.Drawing.Point(438, 28)
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox7.TabIndex = 1
        Me.CheckBox7.Text = "7"
        Me.CheckBox7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox7.UseVisualStyleBackColor = True
        '
        'CheckBox84
        '
        Me.CheckBox84.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox84.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox84.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox84.Location = New System.Drawing.Point(294, 516)
        Me.CheckBox84.Name = "CheckBox84"
        Me.CheckBox84.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox84.TabIndex = 1
        Me.CheckBox84.Text = "85"
        Me.CheckBox84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox84.UseVisualStyleBackColor = True
        '
        'CheckBox74
        '
        Me.CheckBox74.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox74.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox74.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox74.Location = New System.Drawing.Point(294, 455)
        Me.CheckBox74.Name = "CheckBox74"
        Me.CheckBox74.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox74.TabIndex = 1
        Me.CheckBox74.Text = "75"
        Me.CheckBox74.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox74.UseVisualStyleBackColor = True
        '
        'CheckBox64
        '
        Me.CheckBox64.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox64.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox64.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox64.Location = New System.Drawing.Point(294, 394)
        Me.CheckBox64.Name = "CheckBox64"
        Me.CheckBox64.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox64.TabIndex = 1
        Me.CheckBox64.Text = "65"
        Me.CheckBox64.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox64.UseVisualStyleBackColor = True
        '
        'CheckBox54
        '
        Me.CheckBox54.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox54.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox54.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox54.Location = New System.Drawing.Point(294, 333)
        Me.CheckBox54.Name = "CheckBox54"
        Me.CheckBox54.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox54.TabIndex = 1
        Me.CheckBox54.Text = "55"
        Me.CheckBox54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox54.UseVisualStyleBackColor = True
        '
        'CheckBox44
        '
        Me.CheckBox44.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox44.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox44.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox44.Location = New System.Drawing.Point(294, 272)
        Me.CheckBox44.Name = "CheckBox44"
        Me.CheckBox44.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox44.TabIndex = 1
        Me.CheckBox44.Text = "45"
        Me.CheckBox44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox44.UseVisualStyleBackColor = True
        '
        'CheckBox34
        '
        Me.CheckBox34.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox34.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox34.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox34.Location = New System.Drawing.Point(294, 211)
        Me.CheckBox34.Name = "CheckBox34"
        Me.CheckBox34.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox34.TabIndex = 1
        Me.CheckBox34.Text = "35"
        Me.CheckBox34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox34.UseVisualStyleBackColor = True
        '
        'CheckBox24
        '
        Me.CheckBox24.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox24.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox24.Location = New System.Drawing.Point(294, 150)
        Me.CheckBox24.Name = "CheckBox24"
        Me.CheckBox24.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox24.TabIndex = 1
        Me.CheckBox24.Text = "25"
        Me.CheckBox24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox24.UseVisualStyleBackColor = True
        '
        'CheckBox14
        '
        Me.CheckBox14.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox14.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox14.Location = New System.Drawing.Point(294, 89)
        Me.CheckBox14.Name = "CheckBox14"
        Me.CheckBox14.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox14.TabIndex = 1
        Me.CheckBox14.Text = "15"
        Me.CheckBox14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox14.UseVisualStyleBackColor = True
        '
        'CheckBox6
        '
        Me.CheckBox6.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox6.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox6.Location = New System.Drawing.Point(367, 28)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox6.TabIndex = 1
        Me.CheckBox6.Text = "6"
        Me.CheckBox6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox6.UseVisualStyleBackColor = True
        '
        'CheckBox83
        '
        Me.CheckBox83.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox83.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox83.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox83.Location = New System.Drawing.Point(222, 516)
        Me.CheckBox83.Name = "CheckBox83"
        Me.CheckBox83.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox83.TabIndex = 1
        Me.CheckBox83.Text = "84"
        Me.CheckBox83.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox83.UseVisualStyleBackColor = True
        '
        'CheckBox73
        '
        Me.CheckBox73.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox73.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox73.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox73.Location = New System.Drawing.Point(222, 455)
        Me.CheckBox73.Name = "CheckBox73"
        Me.CheckBox73.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox73.TabIndex = 1
        Me.CheckBox73.Text = "74"
        Me.CheckBox73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox73.UseVisualStyleBackColor = True
        '
        'CheckBox63
        '
        Me.CheckBox63.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox63.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox63.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox63.Location = New System.Drawing.Point(222, 394)
        Me.CheckBox63.Name = "CheckBox63"
        Me.CheckBox63.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox63.TabIndex = 1
        Me.CheckBox63.Text = "64"
        Me.CheckBox63.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox63.UseVisualStyleBackColor = True
        '
        'CheckBox53
        '
        Me.CheckBox53.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox53.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox53.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox53.Location = New System.Drawing.Point(222, 333)
        Me.CheckBox53.Name = "CheckBox53"
        Me.CheckBox53.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox53.TabIndex = 1
        Me.CheckBox53.Text = "54"
        Me.CheckBox53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox53.UseVisualStyleBackColor = True
        '
        'CheckBox43
        '
        Me.CheckBox43.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox43.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox43.Location = New System.Drawing.Point(222, 272)
        Me.CheckBox43.Name = "CheckBox43"
        Me.CheckBox43.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox43.TabIndex = 1
        Me.CheckBox43.Text = "44"
        Me.CheckBox43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox43.UseVisualStyleBackColor = True
        '
        'CheckBox33
        '
        Me.CheckBox33.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox33.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox33.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox33.Location = New System.Drawing.Point(222, 211)
        Me.CheckBox33.Name = "CheckBox33"
        Me.CheckBox33.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox33.TabIndex = 1
        Me.CheckBox33.Text = "34"
        Me.CheckBox33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox33.UseVisualStyleBackColor = True
        '
        'CheckBox23
        '
        Me.CheckBox23.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox23.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox23.Location = New System.Drawing.Point(222, 150)
        Me.CheckBox23.Name = "CheckBox23"
        Me.CheckBox23.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox23.TabIndex = 1
        Me.CheckBox23.Text = "24"
        Me.CheckBox23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox23.UseVisualStyleBackColor = True
        '
        'CheckBox13
        '
        Me.CheckBox13.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox13.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox13.Location = New System.Drawing.Point(222, 89)
        Me.CheckBox13.Name = "CheckBox13"
        Me.CheckBox13.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox13.TabIndex = 1
        Me.CheckBox13.Text = "14"
        Me.CheckBox13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox13.UseVisualStyleBackColor = True
        '
        'CheckBox5
        '
        Me.CheckBox5.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox5.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox5.Location = New System.Drawing.Point(294, 28)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox5.TabIndex = 1
        Me.CheckBox5.Text = "5"
        Me.CheckBox5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'CheckBox82
        '
        Me.CheckBox82.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox82.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox82.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox82.Location = New System.Drawing.Point(78, 516)
        Me.CheckBox82.Name = "CheckBox82"
        Me.CheckBox82.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox82.TabIndex = 0
        Me.CheckBox82.Text = "82"
        Me.CheckBox82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox82.UseVisualStyleBackColor = True
        '
        'CheckBox72
        '
        Me.CheckBox72.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox72.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox72.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox72.Location = New System.Drawing.Point(78, 455)
        Me.CheckBox72.Name = "CheckBox72"
        Me.CheckBox72.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox72.TabIndex = 0
        Me.CheckBox72.Text = "72"
        Me.CheckBox72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox72.UseVisualStyleBackColor = True
        '
        'CheckBox62
        '
        Me.CheckBox62.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox62.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox62.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox62.Location = New System.Drawing.Point(78, 394)
        Me.CheckBox62.Name = "CheckBox62"
        Me.CheckBox62.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox62.TabIndex = 0
        Me.CheckBox62.Text = "62"
        Me.CheckBox62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox62.UseVisualStyleBackColor = True
        '
        'CheckBox52
        '
        Me.CheckBox52.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox52.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox52.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox52.Location = New System.Drawing.Point(78, 333)
        Me.CheckBox52.Name = "CheckBox52"
        Me.CheckBox52.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox52.TabIndex = 0
        Me.CheckBox52.Text = "52"
        Me.CheckBox52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox52.UseVisualStyleBackColor = True
        '
        'CheckBox42
        '
        Me.CheckBox42.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox42.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox42.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox42.Location = New System.Drawing.Point(78, 272)
        Me.CheckBox42.Name = "CheckBox42"
        Me.CheckBox42.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox42.TabIndex = 0
        Me.CheckBox42.Text = "42"
        Me.CheckBox42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox42.UseVisualStyleBackColor = True
        '
        'CheckBox32
        '
        Me.CheckBox32.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox32.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox32.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox32.Location = New System.Drawing.Point(78, 211)
        Me.CheckBox32.Name = "CheckBox32"
        Me.CheckBox32.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox32.TabIndex = 0
        Me.CheckBox32.Text = "32"
        Me.CheckBox32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox32.UseVisualStyleBackColor = True
        '
        'CheckBox22
        '
        Me.CheckBox22.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox22.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox22.Location = New System.Drawing.Point(78, 150)
        Me.CheckBox22.Name = "CheckBox22"
        Me.CheckBox22.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox22.TabIndex = 0
        Me.CheckBox22.Text = "22"
        Me.CheckBox22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox22.UseVisualStyleBackColor = True
        '
        'CheckBox12
        '
        Me.CheckBox12.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox12.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox12.Location = New System.Drawing.Point(78, 89)
        Me.CheckBox12.Name = "CheckBox12"
        Me.CheckBox12.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox12.TabIndex = 0
        Me.CheckBox12.Text = "12"
        Me.CheckBox12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox12.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox4.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox4.Location = New System.Drawing.Point(222, 28)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox4.TabIndex = 1
        Me.CheckBox4.Text = "4"
        Me.CheckBox4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'CheckBox81
        '
        Me.CheckBox81.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox81.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox81.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox81.Location = New System.Drawing.Point(6, 516)
        Me.CheckBox81.Name = "CheckBox81"
        Me.CheckBox81.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox81.TabIndex = 0
        Me.CheckBox81.Text = "81"
        Me.CheckBox81.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox81.UseVisualStyleBackColor = True
        '
        'CheckBox71
        '
        Me.CheckBox71.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox71.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox71.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox71.Location = New System.Drawing.Point(6, 455)
        Me.CheckBox71.Name = "CheckBox71"
        Me.CheckBox71.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox71.TabIndex = 0
        Me.CheckBox71.Text = "71"
        Me.CheckBox71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox71.UseVisualStyleBackColor = True
        '
        'CheckBox61
        '
        Me.CheckBox61.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox61.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox61.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox61.Location = New System.Drawing.Point(6, 394)
        Me.CheckBox61.Name = "CheckBox61"
        Me.CheckBox61.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox61.TabIndex = 0
        Me.CheckBox61.Text = "61"
        Me.CheckBox61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox61.UseVisualStyleBackColor = True
        '
        'CheckBox51
        '
        Me.CheckBox51.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox51.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox51.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox51.Location = New System.Drawing.Point(6, 333)
        Me.CheckBox51.Name = "CheckBox51"
        Me.CheckBox51.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox51.TabIndex = 0
        Me.CheckBox51.Text = "51"
        Me.CheckBox51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox51.UseVisualStyleBackColor = True
        '
        'CheckBox41
        '
        Me.CheckBox41.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox41.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox41.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox41.Location = New System.Drawing.Point(6, 272)
        Me.CheckBox41.Name = "CheckBox41"
        Me.CheckBox41.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox41.TabIndex = 0
        Me.CheckBox41.Text = "41"
        Me.CheckBox41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox41.UseVisualStyleBackColor = True
        '
        'CheckBox31
        '
        Me.CheckBox31.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox31.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox31.Location = New System.Drawing.Point(6, 211)
        Me.CheckBox31.Name = "CheckBox31"
        Me.CheckBox31.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox31.TabIndex = 0
        Me.CheckBox31.Text = "31"
        Me.CheckBox31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox31.UseVisualStyleBackColor = True
        '
        'CheckBox21
        '
        Me.CheckBox21.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox21.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox21.Location = New System.Drawing.Point(6, 150)
        Me.CheckBox21.Name = "CheckBox21"
        Me.CheckBox21.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox21.TabIndex = 0
        Me.CheckBox21.Text = "21"
        Me.CheckBox21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox21.UseVisualStyleBackColor = True
        '
        'CheckBox11
        '
        Me.CheckBox11.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox11.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox11.Location = New System.Drawing.Point(6, 89)
        Me.CheckBox11.Name = "CheckBox11"
        Me.CheckBox11.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox11.TabIndex = 0
        Me.CheckBox11.Text = "11"
        Me.CheckBox11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox11.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox2.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.Location = New System.Drawing.Point(78, 28)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox2.TabIndex = 0
        Me.CheckBox2.Text = "2"
        Me.CheckBox2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.Appearance = System.Windows.Forms.Appearance.Button
        Me.CheckBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CheckBox1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.Location = New System.Drawing.Point(6, 28)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(59, 55)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "1"
        Me.CheckBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.RadioButton3)
        Me.GroupBox3.Controls.Add(Me.RadioButton2)
        Me.GroupBox3.Controls.Add(Me.RadioButton1)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.White
        Me.GroupBox3.Location = New System.Drawing.Point(14, 17)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(384, 139)
        Me.GroupBox3.TabIndex = 11
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Mode de Jeu"
        '
        'RadioButton3
        '
        Me.RadioButton3.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton3.BackColor = System.Drawing.Color.Firebrick
        Me.RadioButton3.Enabled = False
        Me.RadioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton3.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton3.Location = New System.Drawing.Point(256, 49)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(115, 47)
        Me.RadioButton3.TabIndex = 3
        Me.RadioButton3.Text = "TRIPLET"
        Me.RadioButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton3.UseVisualStyleBackColor = False
        '
        'RadioButton2
        '
        Me.RadioButton2.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton2.BackColor = System.Drawing.Color.Firebrick
        Me.RadioButton2.Enabled = False
        Me.RadioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton2.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton2.Location = New System.Drawing.Point(135, 49)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(115, 47)
        Me.RadioButton2.TabIndex = 3
        Me.RadioButton2.Text = "DOUBLET"
        Me.RadioButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'RadioButton1
        '
        Me.RadioButton1.Appearance = System.Windows.Forms.Appearance.Button
        Me.RadioButton1.BackColor = System.Drawing.Color.Firebrick
        Me.RadioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.RadioButton1.Location = New System.Drawing.Point(14, 49)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(115, 47)
        Me.RadioButton1.TabIndex = 3
        Me.RadioButton1.Text = "SOLO"
        Me.RadioButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label8.Location = New System.Drawing.Point(121, 28)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(0, 24)
        Me.Label8.TabIndex = 2
        '
        'Dgd_Tick
        '
        Me.Dgd_Tick.AllowUserToAddRows = False
        Me.Dgd_Tick.AllowUserToDeleteRows = False
        Me.Dgd_Tick.AllowUserToResizeColumns = False
        Me.Dgd_Tick.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkSlateGray
        Me.Dgd_Tick.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.Dgd_Tick.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgd_Tick.Location = New System.Drawing.Point(14, 20)
        Me.Dgd_Tick.MultiSelect = False
        Me.Dgd_Tick.Name = "Dgd_Tick"
        Me.Dgd_Tick.ReadOnly = True
        Me.Dgd_Tick.RowHeadersWidth = 20
        Me.Dgd_Tick.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Dgd_Tick.Size = New System.Drawing.Size(305, 113)
        Me.Dgd_Tick.TabIndex = 13
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Dgd_Tick)
        Me.GroupBox4.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(14, 684)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(449, 35)
        Me.GroupBox4.TabIndex = 14
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Combinaison"
        Me.GroupBox4.Visible = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkGreen
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(14, 643)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(121, 35)
        Me.Button2.TabIndex = 12
        Me.Button2.Text = "Ajouter"
        Me.Button2.UseVisualStyleBackColor = False
        Me.Button2.Visible = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Lb_Min)
        Me.SplitContainer1.Panel2.Controls.Add(Me.LB_second)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupBox6)
        Me.SplitContainer1.Panel2.Controls.Add(Me.PbVoyant)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupBox3)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupBox4)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button4)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.bt_print)
        Me.SplitContainer1.Panel2.Controls.Add(Me.BtCancel)
        Me.SplitContainer1.Size = New System.Drawing.Size(1287, 733)
        Me.SplitContainer1.SplitterDistance = 778
        Me.SplitContainer1.TabIndex = 15
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Firebrick
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.LbCaisseEntrance)
        Me.Panel1.Location = New System.Drawing.Point(666, 634)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(81, 96)
        Me.Panel1.TabIndex = 14
        Me.Panel1.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(81, 2)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 36)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "CASH"
        '
        'LbCaisseEntrance
        '
        Me.LbCaisseEntrance.Font = New System.Drawing.Font("Century Gothic", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbCaisseEntrance.ForeColor = System.Drawing.Color.Transparent
        Me.LbCaisseEntrance.Location = New System.Drawing.Point(3, 47)
        Me.LbCaisseEntrance.Name = "LbCaisseEntrance"
        Me.LbCaisseEntrance.Size = New System.Drawing.Size(263, 32)
        Me.LbCaisseEntrance.TabIndex = 0
        Me.LbCaisseEntrance.Text = "100000 FCFA"
        Me.LbCaisseEntrance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.PbTickGagnant)
        Me.GroupBox5.Controls.Add(Me.Button3)
        Me.GroupBox5.Controls.Add(Me.TbIdticket)
        Me.GroupBox5.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.White
        Me.GroupBox5.Location = New System.Drawing.Point(12, 619)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(569, 111)
        Me.GroupBox5.TabIndex = 13
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Ticket"
        '
        'PbTickGagnant
        '
        Me.PbTickGagnant.Image = Global.PL_Lotto.My.Resources.Resources.button_cancel1
        Me.PbTickGagnant.Location = New System.Drawing.Point(487, 32)
        Me.PbTickGagnant.Name = "PbTickGagnant"
        Me.PbTickGagnant.Size = New System.Drawing.Size(64, 64)
        Me.PbTickGagnant.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PbTickGagnant.TabIndex = 4
        Me.PbTickGagnant.TabStop = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkGreen
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Image = Global.PL_Lotto.My.Resources.Resources.search
        Me.Button3.Location = New System.Drawing.Point(438, 45)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(43, 33)
        Me.Button3.TabIndex = 3
        Me.Button3.UseVisualStyleBackColor = False
        '
        'TbIdticket
        '
        Me.TbIdticket.ForeColor = System.Drawing.Color.Black
        Me.TbIdticket.Location = New System.Drawing.Point(39, 45)
        Me.TbIdticket.Name = "TbIdticket"
        Me.TbIdticket.Size = New System.Drawing.Size(386, 33)
        Me.TbIdticket.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Showcard Gothic", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(434, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 40)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = ":"
        '
        'Lb_Min
        '
        Me.Lb_Min.AutoSize = True
        Me.Lb_Min.Font = New System.Drawing.Font("Showcard Gothic", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lb_Min.ForeColor = System.Drawing.Color.White
        Me.Lb_Min.Location = New System.Drawing.Point(403, 8)
        Me.Lb_Min.Name = "Lb_Min"
        Me.Lb_Min.Size = New System.Drawing.Size(32, 33)
        Me.Lb_Min.TabIndex = 27
        Me.Lb_Min.Text = "0"
        Me.Lb_Min.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LB_second
        '
        Me.LB_second.AutoSize = True
        Me.LB_second.Font = New System.Drawing.Font("Showcard Gothic", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_second.ForeColor = System.Drawing.Color.White
        Me.LB_second.Location = New System.Drawing.Point(458, 8)
        Me.LB_second.Name = "LB_second"
        Me.LB_second.Size = New System.Drawing.Size(32, 33)
        Me.LB_second.TabIndex = 26
        Me.LB_second.Text = "0"
        Me.LB_second.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Dgv_LastTick)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox6.ForeColor = System.Drawing.Color.White
        Me.GroupBox6.Location = New System.Drawing.Point(14, 427)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(466, 206)
        Me.GroupBox6.TabIndex = 16
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Historique"
        '
        'Dgv_LastTick
        '
        Me.Dgv_LastTick.AllowUserToAddRows = False
        Me.Dgv_LastTick.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        Me.Dgv_LastTick.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.Dgv_LastTick.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgv_LastTick.Location = New System.Drawing.Point(14, 33)
        Me.Dgv_LastTick.Name = "Dgv_LastTick"
        Me.Dgv_LastTick.ReadOnly = True
        Me.Dgv_LastTick.Size = New System.Drawing.Size(434, 162)
        Me.Dgv_LastTick.TabIndex = 0
        '
        'PbVoyant
        '
        Me.PbVoyant.Image = Global.PL_Lotto.My.Resources.Resources.Vvert
        Me.PbVoyant.Location = New System.Drawing.Point(414, 56)
        Me.PbVoyant.Name = "PbVoyant"
        Me.PbVoyant.Size = New System.Drawing.Size(66, 100)
        Me.PbVoyant.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PbVoyant.TabIndex = 15
        Me.PbVoyant.TabStop = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkGreen
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(14, 253)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(70, 47)
        Me.Button4.TabIndex = 12
        Me.Button4.Text = ">>"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'FrmTicket
        '
        Me.AcceptButton = Me.Button3
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkSlateGray
        Me.ClientSize = New System.Drawing.Size(1287, 733)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "FrmTicket"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edition de Ticket"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.Dgd_Tick, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.PbTickGagnant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.Dgv_LastTick, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbVoyant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bt_print As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtCancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox90 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox80 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox70 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox60 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox50 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox40 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox30 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox20 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox8 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox89 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox79 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox69 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox59 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox49 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox39 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox29 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox19 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox9 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox88 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox78 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox68 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox58 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox48 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox38 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox28 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox18 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox87 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox77 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox67 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox57 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox47 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox37 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox27 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox17 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox86 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox76 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox66 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox56 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox46 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox36 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox26 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox16 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox10 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox85 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox75 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox65 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox55 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox45 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox35 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox25 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox15 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox7 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox84 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox74 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox64 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox54 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox44 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox34 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox24 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox14 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox83 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox73 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox63 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox53 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox43 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox33 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox23 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox13 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox82 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox72 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox62 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox52 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox42 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox32 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox22 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox12 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox81 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox71 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox61 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox51 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox41 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox31 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox21 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox11 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Dgd_Tick As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents RadioButton12 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton7 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton11 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton9 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton10 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton8 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton5 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TbIdticket As System.Windows.Forms.TextBox
    Friend WithEvents PbTickGagnant As System.Windows.Forms.PictureBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents PbVoyant As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Dgv_LastTick As System.Windows.Forms.DataGridView
    Friend WithEvents LbCaisseEntrance As System.Windows.Forms.Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Lb_Min As Label
    Friend WithEvents LB_second As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents LB_NumTirage As Label
End Class
