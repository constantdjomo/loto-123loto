﻿Imports System.ComponentModel
Imports System.Text
Imports System.Drawing.Drawing2D
Imports Spire.Barcode
Imports Zen.Barcode

Imports System.Drawing.Printing
Imports System.Management
Imports System.IO
Imports System.Globalization

Public Class FrmComptescli
    Public settings As BarcodeSettings
    Private oCompte As BL_Lotto.BlCompte
    Private oTicket As BL_Lotto.BlTicket
    Private oUser As BL_Lotto.BlUser

    Private PRINTER_NAME As String = My.Settings.PRINTER_NAME

    Private WithEvents couponprint As PrintDocument
    Dim printdialog As PrintDialog
    Private WithEvents prdocument As PrintDocument


    Dim Tabdatedebut(), Tabdatefin() As String
    Dim datedebut, datefin As String
    Dim delim As Char = CChar("/")
    Dim dsTicket, dsTicketJack, dsTicketAnnul, dsTickJackMon, dsUsers, dsTouticket, dsPari, ODsPari, dsTirage, DsGain As DataSet

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        If CInt(LbNbTirage.Text) <> 0 Then
            If MsgBox("voulez vous editez un coupon?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                edit_coupon()
            End If
        Else
            MsgBox("Aucun Tirage n'a ete realiser pour le moment?", MsgBoxStyle.OkOnly, "Information")
        End If

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint, Panel7.Paint, Panel6.Paint, Panel5.Paint, Panel4.Paint, Panel3.Paint, Panel2.Paint

    End Sub

    Dim id_ticket As String

    Private Sub FrmComptes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PRINTER_NAME = DefaultPrinterName()
        My.Settings.PRINTER_NAME = GetDefaultPrinterName()

        Using oUser As New BL_Lotto.BlUser(source)
            'CONNECTED_USERID = "1"
            dsUsers = oUser.UserParNom(CONNECTED_USERID)

            If (dsUsers.Tables("users").Rows.Count > 0) Then
                TbNomUser.Text = dsUsers.Tables("users").Rows(0).Item("NOM").ToString & " " & dsUsers.Tables("users").Rows(0).Item("PRENOM").ToString
                Label9.Text = Date.Now.ToLongDateString
            End If

        End Using

    End Sub
    Public Shared Function DefaultPrinterName() As String
        Dim oPS As New System.Drawing.Printing.PrinterSettings

        Try
            DefaultPrinterName = oPS.PrinterName
        Catch ex As System.Exception
            DefaultPrinterName = ""
        Finally
            oPS = Nothing
        End Try
    End Function
    Function GetDefaultPrinterName() As String
        'Dim searcher As ManagementObjectSearcher
        'Dim query As ObjectQuery
        'query = New ObjectQuery("SELECT * FROM Win32_Printer")
        'searcher = New ManagementObjectSearcher(query)

        'For Each mo As ManagementObject In searcher.Get()
        '    Return mo.Properties("Name").ToString
        'Next

        Return vbNullChar
    End Function

    Private Sub BtOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtOk.Click
        Dim Gagner As Boolean = False
        Dim Gains As Integer = 0
        Dim TotalmontantJack As Integer = 0
        Dim totaltickannuller As Integer = 0
        If DateTime.Compare(DpDebut.Value, DpFin.Value) <= 0 Then
            Tabdatedebut = DpDebut.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
            datedebut = Tabdatedebut(2) & "/" & Tabdatedebut(1) & "/" & Tabdatedebut(0)
            'datedebut = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
            Tabdatefin = DpFin.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
            datefin = Tabdatefin(2) & "/" & Tabdatefin(1) & "/" & Tabdatefin(0)
            'datefin = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
            Using oCompte As New BL_Lotto.BlCompte(source)

                dsTicketJack = oCompte.getStatTicketJackParUser(CONNECTED_USERID, datedebut & " 00:00:00", datefin & " 23:59:59")
                dsTickJackMon = oCompte.getStatTickJackMonParUser(CONNECTED_USERID, datedebut & " 00:00:00", datefin & " 23:59:59")

                dsTicket = oCompte.getStatTicketParUser(CONNECTED_USERID, datedebut & " 00:00:00", datefin & " 23:59:59")

                dsTicketAnnul = oCompte.getStatTickAnnulParUser(CONNECTED_USERID, datedebut & " 00:00:00", datefin & " 23:59:59")

                dsPari = oCompte.getStatPariParUser(CONNECTED_USERID, datedebut & " 00:00:00", datefin & " 23:59:59")

                If (dsTicketJack.Tables("ticket").Rows.Count > 0) Then
                    LbNbtTicketJack.Text = dsTicketJack.Tables("ticket").Rows(0).Item("NB_TICKET").ToString
                End If
                If (dsTickJackMon.Tables("ticket").Rows.Count > 0) Then
                    If (Not dsTickJackMon.Tables("ticket").Rows(0).IsNull("TOTAL_MONTANT")) Then
                        TotalmontantJack = CInt(dsTickJackMon.Tables("ticket").Rows(0).Item("TOTAL_MONTANT"))
                    Else
                        TotalmontantJack = 0
                    End If

                End If

                If (dsTicketAnnul.Tables("ticket").Rows.Count > 0) Then
                    If (Not dsTicketAnnul.Tables("ticket").Rows(0).IsNull("NB_TICKET")) Then
                        totaltickannuller = CInt(dsTicketAnnul.Tables("ticket").Rows(0).Item("NB_TICKET"))
                    Else
                        totaltickannuller = 0
                    End If
                End If

                If (dsTicket.Tables("ticket").Rows.Count > 0) Then
                    LbNbTicket.Text = dsTicket.Tables("ticket").Rows(0).Item("NB_TICKET").ToString
                    LbTotalMise.Text = dsPari.Tables("pari").Rows(0).Item("TOTALMISE").ToString

                    'calcul du nombre de ticket gagnant
                    dsTouticket = oCompte.getListTicketServi(CONNECTED_USERID, datedebut & " 00:00:00", datefin & " 23:59:59")
                    Dim MontantSortie, nbticketGagnant As Integer
                    nbticketGagnant = 0
                    MontantSortie = 0
                    Using oTicket As New BL_Lotto.BlTicket(source)
                        For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
                            id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                            Dim oDsNumJou As DataSet

                            ODsPari = oTicket.GetPari(id_ticket)
                            Gagner = False
                            Gains = 0
                            'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                            If (ODsPari.Tables("pari").Rows.Count > 0) Then

                                For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                                    'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                                    If (oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                                    dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)) Then
                                        Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                                    End If


                                    Gagner = Gagner Or oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                                dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                                Next

                            End If

                            oDsNumJou = oTicket.getNumJouer(id_ticket)
                            If (Gagner) Then
                                nbticketGagnant += 1
                                MontantSortie += CInt(Gains)
                            End If


                        Next
                    End Using
                    MontantSortie += TotalmontantJack

                    LbNbtTicketGagna.Text = nbticketGagnant.ToString
                    LbTotalSortie.Text = MontantSortie.ToString
                    Lb_Nb_annule.Text = totaltickannuller.ToString

                    If LbTotalMise.Text = "" Then
                        LbTotalMise.Text = 0.ToString
                    End If
                    If LbTotalSortie.Text = "" Then
                        LbTotalSortie.Text = 0.ToString
                    End If

                    Dim benef As Integer = CInt(LbTotalMise.Text) - CInt(LbTotalSortie.Text)
                    benef = CInt(LbTotalMise.Text) - CInt(LbTotalSortie.Text)
                    LbBenefice.Text = benef.ToString
                Else
                    LbNbTicket.Text = "0"
                    LbTotalMise.Text = "0"
                End If


                dsTirage = oCompte.getStatTirage(datedebut & " 00:00:00", datefin & " 23:59:59")
                If (dsTirage.Tables("tirage").Rows.Count > 0) Then
                    LbNbTirage.Text = dsTirage.Tables("tirage").Rows(0).Item("NB_TIRAGE").ToString
                Else
                    LbNbTirage.Text = "0"
                End If



            End Using

        Else
            MessageBox.Show("Attention a la plage de Date.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Close()
    End Sub

    Public Sub edit_coupon()

        Using oTicket As New BL_Lotto.BlTicket(source)


            printdialog = New PrintDialog
                couponprint = New PrintDocument
                printdialog.Document = couponprint

                If PRINTER_NAME <> vbNullChar Then
                    'MsgBox(PRINTER_NAME)
                    couponprint.PrinterSettings.PrinterName = PRINTER_NAME
                End If


            'AnTickPrint.Print()


            If couponprint.PrinterSettings.IsValid Then
                couponprint.DocumentName = "Elite Ticket"
                ' Start printing
                couponprint.Print()
                ' Wait until callback function will say that the task is done.
                ' When done, end the monitoring of printer status.
            Else
                MessageBox.Show("Printer is not available.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        End Using
    End Sub

    Private Sub Couponprint_Print(ByVal sender As System.Object, ByVal e As PrintPageEventArgs) Handles couponprint.PrintPage
        Dim graphic As Graphics

        graphic = e.Graphics

        Dim printFont As New Font("Courier New", 18, FontStyle.Regular, GraphicsUnit.Point) 'Substituted to FontA Font

        Dim fontheight As Double
        Dim x, y, offset As Integer

        fontheight = printFont.GetHeight

        offset = 40


        Dim codbar As String = ""
        Dim user As String = ""
        Dim somme As Integer
        somme = 0


        'setting pour le code bare

        settings = New BarcodeSettings()
        Dim data As String = "20140983-90"
        Dim type As String = "Code128"


        settings.Type = CType(System.Enum.Parse(GetType(BarCodeType), type), BarCodeType)
        settings.X = 0.51


        Dim barHeight As Short = 15
        settings.BarHeight = barHeight

        settings.ShowTextOnBottom = True

        ' fin setting code bare

        graphic.PageUnit = GraphicsUnit.Point

        ' Draw the bitmap
        x = 30
        y = 0

        ' 
        'graphic.DrawString("ELITE LOTO", printFont, New SolidBrush(Color.Red), x, y)
        graphic.DrawImage(Global.PL_Lotto.My.Resources.Resources.loto, x + 30, y, 100, 33)
        'e.Graphics.DrawImage(pbImage.Image, x, y, pbImage.Image.Width - 187, 50)

        ' Print the receipt text
        printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)

        x = 10
        y = 10 + offset

        Try


            Using oUser As New BL_Lotto.BlUser(source)
                dsUsers = oUser.UserParID(CONNECTED_USERID)
            End Using

            Using ODsTicket As New BL_Lotto.BlTicket(source)
                user = dsUsers.Tables("users").Rows(dsUsers.Tables("users").Rows.Count - 1).Item("NOM").ToString
            End Using

            graphic.DrawString("   EDITION DU COUPON", printFont, Brushes.Black, x, y)

            printFont = New Font("Courier New", 10, FontStyle.Regular, GraphicsUnit.Point)
            fontheight = printFont.GetHeight()
            offset = CInt(fontheight)
            y += offset

            graphic.DrawString("=============================", printFont, Brushes.Black, x, y)

            y += offset
            graphic.DrawString("|          PERIODE          |", printFont, Brushes.Black, x, y)

            y += offset
            graphic.DrawString("=============================", printFont, Brushes.Black, x, y)

            y += offset
            graphic.DrawString("DU :" & DpDebut.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) & " AU :" & DpFin.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture), printFont, Brushes.Black, x, y)

            y += offset
            graphic.DrawString("Imp. le:" & Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) & " " & Date.Now.ToLongTimeString, printFont, Brushes.Black, x, y)

            y += offset
            graphic.DrawString("CAISSE :".PadRight(20) & user, printFont, Brushes.Black, x, y)
            offset = CInt(printFont.GetHeight(e.Graphics))
            y = CInt(y + (offset * 1.7))

            'on affiche le tirage


            graphic.DrawString("TICK ANNULE :".PadRight(18) & Lb_Nb_annule.Text, printFont, Brushes.Black, x, y)
            y += offset

            graphic.DrawString("ENTREE :".PadRight(20) & LbTotalMise.Text, printFont, Brushes.Black, x, y)
            y += offset

            graphic.DrawString("SORTIE :".PadRight(20) & LbTotalSortie.Text, printFont, Brushes.Black, x, y)
            y += offset

            graphic.DrawString("RESTE :".PadRight(20) & LbBenefice.Text, printFont, Brushes.Black, x, y)
            y += offset

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        printFont = New Font("Comic Sans MS", 12, FontStyle.Bold, GraphicsUnit.Point)
        offset = CInt(printFont.GetHeight(e.Graphics) - 4)
        y = CInt(y + (offset * 1.8))
        'e.Graphics.DrawString("oui!! c'est vrai!", printFont, Brushes.Red, 61, y)
        'y += lineOffset
        settings.Data2D = codbar
        settings.Data = codbar

        e.HasMorePages = False
    End Sub

    Private Sub FrmComptescli_ContextMenuStripChanged(sender As Object, e As EventArgs) Handles Me.ContextMenuStripChanged
        FrmPrincipal.Show()
    End Sub

    Private Sub FrmComptescli_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        FrmPrincipal.Show()
    End Sub
End Class