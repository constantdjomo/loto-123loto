﻿Imports MySql
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports MySql.Data.Types

Imports System.Net.Sockets
Imports System.Net
Imports System.Threading

Module settings
    Public ListeClients As List(Of Client) 'Liste destinée à contenir les clients connectés

    Public TirageLancer As Integer = 0
    Public timetosend As String = "0"
    Public animaDemarer As Boolean = False
    
    Dim Oactivation As BL_Lotto.BlActivation
    Public CONNECTED_USERID As String = ""
    Public SERVEUR As String = "127.0.0.1"
    Public BD As String = "bd_lot"
    Public ID As String = "root"
    Public PWD As String = "jlprom2at@"
    Public source As String = "server=" & SERVEUR & ";user id=" & ID & ";Password=" & PWD & ";persist security info=True;database=" & BD
#Region "definition des settings"

    Public DIFICULT_LEVEL, DELAI_AV_GAINS, GOLD_POT, SILVER_POT, IRON_POT, ID_GOLD, ID_SILVER, ID_IRON, MIN_GAIIN_SILVER, MAX_GAIIN_SILVER As Integer
    Public PORT, SCREEN_SELECT As String

    Public Resultat(4) As Integer

    Public Sub initialiseSetting()
        Dim oDataSet As DataSet

        Using oUser As New BL_Lotto.BlUser(source)
            oDataSet = oUser.GetSetting

            DIFICULT_LEVEL = oDataSet.Tables("setting").Rows(0).Item("DIFICULT_LEVEL").ToString

            DELAI_AV_GAINS = oDataSet.Tables("setting").Rows(0).Item("DELAI_AV_GAINS").ToString

            GOLD_POT = oDataSet.Tables("setting").Rows(0).Item("GOLD_POT").ToString

            SILVER_POT = oDataSet.Tables("setting").Rows(0).Item("SILVER_POT").ToString

            IRON_POT = oDataSet.Tables("setting").Rows(0).Item("IRON_POT").ToString

            ID_GOLD = oDataSet.Tables("setting").Rows(0).Item("ID_GOLD").ToString

            ID_SILVER = oDataSet.Tables("setting").Rows(0).Item("ID_SILVER").ToString

            ID_IRON = oDataSet.Tables("setting").Rows(0).Item("ID_IRON").ToString

            MIN_GAIIN_SILVER = oDataSet.Tables("setting").Rows(0).Item("MIN_GAIIN_SILVER").ToString

            MAX_GAIIN_SILVER = oDataSet.Tables("setting").Rows(0).Item("MAX_GAIIN_SILVER").ToString

            PORT = oDataSet.Tables("setting").Rows(0).Item("PORT").ToString

            SCREEN_SELECT = oDataSet.Tables("setting").Rows(0).Item("SCREEN_SELECT").ToString

        End Using
    End Sub

    Public Function PersistSetting() As Boolean

        Using oUser As New BL_Lotto.BlUser(source)
            Return oUser.SaveSetting(DIFICULT_LEVEL, DELAI_AV_GAINS, GOLD_POT, SILVER_POT, IRON_POT, ID_GOLD, ID_SILVER, ID_IRON, MIN_GAIIN_SILVER, MAX_GAIIN_SILVER, PORT, SCREEN_SELECT)
        End Using

    End Function
#End Region



    Sub Combo(ByVal ComboI As ComboBox, ByVal Req As String)
        Dim con As MySqlConnection = New MySqlConnection(source)
        Dim cmd As New MySqlCommand(Req, con)
        Dim dr As MySqlDataReader
        con.Open()
        dr = cmd.ExecuteReader
        While dr.Read
            ComboI.Items.Add(dr(0))
        End While
        dr.Close()
        con.Close()
    End Sub
    Public Function GetRandomDistinct(ByVal tailleVoulue As Integer, ByVal cpt As Integer) As Integer()
        Dim nombres As New List(Of Integer)()
        While nombres.Count <> tailleVoulue
            While nombres.Count <> tailleVoulue
                Dim rand As New Random()

                nombres.Add(rand.Next(0, cpt))
            End While
            nombres = nombres.Distinct().ToList()
        End While
        Return nombres.ToArray()
    End Function

    Public Sub select_another_lot(ByVal id_jack As String, Optional IsSilver As Boolean = False)

        Dim oDataSet As DataSet
        Dim q() As Integer
        Dim aqt As Integer
        Dim cpt As Integer

        Using ojacpot As New BL_Lotto.BlJackot(source)
            Try
                'todo a revoir cette partie
                oDataSet = ojacpot.GetListlot(id_jack, False)
                cpt = oDataSet.Tables("jackpot").Rows.Count
                aqt = 1
                q = GetRandomDistinct(aqt, cpt)

                If Not IsSilver Then

                    'MsgBox(" avant d'entrer" & ListWinTemp.Length)
                    If (oDataSet.Tables("jackpot").Rows.Count > 0) Then

                        ojacpot.Maj_mon_Jackpot(id_jack, oDataSet.Tables("jackpot").Rows(q(0)).Item("id_lot").ToString, oDataSet.Tables("jackpot").Rows(q(0)).Item("montant_jackp").ToString, CBool(oDataSet.Tables("jackpot").Rows(q(0)).Item("is_matirial")), oDataSet.Tables("jackpot").Rows(q(0)).Item("nom_materiel").ToString.Trim)

                    End If
                Else
                    'MsgBox(" avant d'entrer" & ListWinTemp.Length)
                    If (oDataSet.Tables("jackpot").Rows.Count > 0) Then

                        If (oDataSet.Tables("jackpot").Rows(q(0)).Item("id_lot").ToString <> "0") Then
                            ojacpot.Maj_mon_Jackpot(id_jack, oDataSet.Tables("jackpot").Rows(q(0)).Item("id_lot").ToString, oDataSet.Tables("jackpot").Rows(q(0)).Item("montant_jackp").ToString, CBool(oDataSet.Tables("jackpot").Rows(q(0)).Item("is_matirial")), oDataSet.Tables("jackpot").Rows(q(0)).Item("nom_materiel").ToString.Trim)
                        Else
                            Dim rand As New Random()
                            Dim nv_jack_sil As Integer

                            nv_jack_sil = rand.Next(MIN_GAIIN_SILVER, MAX_GAIIN_SILVER)
                            ojacpot.Maj_mon_Jackpot(id_jack, 0, nv_jack_sil, 0, "Cagnotte variable")
                            ojacpot.Maj_mon_Lot(0, nv_jack_sil, 0, "Cagnotte variable")

                        End If
                    End If
                End If


            Catch ex As Exception

            End Try
        End Using

    End Sub

    Sub main()

    End Sub


    Public Class Client
        Private _SocketClient As Socket 'Le socket du client
        Private _Pseudo As String 'Le pseudo du client

        'Constructeur
        Sub New(ByVal Sock As Socket)
            _SocketClient = Sock
        End Sub

        Public Sub TraitementClient()

            'Console.WriteLine("Thread client lancé. ")

            'Le client vient de se connecter

            'Broadcast(_Pseudo & " identifié sur le chat") 'Diffuse le message à tout le monde 
            While (_SocketClient.Connected)
                Try
                    Dim Message As String = TirageLancer.ToString & "/" & timetosend
                    'Recu = _SocketClient.Receive(Bytes)
                    Me.EnvoiMessage(Message)
                    
                    'Broadcast(_Pseudo & " dit : " & Message) 'Diffuse le message à tout le monde 
                Catch ex As Exception 'Le client est déconnecté
                    ListeClients.Remove(Me) 'Le supprime de la liste des clients connectés
                    _SocketClient.Close() 'Ferme son socket
                    'Broadcast(_Pseudo & " déconnecté.") 'Diffuse le message à tout le monde 
                    Return 'Fin de la fonction
                End Try

                System.Threading.Thread.Sleep(1000)
            End While

        End Sub

        Public Sub EnvoiMessage(ByVal Message As String)
            Dim Mess As Byte() = System.Text.Encoding.UTF8.GetBytes(Message)
            Dim Envoi As Integer = _SocketClient.Send(Mess)
            'Console.WriteLine(Envoi & " bytes envoyés au client " & _Pseudo)
        End Sub

    End Class


    Sub Broadcast(ByVal Message As String)

        'Écrit le message dans la console et l'envoie à tous les clients connectés
        Console.WriteLine("BROADCAST : " & Message)
        For Each Cli In ListeClients
            Cli.EnvoiMessage(Message)
        Next

    End Sub


End Module
