﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCUser
    Inherits System.Windows.Forms.UserControl

    'UserControl remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DgvUser = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QfqdfToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DqfqToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TBrech = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbRech = New System.Windows.Forms.ComboBox()
        Me.BTtous = New System.Windows.Forms.Button()
        Me.BTrech = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        CType(Me.DgvUser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(119, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(178, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Liste des Utilisateurs"
        '
        'DgvUser
        '
        Me.DgvUser.AllowUserToAddRows = False
        Me.DgvUser.AllowUserToDeleteRows = False
        Me.DgvUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvUser.ContextMenuStrip = Me.ContextMenuStrip1
        Me.DgvUser.Location = New System.Drawing.Point(38, 172)
        Me.DgvUser.Name = "DgvUser"
        Me.DgvUser.ReadOnly = True
        Me.DgvUser.Size = New System.Drawing.Size(528, 316)
        Me.DgvUser.TabIndex = 2
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AsToolStripMenuItem, Me.QfqdfToolStripMenuItem, Me.DqfqToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(153, 92)
        '
        'AsToolStripMenuItem
        '
        Me.AsToolStripMenuItem.Name = "AsToolStripMenuItem"
        Me.AsToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AsToolStripMenuItem.Text = "Ajouter"
        '
        'QfqdfToolStripMenuItem
        '
        Me.QfqdfToolStripMenuItem.Name = "QfqdfToolStripMenuItem"
        Me.QfqdfToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.QfqdfToolStripMenuItem.Text = "Supprimer"
        '
        'DqfqToolStripMenuItem
        '
        Me.DqfqToolStripMenuItem.Name = "DqfqToolStripMenuItem"
        Me.DqfqToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.DqfqToolStripMenuItem.Text = "Editer"
        '
        'TBrech
        '
        Me.TBrech.Location = New System.Drawing.Point(279, 31)
        Me.TBrech.Name = "TBrech"
        Me.TBrech.Size = New System.Drawing.Size(120, 20)
        Me.TBrech.TabIndex = 15
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Rechercher par"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(231, 33)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(42, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Mot-clé"
        '
        'cbRech
        '
        Me.cbRech.FormattingEnabled = True
        Me.cbRech.Location = New System.Drawing.Point(98, 30)
        Me.cbRech.Name = "cbRech"
        Me.cbRech.Size = New System.Drawing.Size(121, 21)
        Me.cbRech.TabIndex = 10
        '
        'BTtous
        '
        Me.BTtous.Location = New System.Drawing.Point(498, 28)
        Me.BTtous.Name = "BTtous"
        Me.BTtous.Size = New System.Drawing.Size(44, 23)
        Me.BTtous.TabIndex = 12
        Me.BTtous.Text = "Tous"
        Me.BTtous.UseVisualStyleBackColor = True
        '
        'BTrech
        '
        Me.BTrech.Location = New System.Drawing.Point(430, 28)
        Me.BTrech.Name = "BTrech"
        Me.BTrech.Size = New System.Drawing.Size(39, 23)
        Me.BTrech.TabIndex = 11
        Me.BTrech.Text = "OK"
        Me.BTrech.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Bisque
        Me.GroupBox1.Controls.Add(Me.TBrech)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cbRech)
        Me.GroupBox1.Controls.Add(Me.BTtous)
        Me.GroupBox1.Controls.Add(Me.BTrech)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 65)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(563, 68)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'UCUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.DgvUser)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UCUser"
        Me.Size = New System.Drawing.Size(624, 491)
        CType(Me.DgvUser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DgvUser As System.Windows.Forms.DataGridView
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents AsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QfqdfToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DqfqToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TBrech As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbRech As System.Windows.Forms.ComboBox
    Friend WithEvents BTtous As System.Windows.Forms.Button
    Friend WithEvents BTrech As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox

End Class
