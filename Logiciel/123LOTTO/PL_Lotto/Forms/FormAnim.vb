﻿Imports System.Net
Imports System.Threading
Imports System.Timers

Public Class FormAnim
    Private oTirage As BL_Lotto.BlTirage
    Private oTicketTi As BL_Lotto.BlTicket
    Private oBltriche As BL_Lotto.BlTriche
    Private Const Delai_JEu = 150 '300
    Private Const Delai_demarage_tir = 10 '10
    Private Const DureeAnim = 27
    Private Const MaxBoul = 90
    Private Const NbBoulTirag = 5
    Private Const Delai_anim_jackpot = 180000 ' 3min (1s = 1000 miliseconde )
    Private p As Point = New Point(136, 39)

    Private TabDelaiGagnant As Integer() = {Delai_JEu, Delai_JEu} ' 5 min; 10 min; 15 h;

    Private TotalEntrer, TotalEntrerSolo, TotalEntrerDoublet As ULong
    Private TotalBenef, TotalBenefSolo, TotalBenefDoublet As ULong
    Private prelSilverpot, prelSilverpotSolo, prelSilverpotDoub, prelevGoldpot, prelevGoldpotSolo, prelevGoldpotDoubl, prelevIrondpot, prelevIronpotSolo, prelevIronpotDoubl As ULong
    Dim GainsActuelClient, TotalmontantJack, GainsSolo, GainsDoublet As ULong



    Private Tir_cour_jack As Integer



    Dim ThreadClient, ThreadAnim, ThreadCl As Thread



    Dim MonThread As Thread

    Dim dsti As DataSet
    Dim d As DateTime = DateTime.Now


    Dim Tir_No As Integer = 0
    Dim time As Integer = Delai_JEu
    Dim DElaiGains As Integer = 0
    Dim tab1(5), tab2(5), tab3(5), tab4(5), tab5(5) As Integer
    Dim min, sec As Double


    Private Sub FormAnim_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        With Me

            Dim d1 As DateTime = DateTime.Now
            While DateTime.Now < d1.AddSeconds(DureeAnim)
                'on fait defiler les nombres
                While DateTime.Now < d1.AddSeconds(22)
                    While DateTime.Now < d1.AddSeconds(17)
                        While DateTime.Now < d1.AddSeconds(12)
                            While DateTime.Now < d1.AddSeconds(7)

                                Randomize()
                                .num1.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num1.Refresh()
                                Randomize()
                                .num2.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num2.Refresh()
                                Randomize()
                                .num3.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num3.Refresh()
                                Randomize()
                                .num4.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num4.Refresh()
                                Randomize()
                                .num5.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num5.Refresh()
                                Application.DoEvents()
                                Thread.Sleep(50)
                            End While
                            'on affiche le 1er nombre
                            .num1.Text = Resultat(0).ToString : .num1.Refresh()
                            'on fait defiler les 4 deniers
                            Randomize()
                            .num2.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num2.Refresh()
                            Randomize()
                            .num3.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num3.Refresh()
                            Randomize()
                            .num4.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num4.Refresh()
                            Randomize()
                            .num5.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num5.Refresh()
                            Application.DoEvents()
                            Thread.Sleep(50)

                        End While
                        'on affiche le 2eme nombres
                        .num2.Text = Resultat(1).ToString : .num2.Refresh()
                        'on fait defiler les 3 derniers
                        Randomize()
                        .num3.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num3.Refresh()
                        Randomize()
                        .num4.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num4.Refresh()
                        Randomize()
                        .num5.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num5.Refresh()
                        Application.DoEvents()
                        Thread.Sleep(50)
                    End While
                    'on affiche le 3eme nombres
                    .num3.Text = Resultat(2).ToString : .num3.Refresh()
                    'on fait defiler le 2 derniers
                    Randomize()
                    .num4.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num4.Refresh()
                    Randomize()
                    .num5.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num5.Refresh()
                    Application.DoEvents()
                    Thread.Sleep(50)
                End While
                'on affiche le 4eme nombres
                .num4.Text = Resultat(3).ToString : .num4.Refresh()
                'on fait defiler le dernier

                Randomize()
                .num5.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : .num5.Refresh()
                Application.DoEvents()
                Thread.Sleep(50)
            End While
            'on affiche le dernier nombre
            .num5.Text = Resultat(4).ToString : .num5.Refresh()

        End With

        Thread.Sleep(3000)
        Me.Hide()
    End Sub
End Class