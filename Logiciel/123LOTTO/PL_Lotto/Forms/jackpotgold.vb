﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports DirectShowLib

Public Class jackpotgold

    Enum PlayState
        Stopped
        Paused
        Running
        Init
    End Enum

    Enum MediaType
        Audio
        Video
    End Enum


    Private Const WMGraphNotify As Integer = 13
    Private Const VolumeFull As Integer = 0
    Private Const VolumeSilence As Integer = -10000

    Private graphBuilder As IGraphBuilder = Nothing
    Private mediaControl As IMediaControl = Nothing
    Private mediaEventEx As IMediaEventEx = Nothing
    Private videoWindow As IVideoWindow = Nothing
    Private basicAudio As IBasicAudio = Nothing
    Private basicVideo As IBasicVideo = Nothing
    Private mediaSeeking As IMediaSeeking = Nothing
    Private mediaPosition As IMediaPosition = Nothing
    Private frameStep As IVideoFrameStep = Nothing

    Public videofile As String = "vid1.avi"
    Private filename As String = String.Empty
    Private isAudioOnly As Boolean = False
    Private isFullScreen As Boolean = False
    Private currentVolume As Integer = VolumeFull
    Private currentState As PlayState = PlayState.Stopped
    Private currentPlaybackRate As Double = 1.0
    Private UseHand As IntPtr
    Private UseCtrl As System.Windows.Forms.Control
    Private FsDrain As IntPtr = IntPtr.Zero
    Private Event MedClose()
#If DEBUG Then
    Private rot As DsROTEntry = Nothing
#End If


    Private Sub jackpotgold_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OpenFile(videofile, VidScreenBox.Handle, Me)
    End Sub

    Private Sub OpenFile(ByVal fName As String, ByVal VidHand As IntPtr, ByVal VidCtrl As System.Windows.Forms.Control)
        ' Make sure everything is closed
        filename = ""
        CloseClip()

        'MaxToolStripMenuItem.Enabled = True
        'MinToolStripMenuItem.Enabled = True
        'CustomToolStripMenuItem.Enabled = True

        'NormalPlaybackRateToolStripMenuItem.Enabled = True
        'DecreasePlaybackRateToolStripMenuItem.Enabled = False
        'IncreasePlaybackRateToolStripMenuItem.Enabled = True
        'HalfSpeedToolStripMenuItem.Enabled = True
        'DoubleSpeedToolStripMenuItem.Enabled = True

        'DoubleSize200ToolStripMenuItem.Enabled = True
        'NormalSize100ToolStripMenuItem.Enabled = True
        'HalfSize50ToolStripMenuItem.Enabled = True
        'Size75ToolStripMenuItem.Enabled = True
        'FullScreenToolStripMenuItem.Enabled = True


        UseHand = VidHand 'Handle to Display Video if any
        UseCtrl = VidCtrl 'Control to Display Video if any

        filename = fName
        currentState = PlayState.Stopped 'Reset State to Stopped
        currentVolume = VolumeFull 'Reset Volume

        PlayMedia(fName) 'Call Main Sub
        Call SetFormTitle()
        'Timer1.Enabled = True
    End Sub

    Private Sub CloseClip()
        Try
            Dim hr As Integer = 0

            'Update Form Title
            Call SetFormTitle()

            'Reset all Properties to Default
            currentState = PlayState.Stopped
            isAudioOnly = True
            isFullScreen = False
            'MaxToolStripMenuItem.Enabled = False
            'MinToolStripMenuItem.Enabled = False
            'CustomToolStripMenuItem.Enabled = False

            'NormalPlaybackRateToolStripMenuItem.Enabled = False
            'DecreasePlaybackRateToolStripMenuItem.Enabled = False
            'IncreasePlaybackRateToolStripMenuItem.Enabled = False
            'HalfSpeedToolStripMenuItem.Enabled = False
            'DoubleSpeedToolStripMenuItem.Enabled = False

            'DoubleSize200ToolStripMenuItem.Enabled = False
            'NormalSize100ToolStripMenuItem.Enabled = False
            'HalfSize50ToolStripMenuItem.Enabled = False
            'Size75ToolStripMenuItem.Enabled = False
            'FullScreenToolStripMenuItem.Enabled = False
            filename = ""

            'Call sub to Close and Release from memory
            Call CloseInterfaces()

            'Reset more properties
            currentState = PlayState.Init

        Catch ex As Exception
            MsgBox("Errpr " & ex.Message, MsgBoxStyle.Critical, "Error")
            RaiseEvent MedClose()
        End Try
    End Sub

    Private Sub PlayMedia(ByVal fName As String)
        Dim hr As Integer = 0
        If fName = Nothing Then Exit Sub
        Try
            graphBuilder = DirectCast(New FilterGraph, IFilterGraph2) 'Load Graph Builder Device

            hr = graphBuilder.RenderFile(fName, Nothing) ' Initialize Graph Builder
            DsError.ThrowExceptionForHR(hr)

            'Load all Interfaces we will use
            mediaControl = DirectCast(graphBuilder, IMediaControl)
            mediaEventEx = DirectCast(graphBuilder, IMediaEventEx)
            mediaSeeking = DirectCast(graphBuilder, IMediaSeeking)
            mediaPosition = DirectCast(graphBuilder, IMediaPosition)
            videoWindow = DirectCast(graphBuilder, IVideoWindow)
            basicAudio = DirectCast(graphBuilder, IBasicVideo)
            basicVideo = DirectCast(graphBuilder, IBasicAudio)

            Call CheckType() 'Check to See if Audio or Video Call

            If isAudioOnly = False Then
                'Notfy Window of Video
                hr = mediaEventEx.SetNotifyWindow(UseHand, WMGraphNotify, IntPtr.Zero)
                DsError.ThrowExceptionForHR(hr)

                'Set Owner to Display Video
                hr = videoWindow.put_Owner(UseHand)
                DsError.ThrowExceptionForHR(hr)

                'Set Owner Video Style
                hr = videoWindow.put_WindowStyle(WindowStyle.Child And WindowStyle.ClipSiblings And WindowStyle.ClipChildren)
                DsError.ThrowExceptionForHR(hr)
            End If

            ''#If DEBUG Then
            ''            rot = New DsROTEntry(graphBuilder)
            ''#End If

            Me.Focus()
            'Update Form Title
            Call SetFormTitle()

            'Start Media
            hr = mediaControl.Run
            DsError.ThrowExceptionForHR(hr)

            currentState = PlayState.Running

            If isAudioOnly = False Then
                'Set Video Size
                hr = VideoWindowSize(1, 1)
                DsError.ThrowExceptionForHR(hr)
            End If
        Catch ex As Exception
            MsgBox("Error " & ex.Message, MsgBoxStyle.Critical, "Error")
            RaiseEvent MedClose()
        End Try
    End Sub

    Private Sub CheckType()
        Try
            Dim hr As Integer = 0
            Dim lVisible As OABool

            'If Interface is Nothing then Media is Audio
            If basicVideo Is Nothing Or videoWindow Is Nothing Then
                isAudioOnly = True
            Else
                isAudioOnly = False
            End If

            'Another way to test if Audio or Video
            hr = videoWindow.get_Visible(lVisible)
            If hr < 0 Then
                isAudioOnly = True
            End If
        Catch ex As Exception
            MsgBox("Errpr " & ex.Message, MsgBoxStyle.Critical, "Error")
            RaiseEvent MedClose()
        End Try
    End Sub

    Private Sub SetFormTitle()
        Dim hr As Integer = 0
        If filename = Nothing Then
            Me.Text = "Media Player"
        Else
            Dim MedTitle As String = ""

            If isAudioOnly = True Then
                MedTitle = "Audio"
            Else
                MedTitle = "Video"
            End If

            If currentState = PlayState.Paused Then
                MedTitle = MedTitle & " - Paused - "
            End If
            If currentState = PlayState.Running Then
                MedTitle = MedTitle & " - Playing - "
            End If
            If currentState = PlayState.Stopped Then
                MedTitle = MedTitle & " - Stopped - "
            End If

            Me.Text = MedTitle & filename
            MedTitle = Nothing
        End If
    End Sub
    Private Function VideoWindowSize(ByVal nMultiplier As Integer, ByVal nDivider As Integer) As Integer
        Try
            Dim hr As Integer = 0
            Dim lHeight As Integer, lWidth As Integer
            'Get Video Size
            hr = basicVideo.GetVideoSize(lWidth, lHeight)
            If hr = DsResults.E_NoInterface Then Return 0 : Exit Function

            'Change if Different Size is selected in menu(50%, 200%..)
            lWidth = lWidth * nMultiplier / nDivider
            lHeight = lHeight * nMultiplier / nDivider


            'Set Window Size video will play on
            'UseCtrl.ClientSize = New Size(lWidth, lHeight)
            'Dim s As Size = UseCtrl.ClientSize
            Application.DoEvents()

            'Set Video Position on Window
            hr = videoWindow.SetWindowPosition(0, 0, lWidth, lHeight)
            'hr = videoWindow.SetWindowPosition(0, 0, s.Width, s.Height)
            Return hr
        Catch ex As Exception
            MsgBox("Error " & ex.Message, MsgBoxStyle.Critical, "Error")
            RaiseEvent MedClose()
        End Try
    End Function

    Private Sub CloseInterfaces()
        Try
            Dim hr As Integer = 0
            'Release Window Handle, Reset back to Normal
            If isAudioOnly = False Then
                hr = videoWindow.put_Visible(OABool.False)
                DsError.ThrowExceptionForHR(hr)

                hr = videoWindow.put_Owner(IntPtr.Zero)
                DsError.ThrowExceptionForHR(hr)
            End If

            If mediaEventEx Is Nothing = False Then
                hr = mediaEventEx.SetNotifyWindow(IntPtr.Zero, 0, IntPtr.Zero)
                DsError.ThrowExceptionForHR(hr)
            End If


#If DEBUG Then
            If rot Is Nothing = False Then
                rot.Dispose()
                rot = Nothing
            End If
#End If

            'Release everything from memory
            mediaEventEx = Nothing
            mediaSeeking = Nothing
            mediaPosition = Nothing
            mediaControl = Nothing
            basicAudio = Nothing
            basicVideo = Nothing
            videoWindow = Nothing
            frameStep = Nothing
            Marshal.ReleaseComObject(graphBuilder)
            graphBuilder = Nothing
            GC.Collect()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub jackpotgold_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        CloseClip()
    End Sub
End Class