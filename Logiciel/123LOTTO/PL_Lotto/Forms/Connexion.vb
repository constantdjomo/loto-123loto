﻿Public Class Connexion
    Private oUser As BL_Lotto.BlUser
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Using oUser As New BL_Lotto.BlUser(source)
            If oUser.ConPossible(txtUsername.Text.Trim, txtPassword.Text.Trim) = True Then

                CONNECTED_USERID = CType(oUser.GetUserID(txtUsername.Text.Trim, txtPassword.Text.Trim), String)
                If CONNECTED_USERID = "0" Then
                    MessageBox.Show("Impossible de vous connecter!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    txtUsername.Focus()
                Else
                    FrmPrincipal.Show()
                    Me.Close()
                End If
                
            Else
                MessageBox.Show("Utilisateur Invalide!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtUsername.Focus()
            End If
        End Using
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Dispose()

    End Sub

End Class
