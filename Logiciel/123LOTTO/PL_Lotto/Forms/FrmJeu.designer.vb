﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.gpot_lbl = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Lb_Min = New System.Windows.Forms.Label()
        Me.LB_NumTir = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LB_DT_31 = New System.Windows.Forms.Label()
        Me.LB_DT_32 = New System.Windows.Forms.Label()
        Me.LB_DT_33 = New System.Windows.Forms.Label()
        Me.LB_DT_34 = New System.Windows.Forms.Label()
        Me.LB_DT_35 = New System.Windows.Forms.Label()
        Me.LB_DT_41 = New System.Windows.Forms.Label()
        Me.LB_DT_42 = New System.Windows.Forms.Label()
        Me.LB_DT_43 = New System.Windows.Forms.Label()
        Me.LB_DT_44 = New System.Windows.Forms.Label()
        Me.LB_DT_45 = New System.Windows.Forms.Label()
        Me.LB_DT_51 = New System.Windows.Forms.Label()
        Me.LB_DT_52 = New System.Windows.Forms.Label()
        Me.LB_DT_53 = New System.Windows.Forms.Label()
        Me.LB_DT_54 = New System.Windows.Forms.Label()
        Me.LB_DT_55 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.gpot_name_lbl = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Lb_iron_pot_name = New System.Windows.Forms.Label()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Lb_iron_pot = New System.Windows.Forms.Label()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.lbelSilver_name = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.lbelSilver = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.LB_DT_15 = New System.Windows.Forms.Label()
        Me.LB_DT_14 = New System.Windows.Forms.Label()
        Me.LB_DT_13 = New System.Windows.Forms.Label()
        Me.LB_DT_12 = New System.Windows.Forms.Label()
        Me.LB_DT_11 = New System.Windows.Forms.Label()
        Me.LB_DT_1 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.LB_DT_25 = New System.Windows.Forms.Label()
        Me.LB_DT_24 = New System.Windows.Forms.Label()
        Me.LB_DT_23 = New System.Windows.Forms.Label()
        Me.LB_DT_22 = New System.Windows.Forms.Label()
        Me.LB_DT_21 = New System.Windows.Forms.Label()
        Me.LB_DT_2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.LB_DT_5 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.LB_DT_4 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.LB_DT_3 = New System.Windows.Forms.Label()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.LB_second = New System.Windows.Forms.Label()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.Panel14.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel18.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel16.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel11.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Tag = "time"
        '
        'gpot_lbl
        '
        Me.gpot_lbl.AutoSize = True
        Me.gpot_lbl.Font = New System.Drawing.Font("Century Gothic", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpot_lbl.ForeColor = System.Drawing.Color.White
        Me.gpot_lbl.Location = New System.Drawing.Point(121, 33)
        Me.gpot_lbl.Name = "gpot_lbl"
        Me.gpot_lbl.Size = New System.Drawing.Size(203, 36)
        Me.gpot_lbl.TabIndex = 24
        Me.gpot_lbl.Text = "Ecran Plasma"
        Me.gpot_lbl.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(166, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 46)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = ":"
        '
        'Lb_Min
        '
        Me.Lb_Min.AutoSize = True
        Me.Lb_Min.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lb_Min.ForeColor = System.Drawing.Color.White
        Me.Lb_Min.Location = New System.Drawing.Point(128, 5)
        Me.Lb_Min.Name = "Lb_Min"
        Me.Lb_Min.Size = New System.Drawing.Size(43, 46)
        Me.Lb_Min.TabIndex = 24
        Me.Lb_Min.Text = "0"
        Me.Lb_Min.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LB_NumTir
        '
        Me.LB_NumTir.AutoSize = True
        Me.LB_NumTir.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.SetColumnSpan(Me.LB_NumTir, 2)
        Me.LB_NumTir.Font = New System.Drawing.Font("Showcard Gothic", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_NumTir.ForeColor = System.Drawing.Color.White
        Me.LB_NumTir.Location = New System.Drawing.Point(761, 0)
        Me.LB_NumTir.Name = "LB_NumTir"
        Me.LB_NumTir.Size = New System.Drawing.Size(122, 60)
        Me.LB_NumTir.TabIndex = 18
        Me.LB_NumTir.Text = "1024"
        Me.LB_NumTir.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.SetColumnSpan(Me.Label1, 2)
        Me.Label1.Font = New System.Drawing.Font("Showcard Gothic", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(469, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(286, 60)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Tirage No:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LB_DT_31
        '
        Me.LB_DT_31.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_31.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_31.ForeColor = System.Drawing.Color.White
        Me.LB_DT_31.Location = New System.Drawing.Point(6, 5)
        Me.LB_DT_31.Name = "LB_DT_31"
        Me.LB_DT_31.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_31.TabIndex = 0
        Me.LB_DT_31.Text = "--"
        Me.LB_DT_31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_32
        '
        Me.LB_DT_32.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_32.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_32.ForeColor = System.Drawing.Color.White
        Me.LB_DT_32.Location = New System.Drawing.Point(81, 5)
        Me.LB_DT_32.Name = "LB_DT_32"
        Me.LB_DT_32.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_32.TabIndex = 0
        Me.LB_DT_32.Text = "--"
        Me.LB_DT_32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_33
        '
        Me.LB_DT_33.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_33.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_33.ForeColor = System.Drawing.Color.White
        Me.LB_DT_33.Location = New System.Drawing.Point(156, 5)
        Me.LB_DT_33.Name = "LB_DT_33"
        Me.LB_DT_33.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_33.TabIndex = 0
        Me.LB_DT_33.Text = "--"
        Me.LB_DT_33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_34
        '
        Me.LB_DT_34.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_34.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_34.ForeColor = System.Drawing.Color.White
        Me.LB_DT_34.Location = New System.Drawing.Point(231, 5)
        Me.LB_DT_34.Name = "LB_DT_34"
        Me.LB_DT_34.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_34.TabIndex = 0
        Me.LB_DT_34.Text = "--"
        Me.LB_DT_34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_35
        '
        Me.LB_DT_35.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_35.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_35.ForeColor = System.Drawing.Color.White
        Me.LB_DT_35.Location = New System.Drawing.Point(306, 5)
        Me.LB_DT_35.Name = "LB_DT_35"
        Me.LB_DT_35.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_35.TabIndex = 0
        Me.LB_DT_35.Text = "--"
        Me.LB_DT_35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_41
        '
        Me.LB_DT_41.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_41.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_41.ForeColor = System.Drawing.Color.White
        Me.LB_DT_41.Location = New System.Drawing.Point(6, 6)
        Me.LB_DT_41.Name = "LB_DT_41"
        Me.LB_DT_41.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_41.TabIndex = 0
        Me.LB_DT_41.Text = "--"
        Me.LB_DT_41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_42
        '
        Me.LB_DT_42.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_42.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_42.ForeColor = System.Drawing.Color.White
        Me.LB_DT_42.Location = New System.Drawing.Point(81, 5)
        Me.LB_DT_42.Name = "LB_DT_42"
        Me.LB_DT_42.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_42.TabIndex = 0
        Me.LB_DT_42.Text = "--"
        Me.LB_DT_42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_43
        '
        Me.LB_DT_43.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_43.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_43.ForeColor = System.Drawing.Color.White
        Me.LB_DT_43.Location = New System.Drawing.Point(156, 5)
        Me.LB_DT_43.Name = "LB_DT_43"
        Me.LB_DT_43.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_43.TabIndex = 0
        Me.LB_DT_43.Text = "--"
        Me.LB_DT_43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_44
        '
        Me.LB_DT_44.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_44.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_44.ForeColor = System.Drawing.Color.White
        Me.LB_DT_44.Location = New System.Drawing.Point(231, 5)
        Me.LB_DT_44.Name = "LB_DT_44"
        Me.LB_DT_44.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_44.TabIndex = 0
        Me.LB_DT_44.Text = "--"
        Me.LB_DT_44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_45
        '
        Me.LB_DT_45.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_45.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_45.ForeColor = System.Drawing.Color.White
        Me.LB_DT_45.Location = New System.Drawing.Point(306, 5)
        Me.LB_DT_45.Name = "LB_DT_45"
        Me.LB_DT_45.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_45.TabIndex = 0
        Me.LB_DT_45.Text = "--"
        Me.LB_DT_45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_51
        '
        Me.LB_DT_51.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_51.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_51.ForeColor = System.Drawing.Color.White
        Me.LB_DT_51.Location = New System.Drawing.Point(6, 5)
        Me.LB_DT_51.Name = "LB_DT_51"
        Me.LB_DT_51.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_51.TabIndex = 0
        Me.LB_DT_51.Text = "--"
        Me.LB_DT_51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_52
        '
        Me.LB_DT_52.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_52.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_52.ForeColor = System.Drawing.Color.White
        Me.LB_DT_52.Location = New System.Drawing.Point(81, 5)
        Me.LB_DT_52.Name = "LB_DT_52"
        Me.LB_DT_52.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_52.TabIndex = 0
        Me.LB_DT_52.Text = "--"
        Me.LB_DT_52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_53
        '
        Me.LB_DT_53.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_53.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_53.ForeColor = System.Drawing.Color.White
        Me.LB_DT_53.Location = New System.Drawing.Point(156, 5)
        Me.LB_DT_53.Name = "LB_DT_53"
        Me.LB_DT_53.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_53.TabIndex = 0
        Me.LB_DT_53.Text = "--"
        Me.LB_DT_53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_54
        '
        Me.LB_DT_54.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_54.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_54.ForeColor = System.Drawing.Color.White
        Me.LB_DT_54.Location = New System.Drawing.Point(231, 5)
        Me.LB_DT_54.Name = "LB_DT_54"
        Me.LB_DT_54.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_54.TabIndex = 0
        Me.LB_DT_54.Text = "--"
        Me.LB_DT_54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_55
        '
        Me.LB_DT_55.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_55.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_55.ForeColor = System.Drawing.Color.White
        Me.LB_DT_55.Location = New System.Drawing.Point(306, 5)
        Me.LB_DT_55.Name = "LB_DT_55"
        Me.LB_DT_55.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_55.TabIndex = 0
        Me.LB_DT_55.Text = "--"
        Me.LB_DT_55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.Panel1.Controls.Add(Me.TableLayoutPanel2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.Panel2.Controls.Add(Me.TableLayoutPanel1)
        Me.SplitContainer1.Size = New System.Drawing.Size(1358, 738)
        Me.SplitContainer1.SplitterDistance = 966
        Me.SplitContainer1.TabIndex = 34
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 7
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.923077!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.23077!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.23077!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.23077!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.23077!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.23077!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.923077!))
        Me.TableLayoutPanel2.Controls.Add(Me.LB_NumTir, 5, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel13, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label1, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel17, 4, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 8
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(966, 738)
        Me.TableLayoutPanel2.TabIndex = 41
        '
        'Panel13
        '
        Me.Panel13.BackColor = System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(172, Byte), Integer))
        Me.TableLayoutPanel2.SetColumnSpan(Me.Panel13, 2)
        Me.Panel13.Controls.Add(Me.Panel15)
        Me.Panel13.Controls.Add(Me.Label4)
        Me.Panel13.Controls.Add(Me.Label3)
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel13.Location = New System.Drawing.Point(21, 95)
        Me.Panel13.Name = "Panel13"
        Me.TableLayoutPanel2.SetRowSpan(Me.Panel13, 6)
        Me.Panel13.Size = New System.Drawing.Size(364, 546)
        Me.Panel13.TabIndex = 19
        '
        'Panel15
        '
        Me.Panel15.BackColor = System.Drawing.Color.White
        Me.Panel15.Controls.Add(Me.Label7)
        Me.Panel15.Controls.Add(Me.Label6)
        Me.Panel15.Controls.Add(Me.Label5)
        Me.Panel15.Location = New System.Drawing.Point(0, 143)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(364, 403)
        Me.Panel15.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Impact", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(172, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(3, 306)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(357, 85)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "*ET PARTICIPEZ AU TIRAGE AU SORT DU JACKPOT"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Impact", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(172, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(7, 184)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(357, 74)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "VOTRE MISE"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Impact", 50.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(172, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(3, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(361, 188)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "    GAGNEZ    15X"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Impact", 45.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(29, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(299, 65)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "1 Numero:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Impact", 50.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(12, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(341, 82)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Choisissez"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel17
        '
        Me.Panel17.BackColor = System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(172, Byte), Integer))
        Me.TableLayoutPanel2.SetColumnSpan(Me.Panel17, 2)
        Me.Panel17.Controls.Add(Me.Panel19)
        Me.Panel17.Controls.Add(Me.Label13)
        Me.Panel17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel17.Location = New System.Drawing.Point(576, 95)
        Me.Panel17.Name = "Panel17"
        Me.TableLayoutPanel2.SetRowSpan(Me.Panel17, 6)
        Me.Panel17.Size = New System.Drawing.Size(364, 546)
        Me.Panel17.TabIndex = 19
        '
        'Panel19
        '
        Me.Panel19.BackColor = System.Drawing.Color.White
        Me.Panel19.Controls.Add(Me.Panel14)
        Me.Panel19.Controls.Add(Me.Panel18)
        Me.Panel19.Controls.Add(Me.Panel16)
        Me.Panel19.Location = New System.Drawing.Point(0, 99)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(364, 447)
        Me.Panel19.TabIndex = 0
        '
        'Panel14
        '
        Me.Panel14.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Panel14.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(42, Byte), Integer))
        Me.Panel14.Controls.Add(Me.gpot_name_lbl)
        Me.Panel14.Controls.Add(Me.PictureBox2)
        Me.Panel14.Controls.Add(Me.gpot_lbl)
        Me.Panel14.Location = New System.Drawing.Point(1, 57)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(360, 104)
        Me.Panel14.TabIndex = 3
        '
        'gpot_name_lbl
        '
        Me.gpot_name_lbl.AutoSize = True
        Me.gpot_name_lbl.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold)
        Me.gpot_name_lbl.ForeColor = System.Drawing.Color.White
        Me.gpot_name_lbl.Location = New System.Drawing.Point(111, 6)
        Me.gpot_name_lbl.Name = "gpot_name_lbl"
        Me.gpot_name_lbl.Size = New System.Drawing.Size(193, 28)
        Me.gpot_name_lbl.TabIndex = 28
        Me.gpot_name_lbl.Text = "Super Cagnotte"
        Me.gpot_name_lbl.Visible = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.PL_Lotto.My.Resources.Resources.mobile_512
        Me.PictureBox2.Location = New System.Drawing.Point(7, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(98, 95)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 27
        Me.PictureBox2.TabStop = False
        '
        'Panel18
        '
        Me.Panel18.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Panel18.BackColor = System.Drawing.Color.DarkGreen
        Me.Panel18.Controls.Add(Me.Lb_iron_pot_name)
        Me.Panel18.Controls.Add(Me.PictureBox5)
        Me.Panel18.Controls.Add(Me.Lb_iron_pot)
        Me.Panel18.Location = New System.Drawing.Point(1, 188)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(360, 103)
        Me.Panel18.TabIndex = 3
        '
        'Lb_iron_pot_name
        '
        Me.Lb_iron_pot_name.AutoSize = True
        Me.Lb_iron_pot_name.BackColor = System.Drawing.Color.Transparent
        Me.Lb_iron_pot_name.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold)
        Me.Lb_iron_pot_name.ForeColor = System.Drawing.Color.White
        Me.Lb_iron_pot_name.Location = New System.Drawing.Point(108, 10)
        Me.Lb_iron_pot_name.Name = "Lb_iron_pot_name"
        Me.Lb_iron_pot_name.Size = New System.Drawing.Size(121, 28)
        Me.Lb_iron_pot_name.TabIndex = 28
        Me.Lb_iron_pot_name.Text = "Cagnotte"
        Me.Lb_iron_pot_name.Visible = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.White
        Me.PictureBox5.Image = Global.PL_Lotto.My.Resources.Resources.cash_money_512
        Me.PictureBox5.Location = New System.Drawing.Point(5, 3)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(98, 97)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox5.TabIndex = 27
        Me.PictureBox5.TabStop = False
        '
        'Lb_iron_pot
        '
        Me.Lb_iron_pot.AutoSize = True
        Me.Lb_iron_pot.BackColor = System.Drawing.Color.Transparent
        Me.Lb_iron_pot.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold)
        Me.Lb_iron_pot.ForeColor = System.Drawing.Color.White
        Me.Lb_iron_pot.Location = New System.Drawing.Point(109, 51)
        Me.Lb_iron_pot.Name = "Lb_iron_pot"
        Me.Lb_iron_pot.Size = New System.Drawing.Size(156, 28)
        Me.Lb_iron_pot.TabIndex = 24
        Me.Lb_iron_pot.Text = "Smart Phone"
        Me.Lb_iron_pot.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel16
        '
        Me.Panel16.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Panel16.BackColor = System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(140, Byte), Integer), CType(CType(172, Byte), Integer))
        Me.Panel16.Controls.Add(Me.lbelSilver_name)
        Me.Panel16.Controls.Add(Me.PictureBox6)
        Me.Panel16.Controls.Add(Me.lbelSilver)
        Me.Panel16.Location = New System.Drawing.Point(2, 317)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(360, 110)
        Me.Panel16.TabIndex = 3
        '
        'lbelSilver_name
        '
        Me.lbelSilver_name.AutoSize = True
        Me.lbelSilver_name.BackColor = System.Drawing.Color.Transparent
        Me.lbelSilver_name.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold)
        Me.lbelSilver_name.ForeColor = System.Drawing.Color.White
        Me.lbelSilver_name.Location = New System.Drawing.Point(112, 10)
        Me.lbelSilver_name.Name = "lbelSilver_name"
        Me.lbelSilver_name.Size = New System.Drawing.Size(121, 28)
        Me.lbelSilver_name.TabIndex = 28
        Me.lbelSilver_name.Text = "Cagnotte"
        Me.lbelSilver_name.Visible = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.White
        Me.PictureBox6.Image = Global.PL_Lotto.My.Resources.Resources.cash_money_512
        Me.PictureBox6.Location = New System.Drawing.Point(8, 3)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(98, 97)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox6.TabIndex = 27
        Me.PictureBox6.TabStop = False
        '
        'lbelSilver
        '
        Me.lbelSilver.AutoSize = True
        Me.lbelSilver.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold)
        Me.lbelSilver.ForeColor = System.Drawing.Color.White
        Me.lbelSilver.Location = New System.Drawing.Point(112, 51)
        Me.lbelSilver.Name = "lbelSilver"
        Me.lbelSilver.Size = New System.Drawing.Size(220, 28)
        Me.lbelSilver.TabIndex = 24
        Me.lbelSilver.Text = "GSBE-022015-2546"
        Me.lbelSilver.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Impact", 50.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(28, 8)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(309, 82)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "JACKPOTS"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.Panel9, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel7, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel5, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel11, 0, 5)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(388, 738)
        Me.TableLayoutPanel1.TabIndex = 33
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Black
        Me.Panel9.Controls.Add(Me.Panel10)
        Me.Panel9.Controls.Add(Me.LB_DT_1)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel9.Location = New System.Drawing.Point(3, 3)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(385, 116)
        Me.Panel9.TabIndex = 6
        '
        'Panel10
        '
        Me.Panel10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel10.BackColor = System.Drawing.Color.Black
        Me.Panel10.Controls.Add(Me.LB_DT_15)
        Me.Panel10.Controls.Add(Me.LB_DT_14)
        Me.Panel10.Controls.Add(Me.LB_DT_13)
        Me.Panel10.Controls.Add(Me.LB_DT_12)
        Me.Panel10.Controls.Add(Me.LB_DT_11)
        Me.Panel10.Location = New System.Drawing.Point(3, 48)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(379, 65)
        Me.Panel10.TabIndex = 3
        '
        'LB_DT_15
        '
        Me.LB_DT_15.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_15.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_15.ForeColor = System.Drawing.Color.White
        Me.LB_DT_15.Location = New System.Drawing.Point(306, 6)
        Me.LB_DT_15.Name = "LB_DT_15"
        Me.LB_DT_15.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_15.TabIndex = 1
        Me.LB_DT_15.Text = "--"
        Me.LB_DT_15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_14
        '
        Me.LB_DT_14.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_14.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_14.ForeColor = System.Drawing.Color.White
        Me.LB_DT_14.Location = New System.Drawing.Point(231, 6)
        Me.LB_DT_14.Name = "LB_DT_14"
        Me.LB_DT_14.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_14.TabIndex = 2
        Me.LB_DT_14.Text = "--"
        Me.LB_DT_14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_13
        '
        Me.LB_DT_13.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_13.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_13.ForeColor = System.Drawing.Color.White
        Me.LB_DT_13.Location = New System.Drawing.Point(156, 6)
        Me.LB_DT_13.Name = "LB_DT_13"
        Me.LB_DT_13.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_13.TabIndex = 3
        Me.LB_DT_13.Text = "--"
        Me.LB_DT_13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_12
        '
        Me.LB_DT_12.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_12.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_12.ForeColor = System.Drawing.Color.White
        Me.LB_DT_12.Location = New System.Drawing.Point(81, 6)
        Me.LB_DT_12.Name = "LB_DT_12"
        Me.LB_DT_12.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_12.TabIndex = 4
        Me.LB_DT_12.Text = "--"
        Me.LB_DT_12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_11
        '
        Me.LB_DT_11.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_11.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_11.ForeColor = System.Drawing.Color.White
        Me.LB_DT_11.Location = New System.Drawing.Point(6, 6)
        Me.LB_DT_11.Name = "LB_DT_11"
        Me.LB_DT_11.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_11.TabIndex = 5
        Me.LB_DT_11.Text = "--"
        Me.LB_DT_11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_1
        '
        Me.LB_DT_1.AutoSize = True
        Me.LB_DT_1.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LB_DT_1.ForeColor = System.Drawing.Color.White
        Me.LB_DT_1.Location = New System.Drawing.Point(3, 9)
        Me.LB_DT_1.Name = "LB_DT_1"
        Me.LB_DT_1.Size = New System.Drawing.Size(134, 27)
        Me.LB_DT_1.TabIndex = 2
        Me.LB_DT_1.Text = "Tirage No:"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Black
        Me.Panel7.Controls.Add(Me.Panel8)
        Me.Panel7.Controls.Add(Me.LB_DT_2)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel7.Location = New System.Drawing.Point(3, 125)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(385, 116)
        Me.Panel7.TabIndex = 4
        '
        'Panel8
        '
        Me.Panel8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel8.BackColor = System.Drawing.Color.FromArgb(CType(CType(55, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.Panel8.Controls.Add(Me.LB_DT_25)
        Me.Panel8.Controls.Add(Me.LB_DT_24)
        Me.Panel8.Controls.Add(Me.LB_DT_23)
        Me.Panel8.Controls.Add(Me.LB_DT_22)
        Me.Panel8.Controls.Add(Me.LB_DT_21)
        Me.Panel8.Location = New System.Drawing.Point(3, 48)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(379, 65)
        Me.Panel8.TabIndex = 3
        '
        'LB_DT_25
        '
        Me.LB_DT_25.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_25.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_25.ForeColor = System.Drawing.Color.White
        Me.LB_DT_25.Location = New System.Drawing.Point(306, 5)
        Me.LB_DT_25.Name = "LB_DT_25"
        Me.LB_DT_25.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_25.TabIndex = 1
        Me.LB_DT_25.Text = "--"
        Me.LB_DT_25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_24
        '
        Me.LB_DT_24.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_24.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_24.ForeColor = System.Drawing.Color.White
        Me.LB_DT_24.Location = New System.Drawing.Point(231, 5)
        Me.LB_DT_24.Name = "LB_DT_24"
        Me.LB_DT_24.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_24.TabIndex = 2
        Me.LB_DT_24.Text = "--"
        Me.LB_DT_24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_23
        '
        Me.LB_DT_23.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_23.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_23.ForeColor = System.Drawing.Color.White
        Me.LB_DT_23.Location = New System.Drawing.Point(156, 5)
        Me.LB_DT_23.Name = "LB_DT_23"
        Me.LB_DT_23.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_23.TabIndex = 3
        Me.LB_DT_23.Text = "--"
        Me.LB_DT_23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_22
        '
        Me.LB_DT_22.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_22.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_22.ForeColor = System.Drawing.Color.White
        Me.LB_DT_22.Location = New System.Drawing.Point(81, 5)
        Me.LB_DT_22.Name = "LB_DT_22"
        Me.LB_DT_22.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_22.TabIndex = 4
        Me.LB_DT_22.Text = "--"
        Me.LB_DT_22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_21
        '
        Me.LB_DT_21.BackColor = System.Drawing.Color.Transparent
        Me.LB_DT_21.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_DT_21.ForeColor = System.Drawing.Color.White
        Me.LB_DT_21.Location = New System.Drawing.Point(6, 5)
        Me.LB_DT_21.Name = "LB_DT_21"
        Me.LB_DT_21.Size = New System.Drawing.Size(66, 55)
        Me.LB_DT_21.TabIndex = 5
        Me.LB_DT_21.Text = "--"
        Me.LB_DT_21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LB_DT_2
        '
        Me.LB_DT_2.AutoSize = True
        Me.LB_DT_2.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LB_DT_2.ForeColor = System.Drawing.Color.White
        Me.LB_DT_2.Location = New System.Drawing.Point(3, 9)
        Me.LB_DT_2.Name = "LB_DT_2"
        Me.LB_DT_2.Size = New System.Drawing.Size(134, 27)
        Me.LB_DT_2.TabIndex = 2
        Me.LB_DT_2.Text = "Tirage No:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Black
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.LB_DT_5)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 491)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(385, 116)
        Me.Panel1.TabIndex = 2
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Black
        Me.Panel2.Controls.Add(Me.LB_DT_55)
        Me.Panel2.Controls.Add(Me.LB_DT_51)
        Me.Panel2.Controls.Add(Me.LB_DT_54)
        Me.Panel2.Controls.Add(Me.LB_DT_52)
        Me.Panel2.Controls.Add(Me.LB_DT_53)
        Me.Panel2.Location = New System.Drawing.Point(3, 48)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(379, 65)
        Me.Panel2.TabIndex = 3
        '
        'LB_DT_5
        '
        Me.LB_DT_5.AutoSize = True
        Me.LB_DT_5.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LB_DT_5.ForeColor = System.Drawing.Color.White
        Me.LB_DT_5.Location = New System.Drawing.Point(3, 9)
        Me.LB_DT_5.Name = "LB_DT_5"
        Me.LB_DT_5.Size = New System.Drawing.Size(134, 27)
        Me.LB_DT_5.TabIndex = 2
        Me.LB_DT_5.Text = "Tirage No:"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Black
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.LB_DT_4)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 369)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(385, 116)
        Me.Panel3.TabIndex = 2
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(55, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.Panel4.Controls.Add(Me.LB_DT_45)
        Me.Panel4.Controls.Add(Me.LB_DT_41)
        Me.Panel4.Controls.Add(Me.LB_DT_44)
        Me.Panel4.Controls.Add(Me.LB_DT_42)
        Me.Panel4.Controls.Add(Me.LB_DT_43)
        Me.Panel4.Location = New System.Drawing.Point(3, 48)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(379, 65)
        Me.Panel4.TabIndex = 3
        '
        'LB_DT_4
        '
        Me.LB_DT_4.AutoSize = True
        Me.LB_DT_4.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LB_DT_4.ForeColor = System.Drawing.Color.White
        Me.LB_DT_4.Location = New System.Drawing.Point(3, 9)
        Me.LB_DT_4.Name = "LB_DT_4"
        Me.LB_DT_4.Size = New System.Drawing.Size(134, 27)
        Me.LB_DT_4.TabIndex = 2
        Me.LB_DT_4.Text = "Tirage No:"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Black
        Me.Panel5.Controls.Add(Me.Panel6)
        Me.Panel5.Controls.Add(Me.LB_DT_3)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(3, 247)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(385, 116)
        Me.Panel5.TabIndex = 2
        '
        'Panel6
        '
        Me.Panel6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel6.BackColor = System.Drawing.Color.Black
        Me.Panel6.Controls.Add(Me.LB_DT_35)
        Me.Panel6.Controls.Add(Me.LB_DT_31)
        Me.Panel6.Controls.Add(Me.LB_DT_34)
        Me.Panel6.Controls.Add(Me.LB_DT_32)
        Me.Panel6.Controls.Add(Me.LB_DT_33)
        Me.Panel6.Location = New System.Drawing.Point(3, 48)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(379, 65)
        Me.Panel6.TabIndex = 3
        '
        'LB_DT_3
        '
        Me.LB_DT_3.AutoSize = True
        Me.LB_DT_3.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LB_DT_3.ForeColor = System.Drawing.Color.White
        Me.LB_DT_3.Location = New System.Drawing.Point(3, 9)
        Me.LB_DT_3.Name = "LB_DT_3"
        Me.LB_DT_3.Size = New System.Drawing.Size(134, 27)
        Me.LB_DT_3.TabIndex = 2
        Me.LB_DT_3.Text = "Tirage No:"
        '
        'Panel11
        '
        Me.Panel11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel11.BackColor = System.Drawing.Color.Black
        Me.Panel11.Controls.Add(Me.Label11)
        Me.Panel11.Controls.Add(Me.PictureBox1)
        Me.Panel11.Controls.Add(Me.Panel12)
        Me.Panel11.Location = New System.Drawing.Point(3, 629)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(385, 106)
        Me.Panel11.TabIndex = 30
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Showcard Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(164, 8)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label11.Size = New System.Drawing.Size(92, 30)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "Timer" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.PL_Lotto.My.Resources.Resources._94
        Me.PictureBox1.Location = New System.Drawing.Point(116, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(60, 40)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 4
        Me.PictureBox1.TabStop = False
        '
        'Panel12
        '
        Me.Panel12.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel12.BackColor = System.Drawing.Color.FromArgb(CType(CType(55, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(55, Byte), Integer))
        Me.Panel12.Controls.Add(Me.Label2)
        Me.Panel12.Controls.Add(Me.Lb_Min)
        Me.Panel12.Controls.Add(Me.LB_second)
        Me.Panel12.Location = New System.Drawing.Point(8, 48)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(364, 55)
        Me.Panel12.TabIndex = 3
        '
        'LB_second
        '
        Me.LB_second.AutoSize = True
        Me.LB_second.Font = New System.Drawing.Font("Showcard Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_second.ForeColor = System.Drawing.Color.White
        Me.LB_second.Location = New System.Drawing.Point(194, 5)
        Me.LB_second.Name = "LB_second"
        Me.LB_second.Size = New System.Drawing.Size(43, 46)
        Me.LB_second.TabIndex = 23
        Me.LB_second.Text = "0"
        Me.LB_second.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Timer2
        '
        '
        'FrmJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.Black
        Me.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.free_sport_bets
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(1358, 738)
        Me.Controls.Add(Me.SplitContainer1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmJeu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "123Lotto-Jeu"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        Me.Panel19.ResumeLayout(False)
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents LB_NumTir As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Lb_Min As System.Windows.Forms.Label
    Friend WithEvents gpot_lbl As System.Windows.Forms.Label
    Friend WithEvents LB_DT_55 As Label
    Friend WithEvents LB_DT_54 As Label
    Friend WithEvents LB_DT_53 As Label
    Friend WithEvents LB_DT_52 As Label
    Friend WithEvents LB_DT_51 As Label
    Friend WithEvents LB_DT_45 As Label
    Friend WithEvents LB_DT_44 As Label
    Friend WithEvents LB_DT_43 As Label
    Friend WithEvents LB_DT_42 As Label
    Friend WithEvents LB_DT_41 As Label
    Friend WithEvents LB_DT_35 As Label
    Friend WithEvents LB_DT_34 As Label
    Friend WithEvents LB_DT_33 As Label
    Friend WithEvents LB_DT_32 As Label
    Friend WithEvents LB_DT_31 As Label
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents LB_second As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents LB_DT_5 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents LB_DT_4 As Label
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Panel6 As Panel
    Friend WithEvents LB_DT_3 As Label
    Friend WithEvents Panel9 As Panel
    Friend WithEvents Panel10 As Panel
    Friend WithEvents LB_DT_15 As Label
    Friend WithEvents LB_DT_14 As Label
    Friend WithEvents LB_DT_13 As Label
    Friend WithEvents LB_DT_12 As Label
    Friend WithEvents LB_DT_11 As Label
    Friend WithEvents LB_DT_1 As Label
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Panel8 As Panel
    Friend WithEvents LB_DT_25 As Label
    Friend WithEvents LB_DT_24 As Label
    Friend WithEvents LB_DT_23 As Label
    Friend WithEvents LB_DT_22 As Label
    Friend WithEvents LB_DT_21 As Label
    Friend WithEvents LB_DT_2 As Label
    Friend WithEvents Panel11 As Panel
    Friend WithEvents Panel12 As Panel
    Friend WithEvents Label11 As Label
    Friend WithEvents Panel14 As Panel
    Friend WithEvents Panel16 As Panel
    Friend WithEvents lbelSilver As Label
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Timer2 As Timer
    Friend WithEvents Panel18 As Panel
    Friend WithEvents Lb_iron_pot As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents gpot_name_lbl As Label
    Friend WithEvents Lb_iron_pot_name As Label
    Friend WithEvents lbelSilver_name As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Panel13 As Panel
    Friend WithEvents Panel15 As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Panel17 As Panel
    Friend WithEvents Panel19 As Panel
    Friend WithEvents Label13 As Label
End Class
