﻿Public Class CompConnexion
    Private oUser As BL_Lotto.BlUser
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Using oUser As New BL_Lotto.BlUser(source)
            If oUser.ConPossible(txtUsername.Text.Trim, txtPassword.Text.Trim) = True Then

                If CType(oUser.GetUserID(txtUsername.Text.Trim, txtPassword.Text.Trim), String) = "0" Then
                    MessageBox.Show("Impossible de vous connecter!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    txtUsername.Focus()
                Else
                    If (oUser.can(CType(oUser.GetUserID(txtUsername.Text.Trim, txtPassword.Text.Trim), String))) Then
                        FrmComptes.Show()
                        Me.Close()
                    Else
                        MessageBox.Show("Acces Refuser", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                    End If
                End If

            Else
                MessageBox.Show("Utilisateur Invalide!", "Echec de connexion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtUsername.Focus()
            End If
        End Using
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Dispose()
    End Sub

End Class
