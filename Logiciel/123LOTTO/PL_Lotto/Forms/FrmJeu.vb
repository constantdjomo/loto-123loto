﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading
Imports System.Globalization
Imports System.Timers


Public Class FrmJeu


    Private oTirage As BL_Lotto.BlTirage
    Private oTicketTi As BL_Lotto.BlTicket
    Private oBltriche As BL_Lotto.BlTriche
    Private Const Delai_JEu = 300 '300
    Private Const Delai_demarage_tir = 10 '10
    Private Const DureeAnim = 27
    Private Const MaxBoul = 90
    Private Const NbBoulTirag = 5
    Private Const Delai_anim_jackpot = 90000 ' 3min (1s = 1000 miliseconde )
    Private p As Point = New Point(136, 39)

    Private TabDelaiGagnant As Integer() = {Delai_JEu, Delai_JEu} ' 5 min; 10 min; 15 h;

    Private TotalEntrer, TotalEntrerSolo, TotalEntrerDoublet As ULong
    Private TotalBenef, TotalBenefSolo, TotalBenefDoublet As ULong
    Private prelSilverpot, prelSilverpotSolo, prelSilverpotDoub, prelevGoldpot, prelevGoldpotSolo, prelevGoldpotDoubl, prelevIrondpot, prelevIronpotSolo, prelevIronpotDoubl As ULong
    Dim GainsActuelClient, TotalmontantJack, GainsSolo, GainsDoublet As ULong

    Private ID_TICKET_GOLG, ID_TICKET_SILVER, ID_TICKET_IRON, ID_TICKET_CAGNOTE As String

    Private Tir_cour_jack As Integer

    Dim ThreadClient, ThreadAnim As Thread
    Dim MonSocketServeur As Socket

    Dim dsti As DataSet
    Dim d As DateTime = DateTime.Now


    Dim Tir_No As Integer = 0
    Dim time As Integer = Delai_JEu
    Dim DElaiGains As Integer = 0
    Dim tab1(5), tab2(5), tab3(5), tab4(5), tab5(5) As Integer
    Dim min, sec As Double


    Private Sub FrmJeu_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Dispose()
    End Sub

    Public Sub initialiseEcartGagnant()
        Dim ind As Integer()
        ind = CType(GetRandomDistinct(1, TabDelaiGagnant.Length), Integer())
        DELAI_AV_GAINS = TabDelaiGagnant(ind(0))
        DElaiGains = DELAI_AV_GAINS
    End Sub



    Public Sub GererClient()

        'Crée le socket et l'IP EP
        MonSocketServeur = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        Dim MonEP As IPEndPoint = New IPEndPoint(IPAddress.Any, CInt(PORT))

        ListeClients = New List(Of Client) 'Initialise la liste

        MonSocketServeur.Bind(MonEP) 'Lie le socket à cette IP
        MonSocketServeur.Listen(1) 'Se met en écoute

        'Console.WriteLine("Socket serveur initialisé sur le port " & port)

        While True 'Boucle à l'infini
            'Console.WriteLine("En attente d'un client.")
            'Se met en attente de connexion et appelle TraitementConnexion() lors d'une connexion.
            Dim SocketEnvoi As Socket = MonSocketServeur.Accept() 'Bloquant tant que pas de connexion
            TraitementConnexion(SocketEnvoi) 'Traite la connexion du client
        End While

    End Sub

    Sub TraitementConnexion(ByVal SocketEnvoi As Socket)
        'Console.WriteLine("Socket client connecté, création d'un thread.")
        Dim NouveauClient As New Client(SocketEnvoi) 'Crée une instance de « client »
        ListeClients.Add(NouveauClient) 'Ajoute le client à la liste
        'Crée un thread pour traiter ce client et le démarre
        Dim ThreadClient As New Thread(AddressOf NouveauClient.TraitementClient)
        ThreadClient.Start()
    End Sub

    Private Sub FrmJeu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not MonSocketServeur Is Nothing Then 'Si le socket a été créé
            MonSocketServeur.Close() 'On le ferme
        End If
        If Not ThreadClient Is Nothing Then 'Si le thread a été créé
            If ThreadClient.IsAlive Then 'S'il tourne
                ThreadClient.Abort() 'On le stoppe
            End If
        End If
        If Not ThreadAnim Is Nothing Then 'Si le thread a été créé
            If ThreadAnim.IsAlive Then 'S'il tourne
                ThreadAnim.Abort() 'On le stoppe
            End If
        End If
    End Sub

    Public Sub statistirage()



        Dim tir, oDsTirage As New DataSet
            'Dim t As Integer
            Using otirage As New BL_Lotto.BlTirage(source)
                'tir = otirage.DerniersTirages()
                oDsTirage = otirage.getTirage()
                For h As Integer = 0 To oDsTirage.Tables("contenir").Rows.Count - 1
                't = h Mod 10
                Select Case h
                    Case 0 To 4

                        LB_DT_1.Text = Label1.Text & " " & oDsTirage.Tables("contenir").Rows(h).Item("NUM_TIRAGE").ToString & " #"
                        If h = 0 Then
                            LB_DT_11.Text = oDsTirage.Tables("contenir").Rows(h).Item("ID_NUM_TIRER").ToString
                            LB_DT_12.Text = oDsTirage.Tables("contenir").Rows(h + 1).Item("ID_NUM_TIRER").ToString
                            LB_DT_13.Text = oDsTirage.Tables("contenir").Rows(h + 2).Item("ID_NUM_TIRER").ToString
                            LB_DT_14.Text = oDsTirage.Tables("contenir").Rows(h + 3).Item("ID_NUM_TIRER").ToString
                            LB_DT_15.Text = oDsTirage.Tables("contenir").Rows(h + 4).Item("ID_NUM_TIRER").ToString
                        End If



                    Case 5 To 9
                        If h = 5 Then
                            LB_DT_2.Text = Label1.Text & " " & oDsTirage.Tables("contenir").Rows(h).Item("NUM_TIRAGE").ToString & " #"
                            LB_DT_21.Text = oDsTirage.Tables("contenir").Rows(h + 0).Item("ID_NUM_TIRER").ToString
                            LB_DT_22.Text = oDsTirage.Tables("contenir").Rows(h + 1).Item("ID_NUM_TIRER").ToString
                            LB_DT_23.Text = oDsTirage.Tables("contenir").Rows(h + 2).Item("ID_NUM_TIRER").ToString
                            LB_DT_24.Text = oDsTirage.Tables("contenir").Rows(h + 3).Item("ID_NUM_TIRER").ToString
                            LB_DT_25.Text = oDsTirage.Tables("contenir").Rows(h + 4).Item("ID_NUM_TIRER").ToString
                        End If

                    Case 10 To 14

                        If h = 10 Then
                            LB_DT_3.Text = Label1.Text & " " & oDsTirage.Tables("contenir").Rows(h).Item("NUM_TIRAGE").ToString & " #"

                            LB_DT_31.Text = oDsTirage.Tables("contenir").Rows(h + 0).Item("ID_NUM_TIRER").ToString
                            LB_DT_32.Text = oDsTirage.Tables("contenir").Rows(h + 1).Item("ID_NUM_TIRER").ToString
                            LB_DT_33.Text = oDsTirage.Tables("contenir").Rows(h + 2).Item("ID_NUM_TIRER").ToString
                            LB_DT_34.Text = oDsTirage.Tables("contenir").Rows(h + 3).Item("ID_NUM_TIRER").ToString
                            LB_DT_35.Text = oDsTirage.Tables("contenir").Rows(h + 4).Item("ID_NUM_TIRER").ToString
                        End If


                    Case 15 To 19
                        If h = 15 Then
                            LB_DT_4.Text = Label1.Text & " " & oDsTirage.Tables("contenir").Rows(h).Item("NUM_TIRAGE").ToString & " #"

                            LB_DT_41.Text = oDsTirage.Tables("contenir").Rows(h + 0).Item("ID_NUM_TIRER").ToString
                            LB_DT_42.Text = oDsTirage.Tables("contenir").Rows(h + 1).Item("ID_NUM_TIRER").ToString
                            LB_DT_43.Text = oDsTirage.Tables("contenir").Rows(h + 2).Item("ID_NUM_TIRER").ToString
                            LB_DT_44.Text = oDsTirage.Tables("contenir").Rows(h + 3).Item("ID_NUM_TIRER").ToString
                            LB_DT_45.Text = oDsTirage.Tables("contenir").Rows(h + 4).Item("ID_NUM_TIRER").ToString
                        End If

                    Case 20 To 24
                        If h = 20 Then
                            LB_DT_5.Text = Label1.Text & " " & oDsTirage.Tables("contenir").Rows(h).Item("NUM_TIRAGE").ToString & " #"

                            LB_DT_51.Text = oDsTirage.Tables("contenir").Rows(h + 0).Item("ID_NUM_TIRER").ToString
                            LB_DT_52.Text = oDsTirage.Tables("contenir").Rows(h + 1).Item("ID_NUM_TIRER").ToString
                            LB_DT_53.Text = oDsTirage.Tables("contenir").Rows(h + 2).Item("ID_NUM_TIRER").ToString
                            LB_DT_54.Text = oDsTirage.Tables("contenir").Rows(h + 3).Item("ID_NUM_TIRER").ToString
                            LB_DT_55.Text = oDsTirage.Tables("contenir").Rows(h + 4).Item("ID_NUM_TIRER").ToString
                        End If

                End Select

            Next


            End Using
    End Sub
    Public Sub majforJack()
        Using ojacpot As New BL_Lotto.BlJackot(source)
            Dim dstsilver, dstgold, dstiron As New DataSet

            dstsilver = ojacpot.GetJackpot(ID_SILVER)
            lbelSilver_name.Text = dstsilver.Tables("jackpot").Rows(0).Item("nom_jackp").ToString
            PictureBox6.ImageLocation = Application.StartupPath & "\photo\" & dstsilver.Tables("jackpot").Rows(0).Item("photo").ToString
            PictureBox6.Refresh()
            If (CBool(dstsilver.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
                lbelSilver.Text = dstsilver.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
                'lbelSilver.Text = ojacpot.GetMontStatutJack(ID_SILVER) & " FCFA"
            Else
                'lbelSilver.Text = ojacpot.GetMontStatutJack(ID_SILVER) & " FCFA"
                lbelSilver.Text = ojacpot.GetMontantJackpot(ID_SILVER) & " FCFA"
            End If

            dstgold = ojacpot.GetJackpot(ID_GOLD)
            gpot_name_lbl.Text = dstgold.Tables("jackpot").Rows(0).Item("nom_jackp").ToString
            PictureBox2.ImageLocation = Application.StartupPath & "\photo\" & dstgold.Tables("jackpot").Rows(0).Item("photo").ToString
            PictureBox2.Refresh()
            If (CBool(dstgold.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
                gpot_lbl.Text = dstgold.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
                'lbelSatGold.Text = ojacpot.GetMontStatutJack(ID_GOLD) & " FCFA"
            Else
                gpot_lbl.Text = ojacpot.GetMontantJackpot(ID_GOLD) & " FCFA"
                'lbelSatGold.Text = "--- --- ----"
            End If

            ' iRON 
            dstiron = ojacpot.GetJackpot(ID_IRON)
            Lb_iron_pot_name.Text = dstiron.Tables("jackpot").Rows(0).Item("nom_jackp").ToString
            PictureBox5.ImageLocation = Application.StartupPath & "\photo\" & dstiron.Tables("jackpot").Rows(0).Item("photo").ToString
            PictureBox5.Refresh()
            If (CBool(dstiron.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
                Lb_iron_pot.Text = dstiron.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
            'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"
            'lbelSatGold.Text = "--- --- ----"
            Else
                Lb_iron_pot.Text = ojacpot.GetMontantJackpot(ID_IRON) & " FCFA"
                'Lb_iron_pot.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"
                'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"
                'lbelSatGold.Text = "--- --- ----"
            End If
        End Using
    End Sub
    Delegate Sub dmajforJack()



    Public Sub majSatutforJack(ByRef tes As Integer)
        Using ojacpot As New BL_Lotto.BlJackot(source)
            Dim dstsilver, dstgold, dstiron As New DataSet

            dstsilver = ojacpot.GetJackpot(ID_SILVER)

            If ((ID_TICKET_SILVER = "0") OrElse (tes = 1)) Then

                'lbelSatSilver.Text = ojacpot.GetMontStatutJack(ID_SILVER) & " FCFA"

                If (CBool(dstsilver.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
                    lbelSilver.Text = dstsilver.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
                    'lbelSilver.Text = ojacpot.GetMontStatutJack(ID_SILVER) & " FCFA"
                Else
                    lbelSilver.Text = ojacpot.GetMontantJackpot(ID_SILVER) & " FCFA"
                    'lbelSilver.Text = ojacpot.GetMontStatutJack(ID_SILVER) & " FCFA"
                End If

            Else
                'silpot_lbl.Text = ID_TICKET_SILVER
                lbelSilver.Text = ID_TICKET_SILVER
            End If

            If ((ID_TICKET_IRON = "0") OrElse (tes = 1)) Then

                dstiron = ojacpot.GetJackpot(ID_IRON)

                If (CBool(dstiron.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
                    Lb_iron_pot.Text = dstiron.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
                    'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"

                Else
                    Lb_iron_pot.Text = ojacpot.GetMontantJackpot(ID_IRON) & " FCFA"
                    'Lb_iron_pot.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"
                    'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"

                End If

            Else

                'Lb_iron_pot.Text = ID_TICKET_IRON
            End If

            If ((ID_TICKET_GOLG = "0") OrElse (tes = 1)) Then

                dstgold = ojacpot.GetJackpot(ID_GOLD)

                If (CBool(dstgold.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
                    gpot_lbl.Text = dstgold.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
                    'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"

                Else
                    gpot_lbl.Text = ojacpot.GetMontantJackpot(ID_GOLD) & " FCFA"
                    'lbel_stat_iron.Text = ojacpot.GetMontStatutJack(ID_IRON) & " FCFA"

                End If

            Else

                gpot_lbl.Text = ID_TICKET_GOLG
            End If

        End Using
    End Sub


    Private Sub FrmJeu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Me.SetStyle(ControlStyles.AllPaintingInWmPaint Or ControlStyles.DoubleBuffer Or ControlStyles.ResizeRedraw Or ControlStyles.UserPaint, True)

        ID_TICKET_GOLG = "0"
        ID_TICKET_SILVER = "0"
        ID_TICKET_IRON = "0"
        ID_TICKET_CAGNOTE = "0"
        majforJack()
        'Crée un thread pour traiter ce client et le démarre
        ThreadClient = New Thread(AddressOf GererClient)
        ThreadClient.Start()

        Using oTicketTi As New BL_Lotto.BlTicket(source)
            dsti = oTicketTi.GetLastTirageNum()
            If (dsti.Tables("tirage").Rows.Count > 0) Then
                Tir_No = CInt(dsti.Tables("tirage").Rows(0).Item("NUM_TIRAGE"))
            Else
                Tir_No = 0
            End If
        End Using
        Timer1.Interval = 1000 'Timer1_Tick sera déclenché toutes les secondes.
        Timer1.Start()  'On démarre le Timer

        'initialiseEcartGagnant() 'initialisation au meme moment du delai avant prochain gains
        TirageLancer = 0
        LB_NumTir.Text = Tir_No.ToString : LB_NumTir.Refresh()

        Lb_Min.Text = 5.ToString
        statistirage()
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        'If (Not animaDemarer) Then
        '    min = Math.Truncate(time / 60)
        '    sec = ((time / 60) - (Math.Truncate(time / 60))) * 60
        '    LB_second.Text = CInt(sec).ToString : LB_second.Refresh()
        '    Lb_Min.Text = min.ToString : Lb_Min.Refresh()
        '    time -= 1
        '    DElaiGains -= 1

        '    If (time Mod 10) = 0 Then
        '        majSatutforJack(1)
        '    Else
        '        majSatutforJack(0)
        '    End If
        'End If
    End Sub

    Private Sub Timer1_Tick(ByVal ByValsender As Object, ByVal ByVale As System.EventArgs) Handles Timer1.Tick

        'chronometre
        If (Not animaDemarer) Then
            min = Math.Truncate(time / 60)
            sec = ((time / 60) - (Math.Truncate(time / 60))) * 60
            LB_second.Text = CInt(sec).ToString : LB_second.Refresh()
            Lb_Min.Text = min.ToString : Lb_Min.Refresh()
            time -= 1
            DElaiGains -= 1
            If (time Mod 10) Then
                majSatutforJack(0)
            Else
                majSatutforJack(1)
            End If

            timetosend = time.ToString
        End If

        If (Lb_Min.Text = 0.ToString And LB_second.Text = Delai_demarage_tir.ToString) And (Not animaDemarer) Then
            TirageLancer = 1
        End If

        'la condition est verifié apres 5min et on entre dans la boucle
        If (Lb_Min.Text = 0.ToString And LB_second.Text = (Delai_demarage_tir - 1).ToString) And (Not animaDemarer) Then
            'on initialise la variable de com
            TirageLancer = 1

            ThreadAnim = New Thread(AddressOf GererAnimation)
            ThreadAnim.SetApartmentState(Threading.ApartmentState.STA)
            ThreadAnim.Start()

        End If

    End Sub

    Public Sub enegistrerTirage(ByVal table() As Integer)

        Using oTirage As New BL_Lotto.BlTirage(source)
            oTirage.AjouterTirage(table)
        End Using

    End Sub
    Private Sub GererAnimation()

        Dim indice(4) As Integer
        Dim random As New Random
        Dim d1 As DateTime = DateTime.Now
        Dim table1 As Array
        Dim tabNumeroPrG As Array
        Dim ListTirage(MaxBoul) As Integer
        Dim ListWinTemp(MaxBoul) As Integer
        Dim ListWin(MaxBoul) As Integer
        Dim NumTir As New Integer
        NumTir = CInt(LB_NumTir.Text)
        Dim cpt As Integer
        Dim dsTicket, dsPari, dsPariSolo, dsPariDoublet, dsTickJackMon As DataSet
        Dim Tabdatedebut() As String
        Dim datedebut, datefin As String


        'If (DElaiGains <= 0) Then

        ' il faut un gagnant

        ' on calcul les entrers


        Tabdatedebut = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
        datefin = Tabdatedebut(2) & "/" & Tabdatedebut(1) & "/" & Tabdatedebut(0)

        datedebut = datefin & " " & "00:00:00"
        datefin = datefin & " " & Date.Now.ToLongTimeString()

        Using oCompte As New BL_Lotto.BlCompte(source)
            'datedebut = oCompte.getDateInscription 'pour activer le comptage par 

            'MsgBox("date de debut " & datedebut)
            dsTicket = oCompte.getStatTicket(datedebut, datefin)
            dsPari = oCompte.getStatPari(datedebut, datefin)
            dsPariSolo = oCompte.getStatPariSolo(datedebut, datefin)
            dsPariDoublet = oCompte.getStatPariDoublet(datedebut, datefin)

            If (dsTicket.Tables("ticket").Rows.Count > 0) Then
                'LbNbTicket.Text = dsTicket.Tables("ticket").Rows(0).Item("NB_TICKET").ToString
                If (Not dsPari.Tables("pari").Rows(0).IsNull("TOTALMISE")) Then
                    TotalEntrer = CType(dsPari.Tables("pari").Rows(0).Item("TOTALMISE"), ULong)
                Else
                    TotalEntrer = 0
                End If


                If (Not dsPariSolo.Tables("pari").Rows(0).IsNull("TOTALMISE")) Then
                    TotalEntrerSolo = CType(dsPariSolo.Tables("pari").Rows(0).Item("TOTALMISE"), ULong)
                Else
                    TotalEntrerSolo = 0
                End If


                If (Not dsPariDoublet.Tables("pari").Rows(0).IsNull("TOTALMISE")) Then
                    TotalEntrerDoublet = CType(dsPariDoublet.Tables("pari").Rows(0).Item("TOTALMISE"), ULong)
                Else
                    TotalEntrerDoublet = 0
                End If


            Else
                TotalEntrer = 0
            End If


            'on preleve le pourcentage prevue
            If (TotalEntrer = 0) Then
                table1 = triche(NumTir)
                ListTirage = CType(table1, Integer())
                cpt = table1.Length
            Else

                'on preleve les jackpot
                Dim monPariensolo, monParienDouble As ULong

                Using ojacpot As New BL_Lotto.BlJackot(source)
                    monPariensolo = CULng(ojacpot.GetMontantPariEncour(Tir_No.ToString, "1"))
                    monParienDouble = CULng(ojacpot.GetMontantPariEncour(Tir_No.ToString, "2"))
                End Using

                'silver jackpot
                prelSilverpotSolo = CType((SILVER_POT / 100) * monPariensolo, ULong)
                prelSilverpotDoub = CType((SILVER_POT / 100) * monParienDouble, ULong)
                prelSilverpot = prelSilverpotSolo + prelSilverpotDoub

                'Gold jacpot
                prelevGoldpotSolo = CType((GOLD_POT / 100) * monPariensolo, ULong)
                prelevGoldpotDoubl = CType((GOLD_POT / 100) * monParienDouble, ULong)
                prelevGoldpot = prelevGoldpotSolo + prelevGoldpotDoubl

                'iron jacpot
                prelevIronpotSolo = CType((IRON_POT / 100) * monPariensolo, ULong)
                prelevIronpotDoubl = CType((IRON_POT / 100) * monParienDouble, ULong)
                prelevIrondpot = prelevIronpotSolo + prelevGoldpotDoubl

                'cagnote

                Using ojacpot As New BL_Lotto.BlJackot(source)
                    ojacpot.Maj_Satut_Jackpot(ID_SILVER, prelSilverpot.ToString)
                    ojacpot.Maj_Satut_Jackpot(ID_GOLD, prelevGoldpot.ToString)
                    ojacpot.Maj_Satut_Jackpot(ID_IRON, prelevIrondpot.ToString)
                End Using

                Dim POURVENTAGE_PRELEVER As Integer = DIFICULT_LEVEL + SILVER_POT + GOLD_POT + IRON_POT

                TotalBenef = CType((POURVENTAGE_PRELEVER / 100) * TotalEntrer, ULong)
                TotalBenefSolo = CType((POURVENTAGE_PRELEVER / 100) * TotalEntrerSolo, ULong)
                TotalBenefDoublet = CType((POURVENTAGE_PRELEVER / 100) * TotalEntrerDoublet, ULong)

                Dim resteAgagner As Long = CLng(TotalEntrer - TotalBenef)
                Dim resteAgagnerSolo As Long = CLng(TotalEntrerSolo - TotalBenefSolo)
                Dim resteAgagnerDoublet As Long = CLng(TotalEntrerDoublet - TotalBenefDoublet)


                ' le reste on retire le montant des tickets deja gagnants ensuite
                GainsActuelClient = MontantGagnerActuel(datedebut, datefin)
                GainsSolo = GainsActuelSolo(datedebut, datefin)
                GainsDoublet = GainsActuelDoublet(datedebut, datefin)

                ' le reste on retire le montant des tickets deja gagnants du jackpot
                dsTickJackMon = oCompte.getStatTickJackMon(datedebut, datefin)
                If (dsTickJackMon.Tables("ticket").Rows.Count > 0) Then
                    If (Not dsTickJackMon.Tables("ticket").Rows(0).IsNull("TOTAL_MONTANT")) Then
                        TotalmontantJack = CULng(dsTickJackMon.Tables("ticket").Rows(0).Item("TOTAL_MONTANT"))
                    Else
                        TotalmontantJack = 0
                    End If
                End If



                resteAgagner = resteAgagner - CLng(GainsActuelClient) - CLng(TotalmontantJack)
                'resteAgagner = resteAgagner - CLng(GainsActuelClient)

                resteAgagnerSolo = resteAgagnerSolo - CLng(GainsSolo)

                resteAgagnerSolo = resteAgagnerSolo - CLng(TotalmontantJack)

                resteAgagnerDoublet = resteAgagnerDoublet - CLng(GainsDoublet)

                'MsgBox(resteAgagnerSolo & " pour le solo")


                ' on preleve les tickets succeptibles de gagner
                Dim tabperdant, tabgagnant, totalnum, totalnumper, soustra As New List(Of Integer)
                Dim tabperdantSolo, tabgagnantSolo As New List(Of Integer)
                Dim tabperdantDoublet, tabgagnantDoublet As New List(Of Integer)

                'tabgagnant = ScinderGagnant(resteAgagner, datedebut, datefin, tabperdant)
                tabgagnantSolo = ScinderGagnantSolo(resteAgagnerSolo, datedebut, datefin, tabperdantSolo)
                tabgagnantDoublet = ScinderGagnantDoublet(resteAgagnerDoublet, datedebut, datefin, tabperdantDoublet)
                'MsgBox("gagnant solo" & tabgagnantSolo.Count & " perdant " & tabperdantSolo.Count)
                'fusionner les tableau gagnant
                tabgagnant = tabgagnantSolo

                For j As Integer = 0 To tabgagnantDoublet.Count - 1
                    tabgagnant.Add(tabgagnantDoublet.Item(j))
                Next
                'MsgBox("tableau gagnant solo " & tabgagnantSolo.Count & " tableau gagnant general " & tabgagnant.Count & " reste a gagner " & resteAgagnerSolo)


                'fusionner les tableau de perdant
                tabperdant = tabperdantSolo
                For j As Integer = 0 To tabperdantDoublet.Count - 1
                    tabperdant.Add(tabperdantDoublet.Item(j))
                Next

                table1 = getNumBerGagnant(tabperdant)

                tabNumeroPrG = NumBerGagnant(tabgagnant)

                'MsgBox(" nb gagnant perdant " & table1.Length)

                ListWin = CType(tabNumeroPrG, Integer())
                ListWinTemp = CType(table1, Integer())
                totalnum.Clear()

                For h As Integer = 0 To ListWinTemp.Length - 1
                    totalnumper.Add(ListWinTemp(h))
                Next
                totalnumper = totalnumper.Distinct.ToList

                For h As Integer = 0 To ListWin.Length - 1
                    totalnum.Add(ListWin(h))
                Next
                totalnum = totalnum.Distinct.ToList
                'on doit verifier qu'il ya pas un doublet entre perdant et gagnant
                soustra.Clear()
                For Each z As Integer In totalnum
                    If (totalnumper.IndexOf(z) = -1) Then
                        soustra.Add(z)
                    End If
                Next
                For Each z As Integer In soustra
                    totalnum.Remove(z)
                Next

                'MsgBox(totalnum.Count)

                If totalnum.Count < 5 Then
                    Dim q() As Integer
                    Dim aqt As Integer
                    'MsgBox(" avant d'entrer" & ListWinTemp.Length)
                    Do
                        cpt = ListWinTemp.Length
                        'If (totalnum.Count = 5) Then
                        '    aqt = 7 - ListWin.Length
                        'Else
                        '    aqt = 6 - ListWin.Length
                        'End If
                        aqt = 5 - totalnum.Count

                        q = GetRandomDistinct(aqt, cpt)
                        'MsgBox("taille " & q.Length)
                        For j As Integer = 0 To aqt - 1
                            If (q(j) < ListWinTemp.Length) AndAlso totalnum.IndexOf(ListWinTemp(q(j))) = -1 Then
                                totalnum.Add(ListWinTemp(q(j)))
                            End If
                        Next

                    Loop Until totalnum.Count = 5

                End If
                'MsgBox("dans le for " & totalnum.Count)
                table1 = totalnum.ToArray
            End If
        End Using

        ' on prend 1 ou plusieurs tickets qu'on fait gagner
        ' le reste on fait pedre
        'initialiseEcartGagnant()
        'Else
        '    'on continue a bouffe l'argent

        '    table1 = triche(NumTir)

        'End If


        ListTirage = CType(table1, Integer())
        cpt = table1.Length


        'tirage 50% de chance
        'MsgBox(indice.Length)
        indice = GetRandomDistinct(5, cpt)
        For i As Integer = 0 To indice.Length - 1
            Resultat(i) = ListTirage(indice(i))
        Next

        'enregistrement des tirage dans la base de donnee
        enegistrerTirage(Resultat)


        'pour controler qu'on demare bien a 0
        If (CInt(LB_second.Text) > 0) Then
            Thread.Sleep(CInt(LB_second.Text) * 1000)
        End If

        animaDemarer = True

        If animaDemarer Then
            Me.Invoke(New dAnimation(AddressOf Animantion), Resultat)

            'verification des jackpot

            Using oTicketTi As New BL_Lotto.BlTicket(source)
                dsti = oTicketTi.GetLastTirageNum()
                If (dsti.Tables("tirage").Rows.Count > 0) Then
                    Tir_cour_jack = CInt(dsti.Tables("tirage").Rows(0).Item("NUM_TIRAGE"))
                Else
                    Tir_cour_jack = 1
                End If
            End Using


            Using ojacpot As New BL_Lotto.BlJackot(source)
                If (ojacpot.verifWinnerJackpot(ID_SILVER, Tir_cour_jack.ToString, ID_TICKET_SILVER)) Then
                    If (ID_TICKET_SILVER <> "0") Then
                        Dim myScreens() As Screen = Screen.AllScreens
                        Dim FomCongrat As New jackpotgold

                        Dim dstsilver As DataSet = ojacpot.GetJackpot(ID_SILVER)

                        FomCongrat.lbtitrejack.Text = dstsilver.Tables("jackpot").Rows(0).Item("nom_jackp").ToString.ToUpper
                        FomCongrat.lbelNumtick.Text = ID_TICKET_SILVER
                        FomCongrat.videofile = "vid.avi"
                        If (CBool(dstsilver.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
                            FomCongrat.lbeljackpot.Text = dstsilver.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
                        Else
                            FomCongrat.lbeljackpot.Text = ojacpot.GetMontantJackpot(ID_SILVER) & " FCFA"
                        End If
                        FomCongrat.pb_photo_lot.ImageLocation = Application.StartupPath & "\photo\" & dstsilver.Tables("jackpot").Rows(0).Item("photo").ToString
                        FomCongrat.pb_photo_lot.Refresh()

                        'aryForms[currentPointer] = New frmLaunchedWindow();
                        'aryForms[currentPointer].Text =aryForms[currentPointer].Text + currentPointer;
                        FomCongrat.Left = myScreens(CInt(SCREEN_SELECT)).Bounds.Width

                        FomCongrat.Top = myScreens(CInt(SCREEN_SELECT)).Bounds.Height

                        FomCongrat.StartPosition = FormStartPosition.Manual

                        FomCongrat.Location = myScreens(CInt(SCREEN_SELECT)).Bounds.Location
                        'Point p = New Point(scr[0].Bounds.Location.X, scr[0].Bounds.Location.Y);
                        'aryForms[currentPointer].Location = p;
                        FomCongrat.Show() : FomCongrat.Refresh()

                        Thread.Sleep(Delai_anim_jackpot)
                        FomCongrat.Close()
                        select_another_lot(ID_SILVER, True)
                        'Me.BeginInvoke(New Action(Sub() FomCongrat.Close()))
                    End If


                End If


                If (ojacpot.verifWinnerJackpot(ID_IRON, Tir_cour_jack.ToString, ID_TICKET_IRON)) Then
                    If (ID_TICKET_IRON <> "0") Then
                        Dim myScreens() As Screen = Screen.AllScreens
                        Dim FomCongrat As New jackpotgold

                        Dim dstbronze As DataSet = ojacpot.GetJackpot(ID_IRON)

                        FomCongrat.lbtitrejack.Text = dstbronze.Tables("jackpot").Rows(0).Item("nom_jackp").ToString.ToUpper
                        FomCongrat.lbelNumtick.Text = ID_TICKET_IRON
                        FomCongrat.videofile = "vid2.avi"
                        If (CBool(dstbronze.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
                            FomCongrat.lbeljackpot.Text = dstbronze.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
                        Else
                            FomCongrat.lbeljackpot.Text = ojacpot.GetMontantJackpot(ID_IRON) & " FCFA"
                        End If
                        FomCongrat.pb_photo_lot.ImageLocation = Application.StartupPath & "\photo\" & dstbronze.Tables("jackpot").Rows(0).Item("photo").ToString
                        FomCongrat.pb_photo_lot.Refresh()

                        'aryForms[currentPointer] = New frmLaunchedWindow();
                        'aryForms[currentPointer].Text =aryForms[currentPointer].Text + currentPointer;
                        FomCongrat.Left = myScreens(CInt(SCREEN_SELECT)).Bounds.Width

                        FomCongrat.Top = myScreens(CInt(SCREEN_SELECT)).Bounds.Height

                        FomCongrat.StartPosition = FormStartPosition.Manual

                        FomCongrat.Location = myScreens(CInt(SCREEN_SELECT)).Bounds.Location
                        'Point p = New Point(scr[0].Bounds.Location.X, scr[0].Bounds.Location.Y);
                        'aryForms[currentPointer].Location = p;
                        FomCongrat.Show() : FomCongrat.Refresh()

                        Thread.Sleep(Delai_anim_jackpot)
                        FomCongrat.Close()
                        select_another_lot(ID_IRON)
                        'Me.BeginInvoke(New Action(Sub() FomCongrat.Close()))
                    End If


                End If

                If (ojacpot.verifWinnerJackpot(ID_GOLD, Tir_cour_jack.ToString, ID_TICKET_GOLG)) Then
                    If (ID_TICKET_GOLG <> "0") Then
                        Dim FomCongrat As New jackpotgold
                        Dim myScreens() As Screen = Screen.AllScreens

                        'FomCongrat.lbtitrejack.Text = "GOLD JACKPOT"

                        Dim dstsilver As DataSet = ojacpot.GetJackpot(ID_GOLD)

                        FomCongrat.lbtitrejack.Text = dstsilver.Tables("jackpot").Rows(0).Item("nom_jackp").ToString.ToUpper
                        FomCongrat.lbelNumtick.Text = ID_TICKET_GOLG
                        FomCongrat.videofile = "vid1.avi"

                        If (CBool(dstsilver.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
                            FomCongrat.lbeljackpot.Text = dstsilver.Tables("jackpot").Rows(0).Item("nom_materiel").ToString
                        Else
                            FomCongrat.lbeljackpot.Text = ojacpot.GetMontantJackpot(ID_GOLD) & " FCFA"
                        End If
                        FomCongrat.pb_photo_lot.ImageLocation = Application.StartupPath & "\photo\" & dstsilver.Tables("jackpot").Rows(0).Item("photo").ToString
                        FomCongrat.pb_photo_lot.Refresh()

                        FomCongrat.Left = myScreens(CInt(SCREEN_SELECT)).Bounds.Width

                        FomCongrat.Top = myScreens(CInt(SCREEN_SELECT)).Bounds.Height

                        FomCongrat.StartPosition = FormStartPosition.Manual

                        FomCongrat.Location = myScreens(CInt(SCREEN_SELECT)).Bounds.Location
                        'Point p = New Point(scr[0].Bounds.Location.X, scr[0].Bounds.Location.Y);
                        'aryForms[currentPointer].Location = p;
                        FomCongrat.Show() : FomCongrat.Refresh()


                        Thread.Sleep(Delai_anim_jackpot)
                        FomCongrat.Close()
                        select_another_lot(ID_GOLD)
                    End If


                End If
                Me.Invoke(New dmajforJack(AddressOf majforJack))
                'majforJack()

            End Using
            animaDemarer = False

        End If

        ' c'est ici qu'on devrai modifier le picture box

    End Sub
    Delegate Sub dAnimation(ByVal Resultat() As Integer)


    Public Sub Animantion(ByVal Resultat() As Integer)

        Tir_No += 1
        LB_NumTir.Text = Tir_No.ToString : LB_NumTir.Refresh()

        Dim forAnim As New FormAnim
        Dim myScreens() As Screen = Screen.AllScreens

        FormAnim.LB_NumTir.Text = LB_NumTir.Text

        FormAnim.Left = myScreens(CInt(SCREEN_SELECT)).Bounds.Width

        FormAnim.Top = myScreens(CInt(SCREEN_SELECT)).Bounds.Height

        'FormAnim.StartPosition = FormStartPosition.CenterParent
        FormAnim.StartPosition = FormStartPosition.Manual

        FormAnim.Location = myScreens(CInt(SCREEN_SELECT)).Bounds.Location

        FormAnim.ShowDialog()

        'Thread.Sleep(3000)
        FormAnim.Hide()


        'Dim d1 As DateTime = DateTime.Now

        'While DateTime.Now < d1.AddSeconds(DureeAnim)
        '    'on fait defiler les nombres
        '    While DateTime.Now < d1.AddSeconds(22)
        '        While DateTime.Now < d1.AddSeconds(17)
        '            While DateTime.Now < d1.AddSeconds(12)
        '                While DateTime.Now < d1.AddSeconds(7)

        '                    Randomize()
        '                    num1.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num1.Refresh()
        '                    Randomize()
        '                    num2.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num2.Refresh()
        '                    Randomize()
        '                    num3.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num3.Refresh()
        '                    Randomize()
        '                    num4.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num4.Refresh()
        '                    Randomize()
        '                    num5.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num5.Refresh()
        '                    Application.DoEvents()
        '                    Thread.Sleep(100)
        '                End While
        '                'on affiche le 1er nombre
        '                num1.Text = Resultat(0).ToString : num1.Refresh()
        '                'on fait defiler les 4 deniers
        '                Randomize()
        '                num2.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num2.Refresh()
        '                Randomize()
        '                num3.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num3.Refresh()
        '                Randomize()
        '                num4.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num4.Refresh()
        '                Randomize()
        '                num5.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num5.Refresh()
        '                Application.DoEvents()
        '                Thread.Sleep(100)

        '            End While
        '            'on affiche le 2eme nombres
        '            num2.Text = Resultat(1).ToString : num2.Refresh()
        '            'on fait defiler les 3 derniers
        '            Randomize()
        '            num3.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num3.Refresh()
        '            Randomize()
        '            num4.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num4.Refresh()
        '            Randomize()
        '            num5.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num5.Refresh()
        '            Application.DoEvents()
        '            Thread.Sleep(100)
        '        End While
        '        'on affiche le 3eme nombres
        '        num3.Text = Resultat(2).ToString : num3.Refresh()
        '        'on fait defiler le 2 derniers
        '        Randomize()
        '        num4.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num4.Refresh()
        '        Randomize()
        '        num5.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num5.Refresh()
        '        Application.DoEvents()
        '        Thread.Sleep(100)
        '    End While
        '    'on affiche le 4eme nombres
        '    num4.Text = Resultat(3).ToString : num4.Refresh()
        '    'on fait defiler le dernier

        '    Randomize()
        '    num5.Text = CInt(Int((MaxBoul * Rnd()) + 1)).ToString : num5.Refresh()
        '    Application.DoEvents()
        '    Thread.Sleep(100)
        'End While
        'on affiche le dernier nombre
        'num5.Text = Resultat(4).ToString : num5.Refresh()


        'LB_DT_51.Text = LB_DT_41.Text
        'LB_DT_52.Text = LB_DT_42.Text
        'LB_DT_53.Text = LB_DT_43.Text
        'LB_DT_54.Text = LB_DT_44.Text
        'LB_DT_55.Text = LB_DT_45.Text
        'LB_DT_5.Text = LB_DT_4.Text

        'LB_DT_41.Text = LB_DT_31.Text
        'LB_DT_42.Text = LB_DT_32.Text
        'LB_DT_43.Text = LB_DT_33.Text
        'LB_DT_44.Text = LB_DT_34.Text
        'LB_DT_45.Text = LB_DT_35.Text
        'LB_DT_4.Text = LB_DT_3.Text

        'LB_DT_31.Text = LB_DT_21.Text
        'LB_DT_32.Text = LB_DT_22.Text
        'LB_DT_33.Text = LB_DT_23.Text
        'LB_DT_34.Text = LB_DT_24.Text
        'LB_DT_35.Text = LB_DT_25.Text
        'LB_DT_3.Text = LB_DT_2.Text

        'LB_DT_21.Text = LB_DT_11.Text
        'LB_DT_22.Text = LB_DT_12.Text
        'LB_DT_23.Text = LB_DT_13.Text
        'LB_DT_24.Text = LB_DT_14.Text
        'LB_DT_25.Text = LB_DT_15.Text
        'LB_DT_2.Text = LB_DT_1.Text

        'LB_DT_1.Text = Label1.Text & " " & LB_NumTir.Text
        'LB_DT_11.Text = num1.Text
        'LB_DT_12.Text = num2.Text
        'LB_DT_13.Text = num3.Text
        'LB_DT_14.Text = num4.Text
        'LB_DT_15.Text = num5.Text
        statistirage()
        time = Delai_JEu

        TirageLancer = 0


    End Sub

    'Fonction et procedure



    Private Function GetRandomDistinctImpair(ByVal tailleVoulue As Integer, ByVal cpt As Integer) As Integer()
        Dim nombres As New List(Of Integer)()

        Do
            Do
                'Dim rand As New Random()
                'nombres.Add(rand.Next(1, cpt))
                Randomize()
                nombres.Add(CInt(Int(cpt * Rnd())))
            Loop While nombres.Count <> tailleVoulue
            nombres = nombres.Distinct().ToList()
        Loop While nombres.Count <> tailleVoulue
        Return nombres.ToArray()
    End Function

    Private Function triche(ByVal NumTir As Integer) As Array
        Using oBltriche As New BL_Lotto.BlTriche(source)
            Dim NbTriche As New List(Of Integer)()
            Dim Dt As DataSet
            Dim N As Integer = MaxBoul - 1
            Dim ListTirage As New List(Of Integer)()
            Dim nbmax As Integer = 0

            'Dim porcentage As Double

            Dt = oBltriche.RecupNumJouer(NumTir)

            For i = 0 To MaxBoul - 1
                ListTirage.Add(i + 1)
            Next

            'apres avoir recuperer les numero jouer dans base de donnée, on conserve que la moitié de ses numeros
            If (Dt.Tables("numero").Rows.Count > 0) Then

                'Tricherie 50% de chance

                nbmax = CInt(Dt.Tables("numero").Rows.Count)

                If nbmax > 0 Then
                    'If (nbmax = MaxBoul) Then
                    '    ' si il arrive que 90 personne on parier sur 90 nombre differents on laisse passeer 90 %
                    '    porcentage = 93 / 100
                    '    nbmax = CInt(Math.Truncate(Dt.Tables("numero").Rows.Count * porcentage))

                    'End If
                    If (nbmax > (MaxBoul - NbBoulTirag)) Then
                        ' si il arrive que 90 personne on parier sur 90 nombre differents on laisse passeer 90 %
                        'Dim porcentage As Double = 93 / 100
                        nbmax = MaxBoul - NbBoulTirag
                    End If

                    For i = 0 To nbmax - 1
                        NbTriche.Add(CInt(Dt.Tables("numero").Rows(i).Item("NUMEROS")))
                    Next

                End If

                'ici on retire la moitié des nombres jouer de la liste de tirage (1-90)
                For i = 0 To NbTriche.Count - 1
                    ListTirage.Remove(NbTriche(i))
                Next
            End If
            Return ListTirage.ToArray()
        End Using

    End Function

    Public Function MontantGagnerActuel(ByVal datedebut As String, ByVal datefin As String) As ULong
        Dim dsTouticket, ODsPari As DataSet
        'Dim oDsNumJou As DataSet
        Dim MontantSortie, nbticketGagnant, Gains, Tir_Noa As Integer
        Dim id_ticket As String
        Dim Gagner As Boolean
        nbticketGagnant = 0
        MontantSortie = 0

        Using oTicketTi As New BL_Lotto.BlTicket(source)
            dsti = oTicketTi.GetLastTirageNum()
            If (dsti.Tables("tirage").Rows.Count > 0) Then
                Tir_Noa = CInt(dsti.Tables("tirage").Rows(0).Item("NUM_TIRAGE")) + 1
            Else
                Tir_Noa = 1
            End If
        End Using

        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTouticket = oCompte.getListTicket(datedebut, datefin)

            Using oTicket As New BL_Lotto.BlTicket(source)
                For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1

                    ' on teste si le ticket fait partie des ticket en cour
                    If (Tir_Noa = CInt(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE"))) Then
                        Continue For
                    End If

                    id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                    ODsPari = oTicket.GetPari(id_ticket)
                    Gagner = False
                    Gains = 0
                    'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then

                        For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                            If (oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString, _
                                                                            dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)) Then
                                Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                            End If

                            Gagner = Gagner Or oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString, _
                                                                        dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)
                        Next

                    End If

                    'oDsNumJou = oTicket.getNumJouer(id_ticket)
                    If (Gagner) Then
                        nbticketGagnant += 1
                        MontantSortie += CInt(Gains)
                    End If
                Next
            End Using
        End Using
        Return CType(MontantSortie, ULong)
    End Function

    Public Function GainsActuelSolo(ByVal datedebut As String, ByVal datefin As String) As ULong
        Dim dsTouticket, ODsPari As DataSet
        'Dim oDsNumJou As DataSet
        Dim MontantSortie, nbticketGagnant, Gains, Tir_Noa As Integer
        Dim id_ticket As String
        Dim Gagner As Boolean
        nbticketGagnant = 0
        MontantSortie = 0



        Using oTicketTi As New BL_Lotto.BlTicket(source)
            dsti = oTicketTi.GetLastTirageNum()
            If (dsti.Tables("tirage").Rows.Count > 0) Then
                Tir_Noa = CInt(dsti.Tables("tirage").Rows(0).Item("NUM_TIRAGE")) + 1
            Else
                Tir_Noa = 1
            End If
        End Using

        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTouticket = oCompte.getListTicket(datedebut, datefin)

            Using oTicket As New BL_Lotto.BlTicket(source)
                For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1

                    If (Tir_Noa = CInt(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE"))) Then
                        Continue For
                    End If

                    id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                    ODsPari = oTicket.GetPariParMode(id_ticket, "1")
                    Gagner = False
                    Gains = 0
                    'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then

                        For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                            If (oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString, _
                                                                            dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)) Then
                                Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                            End If


                            Gagner = Gagner Or oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString, _
                                                                        dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                        Next

                    End If

                    'oDsNumJou = oTicket.getNumJouer(id_ticket)
                    If (Gagner) Then
                        nbticketGagnant += 1
                        MontantSortie += CInt(Gains)
                    End If
                Next
            End Using
        End Using
        Return CType(MontantSortie, ULong)
    End Function


    Public Function GainsActuelDoublet(ByVal datedebut As String, ByVal datefin As String) As ULong
        Dim dsTouticket, ODsPari As DataSet
        'Dim oDsNumJou As DataSet
        Dim MontantSortie, nbticketGagnant, Gains, Tir_Noa As Integer
        Dim id_ticket As String
        Dim Gagner As Boolean
        nbticketGagnant = 0
        MontantSortie = 0

        Using oTicketTi As New BL_Lotto.BlTicket(source)
            dsti = oTicketTi.GetLastTirageNum()
            If (dsti.Tables("tirage").Rows.Count > 0) Then
                Tir_Noa = CInt(dsti.Tables("tirage").Rows(0).Item("NUM_TIRAGE")) + 1
            Else
                Tir_Noa = 1
            End If
        End Using

        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTouticket = oCompte.getListTicket(datedebut, datefin)

            Using oTicket As New BL_Lotto.BlTicket(source)
                For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1


                    If (Tir_Noa = CInt(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE"))) Then
                        Continue For
                    End If

                    id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                    ODsPari = oTicket.GetPariParMode(id_ticket, "2")
                    Gagner = False
                    Gains = 0
                    'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then

                        For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                            If (oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString, _
                                                                            dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)) Then
                                Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                            End If


                            Gagner = Gagner Or oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString, _
                                                                        dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                        Next

                    End If

                    'oDsNumJou = oTicket.getNumJouer(id_ticket)
                    If (Gagner) Then
                        nbticketGagnant += 1
                        MontantSortie += CInt(Gains)
                    End If
                Next
            End Using
        End Using
        Return CType(MontantSortie, ULong)
    End Function

    Public Function ScinderGagnant(ByVal montantdispo As ULong, ByVal datedebut As String, ByVal datefin As String, ByRef TabPerdant As List(Of Integer)) As List(Of Integer)
        Dim perdant, gagnat As New List(Of Integer)
        Dim dsTouticket, ODsPari As DataSet
        Dim MontantSortie, nbticketGagnant, Gains As Integer
        Dim id_ticket As String
        Dim Gagner As Boolean
        Dim Tir_Noa As Integer
        nbticketGagnant = 0
        MontantSortie = 0

        Using oTicketTi As New BL_Lotto.BlTicket(source)
            dsti = oTicketTi.GetLastTirageNum()
            If (dsti.Tables("tirage").Rows.Count > 0) Then
                Tir_Noa = CInt(dsti.Tables("tirage").Rows(0).Item("NUM_TIRAGE")) + 1
            Else
                Tir_Noa = 1
            End If
        End Using


        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTouticket = oCompte.getListTicketTirag(datedebut, datefin, Tir_Noa.ToString)

            Using oTicket As New BL_Lotto.BlTicket(source)
                For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
                    id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                    ODsPari = oTicket.GetPari(id_ticket)
                    Gagner = False
                    Gains = 0
                    'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then

                        For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1

                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                            Gains = (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                            If (Gains < montantdispo) Then
                                montantdispo = CULng(montantdispo - Gains)

                                'gagnat.Add({CInt(id_ticket), CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI"))})
                                gagnat.Add(CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI")))
                            Else
                                perdant.Add(CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI")))
                            End If
                        Next

                    End If
                Next
            End Using
            TabPerdant = perdant
            Return gagnat
        End Using
    End Function


    Public Function ScinderGagnantSolo(ByVal montantdispo As Long, ByVal datedebut As String, ByVal datefin As String, ByRef TabPerdant As List(Of Integer)) As List(Of Integer)
        Dim perdant, gagnat, parilist As New List(Of Integer)
        Dim dsTouticket, ODsPari As DataSet
        Dim MontantSortie, nbticketGagnant, Gains As Integer
        Dim id_ticket As String
        Dim Gagner As Boolean
        Dim MONTANTDISPON As Long = montantdispo
        Dim Tir_Noa As Integer
        nbticketGagnant = 0
        MontantSortie = 0

        Using oTicketTi As New BL_Lotto.BlTicket(source)
            dsti = oTicketTi.GetLastTirageNum()
            If (dsti.Tables("tirage").Rows.Count > 0) Then
                Tir_Noa = CInt(dsti.Tables("tirage").Rows(0).Item("NUM_TIRAGE")) + 1
            Else
                Tir_Noa = 1
            End If
        End Using

        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTouticket = oCompte.getListTicketTirag(datedebut, datefin, Tir_Noa.ToString)

            Using oTicket As New BL_Lotto.BlTicket(source)
                For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
                    id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString
                    ODsPari = oTicket.GetPariParMode(id_ticket, "1")
                    Gagner = False
                    Gains = 0
                    'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then

                        For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                            parilist.Add(CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI")))
                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                            'Gains = (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                            'If (Gains < MONTANTDISPON) Then
                            '    MONTANTDISPON = CULng(MONTANTDISPON - Gains)

                            '    'gagnat.Add({CInt(id_ticket), CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI"))})
                            '    gagnat.Add(CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI")))
                            'Else
                            '    perdant.Add(CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI")))
                            'End If
                        Next

                    End If
                Next
                If (parilist.Count > 0) Then
                    Dim tempon As List(Of Integer) = parilist
                    Dim ODsetPari As DataSet
                    Dim nb As Integer
                    While parilist.Count > 0
                        Dim rand As New Random()

                        nb = rand.Next(0, parilist.Count)

                        ODsetPari = oTicket.GetUnPari(parilist.Item(nb).ToString)

                        If (ODsetPari.Tables("pari").Rows.Count > 0) Then

                            Gains = (CInt(ODsetPari.Tables("pari").Rows(0).Item("MONTANT_MISE")) * CInt(ODsetPari.Tables("pari").Rows(0).Item("QUOTA")))

                            If (Gains < MONTANTDISPON) Then
                                MONTANTDISPON = MONTANTDISPON - CLng(Gains)

                                'gagnat.Add({CInt(id_ticket), CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI"))})
                                gagnat.Add(CInt(ODsetPari.Tables("pari").Rows(0).Item("ID_PARI")))
                            Else
                                perdant.Add(CInt(ODsetPari.Tables("pari").Rows(0).Item("ID_PARI")))
                            End If
                        End If
                        parilist.RemoveAt(nb)

                    End While
                End If


            End Using
            TabPerdant = perdant
            Return gagnat
        End Using
    End Function

    Public Function ScinderGagnantDoublet(ByVal montantdispo As Long, ByVal datedebut As String, ByVal datefin As String, ByRef TabPerdant As List(Of Integer)) As List(Of Integer)
        Dim perdant, gagnat, parilist As New List(Of Integer)
        Dim dsTouticket, ODsPari As DataSet
        Dim MontantSortie, nbticketGagnant, Gains As Integer
        Dim id_ticket As String
        Dim Gagner As Boolean
        Dim Tir_Noa As Integer
        Dim MONTANTDISPON As Long = montantdispo
        nbticketGagnant = 0
        MontantSortie = 0

        Using oTicketTi As New BL_Lotto.BlTicket(source)
            dsti = oTicketTi.GetLastTirageNum()
            If (dsti.Tables("tirage").Rows.Count > 0) Then
                Tir_Noa = CInt(dsti.Tables("tirage").Rows(0).Item("NUM_TIRAGE")) + 1
            Else
                Tir_Noa = 1
            End If
        End Using


        Using oCompte As New BL_Lotto.BlCompte(source)

            dsTouticket = oCompte.getListTicketTirag(datedebut, datefin, Tir_Noa.ToString)

            Using oTicket As New BL_Lotto.BlTicket(source)
                For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
                    id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                    ODsPari = oTicket.GetPariParMode(id_ticket, "2")
                    Gagner = False
                    Gains = 0
                    'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                    If (ODsPari.Tables("pari").Rows.Count > 0) Then

                        For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1


                            parilist.Add(CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI")))



                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                            'Gains = (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                            'If (Gains < MONTANTDISPON) Then
                            '    MONTANTDISPON = MONTANTDISPON - CLng(Gains)

                            '    'gagnat.Add({CInt(id_ticket), CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI"))})
                            '    gagnat.Add(CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI")))
                            'Else
                            '    perdant.Add(CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI")))
                            'End If
                        Next

                    End If
                Next

                If (parilist.Count > 0) Then
                    Dim tempon As List(Of Integer) = parilist
                    Dim ODsetPari As DataSet
                    Dim nb As Integer
                    While parilist.Count > 0
                        Dim rand As New Random()

                        nb = rand.Next(0, parilist.Count)

                        ODsetPari = oTicket.GetUnPari(parilist.Item(nb).ToString)

                        If (ODsetPari.Tables("pari").Rows.Count > 0) Then

                            Gains = (CInt(ODsetPari.Tables("pari").Rows(0).Item("MONTANT_MISE")) * CInt(ODsetPari.Tables("pari").Rows(0).Item("QUOTA")))

                            If (Gains < MONTANTDISPON) Then
                                MONTANTDISPON = MONTANTDISPON - CLng(Gains)

                                'gagnat.Add({CInt(id_ticket), CInt(ODsPari.Tables("pari").Rows(k).Item("ID_PARI"))})
                                gagnat.Add(CInt(ODsetPari.Tables("pari").Rows(0).Item("ID_PARI")))
                            Else
                                perdant.Add(CInt(ODsetPari.Tables("pari").Rows(0).Item("ID_PARI")))
                            End If
                        End If
                        parilist.RemoveAt(nb)

                    End While
                End If


            End Using
            TabPerdant = perdant
            Return gagnat
        End Using
    End Function


    Public Function getNumBerGagnant(ByVal perdant As List(Of Integer)) As Integer()
        Using oBltriche As New BL_Lotto.BlTriche(source)
            Dim NbTriche As New List(Of Integer)()
            Dim NbPerdant As New List(Of Integer)
            Dim oDsNumJou As DataSet
            'Dim N As Integer = MaxBoul - 1
            Dim ListTirage As New List(Of Integer)()
            Dim temp As Integer
            Dim nbmax As Integer = 0
            Dim nb As Integer = 0
            Dim trouve As Boolean = False

            For i = 0 To MaxBoul - 1
                ListTirage.Add(i + 1)
            Next

            'apres avoir recuperer les numero jouer dans base de donnée, on conserve que la moitié de ses numeros
            Using oTicket As New BL_Lotto.BlTicket(source)
                For j As Integer = 0 To perdant.Count - 1
                    temp = perdant.Item(j)
                    oDsNumJou = oTicket.getNumJouer(temp.ToString)

                    If (oDsNumJou.Tables("avoir").Rows.Count > 0) Then

                        ' si le pari est un doublet il suffit de retirer un des 
                        If (oDsNumJou.Tables("avoir").Rows.Count = 1) Then

                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)
                            If NbPerdant.IndexOf(CInt(oDsNumJou.Tables("avoir").Rows(0).Item("NUMEROS"))) = -1 Then
                                NbPerdant.Add(CInt(oDsNumJou.Tables("avoir").Rows(0).Item("NUMEROS")))
                            End If
                        Else
                            nb = 0
                            trouve = False
                            While (Not trouve) And (nb < oDsNumJou.Tables("avoir").Rows.Count)
                                'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)
                                If NbPerdant.IndexOf(CInt(oDsNumJou.Tables("avoir").Rows(nb).Item("NUMEROS"))) = -1 Then
                                    NbPerdant.Add(CInt(oDsNumJou.Tables("avoir").Rows(nb).Item("NUMEROS")))

                                End If
                                trouve = True
                                nb += 1
                            End While
                        End If


                    End If

                Next
            End Using

            nbmax = NbPerdant.Count


            If (nbmax > (MaxBoul - NbBoulTirag)) Then
                ' si il arrive que 90 personne on parier sur 90 nombre differents on laisse passeer 90 %
                'Dim porcentage As Double = 93 / 100
                nbmax = MaxBoul - NbBoulTirag
            End If

            For i = 0 To nbmax - 1
                NbTriche.Add(CInt(NbPerdant.Item(i)))
            Next



            'ici on retire la moitié des nombres jouer de la liste de tirage (1-90)
            For i = 0 To NbTriche.Count - 1
                ListTirage.Remove(NbTriche.Item(i))
            Next

            Return ListTirage.ToArray()
        End Using
    End Function


    Public Function NumBerGagnant(ByVal gagnant As List(Of Integer)) As Integer()
        Using oBltriche As New BL_Lotto.BlTriche(source)
            Dim NbTriche As New List(Of Integer)()
            Dim NbPerdant As New List(Of Integer)
            Dim oDsNumJou As DataSet
            'Dim N As Integer = MaxBoul - 1
            Dim ListTirage As New List(Of Integer)()
            Dim temp As Integer
            Dim nbmax As Integer = 0
            Dim nb As Integer = 0
            Dim trouve As Boolean = False

            For i = 0 To MaxBoul - 1
                ListTirage.Add(i + 1)
            Next

            'apres avoir recuperer les numero jouer dans base de donnée, on conserve que la moitié de ses numeros
            Using oTicket As New BL_Lotto.BlTicket(source)
                For j As Integer = 0 To gagnant.Count - 1
                    temp = gagnant.Item(j)
                    oDsNumJou = oTicket.getNumJouer(temp.ToString)

                    If (oDsNumJou.Tables("avoir").Rows.Count > 0) Then

                        For k As Integer = 0 To oDsNumJou.Tables("avoir").Rows.Count - 1

                            'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)
                            If NbPerdant.IndexOf(CInt(oDsNumJou.Tables("avoir").Rows(k).Item("NUMEROS"))) = -1 Then
                                NbPerdant.Add(CInt(oDsNumJou.Tables("avoir").Rows(k).Item("NUMEROS")))
                            End If

                        Next

                    End If


                Next
            End Using

            nbmax = CInt(NbPerdant.Count)


            For i = 0 To nbmax - 1
                NbTriche.Add(CInt(NbPerdant.Item(i)))
            Next

            Return NbTriche.ToArray()
        End Using
    End Function



    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

End Class