﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormAnim
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormAnim))
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.LB_NumTir = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.num1 = New System.Windows.Forms.Label()
        Me.num2 = New System.Windows.Forms.Label()
        Me.num3 = New System.Windows.Forms.Label()
        Me.num4 = New System.Windows.Forms.Label()
        Me.num5 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackgroundImage = CType(resources.GetObject("TableLayoutPanel2.BackgroundImage"), System.Drawing.Image)
        Me.TableLayoutPanel2.ColumnCount = 6
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.LB_NumTir, 4, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label1, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.num1, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.num2, 2, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.num3, 3, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.num4, 4, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.num5, 5, 2)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 6
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.83333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.83333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.83333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.83333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1342, 699)
        Me.TableLayoutPanel2.TabIndex = 42
        '
        'LB_NumTir
        '
        Me.LB_NumTir.AutoSize = True
        Me.LB_NumTir.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.SetColumnSpan(Me.LB_NumTir, 2)
        Me.LB_NumTir.Font = New System.Drawing.Font("Showcard Gothic", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LB_NumTir.ForeColor = System.Drawing.Color.White
        Me.LB_NumTir.Location = New System.Drawing.Point(802, 58)
        Me.LB_NumTir.Name = "LB_NumTir"
        Me.LB_NumTir.Size = New System.Drawing.Size(164, 79)
        Me.LB_NumTir.TabIndex = 18
        Me.LB_NumTir.Text = "1024"
        Me.LB_NumTir.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.SetColumnSpan(Me.Label1, 3)
        Me.Label1.Font = New System.Drawing.Font("Showcard Gothic", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(406, 58)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(390, 79)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Tirage No:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'num1
        '
        Me.num1.BackColor = System.Drawing.Color.Transparent
        Me.num1.Font = New System.Drawing.Font("Showcard Gothic", 65.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num1.ForeColor = System.Drawing.Color.White
        Me.num1.Image = CType(resources.GetObject("num1.Image"), System.Drawing.Image)
        Me.num1.Location = New System.Drawing.Point(3, 203)
        Me.num1.Name = "num1"
        Me.TableLayoutPanel2.SetRowSpan(Me.num1, 2)
        Me.num1.Size = New System.Drawing.Size(251, 264)
        Me.num1.TabIndex = 21
        Me.num1.Text = "??"
        Me.num1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num2
        '
        Me.num2.BackColor = System.Drawing.Color.Transparent
        Me.num2.Font = New System.Drawing.Font("Showcard Gothic", 65.25!)
        Me.num2.ForeColor = System.Drawing.Color.White
        Me.num2.Image = CType(resources.GetObject("num2.Image"), System.Drawing.Image)
        Me.num2.Location = New System.Drawing.Point(260, 203)
        Me.num2.Name = "num2"
        Me.TableLayoutPanel2.SetRowSpan(Me.num2, 2)
        Me.num2.Size = New System.Drawing.Size(259, 264)
        Me.num2.TabIndex = 19
        Me.num2.Text = "??"
        Me.num2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num3
        '
        Me.num3.BackColor = System.Drawing.Color.Transparent
        Me.num3.Font = New System.Drawing.Font("Showcard Gothic", 65.25!)
        Me.num3.ForeColor = System.Drawing.Color.White
        Me.num3.Image = CType(resources.GetObject("num3.Image"), System.Drawing.Image)
        Me.num3.Location = New System.Drawing.Point(525, 203)
        Me.num3.Name = "num3"
        Me.TableLayoutPanel2.SetRowSpan(Me.num3, 2)
        Me.num3.Size = New System.Drawing.Size(271, 264)
        Me.num3.TabIndex = 16
        Me.num3.Text = "??"
        Me.num3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num4
        '
        Me.num4.BackColor = System.Drawing.Color.Transparent
        Me.num4.Font = New System.Drawing.Font("Showcard Gothic", 65.25!)
        Me.num4.ForeColor = System.Drawing.Color.White
        Me.num4.Image = CType(resources.GetObject("num4.Image"), System.Drawing.Image)
        Me.num4.Location = New System.Drawing.Point(802, 203)
        Me.num4.Name = "num4"
        Me.TableLayoutPanel2.SetRowSpan(Me.num4, 2)
        Me.num4.Size = New System.Drawing.Size(265, 264)
        Me.num4.TabIndex = 17
        Me.num4.Text = "??"
        Me.num4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'num5
        '
        Me.num5.BackColor = System.Drawing.Color.Transparent
        Me.num5.Font = New System.Drawing.Font("Showcard Gothic", 65.25!)
        Me.num5.ForeColor = System.Drawing.Color.White
        Me.num5.Image = CType(resources.GetObject("num5.Image"), System.Drawing.Image)
        Me.num5.Location = New System.Drawing.Point(1073, 203)
        Me.num5.Name = "num5"
        Me.TableLayoutPanel2.SetRowSpan(Me.num5, 2)
        Me.num5.Size = New System.Drawing.Size(242, 264)
        Me.num5.TabIndex = 20
        Me.num5.Text = "??"
        Me.num5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FormAnim
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1342, 699)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FormAnim"
        Me.Text = "FormAnim"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents num3 As Label
    Friend WithEvents num2 As Label
    Friend WithEvents num1 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents LB_NumTir As Label
    Friend WithEvents num4 As Label
    Friend WithEvents num5 As Label
End Class
