﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Activation
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TB_cle1 = New System.Windows.Forms.TextBox()
        Me.TB_cle2 = New System.Windows.Forms.TextBox()
        Me.TB_cle3 = New System.Windows.Forms.TextBox()
        Me.TB_cle4 = New System.Windows.Forms.TextBox()
        Me.TB_cle5 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TB_cle5)
        Me.GroupBox1.Controls.Add(Me.TB_cle4)
        Me.GroupBox1.Controls.Add(Me.TB_cle3)
        Me.GroupBox1.Controls.Add(Me.TB_cle2)
        Me.GroupBox1.Controls.Add(Me.TB_cle1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(515, 100)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Veuillez saisir le numero de Serie"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Numero De Serie"
        '
        'TB_cle1
        '
        Me.TB_cle1.Location = New System.Drawing.Point(107, 40)
        Me.TB_cle1.MaxLength = 5
        Me.TB_cle1.Name = "TB_cle1"
        Me.TB_cle1.Size = New System.Drawing.Size(64, 20)
        Me.TB_cle1.TabIndex = 1
        '
        'TB_cle2
        '
        Me.TB_cle2.Location = New System.Drawing.Point(189, 40)
        Me.TB_cle2.MaxLength = 5
        Me.TB_cle2.Name = "TB_cle2"
        Me.TB_cle2.Size = New System.Drawing.Size(64, 20)
        Me.TB_cle2.TabIndex = 1
        '
        'TB_cle3
        '
        Me.TB_cle3.Location = New System.Drawing.Point(271, 40)
        Me.TB_cle3.MaxLength = 5
        Me.TB_cle3.Name = "TB_cle3"
        Me.TB_cle3.Size = New System.Drawing.Size(64, 20)
        Me.TB_cle3.TabIndex = 1
        '
        'TB_cle4
        '
        Me.TB_cle4.Location = New System.Drawing.Point(353, 40)
        Me.TB_cle4.MaxLength = 5
        Me.TB_cle4.Name = "TB_cle4"
        Me.TB_cle4.Size = New System.Drawing.Size(64, 20)
        Me.TB_cle4.TabIndex = 1
        '
        'TB_cle5
        '
        Me.TB_cle5.Location = New System.Drawing.Point(435, 40)
        Me.TB_cle5.MaxLength = 5
        Me.TB_cle5.Name = "TB_cle5"
        Me.TB_cle5.Size = New System.Drawing.Size(64, 20)
        Me.TB_cle5.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(52, 159)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(163, 35)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Continuer L'evaluation"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(331, 159)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(135, 35)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "OK"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(52, 118)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(414, 35)
        Me.Button3.TabIndex = 1
        Me.Button3.Text = "Acheter En ligne"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(175, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(10, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "-"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(257, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(10, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "-"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(339, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(10, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "-"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(421, 43)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(10, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "-"
        '
        'Activation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkSlateGray
        Me.ClientSize = New System.Drawing.Size(541, 205)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Activation"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Activation"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TB_cle1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TB_cle5 As System.Windows.Forms.TextBox
    Friend WithEvents TB_cle4 As System.Windows.Forms.TextBox
    Friend WithEvents TB_cle3 As System.Windows.Forms.TextBox
    Friend WithEvents TB_cle2 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
