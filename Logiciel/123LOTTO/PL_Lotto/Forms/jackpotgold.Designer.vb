﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class jackpotgold
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lbtitrejack = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbelNumtick = New System.Windows.Forms.Label()
        Me.lbeljackpot = New System.Windows.Forms.Label()
        Me.VidScreenBox = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.pb_photo_lot = New System.Windows.Forms.PictureBox()
        CType(Me.VidScreenBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pb_photo_lot, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbtitrejack
        '
        Me.lbtitrejack.AutoSize = True
        Me.lbtitrejack.BackColor = System.Drawing.Color.Black
        Me.lbtitrejack.Font = New System.Drawing.Font("Century Gothic", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbtitrejack.ForeColor = System.Drawing.Color.White
        Me.lbtitrejack.Location = New System.Drawing.Point(636, 59)
        Me.lbtitrejack.Name = "lbtitrejack"
        Me.lbtitrejack.Padding = New System.Windows.Forms.Padding(5)
        Me.lbtitrejack.Size = New System.Drawing.Size(437, 66)
        Me.lbtitrejack.TabIndex = 1
        Me.lbtitrejack.Text = "SUPER CAGNOTTE"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(638, 217)
        Me.Label3.Name = "Label3"
        Me.Label3.Padding = New System.Windows.Forms.Padding(5)
        Me.Label3.Size = New System.Drawing.Size(258, 48)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Ticket gagnant"
        '
        'lbelNumtick
        '
        Me.lbelNumtick.AutoSize = True
        Me.lbelNumtick.BackColor = System.Drawing.Color.White
        Me.lbelNumtick.Font = New System.Drawing.Font("Century Gothic", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbelNumtick.ForeColor = System.Drawing.Color.Black
        Me.lbelNumtick.Location = New System.Drawing.Point(902, 217)
        Me.lbelNumtick.Name = "lbelNumtick"
        Me.lbelNumtick.Padding = New System.Windows.Forms.Padding(5)
        Me.lbelNumtick.Size = New System.Drawing.Size(306, 48)
        Me.lbelNumtick.TabIndex = 1
        Me.lbelNumtick.Text = "COAF-2311-26254"
        '
        'lbeljackpot
        '
        Me.lbeljackpot.AutoSize = True
        Me.lbeljackpot.BackColor = System.Drawing.Color.Black
        Me.lbeljackpot.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbeljackpot.ForeColor = System.Drawing.Color.White
        Me.lbeljackpot.Location = New System.Drawing.Point(638, 144)
        Me.lbeljackpot.Name = "lbeljackpot"
        Me.lbeljackpot.Padding = New System.Windows.Forms.Padding(5)
        Me.lbeljackpot.Size = New System.Drawing.Size(356, 54)
        Me.lbeljackpot.TabIndex = 1
        Me.lbeljackpot.Text = "COAF-2311-26254"
        '
        'VidScreenBox
        '
        Me.VidScreenBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.VidScreenBox.BackColor = System.Drawing.Color.Black
        Me.VidScreenBox.Location = New System.Drawing.Point(637, 274)
        Me.VidScreenBox.Name = "VidScreenBox"
        Me.VidScreenBox.Size = New System.Drawing.Size(649, 377)
        Me.VidScreenBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.VidScreenBox.TabIndex = 0
        Me.VidScreenBox.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Image = Global.PL_Lotto.My.Resources.Resources.Loto
        Me.PictureBox1.Location = New System.Drawing.Point(1231, 59)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(119, 66)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'pb_photo_lot
        '
        Me.pb_photo_lot.BackColor = System.Drawing.Color.Transparent
        Me.pb_photo_lot.Image = Global.PL_Lotto.My.Resources.Resources.IMG_8342
        Me.pb_photo_lot.Location = New System.Drawing.Point(12, 59)
        Me.pb_photo_lot.Name = "pb_photo_lot"
        Me.pb_photo_lot.Size = New System.Drawing.Size(607, 592)
        Me.pb_photo_lot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pb_photo_lot.TabIndex = 5
        Me.pb_photo_lot.TabStop = False
        '
        'jackpotgold
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gold
        Me.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.red_background_gold_stars
        Me.ClientSize = New System.Drawing.Size(1362, 746)
        Me.Controls.Add(Me.pb_photo_lot)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lbelNumtick)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.VidScreenBox)
        Me.Controls.Add(Me.lbeljackpot)
        Me.Controls.Add(Me.lbtitrejack)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "jackpotgold"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Felicitation!!!"
        Me.TopMost = True
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.VidScreenBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pb_photo_lot, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents VidScreenBox As System.Windows.Forms.PictureBox
    Friend WithEvents lbtitrejack As System.Windows.Forms.Label
    Friend WithEvents lbeljackpot As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lbelNumtick As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents pb_photo_lot As PictureBox
End Class
