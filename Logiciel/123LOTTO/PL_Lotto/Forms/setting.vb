﻿Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing
Imports System

Public Class setting
#Region "bitmaps"

    Private oImage As Byte() = {
        &H42, &H4D, &HC6, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H76, &H0, &H0, &H0, &H28, &H0,
        &H0, &H0, &HB, &H0, &H0, &H0, &HA, &H0,
        &H0, &H0, &H1, &H0, &H4, &H0, &H0, &H0,
        &H0, &H0, &H50, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H0, &H0, &H10, &H0,
        &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H80, &H0, &H0, &H80,
        &H0, &H0, &H0, &H80, &H80, &H0, &H80, &H0,
        &H0, &H0, &H80, &H0, &H80, &H0, &H80, &H80,
        &H0, &H0, &HC0, &HC0, &HC0, &H0, &H80, &H80,
        &H80, &H0, &H0, &H0, &HFF, &H0, &H0, &HFF,
        &H0, &H0, &H0, &HFF, &HFF, &H0, &HFF, &H0,
        &H0, &H0, &HFF, &H0, &HFF, &H0, &HFF, &HFF,
        &H0, &H0, &HFF, &HFF, &HFF, &H0, &HFF, &HFF,
        &H0, &HF, &HFF, &HF0, &H0, &H0, &HFF, &H0,
        &HFF, &HF0, &HF, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HF, &HFF,
        &HFF, &HFF, &HFF, &H0, &H0, &H0, &HF, &HFF,
        &HFF, &HFF, &HFF, &H0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HFF, &H0,
        &HFF, &HF0, &HF, &HF0, &H0, &H0, &HFF, &HFF,
        &H0, &HF, &HFF, &HF0, &H0, &H0}

    Private xImage As Byte() = {
        &H42, &H4D, &HC6, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H76, &H0, &H0, &H0, &H28, &H0,
        &H0, &H0, &HB, &H0, &H0, &H0, &HA, &H0,
        &H0, &H0, &H1, &H0, &H4, &H0, &H0, &H0,
        &H0, &H0, &H50, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H0, &H0, &H10, &H0,
        &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H80, &H0, &H0, &H80,
        &H0, &H0, &H0, &H80, &H80, &H0, &H80, &H0,
        &H0, &H0, &H80, &H0, &H80, &H0, &H80, &H80,
        &H0, &H0, &HC0, &HC0, &HC0, &H0, &H80, &H80,
        &H80, &H0, &H0, &H0, &HFF, &H0, &H0, &HFF,
        &H0, &H0, &H0, &HFF, &HFF, &H0, &HFF, &H0,
        &H0, &H0, &HFF, &H0, &HFF, &H0, &HFF, &HFF,
        &H0, &H0, &HFF, &HFF, &HFF, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HFF, &HF,
        &HFF, &HFF, &HF, &HF0, &H0, &H0, &HFF, &HF0,
        &HFF, &HF0, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HF, &HF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HF, &HF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HF, &HF, &HFF, &HF0, &H0, &H0, &HFF, &HF0,
        &HFF, &HF0, &HFF, &HF0, &H0, &H0, &HFF, &HF,
        &HFF, &HFF, &HF, &HF0, &H0, &H0, &HF0, &HFF,
        &HFF, &HFF, &HF0, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0}

    Private blankImage As Byte() = {
        &H42, &H4D, &HC6, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H76, &H0, &H0, &H0, &H28, &H0,
        &H0, &H0, &HB, &H0, &H0, &H0, &HA, &H0,
        &H0, &H0, &H1, &H0, &H4, &H0, &H0, &H0,
        &H0, &H0, &H50, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H0, &H0, &H10, &H0,
        &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0,
        &H0, &H0, &H0, &H0, &H80, &H0, &H0, &H80,
        &H0, &H0, &H0, &H80, &H80, &H0, &H80, &H0,
        &H0, &H0, &H80, &H0, &H80, &H0, &H80, &H80,
        &H0, &H0, &HC0, &HC0, &HC0, &H0, &H80, &H80,
        &H80, &H0, &H0, &H0, &HFF, &H0, &H0, &HFF,
        &H0, &H0, &H0, &HFF, &HFF, &H0, &HFF, &H0,
        &H0, &H0, &HFF, &H0, &HFF, &H0, &HFF, &HFF,
        &H0, &H0, &HFF, &HFF, &HFF, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0, &HFF, &HFF,
        &HFF, &HFF, &HFF, &HF0, &H0, &H0}
#End Region
    Private x As Bitmap = New Bitmap(New MemoryStream(xImage))
    Private o As Bitmap = New Bitmap(New MemoryStream(oImage))

    Private blank As Bitmap = New Bitmap(New MemoryStream(blankImage))
    Private bitmapPadding As Integer = 6

    Private ojacpot As BL_Lotto.BlJackot
    Dim Coleu As Drawing.Color
    Public is_material_gold As Integer = 1
    Public is_material_silver As Integer = 1
    Public is_material_bronze As Integer = 1
    Private oDataSet, dstjack1, dstjack2 As Data.DataSet
    Private ojackpotDataSet As DataSet
    Private Selectsilver, Selectbronze As String
    Private Selectjackpot As String
    Dim scr As Screen() = Screen.AllScreens

    Private Sub BtValider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtValider.Click
        If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok And bonPourcent() Then

            My.Settings.PRINTER_NAME = TbNameImp.Text
            DIFICULT_LEVEL = TrackBar1.Value
            'MsgBox(CStr(getScreenNumber(Me.cmbDevices.Text)))
            SCREEN_SELECT = CStr(getScreenNumber(Me.cmbDevices.Text.ToString()))
            PersistSetting()

            Me.Close()

        Else
            Me.Close()
        End If
    End Sub

    Private Function getScreenNumber(ByVal DeviceID As String) As Integer
        Dim i As Integer = 0
        For Each s As Screen In scr
            'MsgBox("device " & s.DeviceName)
            'MsgBox("selected " & "\\.\" & DeviceID)
            If (s.DeviceName = "\\.\" & DeviceID) Then Return i
            i += 1
        Next

        'if cannot find the device reset to the default 0
        Return 0
    End Function



    Private Sub setting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TbNameImp.Text = My.Settings.PRINTER_NAME
        TrackBar1.Value = DIFICULT_LEVEL
        TextBox1.Text = DIFICULT_LEVEL & " %"

        TrackBar2.Value = SILVER_POT
        TextBox2.Text = SILVER_POT & " %"
        Tb_min_jack.Text = MIN_GAIIN_SILVER.ToString
        Tb_max_jack.Text = MAX_GAIIN_SILVER.ToString


        TrackBar3.Value = GOLD_POT
        TextBox4.Text = GOLD_POT & " %"

        TrackBar4.Value = IRON_POT
        TextBox6.Text = IRON_POT & " %"

        Using ojacpot As New BL_Lotto.BlJackot(source)
            Tbsilver.Text = ojacpot.GetMontantJackpot(ID_SILVER)
            tbNomLotSilver.Text = ojacpot.GetNomLotJackpot(ID_SILVER)
            Tb_etat_bonze.Text = ojacpot.GetEtatJackpot(ID_SILVER)

            TbBronze.Text = ojacpot.GetMontantJackpot(ID_IRON)
            tbNomLotBronze.Text = ojacpot.GetNomLotJackpot(ID_IRON)
            Tb_etat_silver.Text = ojacpot.GetEtatJackpot(ID_IRON)

            TbGold.Text = ojacpot.GetMontantJackpot(ID_GOLD)
            tbNomLotGold.Text = ojacpot.GetNomLotJackpot(ID_GOLD)
            Tb_etat_gold.Text = ojacpot.GetEtatJackpot(ID_GOLD)


            Dim dstsilver, dstgold, dstbronze As New DataSet

            dstsilver = ojacpot.GetJackpot(ID_SILVER)

            If (CBool(dstsilver.Tables("jackpot").Rows(0).Item("is_matirial"))) Then
                'RadioButton4.PerformClick()
                tb_jackmat.Text = "OUI"
            Else
                'RadioButton3.PerformClick()
                tb_jackmat.Text = "NON"
            End If

            dstgold = ojacpot.GetJackpot(ID_GOLD)

            If (CBool(dstgold.Tables("jackpot").Rows(0).Item("is_matirial"))) Then

                tb_gold_mat.Text = "OUI"
                is_material_gold = 1
                Panel7.Visible = True
                tbNomLotGold.Visible = True

            Else

                tb_gold_mat.Text = "NON"
                is_material_gold = 0
                Panel7.Visible = False
                tbNomLotGold.Visible = False

            End If

            dstbronze = ojacpot.GetJackpot(ID_IRON)

            If (CBool(dstbronze.Tables("jackpot").Rows(0).Item("is_matirial"))) Then

                tb_bronze_mat.Text = "OUI"
                is_material_bronze = 1
                Panel11.Visible = True
                tbNomLotBronze.Visible = True

            Else
                'RadioButton2.PerformClick()
                'tb_jackmat.Text = "NON"
                'Coleu = RadioButton2.BackColor

                'RadioButton2.BackColor = RadioButton2.ForeColor
                'RadioButton2.ForeColor = Coleu
                tb_gold_mat.Text = "NON"
                is_material_bronze = 0
                Panel11.Visible = False
                tbNomLotBronze.Visible = False

            End If

            chergergrid_silver()
            'chergergrid_gold()
            'chergergrid_bronze()
            chergergrid_Lot()
            chergergrid_screen()
        End Using

    End Sub
    Private Sub chergergrid_screen()

        Dim strDeviceName As String

        Me.cmbDevices.Items.Clear()
        For Each s As System.Windows.Forms.Screen In scr
            strDeviceName = s.DeviceName.Replace("\\.\", "")
            'strDeviceName = s.DeviceName
            'Me.cmbDevices.Items.Add(strDeviceName)
            ' you can check the device Is primary Or Not this way
            'If (s.Primary) Then
            'Me.cmbDevices.Items.Add(">" + strDeviceName)
            'Else
            Me.cmbDevices.Items.Add(strDeviceName)
            'End If
        Next
        For i As Integer = 0 To Me.cmbDevices.Items.Count - 1
            If (SCREEN_SELECT = CStr(getScreenNumber(Me.cmbDevices.Items(i).ToString))) Then
                Me.cmbDevices.SelectedIndex = i
            End If
        Next

    End Sub
    Private Sub chergergrid_silver()
        Using ojacpot As New BL_Lotto.BlJackot(source)
            Try
                With dtg_lot_silver


                    oDataSet = ojacpot.GetListlot((ID_SILVER))

                    .DataSource = Nothing
                    .Columns.Clear()
                    .Rows.Clear()

                    .DataSource = oDataSet.Tables("jackpot")

                    .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                    'Empêche les modifications de lignes, colonnes, l'ajout, la suppression
                    .AllowUserToAddRows = False
                    .AllowUserToDeleteRows = False
                    .AllowUserToOrderColumns = False
                    .AllowUserToResizeColumns = False
                    .AllowUserToResizeRows = False

                    'masquer la premiere colonne  `id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp`FROM `lot_jackpot`
                    .Columns(0).HeaderText = "id"
                    .Columns(0).Visible = False
                    'empeche les colonne d'etre reordonner
                    .Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable


                    'Nomme les colonnes (en têtes)
                    .Columns(1).HeaderText = "Materiel 0=non 1=oui"
                    'empeche les colonne d'etre reordonner
                    .Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable

                    'Nomme les colonnes (en têtes)
                    .Columns(2).HeaderText = "Nom Materiel"

                    'Nomme les colonnes (en têtes)
                    .Columns(3).HeaderText = "Montant/Valeur"





                    Dim imageColumn As New DataGridViewImageColumn


                    Dim unMarked As Bitmap = blank
                    imageColumn = New DataGridViewImageColumn()

                    ' Add twice the padding for the left and 
                    ' right sides of the cell.
                    imageColumn.Width = x.Width + 2 * bitmapPadding + 1

                    imageColumn.Image = unMarked
                    imageColumn.ImageLayout = DataGridViewImageCellLayout.NotSet
                    imageColumn.Description = "default image layout"
                    imageColumn.HeaderText = "default"

                    '.Columns.Insert(.ColumnCount, imageColumn)
                    .Columns.Insert(0, imageColumn)
                    dstjack1 = ojacpot.GetJackpot(ID_SILVER)

                    Dim Img As New DataGridViewImageCell
                    Img.Value = My.Resources.star_128

                    For j As Integer = 0 To .Rows.Count - 1

                        If (.Item(1, j).Value.ToString = dstjack1.Tables("jackpot").Rows(0).Item("id_lot_encour").ToString) Then
                            Dim cell As DataGridViewImageCell = CType(.Rows(j).Cells(0), DataGridViewImageCell)
                            cell.Value = x
                        End If
                    Next

                End With

            Catch ExceptionErr As Exception
                MessageBox.Show(ExceptionErr.Message.ToString)
            End Try
        End Using
        If dtg_lot_silver.Rows.Count <= 0 Then Exit Sub
        Selectsilver = CStr(dtg_lot_silver.Rows(0).Cells(1).Value)
    End Sub

    Private Sub chergergrid_bronze()
        Using ojacpot As New BL_Lotto.BlJackot(source)
            Try
                oDataSet = ojacpot.GetListlot((ID_IRON))

                dtg_lot_bronze.DataSource = Nothing
                dtg_lot_bronze.DataSource = oDataSet.Tables("jackpot")

                dtg_lot_bronze.SelectionMode = DataGridViewSelectionMode.FullRowSelect
                'Empêche les modifications de lignes, colonnes, l'ajout, la suppression
                dtg_lot_bronze.AllowUserToAddRows = False
                dtg_lot_bronze.AllowUserToDeleteRows = False
                dtg_lot_bronze.AllowUserToOrderColumns = False
                dtg_lot_bronze.AllowUserToResizeColumns = False
                dtg_lot_bronze.AllowUserToResizeRows = False

                'masquer la premiere colonne  `id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp`FROM `lot_jackpot`
                dtg_lot_bronze.Columns(0).HeaderText = "id_lot"
                dtg_lot_bronze.Columns(0).Visible = False
                'empeche les colonne d'etre reordonner
                dtg_lot_bronze.Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable


                'Nomme les colonnes (en têtes)
                dtg_lot_bronze.Columns(1).HeaderText = "Materiel 0=non 1=oui"
                'empeche les colonne d'etre reordonner
                dtg_lot_bronze.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable

                'Nomme les colonnes (en têtes)
                dtg_lot_bronze.Columns(2).HeaderText = "Nom Materiel"

                'Nomme les colonnes (en têtes)
                dtg_lot_bronze.Columns(3).HeaderText = "Montant/Valeur"


            Catch ExceptionErr As Exception
                MessageBox.Show(ExceptionErr.Message.ToString)
            End Try
        End Using
        If dtg_lot_bronze.Rows.Count <= 0 Then Exit Sub
        Selectbronze = CStr(dtg_lot_bronze.Rows(0).Cells(0).Value)
    End Sub

    Private Sub chergergrid_Lot()

        Using ojacpot As New BL_Lotto.BlJackot(source)
            Try

                With dtg_lot_gold
                    oDataSet = ojacpot.GetListlot(ID_GOLD.ToString, ID_IRON.ToString)

                    .DataSource = Nothing
                    .Columns.Clear()
                    .Rows.Clear()
                    .DataSource = oDataSet.Tables("jackpot")
                    .SelectionMode = DataGridViewSelectionMode.FullRowSelect
                    'Empêche les modifications de lignes, colonnes, l'ajout, la suppression
                    .AllowUserToAddRows = False
                    .AllowUserToDeleteRows = False
                    .AllowUserToOrderColumns = False
                    .AllowUserToResizeColumns = False
                    .AllowUserToResizeRows = False
                    .DefaultCellStyle.Font = New Font(.Font.FontFamily, 9.25, FontStyle.Bold)


                    ''Une ligne sur 2 en bleue
                    '.RowsDefaultCellStyle.BackColor = Color.White
                    '.AlternatingRowsDefaultCellStyle.BackColor = Color.Gold

                    'masquer la premiere colonne  `id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp`FROM `lot_jackpot`
                    .Columns(0).HeaderText = "Id"
                    .Columns(0).Visible = False
                    'empeche les colonne d'etre reordonner
                    .Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable

                    'Nomme les colonnes (en têtes)
                    .Columns(1).HeaderText = "Type"
                    'masquer la premiere colonne
                    .Columns(1).Visible = False



                    'Nomme les colonnes (en têtes)
                    .Columns(2).HeaderText = "Materiel 0=non 1=oui"
                    'empeche les colonne d'etre reordonner
                    .Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable

                    'Nomme les colonnes (en têtes)
                    .Columns(3).HeaderText = "Nom Materiel"

                    'Nomme les colonnes (en têtes)
                    .Columns(4).HeaderText = "Montant/Valeur"


                    Dim imageColumn As New DataGridViewImageColumn
                    '.Columns.Insert(2, chk)
                    'chk.HeaderText = "Statut"
                    'chk.Name = "default"

                    Dim unMarked As Bitmap = blank
                    imageColumn = New DataGridViewImageColumn()

                    ' Add twice the padding for the left and 
                    ' right sides of the cell.
                    imageColumn.Width = x.Width + 2 * bitmapPadding + 1

                    imageColumn.Image = unMarked
                    imageColumn.ImageLayout = DataGridViewImageCellLayout.NotSet
                    imageColumn.Description = "default image layout"
                    imageColumn.HeaderText = "default"

                    .Columns.Insert(.ColumnCount, imageColumn)

                    dstjack1 = ojacpot.GetJackpot(ID_GOLD)
                    dstjack2 = ojacpot.GetJackpot(ID_IRON)


                    For j As Integer = 0 To .Rows.Count - 1
                        'MsgBox(.Item(1, j).Value.ToString)
                        If (.Item(1, j).Value.ToString = ID_GOLD) Then
                            .Rows(j).DefaultCellStyle.BackColor = Color.Gold
                        ElseIf (.Item(1, j).Value.ToString = ID_IRON)
                            .Rows(j).DefaultCellStyle.BackColor = Color.Silver
                        End If
                    Next

                    Dim Img As New DataGridViewImageCell
                    Img.Value = My.Resources.star_128

                    For j As Integer = 0 To .Rows.Count - 1

                        If (.Item(0, j).Value.ToString = dstjack1.Tables("jackpot").Rows(0).Item("id_lot_encour").ToString) Then

                            Dim cell As DataGridViewImageCell = CType(.Rows(j).Cells(.ColumnCount - 1), DataGridViewImageCell)
                            cell.Value = x
                        ElseIf (.Item(0, j).Value.ToString = dstjack2.Tables("jackpot").Rows(0).Item("id_lot_encour").ToString)
                            'MsgBox("ici 2")
                            Dim cell As DataGridViewImageCell = CType(.Rows(j).Cells(.ColumnCount - 1), DataGridViewImageCell)
                            cell.Value = x
                        End If
                    Next

                End With

            Catch ExceptionErr As Exception
                MessageBox.Show(ExceptionErr.Message.ToString)
            End Try
        End Using
    End Sub

    Private Sub chergergrid_gold()
        Using ojacpot As New BL_Lotto.BlJackot(source)
            Try
                oDataSet = ojacpot.GetListlot(ID_GOLD)

                dtg_lot_gold.DataSource = Nothing
                dtg_lot_gold.DataSource = oDataSet.Tables("jackpot")

                dtg_lot_gold.SelectionMode = DataGridViewSelectionMode.FullRowSelect
                'Empêche les modifications de lignes, colonnes, l'ajout, la suppression
                dtg_lot_gold.AllowUserToAddRows = False
                dtg_lot_gold.AllowUserToDeleteRows = False
                dtg_lot_gold.AllowUserToOrderColumns = False
                dtg_lot_gold.AllowUserToResizeColumns = False
                dtg_lot_gold.AllowUserToResizeRows = False

                'masquer la premiere colonne  `id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp`FROM `lot_jackpot`
                dtg_lot_gold.Columns(0).Name = "id_lot"
                dtg_lot_gold.Columns(0).Visible = False
                'empeche les colonne d'etre reordonner
                dtg_lot_gold.Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable


                'Nomme les colonnes (en têtes)
                dtg_lot_gold.Columns(1).Name = "Materiel 0=non 1=oui"
                'empeche les colonne d'etre reordonner
                dtg_lot_gold.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable

                'Nomme les colonnes (en têtes)
                dtg_lot_gold.Columns(2).Name = "Nom Materiel"

                'Nomme les colonnes (en têtes)
                dtg_lot_gold.Columns(3).Name = "Montant/Valeur"


            Catch ExceptionErr As Exception
                MessageBox.Show(ExceptionErr.Message.ToString)
            End Try
        End Using
        If dtg_lot_gold.Rows.Count <= 0 Then Exit Sub
        Selectjackpot = CStr(dtg_lot_gold.Rows(0).Cells(0).Value)
    End Sub
    Private Sub TrackBar1_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBar1.Scroll
        TextBox1.Text = TrackBar1.Value & " %"
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If (Tbsilver.Text <> "" And Tb_etat_bonze.Text <> "" And IsNumeric(Tb_etat_bonze.Text.Trim) And IsNumeric(Tbsilver.Text.Trim) And IsNumeric(Tb_min_jack.Text.Trim) And IsNumeric(Tb_max_jack.Text.Trim) And bonPourcent()) Then
            If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                Using ojacpot As New BL_Lotto.BlJackot(source)
                    'MsgBox(CBool(is_material_silver))
                    'ojacpot.Maj_mon_Jackpot(ID_SILVER, Tbsilver.Text.Trim, CBool(is_material_silver), tbNomLotSilver.Text.Trim)

                    ojacpot.Maj_mon_Jackpot(ID_SILVER, Tb_id_lot_silver.Text.Trim, Tbsilver.Text.Trim, Tb_etat_bonze.Text, is_material_silver, tbNomLotSilver.Text.Trim)
                    ojacpot.Maj_mon_Lot(Tb_id_lot_silver.Text, Tbsilver.Text.Trim, CBool(is_material_silver), tbNomLotSilver.Text.Trim)

                    SILVER_POT = CInt(TrackBar2.Value)
                    MIN_GAIIN_SILVER = CInt(Tb_min_jack.Text)
                    MAX_GAIIN_SILVER = CInt(Tb_max_jack.Text)
                    PersistSetting()

                    MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    chergergrid_silver()
                End Using
            End If

        Else
            MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        If (tb_id_jack.Text = ID_GOLD) Then

            If (TbGold.Text <> "" And Tb_etat_gold.Text <> "" And IsNumeric(TbGold.Text.Trim) And bonPourcent()) Then
                If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                    Using ojacpot As New BL_Lotto.BlJackot(source)
                        ojacpot.Maj_mon_Lot(Tb_id_lot.Text, TbGold.Text.Trim, CBool(is_material_gold), tbNomLotGold.Text.Trim)
                        Dim cell As DataGridViewImageCell = CType(dtg_lot_gold.Rows(dtg_lot_gold.CurrentRow.Index).Cells(dtg_lot_gold.ColumnCount - 1), DataGridViewImageCell)
                        If (cell.Value IsNot blank) Then
                            ojacpot.Maj_mon_Jackpot(ID_GOLD, Tb_id_lot.Text, TbGold.Text.Trim, Tb_etat_gold.Text.Trim, CBool(is_material_gold), tbNomLotGold.Text.Trim)
                            GOLD_POT = CInt(TrackBar3.Value)
                            PersistSetting()
                        End If

                        MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        chergergrid_Lot()
                    End Using
                End If

            Else
                MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        ElseIf (tb_id_jack.Text = ID_IRON)

            If (TbGold.Text <> "" And Tb_etat_gold.Text <> "" And IsNumeric(TbGold.Text.Trim) And bonPourcent()) Then
                If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                    Using ojacpot As New BL_Lotto.BlJackot(source)
                        ojacpot.Maj_mon_Lot(Tb_id_lot.Text, TbGold.Text.Trim, CBool(is_material_gold), tbNomLotGold.Text.Trim)
                        Dim cell As DataGridViewImageCell = CType(dtg_lot_gold.Rows(dtg_lot_gold.CurrentRow.Index).Cells(dtg_lot_gold.ColumnCount - 1), DataGridViewImageCell)
                        If (cell.Value IsNot blank) Then
                            ojacpot.Maj_mon_Jackpot(ID_IRON.ToString, Tb_id_lot.Text, TbGold.Text.Trim, Tb_etat_gold.Text.Trim, CBool(is_material_gold), tbNomLotGold.Text.Trim)
                            IRON_POT = CInt(TrackBar3.Value)
                            PersistSetting()
                        End If
                        MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        chergergrid_Lot()
                    End Using
                End If

            Else
                MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        End If

    End Sub


    Private Sub TrackBar2_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBar2.Scroll
        TextBox2.Text = TrackBar2.Value & " %"
    End Sub

    Private Sub TrackBar3_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBar3.Scroll
        TextBox4.Text = TrackBar3.Value & " %"
    End Sub
    Public Function bonPourcent() As Boolean
        If (TrackBar1.Value + TrackBar2.Value + TrackBar3.Value + TrackBar4.Value) >= 100 Then
            Return False
        Else
            Return True
        End If
    End Function

    'Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
    '    If RadioButton1.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton1.BackColor

    '        RadioButton1.BackColor = RadioButton1.ForeColor
    '        RadioButton1.ForeColor = Coleu

    '        is_material_gold = 1
    '        Panel7.Visible = True
    '        tbNomLotGold.Visible = True

    '    Else
    '        Coleu = RadioButton1.BackColor
    '        RadioButton1.BackColor = RadioButton1.ForeColor
    '        RadioButton1.ForeColor = Coleu

    '    End If
    'End Sub

    'Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
    '    If RadioButton2.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton2.BackColor

    '        RadioButton2.BackColor = RadioButton2.ForeColor
    '        RadioButton2.ForeColor = Coleu
    '        is_material_gold = 0
    '        Panel7.Visible = False
    '        tbNomLotGold.Visible = False
    '    Else
    '        Coleu = RadioButton2.BackColor
    '        RadioButton2.BackColor = RadioButton2.ForeColor
    '        RadioButton2.ForeColor = Coleu
    '    End If
    'End Sub

    'Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If RadioButton4.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton4.BackColor

    '        RadioButton4.BackColor = RadioButton4.ForeColor
    '        RadioButton4.ForeColor = Coleu

    '        is_material_silver = 1
    '        Panel8.Visible = True
    '        tbNomLotSilver.Visible = True

    '    Else
    '        Coleu = RadioButton4.BackColor
    '        RadioButton4.BackColor = RadioButton4.ForeColor
    '        RadioButton4.ForeColor = Coleu

    '    End If
    'End Sub

    'Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If RadioButton3.Checked = True Then ' Teste si le bouton est coché.
    '        Coleu = RadioButton3.BackColor

    '        RadioButton3.BackColor = RadioButton3.ForeColor
    '        RadioButton3.ForeColor = Coleu
    '        is_material_silver = 0
    '        Panel8.Visible = False
    '        tbNomLotSilver.Visible = False
    '    Else
    '        Coleu = RadioButton3.BackColor
    '        RadioButton3.BackColor = RadioButton3.ForeColor
    '        RadioButton3.ForeColor = Coleu
    '    End If
    'End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        If dtg_lot_silver.Rows.Count <= 0 Then Exit Sub
        Using ojacpot As New BL_Lotto.BlJackot(source)
            Dim cell As DataGridViewImageCell = CType(dtg_lot_silver.Rows(dtg_lot_silver.CurrentRow.Index).Cells(0), DataGridViewImageCell)
            If (dtg_lot_silver.Rows(dtg_lot_silver.CurrentRow.Index).Cells(1).Value.ToString = 0) Then
                MsgBox("Ce Lot ne peut etre supprimé", MsgBoxStyle.Information, "Warning")
                Exit Sub
            End If
            If (cell.Value Is blank) Then
                If MsgBox("Etes-vous sûr de vouloir supprimer le(s) enregistrement(s)?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                    ojacpot.suprim_lot_Jackpot(Selectsilver)
                    chergergrid_silver()
                End If
            Else
                If MsgBox("Vous etes sur le point de supprimer un Lot encours continuer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                    ojacpot.suprim_lot_Jackpot(Selectsilver)
                    select_another_lot(ID_SILVER)
                    chergergrid_silver()
                End If
            End If

        End Using


    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim f As New ajouter_lot
        'f.tbid_jck.Text = ID_GOLD
        f.cb_type_jack.Items.Clear()
        f.cb_type_jack.Items.Add(ID_SILVER & "# LOT")
        f.ShowDialog()
        chergergrid_silver()
    End Sub

    Private Sub TabPage2_Click(sender As Object, e As EventArgs) Handles TabPage2.Click

    End Sub





    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Dim f As New ajouter_lot
        'f.tbid_jck.Text = ID_GOLD
        f.cb_type_jack.Items.Clear()
        f.cb_type_jack.Items.Add(ID_GOLD & "# CAGNOTE")
        f.cb_type_jack.Items.Add(ID_IRON & "# LOT")
        f.ShowDialog()
        chergergrid_Lot()
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        If dtg_lot_gold.Rows.Count <= 0 Then Exit Sub
        Using ojacpot As New BL_Lotto.BlJackot(source)
            Dim cell As DataGridViewImageCell = CType(dtg_lot_gold.Rows(dtg_lot_gold.CurrentRow.Index).Cells(dtg_lot_gold.ColumnCount - 1), DataGridViewImageCell)
            If (cell.Value Is blank) Then
                If MsgBox("Etes-vous sûr de vouloir supprimer le(s) enregistrement(s)?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                    ojacpot.suprim_lot_Jackpot(Selectjackpot)
                    chergergrid_Lot()
                End If
            Else
                If MsgBox("Vous etes sur le point de supprimer un Lot encours continuer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
                    ojacpot.suprim_lot_Jackpot(Selectjackpot)
                    select_another_lot(tb_id_jack.Text)
                    chergergrid_Lot()
                End If
            End If


        End Using
    End Sub


    Private Sub dtg_lot_gold_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dtg_lot_gold.CellMouseClick
        If dtg_lot_gold.Rows.Count <= 0 Then Exit Sub
        Selectjackpot = CStr(dtg_lot_gold.Rows(dtg_lot_gold.CurrentRow.Index).Cells(0).Value)
    End Sub

    'Private Sub Button13_Click(sender As Object, e As EventArgs)
    '    If (TbBronze.Text <> "" And IsNumeric(TbBronze.Text.Trim) And bonPourcent()) Then
    '        If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then
    '            Using ojacpot As New BL_Lotto.BlJackot(source)
    '                'MsgBox(CBool(is_material_silver))
    '                ojacpot.Maj_mon_Jackpot(ID_IRON, TbBronze.Text.Trim, CBool(is_material_bronze), tbNomLotBronze.Text.Trim)
    '                IRON_POT = CInt(TrackBar4.Value)
    '                MessageBox.Show("Informations Enregistrées.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            End Using
    '        End If
    '    Else
    '        MessageBox.Show("Verifier vos entrees.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End If
    'End Sub

    Private Sub TrackBar4_Scroll(sender As Object, e As EventArgs) Handles TrackBar4.Scroll
        TextBox6.Text = TrackBar4.Value & " %"
    End Sub

    Private Sub TextBox3_TextChanged(sender As Object, e As EventArgs) Handles Tb_etat_bonze.TextChanged

    End Sub




    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Dim f As New ajouter_lot
        f.tbid_jck.Text = ID_IRON
        f.ShowDialog()
        chergergrid_bronze()
    End Sub


    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
        With dtg_lot_silver
            Using ojacpot As New BL_Lotto.BlJackot(source)

                dstjack1 = ojacpot.GetJackpot(ID_SILVER)

                Dim Img As New DataGridViewImageCell
                Img.Value = My.Resources.star_128

                For j As Integer = 0 To .Rows.Count - 1
                    If (.Item(1, j).Value.ToString = dstjack1.Tables("jackpot").Rows(0).Item("id_lot_encour").ToString) Then
                        Dim cell As DataGridViewImageCell = CType(.Rows(j).Cells(0), DataGridViewImageCell)
                        cell.Value = x
                    End If
                Next
            End Using


            'For j As Integer = 0 To .Rows.Count - 1
            '    'MsgBox(.Item(1, j).Value.ToString)
            '    If (.Item(1, j).Value.ToString = ID_GOLD) Then
            '        .Rows(j).DefaultCellStyle.BackColor = Color.Gold
            '    Else
            '        .Rows(j).DefaultCellStyle.BackColor = Color.Silver
            '    End If
            'Next

            'Dim Img As New DataGridViewImageCell
            'Img.Value = My.Resources.star_128

            'For j As Integer = 0 To .Rows.Count - 1

            '    If (.Item(1, j).Value.ToString = dstjack1.Tables("jackpot").Rows(0).Item("id_lot_encour").ToString) Then

            '        Dim cell As DataGridViewImageCell = CType(.Rows(j).Cells(0), DataGridViewImageCell)
            '        cell.Value = x
            '    ElseIf (.Item(1, j).Value.ToString = dstjack2.Tables("jackpot").Rows(0).Item("id_lot_encour").ToString)
            '        'MsgBox("ici 2")
            '        Dim cell As DataGridViewImageCell = CType(.Rows(j).Cells(0), DataGridViewImageCell)
            '        cell.Value = x
            '    End If
            'Next

        End With

    End Sub


    Private Sub dtg_lot_gold_Click(sender As Object, e As EventArgs) Handles dtg_lot_gold.Click
        'If DgvNiveau.Rows.Count <= 0 Then Exit Sub
        'SelectMatiere = CStr(DgvNiveau.Rows(DgvNiveau.CurrentRow.Index).Cells(0).Value)
        'SelectcodMat = CStr(DgvNiveau.Rows(DgvNiveau.CurrentRow.Index).Cells(1).Value)

        If dtg_lot_gold.Rows.Count <= 0 Then Exit Sub
        Selectjackpot = CStr(dtg_lot_gold.Rows(dtg_lot_gold.CurrentRow.Index).Cells(0).Value)


        Dim cell As DataGridViewImageCell = CType(dtg_lot_gold.Rows(dtg_lot_gold.CurrentRow.Index).Cells(dtg_lot_gold.ColumnCount - 1), DataGridViewImageCell)
        If (cell.Value Is blank) Then
            TrackBar3.Visible = False
            TextBox4.Visible = False
        Else
            TrackBar3.Visible = True
            TextBox4.Visible = True
        End If

        chargerLot(Selectjackpot)
    End Sub

    Public Sub chargerLot(ByVal Selectjackpot As String)
        Using ojacpot As New BL_Lotto.BlJackot(source)

            'id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp`

            oDataSet = ojacpot.Getlot(Selectjackpot)
            TbGold.Text = oDataSet.Tables("lot_jackpot").Rows(0).Item("montant_jackp").ToString
            tbNomLotGold.Text = oDataSet.Tables("lot_jackpot").Rows(0).Item("nom_materiel").ToString
            tb_id_jack.Text = oDataSet.Tables("lot_jackpot").Rows(0).Item("id_jack").ToString
            Tb_id_lot.Text = oDataSet.Tables("lot_jackpot").Rows(0).Item("id_lot").ToString
            pb_photo_loto.ImageLocation = Application.StartupPath & "\photo\" & oDataSet.Tables("lot_jackpot").Rows(0).Item("photo").ToString
            Tb_etat_gold.ReadOnly = False
            If (oDataSet.Tables("lot_jackpot").Rows(0).Item("id_jack").ToString = ID_GOLD) Then

                Tb_etat_gold.Text = ojacpot.GetEtatJackpot(ID_GOLD)
                TrackBar3.Value = GOLD_POT
                TextBox4.Text = GOLD_POT & " %"

            ElseIf (oDataSet.Tables("lot_jackpot").Rows(0).Item("id_jack").ToString = ID_IRON)

                Tb_etat_gold.Text = ojacpot.GetEtatJackpot(ID_IRON)
                TrackBar3.Value = IRON_POT
                TextBox4.Text = IRON_POT & " %"
            End If

            If TrackBar3.Visible = True Then
                If (CInt(Tb_etat_gold.Text) < CInt(TbGold.Text)) Then
                    ProgressBar1.Value = (CInt(Tb_etat_gold.Text) / CInt(TbGold.Text)) * 100
                Else
                    ProgressBar1.Value = 100
                End If
            Else
                Tb_etat_gold.ReadOnly = True
                ProgressBar1.Value = 0
            End If

            If (CBool(oDataSet.Tables("lot_jackpot").Rows(0).Item("is_matirial"))) Then

                tb_gold_mat.Text = "OUI"
                is_material_gold = 1
                Panel7.Visible = True
                tbNomLotGold.Visible = True
            Else
                tb_gold_mat.Text = "NON"
                is_material_gold = 0
                Panel7.Visible = False
                tbNomLotGold.Visible = False
            End If

        End Using
    End Sub

    Private Sub dtg_lot_gold_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtg_lot_gold.CellClick


    End Sub

    Private Sub dtg_lot_silver_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtg_lot_silver.CellContentClick

    End Sub

    Private Sub dtg_lot_gold_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtg_lot_gold.CellContentClick

    End Sub



    Private Sub dtg_lot_silver_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dtg_lot_silver.CellMouseClick
        If dtg_lot_silver.Rows.Count <= 0 Then Exit Sub
        Selectsilver = CStr(dtg_lot_silver.Rows(dtg_lot_silver.CurrentRow.Index).Cells(1).Value)

        Dim cell As DataGridViewImageCell = CType(dtg_lot_silver.Rows(dtg_lot_silver.CurrentRow.Index).Cells(0), DataGridViewImageCell)
        If (cell.Value Is blank) Then
            TrackBar2.Visible = False
            TextBox2.Visible = False
        Else
            TrackBar2.Visible = True
            TextBox2.Visible = True
        End If

        If (dtg_lot_silver.Rows(dtg_lot_silver.CurrentRow.Index).Cells(1).Value.ToString = 0) Then
            Tb_min_jack.ReadOnly = False
            Tb_max_jack.ReadOnly = False
        Else
            Tb_min_jack.ReadOnly = True
            Tb_max_jack.ReadOnly = True
        End If

        chargerLotSilver(Selectsilver)
    End Sub



    Public Sub chargerLotSilver(ByVal Selectsilver As String)
        Using ojacpot As New BL_Lotto.BlJackot(source)

            'id_lot`,`is_matirial`,`nom_materiel`,`montant_jackp`

            oDataSet = ojacpot.Getlot(Selectsilver)
            Tbsilver.Text = oDataSet.Tables("lot_jackpot").Rows(0).Item("montant_jackp").ToString
            tbNomLotSilver.Text = oDataSet.Tables("lot_jackpot").Rows(0).Item("nom_materiel").ToString
            Tb_id_lot_silver.Text = oDataSet.Tables("lot_jackpot").Rows(0).Item("id_lot").ToString

            pb_photo_lotbronze.ImageLocation = Application.StartupPath & "\photo\" & oDataSet.Tables("lot_jackpot").Rows(0).Item("photo").ToString

            If (CBool(oDataSet.Tables("lot_jackpot").Rows(0).Item("is_matirial"))) Then
                is_material_silver = 1
            Else
                is_material_silver = 0
            End If



            If (oDataSet.Tables("lot_jackpot").Rows(0).Item("id_jack").ToString = ID_SILVER) Then
                Tb_etat_bonze.ReadOnly = False
                Tb_etat_bonze.Text = ojacpot.GetEtatJackpot(ID_SILVER)
                TrackBar2.Value = SILVER_POT
                TextBox2.Text = SILVER_POT & " %"
            End If

            If TrackBar2.Visible = True Then
                If (CInt(Tb_etat_bonze.Text) < CInt(Tbsilver.Text)) Then
                    ProgressBar2.Value = (CInt(Tb_etat_bonze.Text) / CInt(Tbsilver.Text)) * 100
                Else
                    ProgressBar2.Value = 100
                End If
            Else
                Tb_etat_bonze.ReadOnly = True
                ProgressBar2.Value = 0
            End If

        End Using
    End Sub
End Class