﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class setting
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TbNameImp = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cmbDevices = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TrackBar1 = New System.Windows.Forms.TrackBar()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.pb_photo_loto = New System.Windows.Forms.PictureBox()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.dtg_lot_gold = New System.Windows.Forms.DataGridView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Tb_id_lot = New System.Windows.Forms.TextBox()
        Me.tb_id_jack = New System.Windows.Forms.TextBox()
        Me.Tb_etat_gold = New System.Windows.Forms.TextBox()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TrackBar3 = New System.Windows.Forms.TrackBar()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.tb_gold_mat = New System.Windows.Forms.TextBox()
        Me.tbNomLotGold = New System.Windows.Forms.TextBox()
        Me.TbGold = New System.Windows.Forms.TextBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.pb_photo_lotbronze = New System.Windows.Forms.PictureBox()
        Me.Tb_id_lot_silver = New System.Windows.Forms.TextBox()
        Me.ProgressBar2 = New System.Windows.Forms.ProgressBar()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.dtg_lot_silver = New System.Windows.Forms.DataGridView()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Tb_etat_bonze = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Tb_max_jack = New System.Windows.Forms.TextBox()
        Me.Tb_min_jack = New System.Windows.Forms.TextBox()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tb_jackmat = New System.Windows.Forms.TextBox()
        Me.tbNomLotSilver = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Tbsilver = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TrackBar2 = New System.Windows.Forms.TrackBar()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BtValider = New System.Windows.Forms.Button()
        Me.TbBronze = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Tb_etat_silver = New System.Windows.Forms.TextBox()
        Me.TrackBar4 = New System.Windows.Forms.TrackBar()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.tbNomLotBronze = New System.Windows.Forms.TextBox()
        Me.tb_bronze_mat = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.dtg_lot_bronze = New System.Windows.Forms.DataGridView()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.pb_photo_loto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel23.SuspendLayout()
        Me.Panel22.SuspendLayout()
        Me.Panel19.SuspendLayout()
        Me.Panel20.SuspendLayout()
        CType(Me.dtg_lot_gold, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.Panel17.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.TrackBar3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.pb_photo_lotbronze, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel21.SuspendLayout()
        CType(Me.dtg_lot_silver, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.Panel18.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.TrackBar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.TrackBar4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtg_lot_bronze, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(75, 152)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 21)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Imprimante"
        Me.Label1.Visible = False
        '
        'TbNameImp
        '
        Me.TbNameImp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TbNameImp.Location = New System.Drawing.Point(201, 151)
        Me.TbNameImp.Name = "TbNameImp"
        Me.TbNameImp.Size = New System.Drawing.Size(222, 26)
        Me.TbNameImp.TabIndex = 1
        Me.TbNameImp.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel10)
        Me.GroupBox1.Controls.Add(Me.cmbDevices)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Controls.Add(Me.TrackBar1)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.TbNameImp)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox1.Location = New System.Drawing.Point(77, 26)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(622, 195)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Transparent
        Me.Panel10.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel10.Controls.Add(Me.Label11)
        Me.Panel10.Location = New System.Drawing.Point(34, 102)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(160, 27)
        Me.Panel10.TabIndex = 6
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(10, 4)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(146, 21)
        Me.Label11.TabIndex = 3
        Me.Label11.Text = "Selection D'ecran"
        '
        'cmbDevices
        '
        Me.cmbDevices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDevices.FormattingEnabled = True
        Me.cmbDevices.Location = New System.Drawing.Point(194, 103)
        Me.cmbDevices.Name = "cmbDevices"
        Me.cmbDevices.Size = New System.Drawing.Size(174, 26)
        Me.cmbDevices.TabIndex = 7
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(6, 51)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(188, 27)
        Me.Panel1.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(10, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(174, 21)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Prélèvement Marché"
        '
        'TrackBar1
        '
        Me.TrackBar1.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TrackBar1.Location = New System.Drawing.Point(186, 51)
        Me.TrackBar1.Maximum = 100
        Me.TrackBar1.Name = "TrackBar1"
        Me.TrackBar1.Size = New System.Drawing.Size(278, 45)
        Me.TrackBar1.TabIndex = 4
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(470, 57)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(61, 26)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1048, 652)
        Me.TabControl1.TabIndex = 3
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TabPage3.Controls.Add(Me.GroupBox4)
        Me.TabPage3.Controls.Add(Me.Panel23)
        Me.TabPage3.Controls.Add(Me.ProgressBar1)
        Me.TabPage3.Controls.Add(Me.Panel22)
        Me.TabPage3.Controls.Add(Me.dtg_lot_gold)
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Controls.Add(Me.Button8)
        Me.TabPage3.Controls.Add(Me.Button9)
        Me.TabPage3.Controls.Add(Me.Button5)
        Me.TabPage3.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage3.Location = New System.Drawing.Point(4, 27)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1040, 621)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Jackpot"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.pb_photo_loto)
        Me.GroupBox4.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox4.ForeColor = System.Drawing.Color.White
        Me.GroupBox4.Location = New System.Drawing.Point(26, 138)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(216, 215)
        Me.GroupBox4.TabIndex = 15
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Photo"
        '
        'pb_photo_loto
        '
        Me.pb_photo_loto.Image = Global.PL_Lotto.My.Resources.Resources.gift_logo
        Me.pb_photo_loto.Location = New System.Drawing.Point(6, 36)
        Me.pb_photo_loto.Name = "pb_photo_loto"
        Me.pb_photo_loto.Size = New System.Drawing.Size(204, 173)
        Me.pb_photo_loto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pb_photo_loto.TabIndex = 14
        Me.pb_photo_loto.TabStop = False
        '
        'Panel23
        '
        Me.Panel23.BackColor = System.Drawing.Color.Transparent
        Me.Panel23.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel23.Controls.Add(Me.Label23)
        Me.Panel23.Location = New System.Drawing.Point(393, 39)
        Me.Panel23.Name = "Panel23"
        Me.Panel23.Size = New System.Drawing.Size(155, 42)
        Me.Panel23.TabIndex = 13
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(37, 4)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(114, 23)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "Progression"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(548, 39)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(253, 42)
        Me.ProgressBar1.TabIndex = 10
        '
        'Panel22
        '
        Me.Panel22.BackColor = System.Drawing.Color.DarkSlateGray
        Me.Panel22.Controls.Add(Me.Label24)
        Me.Panel22.Controls.Add(Me.Panel19)
        Me.Panel22.Controls.Add(Me.Panel20)
        Me.Panel22.Location = New System.Drawing.Point(26, 27)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Size = New System.Drawing.Size(279, 87)
        Me.Panel22.TabIndex = 12
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(91, 8)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(91, 23)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "Legende"
        '
        'Panel19
        '
        Me.Panel19.BackColor = System.Drawing.Color.Gold
        Me.Panel19.Controls.Add(Me.Label21)
        Me.Panel19.Location = New System.Drawing.Point(22, 42)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(112, 42)
        Me.Panel19.TabIndex = 9
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(10, 10)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(92, 23)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Cagnote"
        '
        'Panel20
        '
        Me.Panel20.BackColor = System.Drawing.Color.Silver
        Me.Panel20.Controls.Add(Me.Label22)
        Me.Panel20.Location = New System.Drawing.Point(152, 42)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(112, 42)
        Me.Panel20.TabIndex = 9
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(25, 10)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(59, 23)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "Silver"
        '
        'dtg_lot_gold
        '
        Me.dtg_lot_gold.AllowUserToAddRows = False
        Me.dtg_lot_gold.AllowUserToDeleteRows = False
        Me.dtg_lot_gold.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dtg_lot_gold.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtg_lot_gold.Location = New System.Drawing.Point(26, 384)
        Me.dtg_lot_gold.Name = "dtg_lot_gold"
        Me.dtg_lot_gold.ReadOnly = True
        Me.dtg_lot_gold.Size = New System.Drawing.Size(835, 215)
        Me.dtg_lot_gold.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Panel17)
        Me.GroupBox3.Controls.Add(Me.Tb_id_lot)
        Me.GroupBox3.Controls.Add(Me.tb_id_jack)
        Me.GroupBox3.Controls.Add(Me.Tb_etat_gold)
        Me.GroupBox3.Controls.Add(Me.Panel7)
        Me.GroupBox3.Controls.Add(Me.Panel5)
        Me.GroupBox3.Controls.Add(Me.Panel6)
        Me.GroupBox3.Controls.Add(Me.Panel3)
        Me.GroupBox3.Controls.Add(Me.TrackBar3)
        Me.GroupBox3.Controls.Add(Me.TextBox4)
        Me.GroupBox3.Controls.Add(Me.tb_gold_mat)
        Me.GroupBox3.Controls.Add(Me.tbNomLotGold)
        Me.GroupBox3.Controls.Add(Me.TbGold)
        Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox3.Location = New System.Drawing.Point(248, 148)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(774, 205)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        '
        'Panel17
        '
        Me.Panel17.BackColor = System.Drawing.Color.Transparent
        Me.Panel17.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel17.Controls.Add(Me.Label19)
        Me.Panel17.Location = New System.Drawing.Point(410, 61)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(155, 27)
        Me.Panel17.TabIndex = 8
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(107, 3)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(44, 21)
        Me.Label19.TabIndex = 3
        Me.Label19.Text = "Etat"
        '
        'Tb_id_lot
        '
        Me.Tb_id_lot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Tb_id_lot.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tb_id_lot.Location = New System.Drawing.Point(688, 173)
        Me.Tb_id_lot.Name = "Tb_id_lot"
        Me.Tb_id_lot.ReadOnly = True
        Me.Tb_id_lot.Size = New System.Drawing.Size(80, 26)
        Me.Tb_id_lot.TabIndex = 7
        Me.Tb_id_lot.Visible = False
        '
        'tb_id_jack
        '
        Me.tb_id_jack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tb_id_jack.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_id_jack.Location = New System.Drawing.Point(592, 173)
        Me.tb_id_jack.Name = "tb_id_jack"
        Me.tb_id_jack.ReadOnly = True
        Me.tb_id_jack.Size = New System.Drawing.Size(80, 26)
        Me.tb_id_jack.TabIndex = 7
        Me.tb_id_jack.Visible = False
        '
        'Tb_etat_gold
        '
        Me.Tb_etat_gold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Tb_etat_gold.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tb_etat_gold.Location = New System.Drawing.Point(564, 62)
        Me.Tb_etat_gold.Name = "Tb_etat_gold"
        Me.Tb_etat_gold.Size = New System.Drawing.Size(80, 26)
        Me.Tb_etat_gold.TabIndex = 7
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Transparent
        Me.Panel7.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.Label8)
        Me.Panel7.Location = New System.Drawing.Point(17, 63)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(155, 27)
        Me.Panel7.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(49, 3)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(101, 21)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Nom Du Lot"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Panel5.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Location = New System.Drawing.Point(411, 18)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(155, 27)
        Me.Panel5.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(71, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 21)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Montant"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Transparent
        Me.Panel6.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.Label6)
        Me.Panel6.Location = New System.Drawing.Point(17, 19)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(155, 27)
        Me.Panel6.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(8, 3)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(143, 21)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Jackpot Materiel"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Location = New System.Drawing.Point(17, 108)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(155, 27)
        Me.Panel3.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(41, 3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 21)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Prélèvement"
        '
        'TrackBar3
        '
        Me.TrackBar3.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TrackBar3.Location = New System.Drawing.Point(164, 105)
        Me.TrackBar3.Maximum = 100
        Me.TrackBar3.Name = "TrackBar3"
        Me.TrackBar3.Size = New System.Drawing.Size(278, 45)
        Me.TrackBar3.TabIndex = 4
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.Location = New System.Drawing.Point(448, 110)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(61, 26)
        Me.TextBox4.TabIndex = 1
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb_gold_mat
        '
        Me.tb_gold_mat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tb_gold_mat.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_gold_mat.Location = New System.Drawing.Point(171, 20)
        Me.tb_gold_mat.Name = "tb_gold_mat"
        Me.tb_gold_mat.ReadOnly = True
        Me.tb_gold_mat.Size = New System.Drawing.Size(78, 26)
        Me.tb_gold_mat.TabIndex = 1
        '
        'tbNomLotGold
        '
        Me.tbNomLotGold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNomLotGold.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNomLotGold.Location = New System.Drawing.Point(171, 63)
        Me.tbNomLotGold.Name = "tbNomLotGold"
        Me.tbNomLotGold.Size = New System.Drawing.Size(186, 26)
        Me.tbNomLotGold.TabIndex = 1
        '
        'TbGold
        '
        Me.TbGold.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TbGold.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbGold.Location = New System.Drawing.Point(564, 18)
        Me.TbGold.Name = "TbGold"
        Me.TbGold.Size = New System.Drawing.Size(197, 26)
        Me.TbGold.TabIndex = 1
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Crimson
        Me.Button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.Location = New System.Drawing.Point(880, 496)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(129, 49)
        Me.Button8.TabIndex = 5
        Me.Button8.Text = "Supprimer"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.Crimson
        Me.Button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Button9.ForeColor = System.Drawing.Color.White
        Me.Button9.Location = New System.Drawing.Point(880, 415)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(129, 49)
        Me.Button9.TabIndex = 5
        Me.Button9.Text = "Ajouter"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Crimson
        Me.Button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(800, 95)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(222, 47)
        Me.Button5.TabIndex = 8
        Me.Button5.Text = "Enregistrer"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TabPage2.Controls.Add(Me.GroupBox5)
        Me.TabPage2.Controls.Add(Me.Tb_id_lot_silver)
        Me.TabPage2.Controls.Add(Me.ProgressBar2)
        Me.TabPage2.Controls.Add(Me.Panel21)
        Me.TabPage2.Controls.Add(Me.dtg_lot_silver)
        Me.TabPage2.Controls.Add(Me.Button7)
        Me.TabPage2.Controls.Add(Me.Button6)
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Controls.Add(Me.Button3)
        Me.TabPage2.Controls.Add(Me.Panel2)
        Me.TabPage2.Controls.Add(Me.TextBox2)
        Me.TabPage2.Controls.Add(Me.TrackBar2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 27)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1040, 621)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Bronze"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.pb_photo_lotbronze)
        Me.GroupBox5.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox5.ForeColor = System.Drawing.Color.White
        Me.GroupBox5.Location = New System.Drawing.Point(19, 154)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(216, 215)
        Me.GroupBox5.TabIndex = 17
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Photo"
        '
        'pb_photo_lotbronze
        '
        Me.pb_photo_lotbronze.Image = Global.PL_Lotto.My.Resources.Resources.gift_logo
        Me.pb_photo_lotbronze.Location = New System.Drawing.Point(6, 36)
        Me.pb_photo_lotbronze.Name = "pb_photo_lotbronze"
        Me.pb_photo_lotbronze.Size = New System.Drawing.Size(204, 173)
        Me.pb_photo_lotbronze.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pb_photo_lotbronze.TabIndex = 14
        Me.pb_photo_lotbronze.TabStop = False
        '
        'Tb_id_lot_silver
        '
        Me.Tb_id_lot_silver.Location = New System.Drawing.Point(932, 6)
        Me.Tb_id_lot_silver.Name = "Tb_id_lot_silver"
        Me.Tb_id_lot_silver.Size = New System.Drawing.Size(100, 26)
        Me.Tb_id_lot_silver.TabIndex = 16
        Me.Tb_id_lot_silver.Visible = False
        '
        'ProgressBar2
        '
        Me.ProgressBar2.Location = New System.Drawing.Point(201, 86)
        Me.ProgressBar2.Name = "ProgressBar2"
        Me.ProgressBar2.Size = New System.Drawing.Size(277, 42)
        Me.ProgressBar2.TabIndex = 15
        '
        'Panel21
        '
        Me.Panel21.BackColor = System.Drawing.Color.Transparent
        Me.Panel21.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel21.Controls.Add(Me.Label25)
        Me.Panel21.Location = New System.Drawing.Point(47, 86)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Size = New System.Drawing.Size(155, 42)
        Me.Panel21.TabIndex = 14
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(37, 8)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(114, 23)
        Me.Label25.TabIndex = 0
        Me.Label25.Text = "Progression"
        '
        'dtg_lot_silver
        '
        Me.dtg_lot_silver.AllowUserToAddRows = False
        Me.dtg_lot_silver.AllowUserToDeleteRows = False
        Me.dtg_lot_silver.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dtg_lot_silver.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.DarkOrange
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtg_lot_silver.DefaultCellStyle = DataGridViewCellStyle9
        Me.dtg_lot_silver.Location = New System.Drawing.Point(19, 387)
        Me.dtg_lot_silver.Name = "dtg_lot_silver"
        Me.dtg_lot_silver.ReadOnly = True
        Me.dtg_lot_silver.Size = New System.Drawing.Size(816, 215)
        Me.dtg_lot_silver.TabIndex = 0
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.Crimson
        Me.Button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Button7.ForeColor = System.Drawing.Color.White
        Me.Button7.Location = New System.Drawing.Point(867, 508)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(132, 55)
        Me.Button7.TabIndex = 5
        Me.Button7.Text = "Supprimer"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Crimson
        Me.Button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.Location = New System.Drawing.Point(867, 425)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(132, 55)
        Me.Button6.TabIndex = 5
        Me.Button6.Text = "Ajouter"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Panel18)
        Me.GroupBox2.Controls.Add(Me.Tb_etat_bonze)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Panel8)
        Me.GroupBox2.Controls.Add(Me.Panel15)
        Me.GroupBox2.Controls.Add(Me.Tb_max_jack)
        Me.GroupBox2.Controls.Add(Me.Tb_min_jack)
        Me.GroupBox2.Controls.Add(Me.Panel9)
        Me.GroupBox2.Controls.Add(Me.tb_jackmat)
        Me.GroupBox2.Controls.Add(Me.tbNomLotSilver)
        Me.GroupBox2.Controls.Add(Me.Panel4)
        Me.GroupBox2.Controls.Add(Me.Tbsilver)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox2.Font = New System.Drawing.Font("Showcard Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(241, 154)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(770, 213)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'Panel18
        '
        Me.Panel18.BackColor = System.Drawing.Color.Transparent
        Me.Panel18.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel18.Controls.Add(Me.Label20)
        Me.Panel18.Location = New System.Drawing.Point(567, 156)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(106, 27)
        Me.Panel18.TabIndex = 12
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(58, 3)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(44, 21)
        Me.Label20.TabIndex = 3
        Me.Label20.Text = "Etat"
        '
        'Tb_etat_bonze
        '
        Me.Tb_etat_bonze.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Tb_etat_bonze.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tb_etat_bonze.Location = New System.Drawing.Point(675, 156)
        Me.Tb_etat_bonze.Name = "Tb_etat_bonze"
        Me.Tb_etat_bonze.Size = New System.Drawing.Size(80, 26)
        Me.Tb_etat_bonze.TabIndex = 11
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(340, 54)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(21, 21)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "à"
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Transparent
        Me.Panel8.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.Label9)
        Me.Panel8.Location = New System.Drawing.Point(38, 107)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(176, 27)
        Me.Panel8.TabIndex = 9
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(71, 2)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(101, 21)
        Me.Label9.TabIndex = 3
        Me.Label9.Text = "Nom Du Lot"
        '
        'Panel15
        '
        Me.Panel15.BackColor = System.Drawing.Color.Transparent
        Me.Panel15.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel15.Controls.Add(Me.Label16)
        Me.Panel15.Location = New System.Drawing.Point(38, 50)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(176, 27)
        Me.Panel15.TabIndex = 10
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(19, 3)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(153, 21)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "Intervalle Jackpot"
        '
        'Tb_max_jack
        '
        Me.Tb_max_jack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Tb_max_jack.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tb_max_jack.Location = New System.Drawing.Point(378, 51)
        Me.Tb_max_jack.Name = "Tb_max_jack"
        Me.Tb_max_jack.Size = New System.Drawing.Size(101, 26)
        Me.Tb_max_jack.TabIndex = 8
        '
        'Tb_min_jack
        '
        Me.Tb_min_jack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Tb_min_jack.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tb_min_jack.Location = New System.Drawing.Point(220, 51)
        Me.Tb_min_jack.Name = "Tb_min_jack"
        Me.Tb_min_jack.Size = New System.Drawing.Size(101, 26)
        Me.Tb_min_jack.TabIndex = 8
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Transparent
        Me.Panel9.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel9.Controls.Add(Me.Label10)
        Me.Panel9.Location = New System.Drawing.Point(59, 267)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(155, 27)
        Me.Panel9.TabIndex = 10
        Me.Panel9.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(17, 3)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(118, 19)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Jackpot Materiel :"
        '
        'tb_jackmat
        '
        Me.tb_jackmat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tb_jackmat.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_jackmat.Location = New System.Drawing.Point(213, 268)
        Me.tb_jackmat.Name = "tb_jackmat"
        Me.tb_jackmat.ReadOnly = True
        Me.tb_jackmat.Size = New System.Drawing.Size(120, 26)
        Me.tb_jackmat.TabIndex = 8
        Me.tb_jackmat.Visible = False
        '
        'tbNomLotSilver
        '
        Me.tbNomLotSilver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNomLotSilver.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNomLotSilver.Location = New System.Drawing.Point(213, 107)
        Me.tbNomLotSilver.Name = "tbNomLotSilver"
        Me.tbNomLotSilver.Size = New System.Drawing.Size(339, 26)
        Me.tbNomLotSilver.TabIndex = 8
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Location = New System.Drawing.Point(38, 156)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(176, 27)
        Me.Panel4.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(92, 2)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 21)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Montant"
        '
        'Tbsilver
        '
        Me.Tbsilver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Tbsilver.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tbsilver.Location = New System.Drawing.Point(213, 156)
        Me.Tbsilver.Name = "Tbsilver"
        Me.Tbsilver.Size = New System.Drawing.Size(339, 26)
        Me.Tbsilver.TabIndex = 1
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Crimson
        Me.Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(855, 106)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(156, 55)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "ENREGISTRER"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(47, 18)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(155, 42)
        Me.Panel2.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(25, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(126, 23)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Prélèvement"
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.TextBox2.Location = New System.Drawing.Point(479, 25)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(61, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TrackBar2
        '
        Me.TrackBar2.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TrackBar2.Location = New System.Drawing.Point(195, 18)
        Me.TrackBar2.Maximum = 100
        Me.TrackBar2.Name = "TrackBar2"
        Me.TrackBar2.Size = New System.Drawing.Size(283, 45)
        Me.TrackBar2.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.BtValider)
        Me.TabPage1.ForeColor = System.Drawing.Color.White
        Me.TabPage1.Location = New System.Drawing.Point(4, 27)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1040, 621)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "% Marché"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Crimson
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.Button1.Location = New System.Drawing.Point(871, 53)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(111, 51)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Annuler"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'BtValider
        '
        Me.BtValider.BackColor = System.Drawing.Color.Crimson
        Me.BtValider.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtValider.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtValider.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold)
        Me.BtValider.Location = New System.Drawing.Point(726, 53)
        Me.BtValider.Name = "BtValider"
        Me.BtValider.Size = New System.Drawing.Size(101, 47)
        Me.BtValider.TabIndex = 2
        Me.BtValider.Text = "Valider"
        Me.BtValider.UseVisualStyleBackColor = False
        '
        'TbBronze
        '
        Me.TbBronze.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TbBronze.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TbBronze.Location = New System.Drawing.Point(210, 183)
        Me.TbBronze.Name = "TbBronze"
        Me.TbBronze.Size = New System.Drawing.Size(339, 26)
        Me.TbBronze.TabIndex = 1
        '
        'TextBox6
        '
        Me.TextBox6.BackColor = System.Drawing.SystemColors.Window
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.TextBox6.Location = New System.Drawing.Point(488, 142)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(61, 20)
        Me.TextBox6.TabIndex = 1
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Tb_etat_silver
        '
        Me.Tb_etat_silver.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Tb_etat_silver.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tb_etat_silver.Location = New System.Drawing.Point(660, 184)
        Me.Tb_etat_silver.Name = "Tb_etat_silver"
        Me.Tb_etat_silver.ReadOnly = True
        Me.Tb_etat_silver.Size = New System.Drawing.Size(80, 26)
        Me.Tb_etat_silver.TabIndex = 1
        '
        'TrackBar4
        '
        Me.TrackBar4.BackColor = System.Drawing.Color.DarkSlateGray
        Me.TrackBar4.Location = New System.Drawing.Point(204, 135)
        Me.TrackBar4.Maximum = 100
        Me.TrackBar4.Name = "TrackBar4"
        Me.TrackBar4.Size = New System.Drawing.Size(278, 45)
        Me.TrackBar4.TabIndex = 4
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(36, 4)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(83, 19)
        Me.Label15.TabIndex = 3
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(46, 3)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(60, 19)
        Me.Label14.TabIndex = 3
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(30, 3)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(33, 19)
        Me.Label18.TabIndex = 3
        '
        'tbNomLotBronze
        '
        Me.tbNomLotBronze.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNomLotBronze.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNomLotBronze.Location = New System.Drawing.Point(213, 85)
        Me.tbNomLotBronze.Name = "tbNomLotBronze"
        Me.tbNomLotBronze.Size = New System.Drawing.Size(336, 26)
        Me.tbNomLotBronze.TabIndex = 8
        Me.tbNomLotBronze.Visible = False
        '
        'tb_bronze_mat
        '
        Me.tb_bronze_mat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tb_bronze_mat.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_bronze_mat.Location = New System.Drawing.Point(213, 43)
        Me.tb_bronze_mat.Name = "tb_bronze_mat"
        Me.tb_bronze_mat.ReadOnly = True
        Me.tb_bronze_mat.Size = New System.Drawing.Size(120, 26)
        Me.tb_bronze_mat.TabIndex = 8
        Me.tb_bronze_mat.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(17, 3)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(118, 19)
        Me.Label13.TabIndex = 3
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(29, 3)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(94, 19)
        Me.Label12.TabIndex = 3
        '
        'dtg_lot_bronze
        '
        Me.dtg_lot_bronze.AllowUserToAddRows = False
        Me.dtg_lot_bronze.AllowUserToDeleteRows = False
        Me.dtg_lot_bronze.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtg_lot_bronze.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dtg_lot_bronze.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtg_lot_bronze.DefaultCellStyle = DataGridViewCellStyle11
        Me.dtg_lot_bronze.Location = New System.Drawing.Point(16, 57)
        Me.dtg_lot_bronze.Name = "dtg_lot_bronze"
        Me.dtg_lot_bronze.ReadOnly = True
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Century Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtg_lot_bronze.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dtg_lot_bronze.Size = New System.Drawing.Size(772, 215)
        Me.dtg_lot_bronze.TabIndex = 0
        '
        'Panel14
        '
        Me.Panel14.BackColor = System.Drawing.Color.Transparent
        Me.Panel14.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel14.Location = New System.Drawing.Point(59, 136)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(155, 27)
        Me.Panel14.TabIndex = 6
        '
        'Panel13
        '
        Me.Panel13.BackColor = System.Drawing.Color.Transparent
        Me.Panel13.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel13.Location = New System.Drawing.Point(59, 183)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(155, 27)
        Me.Panel13.TabIndex = 6
        '
        'Panel16
        '
        Me.Panel16.BackColor = System.Drawing.Color.Transparent
        Me.Panel16.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel16.Location = New System.Drawing.Point(555, 183)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(106, 27)
        Me.Panel16.TabIndex = 6
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.Transparent
        Me.Panel12.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel12.Location = New System.Drawing.Point(59, 42)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(155, 27)
        Me.Panel12.TabIndex = 10
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.Transparent
        Me.Panel11.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bakcolor
        Me.Panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel11.Location = New System.Drawing.Point(59, 85)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(155, 27)
        Me.Panel11.TabIndex = 9
        '
        'Button11
        '
        Me.Button11.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bg_bt_save
        Me.Button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Font = New System.Drawing.Font("Showcard Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.White
        Me.Button11.Location = New System.Drawing.Point(833, 94)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(98, 49)
        Me.Button11.TabIndex = 5
        Me.Button11.Text = "Ajouter"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.BackgroundImage = Global.PL_Lotto.My.Resources.Resources.bg_bt_save
        Me.Button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Showcard Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.ForeColor = System.Drawing.Color.White
        Me.Button10.Location = New System.Drawing.Point(833, 175)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(98, 49)
        Me.Button10.TabIndex = 5
        Me.Button10.Text = "Supprimer"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'setting
        '
        Me.AcceptButton = Me.BtValider
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkSlateGray
        Me.ClientSize = New System.Drawing.Size(1048, 652)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "setting"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.pb_photo_loto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel23.ResumeLayout(False)
        Me.Panel23.PerformLayout()
        Me.Panel22.ResumeLayout(False)
        Me.Panel22.PerformLayout()
        Me.Panel19.ResumeLayout(False)
        Me.Panel19.PerformLayout()
        Me.Panel20.ResumeLayout(False)
        Me.Panel20.PerformLayout()
        CType(Me.dtg_lot_gold, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.TrackBar3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.pb_photo_lotbronze, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel21.ResumeLayout(False)
        Me.Panel21.PerformLayout()
        CType(Me.dtg_lot_silver, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.TrackBar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        CType(Me.TrackBar4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtg_lot_bronze, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TbNameImp As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtValider As System.Windows.Forms.Button
    Friend WithEvents TrackBar1 As System.Windows.Forms.TrackBar
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TrackBar2 As System.Windows.Forms.TrackBar
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Tbsilver As System.Windows.Forms.TextBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TrackBar3 As System.Windows.Forms.TrackBar
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TbGold As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbNomLotGold As System.Windows.Forms.TextBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tbNomLotSilver As System.Windows.Forms.TextBox
    Friend WithEvents dtg_lot_silver As DataGridView
    Friend WithEvents Button7 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents tb_jackmat As TextBox
    Friend WithEvents dtg_lot_gold As DataGridView
    Friend WithEvents Button8 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents tb_gold_mat As TextBox
    Friend WithEvents Panel10 As Panel
    Friend WithEvents Label11 As Label
    Friend WithEvents cmbDevices As ComboBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Panel15 As Panel
    Friend WithEvents Label16 As Label
    Friend WithEvents Tb_max_jack As TextBox
    Friend WithEvents Tb_min_jack As TextBox
    Friend WithEvents Panel17 As Panel
    Friend WithEvents Label19 As Label
    Friend WithEvents Panel18 As Panel
    Friend WithEvents Label20 As Label
    Friend WithEvents Tb_etat_bonze As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents Panel20 As Panel
    Friend WithEvents Label22 As Label
    Friend WithEvents Panel19 As Panel
    Friend WithEvents Label21 As Label
    Friend WithEvents tb_id_jack As TextBox
    Friend WithEvents Tb_id_lot As TextBox
    Friend WithEvents TbBronze As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents Tb_etat_silver As TextBox
    Friend WithEvents TrackBar4 As TrackBar
    Friend WithEvents Panel14 As Panel
    Friend WithEvents Label15 As Label
    Friend WithEvents Panel13 As Panel
    Friend WithEvents Label14 As Label
    Friend WithEvents Panel16 As Panel
    Friend WithEvents Label18 As Label
    Friend WithEvents tbNomLotBronze As TextBox
    Friend WithEvents tb_bronze_mat As TextBox
    Friend WithEvents Panel12 As Panel
    Friend WithEvents Label13 As Label
    Friend WithEvents Panel11 As Panel
    Friend WithEvents Label12 As Label
    Friend WithEvents Button11 As Button
    Friend WithEvents Button10 As Button
    Friend WithEvents dtg_lot_bronze As DataGridView
    Friend WithEvents Tb_etat_gold As TextBox
    Friend WithEvents Panel22 As Panel
    Friend WithEvents Label24 As Label
    Friend WithEvents Panel23 As Panel
    Friend WithEvents ProgressBar2 As ProgressBar
    Friend WithEvents Panel21 As Panel
    Friend WithEvents Label25 As Label
    Friend WithEvents Tb_id_lot_silver As TextBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents pb_photo_loto As PictureBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents pb_photo_lotbronze As PictureBox
End Class
