﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Passez en revue les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("ELITE LOTO")> 
<Assembly: AssemblyDescription("Developed by Mosa Team")> 
<Assembly: AssemblyCompany("Mosa Ent.")> 
<Assembly: AssemblyProduct("ELITE LOTO")> 
<Assembly: AssemblyCopyright("Copyright © Mosa 2014 All Rights Reserved")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("2651b9df-b74c-4493-b799-d24444122e13")> 

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.1.0.0")> 
<Assembly: AssemblyFileVersion("1.1.0.0")> 
