﻿Public Class BlTriche
    Implements IDisposable
    Private oDlTriche As DL_Lotto.DlTriche
    Private disposedValue As Boolean = False        ' Détection des appels rédondants

#Region "Constructeur et Destructeur"
    Public Sub New(ByVal Constring As String)
        oDlTriche = New DL_Lotto.DlTriche(Constring)
    End Sub
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: Libérer les ressources non gérées lors d'appel expplicite
            End If
            oDlTriche.Dispose()
            oDlTriche = Nothing
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

#Region " Public Ticket Functions "
    Public Function RecupNumJouer(ByVal NumTir As Integer) As DataSet
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour recuperer la liste numero au tirage No:"NumTir"
            Return oDlTriche.RecupNumJouer(NumTir)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function
#End Region
End Class
