﻿Public Class BlUser
    Implements IDisposable
    Private oDLUser As DL_Lotto.DlUser
    Private disposedValue As Boolean = False        ' Détection des appels rédondants

    ' IDisposable
#Region "Constructeur et Destructeur"
    Public Sub New(ByVal Constring As String)
        oDLUser = New DL_Lotto.DlUser(Constring)
    End Sub
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: Libérer les ressources non gérées lors d'appel expplicite
            End If
            oDLUser.Dispose()
            oDLUser = Nothing
        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region
#Region " Public User Functions "
    Function DejaUser(ByVal UserName As String) As Boolean
        Try
            Dim UserDataSet As DataSet
            UserDataSet = New DataSet
            UserDataSet = oDLUser.UserParID(UserName)
            If UserDataSet.Tables("users").Rows.Count > 0 Then
                DejaUser = True
            Else
                DejaUser = False
            End If
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function NvDSUser() As DataSet
        Try
            'instancie un nouveau objet DataSet 
            NvDSUser = New DataSet

            'crée un objet Datatable 
            Dim objDataTable As DataTable = NvDSUser.Tables.Add("users")

            'crée un objet DataColumn 
            Dim objDataColumn As DataColumn


            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("ID_USER", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = True
            objDataColumn.MaxLength = 20
            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)

            
            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("NOM", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = False
            objDataColumn.MaxLength = 20
            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)

            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("PRENOM", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = False
            objDataColumn.MaxLength = 20
            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)

            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("LOGIN", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = False
            objDataColumn.MaxLength = 20
            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)

            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("id_user_typ", _
                Type.GetType("System.String"))
            objDataColumn.AllowDBNull = False
            objDataColumn.MaxLength = 20
            'Ajoute le datacolumn à la table
            objDataTable.Columns.Add(objDataColumn)


            'Instancie un nouveau DataColumn et fixe ses propriétés
            objDataColumn = New DataColumn("PWD", _
                Type.GetType("System.String"))
            objDataColumn.MaxLength = 20
            'Add the column to the table
            objDataTable.Columns.Add(objDataColumn)


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ListUser() As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            ListUser = oDLUser.ListUser
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function


    Public Function GetUserTyp() As DataSet
        Try
            'Appel le composant de donnée pour obtenir tous les utilisateurs
            GetUserTyp = oDLUser.GetUserTyp
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function UserParNom(ByVal UserName As String) As DataSet
        Try
            'Appel le composant de donnée pour obtenir un utilisateur spécifique
            UserParNom = oDLUser.UserParID(UserName)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function ConPossible(ByVal UserName As String, ByVal UserPassword As String) As Boolean
        Try

            ConPossible = oDLUser.ConPossible(UserName, UserPassword)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function can(ByVal use As String) As Boolean
        Try

            can = oDLUser.can(use)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function



    Public Function GetUserID(ByVal UserName As String, ByVal UserPassword As String) As Integer
        Try

            GetUserID = oDLUser.GetUserID(UserName, UserPassword)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function UserParID(ByVal UserName As String) As DataSet
        Try

            UserParID = oDLUser.UserParID(UserName)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message,
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function AjouterUser(ByVal User As DataSet) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour ajouter le nouvel utilisateur
            Return oDLUser.AjouterUser(User)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function MajUser(ByVal User As DataSet) As Boolean
        Try
            'Valide les données de l'utilisateur

            'Appel le composant de données pour mettre à jour l'utilisateur
            Return oDLUser.MajUser(User)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function

    Public Function SupUser(ByVal UserName As String) As Boolean
        Try
            'Appel le composant de données pour supprimer l'utilisateur
            Return oDLUser.SupUser(UserName)
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        End Try
    End Function
#End Region

End Class
