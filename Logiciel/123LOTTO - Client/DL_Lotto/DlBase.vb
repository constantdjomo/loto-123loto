﻿Imports MySql
Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports MySql.Data.Types

Public Class DlBase
    Implements IDisposable

    'variable de la classe disponible pour les classe qui vont instancier celle ci
    Public SQL As String

    Public Connection As MySqlConnection
    Public Command As MySqlCommand
    Public DataAdapter As MySqlDataAdapter
    Public DataReader As MySqlDataReader
    Public disposedvalue As Boolean = False      'detection des appels redondance


    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedvalue Then
            If disposing Then
                ' TODO: libérez les ressources non gérees lors d'appel expplicite 
                If Not DataReader Is Nothing Then
                    DataReader.Close()
                    DataReader = Nothing
                End If
                If Not DataReader Is Nothing Then
                    DataAdapter.Dispose()
                    DataAdapter = Nothing
                End If
                If Not Command Is Nothing Then
                    Command.Dispose()
                    Command = Nothing
                End If
                If Not Connection Is Nothing Then
                    Connection.Close()
                    Connection.Dispose()
                    Connection = Nothing
                End If
            End If

        End If
        Me.disposedvalue = True
    End Sub

    Public Sub New(ByVal conString As String)
        'construire la chaine de connection sql et initialiser l'objet de connection
        Connection = New MySqlConnection(conString)
    End Sub


#Region " IDisposablesupport "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region
    Public Sub OpenConnection()
        Try
            Connection.Open()
        Catch MySqlExceptionErr As MySqlException
            Throw New System.Exception(MySqlExceptionErr.Message, _
                           MySqlExceptionErr.InnerException)
        Catch InvalidOperationExceptionErr As InvalidOperationException
            Throw New System.Exception(InvalidOperationExceptionErr.Message, _
                InvalidOperationExceptionErr.InnerException)
        End Try
    End Sub
    Public Sub CloseConnection()
        Connection.Close()
    End Sub

    Public Sub InitializeCommand()
        If Command Is Nothing Then
            Try
                Command = New MySqlCommand(SQL, Connection)
                'Vérifier si c'est une procédure enregistrée
                If Not SQL.ToUpper.StartsWith("SELECT ") _
                    And Not SQL.ToUpper.StartsWith("INSERT ") _
                    And Not SQL.ToUpper.StartsWith("UPDATE ") _
                    And Not SQL.ToUpper.StartsWith("DELETE ") Then
                    Command.CommandType = CommandType.StoredProcedure
                End If
            Catch MySqlExceptionErr As MySqlException
                Throw New System.Exception(MySqlExceptionErr.Message, _
                   MySqlExceptionErr.InnerException)
            End Try
        End If
    End Sub
    Public Sub AddParameter(ByVal Name As String, ByVal Type As MySqlDbType, _
            ByVal Size As Integer, ByVal value As Object)

        Try
            Command.Parameters.Add(Name, Type, Size).Value = value
        Catch MySqlExceptionErr As MySqlException
            Throw New System.Exception(MySqlExceptionErr.Message, _
               MySqlExceptionErr.InnerException)
        End Try
    End Sub

    Public Sub InitializeDataAdapter()
        Try
            DataAdapter = New MySqlDataAdapter
            DataAdapter.SelectCommand = Command
        Catch MySqlExceptionErr As MySqlException
            Throw New System.Exception(MySqlExceptionErr.Message, _
           MySqlExceptionErr.InnerException)
        End Try
    End Sub

    Public Sub FillDataSet(ByRef oDataSet As DataSet, ByVal TableName As String)
        Try
            DataAdapter = New MySqlDataAdapter
            DataAdapter.SelectCommand = Command
            DataAdapter.Fill(oDataSet, TableName)
        Catch MySqlExceptionErr As MySqlException
            Throw New System.Exception(MySqlExceptionErr.Message, _
                       MySqlExceptionErr.InnerException)
        Finally
            Command.Dispose()
            Command = Nothing
            DataAdapter.Dispose()
            DataAdapter = Nothing
        End Try
    End Sub

    Public Sub FillDataTable(ByRef oDataTable As DataTable)
        Try
            InitializeCommand()
            InitializeDataAdapter()
            DataAdapter.Fill(oDataTable)
        Catch MySqlExceptionErr As MySqlException
            Throw New System.Exception(MySqlExceptionErr.Message, _
                MySqlExceptionErr.InnerException)
        Finally
            Command.Dispose()
            Command = Nothing
            DataAdapter.Dispose()
            DataAdapter = Nothing
        End Try
    End Sub

    Public Function ExecuteStoredProcedure() As Integer
        Try
            OpenConnection()
            'ExecuteStoredProcedure = Command.ExecuteNonQuery()
            ExecuteStoredProcedure = Command.ExecuteNonQuery()
            CloseConnection()
            Command.Dispose()
            Command = Nothing
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        Finally
            'CloseConnection()
        End Try
    End Function
    Public Function ExecuteStoredScalar() As Integer
        Try
            OpenConnection()
            'ExecuteStoredProcedure = Command.ExecuteNonQuery()
            ExecuteStoredScalar = Command.ExecuteScalar()
            CloseConnection()
            Command.Dispose()
            Command = Nothing
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
                ExceptionErr.InnerException)
        Finally
            'CloseConnection()
        End Try
    End Function

End Class
