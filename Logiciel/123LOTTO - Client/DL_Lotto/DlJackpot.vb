﻿Public Class DlJackpot
    Inherits DlBase

#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region

#Region " Public Role Functions "

    Public Function Maj_mon_Jackpot(ByVal id_jack As String, ByVal montant As String) As Boolean
        Try


            MyBase.SQL = "UPDATE jackpot SET montant_jackp = " & montant & " WHERE id_jackp =" & id_jack
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            Maj_mon_Jackpot = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetMontantJackpot(ByVal id_jackp As String) As String
        Try
            Dim dss As New DataSet

            MyBase.SQL = "Select montant_jackp from jackpot WHERE id_jackp=" & id_jackp
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "jackpot")
            GetMontantJackpot = dss.Tables("jackpot").Rows(0).Item("montant_jackp")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function GetGagnantTicketJackpot(ByVal id_ticket As String) As DataSet
        Try
            GetGagnantTicketJackpot = New DataSet
            MyBase.SQL = "Select * from `gagner_jackp` WHERE id_tick = " & id_ticket
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(GetGagnantTicketJackpot, "jackpot")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function GetMontantPariEncour(ByVal tirage As String, ByVal ID_MODE As String) As String
        Try
            Dim dss As New DataSet

            MyBase.SQL = "SELECT SUM(MONTANT_MISE) AS TOTALMISE FROM ticket t,pari p,concerner c WHERE STATUS=1 AND t.ID_TICKET=c.ID_TICKET AND p.ID_PARI=c.ID_PARI AND t.NUM_TIRAGE=" & tirage & " AND p.ID_MODE = " & ID_MODE
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "ticket")

            If (Not dss.Tables("ticket").Rows(0).IsNull("TOTALMISE")) Then
                GetMontantPariEncour = CInt(dss.Tables("ticket").Rows(0).Item("TOTALMISE"))
            Else
                GetMontantPariEncour = 0
            End If


        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function GetMontStatutJack(ByVal id_jackp As String) As String
        Try
            Dim dss As New DataSet

            MyBase.SQL = "Select etat_jackp from jackpot WHERE id_jackp=" & id_jackp
            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "jackpot")
            GetMontStatutJack = dss.Tables("jackpot").Rows(0).Item("etat_jackp")
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function


    Public Function setGagnant(ByVal id_jackp As String, ByVal tirage As String, ByVal montant_jackp As String) As String

        Dim dss As New DataSet

        MyBase.SQL = "SELECT * FROM ticket WHERE STATUS=1 AND NUM_TIRAGE = " & tirage & "  ORDER BY RAND()"
        MyBase.InitializeCommand()

        MyBase.FillDataSet(dss, "ticket")
        If dss.Tables("ticket").Rows.Count > 0 Then
            MyBase.SQL = "INSERT INTO gagner_jackp (`id_tick`, `id_jacp` ,`montant`) VALUES (" & dss.Tables("ticket").Rows(0).Item("ID_TICKET") & "," & id_jackp & " ," & montant_jackp & " )"
            'Initialisation l'objet command
            MyBase.InitializeCommand()
            ExecuteStoredProcedure()
            Return dss.Tables("ticket").Rows(0).Item("COD_BAR").ToString & "-" & dss.Tables("ticket").Rows(0).Item("ID_TICKET").ToString
        Else
            Return "0"
        End If


    End Function


    Public Function verifWinnerJackpot(ByVal id_jackp As String, ByVal tirage As String, ByRef idticket As String) As Boolean
        Try
            Dim dss As New DataSet
            Dim rest, montant_jackp As Integer
            MyBase.SQL = "SELECT ( etat_jackp - montant_jackp) as reste , montant_jackp FROM `jackpot` WHERE id_jackp =" & id_jackp

            MyBase.InitializeCommand()
            'remplir le dataset
            MyBase.FillDataSet(dss, "jackpot")
            rest = CInt(dss.Tables("jackpot").Rows(0).Item("reste"))
            montant_jackp = CInt(dss.Tables("jackpot").Rows(0).Item("montant_jackp"))

            If (rest >= 0) Then
                idticket = setGagnant(id_jackp, tirage, montant_jackp.ToString)
                Maj_Jackpot(id_jackp, rest.ToString)
                verifWinnerJackpot = True
            Else
                verifWinnerJackpot = False
            End If

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
    Public Function Maj_Jackpot(ByVal id_jack As String, ByVal montant As String) As Boolean
        Try


            MyBase.SQL = "UPDATE jackpot SET etat_jackp = " & montant & " WHERE id_jackp =" & id_jack
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            Maj_Jackpot = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

    Public Function Maj_Satut_Jackpot(ByVal id_jack As String, ByVal montant As String) As Boolean
        Try


            MyBase.SQL = "UPDATE jackpot SET etat_jackp = (etat_jackp + " & montant & ") WHERE id_jackp =" & id_jack
            'Initialisation l'objet command
            MyBase.InitializeCommand()

            Maj_Satut_Jackpot = ExecuteStoredProcedure()
        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function
#End Region
End Class
