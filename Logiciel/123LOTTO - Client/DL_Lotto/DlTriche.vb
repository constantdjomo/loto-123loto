﻿Public Class DlTriche
    Inherits DlBase

#Region "Constructor And Destructor"
    Sub New(ByVal ConString As String)
        MyBase.New(ConString)
    End Sub

    Public Shadows Sub Dispose()
        MyBase.Dispose()
    End Sub
#End Region


#Region " Public Role Functions "
    Public Function RecupNumJouer(ByVal NumTir As Integer) As DataSet
        Try
            RecupNumJouer = New DataSet

            MyBase.SQL = "Select DISTINCT NUMEROS from ticket t,avoir a where t.ID_TICKET = a.ID_TICKET AND NUM_TIRAGE=" + NumTir.ToString
            MyBase.InitializeCommand()
            'MyBase.AddParameter("@IdUser", Data.OleDb.OleDbType.VarChar,15, UserName)
            'remplir le dataset
            MyBase.FillDataSet(RecupNumJouer, "numero")
            'AjouterTirage = ExecuteStoredProcedure()

        Catch ExceptionErr As Exception
            Throw New System.Exception(ExceptionErr.Message, _
            ExceptionErr.InnerException)
        End Try
    End Function

#End Region
End Class
