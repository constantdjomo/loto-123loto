﻿Imports System.Globalization
Public Class FrmComptes
    Private oCompte As BL_Lotto.BlCompte
    Private oTicket As BL_Lotto.BlTicket
    Private oUser As BL_Lotto.BlUser

    Dim Tabdatedebut(), Tabdatefin() As String
    Dim datedebut, datefin As String
    Dim delim As Char = CChar("/")
    Dim dsTicket, dsTicketJack, dsTicketAnnul, dsTickJackMon, dsUsers, dsTouticket, dsPari, ODsPari, dsTirage, DsGain As DataSet



    Dim id_ticket As String

    Private Sub FrmComptes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Using oUser As New BL_Lotto.BlUser(source)
            'CONNECTED_USERID = "1"
            dsUsers = oUser.UserParNom(CONNECTED_USERID)

            If (dsUsers.Tables("users").Rows.Count > 0) Then
                TbNomUser.Text = dsUsers.Tables("users").Rows(0).Item("NOM").ToString & " " & dsUsers.Tables("users").Rows(0).Item("PRENOM").ToString
                Label9.Text = Date.Now.ToLongDateString
            End If

            dsUsers = oUser.ListUser()

            Cbuser.DataSource = dsUsers.Tables("users")

            Cbuser.DisplayMember = "NOM"
            Cbuser.ValueMember = "ID_USER"

        End Using

    End Sub

    Private Sub BtOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtOk.Click

        Dim Gagner As Boolean = False
        Dim Gains As Integer = 0
        Dim TotalmontantJack As Integer = 0
        Dim totaltickannuller As Integer = 0
        Using oCompte As New BL_Lotto.BlCompte(source)

            If DateTime.Compare(DpDebut.Value, DpFin.Value) <= 0 Then
                Tabdatedebut = DpDebut.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
                datedebut = Tabdatedebut(2) & "/" & Tabdatedebut(1) & "/" & Tabdatedebut(0)
                'datedebut = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
                'MsgBox(datedebut)
                Tabdatefin = DpFin.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture).Split(CChar("/"))
                datefin = Tabdatefin(2) & "/" & Tabdatefin(1) & "/" & Tabdatefin(0)
                'datefin = Date.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
                If Cbuser.SelectedValue.ToString <> "0" Then
                    dsTicket = oCompte.getStatTicketParUser(Cbuser.SelectedValue.ToString(), datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTicketAnnul = oCompte.getStatTickAnnulParUser(Cbuser.SelectedValue.ToString(), datedebut & " 00:00:00", datefin & " 23:59:59")

                    dsTicketJack = oCompte.getStatTicketJackParUser(Cbuser.SelectedValue.ToString(), datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTickJackMon = oCompte.getStatTickJackMonParUser(Cbuser.SelectedValue.ToString(), datedebut & " 00:00:00", datefin & " 23:59:59")

                    dsPari = oCompte.getStatPariParUser(Cbuser.SelectedValue.ToString(), datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTouticket = oCompte.getListTicketServi(Cbuser.SelectedValue.ToString(), datedebut & " 00:00:00", datefin & " 23:59:59")


                Else
                    dsTicket = oCompte.getStatTicket(datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTicketAnnul = oCompte.getStatTickAnnul(datedebut & " 00:00:00", datefin & " 23:59:59")

                    dsTicketJack = oCompte.getStatTicketJack(datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTickJackMon = oCompte.getStatTickJackMon(datedebut & " 00:00:00", datefin & " 23:59:59")

                    dsPari = oCompte.getStatPari(datedebut & " 00:00:00", datefin & " 23:59:59")
                    dsTouticket = oCompte.getListTicket(datedebut & " 00:00:00", datefin & " 23:59:59")
                End If

                If (dsTicketJack.Tables("ticket").Rows.Count > 0) Then
                    LbNbtTicketJack.Text = dsTicketJack.Tables("ticket").Rows(0).Item("NB_TICKET").ToString
                End If
                If (dsTickJackMon.Tables("ticket").Rows.Count > 0) Then
                    If (Not dsTickJackMon.Tables("ticket").Rows(0).IsNull("TOTAL_MONTANT")) Then
                        TotalmontantJack = CInt(dsTickJackMon.Tables("ticket").Rows(0).Item("TOTAL_MONTANT"))
                    Else
                        TotalmontantJack = 0
                    End If
                End If

                If (dsTicketAnnul.Tables("ticket").Rows.Count > 0) Then
                    If (Not dsTicketAnnul.Tables("ticket").Rows(0).IsNull("NB_TICKET")) Then
                        totaltickannuller = CInt(dsTicketAnnul.Tables("ticket").Rows(0).Item("NB_TICKET"))
                    Else
                        totaltickannuller = 0
                    End If
                End If

                If (dsTicket.Tables("ticket").Rows.Count > 0) Then
                    LbNbTicket.Text = dsTicket.Tables("ticket").Rows(0).Item("NB_TICKET").ToString
                    LbTotalMise.Text = dsPari.Tables("pari").Rows(0).Item("TOTALMISE").ToString

                    'calcul du nombre de ticket gagnant


                    Dim MontantSortie, nbticketGagnant As Integer
                    nbticketGagnant = 0
                    MontantSortie = 0
                    Using oTicket As New BL_Lotto.BlTicket(source)
                        For i As Integer = 0 To dsTouticket.Tables("ticket").Rows.Count - 1
                            id_ticket = dsTouticket.Tables("ticket").Rows(i).Item("ID_TICKET").ToString

                            Dim oDsNumJou As DataSet

                            ODsPari = oTicket.GetPari(id_ticket)
                            Gagner = False
                            Gains = 0
                            'oDsTirage = oTicket.getTirage(dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                            If (ODsPari.Tables("pari").Rows.Count > 0) Then

                                For k As Integer = 0 To ODsPari.Tables("pari").Rows.Count - 1
                                    'oDsNumJou = oTicket.getNumJouer(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString)

                                    If (oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                                    dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)) Then
                                        Gains += (CInt(ODsPari.Tables("pari").Rows(k).Item("MONTANT_MISE")) * CInt(ODsPari.Tables("pari").Rows(k).Item("QUOTA")))

                                    End If


                                    Gagner = Gagner Or oTicket.verifWinner(ODsPari.Tables("pari").Rows(k).Item("ID_PARI").ToString,
                                                                                dsTouticket.Tables("ticket").Rows(i).Item("NUM_TIRAGE").ToString)

                                Next

                            End If

                            oDsNumJou = oTicket.getNumJouer(id_ticket)
                            If (Gagner) Then
                                nbticketGagnant += 1
                                MontantSortie += CInt(Gains)
                            End If

                        Next
                    End Using

                    MontantSortie += TotalmontantJack

                    LbNbtTicketGagna.Text = nbticketGagnant.ToString
                    LbTotalSortie.Text = MontantSortie.ToString
                    Lb_sortie_jackpot.Text = TotalmontantJack.ToString
                    Lb_Nb_annule.Text = totaltickannuller.ToString

                    If LbTotalMise.Text = "" Then
                        LbTotalMise.Text = 0.ToString
                    End If
                    If LbTotalSortie.Text = "" Then
                        LbTotalSortie.Text = 0.ToString
                    End If

                    Dim benef As Integer = CInt(LbTotalMise.Text) - CInt(LbTotalSortie.Text)
                    benef = CInt(LbTotalMise.Text) - CInt(LbTotalSortie.Text)
                    LbBenefice.Text = benef.ToString
                Else
                    LbNbTicket.Text = "0"
                    LbTotalMise.Text = "0"
                End If


                dsTirage = oCompte.getStatTirage(datedebut & " 00:00:00", datefin & " 23:59:59")
                If (dsTirage.Tables("tirage").Rows.Count > 0) Then
                    LbNbTirage.Text = dsTirage.Tables("tirage").Rows(0).Item("NB_TIRAGE").ToString
                Else
                    LbNbTirage.Text = "0"
                End If

                'on gere le credit residuel
                If (oCompte.getCreditResiduel = -1) Then
                    Lbcreditesiduel.Text = "Illimité"
                Else
                    Lbcreditesiduel.Text = CStr(oCompte.getCreditResiduel)
                End If



            Else
                MessageBox.Show("Attention a la plage de Date.", "123... LOTO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Using
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

    End Sub

    Private Sub FrmComptes_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        FrmPrincipal.Show()
    End Sub
End Class