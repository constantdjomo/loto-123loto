﻿Public Class setting


    Private Sub BtValider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtValider.Click
        If MsgBox("Etes-vous sûr de vouloir l'enregistrer?", MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Ok Then

            If (TbCodeSal.Text <> "") Then

                My.Settings.PRINTER_NAME = TbNameImp.Text
                My.Settings.HOST = TbHost.Text
                My.Settings.USER_BD = TBUser.Text
                My.Settings.PWD_BD = TbPwd.Text
                My.Settings.CODE_SALE = TbCodeSal.Text.ToUpper

                Me.Close()

            End If

        Else
            Me.Close()
        End If
    End Sub


    Private Sub setting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        TbNameImp.Text = My.Settings.PRINTER_NAME
        TbHost.Text = My.Settings.HOST

        TBUser.Text = My.Settings.USER_BD
        TbPwd.Text = My.Settings.PWD_BD
        TbCodeSal.Text = My.Settings.CODE_SALE
       
    End Sub

   
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class